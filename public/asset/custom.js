var isset = function(variable){
	return typeof(variable) !== "undefined" && variable !== null && variable !== '' && variable !== 'null' ? variable : '';
}

var humanDate = function(variable){
	if(typeof(variable) !== "undefined" && variable !== null && variable !== ''){
		var dt = variable.split('-');
		return dt[2]+'/'+dt[1]+'/'+dt[0];
	};
}

$(function(){
	$(".dates").datepicker({ autoclose: true, todayHighlight: true, format: 'dd-mm-yyyy' }).inputmask({'alias':'dd-mm-yyyy'});
	$(".datenow").datepicker({ autoclose: true, todayHighlight: true, format: 'dd-mm-yyyy', startDate: new Date() }).inputmask({'alias':'dd-mm-yyyy'});
	$(".datemax").datepicker({ autoclose: true, todayHighlight: true, format: 'dd-mm-yyyy', endDate: new Date() }).inputmask({'alias':'dd-mm-yyyy'});
	$('input[type="radio"]').iCheck({radioClass: 'iradio_flat-blue'});
	$('input[type="checkbox"]').iCheck({checkboxClass: 'icheckbox_flat-blue'});
	$('select').select2({placeholder: '--Pilih--', allowClear  : true, closeOnSelect : true, width : '100%' });
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		if($('div.chart').length){
			$('.chart').each(function() {
				// $(this).highcharts().reflow();
			});
		}
	});
});

function getKabupaten(id){
	var id_provinsi = $('#id_provinsi'+id).val();
	var url = BASE_URL+'wilayah/kabupaten';
	$('#id_kabupaten'+id).html('<option value="">Pilih Kabupaten</option>').trigger('change');
	if(isset(id_provinsi)){
		$.post(url,{id_provinsi:id_provinsi},function(data, status){
			if(isset(data)){
				$.each(JSON.parse(data), function(key, value) {
					$('#id_kabupaten'+id)
					.append($("<option></option>")
						.attr("value",key)
						.text(value));
				});
				if (isset($('.id_kabupaten' + id).val())) {
					$('#id_kabupaten' + id).select2('val',$('.id_kabupaten' + id).val());
				}
			}
		});
		$('#id_kabupaten'+id).removeAttr('disabled');
	}else{
		$('#id_kabupaten'+id).attr('disabled','disabled');
	}

	return false;
}

function getKecamatan(id){
	var id_kabupaten = $('#id_kabupaten'+id).val();
	var url = BASE_URL+'wilayah/kecamatan';
	$('#id_kecamatan'+id).html('<option value="">Pilih Kecamatan</option>').trigger('change');
	if(isset(id_kabupaten)){
		$.post(url,{id_kabupaten:id_kabupaten},function(data, status){
			if(isset(data)){
				$.each(JSON.parse(data), function(key, value) {
					$('#id_kecamatan'+id)
					.append($("<option></option>")
						.attr("value",key)
						.text(value));
				});
				if (isset($('.id_kecamatan' + id).val())) {
					$('#id_kecamatan' + id).select2('val', $('.id_kecamatan' + id).val());
				}
			}
		})
		$('#id_kecamatan'+id).removeAttr('disabled');
	}else{
		$('#id_kecamatan'+id).attr('disabled','disabled');
	}
	return false;
}

function getKelurahan(id){
	var id_kecamatan = $('#id_kecamatan'+id).val();
	var url = BASE_URL+'wilayah/kelurahan';
	$('#id_kelurahan'+id).html('<option value="">Pilih Kelurahan</option>').trigger('change');
	if(isset(id_kecamatan)){
		$.post(url,{id_kecamatan:id_kecamatan},function(data, status){
			if(isset(data)){
				$.each(JSON.parse(data), function(key, value) {
					$('#id_kelurahan'+id)
					.append($("<option></option>")
						.attr("value",key)
						.text(value));
				});
				if (isset($('.id_kelurahan' + id).val())) {
					$('#id_kelurahan' + id).select2('val', $('.id_kelurahan' + id).val());
				}
			}
		})
		$('#id_kelurahan'+id).removeAttr('disabled');
	}else{
		$('#id_kelurahan'+id).attr('disabled','disabled');
	}
	return false;
}

function getPuskesmas(id) {
	var code_provinsi = $('#id_provinsi'+id).val();
	var code_kabupaten = $('#id_kabupaten'+id).val();
	var code_kecamatan = $('#id_kecamatan'+id).val();
	var url = BASE_URL+'puskesmas/list';
	$('#puskesmas'+id).html('<option value="">Pilih Puskesmas</option>').trigger('change');
	$('#puskesmas'+id).append($("<option></option>").attr("value",'-').text('SEMUA PUSKESMAS'));
	if(isset(code_provinsi) || isset(code_kabupaten) || isset(code_kecamatan)){
		$('#puskesmas'+id).html('<option value="">Pilih Puskesmas</option>').trigger('change');
		$('#puskesmas'+id).append($("<option></option>").attr("value",'-').text('SEMUA PUSKESMAS'));
		$.post(url,{code_provinsi:code_provinsi,code_kabupaten:code_kabupaten,code_kecamatan:code_kecamatan},function(data, status){
			if(isset(data)){
				$.each(JSON.parse(data), function(key, value) {
					$('#puskesmas'+id)
					.append($("<option></option>")
						.attr("value",value.id)
						.text(value.name));
				});
			}
		});
		$('#puskesmas'+id).removeAttr('disabled');
		$('#rs'+id).val('').attr('disabled','disabled').trigger('change');
	}else{
		$('#puskesmas'+id).attr('disabled','disabled');
		var id_kabupaten = $('#id_kabupaten'+id).val();
		if(isset(id_kabupaten)){
			$('#rs'+id).removeAttr('disabled');
		}
	}
	$('#kode_puskesmas, #alamat, #xyz').val(null);
	$('#puskesmas'+id).val(null);
	return false;
}

function getRs(id) {
	var code_provinsi = $('#id_provinsi'+id).val();
	var code_kabupaten = $('#id_kabupaten'+id).val();
	var url = BASE_URL+'rs/list';
	$('#rs'+id).html('<option value="">Pilih Rumah Sakit</option>').trigger('change');
	$('#rs'+id).append($("<option></option>").attr("value",'-').text('SEMUA RUMAH SAKIT'));
	if(isset(code_provinsi) || isset(code_kabupaten)){
		$.post(url,{code_provinsi:code_provinsi,code_kabupaten:code_kabupaten},function(data, status){
			$('#rs'+id).html('<option value="">Pilih Rumah Sakit</option>').trigger('change');
			$('#rs'+id).append($("<option></option>").attr("value",'-').text('SEMUA RUMAH SAKIT'));
			if(data){
				$.each(JSON.parse(data), function(key, value) {
					$('#rs'+id)
					.append($("<option></option>")
						.attr("value",value.id)
						.text(value.name));
				});
			}
		});
		$('#rs'+id).removeAttr('disabled');
	}else{
		$('#rs'+id).attr('disabled','disabled');
	}
	$('#kode_rs, #alamat, #xyz').val(null);
	$('#rs'+id).val(null);
	return false;
}

function getWilayahKerja(id){
	var id_puskesmas = $('#puskesmas'+id).val();
	var url = BASE_URL+'puskesmas/wilayahKerjaPuskesmas';
	$('#id_wilayah_kerja'+id).html('<option value="">Pilih Wilayah Kerja Puskesmas</option>').trigger('change');
	if(isset(id_puskesmas)){
		$.post(url,{id_puskesmas:id_puskesmas},function(data, status){
			if(isset(data)){
				$.each(JSON.parse(data), function(key, value) {
					$('#id_wilayah_kerja'+id)
					.append($("<option></option>")
						.attr("value",key)
						.text(value));
				});
			}
		})
		$('#id_wilayah_kerja'+id).removeAttr('disabled');
	}else{
		$('#id_wilayah_kerja'+id).attr('disabled','disabled');
	}
	return false;
}

function getAge() {
	var tgl_lahir = $('#tgl_lahir').val();
	// var umur = $('#umur').val();
	// var umur_bln = $('#umur_bln').val();
	// var umur_hari = $('#umur_hari').val();
	var tgl_mulai_demam = $('#tgl_mulai_demam').val();
	var tgl_mulai_rash = $('#tgl_mulai_rash').val();
	var tgl_mulai_lumpuh = $('#tgl_mulai_lumpuh').val();
	var tgl_mulai_sakit = $('#tgl_mulai_sakit').val();
	var tgl_periksa = $('#tgl_periksa').val();
	var tgl_sakit = '';
	if(isset(tgl_mulai_demam)){
		tgl_sakit = tgl_mulai_demam;
	}
	if(isset(tgl_mulai_rash)){
		tgl_sakit = tgl_mulai_rash;
	}
	if(isset(tgl_mulai_lumpuh)){
		tgl_sakit = tgl_mulai_lumpuh;
	}
	if(isset(tgl_mulai_sakit)){
		tgl_sakit = tgl_mulai_sakit;
	}
	if(isset(tgl_periksa)){
		tgl_sakit = tgl_periksa;
	}
	if((tgl_lahir /*|| umur || umur_bln || umur_hari*/) && tgl_sakit){
		var url = BASE_URL+'pasien/getAge';
		$.post(url, JSON.stringify({tgl_lahir:tgl_lahir/*, umur:umur, umur_bln:umur_bln, umur_hari:umur_hari*/, tgl_sakit:tgl_sakit}), function(data, status){
			// if(isset(tgl_lahir)){
				$('#umur_thn').val(data.response.years).removeAttr('disabled');
				$('#umur_bln').val(data.response.month).removeAttr('disabled');
				$('#umur_hari').val(data.response.day).removeAttr('disabled');
			/*}else{
				$('#tgl_lahir').val(data.response.tgl_lahir).removeAttr('disabled');
			}*/
		});
	}
	return false;
}

// messageAlert('success/info/danger/warning', 'Informasi', 'Ada perubahan sistem');
function messageAlert(status, title, message) {
	$('.callout').addClass('callout-'+status);
	$('.notification').find('.title').text(title);
	$('.notification').find('.message').text(message);
	$('.notification').fadeIn(function(){
		setTimeout(function(){
			$('.notification').fadeOut();
		},3000);
	});
}

function startProcess()
{
	$.blockUI({
		message: $('#progress-bar-box'),
		css : {
			padding:0,
			margin:0,
			border: 'none',
			backgroundColor: 'none',
		}
	});
}

function endProcess()
{
	setTimeout($.unblockUI, 100);
}

function active(id, val) {
	$('.'+id+val).removeAttr('disabled');
	return false;
}

function deactive(id, val) {
	$('.'+id+val).attr('disabled','disabled').val(null);
	return false;
}

var data_type = {
	csv: 'text/csv',
	txt: 'text/plain',
	xls: 'application/vnd.ms-excel',
	json: 'application/json',
};

function exportXls(title, headers, items) {
	template = '<table><thead>%thead%</thead><tbody>%tbody%</tbody></table>';
	var res = '';
	res += '<tr><th colspan="3">'+title+'</th></tr>';
	res += '<tr><th></th></tr>';
	headers.forEach(function(item, i) {
		res += '<th>' + item + '</th>';
	});
	template = template.replace('%thead%', res);
	res = '';
	items.forEach(function(item, i) {
		res += '<tr>';
		item.forEach(function(td, i) {
			res += '<td>' + td + '</td>';
		});
		res += '</tr>';
	});
	template = template.replace('%tbody%', res);
	result = template;
	download(result,title+'_%DD%-%MM%-%YY%','xls');
}

function download(data, filename, format) {
	var a = document.createElement('a');
	var blob = new Blob(['\ufeff', data], { type: data_type[format] });
	a.href = URL.createObjectURL(blob);

	var now = new Date();
	var time_arr = [
	'DD:' + now.getDate(),
	'MM:' + (now.getMonth() + 1),
	'YY:' + now.getFullYear(),
	'hh:' + now.getHours(),
	'mm:' + now.getMinutes(),
	'ss:' + now.getSeconds(),
	];

	time_arr.forEach(function(item) {
		var key = item.split(':')[0];
		var val = item.split(':')[1];
		var regexp = new RegExp('%' + key + '%', 'g');
		filename = filename.replace(regexp, val);
	});

	a.download = filename + '.' + format;
	document.body.appendChild(a);
	a.click();
    document.body.removeChild(a);

	if (!navigator.userAgent.toLowerCase().match(/firefox/)) {
		a.remove();
	}
}

function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;//If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var CSV = '';
    CSV += ReportTitle + '\r\n\n';//Set Report title in first row or line
    //This condition will generate the Label/Header
    if (ShowLabel) {
    	var row = "";
        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {
        	row += index + ',';
        }
        row = row.slice(0, -1);
        CSV += row + '\r\n';
     }
    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
    	var row = "";
        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
        	row += '"' + arrData[i][index] + '",';
        }
        row.slice(0, row.length - 1);
        //add a line break after each row
        CSV += row + '\r\n';
     }
     if (CSV == '') {
     	alert("Invalid data");
     	return;
     }
    var fileName = "Analisa_Berdasar_";//Generate a file name
    fileName += ReportTitle.replace(/ /g,"_");//this will remove the blank-spaces from the title and replace it with an underscore
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);//Initialize file format you want csv or xls

    var link = document.createElement("a");//this trick will generate a temp <a /> tag
    link.href = uri;
    link.style = "visibility:hidden";    //set the visibility hidden so it will not effect on your web-layout
    link.download = fileName + ".csv";
    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
 }

 function JSONToTXTConvertor(JSONData, ReportTitle, ShowLabel) {

    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;//If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var CSV = '';
    CSV += ReportTitle + '\r\n\n';//Set Report title in first row or line
    //This condition will generate the Label/Header
    if (ShowLabel) {
    	var row = "";
        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {
        	row += index + ',';
        }
        row = row.slice(0, -1);
        CSV += row + '\r\n';
     }
    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
    	var row = "";
        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
        	row += '"' + arrData[i][index] + '",';
        }
        row.slice(0, row.length - 1);
        //add a line break after each row
        CSV += row + '\r\n';
     }
     if (CSV == '') {
     	alert("Invalid data");
     	return;
     }
    var fileName = "Analisa_Berdasar_";//Generate a file name
    fileName += ReportTitle.replace(/ /g,"_");//this will remove the blank-spaces from the title and replace it with an underscore
    var uri = 'data:text/json;charset=utf-8,' + escape(CSV);//Initialize file format you want csv or xls

    var link = document.createElement("a");//this trick will generate a temp <a /> tag
    link.href = uri;
    link.style = "visibility:hidden";    //set the visibility hidden so it will not effect on your web-layout
    link.download = fileName + ".txt";
    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
 }

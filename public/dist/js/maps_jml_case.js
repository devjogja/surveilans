function formatterIndonesia(e,type){
         var s = e.name + '<br/>';
         if(!type) {
            s += 'Jumlah Kasus Campak : ' + e.campak + ' orang<br/>';
            s += 'Jumlah Kasus Tetanus : ' + e.tetanus + ' orang<br/>';
            s += 'Jumlah Kasus Difteri : ' + e.difteri + ' orang<br/>';
         }else{
            s += 'Jumlah Kasus '+type+' : ' + e.value + ' orang<br/>';
         }
         return s;
     };

     function formatterProvinsi(e,type){
         var s = e.name + '<br/>';
         if(!type) {
            s += 'Jumlah Kasus Campak : ' + e.campak + ' orang<br/>';
            s += 'Jumlah Kasus Tetanus : ' + e.tetanus + ' orang<br/>';
            s += 'Jumlah Kasus Difteri : ' + e.difteri + ' orang<br/>';
         }else{
            s += 'Jumlah Kasus '+type+' : ' + e.value + ' orang<br/>';
         }
         return s;
     };

	function getMapsDrilldown() {
		var type = $('#Maps').val();
		var mapData = Highcharts.geojson(Highcharts.maps['indonesia']);
        $.each(mapData, function () {
            this.id = this.properties['SPROVINCE'];
            // this.name = this.properties['PROVINCE'];
            this.drilldown = this.properties['SPROVINCE'];
        });
		var data1 = [];
    var identifier=0;
    var points, tmp;
    var map;
    var data2=[{
                'kabkot': 'KULON PROGO',
                'campak' : 10,
                'tetanus' : 3,
                'difteri' :7,
                'value' :20
            },{
                'kabkot': 'SLEMAN',
                'campak' : 4,
                'tetanus' : 1,
                'difteri' :3,
                'value' :8
            },{
                'kabkot': 'BANTUL',
                'campak' : 12,
                'tetanus' : 11,
                'difteri' :13,
                'value' :43
            },{
                'kabkot': 'KOTA YOGYAKARTA',
                'campak' : 12,
                'tetanus' : 11,
                'difteri' :13,
                'value' :43
            },{
                'kabkot': 'GUNUNG KIDUL',
                'campak' : 12,
                'tetanus' : 11,
                'difteri' :13,
                'value' :43
            },{
      				'kabkot': 'PANDEGLANG',
      				'value' :8
      			},{
      				'kabkot': 'LEBAK',
      				'value' :32
      			},{
              'kabkot': 'KOTA TANGERANG',
              'value' :8
            },{
              'kabkot': 'SERANG',
              'value' :32
            },{
              'kabkot': 'KOTA SERANG',
              'value' :32
            },{
              'kabkot': 'CILEGON',
              'value' :32
            },{
              'kabkot': 'TANGERANG SELATAN',
              'value' :32
            },{
              'kabkot': 'TANGERANG UTARA',
              'value' :32
            }];
		if (!type) {
			data1=[{
    				'province': 'BALI',
    				'campak' : 4,
    				'tetanus' : 1,
    				'difteri' :3,
    				'value' :8
    			},{
    				'province': 'YOGYA',
    				'campak' : 12,
    				'tetanus' : 11,
    				'difteri' :13,
    				'value' :43
    			},{
    				'province': 'BANTEN',
    				'campak' : 12,
    				'tetanus' : 11,
    				'difteri' :13,
    				'value' :0
    			}];
		}else{
			data1=[{
    				'province': 'BALI',
    				'value' :8
    			},{
    				'province': 'YOGYA',
    				'value' :32
    			},{
    				'province': 'BANTEN',
    				'value' :0
    			}];
		}

		map = $('#graph_maps').highcharts('Map', {
			chart : {
                events: {
                    drilldown: function (e) {

                    if (!e.seriesOptions) {
                        var chart = this,

                            mapKey = e.point.drilldown,
                            // Handle error, the timeout is cleared on success
                            fail = setTimeout(function () {
                                if (!Highcharts.maps[mapKey]) {
                                    chart.showLoading('<i class="icon-frown"></i> Failed loading ' + e.point.name);
                                    fail = setTimeout(function () {
                                        chart.hideLoading();
                                    }, 1000);
                                }
                            }, 3000);

                        // Show the spinner
                        chart.showLoading('<i class="icon-spinner icon-spin icon-3x"></i>');
                            mapData = Highcharts.geojson(Highcharts.maps[mapKey]);
                            if(identifier==0){
                              tmp=mapKey;
                              identifier++;
                            }else{
                              identifier=0;
                            }
                            console.log(tmp);
                            // Set a non-random bogus value
                            $.each(mapData, function (i) {
                                // this.name = this.properties['kabkot'];
                                this.drilldown = this.name;
                                this.mapKey=mapKey;
                                // this.value = i;
                            });

                            // Hide loading and add series
                            chart.hideLoading();
                            clearTimeout(fail);
                            chart.tooltip.options.formatter=function(){
                                 var s = this.point;
                                 var t =formatterProvinsi(s,type);
                                 return t;
                             };
                            chart.addSeriesAsDrilldown(e.point, {
                                name: e.point.properties['kabkot'],
                                mapData: mapData,
                                data: data2,
                                joinBy:['kabkot','kabkot'],
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}'
                                },
                            });

                    }
                    this.setTitle(null, { text: e.point.name });
                },
                drillup: function () {
                    mapData = Highcharts.geojson(Highcharts.maps[tmp]);
                    this.tooltip.options.formatter=function(){
                         var s = this.point;
                         var t =formatterIndonesia(s,type);
                         return t;

                     };
                    //  chart=null;
                    // this.setTitle(null, { text: 'INDONESIA' });
                    // getMaps();
                }
            }
        },
        title : {
				text : 'Peta Persebaran Kasus '+type
			},
			mapNavigation: {
				enabled: true,
				buttonOptions: {
					verticalAlign: 'bottom'
				}
			},
			credits:{
        		enabled:false
        	},
			tooltip: {
                formatter: function(){
                    var s=this.point;
                    var t =formatterIndonesia(s,type);
                    return t;
                },
             },
			colorAxis: {
                    dataClasses: [{
                        to: 0,
                        color:'#4CAF50',
                        name:'0',
                    }, {
                        from: 1,
                        to: 9,
                        name:'1-9',
                        color:"#AEEA00"
                    }, {
                        from: 10,
                        to: 99,
                        name:'10-99',
                        color:"#FFEB3B"
                    }, {
                        from: 100,
                        to: 999,
                        name:'100-999',
                        color:"#E65100"
                    }, {
                        from: 1000,
                        name:'>1000',
                        color:"#F44336"
                    }]
                },

			series: [{
        name:'Indonesia',
				mapData: mapData,
				joinBy: ['SPROVINCE', 'province'],
    			data:data1,
    			allowPointSelect: false,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                },
    			cursor: 'pointer',
                // events: {
                //     click: function (e) {
                //         console.log(e);
                //     }
                // },
    			states: {
					hover: {
						color: '#006064'
					},
					select: {
                        color: '#2196F3',
                        borderColor: 'white',
                        dashStyle: 'line'
                    }
				}
			}],
			drilldown: {
	            //series: drilldownSeries,
	            activeDataLabelStyle: {
	                color: '#FFFFFF',
	                textDecoration: 'none',
	                textShadow: '0 0 3px #000000'
	            },
	            drillUpButton: {
	                relativeTo: 'spacingBox',
	                position: {
	                    x: 0,
	                    y: 60
	                }
	            }
        	}
		}).highcharts();
		 // map.get('BENGKULU').zoomTo();
   //       points = map.getSelectedPoints();
   //       console.log(points);
	}

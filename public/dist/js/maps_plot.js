function formatterIndonesia(e,type){
         var s = e.name + '<br/>';
         if(!type) {
            s += 'Jumlah Kasus Campak : ' + e.campak + ' orang<br/>';
            s += 'Jumlah Kasus Tetanus : ' + e.tetanus + ' orang<br/>';
            s += 'Jumlah Kasus Difteri : ' + e.difteri + ' orang<br/>';
         }else{
            s += 'Jumlah Kasus '+type+' : ' + e.value + ' orang<br/>';
         }
         return s;
     };

     function formatterProvinsi(e,type){
         var s = e.name + '<br/>';
         if(!type) {
            s += 'Jumlah Kasus Campak : ' + e.campak + ' orang<br/>';
            s += 'Jumlah Kasus Tetanus : ' + e.tetanus + ' orang<br/>';
            s += 'Jumlah Kasus Difteri : ' + e.difteri + ' orang<br/>';
         }else{
            s += 'Jumlah Kasus '+type+' : ' + e.value + ' orang<br/>';
         }
         return s;
     };

	function getMapsPlot() {
		var type = $('#Maps').val();
		var mapData = Highcharts.geojson(Highcharts.maps['indonesia']);
        $.each(mapData, function () {
            this.id = this.properties['SPROVINCE'];
            this.name = this.properties['PROVINCE'];
            this.drilldown = this.properties['SPROVINCE'];
        });
		var data1 = [];
        var points;
		var map;
        var data2=[{
                    'kabkot': 'KULON PROGO',
                    'campak' : 10,
                    'tetanus' : 3,
                    'difteri' :7,
                    'value' :20
                },{
                    'kabkot': 'SLEMAN',
                    'campak' : 4,
                    'tetanus' : 1,
                    'difteri' :3,
                    'value' :8
                },{
                    'kabkot': 'BANTUL',
                    'campak' : 12,
                    'tetanus' : 11,
                    'difteri' :13,
                    'value' :43
                }];
		if (!type) {
			data1=[{
    				'province': 'BENGKULU',
    				'campak' : 10,
    				'tetanus' : 3,
    				'difteri' :7,
    				'value' :20
    			},{
    				'province': 'GRTALO',
    				'campak' : 4,
    				'tetanus' : 1,
    				'difteri' :3,
    				'value' :8
    			},{
    				'province': 'YOGYA',
    				'campak' : 12,
    				'tetanus' : 11,
    				'difteri' :13,
    				'value' :43
    			},{
            'province': 'PAPBAR',
            'campak' : 1,
            'tetanus' : 1,
            'difteri' :1,
            'value' :2
        }];
		}else{
			data1=[{
    				'province': 'BENGKULU',
    				'value' :20
    			},{
    				'province': 'GRTALO',
    				'value' :8
    			},{
    				'province': 'YOGYA',
    				'value' :32
    			},{
            'province': 'PAPBAR',
            'value' :2
        }];
		}

		map = $('#graph_maps').highcharts('Map', {
			chart : {
                events: {
                    drilldown: function (e) {

                    if (!e.seriesOptions) {
                        var chart = this,
                            mapKey = e.point.drilldown,
                            // Handle error, the timeout is cleared on success
                            fail = setTimeout(function () {
                                if (!Highcharts.maps[mapKey]) {
                                    chart.showLoading('<i class="icon-frown"></i> Failed loading ' + e.point.name);

                                    fail = setTimeout(function () {
                                        chart.hideLoading();
                                    }, 1000);
                                }
                            }, 3000);

                        // Show the spinner
                        chart.showLoading('<i class="icon-spinner icon-spin icon-3x"></i>');
                            mapData = Highcharts.geojson(Highcharts.maps[mapKey]);
                            // Set a non-random bogus value
                            $.each(mapData, function (i) {
                                this.name = this.properties['kabkot'];
                                this.drilldown = this.properties['kabkot'];
                                // this.value = i;
                            });

                            // Hide loading and add series
                            chart.hideLoading();
                            clearTimeout(fail);
                            chart.tooltip.options.formatter=function(){
                                 var s = this.point;
                                 var t =formatterProvinsi(s,type);
                                 return t;
                             };
                            chart.addSeriesAsDrilldown(e.point, {
                                name: e.point.properties['kabkot'],
                                mapData: mapData,
                                data: data2,
                                joinBy:['name','kabkot'],
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}'
                                },
                            });
                            // plotData = chart.addSeries({
                            //     id: 'points',
                            //     type: 'mappoint',
                            //     name: 'Persebaran Data Kasus',
                            //     // showInLegend: false,
                            //     data: [{
                            //         name: 'Sleman',
                            //         x: -7.7009743,
                            //         y: 110.4310616
                            //     },{
                            //         name: 'Bantul',
                            //         x: -7.8139255,
                            //         y: 110.4444341
                            //     }
                            
                            //     ]
                            // });

                    }
                    this.setTitle(null, { text: e.point.name });
                },
                drillup: function () {
                    plotData.remove(false);
                    this.tooltip.options.formatter=function(){
                         var s = this.point;
                         var t =formatterIndonesia(s,type);
                         return t;
                     };
                    // this.setTitle(null, { text: 'INDONESIA' });
                    // getMaps();
                }
            }
        },
        title : {
				text : 'Peta Persebaran Kasus '+type
			},
			mapNavigation: {
				enabled: true,
				buttonOptions: {
					verticalAlign: 'bottom'
				}
			},
			credits:{
        		enabled:false
        	},
			tooltip: {
                formatter: function(){
                    var s=this.point;
                    var t =formatterIndonesia(s,type);
                    return t;
                },
             },
			colorAxis: {
                    dataClasses: [{
                        to: 0,
                        color:'#4CAF50',
                        name:'0',
                    }, {
                        from: 1,
                        to: 9,
                        name:'1-9',
                        color:"#AEEA00"
                    }, {
                        from: 10,
                        to: 99,
                        name:'10-99',
                        color:"#FFEB3B"
                    }, {
                        from: 100,
                        to: 999,
                        name:'100-999',
                        color:"#E65100"
                    }, {
                        from: 1000,
                        name:'>1000',
                        color:"#F44336"
                    }]
                },

			series: [{
                name:'Indonesia',
				mapData: mapData,
				joinBy: ['SPROVINCE', 'province'],
    			data:data1,
    			allowPointSelect: false,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                },
    			cursor: 'pointer',
                // events: {
                //     click: function (e) {
                //         console.log(e);
                //     }
                // },
    			states: {
					hover: {
						color: '#006064'
					},
					select: {
                        color: '#2196F3',
                        borderColor: 'white',
                        dashStyle: 'line'
                    }
				}
			}],
			drilldown: {
	            //series: drilldownSeries,
	            activeDataLabelStyle: {
	                color: '#FFFFFF',
	                textDecoration: 'none',
	                textShadow: '0 0 3px #000000'
	            },
	            drillUpButton: {
	                relativeTo: 'spacingBox',
	                position: {
	                    x: 0,
	                    y: 60
	                }
	            }
        	}
		}).highcharts();
		 // map.get('BENGKULU').zoomTo();
   //       points = map.getSelectedPoints();
   //       console.log(points);
	}

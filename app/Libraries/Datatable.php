<?php

/**
 * Data table Class Datatable
 * @author      Hanif Burhanudin | _boerhan
 * @link        boerhan.jogja@gmail.com
 */

namespace App\Libraries;

use DB;
use Request;

class Datatable
{
    public static function getData($params = array())
    {
        $query = DB::table($params['table']);
        if (isset($params['join'])) {
            foreach ($params['join'] as $key => $val) {
                $query->join($val[0], $val[1], '=', $val[2]);
            }
        }
        if (isset($params['leftJoin'])) {
            foreach ($params['leftJoin'] as $key => $val) {
                $query->leftJoin($val[0], $val[1], '=', $val[2]);
            }
        }
        if (isset($params['key'])) {
            $query->select($params['key']);
        }
        if (isset($params['columnSelect'])) {
            $query->addSelect($params['columnSelect']);
        }
        if (isset($params['where'])) {
            foreach ($params['where'] as $key => $val) {
                foreach ($val as $k => $v) {
                    if (!empty($v)) {
                        $query->where($k, $v);
                    }
                }
            }
        }
        if (isset($params['orWhere'])) {
            $query->orWhere(function ($q) use ($params) {
                foreach ($params['orWhere'] as $key => $val) {
                    foreach ($val as $k => $v) {
                        $q->where($k, $v);
                    }
                }
                if (isset($params['orWhereIn'])) {
                    $q->orWhere(function ($z) use ($params) {
                        foreach ($params['orWhereIn'] as $key => $val) {
                            foreach ($val as $k => $v) {
                                $z->whereIn($k, $v);
                            }
                        }
                    });
                }
            });
        }
        if (isset($params['whereNotNull'])) {
            foreach ($params['whereNotNull'] as $key => $val) {
                $query->whereNotNull($val);
            }
        }
        if (isset($params['whereIn'])) {
            foreach ($params['whereIn'] as $key => $val) {
                $query->whereIn($key, $val);
            }
        }
        if (isset($params['whereBetween'])) {
            foreach ($params['whereBetween'] as $key => $val) {
                $query->whereBetween($key, $val);
            }
        }
        if (isset($params['searchColumn'])) {
            $query->where(function ($query) use ($params) {
                $search = Request::get('search')['value'];
                foreach ($params['searchColumn'] as $key => $val) {
                    if ($key == '0') {
                        $query->where($val, 'LIKE', "%" . $search . "%");
                    } else {
                        $query->orWhere($val, 'like', "%" . $search . "%");
                    }
                }
            });
        }
        if (isset(Request::get('order')[0]['column']) && isset(Request::get('order')[0]['dir'])) {
            $order = Request::get('columns')[Request::get('order')[0]['column']]['data'];
            $typeOrder = Request::get('order')[0]['dir'];
            if (in_array($order, ['no', 'action'])) {
                $order = 'id';
            }
            $query->orderBy($order, $typeOrder);
        }
        $limit = (Request::get('length')) ? Request::get('length') : 10;
        $offset = (Request::get('start')) ? Request::get('start') : 0;
        $resultAll = $query->count();
        $result = $query->skip($offset)->take($limit)->get();

        if (count($result) >= 1) {
            $no = ($offset == 0) ? 1 : $offset + 1;
            foreach ($result as $key => $val) {
                $val = (array) $val;
                $dataValue = $val;
                $dataValue['no'] = ($no++);
                if (isset($params['action'])) {
                    $action = '<div class="btn-group">';
                    foreach ($params['action'] as $k => $v) {
                        if ($v['action'] == 'detail') {
                            $action .= '<a href="' . url($v['url'] . '/' . $val[$v['key']]) . '" title="Detail data" data-toggle="tooltip" class="btn-sm btn-success btn-flat detail" dtId="' . $val[$v['key']] . '"><i class="fa fa-asterisk"></i></a>';
                        } else if ($v['action'] == 'edit') {
                            $action .= '<a href="' . url($v['url'] . '/' . $val[$v['key']]) . '" title="Edit data" data-toggle="tooltip" class="btn-sm btn-warning btn-flat"><i class="fa fa-pencil edit"></i></a>';
                        } else if ($v['action'] == 'editMaster') {
                            $action .= '<a action="' . url($v['url'] . '/' . $val[$v['key']]) . '" dtid="' . $val[$v['key']] . '" title="Edit data" data-toggle="tooltip" class="btn-sm btn-warning btn-flat edit"><i class="fa fa-pencil"></i></a>';
                        } else if ($v['action'] == 'delete') {
                            $action .= '<a action="' . url($v['url'] . '/' . $val[$v['key']]) . '" title="Hapus data" class="btn-sm btn-danger btn-flat delete" data-toggle="modal" data-target=".modaldelete" ><i class="fa fa-remove"></i></a>';
                        }
                    }
                    $action .= '</div>';
                    $dataValue['action'] = $action;
                }
                $data['data'][] = $dataValue;
            }
        } else {
            $data['data'] = array();
        }

        $data['iTotalRecords'] = $resultAll;
        $data['iTotalDisplayRecords'] = ($query->value('id') !== null) ? $resultAll : '0';
        return $data;
    }
}

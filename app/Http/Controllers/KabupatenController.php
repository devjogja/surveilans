<?php

namespace App\Http\Controllers;

use Request, DB;
use App\Models\Tx;
use App\Libraries\Datatable;

class KabupatenController extends Controller
{
	public function index()
	{
		$compact = array(
			'title'         => 'Surveilans PD3I',
			'contentTitle'  => 'Kabupaten',
			'data'          => array()
			);
		return view('district.kabupaten',$compact);
	}

	public function getData()
	{
		$config['table'] = 'mst_kabupaten AS a';
		$config['coloumn'] = ['code','code_provinsi','code','name','longitude','latitude','konfirm_code'];
		$config['columnSelect'] = ['a.code','a.code_provinsi','a.code','a.name','a.longitude','a.latitude','a.konfirm_code'];
		$config['columnFormat'] = ['number','','','','','',''];
		$config['searchColumn'] = ['a.code_provinsi','a.code','a.name'];
		$config['where'][] = ['a.deleted_at'=>null];
		$config['key'] = 'a.code';
		$config['action'][]		= array(
			'action'	=> 'editMaster',
			'url' 		=> url('district/kabupaten/edit'),
			'key'		=> 'code',
			);
		$config['action'][]		= array(
			'action'	=> 'delete',
			'url'		=> url('district/kabupaten/delete'),
			'key'		=> 'code',
			);
		$data = Datatable::getData($config);
		echo json_encode($data);
	}

	public function postData()
	{
		$id = Request::get('id');
		$data 	= Request::get('dt');
		if(empty($id)){
			$result = DB::table('mst_kabupaten')->insert($data);
		}else{
			if (Tx::getData('mst_kabupaten',['code'=>$data['code']])->first()) {
				unset($data['code']);
			}
			$result = DB::table('mst_kabupaten')->where(['code'=>$id])->update($data);
		}
		if($result){
			$response 	= array(
				'success'	=> true,
				'messageType'	=> 'info',
				'title'		=> 'Berhasil!',
				'message'	=> 'Data Kabupaten berhasil disimpan',
			);
		}else{
			$response 	= array(
				'success'	=> false,
				'messageType'	=> 'danger',
				'title'		=> 'Gagal!',
				'message'	=> 'Data Kabupaten gagal disimpan',

			);
		}
		echo(json_encode($response));
	}

	public function getDetail($id='')
	{
		$response = Tx::getData('mst_kabupaten',['code'=>$id])->first();
		echo(json_encode($response));
	}

	public function postDelete($id)
	{
		if(Tx::postDelete('mst_kabupaten',['code'=>$id]))
		{
			$response 	= array(
				'success'	=> 1,
				'messageType'	=> 'info',
				'title'		=> 'Berhasil!',
				'message'	=> 'Data Kabupaten berhasil dihapus',
			);
		}else{
			$response 	= array(
				'success'	=> 0,
				'messageType'	=> 'danger',
				'title'		=> 'Gagal!',
				'message'	=> 'Data Kabupaten gagal dihapus',
			);
		}
		echo(json_encode($response));
	}
}
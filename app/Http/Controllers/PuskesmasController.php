<?php

namespace App\Http\Controllers;

use App\Models\Puskesmas;
use App\Models\Tx;
use App\Libraries\Datatable;
use Helper;
use DB;
use Excel;
use Request;
use Response;

class PuskesmasController extends Controller
{
    public function index()
    {
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Master Puskesmas',
            'data' => array(),
        );
        return view('faskes.puskesmas', $compact);
    }

    public function viewWilayahKerja()
    {
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Master Wilayah Kerja Puskesmas',
            'data' => array(),
        );
        return view('faskes.wilayah_kerja_puskesmas', $compact);
    }

    public function getData()
    {
        $config['table'] = 'view_puskesmas AS a';
        $config['coloumn'] = ['id', 'code_faskes', 'name', 'alamat','name_kabupaten','name_provinsi', 'konfirm_code'];
        $config['columnSelect'] = ['a.id', 'a.code_faskes', 'a.name', 'a.alamat','a.name_kabupaten','a.name_provinsi', 'a.konfirm_code'];
        $config['columnFormat'] = ['number', '', '', '', ''];
        $config['searchColumn'] = ['a.code_faskes', 'a.name'];
        $config['key'] = 'a.id';
        $config['action'][] = array(
            'action' => 'editMaster',
            'url' => url('faskes/puskesmas/edit'),
            'key' => 'id',
        );
        $config['action'][] = array(
            'action' => 'delete',
            'url' => url('faskes/puskesmas/delete'),
            'key' => 'id',
        );
        $data = Datatable::getData($config);
        echo json_encode($data);
    }

    public function getDataWilayahKerja()
    {
        $request = Request::all();
        $role = Helper::role();
        if ($role->id_role == '1' OR $role->id_role == '2') {
            $q['where'][] = ['a.id_faskes'=>$role->id_faskes];
        }else if($role->id_role == '5'){
            $q['where'][] = ['a.code_provinsi_faskes'=>$role->id_faskes];
        }else if($role->id_role == '6'){
            $q['where'][] = ['a.code_kabupaten_faskes'=>$role->id_faskes];
        }
        $q['table'] = 'view_wilayah_kerja_puskesmas AS a';
        $q['columnSelect'] = ['a.id', 'a.code_faskes', 'a.name', 'a.full_address'];
        $q['searchColumn'] = ['a.code_faskes', 'a.name', 'a.full_address'];
        $q['action'][] = [
            'action' => 'editMaster',
            'url' => url('faskes/wilayah_kerja_puskesmas/post'),
            'key' => 'id',
        ];
        $q['action'][] = array(
            'action' => 'delete',
            'url' => url('faskes/wilayah_kerja_puskesmas/delete'),
            'key' => 'id',
        );
        $data = Datatable::getData($q);
        echo json_encode($data);
    }

    public function importDataWilayahKerja()
    {
        $path = Request::file('import_wilayah');
        if (!empty($path)) {
            Excel::load($path, function ($reader) {
                $row = array();
                foreach ($reader->toArray() as $key => $val) {
                    $check = Tx::getData('mst_wilayah_kerja_puskesmas', ['code_kelurahan' => $val['code_kelurahan']])->first();
                    if (empty($check)) {
                        Tx::insertData('mst_wilayah_kerja_puskesmas', $val);
                    } else {
                        Tx::updateData('mst_wilayah_kerja_puskesmas', ['code_kelurahan' => $val['code_kelurahan']], $val);
                    }
                };
            })->get();
        }
        echo true;
    }

    public function exportExampleWilayahKerjaPuskesmas()
    {
        $file = public_path() . "/download/contoh_format_wilayah_kerja_pkm.xls";
        return Response::download($file, 'Contoh format import wilayah kerja puskesmas.xls');
    }

    public function postData()
    {
        $id = Request::get('id');
        $data = Request::get('dt');
        if (empty($id)) {
            $result = Tx::insertData('mst_puskesmas', $data);
        } else {
            $result = Tx::updateData('mst_puskesmas', ['id' => $id], $data);
        }
        if ($result) {
            $response = array(
                'success' => true,
                'messageType' => 'info',
                'title' => 'Berhasil!',
                'message' => 'Data Puskesmas berhasil disimpan',
            );
        } else {
            $response = array(
                'success' => false,
                'messageType' => 'danger',
                'title' => 'Gagal!',
                'message' => 'Data Puskesmas gagal disimpan',

            );
        }
        echo (json_encode($response));
    }

    public function postDataWilayahKerja()
    {
        $id = Request::get('id');
        $data = Request::get('dt');
        $data['code_faskes'] = Tx::getData('mst_puskesmas', ['id' => $data['code_faskes']])->first()->code_faskes;
        if (empty($id)) {
            $result = Tx::insertData('mst_wilayah_kerja_puskesmas', $data);
        } else {
            $result = Tx::updateData('mst_wilayah_kerja_puskesmas', ['id' => $id], $data);
        }
        if ($result) {
            $response = array(
                'success' => true,
                'messageType' => 'info',
                'title' => 'Berhasil!',
                'message' => 'Data Wilayah kerja puskesmas berhasil disimpan',
            );
        } else {
            $response = array(
                'success' => false,
                'messageType' => 'danger',
                'title' => 'Gagal!',
                'message' => 'Data Wilayah kerja puskesmas gagal disimpan',

            );
        }
        echo (json_encode($response));
    }

    public function postDelete($id)
    {
        if (Tx::deleteData('mst_puskesmas', ['id' => $id])) {
            $response = array(
                'success' => 1,
                'messageType' => 'info',
                'title' => 'Berhasil!',
                'message' => 'Data Puskesmas berhasil dihapus',
            );
        } else {
            $response = array(
                'success' => 0,
                'messageType' => 'danger',
                'title' => 'Gagal!',
                'message' => 'Data Puskesmas gagal dihapus',
            );
        }
        echo (json_encode($response));
    }

    public function postDeleteWilayahKerja($id)
    {
        if (Tx::deleteData('mst_wilayah_kerja_puskesmas', ['id' => $id])) {
            $response = array(
                'success' => 1,
                'messageType' => 'info',
                'title' => 'Berhasil!',
                'message' => 'Data Wilayah Kerja puskesmas berhasil dihapus',
            );
        } else {
            $response = array(
                'success' => 0,
                'messageType' => 'danger',
                'title' => 'Gagal!',
                'message' => 'Data Wilayah Kerja puskesmas gagal dihapus',
            );
        }
        echo (json_encode($response));
    }

    public function getPuskesmas()
    {
        $where['a.code_provinsi'] = Request::get('code_provinsi');
        $where['a.code_kabupaten'] = Request::get('code_kabupaten');
        $where['a.code_kecamatan'] = Request::get('code_kecamatan');
        $query = Puskesmas::getData($where);
        echo json_encode($query);
    }

    public function getDetail($id = '')
    {
        $response = Tx::getData('view_puskesmas', ['id' => $id])->first();
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getDetailWilayahKerja($id = '')
    {
        $response = Tx::getData('view_wilayah_kerja_puskesmas', ['id' => $id])->first();
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }
    public function wilayahKerjaPuskesmas()
    {
        $codePuskesmas = Request::get('id_puskesmas');
        $codePuskesmas = DB::table('mst_puskesmas')->where(['id' => $codePuskesmas])->first();
        $data = [];
        if ($codePuskesmas) {
            $q = DB::table('mst_wilayah_kerja_puskesmas AS a');
            $q->select(['a.code_kelurahan', 'b.name']);
            $q->join('mst_kelurahan AS b', 'a.code_kelurahan', '=', 'b.code');
            $q->where(['a.code_faskes' => $codePuskesmas->code_faskes]);
            $data = $q->get();
        }
        $response = array();
        foreach ($data as $key => $val) {
            $response[$val->code_kelurahan] = $val->name;
        }
        echo json_encode($response);
    }
}

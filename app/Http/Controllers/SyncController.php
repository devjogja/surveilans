<?php

namespace App\Http\Controllers;

use App\Models\SyncModel;
use DB;

class SyncController extends Controller
{
    public function sync_all()
    {
        /*DB::update("UPDATE 
                        db_pro_surveilans_live.campak a 
                        JOIN db_pro_surveilans_live.puskesmas b ON a.id_tempat_periksa = b.puskesmas_id 
                        SET 
                        a.kode_faskes = 'puskesmas' 
                        WHERE 
                        b.kode_prop = '34' AND a.kode_faskes = 'rs'");
        DB::update("UPDATE 
                        db_pro_surveilans_live.campak a 
                        JOIN db_pro_surveilans_live.rumahsakit2 b ON a.id_tempat_periksa = b.id 
                        SET 
                        a.kode_faskes = 'rs' 
                        WHERE 
                        b.kode_prop = '34' AND a.kode_faskes = 'puskesmas';");
        DB::update("UPDATE 
                        db_pro_surveilans_live.afp a 
                        JOIN db_pro_surveilans_live.puskesmas b ON a.id_tempat_periksa = b.puskesmas_id 
                        SET 
                        a.kode_faskes = 'puskesmas' 
                        WHERE 
                        b.kode_prop = '34' AND a.kode_faskes = 'rs'");
        DB::update("UPDATE 
                        db_pro_surveilans_live.afp a 
                        JOIN db_pro_surveilans_live.rumahsakit2 b ON a.id_tempat_periksa = b.id 
                        SET 
                        a.kode_faskes = 'rs' 
                        WHERE 
                        b.kode_prop = '34' AND a.kode_faskes = 'puskesmas';");*/
       /* DB::update("UPDATE 
                        db_pro_surveilans_live.crs a 
                        JOIN db_pro_surveilans_live.puskesmas b ON a.id_tempat_periksa = b.puskesmas_id 
                        SET 
                        a.kode_faskes = 'puskesmas' 
                        WHERE 
                        b.kode_prop = '34' AND a.kode_faskes = 'rs'");
        DB::update("UPDATE 
                        db_pro_surveilans_live.crs a 
                        JOIN db_pro_surveilans_live.rumahsakit2 b ON a.id_tempat_periksa = b.id 
                        SET 
                        a.kode_faskes = 'rs' 
                        WHERE 
                        b.kode_prop = '34' AND a.kode_faskes = 'puskesmas';");*/
        /*DB::update("UPDATE 
                        db_pro_surveilans_live.difteri a 
                        JOIN db_pro_surveilans_live.puskesmas b ON a.id_tempat_periksa = b.puskesmas_id 
                        SET 
                        a.kode_faskes = 'puskesmas' 
                        WHERE 
                        b.kode_prop = '34' AND a.kode_faskes = 'rs'");
        DB::update("UPDATE 
                        db_pro_surveilans_live.difteri a 
                        JOIN db_pro_surveilans_live.rumahsakit2 b ON a.id_tempat_periksa = b.id 
                        SET 
                        a.kode_faskes = 'rs' 
                        WHERE 
                        b.kode_prop = '34' AND a.kode_faskes = 'puskesmas';");
        DB::update("UPDATE 
                        db_pro_surveilans_live.tetanus a 
                        JOIN db_pro_surveilans_live.puskesmas b ON a.id_tempat_periksa = b.puskesmas_id 
                        SET 
                        a.kode_faskes = 'puskesmas' 
                        WHERE 
                        b.kode_prop = '34' AND a.kode_faskes = 'rs'");
        DB::update("UPDATE 
                        db_pro_surveilans_live.tetanus a 
                        JOIN db_pro_surveilans_live.rumahsakit2 b ON a.id_tempat_periksa = b.id 
                        SET 
                        a.kode_faskes = 'rs' 
                        WHERE 
                        b.kode_prop = '34' AND a.kode_faskes = 'puskesmas';");*/

        /*$this->sync_master_rs();
        $this->sync_master_puskesmas();
        $this->sync_wilayah_kerja_puskesmas();*/
        /*$this->sync_user();
        $this->sync_role_user('puskesmas');*/
        // $this->sync_role_user('rs');
       /* $this->sync_role_user('kabupaten');
        $this->sync_role_user('provinsi');
        $this->sync_role_user('pusat');*/
        // $this->sync_campak();
        /* $this->sync_epid_campak();
        $this->sync_afp();
        $this->sync_epid_afp();
        $this->sync_difteri();
        $this->sync_epid_difteri();
        $this->sync_tetanus(); */
        // $this->sync_crs();
        $this->sync_epid_crs();
    }

    public function sync_master_rs()
    {
        DB::update("UPDATE
            db_pro_surveilans_live.rumahsakit2 a, pd3i_live.mst_rumahsakit b
            SET a.id_new = b.id
            WHERE
            a.kode_faskes = b.code_faskes;");
        $d = DB::table("db_pro_surveilans_live.rumahsakit2 AS a")->whereNull('a.id_new');
        foreach ($d->get() as $key => $val) {
            $dt = [
                'code_faskes' => $val->kode_faskes,
                'code_faskes_bpjs' => $val->kode_faskes_bpjs,
                'name' => $val->nama_faskes,
                'type' => $val->tipe_faskes,
                'kepemilikan' => $val->kepemilikan,
                'alamat' => $val->alamat,
                'kontrak_bpjs' => $val->kontrak_bpjs,
                'regional_bpjs' => $val->regiona_bpjs,
                'nama_kc' => $val->nama_kc,
                'regional_lokal' => $val->regional_lokal,
                'longitude' => $val->longitude,
                'latitude' => $val->latitude,
                'telp' => $val->telp,
                'sms' => $val->sms,
                'wilayah_koordinasi' => $val->wilayah_koordinasi,
                'konfirm_code' => $val->konfirmasi,
                'code_kelurahan' => $val->kode_desa,
                'code_kecamatan' => $val->kode_kec,
                'code_kabupaten' => $val->kode_kab,
                'code_provinsi' => $val->kode_prop,
            ];
            $id = DB::table('mst_rumahsakit')->insertGetId($dt);
            DB::table('db_pro_surveilans_live.rumahsakit2')->where(['id' => $val->id])->update(['id_new' => $id]);
        }
        // return 'success';
    }

    public function sync_master_puskesmas()
    {
        DB::update("UPDATE
            db_pro_surveilans_live.puskesmas a, pd3i_live.mst_puskesmas b
            SET a.id_new = b.id
            WHERE
            a.puskesmas_code_faskes = b.code_faskes");
        $d = DB::table("db_pro_surveilans_live.puskesmas AS a")->whereNull('a.id_new');
        foreach ($d->get() as $key => $val) {
            $dt = [
                'name' => $val->puskesmas_name,
                'code_faskes' => $val->puskesmas_code_faskes,
                'alamat' => $val->alamat,
                'location' => $val->lokasi_puskesmas,
                'longitude' => $val->longitude,
                'latitude' => $val->latitude,
                'konfirm_code' => $val->konfirmasi,
                'code_kelurahan' => $val->kode_desa,
                'code_kecamatan' => $val->kode_kec,
                'code_kabupaten' => $val->kode_kab,
                'code_provinsi' => $val->kode_prop,
            ];
            $id = DB::table('mst_puskesmas')->insertGetId($dt);
            DB::table('db_pro_surveilans_live.puskesmas')->where(['puskesmas_id' => $val->puskesmas_id])->update(['id_new' => $id]);
        }
        // return 'success';
    }

    public function sync_wilayah_kerja_puskesmas()
    {
        $d = DB::select('SELECT
            a.id_kelurahan, a.puskesmas_code_faskes
            FROM
            db_pro_surveilans_live.wilayah_kerja_puskesmas a
            JOIN db_pro_surveilans_live.puskesmas b ON a.puskesmas_code_faskes = b.puskesmas_code_faskes
            WHERE
            a.id_kelurahan IS NOT NULL');
        $dt = array();
        foreach ($d as $key => $val) {
            $dt = array(
                'code_kelurahan' => $val->id_kelurahan,
                'code_faskes' => $val->puskesmas_code_faskes,
                'created_by' => '00',
            );
            $c = DB::table('mst_wilayah_kerja_puskesmas')->where(['code_kelurahan' => $val->id_kelurahan, 'code_faskes' => $val->puskesmas_code_faskes])->first();
            if (!empty($c)) {
                DB::table('mst_wilayah_kerja_puskesmas')->where(['code_kelurahan' => $val->id_kelurahan, 'code_faskes' => $val->puskesmas_code_faskes])->update($dt);
            } else {
                DB::table('mst_wilayah_kerja_puskesmas')->insert($dt);
            }
        }
        // return 'success';
    }

    public function sync_user()
    {
        $duser = SyncModel::gUser();
        $dd = [];
        $i = 0;
        foreach ($duser as $key => $val) {
            $i++;
            $c = DB::table('users')->where('email', $val->email)->first();
            $dd[$i]['id'] = !empty($c->id) ? $c->id : null;
            $dd[$i]['user'] = array
                (
                'id_user_lama' => $val->id,
                'first_name' => $val->first_name,
                'last_name' => $val->last_name,
                'email' => $val->email,
            );
            if (empty($c->id)) {
                $dd[$i]['user']['password'] = '12345';
                $dd[$i]['user']['password_repeat'] = '12345';
            }
            $dd[$i]['user_meta'] = array
                (
                'instansi' => $val->instansi,
                'jabatan' => $val->jabatan,
                'no_telp' => $val->no_telp,
                'jenis_kelamin' => $val->jk,
            );
            $response = app('App\Http\Controllers\AuthenticateController')->postRegister($dd[$i]);
            $dt[$i] = json_decode($response->getContent());
        }
    }

    public function sync_role_user($faskes)
    {
        $droleuser = SyncModel::gRoleUser($faskes);
        if ($faskes == 'puskesmas') {
            $role_id = 1;
        } elseif ($faskes == 'rs') {
            $role_id = 2;
        } elseif ($faskes == 'pusat') {
            $role_id = 3;
        } elseif ($faskes == 'provinsi') {
            $role_id = 5;
        } elseif ($faskes == 'kabupaten') {
            $role_id = 6;
        } elseif ($faskes == 'lab') {
            $role_id = 4;
        }
        $dd = [];
        $i = 0;
        foreach ($droleuser as $key => $val) {
            $i++;
            if (in_array($role_id, [1, 2])) {
                $dd[$key]['dt']['code_provinsi'] = $val->kode_prop;
                $dd[$key]['dt']['code_kabupaten'] = $val->kode_kab;
                $dd[$key]['dt']['code_kecamatan'] = $val->kode_kec;
                $dd[$key]['dt']['code_faskes'] = $val->code_faskes;
                $dd[$key]['dt']['alamat'] = $val->alamat;
            }
            $dd[$key]['role_user'] = array
                (
                'faskes_id' => $val->faskes_id,
                'penanggung_jawab_1' => $val->penanggungjawab_1,
                'penanggung_jawab_2' => $val->penanggungjawab_2,
                'role_id' => $role_id,
            );
            $dd[$key]['created_by'] = $val->id_user;
            $c = DB::table('role_users')->where(['user_id'=>$val->id_user, 'role_id'=>$role_id, 'faskes_id'=>$val->faskes_id])->first();
            if (empty($c) && !empty($val->id_user)) {
                $dt[$i] = app('App\Http\Controllers\RoleUserController')->postRoleProfile($dd[$key]);
            }
        }
    }

    public function sync_campak() 
    {
        $a = SyncModel::gCampak();
        $dt = [];
        foreach ($a as $key => $val) {
            $cek = DB::table('trx_case')->select('id', 'id_pasien')->where('id_campak_old', $val->id_campak_old)->first();
            $dt[$key]['dc'] = [
                'id_campak_old' => $val->id_campak_old,
                'id_faskes' => $val->id_faskes,
                'id_role' => $val->id_role,
                'jenis_input' => 1,
                'no_rm' => $val->no_rm,
                '_no_epid' => $val->no_epid,
                '_no_epid_klb' => $val->no_epid_klb,
                'no_epid_lama' => $val->no_epid_lama,
                'klasifikasi_final' => $val->klasifikasi_final,
                'created_at' => $val->created_at,
                'updated_at' => $val->updated_at,
            ];
            $dt[$key]['code_wilayah_faskes'] = $val->code_wilayah_faskes;
            $dt[$key]['id_trx_case'] = !empty($cek->id) ? $cek->id : '';
            $dt[$key]['dp'] = [
                'name' => $val->name_pasien,
                'nik' => $val->nik,
                'agama' => '',
                'jenis_kelamin' => $val->jenis_kelamin,
                'tgl_lahir' => $val->tgl_lahir,
                'umur_thn' => $val->umur_thn,
                'umur_bln' => $val->umur_bln,
                'umur_hari' => $val->umur_hari,
                'alamat' => $val->alamat,
                'code_kelurahan' => $val->id_kelurahan,
                'code_kecamatan' => '',
                'code_kabupaten' => '',
                'code_provinsi' => '',
            ];
            $dt[$key]['id_pasien'] = !empty($cek->id_pasien) ? $cek->id_pasien : '';
            $dt[$key]['df'] = [
                'name' => $val->nama_ortu,
            ];
            $dt[$key]['dk'] = [
                'tgl_imunisasi_campak' => $val->tgl_imunisasi_campak,
                'jml_imunisasi_campak' => $val->jml_imunisasi_campak,
                'tgl_mulai_demam' => $val->tgl_mulai_demam,
                'tgl_mulai_rash' => $val->tgl_mulai_rash,
                'tgl_laporan_diterima' => $val->tgl_laporan_diterima,
                'tgl_pelacakan' => $val->tgl_pelacakan,
                'vitamin_A' => $val->vitamin_A,
                'keadaan_akhir' => $val->keadaan_akhir,
                'jenis_kasus' => $val->jenis_kasus,
                'klb_ke' => $val->klb_ke,
                'status_kasus' => $val->status_kasus,
            ];
            if (!empty($val->gejala)) {
                foreach (explode('_', $val->gejala) as $k => $v) {
                    $dg = explode('|', $v);
                    $dt[$key]['dg']['gejala'][$k] = !empty($dg[0]) ? $dg[0] : '';
                    $dt[$key]['dg']['tgl_gejala'][$k] = !empty($dg[1]) ? $dg[1] : '';
                }
            }
            $dt[$key]['dkom']['komplikasi'] = explode('|', $val->komplikasi);
            $dt[$key]['ds']['spesimen'] = explode('_', $val->spesimen);
            $dt[$key]['ds']['hasil_lab'][] = $val->hasil_lab;
        }
        $response = app('App\Http\Controllers\CampakController')->postCase($dt);
        $dt = json_decode($response->getContent());
        echo "<pre>";
        print_r($dt);
        echo "</pre>";
    }

    public function sync_epid_campak()
    {
        $a = SyncModel::gEpidCampak();
        foreach ($a as $key => $val) {
            $cek = DB::table('trx_case AS a')->leftJoin('trx_pe AS b', 'a.id', '=', 'b.id_trx_case')->select('a.id AS id_trx_case', 'b.id AS id_trx_pe')->where('a.id_campak_old', $val->id_campak_old)->first();
            $dt[$key]['id_trx_case'] = $cek->id_trx_case;
            $dt[$key]['id_trx_pe'] = !empty($cek->id_trx_pe) ? $cek->id_trx_pe : '';
            $dt[$key]["dpe"] = [
                "id_faskes" => $val->faskes_id,
                "id_role" => $val->role_id,
                'jenis_input' => 1,
                "total_kasus_tambahan" => $val->total_kasus_tambahan,
                "tgl_penyelidikan" => $val->tgl_penyelidikan,
                "created_at" => $val->created_at,
                "updated_at" => $val->updated_at,
            ];
            $dt[$key]["drp"] = [
                "tgl_pengobatan_pertama_kali" => $val->tgl_pengobatan_pertama_kali,
                "tempat_pengobatan_pertama_kali" => $val->tempat_pengobatan_pertama_kali,
                "obat_yg_diberikan" => $val->obat_yg_diberikan,
            ];
            if (!empty($val->alamat_tinggal_sementara)) {
                $dt[$key]['dk']['lokasi'] = ['k1'];
                $dt[$key]['dk']['keterangan'] = [$val->alamat_tinggal_sementara];
                $dt[$key]['dk']['tgl_pe'] = [$val->tgl_pe_dialamat_sementara];
                $dt[$key]['dk']['jml_kasus'] = [$val->jumlah_kasus_dialamat_sementara];
            }
            if (!empty($val->alamat_tempat_tinggal)) {
                $dt[$key]['dk']['lokasi'] = ['k2'];
                $dt[$key]['dk']['keterangan'] = [$val->alamat_tempat_tinggal];
                $dt[$key]['dk']['tgl_pe'] = [$val->tgl_pe_alamat_tempat_tinggal];
                $dt[$key]['dk']['jml_kasus'] = [$val->jumlah_kasus_dialamat_tempat_tinggal];
            }
            if (!empty($val->sekolah)) {
                $dt[$key]['dk']['lokasi'] = ['k3'];
                $dt[$key]['dk']['keterangan'] = [$val->sekolah];
                $dt[$key]['dk']['tgl_pe'] = [$val->tgl_pe_sekolah];
                $dt[$key]['dk']['jml_kasus'] = [$val->jumlah_kasus_disekolah];
            }
            if (!empty($val->tempat_kerja)) {
                $dt[$key]['dk']['lokasi'] = ['k4'];
                $dt[$key]['dk']['keterangan'] = [$val->tempat_kerja];
                $dt[$key]['dk']['tgl_pe'] = [$val->tgl_pe_tempat_kerja];
                $dt[$key]['dk']['jml_kasus'] = [$val->jumlah_kasus_tempat_kerja];
            }
            if (!empty($val->lain_lain)) {
                $dt[$key]['dk']['lokasi'] = [$val->lain_lain];
                $dt[$key]['dk']['keterangan'] = [$val->lain_lain];
                $dt[$key]['dk']['tgl_pe'] = [$val->tgl_pe_lain_lain];
                $dt[$key]['dk']['jml_kasus'] = [$val->jumlah_kasus_lain_lain];
            }
            $dt[$key]["drk"] = [
                "penyakit_sama_dirumah" => $val->penyakit_sama_dirumah,
                "tgl_penyakit_sama_dirumah" => $val->tgl_penyakit_sama_dirumah,
                "penyakit_sama_disekolah" => $val->penyakit_sama_disekolah,
                "tgl_penyakit_sama_disekolah" => $val->tgl_penyakit_sama_disekolah,
                "keadaan_kurang_gizi" => $val->keadaan_kurang_gizi,
            ];
            $dt[$key]['dp']['pelaksana'] = explode(';', $val->pelaksana);
        }
        $response = app('App\Http\Controllers\CampakController')->postPe($dt);
        $dt = json_decode($response->getContent());
        echo "<pre>";
        print_r($dt);
        echo "</pre>";
    }

    public function sync_afp()
    {
        $a = SyncModel::gAfp();
        $dt = [];
        foreach ($a as $key => $val) {
            $cek = DB::table('trx_case')->select('id', 'id_pasien')->where('id_afp_old', $val->id_afp_old)->first();
            $dt[$key]['dc'] = [
                'id_afp_old' => $val->id_afp_old,
                'id_faskes' => $val->id_faskes,
                'id_role' => $val->id_role,
                'jenis_input' => '1',
                'no_rm' => $val->no_rm,
                '_no_epid' => $val->no_epid,
                'no_epid_lama' => $val->no_epid_lama,
                'klasifikasi_final' => $val->klasifikasi_final,
                'created_at' => $val->created_at,
                'updated_at' => $val->updated_at,
            ];
            $dt[$key]['code_wilayah_faskes'] = $val->code_wilayah_faskes;
            $dt[$key]['id_trx_case'] = !empty($cek->id) ? $cek->id : '';
            $dt[$key]['dp'] = [
                'name' => $val->name_pasien,
                'nik' => $val->nik,
                'agama' => '',
                'jenis_kelamin' => $val->jenis_kelamin,
                'tgl_lahir' => $val->tgl_lahir,
                'umur_thn' => $val->umur_thn,
                'umur_bln' => $val->umur_bln,
                'umur_hari' => $val->umur_hari,
                'alamat' => $val->alamat,
                'code_kelurahan' => $val->id_kelurahan,
                'code_kecamatan' => '',
                'code_kabupaten' => '',
                'code_provinsi' => '',
            ];
            $dt[$key]['id_pasien'] = !empty($cek->id_pasien) ? $cek->id_pasien : '';
            $dt[$key]['df'] = [
                'name' => $val->nama_ortu,
            ];
            $dt[$key]['dk'] = [
                'tgl_mulai_lumpuh' => $val->tgl_mulai_lumpuh,
                'demam_sebelum_lumpuh' => $val->demam_sebelum_lumpuh,
                'imunisasi_rutin_polio_sebelum_sakit' => $val->imunisasi_rutin_polio_sebelum_sakit,
                'informasi_imunisasi_rutin_polio_sebelum_sakit' => $val->informasi_imunisasi_rutin_polio_sebelum_sakit,
                'pin_mopup_ori_biaspolio' => $val->pin_mopup_ori_biaspolio,
                'informasi_pin_mopup_ori_biaspolio' => $val->informasi_pin_mopup_ori_biaspolio,
                'tgl_imunisasi_polio_terakhir' => $val->tgl_imunisasi_polio_terakhir,
                'tgl_laporan_diterima' => $val->tgl_laporan_diterima,
                'tgl_pelacakan' => $val->tgl_pelacakan,
                'kontak' => $val->kontak,
                'keadaan_akhir' => $val->keadaan_akhir,
            ];
            $v31 = null;
            if ($val->kelumpuhan_anggota_gerak_kanan == '2' or $val->kelumpuhan_anggota_gerak_kanan == '3') {
                $v31 = 1;
            } else if ($val->kelumpuhan_anggota_gerak_kanan == '4') {
                $v31 = 2;
            }
            $dt[$key]['dg']['kelumpuhan'][31] = $v31;
            $v32 = null;
            if ($val->kelumpuhan_anggota_gerak_kiri == '2' or $val->kelumpuhan_anggota_gerak_kiri == '3') {
                $v32 = 1;
            } else if ($val->kelumpuhan_anggota_gerak_kiri == '4') {
                $v32 = 2;
            }
            $dt[$key]['dg']['kelumpuhan'][32] = $v32;
            $v33 = null;
            if ($val->kelumpuhan_anggota_gerak_kanan == '1' or $val->kelumpuhan_anggota_gerak_kanan == '3') {
                $v33 = 1;
            } else if ($val->kelumpuhan_anggota_gerak_kanan == '4') {
                $v33 = 2;
            }
            $dt[$key]['dg']['kelumpuhan'][33] = $v33;
            $v34 = null;
            if ($val->kelumpuhan_anggota_gerak_kiri == '1' or $val->kelumpuhan_anggota_gerak_kiri == '3') {
                $v34 = 1;
            } else if ($val->kelumpuhan_anggota_gerak_kiri == '4') {
                $v34 = 2;
            }
            $dt[$key]['dg']['kelumpuhan'][34] = $v34;
            $v39 = null;
            if ($val->gangguan_raba_anggota_gerak_kanan == '2' or $val->gangguan_raba_anggota_gerak_kanan == '3') {
                $v39 = 1;
            } else if ($val->gangguan_raba_anggota_gerak_kanan == '4') {
                $v39 = 2;
            }
            $dt[$key]['dg']['gangguan_raba'][39] = $v39;
            $v40 = null;
            if ($val->gangguan_raba_anggota_gerak_kiri == '2' or $val->gangguan_raba_anggota_gerak_kiri == '3') {
                $v40 = 1;
            } else if ($val->gangguan_raba_anggota_gerak_kiri == '4') {
                $v40 = 2;
            }
            $dt[$key]['dg']['gangguan_raba'][40] = $v40;
            $v41 = null;
            if ($val->gangguan_raba_anggota_gerak_kanan == '1' or $val->gangguan_raba_anggota_gerak_kanan == '3') {
                $v41 = 1;
            } else if ($val->gangguan_raba_anggota_gerak_kanan == '4') {
                $v41 = 2;
            }
            $dt[$key]['dg']['gangguan_raba'][41] = $v41;
            $v42 = null;
            if ($val->gangguan_raba_anggota_gerak_kiri == '1' or $val->gangguan_raba_anggota_gerak_kiri == '3') {
                $v42 = 1;
            } else if ($val->gangguan_raba_anggota_gerak_kiri == '4') {
                $v42 = 2;
            }
            $dt[$key]['dg']['gangguan_raba'][42] = $v42;
            if (!empty($val->jp1)) {
                $jenis_pemeriksaan1 = $ket1 = '';
                if ($val->isolasi_virus_jp1) {
                    $jenis_pemeriksaan1 = '1';
                    $ket1 = $val->ket_isolasi_virus_jp1;
                } elseif ($val->itd_jp1) {
                    $jenis_pemeriksaan1 = '2';
                    $ket1 = $val->ket_itd_jp1;
                } elseif ($val->sequencing_jp1) {
                    $jenis_pemeriksaan1 = '3';
                    $ket1 = $val->ket_sequencing_jp1;
                }
                $dt[$key]['ds']['spesimen'][] = '1|' . $val->tanggal_pengambilan_spesimen1 . '|' . $jenis_pemeriksaan1 . '|' . $ket1;
            }
            if (!empty($val->jp2)) {
                $jenis_pemeriksaan2 = $ket2 = '';
                if ($val->isolasi_virus_jp2) {
                    $jenis_pemeriksaan2 = '2';
                    $ket2 = $val->ket_isolasi_virus_jp2;
                } elseif ($val->itd_jp2) {
                    $jenis_pemeriksaan2 = '2';
                    $ket2 = $val->ket_itd_jp2;
                } elseif ($val->sequencing_jp2) {
                    $jenis_pemeriksaan2 = '3';
                    $ket2 = $val->ket_sequencing_jp2;
                }
                $dt[$key]['ds']['spesimen'][] = '2|' . $val->tanggal_pengambilan_spesimen2 . '|' . $jenis_pemeriksaan2 . '|' . $ket2;
            }
        }
        $response = app('App\Http\Controllers\AfpController')->postCase($dt);
        $dt = json_decode($response->getContent());
        echo "<pre>";
        print_r($dt);
        echo "</pre>";
    }

    public function sync_epid_afp()
    {
        $a = SyncModel::gEpidAfp();
        $dt = [];
        foreach ($a as $key => $val) {
            $cek = DB::table('trx_case AS a')->leftJoin('trx_pe AS b', 'a.id', '=', 'b.id_trx_case')->select('a.id AS id_trx_case', 'a.id_pasien', 'b.id AS id_trx_pe')->where('a.id_afp_old', $val->id_afp_old)->first();
            $dt[$key]['id_trx_case'] = $cek->id_trx_case;
            $dt[$key]['id_pasien'] = $cek->id_pasien;
            $dt[$key]['id_trx_pe'] = !empty($cek->id_trx_pe) ? $cek->id_trx_pe : '';
            $dt[$key]["dpe"] = [
                "id_faskes" => $val->faskes_id,
                "id_role" => $val->role_id,
                'jenis_input' => 1,
                "created_at" => $val->created_at,
                "updated_at" => $val->updated_at,
            ];
            $dt[$key]["dc"] = [
                "no_rm" => $val->no_rm,
                "no_epid_lama" => $val->no_epid_lama,
            ];
            $dt[$key]["dsi"] = [
                "code_provinsi_sumber_informasi" => $val->code_provinsi_sumber_informasi,
                "code_kabupaten_sumber_informasi" => $val->code_kabupaten_sumber_informasi,
                "sumber_laporan" => $val->sumber_laporan,
                "keterangan" => $val->keterangan,
            ];
            $dt[$key]['drs'] = [
                "tgl_sakit" => $val->tgl_sakit,
                "tgl_mulai_lumpuh" => $val->tgl_mulai_lumpuh,
                "tgl_meninggal" => $val->tgl_meninggal,
                "berobat_ke_unit_pelayanan_lain" => $val->berobat_ke_unit_pelayanan_lain,
                "nama_unit_pelayanan" => $val->nama_unit_pelayanan,
                "tgl_berobat" => $val->tgl_berobat,
                "diagnosis" => $val->diagnosis,
                "no_rm_lama" => $val->no_rm_lama,
                "kelumpuhan_sifat_akut" => $val->kelumpuhan_sifat_akut,
                "kelumpuhan_sifat_layuh" => $val->kelumpuhan_sifat_layuh,
                "kelumpuhan_disebabkan_ruda" => $val->kelumpuhan_disebabkan_ruda,
                "imunisasi_rutin_polio_sebelum_sakit" => $val->imunisasi_rutin_polio_sebelum_sakit,
                "informasi_imunisasi_rutin_polio_sebelum_sakit" => $val->informasi_imunisasi_rutin_polio_sebelum_sakit,
                "pin_mopup_ori_biaspolio" => $val->pin_mopup_ori_biaspolio,
                "informasi_pin_mopup_ori_biaspolio" => $val->informasi_pin_mopup_ori_biaspolio,
                "tgl_imunisasi_polio_terakhir" => $val->tgl_imunisasi_polio_terakhir,
            ];
            $dt[$key]['dg']['demam_sebelum_lumpuh'] = $val->demam_sebelum_lumpuh;
            $v31 = null;
            if ($val->lumpuh_tungkai_kanan == 'ya') {
                $v31 = 1;
            } else if ($val->lumpuh_tungkai_kanan == 'tidak') {
                $v31 = 2;
            }
            $dt[$key]['dg']['kelumpuhan'][31] = $v31;
            $v32 = null;
            if ($val->lumpuh_tungkai_kiri == 'ya') {
                $v32 = 1;
            } else if ($val->lumpuh_tungkai_kiri == 'tidak') {
                $v32 = 2;
            }
            $dt[$key]['dg']['kelumpuhan'][32] = $v32;
            $v33 = null;
            if ($val->lumpuh_lengan_kanan == 'ya') {
                $v33 = 1;
            } else if ($val->lumpuh_lengan_kanan == 'tidak') {
                $v33 = 2;
            }
            $dt[$key]['dg']['kelumpuhan'][33] = $v33;
            $v34 = null;
            if ($val->lumpuh_lengan_kiri == 'ya') {
                $v34 = 1;
            } else if ($val->lumpuh_lengan_kiri == 'tidak') {
                $v34 = 2;
            }
            $dt[$key]['dg']['kelumpuhan'][34] = $v34;
            $v39 = null;
            if ($val->raba_tungkai_kanan == 'ya') {
                $v39 = 1;
            } else if ($val->raba_tungkai_kanan == 'tidak') {
                $v39 = 2;
            }
            $dt[$key]['dg']['gangguan_raba'][39] = $v39;
            $v40 = null;
            if ($val->raba_tungkai_kiri == 'ya') {
                $v40 = 1;
            } else if ($val->raba_tungkai_kiri == 'tidak') {
                $v40 = 2;
            }
            $dt[$key]['dg']['gangguan_raba'][40] = $v40;
            $v41 = null;
            if ($val->raba_lengan_kanan == 'ya') {
                $v41 = 1;
            } else if ($val->raba_lengan_kanan == 'tidak') {
                $v41 = 2;
            }
            $dt[$key]['dg']['gangguan_raba'][41] = $v41;
            $v42 = null;
            if ($val->raba_lengan_kiri == 'ya') {
                $v42 = 1;
            } else if ($val->raba_lengan_kiri == 'tidak') {
                $v42 = 2;
            }
            $dt[$key]['dg']['gangguan_raba'][42] = $v42;

            $dt[$key]['drk'] = [
                "bepergian_sebelum_sakit" => $val->bepergian_sebelum_sakit,
                "lokasi_pergi_sebelum_sakit" => $val->lokasi_pergi_sebelum_sakit,
                "tgl_pergi_sebelum_sakit" => $val->tgl_pergi_sebelum_sakit,
                "berkunjung_ke_rumah_anak_imunisasi_polio" => $val->berkunjung_ke_rumah_anak_imunisasi_polio,
            ];
            $ps = null;
            if (!empty($val->alasan_spesimen_tidak_diambil)) {
                $ps = '2';
            }
            $s = [];
            if (!empty($val->tanggal_ambil_spesimen_I)) {
                $s[] = '1|' . $val->tanggal_ambil_spesimen_I . '|' . $val->tanggal_kirim_spesimen_I . '|' . $val->tanggal_kirim_spesimen_I_propinsi . '|';
            }
            if (!empty($val->tanggal_ambil_spesimen_II)) {
                $s[] = '2|' . $val->tanggal_ambil_spesimen_II . '|' . $val->tanggal_kirim_spesimen_II . '|' . $val->tanggal_kirim_spesimen_II_propinsi . '|';
            }
            $dt[$key]['ds'] = [
                "pengambilan_spesimen" => $ps,
                "alasan_spesimen_tidak_diambil" => $val->alasan_spesimen_tidak_diambil,
                "spesimen" => $s,
            ];
            $pel = explode(',', $val->pelaksana);
            $dt[$key]['dpel']['pelaksana'] = $pel;
            $dt[$key]['dh'] = [
                "diagnosis" => $val->diagnosis,
                "nama_pemeriksa" => $val->nama_pemeriksa,
                "no_telp" => $val->no_telp,
            ];
        }
        $response = app('App\Http\Controllers\AfpController')->postPe($dt);
        $dt = json_decode($response->getContent());
        echo "<pre>";
        print_r($dt);
        echo "</pre>";
    }

    public function sync_difteri()
    {
        $a = SyncModel::gDifteri();
        $dt = [];
        foreach ($a as $key => $val) {
            $cek = DB::table('trx_case')->select('id', 'id_pasien')->where('id_difteri_old', $val->id_difteri_old)->first();
            $dt[$key]['dc'] = [
                'id_difteri_old' => $val->id_difteri_old,
                'id_faskes' => $val->id_faskes,
                'id_role' => $val->id_role,
                'jenis_input' => 1,
                'no_rm' => $val->no_rm,
                '_no_epid' => $val->no_epid,
                'no_epid_lama' => $val->no_epid_lama,
                'klasifikasi_final' => $val->klasifikasi_final,
                'created_at' => $val->created_at,
                'updated_at' => $val->updated_at,
            ];
            $dt[$key]['code_wilayah_faskes'] = $val->code_wilayah_faskes;
            $dt[$key]['id_trx_case'] = !empty($cek->id) ? $cek->id : '';
            $dt[$key]['dp'] = [
                'name' => $val->name_pasien,
                'nik' => $val->nik,
                'agama' => '',
                'jenis_kelamin' => $val->jenis_kelamin,
                'tgl_lahir' => $val->tgl_lahir,
                'umur_thn' => $val->umur_thn,
                'umur_bln' => $val->umur_bln,
                'umur_hari' => $val->umur_hari,
                'alamat' => $val->alamat,
                'code_kelurahan' => $val->id_kelurahan,
                'code_kecamatan' => '',
                'code_kabupaten' => '',
                'code_provinsi' => '',
            ];
            $dt[$key]['id_pasien'] = !empty($cek->id_pasien) ? $cek->id_pasien : '';
            $dt[$key]['df'] = [
                'name' => $val->nama_ortu,
            ];
            $dt[$key]['dk'] = [
                'tgl_mulai_demam' => $val->tgl_mulai_demam,
                'jml_imunisasi_dpt' => $val->jml_imunisasi_dpt,
                'tgl_imunisasi_difteri' => $val->tgl_imunisasi_difteri,
                'tgl_pelacakan' => $val->tgl_pelacakan,
                'gejala_lain' => $val->gejala_lain,
                'jml_kontak' => $val->jml_kontak,
                'diambil_spec_kontak' => $val->diambil_spec_kontak,
                'positif_kontak' => $val->positif_kontak,
                'keadaan_akhir' => $val->keadaan_akhir,
            ];
            $dt[$key]['ds']['spesimen'] = explode('_', $val->spesimen);
            if (!empty($val->tanggal_diambil_spesimen_tenggorokan) && $val->tanggal_diambil_spesimen_tenggorokan != '00-00-0000') {
                $hlab = '';
                if ($val->jenis_pemeriksaan == '1') {
                    $hlab = $val->hasil_lab_kultur_tenggorokan;
                }
                if ($val->jenis_pemeriksaan == '2') {
                    $hlab = $val->hasil_lab_mikroskop_tenggorokan;
                }
                $dt[$key]['ds']['spesimen'][] = '1|' . $val->tanggal_diambil_spesimen_tenggorokan . '|' . $val->jenis_pemeriksaan . '||' . $hlab;
            }
            if (!empty($val->tanggal_diambil_spesimen_hidung) && $val->tanggal_diambil_spesimen_hidung != '00-00-0000') {
                $hlabb = '';
                if ($val->jenis_pemeriksaan == '1') {
                    $hlabb = $val->hasil_lab_kultur_hidung;
                }
                if ($val->jenis_pemeriksaan == '2') {
                    $hlabb = $val->hasil_lab_mikroskop_hidung;
                }
                $dt[$key]['ds']['spesimen'][] = '1|' . $val->tanggal_diambil_spesimen_hidung . '|' . $val->jenis_pemeriksaan . '||' . $hlabb;
            }
        }
        $response = app('App\Http\Controllers\DifteriController')->postCase($dt);
        $dt = json_decode($response->getContent());
        echo "<pre>";
        print_r($dt);
        echo "</pre>";
    }

    public function sync_epid_difteri()
    {
        $a = SyncModel::gEpidDifteri();
        $dt = [];
        foreach ($a as $key => $val) {
            $cek = DB::table('trx_case AS a')->leftJoin('trx_pe AS b', 'a.id', '=', 'b.id_trx_case')->select('a.id AS id_trx_case', 'a.id_pasien', 'b.id AS id_trx_pe')->where('a.id_difteri_old', $val->id_difteri_old)->first();
            $dt[$key]['id_trx_case'] = $cek->id_trx_case;
            $dt[$key]['id_trx_pe'] = !empty($cek->id_trx_pe) ? $cek->id_trx_pe : '';
            $dt[$key]["dpe"] = [
                "id_faskes" => $val->faskes_id,
                "id_role" => $val->role_id,
                'jenis_input' => 1,
                "created_at" => $val->created_at,
                "updated_at" => $val->updated_at,
            ];
            $dt[$key]["dpel"] = [];
            $dt[$key]["dc"] = [
                "no_rm" => $val->no_rm,
                "faskes_tempat_periksa" => $val->faskes_tempat_periksa,
            ];
            $dt[$key]["id_pasien"] = $cek->id_pasien;
            $dt[$key]["dp"] = [
                "tempat_tinggal_saat_ini" => $val->tempat_tinggal,
                "pekerjaan" => $val->pekerjaan,
                "alamat_tempat_kerja" => $val->alamat_tempat_kerja,
                "code_kelurahan_tempat_tinggal_sekarang" => $val->code_kelurahan_tempat_tinggal_sekarang,
                "alamat_saat_ini" => $val->alamat_saat_ini,
                "no_telp" => $val->no_telp,
            ];
            $dt[$key]['df'] = [
                "name" => $val->nama_ortu,
                "no_telp" => $val->no_telp_saudara,
                "nama_saudara_yg_dihubungi" => $val->nama_saudara_yang_bisa_dihubungi,
            ];
            $dt[$key]['drs'] = [
                "tgl_mulai_sakit" => $val->tgl_mulai_sakit,
                "keluhan_untuk_berobat" => $val->keluhan_untuk_berobat,
                "status_imunisasi_difteri" => $val->status_imunisasi_difteri,
            ];
            if (!empty($val->gejala_sakit_demam)) {
                $dt[$key]['dg']['gejala'][] = '26';
                $dt[$key]['dg']['tgl_gejala'][] = $val->tanggal_gejala_sakit_demam;
            }
            if (!empty($val->gejala_sakit_kerongkongan)) {
                $dt[$key]['dg']['gejala'][] = '27';
                $dt[$key]['dg']['tgl_gejala'][] = $val->tanggal_gejala_sakit_kerongkongan;
            }
            if (!empty($val->gejala_leher_bengkak)) {
                $dt[$key]['dg']['gejala'][] = '28';
                $dt[$key]['dg']['tgl_gejala'][] = $val->tanggal_gejala_leher_bengkak;
            }
            if (!empty($val->gejala_sesak_nafas)) {
                $dt[$key]['dg']['gejala'][] = '29';
                $dt[$key]['dg']['tgl_gejala'][] = $val->tanggal_gejala_sesak_nafas;
            }
            if (!empty($val->gejala_pseudomembran)) {
                $dt[$key]['dg']['gejala'][] = '30';
                $dt[$key]['dg']['tgl_gejala'][] = $val->tanggal_gejala_pseudomembran;
            }
            if (!empty($val->gejala_lain)) {
                $dt[$key]['dg']['other'][] = 'other';
                $dt[$key]['dg']['other_val'][] = $val->gejala_lain;
            }
            $dt[$key]['ds']['spesimen'] = [
                $val->jenis_spesimen . "|" . $val->tanggal_ambil_spesimen . "||" . $val->kode_spesimen . "|",
            ];
            if (!empty($val->nama_kontak_kasus)) {
                $dt[$key]['dkk']['kontak_kasus'] = [
                    $val->nama_kontak_kasus . "|" . $val->umur_kontak_kasus . "|" . $val->hub_dgn_kontak_kasus . "|" . $val->status_imunisasi_kontak_kasus . "|||||" . $val->profilaksis,
                ];
            }
            $berobat_ke_rs = $riwayat_pengobatan_rs = null;
            if ($val->tempat_berobat == '1') {
                $berobat_ke_rs = '1';
                if ($val->dirawat == '1') {
                    $riwayat_pengobatan_rs = '1';
                }
            }
            $berobat_ke_puskesmas = $riwayat_pengobatan_puskesmas = null;
            if ($val->tempat_berobat == '2') {
                $berobat_ke_puskesmas = '1';
                if ($val->dirawat == '1') {
                    $riwayat_pengobatan_puskesmas = '1';
                }
            }
            $dokter_praktek_swasta = null;
            if ($val->tempat_berobat == '3') {
                $dokter_praktek_swasta = '1';
            }
            $perawat_mantri_bidan = null;
            if ($val->tempat_berobat == '4') {
                $perawat_mantri_bidan = '1';
            }
            $tidak_berobat = null;
            if ($val->tempat_berobat == '5') {
                $tidak_berobat = '1';
            }
            $dt[$key]['drp'] = [
                "berobat_ke_rs" => $berobat_ke_rs,
                "riwayat_pengobatan_rs" => $riwayat_pengobatan_rs,
                "trakeostomi" => $val->trakeostomi,
                "berobat_ke_puskesmas" => $berobat_ke_puskesmas,
                "riwayat_pengobatan_puskesmas" => $riwayat_pengobatan_puskesmas,
                "dokter_praktek_swasta" => $dokter_praktek_swasta,
                "perawat_mantri_bidan" => $perawat_mantri_bidan,
                "tidak_berobat" => $tidak_berobat,
                "antibiotik" => $val->antibiotik,
                "obat_lain" => $val->obat_lain,
                "ads" => $val->ads,
                "kondisi_kasus_saat_ini" => $val->kondisi_kasus,
            ];
            $dt[$key]['drk'] = [
                "bepergian_dalam_2_minggu" => $val->penderita_berpergian,
                "tujuan_bepergian_dalam_2_minggu" => $val->tempat_berpergian,
                "2_minggu_terakhir_berkunjung_ke_org_meninggal_gejala_sama" => $val->penderita_berkunjung,
                "tujuan_berkunjung_ke_org_meninggal_gejala_sama" => $val->tempat_berkunjung,
                "2_minggu_terakhir_ada_tamu_gejala_sama" => $val->penderita_menerima_tamu,
                "asal_tamu_gejala_sama" => $val->asal_tamu,
            ];
        }
        $response = app('App\Http\Controllers\DifteriController')->postPe($dt);
        $dt = json_decode($response->getContent());
        echo "<pre>";
        print_r($dt);
        echo "</pre>";
    }

    public function sync_tetanus()
    {
        $a = SyncModel::gTetanus();
        $dt = [];
        foreach ($a as $key => $val) {
            $cek = DB::table('trx_case')->select('id', 'id_pasien')->where('id_tetanus_old', $val->id_tetanus_old)->first();
            $dt[$key]['dc'] = [
                'id_tetanus_old' => $val->id_tetanus_old,
                'id_faskes' => $val->id_faskes,
                'id_role' => $val->id_role,
                'jenis_input' => 1,
                'no_rm' => $val->no_rm,
                '_no_epid' => $val->no_epid,
                'no_epid_lama' => $val->no_epid_lama,
                'klasifikasi_final' => $val->klasifikasi_akhir,
                'created_at' => $val->created_at,
                'updated_at' => $val->updated_at,
            ];
            $dt[$key]['code_wilayah_faskes'] = $val->code_wilayah_faskes;
            $dt[$key]['id_trx_case'] = !empty($cek->id) ? $cek->id : '';
            $dt[$key]['dp'] = [
                'name' => $val->name_pasien,
                'nik' => $val->nik,
                'agama' => '',
                'jenis_kelamin' => $val->jenis_kelamin,
                'tgl_lahir' => $val->tgl_lahir,
                'umur_thn' => $val->umur_thn,
                'umur_bln' => $val->umur_bln,
                'umur_hari' => $val->umur_hari,
                'alamat' => $val->alamat,
                'code_kelurahan' => $val->id_kelurahan,
                'code_kecamatan' => '',
                'code_kabupaten' => '',
                'code_provinsi' => '',
            ];
            $dt[$key]['id_pasien'] = !empty($cek->id_pasien) ? $cek->id_pasien : '';
            $dt[$key]['df'] = [
                'name' => $val->nama_ortu,
            ];
            $dt[$key]['dk'] = [
                'tgl_mulai_sakit' => $val->tgl_mulai_sakit,
                'tgl_laporan_diterima' => $val->tgl_laporan_diterima,
                'tgl_pelacakan' => $val->tgl_pelacakan,
                'antenatal_care' => $val->antenatal_care,
                'status_imunisasi_ibu' => $val->status_imunisasi_ibu,
                'penolong_persalinan' => $val->penolong_persalinan,
                'alat_pemotong_tali_pusat' => $val->alat_pemotong_tali_pusat,
                'rawat_rumah_sakit' => $val->rawat_rumah_sakit,
                'keadaan_akhir' => $val->keadaan_akhir,
            ];
        }
        $response = app('App\Http\Controllers\TetanusController')->postCase($dt);
        $dt = json_decode($response->getContent());
        echo "<pre>";
        print_r($dt);
        echo "</pre>";
    }

    public function sync_epid_tetanus()
    {
        $a = SyncModel::gEpidTetanus();
        foreach ($a as $key => $val) {
            $cek = DB::table('trx_case AS a')->leftJoin('trx_pe AS b', 'a.id', '=', 'b.id_trx_case')->select('a.id AS id_trx_case', 'a.id_pasien', 'b.id AS id_trx_pe')->where('a.id_tetanus_old', $val->id_tetanus_old)->first();
            $dt[$key]['id_trx_case'] = $cek->id_trx_case;
            $dt[$key]['id_trx_pe'] = !empty($cek->id_trx_pe) ? $cek->id_trx_pe : '';
            $dt[$key]["dpe"] = [
                "id_faskes" => $val->faskes_id,
                "id_role" => $val->role_id,
                'jenis_input' => 1,
                "created_at" => $val->created_at,
                "updated_at" => $val->updated_at,
            ];
            $dt[$key]["dpel"] = [
                "sumber_laporan" => $val->sumber_laporan,
                "sumber_laporan_lain" => null,
                "tgl_laporan" => $val->tanggal_laporan_diterima,
                "tgl_investigasi" => $val->tanggal_pelacakan,
            ];
            $dt[$key]['id_pasien'] = $cek->id_pasien;
            $dt[$key]["dp"] = [
                "name" => $val->nama_anak,
                "jenis_kelamin" => $val->jenis_kelamin,
                "tgl_lahir" => $val->tanggal_lahir,
                "umur_hari" => $val->umur_hr,
                "anak_keberapa" => $val->anak_ke,
                "code_provinsi" => null,
                "code_kabupaten" => null,
                "code_kecamatan" => null,
                "code_kelurahan" => $val->id_kelurahan,
                "alamat" => $val->alamat,
            ];
            $dt[$key]["df"] = [
                "nama_ayah" => $val->nama_ayah,
                "umur_th_ayah" => $val->umur_ayah,
                "umur_bln_ayah" => null,
                "umur_hari_ayah" => null,
                "pend_terakhir_ayah" => $val->pendidikan_terakhir_ayah,
                "pekerjaan_ayah" => $val->pekerjaan_ayah,
                "nama_ibu" => $val->nama_ibu,
                "pend_terakhir_ibu" => $val->pendidikan_terakhir_ibu,
                "pekerjaan_ibu" => $val->pekerjaan_ibu,
            ];
            $dt[$key]["dirk"] = [
                "nama_yg_diwawancarai" => $val->nama_yang_diwawancarai,
                "hub_dgn_keluarga_bayi" => $val->hubungan_keluarga,
                "bayi_lahir_hidup" => $val->bayi_hidup,
                "tgl_lahir_bayi" => $val->tanggal_lahir,
                "bayi_meninggal" => $val->bayi_meninggal,
                "tgl_meninggal" => $val->tanggal_meninggal,
                "umur_meninggal" => $val->umur_meninggal,
                "lahir_bayi_menangis" => $val->bayi_menangis,
                "tanda_kehidupan_lain" => $val->tanda_kehidupan_lain,
                "bayi_menetek_menghisap_susu_botol_dgn_baik" => $val->bayi_menetek,
                "3_hari_tiba_tiba_mulut_bayi_mencucu_tidak_menetek" => $val->bayi_mencucu,
                "tgl_tidak_mau_menetek" => null,
                "bayi_mudah_kejang_jika_dsentuh" => $val->bayi_kejang,
                "tgl_mulai_kejang" => null,
                "bayi_dirawat" => $val->dirawat,
                "fasilitas_kesehatan" => $val->tempat_berobat,
                "tgl_dirawat" => $val->tanggal_dirawat,
                "keadaan_bayi" => $val->keadaan_setelah_dirawat,
            ];
            $dt[$key]["drk"] = [
                "gpa" => $val->gpa,
                "periksa_dokter" => null,
                "jml_periksa_dokter" => $val->jml_periksa_kehamilan_dengan_dokter,
                "periksa_bidan" => null,
                "jml_periksa_bidan" => $val->jml_periksa_kehamilan_dengan_bidan_perawat,
                "periksa_dukun" => null,
                "jml_periksa_dukun" => $val->jml_periksa_kehamilan_dengan_dukun,
                "tidak_anc" => $val->jml_Tidak_ANC,
                "tidak_jelas" => $val->jml_tidak_jelas,
                "imunisasi_tt_ibu_waktu_hamil" => null,
                "sumber_informasi_imunisasi_tt" => null,
                "imunisasi_ibu_tt_pertama" => null,
                "umur_kehamilan_ibu_tt_pertama" => null,
                "tgl_imunisasi_ibu_tt_pertama" => null,
                "imunisasi_ibu_tt_kedua" => null,
                "umur_kehamilan_ibu_tt_kedua" => null,
                "tgl_imunisasi_ibu_tt_kedua" => null,
                "imunisasi_tt_kehamilan_sebelumnya" => null,
                "suntikan_tt_pertama_kehamilan_sebelumnya" => null,
                "suntikan_tt_kedua_kehamilan_sebelumnya" => null,
                "suntikan_tt_calon_pengantin" => null,
                "suntikan_tt_pertama_calon_pengantin" => null,
                "suntikan_tt_kedua_calon_pengantin" => null,
                "status_tt_ibu_saat_kehamilan" => null,
            ];
            $dt[$key]["drkp"]['nama'] = [];
            $dt[$key]["drkp"]['profesi'] = [];
            $dt[$key]["drkp"]['alamat'] = [];
            $dt[$key]["drkp"]['frekuensi'] = [];
            $dt[$key]["drpr"]['profesi'] = [];
            $dt[$key]["drpr"]['nama'] = [];
            $dt[$key]["drpr"]['alamat'] = [];
            $dt[$key]["drpr"]['tempat_persalinan'] = [];
            $dt[$key]["drp"] = [
                "obat_yg_dibubuhkan_setelah_tali_pusat_dipotong" => null,
                "sebutkan_obat_bubuhan" => null,
                "petugas_perawat_tali_pusat" => null,
                "obat_saat_merawat_tali_pusat" => null,
                "kesimpulan_diagnosis" => null,
            ];
            $dt[$key]["dtc"] = [
                "cakupan_imunisasi_tt1" => null,
                "cakupan_imunisasi_tt2" => null,
                "cakupan_imunisasi_tt3" => null,
                "cakupan_imunisasi_tt4" => null,
                "cakupan_imunisasi_tt5" => null,
                "cakupan_persalinan_tenaga_kesehatan" => null,
                "cakupan_kunjungan_neonatus1" => null,
                "cakupan_kunjungan_neonatus2" => null,
            ];
            $dt[$key]["dtpel"]['nama'] = [];
            $dt[$key]["dtpel"]['jabatan'] = [];
        }
        $response = app('App\Http\Controllers\TetanusController')->postPe($dt);
        $dt = json_decode($response->getContent());
        echo "<pre>";
        print_r($dt);
        echo "</pre>";
    }

    public function sync_crs()
    {
        $a = SyncModel::gCrs();
        $dt = [];
        foreach ($a as $key => $val) {
            $cek = DB::table('trx_case')->select('id', 'id_pasien')->where('id_crs_old', $val->id_crs_old)->first();
            $dt[$key]['dpel'] = [
                'nama_pelapor' => $val->nama_rs,
                'code_provinsi_pelapor' => $val->id_provinsi_pelapor,
                'code_kabupaten_pelapor' => $val->id_kabupaten_pelapor,
            ];
            $dt[$key]['dk'] = [
                'tgl_laporan_diterima' => $val->tanggal_laporan,
                'tgl_pelacakan' => $val->tanggal_investigasi,
                'nama_dokter_pemeriksa' => $val->nama_dokter_pemeriksa,
                'tgl_periksa' => $val->tgl_mulai_sakit,
                'keadaan_akhir' => $val->keadaan_akhir,
                'penyebab_meninggal' => $val->penyebab_meninggal,
            ];
            $dt[$key]['dc'] = [
                'id_crs_old' => $val->id_crs_old,
                'id_faskes' => $val->id_faskes,
                'id_role' => $val->id_role,
                'jenis_input' => 1,
                'no_rm' => $val->no_rm,
                '_no_epid' => $val->no_epid,
                'no_epid_lama' => $val->no_epid_lama,
                'pengambilan_spesimen' => $val->spesimen,
                'klasifikasi_final' => $val->klasifikasi_final,
                'desc_klasifikasi_final' => $val->klasifikasi_final_desc,
                'created_at' => $val->created_at,
                'updated_at' => $val->updated_at,
            ];
            $dt[$key]['code_wilayah_faskes'] = $val->code_wilayah_faskes;
            $dt[$key]['id_trx_case'] = !empty($cek->id) ? $cek->id : '';
            $dt[$key]['dp'] = [
                'name' => $val->name_pasien,
                'nik' => $val->nik,
                'agama' => '',
                'jenis_kelamin' => $val->jenis_kelamin,
                'tgl_lahir' => $val->tgl_lahir,
                'umur_thn' => $val->umur_thn,
                'umur_bln' => $val->umur_bln,
                'umur_hari' => $val->umur_hari,
                'alamat' => $val->alamat,
                'code_kelurahan' => $val->id_kelurahan,
                'code_kecamatan' => '',
                'code_kabupaten' => '',
                'code_provinsi' => '',
                'tempat_lahir' => $val->tempat_lahir_bayi,
                'umur_kehamilan_bayi_lahir' => $val->umur_kehamilan_bayi,
                'berat_badan_bayi_lahir' => $val->berat_badan_bayi,
            ];
            $dt[$key]['df'] = [
                'name' => $val->nama_ortu,
                'no_telp' => $val->no_telp,
            ];
            $dt[$key]['id_pasien'] = !empty($cek->id_pasien) ? $cek->id_pasien : '';
            $dt[$key]['dg']['gejala'] = [
                '4' => $val->v4,
                '5' => $val->v5,
                '6' => $val->v6,
                '7' => $val->v7,
                '8' => $val->v8,
                '9' => $val->v9,
                '10' => $val->v10,
                '11' => $val->v11,
                '12' => $val->v12,
                '13' => $val->v13,
                '14' => $val->v14,
                '15' => $val->v15,
                '16' => $val->v16,
            ];
            $dt[$key]['dg']['other_gejala'] = [
                '16' => $val->vo16,
            ];
            $dt[$key]['rf'] = [
                'jml_kehamilan_sebelumnya' => $val->jumlah_kehamilan_sebelumnya,
                'umur_ibu' => $val->umur_ibu,
                'hpht' => $val->hari_pertama_haid,
                'ibu_diagnosa_rubella' => $val->diagnosa_rubella,
                'tgl_ibu_diagnosa_rubella' => $val->diagnosa_rubella_date,
                'kontak_ruam_makulopapular' => $val->kontak_ruam_makulopapular,
                'umur_kehamilan_kontak_ruam_makulopapular' => $val->umur_hamil_kontak_ruam_makulopapular,
                'lokasi_kontak_ruam_makulopapular' => $val->desc_kontak_ruam_makulopapular,
                'bepergian_waktu_hamil' => $val->pergi_waktu_hamil,
                'umur_hamil_waktu_pergi' => $val->umur_hamil_waktu_pergi,
                'lokasi_bepergian' => $val->desc_pergi_waktu_hamil,
            ];
            $dt[$key]['dgf']['gejala'] = [
                '17' => $val->conjunctivitis,
                '18' => $val->pilek,
                '19' => $val->batuk,
                '20' => $val->ruam_makulopapular,
                '21' => $val->pembengkakan_kelenjar_limfa,
                '22' => $val->demam,
                '23' => $val->arthralgia,
                '24' => $val->komplikasi_lain,
                '25' => $val->vaksinasi_rubella,
            ];
            $dt[$key]['dgf']['tgl_kejadian'] = [
                '17' => $val->conjunctivitis_date,
                '18' => $val->pilek_date,
                '19' => $val->batuk_date,
                '20' => $val->ruam_makulopapular_date,
                '21' => $val->pembengkakan_kelenjar_limfa_date,
                '22' => $val->demam_date,
                '23' => $val->arthralgia_date,
                '24' => $val->komplikasi_lain_date,
                '25' => $val->vaksinasi_rubella_date,
            ];
            $dt[$key]['ds']['jenis_spesimen'] = [
                '1',
                '2',
                '3',
                '4',
            ];
            $dt[$key]['ds']['tgl_ambil_spesimen'] = [
                $val->tgl_ambil_serum1,
                $val->tgl_ambil_serum2,
                $val->tgl_ambil_throat_swab,
                $val->tgl_ambil_urine,
            ];
            $dt[$key]['ds']['tgl_kirim_lab'] = [
                $val->tgl_kirim_serum1,
                $val->tgl_kirim_serum2,
                $val->tgl_kirim_throat_swab,
                $val->tgl_kirim_urine,
            ];
            $dt[$key]['ds']['tgl_terima_lab'] = [
                $val->tgl_tiba_serum1,
                $val->tgl_tiba_serum2,
                $val->tgl_tiba_throat_swab,
                $val->tgl_tiba_urine,
            ];
            $dt[$key]['dhs']['jenis_pemeriksaan'] = [
                '1',
                '2',
                '3',
                '4',
                '5',
            ];
            $dt[$key]['dhs']['hasil'] = [
                $val->hasil_igm_serum1,
                $val->hasil_igm_serum2,
                $val->hasil_igg_serum1,
                $val->hasil_igg_serum2,
                $val->hasil_isolasi,
            ];
            $dt[$key]['dhs']['jenis_virus'] = [
                $val->virus_igm_serum1,
                $val->virus_igm_serum2,
                $val->virus_igg_serum1,
                $val->virus_igg_serum2,
                $val->virus_isolasi,
            ];
            $dt[$key]['dhs']['tgl_hasil'] = [
                $val->tgl_igm_serum1,
                $val->tgl_igm_serum2,
                $val->tgl_igg_serum1,
                $val->tgl_igg_serum2,
                $val->tgl_isolasi,
            ];
            $dt[$key]['dhs']['kadar_igG'] = [
                '',
                '',
                $val->kadar_igg_serum1,
                $val->kadar_igg_serum2,
                '',
            ];
        }

        $response = app('App\Http\Controllers\CrsController')->postCase($dt);
        $dt = json_decode($response->getContent());
        echo "<pre>";
        print_r($dt);
        echo "</pre>";
    }

    public function sync_epid_crs()
    {
        $a = SyncModel::gEpidCrs();
        $dt = [];
        foreach ($a as $key => $val) {
            $cek = DB::table('trx_case AS a')->leftJoin('trx_pe AS b', 'a.id', '=', 'b.id_trx_case')->select('a.id AS id_trx_case', 'a.id_pasien', 'b.id AS id_trx_pe')->where('a.id_crs_old', $val->id_crs_old)->first();
            if (!empty($cek->id_trx_case)) {
                $dt[$key]['id_trx_case'] = $cek->id_trx_case;
                $dt[$key]['id_trx_pe'] = !empty($cek->id_trx_pe) ? $cek->id_trx_pe : '';
                $dt[$key]['rf'] = [
                    'ibu_diagnosa_rubella' => $val->diagnosa_rubella,
                    'tgl_ibu_diagnosa_rubella' => $val->diagnosa_rubella_date,
                    'kontak_ruam_makulopapular' => $val->kontak_ruam_makulopapular,
                    'umur_kehamilan_kontak_ruam_makulopapular' => $val->umur_hamil_kontak_ruam_makulopapular,
                    'lokasi_kontak_ruam_makulopapular' => $val->desc_kontak_ruam_makulopapular,
                    'bepergian_waktu_hamil' => $val->pergi_waktu_hamil,
                    'umur_hamil_waktu_pergi' => $val->umur_hamil_waktu_pergi,
                    'lokasi_bepergian' => $val->desc_pergi_waktu_hamil,
                ];
            }
        }
        $response = app('App\Http\Controllers\CrsController')->postPe($dt);
        $dt = json_decode($response->getContent());
        echo "<pre>";
        print_r($dt);
        echo "</pre>";
    }
}

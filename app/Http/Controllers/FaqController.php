<?php

namespace App\Http\Controllers;


use App\Models\Faq;
use Response, Request;

class FaqController extends Controller
{
  public function index()
  {
    $data = Faq::getData();
    $compact = array(
      'title'         => 'Surveilans PD3I',
      'contentTitle'  => null,
      'data'          => $data
    );
    return view('help.index',$compact);
  }

  public function viewAdd()
  {
    $compact = array(
      'title'         => 'Surveilans PD3I',
      'contentTitle'  => 'Frequently Asked Questions',
      'data'          => array()
    );
    return view('help.addFaq',$compact);
  }

  public function viewEdit($id)
  {
    $dtFaq = Faq::getDataFaq(array('id'=>$id));
    $compact = array(
      'title'         => 'Surveilans PD3I',
      'contentTitle'  => 'Perubahan Frequently Asked Questions',
      'data'          => $dtFaq[0]
    );
    return view('help.editFaq',$compact);
  }
  public function postFaq()
  {
    $request = Request::json()->all();
    $data['id']=$request[0];
    $data['pertanyaan']=$request[1];
    $data['jawaban']=$request[2];
    $success = true;
    $code = 200;
    if(empty($data['id'])){
      Faq::insert($data);
    }else{
      Faq::pull(array('id'=>$data['id']),$data);
    }
    return Response::json(array(
      'success'=>$success,
    ),$code);
  }
  public function postDelete($id)
  {
    $request = array('id'=>$id);
    if($resp = Faq::postDelete($id))
      {
        $success = true;
        $response = $resp;
        $code = '200';
      }else{
        $success = false;
        $response = $resp;
        $code = '401';
      }
      return Response::json(array(
        'success'=>$success,
        'response'=>$response
      ),$code);
    }
  }

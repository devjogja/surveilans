<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Request, Response;
use App\Models\Laboratorium;
use App\Models\Tx;
use App\Libraries\Datatable;

class LaboratoriumController extends Controller
{
	public function index()
	{
		$compact = array(
			'title'         => 'Surveilans PD3I',
			'contentTitle'  => 'Master Laboratorium',
			'data'          => array()
		);
		return view('faskes.laboratorium', $compact);
	}

	public function getData()
	{
		$config['table'] = 'view_mst_laboratorium AS a';
		$config['coloumn'] = ['id', 'code', 'name', 'full_alamat', 'telepon', 'konfirm_code'];
		$config['columnSelect'] = ['a.id', 'a.code', 'a.name', 'a.full_alamat', 'a.telepon', 'a.konfirm_code'];
		$config['columnFormat'] = ['number', '', '', '', '', ''];
		$config['searchColumn'] = ['a.code', 'a.name'];
		$config['key'] = 'a.id';
		$config['action'][]		= array(
			'action'	=> 'editMaster',
			'url' 		=> url('faskes/laboratorium/edit'),
			'key'		=> 'id',
		);
		$config['action'][]		= array(
			'action'	=> 'delete',
			'url'		=> url('faskes/laboratorium/delete'),
			'key'		=> 'id',
		);
		$data = Datatable::getData($config);
		echo json_encode($data);
	}

	public function postData()
	{
		$id = Request::get('id');
		$data 	= Request::get('dt');
		if (empty($id)) {
			$result = Tx::insertData('mst_laboratorium', $data);
		} else {
			$result = Tx::updateData('mst_laboratorium', ['id' => $id], $data);
		}
		if ($result) {
			$response 	= array(
				'success'	=> true,
				'messageType'	=> 'info',
				'title'		=> 'Berhasil!',
				'message'	=> 'Data Laboratorium berhasil disimpan',
			);
		} else {
			$response 	= array(
				'success'	=> false,
				'messageType'	=> 'danger',
				'title'		=> 'Gagal!',
				'message'	=> 'Data Laboratorium gagal disimpan',

			);
		}
		echo (json_encode($response));
	}

	public function getDetail($id = '')
	{
		$response = Tx::getData('mst_laboratorium', ['id' => $id])->first();
		return Response::json(array(
			'success' => true,
			'response' => $response
		), 200);
	}

	public function postDelete($id)
	{
		if (Tx::deleteData('mst_laboratorium', ['id' => $id])) {
			$response 	= array(
				'success'	=> 1,
				'messageType'	=> 'info',
				'title'		=> 'Berhasil!',
				'message'	=> 'Data Laboratorium berhasil dihapus',
			);
		} else {
			$response 	= array(
				'success'	=> 0,
				'messageType'	=> 'danger',
				'title'		=> 'Gagal!',
				'message'	=> 'Data Laboratorium gagal dihapus',
			);
		}
		echo (json_encode($response));
	}
}

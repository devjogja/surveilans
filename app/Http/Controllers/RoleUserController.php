<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Tx;
use Helper,Request,Response,Session,Validator,DB;

class RoleUserController extends Controller {
	public function getProfile($id_user=null) {
		$where = [];
		if (!empty($id_user)) {
			$where    = array('a.id_user' => $id_user);
		}
		$response = RoleUser::getRoleUser($where);
		return Response::json(array(
			'success'  => true,
			'response' => $response,
		), 200);
	}
	public function selectProfile() {
		Session::forget('id_role');
		Session::forget('id_faskes');
		return redirect('viewProfile');
	}
	public function viewProfile() {
		$dtRoleUser = json_decode($this->getProfile($this->userId)->getContent())->response;
		$resp=[];
		foreach ($dtRoleUser as $key => $val) { 
			$resp[$val->id_role_user] = [
				'id_faskes'=>$val->id_faskes,
				'id_role'=>$val->id_role,
				'code_role'=>$val->code_role,
				'role_faskes'=>$val->name_role,
				'nama_faskes'=>$val->name_faskes,
				'alamat_faskes'=>$val->alamat_faskes,
			];
		}
		$profile= [];
		$dtRoleAllUser = json_decode($this->getProfile()->getContent())->response;
		foreach ($dtRoleAllUser as $key => $val) { 
			if($val->code_kabupaten_faskes){
				$pkm[$val->code_kecamatan_faskes][$val->id_faskes] = [
					'name_puskesmas_faskes'=>'PUSKESMAS '.$val->name_faskes
				];
				if ($val->code_role=='puskesmas') {
					$kec[$val->code_kabupaten_faskes][$val->code_kecamatan_faskes] = [
						'name_kecamatan_faskes'=>'KECAMATAN '.$val->name_kecamatan_faskes,
						'puskesmas'=>$pkm[$val->code_kecamatan_faskes]
					];
				}
				if ($val->code_role=='rs') {
					$rs[$val->code_kabupaten_faskes][$val->id_faskes] = [
						'name_rs_faskes' => 'RUMAH SAKIT '.$val->name_faskes
					];
				}
				$kab[$val->code_provinsi_faskes][$val->code_kabupaten_faskes] = [
					'name_kabupaten_faskes'=>'KABUPATEN '.$val->name_kabupaten_faskes,
					'rs'=>(isset($rs[$val->code_kabupaten_faskes]))?$rs[$val->code_kabupaten_faskes]:null,
					'kecamatan'=>isset($kec[$val->code_kabupaten_faskes])?$kec[$val->code_kabupaten_faskes]:null,
				];
				$profile['provinsi'][$val->code_provinsi_faskes] = [
					'name_provinsi_faskes'=>'PROVINSI '.$val->name_provinsi_faskes,
					'kabupaten'=>$kab[$val->code_provinsi_faskes]
				];
			}
		}
		$response = [];
		foreach ($resp as $key => $val) {
			$response[$key] = $val;
			foreach ($profile as $kpusat => $vpusat) {
				if ($val['code_role']=='pusat') {
					$response[$key]['lab'] = DB::table('mst_laboratorium')->select(['id','name','code'])->get();
					$response[$key]['kemenkes'] = $vpusat;
				}
				foreach ($vpusat as $kprov => $vprov) {
					if ($val['code_role']=='provinsi' && $val['id_faskes']==$kprov) {
						$response[$key]['provinsi'][$kprov] = $vprov;
					}
					foreach ($vprov['kabupaten'] as $kkab => $vkab) {
						if ($val['code_role']=='kabupaten' && $val['id_faskes']==$kkab) {
							$response[$key]['kabupaten'][$kkab] = $vkab;
						}
					}
				}
			}
		}
		if (empty($dtRoleUser)) {
			Helper::session_flash('info', 'Info', 'Selamat Datang.');
			return redirect('role/level');
		}
		if (Session::get('id_role') && Session::get('id_faskes')) {
			return redirect('dashboard');
		}
		$compact = array(
			'title'        => 'Profile',
			'contentTitle' => 'Profile',
			'data'         => array(
				'role' => $response,
			),
		);
		return view('profile.index', $compact);
	}

	function deleteProfile($id_role){
		$response = Tx::deleteData('role_users',['id'=>$id_role]);
		return Response::json(array(
			'success'=>true,
			'response'=>$response
		),200);
	}

	public function viewRoleLevel() {
		$role       = array();
		$role[null] = '--Pilih--';
		foreach (Role::getData() as $key => $value) {
			$role[$value->id] = $value->name;
		}
		$compact = array(
			'title'        => 'Rule level user',
			'contentTitle' => 'Wilayah administrasi',
			'data'         => array(
				'role' => $role,
			),
		);
		return view('role.role_access', $compact);
	}

	public function getRoleProfile() {
		$id_role = Request::get('id_role');
		$index   = '';
		if ($id_role == '1') {
			$index = 'role_puskesmas';
		} else if ($id_role == '2') {
			$index = 'role_rs';
		} else if ($id_role == '3') {
			$index = 'role_pusat';
		} else if ($id_role == '4') {
			$index = 'role_lab';
		} else if ($id_role == '5') {
			$index = 'role_provinsi';
		} else if ($id_role == '6') {
			$index = 'role_kabupaten';
		}

		$compact = array(
			'title'        => 'Rule akses user',
			'contentTitle' => 'Rule Akses',
			'data'         => array(
				'role_id' => $id_role,
			),
		);
		return view('role.' . $index, $compact);
	}

	public function postRoleProfile($dt=[]) {
		if (empty($dt)) {
			$request   = Request::json()->all();
		}else{
			$request = $dt;
			$this->userId = $request['created_by'];
		}
		$validator = Validator::make($request['role_user'], [
			'role_id'   => 'required',
			'faskes_id' => 'required',
		]);

		if ($validator->fails()) {
			$success  = false;
			$response = $validator->errors()->all();
			$code     = 400;
		} else {
			$success                            = true;
			$request['role_user']['user_id']    = $this->userId;
			$request['role_user']['created_by'] = $this->userId;
			if ($request['role_user']['role_id']=='1') {
				Tx::updateData('mst_puskesmas',['id'=>$request['role_user']['faskes_id']],$request['dt']);
			}else if ($request['role_user']['role_id']=='2') {
				Tx::updateData('mst_rumahsakit',['id'=>$request['role_user']['faskes_id']],$request['dt']);
			}
			$where                              = array(
				'id_user'   => $this->userId,
				'id_faskes' => $request['role_user']['faskes_id'],
				'id_role'   => $request['role_user']['role_id'],
			);
			if ($user = RoleUser::getRoleUser($where)) {
				if (isset($request['id_faskes_user'])) {
					$code     = 200;
					$response = true;
					$msg = 'Profile user berhasil di update.';
					Helper::session_flash('success', 'Informasi', $msg);
				}else{
					$code     = 201;
					$response = false;
					$msg = 'Profile user sudah ada.';
					Helper::session_flash('warning', 'Gagal', $msg);
				}
			} else {
				$code     = 200;
				$response = RoleUser::create($request['role_user']);
				$msg = 'Profile user berhasil tersimpan';
				Helper::session_flash('success', 'Informasi', $msg);
			}
		}

		return Response::json(array(
			'success'  => $success,
			// 'request'  => $request,
			// 'response' => $response,
			'msg' => $msg,
		), $code);
	}

	public function setProfile($id_role,$id_faskes) {
		Session::put('id_role', $id_role);
		Session::put('id_faskes', $id_faskes);
		/* if ($id_role=='lab') {
			return redirect('lab');	
		}else{ */
			return redirect('dashboard');
		// }
	}
}

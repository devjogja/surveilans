<?php

namespace App\Http\Controllers;

use App\Models\Logging;
use App\Models\RoleUser;
use App\Models\Tx;
use App\Models\UserMeta;
use App\User;
use Auth;
use Helper;
use JWTAuth;
use Mail;
use Reminder;
use Request;
use Response;
use Sentinel;
use Session;
use Validator;

class AuthenticateController extends Controller
{
    public function getUser()
    {
        $response = User::getData();
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function viewListUser()
    {
        $compact = array(
            'title' => 'Daftar Petugas Surveilans',
            'contentTitle' => 'Daftar Petugas Surveilans',
            'data' => array(),
        );
        return view('user.index', $compact);
    }

    public function viewEditUser()
    {
        $compact = array(
            'title' => 'Edit User',
            'contentTitle' => 'Form Edit User',
            'data' => array(),
        );
        return view('register.index', $compact);
    }

    public function viewEditFaskesUser()
    {
        $role = Helper::role();
        $compact = array(
            'title' => 'Edit Faskes User',
            'contentTitle' => 'Form Edit Faskes User',
            'data' => array(
                'role_id' => $role->id_role,
                'id_faskes_user' => $role->id_faskes,
            ),
        );
        if ($role->code_role == 'puskesmas') {
            $view = 'role.role_puskesmas';
        } else if ($role->code_role == 'rs') {
            $view = 'role.role_rs';
        } else if ($role->code_role == 'pusat') {
            $view = 'role.role_pusat';
        } else if ($role->code_role == 'lab') {
            $view = 'role.role_lab';
        } else if ($role->code_role == 'provinsi') {
            $view = 'role.role_provinsi';
        } else if ($role->code_role == 'kabupaten') {
            $view = 'role.role_kabupaten';
        }

        return view($view, $compact);
    }

    public function postCheckEmail()
    {
        $request = Request::json()->all();
        return false;
    }

    public function getProfileUser()
    {
        if ($user = Sentinel::check()) {
            $id_user = $user->id;
        } else {
            $id_user = Auth::user()->id;
        }
        $response = User::getData(['a.id' => $id_user]);
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getFaskesUser()
    {
        $request = Request::json()->all();
        if (isset($request['id_user'])) {
            $idUser = $request['id_user'];
            $response = RoleUser::getRoleUser(array('a.id_user' => $idUser));
            return Response::json(array(
                'success' => true,
                'response' => $response,
            ), 200);
        }
    }

    public function apiLogin()
    {
        $credentials['email'] = Request::json()->get('email');
        $credentials['password'] = Request::json()->get('password');
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $idUser = Auth::user()->id;
        $userData = RoleUser::getRoleUser(array('a.id_user' => $idUser));
        $response = [
            'token' => $token,
            'user' => $userData,
        ];
        return response()->json(compact('response'));
    }

    public function apiLogout()
    {
        $token = JWTAuth::getToken();
        if ($token) {
            if (JWTAuth::setToken($token)->invalidate()) {
                $success = true;
                $response = 'Success Logout';
            } else {
                $success = false;
                $response = 'Failed Logout';
            }
        } else {
            $success = false;
            $response = 'Failed Logout';
        }
        return Response::json(array(
            'success' => $success,
            'response' => $response,
        ));
    }

    public function viewRegister()
    {
        if (Sentinel::check()) {
            return redirect('/');
        }
        $compact = array(
            'title' => 'Daftar user',
            'contentTitle' => 'Daftar user',
            'data' => array(),
        );
        return view('register.index', $compact);
    }

    public function postResetPass()
    {
        $user = User::whereEmail(Request::get('email'))->first();
        $sentinelUser = Sentinel::findById($user->id);
        if (empty($user)) {
            Helper::session_flash('warning', 'Reset Password', 'Email anda tidak terdaftar.');
        } else {
            $reminder = Reminder::exists($sentinelUser) ?: Reminder::create($sentinelUser);
            $this->sendEmail($user, $reminder->code);
            Helper::session_flash('info', 'Reset Password', 'Reset code sudah dikirim ke email anda.');
        }
        return redirect('/');
    }

    public function postResetPasswords($email, $resetCode)
    {
        $request = Request::json()->all();
        $user = User::whereEmail($email)->first();
        $sentinelUser = Sentinel::findById($user->id);
        if (empty($user)) {
            $status = false;
            $response = 'Email anda tidak terdaftar';
        } else {
            $reminder = Reminder::exists($sentinelUser);
            if ($reminder = Reminder::exists($sentinelUser)) {
                if ($resetCode == $reminder->code) {
                    Reminder::complete($sentinelUser, $resetCode, $request['password']);
                    $status = true;
                    $response = 'Silahkan login dengan password baru.';
                    Helper::session_flash('info', 'Reset Password', 'Silahkan login dengan password baru.');
                } else {
                    $status = false;
                    $response = 'Reset code anda salah.';
                }
            } else {
                $status = false;
                $response = 'Reset code anda salah.';
            }
        }
        return Response::json(array(
            'success' => $status,
            'response' => $response,
        ), 200);
    }

    public function getResetPassword($email, $resetCode)
    {
        $user = User::whereEmail($email)->first();
        $sentinelUser = Sentinel::findById($user->id);
        if (empty($user)) {
            Helper::session_flash('warning', 'Reset Password', 'Email anda tidak terdaftar.');
            return redirect('/');
        } else {
            $reminder = Reminder::exists($sentinelUser);
            if ($reminder = Reminder::exists($sentinelUser)) {
                if ($resetCode == $reminder->code) {
                    $compact = array(
                        'title' => 'Reset Password',
                        'contentTitle' => 'Reset Password',
                        'data' => array(
                            'email' => $email,
                            'resetCode' => $resetCode,
                        ),
                    );
                    return view('user.reset_pass', $compact);
                } else {
                    Helper::session_flash('warning', 'Reset Password', 'Reset code anda salah.');
                    return redirect('/');
                }
            } else {
                Helper::session_flash('warning', 'Reset Password', 'Reset code anda salah.');
                return redirect('/');
            }
        }
    }

    private function sendEmail($user, $code)
    {
        Mail::send('emails.forgot-password', [
            'user' => $user,
            'code' => $code,
        ], function ($message) use ($user) {
            $message->to($user->email);
            $message->subject("Hello $user->first_name, Reset your password");
        });
    }

    public function postRegister($dt = [])
    {
        if (!empty($dt)) {
            $request = $dt;
        } else {
            $request = Request::json()->all();
        }
        $du = Validator::make($request['user'], [
            // 'email'=>'required|unique:users,email|max:255',
            // 'password' => 'required|min:5',
            'first_name' => 'required',
        ]);
        $dum = Validator::make($request['user_meta'], [
            // 'no_telp'=>'required'
        ]);
        if ($du->fails()/* || $dum->fails()*/) {
            $success = false;
            $rp = array_merge($du->errors()->all(), $dum->errors()->all());
            $response = '';
            foreach ($rp as $key => $val) {
                $response .= $val . ', ';
            }
        } else {
            if (empty($request['id'])) {
                $checkEmail = User::getData(['email' => $request['user']['email']]);
                if ($checkEmail) {
                    $success = false;
                    $response = 'Email sudah ada';
                } else {
                    $success = true;
                    $user = Sentinel::registerAndActivate($request['user']);
                    $id_user = $user->id;
                    $request['user_meta']['id_user'] = $id_user;
                    $userMeta = UserMeta::insertData('users_meta', $request['user_meta']);
                    Helper::session_flash('info', 'Informasi', 'Selamat, anda telah berhasil mendaftar');
                    $where = array('a.id' => $user->id);
                    $response = User::getData($where);
                }
            } else {
                $success = true;
                $id_user = $request['id'];
                $du = Sentinel::findById($id_user);
                $user = Sentinel::update($du, $request['user']);
                $userMeta = Tx::updateData('users_meta', ['id_user' => $id_user], $request['user_meta']);
                Helper::session_flash('info', 'Informasi', 'User berhasil di update.');
                $where = array('a.id' => $user->id);
                $response = User::getData($where);
            }
        }

        return Response::json(array(
            'success' => $success,
            'request' => $request,
            'response' => $response,
        ));
    }

    public function getLogin()
    {
        $credentials['email'] = Request::get('email');
        $credentials['password'] = Request::get('password');
        $validator = Validator::make($credentials, [
            'email' => 'required|unique:users,email',
        ]);
        if ($validator->fails()) {
            if ($auth = Sentinel::authenticate($credentials)) {
                Sentinel::login($auth);
                Logging::insert('login');
                Helper::session_flash('info', 'Info', 'Selamat Datang.');
                return redirect('viewProfile');
            } else {
                Helper::session_flash('warning', 'Gagal Login', 'Password tidak sesuai');
                return redirect('/');
            }
        } else {
            Helper::session_flash('warning', 'Login', 'Email not registered');
            return redirect('register');
        }
    }

    public function getLogout()
    {
        Logging::insert('logout');
        $user = Sentinel::check();
        Sentinel::logout($user, true);
        Session::flush();
        Helper::session_flash('info', 'Sign Out', 'Anda telah berhasil keluar');
        return redirect('/');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Sql;
use App\Models\Tx;
use App\Models\TxCase;
use App\Models\TxSpesimen;
use Carbon\Carbon;
use Datatables;
use DB;
use Helper;
use Request;
use Response;
use Validator;

class CrsController extends Controller
{
    protected $index;
    public function __construct()
    {
        $this->index = 'crs';
    }

    public function index()
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Penyakit ' . $dc[0]->name,
            'data' => array(
                'id_trx_case' => null,
                'index' => $index,
            ),
        );
        return view('case.' . $index . '.index', $compact);
    }

    public function exportExampleImportKasus()
    {
        $file = public_path() . "/download/crs/template import crs.xls";
        return Response::download($file, 'Contoh format import kasus crs.xls');
    }

    public function export($request = '')
    {
        $param['filter'] = json_decode($request);
        if (!empty($param['filter']->code_role)) {
            $param['roleUser'] = (object) array(
                'code_role' => $param['filter']->code_role,
                'id_faskes' => $param['filter']->id_faskes,
                'code_faskes' => $param['filter']->code_faskes,
            );
        }
        $q = Sql::getData("crs", $param);
        $q->groupBy('a.id_trx_case');
        $r = $q->get();
      
        $compact['dd'] = $r;
        // return view('case.crs.export', $compact);die();
        $export = view('case.crs.export', $compact);

        $filename = 'export_crs-' . date('YmdHi') . '.xls';

        header("Content-type: application/vnd.ms-excel; name='excel'");
        header("Cache-Control: max-age=0");
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Content-Transfer-Encoding: text');
        echo $export;
    }

    public function indexLab()
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Laboratorium Penyakit ' . $dc[0]->name,
            'data' => array(
                'id_trx_case' => null,
                'index' => $index,
            ),
        );
        return view('lab.case.' . $index . '.index', $compact);
    }

    public function viewAnalisa()
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Analisa Penyakit ' . $dc[0]->name,
            'data' => array(
                'id_trx_case' => null,
                'index' => $index,
            ),
        );
        return view('case.' . $index . '.analisa_mobile', $compact);
    }
    public function viewPeriksa($id_trx_case)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $tx_case = TxCase::getDataCrs(array('id' => $id_trx_case));
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Pemeriksaan Lab Penyakit ' . $case,
            'data' => array(
                'id_trx_case' => $id_trx_case,
                'id_trx_pe' => null,
            ),
        );
        return view('lab.case.' . $index . '.periksa.index', $compact);
    }

    public function getDataCrs()
    {
        $request = Request::all();
        $param['filter'] = json_decode($request['dt']);
        $param['roleUser'] = Helper::role();
        $param['query']['select'] = ['id_trx_case', 'no_epid', 'name_pasien', 'umur_hari', 'umur_bln', 'umur_thn', 'code_kecamatan_pasien', 'full_address', 'keadaan_akhir_txt', 'klasifikasi_final_txt', 'id_role', 'name_faskes', 'code_kecamatan_faskes', 'jenis_input', 'id_pe', 'id_faskes_old', 'id_role_old', 'tgl_input', 'tgl_update', 'jenis_input_text', 'pe_text'];
        $param['query']['where'] = ['id_case' => 5];
        $d = Sql::getData("view_list_case", $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            foreach ($request['columns'] as $key => $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->editColumn('no_epid', function ($dt) {
            $no_epid = $dt->no_epid;
            if ($dt->jenis_input == '1') {
                $no_epid .= '<br><span class="label label-info">Web</span>';
            } else if ($dt->jenis_input == '2') {
                $no_epid .= '<br><span class="label label-success">Android</span>';
            } else if ($dt->jenis_input == '3') {
                $no_epid .= '<br><span class="label label-warning">Import</span>';
            }
            if ($dt->id_role == '2') {
                $no_epid .= "<span class='label label-primary' style='background-color:#b503b5 !important;'> Data dari rumahsakit </span>";
            }
            if ($dt->id_faskes_old) {
                $fo = '';
                if ($dt->id_role_old == '1') {
                    $fo = 'PUSKESMAS ' . DB::table('mst_puskesmas')->select(['name'])->where('id', $dt->id_faskes_old)->first()->name;
                } else if ($dt->id_role_old == '2') {
                    $fo = DB::table('mst_rumahsakit')->select(['name'])->where('id', $dt->id_faskes_old)->first()->name;
                }
                $no_epid .= "<span class='label label-primary'> Data kiriman dari " . $fo . "</span>";
            }
            return $no_epid;
        });
        $q->editColumn('id_pe', function ($dt) {
            $roleUser = Helper::role();
            $disabled = '';
            if (!in_array($roleUser->code_role, ['puskesmas', 'rs'])) {
                $disabled = "disabled='disabled' onclick='return false;'";
            }
            $pe = '<a href="' . url('case/crs/pe/' . $dt->id_trx_case) . '"'.$disabled.' title="Penyelidikan Epidemologi" data-toggle="tooltip" class="btn btn-sm btn-flat btn-success">PE</a>';
            if ($dt->id_pe) {
                $pe = '<a title="Penyelidikan Epidemologi" data-toggle="tooltip" class="btn btn-sm btn-flat btn-danger">Sudah PE</a>';
            }
            return $pe;
        });
        $q->addColumn('action', function ($dt) {
            $roleUser = Helper::role();
            $action = "<div class='text-center'>";
            $action .= "<a href=" . url("case/crs/detail/" . $dt->id_trx_case) . " title='Detail data' data-toggle='tooltip' class='btn btn-sm btn-flat btn-success'><i class='fa fa-asterisk'></i></a>";
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'kabupaten'])) {
                $action .= "<a href=" . url("case/crs/update/" . $dt->id_trx_case) . " title='Edit data' data-toggle='tooltip' class='btn btn-sm btn-flat btn-warning'><i class='fa fa-pencil'></i></a>";
            };
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'provinsi', 'pusat'])) {
                $action .= '<a action="' . url('case/crs/delete/' . $dt->id_trx_case) . '" title="Delete data"  class="btn btn-sm btn-flat btn-danger delete" data-toggle="modal" data-target=".modaldelete"><i class="fa fa-remove"></i></a>';
            };
            if (in_array($roleUser->code_role, ['puskesmas']) and $dt->id_role == '1') {
                if ($dt->code_kecamatan_faskes != $dt->code_kecamatan_pasien) {
                    $action .= '<a action="' . url('case/crs/move/' . $dt->id_trx_case) . '" title="Move data" data-toggle="modal" class="btn btn-sm btn-flat btn-primary move" data-target=".modalmove"><i class="fa fa-step-forward"></i></a>';
                }
            }
            $action .= '</div>';
            return $action;
        });
        return $q->make(true);
    }

    public function getEpid($param = [])
    {
        if (!empty($param)) {
            $request = $param;
        } else {
            $request = Request::json()->all();
        }
        $case = Helper::getCase(array('alias' => $this->index));
        $code = $case[0]->code;
        $id_kelurahan = $request['id_kelurahan'];
        $thn_case = $cthn = '';
        $q = DB::table('view_list_case');
        $q->where('id_case', 5);
        $q->select(DB::raw('MAX(no_epid) AS no_epid'));
        if ($request['tgl_sakit']) {
            $thn_case = \Carbon\Carbon::parse($request['tgl_sakit'])->format('Y');
            $cthn = \Carbon\Carbon::parse($request['tgl_sakit'])->format('y');
            $q->where('thn_sakit', $thn_case);
        }
        $q->where('code_kelurahan_pasien', $id_kelurahan);
        $q->orderBy('id_trx_case', 'DESC');
        $cekEpid = $q->first();

        if (!empty($cekEpid->no_epid)) {
            $cc = substr($cekEpid->no_epid, -3);
            $no = str_pad($cc + 1, 3, 0, STR_PAD_LEFT);
        } else {
            $no = '001';
        }
        $no_epid = $code . $id_kelurahan . $cthn . $no;

        return Response::json(array(
            'response' => $no_epid,
        ));
    }

    public function postDelete($id = null)
    {
        $rId = Request::json()->all();
        if ($id != null) {
            $rId['query']['delete'][] = $id;
        }
        $response = [];
        foreach ($rId['query']['delete'] as $key => $val) {
            $response[$val] = Tx::deleteData('trx_case', ['id' => $val]);
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function viewDetail($id)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $response = $this->getDetail($id);
        $dt = json_decode($response->getContent());
        $data = $dt->response[0];
        $dtGejala = [];
        $i = 1;
        foreach ($data->dtGejala as $key => $val) {
            if ($val->id_gejala) {
                $dtGejala[$val->id_gejala] = $val;
            } else {
                $dtGejala[$i++] = $val;
            }
        }
        $data->dtGejala = $dtGejala;
        $dtGejalaFamily = [];
        $i = 1;
        foreach ($data->dtGejalaFamily as $key => $val) {
            if ($val->id_gejala) {
                $dtGejalaFamily[$val->id_gejala] = $val;
            } else {
                $dtGejalaFamily[$i++] = $val;
            }
        }
        $data->dtGejalaFamily = $dtGejalaFamily;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Detail Pasien Kasus Penyakit ' . $case,
            'data' => array(
                'index' => $index,
                'response' => $data,
            ),
        );
        return view('case.' . $index . '.detail', $compact);
    }

    public function getDetail($id = null)
    {
        if ($id) {
            $request = [
                'where' => ['id' => [$id]],
                'query' => ['select' => ["*", "dtGejala", "dtGejalaFamily", "dtPemeriksaanLab", "dtHasilLab"]],
            ];
        } else {
            $request = Request::json()->all();
        }
        $qQuery = [];
        if (isset($request['where'])) {
            foreach ($request['where'] as $key => $val) {
                if ($val[0] == '*') {
                    $qQuery['where'][$key] = '*';
                } else {
                    $qQuery['whereIn'][$key] = $val;
                }
            }
            if (isset($request['where']['code_roles'])) {
                if ($request['where']['code_roles'][0] == 'puskesmas') {
                    $c = [];
                    foreach ($request['where']['id_faskes'] as $key => $val) {
                        $codeKelurahanKerjaPkm = Tx::getData('view_wilayah_kerja_puskesmas', ['id_faskes' => $val], ['select' => ['code_kelurahan']])->get('code_kelurahan');
                        foreach ($codeKelurahanKerjaPkm as $k => $v) {
                            $c[] = $v->code_kelurahan;
                        }
                    }
                    $qQuery['orWhere']['whereIn'] = ['code_kelurahan_pasien' => $c, 'code_roles' => ['rs']];
                }
            }
        }
        if (isset($request['query'])) {
            foreach ($request['query'] as $key => $val) {
                foreach ($val as $v) {
                    if (!in_array($v, ["dtGejala", "dtGejalaFamily", "dtPemeriksaanLab", "dtHasilLab"])) {
                        $var[] = $v;
                    }
                }
                $qQuery[$key] = $var;
            }
        }
        if (!empty($request['pagination'])) {
            $qQuery['pagination'] = $request['pagination'];
        }
        $resp = Tx::getData('crs', [], $qQuery)->get();
        $dResp = $response = [];
        foreach ($resp as $key => $val) {
            $dt = (array) $val;
            if (isset($request['query'])) {
                foreach ($request['query'] as $k => $v) {
                    foreach ($v as $var) {
                        ($var == 'dtGejala') ? $dt['dtGejala'] = Tx::getData('view_gejala', ['id_trx_klinis' => $val->id_trx_klinis], ['select' => ['id_gejala', 'name', 'val_gejala', 'val_gejala_txt', 'other_gejala']])->get() : null;
                        ($var == 'dtGejalaFamily') ? $dt['dtGejalaFamily'] = Tx::getData('view_gejala_family', ['id_trx_case' => $val->id], ['select' => ['id_gejala', 'name', 'val_gejala', 'val_gejala_txt', 'tgl_kejadian']])->get() : null;
                        ($var == 'dtPemeriksaanLab') ? $dt['dtPemeriksaanLab'] = Tx::getData('view_spesimen_case_crs', ['id_trx_case' => $val->id], ['select' => ['id', 'jenis_spesimen', 'jenis_spesimen_txt', 'tgl_ambil_spesimen', 'tgl_kirim_lab', 'tgl_terima_lab']])->get() : null;
                        ($var == 'dtHasilLab') ? $dt['dtHasilLab'] = Tx::getData('view_hasil_lab_crs', ['id_trx_case' => $val->id], ['select' => ['jenis_pemeriksaan', 'jenis_pemeriksaan_txt', 'hasil', 'hasil_txt', 'jenis_virus', 'tgl_hasil_lab', 'kadar_igG']])->get() : null;
                    }
                }
            }
            $response[] = $dt;
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getDetailPe($id_pe = '')
    {
        if ($id_pe) {
            $request = [
                'where' => ['id' => [$id_pe]],
                'query' => ['select' => ['*']],
            ];
        } else {
            $request = Request::json()->all();
        }
        $qQuery = [];
        if (isset($request['where'])) {
            foreach ($request['where'] as $key => $val) {
                if ($val[0] == '*') {
                    $qQuery['where'][$key] = '*';
                } else {
                    $qQuery['whereIn'][$key] = $val;
                }
            }
        }
        if (isset($request['query'])) {
            foreach ($request['query'] as $key => $val) {
                foreach ($val as $v) {
                    $var[] = $v;
                }
                $qQuery[$key] = $var;
            }
        }
        if (!empty($request['pagination'])) {
            $qQuery['pagination'] = $request['pagination'];
        }
        $resp = Tx::getData('view_pe_crs', [], $qQuery)->get();
        $response = [];
        foreach ($resp as $key => $val) {
            $dt = (array) $val;
            if (isset($request['query'])) {
                foreach ($request['query'] as $k => $v) {
                    foreach ($v as $var) {}
                }
            }
            $response[] = $dt;
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function viewUpdate($id)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Update Kasus Penyakit ' . $case,
            'data' => array(
                'id_trx_case' => $id,
            ),
        );
        return view('case.' . $index . '.update', $compact);
    }

    public function postCase($post = [])
    {
        if ($post) {
            $req = $post;
        } else {
            $req = Request::json()->all();
        }
        $dc = Helper::getCase(array('alias' => $this->index));
        $response = [];
        foreach ($req as $key => $request) {
            $vk = Validator::make($request['dk'], [
                'tgl_periksa' => 'required',
            ]);
            if ($vk->fails()) {
                $success = false;
                $response[] = array_merge($vk->errors()->all());
                $code = 400;
                Helper::session_flash('warning', 'Gagal', 'Data gagal disimpan');
            } else {
                // pasien
                $request['dp']['tgl_lahir'] = !empty($request['dp']['tgl_lahir']) ? Helper::dateFormat($request['dp']['tgl_lahir']) : null;
                if ($id_pasien = $request['id_pasien']) {
                    if (isset($request['dp']) && isset($request['df'])) {
                        Tx::updateData('ref_pasien', ['id' => $id_pasien], $request['dp']);
                        Tx::updateData('ref_family', ['id_pasien' => $id_pasien], $request['df']);
                    }
                    $id_family = ($id_family = Tx::getData('ref_family', ['id_pasien' => $id_pasien])->first()) ? $id_family->id : null;
                } else {
                    $id_pasien = Tx::insertData('ref_pasien', $request['dp']);
                    $request['df']['id_pasien'] = $id_pasien;
                    $id_family = Tx::insertData('ref_family', $request['df']);
                }
                $al = DB::table('ref_pasien AS a');
                $al->join('view_area AS b', 'a.code_kelurahan', '=', 'b.code_kelurahan');
                $al->select('b.code_kecamatan', 'b.code_kelurahan', 'b.code_kabupaten');
                $al->where('a.id', $id_pasien);
                $alamat_pasien = $al->first();

                $code_kabupaten_pasien = empty($alamat_pasien->code_kabupaten) ?:$alamat_pasien->code_kabupaten;
                $code_kecamatan_pasien = empty($alamat_pasien->code_kecamatan) ?:$alamat_pasien->code_kecamatan;
                $code_kelurahan_pasien = empty($alamat_pasien->code_kelurahan) ?:$alamat_pasien->code_kelurahan;
                // tx case
                $tgl_sakit = $request['dk']['tgl_periksa'];
                $epid = $this->getEpid(['tgl_sakit' => $tgl_sakit, 'id_kelurahan' => $code_kelurahan_pasien]);
                $no_epid = json_decode($epid->getContent())->response;
                $request['dc']['id_case'] = $dc[0]->id;
                $request['dc']['id_pasien'] = $id_pasien;
                if (empty($request['id_trx_case'])) {
                    $request['dc']['no_epid'] = $no_epid;
                }
                $request['dc']['klasifikasi_final'] = !empty($request['dc']['klasifikasi_final']) ? $request['dc']['klasifikasi_final'] : null;
                $request['dc']['desc_klasifikasi_final'] = !empty($request['dc']['desc_klasifikasi_final']) ? $request['dc']['desc_klasifikasi_final'] : null;
                (!empty($request['dc']['created_at'])) ? $request['dc']['created_at'] = date('Y-m-d H:i:s', strtotime($request['dc']['created_at'])) : null;
                (!empty($request['dc']['updated_at'])) ? $request['dc']['updated_at'] = date('Y-m-d H:i:s', strtotime($request['dc']['updated_at'])) : null;
                if ($id_trx_case = $request['id_trx_case']) {
                    Tx::updateData('trx_case', ['id' => $id_trx_case], $request['dc']);
                } else {
                    $id_trx_case = Tx::insertData('trx_case', $request['dc']);
                }
                // pelapor
                $request['dpel']['id_trx_case'] = $id_trx_case;
                $request['dpel']['tgl_laporan'] = !empty($request['dk']['tgl_laporan_diterima']) ? Helper::dateFormat($request['dk']['tgl_laporan_diterima']) : null;
                $request['dpel']['tgl_investigasi'] = !empty($request['dk']['tgl_pelacakan']) ? Helper::dateFormat($request['dk']['tgl_pelacakan']) : null;
                if ($request['id_trx_case']) {
                    Tx::updateData('trx_pelapor', ['id_trx_case' => $id_trx_case], $request['dpel']);
                } else {
                    $id_trx_pelapor = Tx::insertData('trx_pelapor', $request['dpel']);
                }
                // klinis
                $request['dk']['id_trx_case'] = $id_trx_case;
                $request['dk']['tgl_laporan_diterima'] = !empty($request['dk']['tgl_laporan_diterima']) ? Helper::dateFormat($request['dk']['tgl_laporan_diterima']) : null;
                $request['dk']['tgl_pelacakan'] = !empty($request['dk']['tgl_pelacakan']) ? Helper::dateFormat($request['dk']['tgl_pelacakan']) : null;
                $request['dk']['tgl_periksa'] = !empty($request['dk']['tgl_periksa']) ? Helper::dateFormat($request['dk']['tgl_periksa']) : null;
                if ($request['id_trx_case']) {
                    Tx::updateData('trx_klinis', ['id_trx_case' => $id_trx_case], $request['dk']);
                    $id_klinis = Tx::getData('trx_klinis', ['id_trx_case' => $id_trx_case])->first()->id;
                } else {
                    $id_klinis = Tx::insertData('trx_klinis', $request['dk']);
                }
                // gejala
                Tx::postDelete('trx_gejala', ['id_trx_klinis' => $id_klinis]);
                if (isset($request['dg'])) {
                    $dtGejala = $request['dg'];
                    $rGejala = [];
                    foreach ($dtGejala['gejala'] as $key => $val) {
                        $other_gejala = null;
                        if (isset($dtGejala['other_gejala'][$key])) {
                            $other_gejala = $dtGejala['other_gejala'][$key];
                        }
                        $rGejala[] = array(
                            'id_trx_klinis' => $id_klinis,
                            'id_gejala' => $key,
                            'val_gejala' => $val,
                            'other_gejala' => $other_gejala,
                        );
                    }
                    Tx::insertArray('trx_gejala', $rGejala);
                }
                // tx family
                $request['rf']['id_family'] = $id_family;
                $request['rf']['id_trx_case'] = $id_trx_case;
                $request['rf']['tgl_ibu_diagnosa_rubella'] = !empty($request['rf']['tgl_ibu_diagnosa_rubella']) ? Helper::dateFormat($request['rf']['tgl_ibu_diagnosa_rubella']) : null;
                if ($request['id_trx_case']) {
                    Tx::updateData('trx_klinis_family', ['id_trx_case' => $id_trx_case], $request['rf']);
                    $id_trx_klinis_family = Tx::getData('trx_klinis_family', ['id_trx_case' => $id_trx_case])->first()->id;
                } else {
                    $id_trx_klinis_family = Tx::insertData('trx_klinis_family', $request['rf']);
                }
                // data gejala family
                Tx::postDelete('trx_gejala_family', ['id_trx_klinis_family' => $id_trx_klinis_family]);
                if (isset($request['dgf'])) {
                    $dtGejalaFamily = $request['dgf'];
                    $rGejalaFamily = [];
                    foreach ($dtGejalaFamily['gejala'] as $key => $val) {
                        if ($val != '') {
                            $tglKejadian = !empty($dtGejalaFamily['tgl_kejadian'][$key]) ? Helper::dateFormat($dtGejalaFamily['tgl_kejadian'][$key]) : null;
                            $rGejalaFamily[] = array(
                                'id_trx_klinis_family' => $id_trx_klinis_family,
                                'id_family' => $id_family,
                                'id_gejala' => $key,
                                'val_gejala' => $val,
                                'tgl_kejadian' => $tglKejadian,
                            );
                        }
                    }
                    Tx::insertArray('trx_gejala_family', $rGejalaFamily);
                }
                // spesimen
                if (isset($request['ds'])) {
                    Tx::postDelete('trx_spesimen', ['id_trx_case' => $id_trx_case]);
                    $dtSpesimen = $request['ds'];
                    $rSpesimen = [];
                    foreach ($dtSpesimen['jenis_spesimen'] as $key => $val) {
                        $da = !empty($dtSpesimen['tgl_ambil_spesimen'][$key]) ? Helper::dateFormat($dtSpesimen['tgl_ambil_spesimen'][$key]) : null;
                        $db = !empty($dtSpesimen['tgl_kirim_lab'][$key]) ? Helper::dateFormat($dtSpesimen['tgl_kirim_lab'][$key]) : null;
                        $dct = !empty($dtSpesimen['tgl_terima_lab'][$key]) ? Helper::dateFormat($dtSpesimen['tgl_terima_lab'][$key]) : null;
                        $rSpesimen[] = array(
                            'id_trx_case' => $id_trx_case,
                            'jenis_spesimen' => $val,
                            'tgl_ambil_spesimen' => $da,
                            'tgl_kirim_lab' => $db,
                            'tgl_terima_lab' => $dct,
                            'case' => 'crs',
                        );
                    }
                    Tx::insertArray('trx_spesimen', $rSpesimen);
                }
                // hasil lab
                Tx::postDelete('trx_hasil_lab', ['id_trx_case' => $id_trx_case]);
                if (isset($request['dhs'])) {
                    $dtHasilLab = $request['dhs'];
                    $rHasilLab = [];
                    $type_faskes = Tx::getData('roles', ['id' => $request['dc']['id_role']])->first()->code;
                    foreach ($dtHasilLab['jenis_pemeriksaan'] as $key => $val) {
                        $dh = !empty($dtHasilLab['tgl_hasil'][$key]) ? Helper::dateFormat($dtHasilLab['tgl_hasil'][$key]) : null;
                        $rHasilLab[] = array(
                            'id_trx_case' => $id_trx_case,
                            'faskes' => $type_faskes,
                            'jenis_pemeriksaan' => $val,
                            'hasil' => $dtHasilLab['hasil'][$key],
                            'jenis_virus' => $dtHasilLab['jenis_virus'][$key],
                            'tgl_hasil' => $dh,
                            'kadar_igG' => $dtHasilLab['kadar_igG'][$key],
                            'case' => 'crs',
                        );
                    }
                    Tx::insertArray('trx_hasil_lab', $rHasilLab);
                }
                // notification
                Tx::postDelete('trx_notification', ['id_trx_case' => $id_trx_case]);
                $code_wilayah_faskes = $request['code_wilayah_faskes'];
                $dtNotification[] = array(
                    'type' => 'klb',
                    'id_trx_case' => $id_trx_case,
                    'from_faskes' => $code_wilayah_faskes,
                    'to_faskes' => $code_kecamatan_pasien,
                );
                if ($request['dc']['id_role'] == '1') {
                    // notif puskesmas
                    if ($code_kecamatan_pasien != $code_wilayah_faskes) {
                        $dtNotification[] = array(
                            'type' => 'cross',
                            'id_trx_case' => $id_trx_case,
                            'from_faskes' => $code_wilayah_faskes,
                            'to_faskes' => $code_kecamatan_pasien,
                        );
                    }
                } else if ($request['dc']['id_role'] == '2') {
                    if ($code_kabupaten_pasien != $code_wilayah_faskes) {
                        $dtNotification[] = array(
                            'type' => 'cross',
                            'id_trx_case' => $id_trx_case,
                            'from_faskes' => $code_wilayah_faskes,
                            'to_faskes' => $code_kabupaten_pasien,
                        );
                    }
                }
                Tx::insertArray('trx_notification', $dtNotification);
                $success = true;
                $code = 200;
                $trx_case = Tx::getData('trx_case', ['id' => $id_trx_case], ['select' => ['created_at', 'updated_at']])->first();
                $response[] = [
                    'id' => $id_trx_case,
                    'created_at' => date('d-m-Y H:i:s', strtotime($trx_case->created_at)),
                    'updated_at' => date('d-m-Y H:i:s', strtotime($trx_case->updated_at)),
                    'no_epid' => $no_epid,
                ];
                Helper::session_flash('success', 'Berhasil', 'Data berhasil disimpan');
            }
        }

        return Response::json(array(
            'success' => $success,
            'response' => $response,
        ), $code);
    }

    public function viewPe($id_trx_case)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Input data penyelidikan epidemologi kasus ' . $case,
            'data' => array(
                'id_trx_case' => $id_trx_case,
                'id_trx_pe' => null,
            ),
        );
        return view('case.' . $index . '.pe.index', $compact);
    }

    public function postPe($post = '')
    {
        if ($post) {
            $req = $post;
        } else {
            $req = Request::json()->all();
        }
        $response = [];
        foreach ($req as $key => $request) {
            $id_trx_case = $request['id_trx_case'];
            $id_trx_pe = $request['id_trx_pe'];
            if (empty($id_trx_case)) {
                $success = false;
                $code = 400;
                $response[] = ["error" => "id_trx_case is empty"];
                Helper::session_flash('warning', 'Gagal', 'Data gagal di simpan');
            } else {
                $request['dpe']['id_trx_case'] = $id_trx_case;
                (!empty($request['dpe']['created_at'])) ? $request['dpe']['created_at'] = date('Y-m-d H:i:s', strtotime($request['dpe']['created_at'])) : null;
                (!empty($request['dpe']['updated_at'])) ? $request['dpe']['updated_at'] = date('Y-m-d H:i:s', strtotime($request['dpe']['updated_at'])) : null;
                if ($id_trx_pe = $request['id_trx_pe']) {
                    Tx::updateData('trx_pe', ['id' => $id_trx_pe], $request['dpe']);
                } else {
                    $id_trx_pe = Tx::insertData('trx_pe', $request['dpe']);
                }

                $request['rf']['tgl_ibu_diagnosa_rubella'] = (isset($request['rf']['tgl_ibu_diagnosa_rubella'])) ? Helper::dateFormat($request['rf']['tgl_ibu_diagnosa_rubella']) : null;
                Tx::updateData('trx_klinis_family', ['id_trx_case' => $id_trx_case], $request['rf']);

                $success = true;
                $code = 200;
                $trx_pe = Tx::getData('trx_pe', ['id' => $id_trx_pe], ['select' => ['created_at', 'updated_at']])->first();
                $response[] = [
                    'id' => $id_trx_pe,
                    'created_at' => date('d-m-Y H:i:s', strtotime($trx_pe->created_at)),
                    'updated_at' => date('d-m-Y H:i:s', strtotime($trx_pe->updated_at)),
                ];
                Helper::session_flash('success', 'Berhasil', 'Data berhasil disimpan');
            }
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), $code);
    }

    public function moveCase($id = null)
    {
        if (!empty($id)) {
            $role = Helper::role();
            $case = Tx::getData('crs', ['id' => $id], ['select' => ['code_kelurahan_pasien', 'id_faskes']])->first();
            $code_kelurahan_pasien = $case->code_kelurahan_pasien;
            $code_faskes_old = $case->id_faskes;
            $code_faskes = Tx::getData('mst_wilayah_kerja_puskesmas', ['code_kelurahan' => $code_kelurahan_pasien], ['code_faskes'])->first();
            if (!empty($code_faskes)) {
                $faskes = Tx::getData('mst_puskesmas', ['code_faskes' => $code_faskes->code_faskes])->first();
                $dt = [
                    'id_faskes' => $faskes->id,
                    'id_faskes_old' => $code_faskes_old,
                    'id_role' => '1',
                    'id_role_old' => $role->id_role,
                ];
                $status = Tx::movedData('trx_case', ['id' => $id], $dt);
            } else {
                $status = false;
            }
        }
        return Response::json(array(
            'success' => $status,
        ), 200);
    }

    public function getDataPeCrs()
    {
        $request = Request::all();
        $param['roleUser'] = Helper::role();
        $param['query']['where'] = ['id_case' => 5];
        $d = Sql::getData("view_list_pe", $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            foreach ($request['columns'] as $key => $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->editColumn('name_pasien', function ($dt) {
            return ucwords($dt->name_pasien);
        });
        $q->editColumn('nama_ortu', function ($dt) {
            return ucwords($dt->nama_ortu);
        });
        $q->editColumn('no_epid', function ($dt) {
            $no_epid = $dt->no_epid;
            if ($dt->jenis_input == '1') {
                $no_epid .= '<br><span class="label label-info">Web</span>';
            } else if ($dt->jenis_input == '2') {
                $no_epid .= '<br><span class="label label-success">Android</span>';
            } else if ($dt->jenis_input == '3') {
                $no_epid .= '<br><span class="label label-warning">Import</span>';
            }
            return $no_epid;
        });
        $q->addColumn('action', function ($dt) {
            $roleUser = Helper::role();
            $action = "<div class='text-center'>";
            $action .= "<a href=" . url("case/crs/pe/detail/" . $dt->id_pe) . " title='Detail data' data-toggle='tooltip' class='btn btn-flat btn-sm btn-success'><i class='fa fa-asterisk'></i></a>";
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'kabupaten'])) {
                $action .= "<a href=" . url("case/crs/pe/update/" . $dt->id_pe) . " title='Edit data' data-toggle='tooltip' class='btn btn-flat btn-sm btn-warning'><i class='fa fa-pencil'></i></a>";
            };
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'provinsi', 'pusat'])) {
                $action .= '<a action="' . url('case/crs/pe/delete/' . $dt->id_pe) . '" title="Delete data"  class="btn btn-flat btn-sm btn-danger delete" data-toggle="modal" data-target=".modaldelete"><i class="fa fa-remove"></i></a>';
            };
            $action .= '</div>';
            return $action;
        });
        return $q->make(true);
    }

    public function viewDetailPe($id_trx_pe)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $response = $this->getDetailPe($id_trx_pe);
        $dt = json_decode($response->getContent());
        $data = $dt->response[0];
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Detail Pasien Kasus Penyelidikan Epidemologi Penyakit ' . $case,
            'data' => array(
                'index' => $index,
                'response' => $data,
            ),
        );
        return view('case.' . $index . '.pe.detail', $compact);
    }

    public function postDeletePe($id = null)
    {
        $rId = Request::json()->all();
        if ($id != null) {
            $rId['query']['delete'][] = $id;
        }
        $response = [];
        foreach ($rId['query']['delete'] as $key => $val) {
            $response[$val] = Tx::deleteData('trx_pe', ['id' => $val]);
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function viewUpdatePe($id_pe)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $cc = Tx::getData('crs', ['id_pe' => $id_pe], ['select' => ['id']])->first();
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Update data penyelidikan epidemologi kasus ' . $case,
            'data' => array(
                'id_trx_pe' => $id_pe,
                'id_trx_case' => $cc->id,
            ),
        );
        return view('case.' . $index . '.pe.index', $compact);
    }

    public function getAnalisa($id = '')
    {
        $request = Request::json()->all();
        $index = $this->index;
        if (!empty($request['range'])) {
            $dt = $request['from'];
            if (empty($dt['month'])) {
                $dt['month'] = 0;
            }if (empty($dt['day'])) {
                $dt['day'] = 0;
            }
            $rangeFrom = \Carbon\Carbon::create($dt['year'], $dt['month'], $dt['day']);
            $dt = $request['to'];
            if (empty($dt['month'])) {
                $dt['month'] = 0;
            }if (empty($dt['day'])) {
                $dt['day'] = 0;
            }
            $rangeTo = \Carbon\Carbon::create($dt['year'], $dt['month'], $dt['day']);
            $range = [$rangeFrom, $rangeTo];
            $data['lol'] = $range;
            // $dt = TxCase::getDataAnalisa($request['filter'],$index);
            $dt = TxCase::getDataAnalisaTime($request['filter'], $index, 'tgl_periksa', $range);
        } else {
            $dt = TxCase::getDataAnalisa($request['filter'], $index);
        }
        if (!empty($dt)) {
            // ---getdata for jenis kelamin graph---
            $kelamin = collect($dt)->groupBy('jenis_kelamin');
            foreach ($kelamin as $key => $val) {
                if ($key == 'L') {
                    $data['jenis_kelamin'][] = ['Laki-laki', count($val)];
                } else if ($key == "P") {
                    $data['jenis_kelamin'][] = ['Perempuan', count($val)];
                } else {
                    $data['jenis_kelamin'][] = ['Tidak Jelas', count($val)];
                    $data['umur'][] = [0, 0, 0, 0, 0];
                }
            }

            // ---getdata for umur graph---
            $umur = collect($dt)->groupBy('umur_bln')->sortBy('umur_bln');
            $tempUmur['name'] = 'Umur';
            $tempUmur['colorByPoint'] = false;
            foreach ($umur as $key => $val) {
                if ($key < 6) {
                    $tmpe[0][] = $val;
                } else if ($key >= 6) {
                    $tmpe[1][] = $val;
                }
            }
            for ($i = 0; $i < 2; $i++) {
                if (!empty($tmpe[$i])) {
                    $tempUmur['data'][$i] = count($tmpe[$i][0]);
                } else {
                    $tempUmur['data'][$i] = 0;
                }
            }
            $data['umur'][] = $tempUmur;

            // ---getdata for klasifikasi_final graph---
            $klafinal = collect($dt)->groupBy('klasifikasi_final');
            foreach ($klafinal as $key => $val) {
                if ($key == "") {
                    $data['klasifikasi_final'][] = ['Belum di isi', count($val)];
                } else if ($key == "1") {
                    $data['klasifikasi_final'][] = ['CRS Pasti(Lab Positif)', count($val)];
                } else if ($key == "2") {
                    $data['klasifikasi_final'][] = ['CRS Klinis', count($val)];
                } else if ($key == "3") {
                    $data['klasifikasi_final'][] = ['Bukan CRS', count($val)];
                } else if ($key == "4") {
                    $data['klasifikasi_final'][] = ['Suspek CRS', count($val)];
                }
            }
        } else {
            $data['jenis_kelamin'][] = ['Tidak Ada Data', 0];
            $data['klasifikasi_final'][] = ['Tidak Ada Data', 0];
            $data['umur'][] = array(
                'name' => 'Umur',
                'data' => [0, 0],
            );
        }
        $response = $data;
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getDataBasedAreaLab()
    {
        $request = Request::all();
        $param['query']['select'] = array('id_trx_case', 'no_epid', 'name_pasien', 'full_address', 'nama_ortu', 'thn_sakit', 'status_kasus', 'name_faskes', 'code_faskes');
        if (!empty($request['area'])) {
            $filter = json_decode($request['area']);
            $param['query']['whereIn'] = array('code_provinsi_pasien' => $filter);
        }
        $param['query']['where'] = ['id_case'=>5];
        $d = Sql::getData('view_list_case', $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            foreach ($request['columns'] as $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->addColumn('action', function ($dt) {
            $action = "<div class='btn-group'>";
            $action .= '<a href="crs/periksa/' . $dt->id_trx_case . '" title="Pemeriksaan Laboratorium" data-toggle="tooltip" class="btn btn-info">Tambah Pemeriksaan</a>';
            $action .= '</div>';
            return $action;
        });
        return $q->make(true);
    }

    public function _getDataBasedAreaLab()
    {
        $config['table'] = 'crs AS a';
        $config['coloumn'] = array('id', 'no_epid', 'name_pasien', 'full_address', 'nama_ortu', 'thn_tgl_sakit', 'name_faskes');
        $config['columnSelect'] = array('a.id', 'a.no_epid', 'a.name_pasien', 'a.nama_ortu', 'a.full_address', 'a.thn_tgl_sakit', 'a.name_faskes');
        $config['columnFormat'] = array('number', '', '', '', '', '', '');
        $config['searchColumn'] = array('a.name_pasien', 'a.no_epid');
        $config['key'] = 'a.id AS id';

        if (Request::get('area')) {
            $filter = json_decode(Request::get('area'));
            if (!empty($filter)) {
                $config['whereIn'] = array('a.code_provinsi_pasien' => $filter);
            }
        }
        if (Request::get('dt')) {
            $filter = json_decode(Request::get('dt'));
            // filter district
            if (isset($filter->filter)) {
                $district = $filter->filter;
                foreach ($district as $key => $val) {
                    if (!empty($val)) {
                        $config['where'][] = array('a.' . $key => $val);
                    }
                }
            }
            // filter time
            $time = $filter->range;
            if ($time) {
                $from = $filter->from;
                $fy = (isset($from->year)) ? $from->year : null;
                $fm = (isset($from->month)) ? $from->month : null;
                $fd = (isset($from->day)) ? $from->day : null;
                $to = $filter->to;
                $ty = (isset($to->year)) ? $to->year : null;
                $tm = (isset($to->month)) ? $to->month : null;
                $td = (isset($to->day)) ? $to->day : null;
                if ($time == '1') {
                    $start = $fy . '-' . $fm . '-' . $fd;
                    $to = $ty . '-' . $tm . '-' . $td;
                    if ($fy && $fm && $fd && $ty && $tm && $td) {
                        $config['whereBetween'] = array('a.tgl_sakit' => array($start, $to));
                    }
                } else if ($time == '2') {
                    $start = $fy . '-' . $fm . '-' . '1';
                    $to = $ty . '-' . $tm . '-' . date('t', mktime(0, 0, 0, $tm, 1, $ty));
                    if ($fy && $fm && $ty && $tm) {
                        $config['whereBetween'] = array('a.tgl_sakit' => array($start, $to));
                    }
                } else if ($time == '3') {
                    $start = $fy;
                    $to = $ty;
                    if ($fy && $ty) {
                        $config['whereBetween'] = array('a.thn_tgl_sakit' => array($start, $to));
                    }
                }
            }
        }

        $Datatable = Datatables::getData($config);
        if (count($Datatable['data']) > 0) {
            foreach ($Datatable['data'] as $key => $val) {
                $no_epid = $val['no_epid'];
                $name = $val['name_pasien'];
                $aksi = '<a href="crs/periksa/' . $val['id'] . '" title="Pemeriksaan Laboratorium" data-toggle="tooltip" class="btn btn-info">Tambah Pemeriksaan</a>';
                $dSampel = TxSpesimen::getData(array('id_trx_case' => $val['id']));
                $status = (!empty($dSampel)) ? "Sudah Periksa" : "Belum Periksa";
                $dt[] = array(
                    'no' => $val['no'],
                    'id' => $val['id'],
                    'no_epid' => $no_epid,
                    'name_pasien' => $name,
                    'nama_ortu' => $val['nama_ortu'],
                    'alamat' => $val['full_address'],
                    'rujukan' => $val['name_faskes'],
                    'status' => $status,
                    'action' => $aksi,
                );
            }
            $Datatable['data'] = $dt;
        }
        echo json_encode($Datatable);
    }

    public function getDataSampel()
    {
        $config['table'] = 'view_lab_crs AS a';
        $config['coloumn'] = array('id_spesimen', 'no_epid', 'no_epid_klb', 'name', 'umur_thn', 'umur_bln', 'umur_hari', 'jenis_kelamin_txt', 'keadaan_akhir_txt', 'kabupaten_name', 'hasil_igm_serum1', 'hasil_igm_serum2', 'hasil_igg_serum1', 'hasil_igg_serum2', 'hasil_isolasi', 'tgl_ambil_serum1', 'tgl_ambil_serum2', 'tgl_ambil_throat_swab', 'tgl_ambil_urin');
        $config['columnSelect'] = array('a.id_spesimen', 'a.no_epid', 'a.no_epid_klb', 'a.name', 'a.umur_thn', 'a.umur_bln', 'a.umur_hari', 'a.jenis_kelamin_txt', 'a.keadaan_akhir_txt', 'a.kabupaten_name', 'a.hasil_igm_serum1', 'a.hasil_igm_serum2', 'a.hasil_igg_serum1', 'a.hasil_igg_serum2', 'a.hasil_isolasi', 'a.tgl_ambil_serum1', 'a.tgl_ambil_serum2', 'a.tgl_ambil_throat_swab', 'a.tgl_ambil_urin');
        $config['columnFormat'] = array('number', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
        $config['searchColumn'] = array('a.name', 'a.no_epid');
        $config['key'] = 'a.id_spesimen AS id';
        $config['action'][] = array(
            'action' => 'detail',
            'url' => url('lab/case/afp/detail'),
            'key' => 'id',
        );
        $config['action'][] = array(
            'action' => 'edit',
            'url' => url('lab/case/afp/update'),
            'key' => 'id',
        );
        $config['action'][] = array(
            'action' => 'delete',
            'url' => url('lab/case/afp/delete'),
            'key' => 'id',
        );

        if (Request::get('area')) {
            $filter = json_decode(Request::get('area'));
            if (!empty($filter)) {
                $config['whereIn'] = array('a.code_provinsi_pasien' => $filter);
            }
        }
        if (Request::get('dt')) {
            $filter = json_decode(Request::get('dt'));
            // filter district
            if (isset($filter->district)) {
                $district = $filter->district;
                foreach ($district as $key => $val) {
                    if (!empty($val)) {
                        $config['where'][] = array('a.' . $key => $val);
                    }
                }
            }
            // filter time
            $time = $filter->filter->range;
            if ($time) {
                $from = $filter->from;
                $fy = (isset($from->year)) ? $from->year : null;
                $fm = (isset($from->month)) ? $from->month : null;
                $fd = (isset($from->day)) ? $from->day : null;
                $to = $filter->to;
                $ty = (isset($to->year)) ? $to->year : null;
                $tm = (isset($to->month)) ? $to->month : null;
                $td = (isset($to->day)) ? $to->day : null;
                if ($time == '1') {
                    $start = $fy . '-' . $fm . '-' . $fd;
                    $to = $ty . '-' . $tm . '-' . $td;
                    if ($fy && $fm && $fd && $ty && $tm && $td) {
                        $config['whereBetween'] = array('a.tgl_sakit' => array($start, $to));
                    }
                } else if ($time == '2') {
                    $start = $fy . '-' . $fm . '-' . '1';
                    $to = $ty . '-' . $tm . '-' . date('t', mktime(0, 0, 0, $tm, 1, $ty));
                    if ($fy && $fm && $ty && $tm) {
                        $config['whereBetween'] = array('a.tgl_sakit' => array($start, $to));
                    }
                } else if ($time == '3') {
                    $start = $fy;
                    $to = $ty;
                    if ($fy && $ty) {
                        $config['whereBetween'] = array('a.thn_tgl_sakit' => array($start, $to));
                    }
                }
            }
        }
        $Datatable = Datatables::getData($config);
        // $lol = $Datatable['data'];
        // foreach ($Datatable['data'] as $key => $value) {
        //     $lol[]=$value;
        // }

        $daftarsampel = collect($Datatable['data'])->groupBy('no_epid');
        $hasil = '';
        $jenis_pemeriksaan = '';
        $jenis_sampel = '';
        $tgl_periksa = '';
        $no = 1;

        if (count($Datatable['data']) > 0) {
            // foreach ($Datatable['data'] as $key => $val) {
            foreach ($daftarsampel as $keys => $value) {
                $umur_thn = (!empty($value[0]['umur_thn'])) ? $value[0]['umur_thn'] . ' Th ' : null;
                $umur_bln = (!empty($value[0]['umur_bln'])) ? $value[0]['umur_bln'] . ' Bln ' : null;
                $umur_hari = (!empty($value[0]['umur_hari'])) ? $value[0]['umur_hari'] . ' Hari ' : null;
                $no_epid = (empty($value[0]['no_epid_klb'])) ? $value[0]['no_epid'] : $value[0]['no_epid_klb'];
                $name = $value[0]['name'];
                // // if ($val['status_kasus']==1) {
                // //     $name=$val['name_pasien'].'<br><span class="label label-danger">Index</span>';
                // // }
                foreach ($value as $key => $val) {
                    if (!empty($val['hasil_igm_serum1'])) {
                        $hasil .= 'Igm Serum 1 : ' . ($val['hasil_igm_serum1'] == '1') ? 'Positif' : 'Negativ' . '<br>';
                        $jenis_pemeriksaan .= 'Igm Serum 1' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= $val['tgl_ambil_serum1'] . '<br>';
                    }
                    if (!empty($val['hasil_igm_serum2'])) {
                        $hasil .= 'Igm Serum 2 : ' . ($val['hasil_igm_serum2'] == '1') ? 'Positif' : 'Negativ' . '<br>';
                        $jenis_pemeriksaan .= 'Igm Serum 2' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= $val['tgl_ambil_serum2'] . '<br>';
                    }
                    if (!empty($val['hasil_igg_serum1'])) {
                        $hasil .= 'Igg Serum 1 : ' . ($val['hasil_igg_serum1'] == '1') ? 'Positif' : 'Negativ' . '<br>';
                        $jenis_pemeriksaan .= 'Igg Serum 1' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= $val['tgl_ambil_serum1'] . '<br>';
                    }
                    if (!empty($val['hasil_igg_serum2'])) {
                        $hasil .= 'Igg Serum 2 : ' . ($val['hasil_igg_serum2'] == '1') ? 'Positif' : 'Negativ' . '<br>';
                        $jenis_pemeriksaan .= 'Igg Serum 2' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= $val['tgl_ambil_serum2'] . '<br>';
                    }
                    if (!empty($val['tgl_ambil_urin'])) {
                        $hasil .= 'Tes Urin : ' . '-' . '<br>';
                        $jenis_pemeriksaan .= 'Tes Urin' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= $val['tgl_ambil_urin'] . '<br>';
                    }
                    if (!empty($val['tgl_ambil_throat_swab'])) {
                        $hasil .= 'Throat Swab : ' . '-' . '<br>';
                        $jenis_pemeriksaan .= 'Throat Swab ' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= $val['tgl_ambil_throat_swab'] . '<br>';
                    }
                    if (!empty($val['hasil_isolasi  '])) {
                        $hasil .= 'Isolasi : ' . ($val['hasil_isolasi'] == '1') ? 'Positif' : 'Negativ' . '<br>';
                        $jenis_pemeriksaan .= '-' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= '-' . '<br>';
                    }
                }
                $dt[] = array(
                    'no' => $no,
                    'id' => $value[0]['id_spesimen'],
                    'no_epid' => $no_epid,
                    'name_pasien' => $name,
                    'umur_thn' => $umur_thn,
                    'umur_bln' => $umur_bln,
                    'umur_hari' => $umur_hari,
                    'jenis_kelamin' => $value[0]['jenis_kelamin_txt'],
                    'kabupaten' => $value[0]['kabupaten_name'],
                    'jenis_pemeriksaan' => $jenis_pemeriksaan,
                    'jenis_sampel' => $jenis_sampel,
                    'tgl_periksa' => $tgl_periksa,
                    'hasil' => $hasil,
                    'keadaan_akhir_txt' => $value[0]['keadaan_akhir_txt'],
                    'action' => $value[0]['action'],
                );
                //flush data
                $hasil = '';
                $jenis_pemeriksaan = '';
                $jenis_sampel = '';
                $tgl_periksa = '';
                $no++;
            }
            $Datatable['data'] = $dt;

        }
        // echo json_encode($lol);
        echo json_encode($Datatable);
    }
}

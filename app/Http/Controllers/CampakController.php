<?php

namespace App\Http\Controllers;

use App\Models\Sql;
use App\Models\Tx;
use App\Models\TxCase;
use App\Models\TxSampelLab;
use App\Models\Wilayah;
use Carbon\Carbon;
use Datatables;
use DB;
use Excel;
use Helper;
use Request;
use Response;
use Validator;

class CampakController extends Controller
{
    protected $index;
    public function __construct()
    {
        $this->index = 'campak';
    }

    public function indexLab()
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Laboratorium Penyakit ' . $dc[0]->name,
            'data' => array(
                'id_trx_case' => null,
                'index' => $index,
            ),
        );
        return view('lab.case.' . $index . '.index', $compact);
    }

    public function index()
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        if (Helper::role()->id_role == 1) {
            $name_faskes = Helper::role()->name_role . ' ' . Helper::role()->name_faskes;
        } else {
            $name_faskes = Helper::role()->name_faskes;
        }

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Penyakit ' . $dc[0]->name,
            'data' => array(
                'id_trx_case' => null,
                'index' => $index,
            ),
            'content' => [
                'name_faskes' => $name_faskes,
                'id_faskes' => Helper::role()->id_faskes,
                'id_role' => Helper::role()->id_role
            ]
        );
        return view('case.' . $index . '.index', $compact);
    }

    public function exportExampleImportKasus()
    {
        $file = public_path() . "/download/campak/template import campak.xls";
        return Response::download($file, 'Contoh format import kasus Campak.xls');
    }

    public function viewAnalisa()
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Analisa Penyakit ' . $dc[0]->name,
            'data' => array(
                'id_trx_case' => null,
                'index' => $index,
            ),
        );
        return view('case.' . $index . '.analisa_mobile', $compact);
    }

    public function export($request = '')
    {
        $param['filter'] = json_decode($request);
        if (!empty($param['filter']->code_role)) {
            $param['roleUser'] = (object) array(
                'code_role' => $param['filter']->code_role,
                'id_faskes' => $param['filter']->id_faskes,
                'code_faskes' => $param['filter']->code_faskes,
            );
        }
        $q = Sql::getData("campak", $param);
        $q->join('campak_pe AS b', 'a.id_trx_case', '=', 'b.id_trx_case', 'left');
        $q->groupBy('a.id_trx_case');
        $r = $q->get();
        $compact['dd'] = $r;
        // return view('case.campak.export', $compact);
        $export = view('case.campak.export', $compact);
        $filename = 'export_campak-' . date('YmdHi') . '.xls';

        header("Content-type: application/vnd.ms-excel; name='excel'");
        header("Cache-Control: max-age=0");
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Content-Transfer-Encoding: text');
        echo $export;
    }

    public function importCase()
    {
        $path = Request::file('import');
        if (!empty($path)) {
            $data = Excel::load($path, function ($reader) {
            })->get();
            $role = Helper::role();
            $row = array();
            if (!empty($data) && $data->count()) {
                foreach ($data->toArray() as $key => $val) {
                    $cd = DB::table('view_list_case')->where(['id_case' => 1, 'name_pasien' => $val['nama_pasien'], 'code_kelurahan_pasien' => $val['wilayah'], 'tgl_sakit_date' => Helper::dateFormat($val['tgl_mulai_rash']), 'nik' => $val['nik']])->count();
                    if ($cd == 0) {
                        $ut = $val['thn'];
                        $ub = $val['bln'];
                        $uh = $val['hari'];
                        if ($val['tanggal_lahir'] && $val['tgl_mulai_rash']) {
                            $age = Helper::getAge(['tgl_sakit' => Helper::dateFormat($val['tgl_mulai_rash']), 'tgl_lahir' => Helper::dateFormat($val['tanggal_lahir'])]);
                            $ut = $age['years'];
                            $ub = $age['month'];
                            $uh = $age['day'];
                        }
                        $wilayah = DB::table('view_area')->where(['code_kelurahan' => $val['wilayah']])->first();
                        $cekpasien = DB::table('ref_pasien')->where(['name' => $val['nama_pasien'], 'nik' => $val['nik'], 'code_kelurahan' => $val['wilayah']])->first();
                        $row[] = [
                            'dc' => [
                                'id_faskes' => $role->id_faskes,
                                'id_role' => $role->id_role,
                                'jenis_input' => '3',
                                'no_rm' => $val['norm'],
                                'no_epid_lama' => $val['noepid_lama'],
                            ],
                            'code_wilayah_faskes' => $role->code_wilayah_faskes,
                            'id_trx_case' => '',
                            'dp' => [
                                "name" => $val['nama_pasien'],
                                "nik" => $val['nik'],
                                "agama" => $val['agama'],
                                "jenis_kelamin" => $val['jenis_kelamin'],
                                "tgl_lahir" => $val['tanggal_lahir'],
                                "umur_thn" => $ut,
                                "umur_bln" => $ub,
                                "umur_hari" => $uh,
                                "alamat" => $val['alamat'],
                                "code_kelurahan" => $val['wilayah'],
                                "code_kecamatan" => (!empty($wilayah)) ? $wilayah->code_kecamatan : null,
                                "code_kabupaten" => (!empty($wilayah)) ? $wilayah->code_kabupaten : null,
                                "code_provinsi" => (!empty($wilayah)) ? $wilayah->code_provinsi : null,
                            ],
                            'id_pasien' => ($cekpasien) ? $cekpasien->id : '',
                            'df' => [
                                'name' => $val['nama_orang_tua'],
                            ],
                            'dk' => [
                                "tgl_imunisasi_campak" => $val['tgl_imunisasi_campak_terakhir'],
                                "jml_imunisasi_campak" => $val['jml_imunisasi_campak'],
                                "tgl_mulai_demam" => $val['tgl_mulai_demam'],
                                "tgl_mulai_rash" => $val['tgl_mulai_rash'],
                                "tgl_laporan_diterima" => $val['tgl_laporan_diterima'],
                                "tgl_pelacakan" => $val['tgl_pelacakan'],
                                "vitamin_A" => $val['vitamin_a'],
                                "keadaan_akhir" => $val['keadaan_akhir'],
                                "jenis_kasus" => $val['jenis_kasus'],
                                "klb_ke" => $val['klb_ke'],
                                "status_kasus" => $val['status_kasus'],
                            ],
                            'dg' => [
                                'gejala' => [
                                    ($val['mata_merah']) ? '1' : null,
                                    ($val['batuk']) ? '2' : null,
                                    ($val['pilek']) ? '3' : null,
                                ],
                                'tgl_gejala' => [
                                    ($val['mata_merah']) ? $val['tgl_mata_merah'] : null,
                                    ($val['batuk']) ? $val['tgl_batuk'] : null,
                                    ($val['pilek']) ? $val['tgl_pilek'] : null,
                                ],
                            ],
                            'dkom' => [
                                'komplikasi' => [
                                    ($val['diare']) ? 'Diare' : null,
                                    ($val['pneumonia']) ? 'Pneumonia' : null,
                                    ($val['bronchopneumonia']) ? 'Bronchopneumonia' : null,
                                    ($val['oma']) ? 'OMA' : null,
                                    ($val['ensefalitis']) ? 'Ensefalitis' : null,
                                ],
                                'otherKomplikasi' => $val['lain_lain'],
                            ],
                        ];
                    }
                }
            }
            if ($row) {
                $campak = $this->postCase($row);
                $response = json_decode($campak->getContent())->response;
            }
            echo true;
        }
    }

    public function getDataCampak()
    {
        $request = Request::all();
        $param['filter'] = json_decode($request['dt']);
        $param['roleUser'] = Helper::role();
        $param['query']['select'] = ['id_trx_case', 'no_epid', 'no_epid_klb', 'name_pasien', 'umur_hari', 'umur_bln', 'umur_thn', 'code_kecamatan_pasien', 'full_address', 'keadaan_akhir_txt', 'klasifikasi_final_txt', 'id_role', 'name_faskes', 'code_kecamatan_faskes', 'jenis_input', 'status_kasus', 'id_pe', 'id_faskes_old', 'id_role_old', 'tgl_input', 'tgl_update', 'jenis_input_text', 'status_kasus_txt', 'pe_text'];
        $param['query']['where'] = ['id_case' => 1];
        $d = Sql::getData("view_list_case", $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            foreach ($request['columns'] as $key => $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->editColumn('name_pasien', function ($dt) {
            $name_pasien = ucwords($dt->name_pasien);
            if ($dt->status_kasus == '1') {
                $name_pasien .= '<br><span class="label label-danger">Index</span>';
            }
            return $name_pasien;
        });
        $q->editColumn('no_epid', function ($dt) {
            $no_epid = ($dt->no_epid_klb) ? $dt->no_epid_klb : $dt->no_epid;
            if ($dt->jenis_input == '1') {
                $no_epid .= '<br><span class="label label-info">Web</span>';
            } else if ($dt->jenis_input == '2') {
                $no_epid .= '<br><span class="label label-success">Android</span>';
            } else if ($dt->jenis_input == '3') {
                $no_epid .= '<br><span class="label label-warning">Import</span>';
            }
            if ($dt->id_role == '2') {
                $no_epid .= "<span class='label label-primary' style='background-color:#b503b5 !important;'> Data dari rumahsakit </span>";
            }
            if ($dt->id_faskes_old) {
                $fo = '';
                if ($dt->id_role_old == '1') {
                    $fo = 'PUSKESMAS ' . DB::table('mst_puskesmas')->select(['name'])->where('id', $dt->id_faskes_old)->first()->name;
                } else if ($dt->id_role_old == '2') {
                    $fo = DB::table('mst_rumahsakit')->select(['name'])->where('id', $dt->id_faskes_old)->first()->name;
                }
                $no_epid .= "<span class='label label-primary'> Data kiriman dari " . $fo . "</span>";
            }
            return $no_epid;
        });
        $q->editColumn('id_pe', function ($dt) {
            $roleUser = Helper::role();
            $disabled = '';
            if (!in_array($roleUser->code_role, ['puskesmas', 'rs'])) {
                $disabled = "disabled='disabled' onclick='return false;'";
            }
            $pe = '<div class="text-center"><a href="' . url('case/campak/pe/' . $dt->id_trx_case) . '" ' . $disabled . ' title="Penyelidikan Epidemologi" data-toggle="tooltip" class="btn btn-sm btn-flat btn-success">PE</a></div>';
            if ($dt->id_pe) {
                $pe = '<div class="text-center"><a title="Penyelidikan Epidemologi" data-toggle="tooltip" class="btn btn-sm btn-flat btn-danger">Sudah PE</a></div>';
            }
            return $pe;
        });
        $q->addColumn('action', function ($dt) {
            $roleUser = Helper::role();
            $action = "<div class='text-center'>";
            $action .= "<a href=" . url("case/campak/detail/" . $dt->id_trx_case) . " title='Detail data' data-toggle='tooltip' class='btn btn-flat btn-sm btn-success'><i class='fa fa-asterisk'></i></a>";
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'kabupaten'])) {
                $action .= "<a href=" . url("case/campak/update/" . $dt->id_trx_case) . " title='Edit data' data-toggle='tooltip' class='btn btn-flat btn-sm btn-warning'><i class='fa fa-pencil'></i></a>";
            };
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'provinsi', 'pusat'])) {
                $action .= '<a action="' . url('case/campak/delete/' . $dt->id_trx_case) . '" title="Delete data"  class="btn btn-flat btn-sm btn-danger delete" data-toggle="modal" data-target=".modaldelete"><i class="fa fa-remove"></i></a>';
            };
            if (in_array($roleUser->code_role, ['puskesmas']) and $dt->id_role == '1') {
                if ($dt->code_kecamatan_faskes != $dt->code_kecamatan_pasien) {
                    $action .= '<a action="' . url('case/campak/move/' . $dt->id_trx_case) . '" title="Move data" data-toggle="modal" class="btn btn-flat btn-sm btn-primary move" data-target=".modalmove"><i class="fa fa-step-forward"></i></a>';
                }
            }
            $action .= '</div>';
            return $action;
        });
        return $q->make(true);
    }

    public function moveCase($id = null)
    {
        if (!empty($id)) {
            $role = Helper::role();
            $case = Tx::getData('campak', ['id' => $id], ['select' => ['code_kelurahan_pasien', 'id_faskes']])->first();
            $code_kelurahan_pasien = $case->code_kelurahan_pasien;
            $code_faskes_old = $case->id_faskes;
            $code_faskes = Tx::getData('mst_wilayah_kerja_puskesmas', ['code_kelurahan' => $code_kelurahan_pasien], ['code_faskes'])->first();
            if (!empty($code_faskes)) {
                $faskes = Tx::getData('mst_puskesmas', ['code_faskes' => $code_faskes->code_faskes])->first();
                $dt = [
                    'id_faskes' => $faskes->id,
                    'id_faskes_old' => $code_faskes_old,
                    'id_role' => '1',
                    'id_role_old' => $role->id_role,
                ];
                $status = Tx::movedData('trx_case', ['id' => $id], $dt);
            } else {
                $status = false;
            }
        }
        return Response::json(array(
            'success' => $status,
        ), 200);
    }

    public function getEpid($param = [])
    {
        if (!empty($param)) {
            $request = $param;
        } else {
            $request = Request::json()->all();
        }
        $case = Helper::getCase(array('alias' => $this->index));
        $code = $case[0]->code;
        $id_kelurahan = $request['id_kelurahan'];
        $thn_case = $cthn = '';
        $q = DB::table('view_list_case');
        $q->where('id_case', 1);
        $q->select(DB::raw('MAX(no_epid) AS no_epid'));
        if ($request['tgl_sakit']) {
            $thn_case = \Carbon\Carbon::parse($request['tgl_sakit'])->format('Y');
            $cthn = \Carbon\Carbon::parse($request['tgl_sakit'])->format('y');
            $q->where('thn_sakit', $thn_case);
        }
        $q->where('code_kelurahan_pasien', $id_kelurahan);
        $q->where('jenis_kasus', 2);
        $cekEpid = $q->first();

        if (!empty($cekEpid->no_epid)) {
            $cc = substr($cekEpid->no_epid, -3);
            $no = str_pad($cc + 1, 3, 0, STR_PAD_LEFT);
        } else {
            $no = '001';
        }

        $no_epid = $code . $id_kelurahan . $cthn . $no;
        return Response::json(array(
            'response' => $no_epid,
        ));
    }

    public function getEpidKlb($param = [])
    {
        if (!empty($param)) {
            $request = $param;
        } else {
            $request = Request::json()->all();
        }
        $index = $this->index;
        $case = Helper::getCase(array('alias' => $index));
        $code = $case[0]->code;
        $id_kelurahan = $request['id_kelurahan'];
        $klb_ke = $request['klb_ke'];

        $q = DB::table('view_list_case');
        $q->where('id_case', 1);
        $q->select(DB::raw('MAX(no_epid_klb) AS no_epid_klb'));
        $thn_case = $cthn = '';
        if ($request['tgl_sakit']) {
            $thn_case = \Carbon\Carbon::parse(Helper::dateFormat($request['tgl_sakit']))->format('Y');
            $cthn = \Carbon\Carbon::parse($request['tgl_sakit'])->format('y');
            $q->where('thn_sakit', $thn_case);
        }
        $q->where('code_kelurahan_pasien', $id_kelurahan);
        $q->where('klb_ke', $klb_ke);
        $cekEpid = $q->first();

        if (!empty($cekEpid->no_epid_klb)) {
            $x = explode('/', $cekEpid->no_epid_klb);
            $cc = substr($x[0], -3);
            $no = str_pad($cc + 1, 3, 0, STR_PAD_LEFT);
        } else {
            $no = '001';
        }
        $no_epid = $code . $id_kelurahan . $cthn . $no . '/' . $klb_ke;
        return Response::json(array(
            'request' => $request,
            'response' => $no_epid,
        ));
    }

    public function postDelete($id = null)
    {
        $rId = Request::json()->all();
        if ($id != null) {
            $rId['query']['delete'][] = $id;
        }
        $response = [];
        foreach ($rId['query']['delete'] as $key => $val) {
            $response[$val] = Tx::deleteData('trx_case', ['id' => $val]);
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function deleteSpesimen()
    {
        $r = Request::json()->all();
        $response = DB::table('trx_spesimen')->where(['id' => $r['id']])->delete();
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function postInputLab()
    {
        $r = Request::json()->all();
        DB::beginTransaction();
        try {
            if (!empty($r['dh'])) {
                $d1 = $r['dh'];
                (empty($d1['tgl_terima_spesimen'])) ?: $d1['tgl_terima_spesimen'] = date('Y-m-d', strtotime($d1['tgl_terima_spesimen']));
                if (DB::table('trx_lab_campak')->select('id_trx_case')->where(['id_trx_case' => $d1['id_trx_case']])->first()) {
                    Tx::updateData('trx_lab_campak', ['id_trx_case' => $d1['id_trx_case']], $d1);
                } else {
                    Tx::insertData('trx_lab_campak', $d1);
                }
            }

            DB::commit();
            return Response::json(array(
                'success' => true,
            ), 200);
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function postHasilLab()
    {
        $r = Request::json()->all();
        DB::beginTransaction();
        try {
            if (!empty($r['dk'])) {
                $d1 = $r['dk'];
                $d1['id_trx_spesimen'] = $r['id'];
                (empty($d1['tgl_periksa_kultur'])) ?: $d1['tgl_periksa_kultur'] = date('Y-m-d', strtotime($d1['tgl_periksa_kultur']));
                (empty($d1['tgl_hasil_kultur_tersedia'])) ?: $d1['tgl_hasil_kultur_tersedia'] = date('Y-m-d', strtotime($d1['tgl_hasil_kultur_tersedia']));
                (empty($d1['tgl_hasil_dilaporkan_kultur'])) ?: $d1['tgl_hasil_dilaporkan_kultur'] = date('Y-m-d', strtotime($d1['tgl_hasil_dilaporkan_kultur']));
                if (DB::table('trx_pemeriksaan_kultur')->select('id_trx_spesimen')->where(['id_trx_spesimen' => $r['id']])->first()) {
                    Tx::updateData('trx_pemeriksaan_kultur', ['id_trx_spesimen' => $r['id']], $d1);
                } else {
                    Tx::insertData('trx_pemeriksaan_kultur', $d1);
                }
            }
            if (!empty($r['dp'])) {
                $d2 = $r['dp'];
                $d2['id_trx_spesimen'] = $r['id'];
                (empty($d2['tgl_periksa_pcr'])) ?: $d2['tgl_periksa_pcr'] = date('Y-m-d', strtotime($d2['tgl_periksa_pcr']));
                (empty($d2['tgl_keluar_hasil_pcr'])) ?: $d2['tgl_keluar_hasil_pcr'] = date('Y-m-d', strtotime($d2['tgl_keluar_hasil_pcr']));
                (empty($d2['tgl_hasil_dilaporkan_pcr'])) ?: $d2['tgl_hasil_dilaporkan_pcr'] = date('Y-m-d', strtotime($d2['tgl_hasil_dilaporkan_pcr']));
                if (DB::table('trx_pemeriksaan_pcr')->select('id_trx_spesimen')->where(['id_trx_spesimen' => $r['id']])->first()) {
                    Tx::updateData('trx_pemeriksaan_pcr', ['id_trx_spesimen' => $r['id']], $d2);
                } else {
                    Tx::insertData('trx_pemeriksaan_pcr', $d2);
                }
            }
            if (!empty($r['dp'])) {
                $d3 = $r['ds'];
                $d3['id_trx_spesimen'] = $r['id'];
                (empty($d3['tgl_dirujuk_sqc'])) ?: $d3['tgl_dirujuk_sqc'] = date('Y-m-d', strtotime($d3['tgl_dirujuk_sqc']));
                (empty($d3['tgl_pemeriksaan_sqc'])) ?: $d3['tgl_pemeriksaan_sqc'] = date('Y-m-d', strtotime($d3['tgl_pemeriksaan_sqc']));
                (empty($d3['tgl_hasil_sqc'])) ?: $d3['tgl_hasil_sqc'] = date('Y-m-d', strtotime($d3['tgl_hasil_sqc']));
                (empty($d3['tgl_hasil_kirim_darilab_sqc'])) ?: $d3['tgl_hasil_kirim_darilab_sqc'] = date('Y-m-d', strtotime($d3['tgl_hasil_kirim_darilab_sqc']));
                (empty($d3['tgl_hasil_dilaporkan_sqc'])) ?: $d3['tgl_hasil_dilaporkan_sqc'] = date('Y-m-d', strtotime($d3['tgl_hasil_dilaporkan_sqc']));
                if (DB::table('trx_pemeriksaan_sequencing')->select('id_trx_spesimen')->where(['id_trx_spesimen' => $r['id']])->first()) {
                    Tx::updateData('trx_pemeriksaan_sequencing', ['id_trx_spesimen' => $r['id']], $d3);
                } else {
                    Tx::insertData('trx_pemeriksaan_sequencing', $d3);
                }
            }
            if (!empty($r['dsr'])) {
                $d4 = $r['dsr'];
                $d4['id_trx_spesimen'] = $r['id'];
                (empty($d4['tgl_periksa_igm_campak'])) ?: $d4['tgl_periksa_igm_campak'] = date('Y-m-d', strtotime($d4['tgl_periksa_igm_campak']));
                (empty($d4['tgl_hasil_tersedia_igm_campak'])) ?: $d4['tgl_hasil_tersedia_igm_campak'] = date('Y-m-d', strtotime($d4['tgl_hasil_tersedia_igm_campak']));
                (empty($d4['tgl_hasil_dilaporkan_igm_campak'])) ?: $d4['tgl_hasil_dilaporkan_igm_campak'] = date('Y-m-d', strtotime($d4['tgl_hasil_dilaporkan_igm_campak']));
                (empty($d4['tgl_periksa_igm_rubella'])) ?: $d4['tgl_periksa_igm_rubella'] = date('Y-m-d', strtotime($d4['tgl_periksa_igm_rubella']));
                (empty($d4['tgl_hasil_tersedia_igm_rubella'])) ?: $d4['tgl_hasil_tersedia_igm_rubella'] = date('Y-m-d', strtotime($d4['tgl_hasil_tersedia_igm_rubella']));
                (empty($d4['tgl_hasil_dilaporkan_igm_rubella'])) ?: $d4['tgl_hasil_dilaporkan_igm_rubella'] = date('Y-m-d', strtotime($d4['tgl_hasil_dilaporkan_igm_rubella']));
                if (DB::table('trx_pemeriksaan_igm')->select('id_trx_spesimen')->where(['id_trx_spesimen' => $r['id']])->first()) {
                    Tx::updateData('trx_pemeriksaan_igm', ['id_trx_spesimen' => $r['id']], $d4);
                } else {
                    Tx::insertData('trx_pemeriksaan_igm', $d4);
                }
            }
            DB::commit();
            return Response::json(array(
                'success' => true,
            ), 200);
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function postDeletePe($id = null)
    {
        $rId = Request::json()->all();
        if ($id != null) {
            $rId['query']['delete'][] = $id;
        }
        $response = [];
        foreach ($rId['query']['delete'] as $key => $val) {
            $response[$val] = Tx::deleteData('trx_pe', ['id' => $val]);
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function postDeleteSampel($id)
    {
        $request = array('id' => $id);
        if ($resp = TxSampelLab::postDeleteSampel($id)) {
            $success = true;
            $response = $resp;
            $code = '200';
        } else {
            $success = false;
            $response = $resp;
            $code = '401';
        }

        return Response::json(array(
            'success' => $success,
            'response' => $response,
        ), $code);
    }

    public function viewDetail($id)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $response = $this->getDetail($id);
        $dt = json_decode($response->getContent());
        $data = $dt->response[0];
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Detail Pasien Kasus Penyakit ' . $case,
            'data' => array(
                'index' => $index,
                'response' => $data,
            ),
        );
        return view('case.' . $index . '.detail', $compact);
    }

    public function getDetail($id = '')
    {
        if ($id) {
            $request = [
                'where' => ['id' => [$id]],
                'query' => ['select' => ['*', 'dtGejala', 'dtKomplikasi', 'dtSpesimen']],
            ];
        } else {
            $request = Request::json()->all();
        }
        $qQuery = [];
        if (!empty($request['where'])) {
            foreach ($request['where'] as $key => $val) {
                if ($val[0] == '*') {
                    $qQuery['where'][$key] = '*';
                } else {
                    $qQuery['whereIn'][$key] = $val;
                }
            }
            if (!empty($request['where']['code_roles'])) {
                if ($request['where']['code_roles'][0] == 'puskesmas') {
                    $c = [];
                    foreach ($request['where']['id_faskes'] as $key => $val) {
                        $codeKelurahanKerjaPkm = Tx::getData('view_wilayah_kerja_puskesmas', ['id_faskes' => $val], ['select' => ['code_kelurahan']])->get('code_kelurahan');
                        foreach ($codeKelurahanKerjaPkm as $k => $v) {
                            $c[] = $v->code_kelurahan;
                        }
                    }
                    $qQuery['orWhere']['whereIn'] = ['code_kelurahan_pasien' => $c, 'code_roles' => ['rs']];
                }
            }
        }
        if (!empty($request['query'])) {
            foreach ($request['query'] as $key => $val) {
                foreach ($val as $v) {
                    if (!in_array($v, ["dtGejala", "dtKomplikasi", "dtSpesimen"])) {
                        $var[] = $v;
                    }
                }
                $qQuery[$key] = $var;
            }
        }
        if (!empty($request['pagination'])) {
            $qQuery['pagination'] = $request['pagination'];
        }
        $resp = Tx::getData('campak', [], $qQuery)->get();
        $dResp = $response = [];
        foreach ($resp as $key => $val) {
            $dt = (array) $val;
            if (!empty($request['query']['select'])) {
                foreach ($request['query']['select'] as $k => $v) {
                    if ($v == 'dtGejala') {
                        $dt['dtGejala'] = [];
                        $d1 = explode('|', $val->gejala);
                        if (!empty($d1[0])) {
                            foreach ($d1 as $k1 => $v1) {
                                $d11 = explode('_', $v1);
                                $dt['dtGejala'][] = (object) [
                                    'id_gejala' => $d11[0],
                                    'name' => $d11[1],
                                    'tgl_kejadian' => $d11[2],
                                ];
                            }
                        }
                    }
                    if ($v == 'dtKomplikasi') {
                        $dt['dtKomplikasi'] = [];
                        $d2 = explode('|', $val->komplikasi);
                        if (!empty($d2[0])) {
                            foreach ($d2 as $k1 => $v1) {
                                if (!empty($v1)) {
                                    $dt['dtKomplikasi'][] = (object) [
                                        'name' => $v1,
                                        'other_komplikasi' => null
                                    ];
                                }
                            }
                        }
                        $d3 = explode('|', $val->other_komplikasi);
                        if (!empty($d3[0])) {
                            foreach ($d3 as $k1 => $v1) {
                                if (!empty($v1)) {
                                    $dt['dtKomplikasi'][] = (object) [
                                        'name' => null,
                                        'other_komplikasi' => $v1
                                    ];
                                }
                            }
                        }
                    }
                    if ($v == 'dtSpesimen') {
                        $dt['dtSpesimen'] = DB::table('campak_spesimen')->where(['id_trx_case' => $val->id])->get();
                    }
                }
            }
            $response[] = $dt;
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getDetailLab($id)
    {
        $response = DB::table('campak_lab')->where(['id_trx_case' => $id])->get();
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getDetailHasilLab($id)
    {
        $response = DB::table('campak_spesimen')->where(['id_trx_spesimen' => $id])->get();
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getDetailPe($id_pe = null)
    {
        if ($id_pe) {
            $request = [
                'where' => ['id' => [$id_pe]],
                'query' => ['select' => ['*', 'dtJmlKasusTambahan', 'dtPelaksana']],
            ];
        } else {
            $request = Request::json()->all();
        }
        $qQuery = [];
        if (!empty($request['where'])) {
            foreach ($request['where'] as $key => $val) {
                if ($val[0] == '*') {
                    $qQuery['where'][$key] = '*';
                } else {
                    $qQuery['whereIn'][$key] = $val;
                }
            }
        }
        if (!empty($request['query'])) {
            foreach ($request['query'] as $key => $val) {
                foreach ($val as $v) {
                    if (!in_array($v, ["dtJmlKasusTambahan", "dtPelaksana"])) {
                        $var[] = $v;
                    }
                }
                $qQuery[$key] = $var;
            }
        }
        if (!empty($request['pagination'])) {
            $qQuery['pagination'] = $request['pagination'];
        }
        $resp = Tx::getData('campak_pe', [], $qQuery)->get();
        $response = [];
        foreach ($resp as $key => $val) {
            $dt = (array) $val;
            if (!empty($request['query']['select'])) {
                foreach ($request['query']['select'] as $k => $v) {
                    if ($v == 'dtJmlKasusTambahan') {
                        $dt['dtJmlKasusTambahan'] = [];
                        $d1 = explode('|', $val->jml_kasus_tambahan);
                        if (!empty($d1[0])) {
                            foreach ($d1 as $k1 => $v1) {
                                $d11 = explode('_', $v1);
                                $dt['dtJmlKasusTambahan'][] = (object) [
                                    'lokasi' => $d11[0],
                                    'keterangan' => $d11[1],
                                    'tgl_pe' => $d11[2],
                                    'jml_kasus' => $d11[3],
                                ];
                            }
                        }
                    }
                    if ($v == 'dtPelaksana') {
                        $dt['dtPelaksana'] = [];
                        $d2 = explode(',', $val->nama_pelaksana);
                        if (!empty($d2[0])) {
                            foreach ($d2 as $k1 => $v1) {
                                if (!empty($v1)) {
                                    $dt['dtPelaksana'][] = (object) [
                                        'nama_pelaksana' => $v1
                                    ];
                                }
                            }
                        }
                    }
                }
            }
            $response[] = $dt;
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function viewUpdate($id)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Update Kasus Penyakit ' . $case,
            'data' => array(
                'id_trx_case' => $id,
            ),
        );
        return view('case.' . $index . '.update', $compact);
    }

    public function viewUpdateLab($id)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Update Kasus Penyakit ' . $case,
            'data' => array(
                'id_trx_case' => $id,
            ),
        );
        return view('lab.case.' . $index . '.update', $compact);
    }

    public function postCase($post = [])
    {
        if ($post) {
            $req = $post;
        } else {
            $req = Request::json()->all();
        }
        $dc = Helper::getCase(array('alias' => $this->index));
        $response = [];
        foreach ($req as $key => $request) {
            $vk = Validator::make($request['dk'], []);
            if ($vk->fails()) {
                $success = false;
                $response[] = array_merge($vk->errors()->all());
                $code = 400;
                Helper::session_flash('warning', 'Gagal', 'Data gagal disimpan');
            } else {
                $request['dp']['tgl_lahir'] = (!empty($request['dp']['tgl_lahir'])) ? Helper::dateFormat($request['dp']['tgl_lahir']) : null;
                if ($id_pasien = $request['id_pasien']) {
                    if (!empty($request['dp']) && !empty($request['df'])) {
                        Tx::updateData('ref_pasien', ['id' => $id_pasien], $request['dp']);
                        Tx::updateData('ref_family', ['id_pasien' => $id_pasien], $request['df']);
                    }
                    $id_family = Tx::getData('ref_family', ['id_pasien' => $id_pasien])->first()->id;
                } else {
                    $id_pasien = Tx::insertData('ref_pasien', $request['dp']);
                    $request['df']['id_pasien'] = $id_pasien;
                    $id_family = Tx::insertData('ref_family', $request['df']);
                }
                $al = DB::table('ref_pasien AS a');
                $al->join('view_area AS b', 'a.code_kelurahan', '=', 'b.code_kelurahan');
                $al->select('b.code_kecamatan', 'b.code_kelurahan', 'b.code_kabupaten');
                $al->where('a.id', $id_pasien);
                $alamat_pasien = $al->first();

                $code_kabupaten_pasien = empty($alamat_pasien->code_kabupaten) ?: $alamat_pasien->code_kabupaten;
                $code_kecamatan_pasien = empty($alamat_pasien->code_kecamatan) ?: $alamat_pasien->code_kecamatan;
                $code_kelurahan_pasien = empty($alamat_pasien->code_kelurahan) ?: $alamat_pasien->code_kelurahan;
                // tx case
                $tgl_sakit = (!empty($request['dk']['tgl_mulai_demam'])) ? $request['dk']['tgl_mulai_demam'] : $request['dk']['tgl_mulai_rash'];
                $epid = $this->getEpid(['tgl_sakit' => $tgl_sakit, 'id_kelurahan' => $code_kelurahan_pasien]);
                $no_epid = json_decode($epid->getContent())->response;
                $no_epid_klb = null;
                if (!empty($request['dk']['klb_ke'])) {
                    $epidKlb = $this->getEpidKlb(['tgl_sakit' => $tgl_sakit, 'id_kelurahan' => $code_kelurahan_pasien, 'klb_ke' => $request['dk']['klb_ke'], 'id_trx_case' => $request['id_trx_case']]);
                    $no_epid_klb = json_decode($epidKlb->getContent())->response;
                }
                $request['dc']['id_case'] = $dc[0]->id;
                $request['dc']['id_pasien'] = $id_pasien;
                if (empty($request['id_trx_case'])) {
                    $request['dc']['no_epid'] = $no_epid;
                    $request['dc']['no_epid_klb'] = $no_epid_klb;
                }
                $request['dc']['klasifikasi_final'] = !empty($request['dc']['klasifikasi_final']) ? $request['dc']['klasifikasi_final'] : null;
                (!empty($request['dc']['created_at'])) ? $request['dc']['created_at'] = date('Y-m-d H:i:s', strtotime($request['dc']['created_at'])) : null;
                (!empty($request['dc']['updated_at'])) ? $request['dc']['updated_at'] = date('Y-m-d H:i:s', strtotime($request['dc']['updated_at'])) : null;
                if ($id_trx_case = $request['id_trx_case']) {
                    Tx::updateData('trx_case', ['id' => $request['id_trx_case']], $request['dc']);
                } else {
                    $id_trx_case = Tx::insertData('trx_case', $request['dc']);
                }
                // klinis
                $request['dk']['id_trx_case'] = $id_trx_case;
                $request['dk']['tgl_imunisasi_campak'] = (!empty($request['dk']['tgl_imunisasi_campak'])) ? Helper::dateFormat($request['dk']['tgl_imunisasi_campak']) : null;
                $request['dk']['tgl_mulai_demam'] = (!empty($request['dk']['tgl_mulai_demam'])) ? Helper::dateFormat($request['dk']['tgl_mulai_demam']) : null;
                $request['dk']['tgl_mulai_rash'] = (!empty($request['dk']['tgl_mulai_rash'])) ? Helper::dateFormat($request['dk']['tgl_mulai_rash']) : null;
                $request['dk']['tgl_laporan_diterima'] = (!empty($request['dk']['tgl_laporan_diterima'])) ? Helper::dateFormat($request['dk']['tgl_laporan_diterima']) : null;
                $request['dk']['tgl_pelacakan'] = (!empty($request['dk']['tgl_pelacakan'])) ? Helper::dateFormat($request['dk']['tgl_pelacakan']) : null;
                if ($request['id_trx_case']) {
                    Tx::updateData('trx_klinis', ['id_trx_case' => $id_trx_case], $request['dk']);
                    $id_klinis = Tx::getData('trx_klinis', ['id_trx_case' => $id_trx_case])->first()->id;
                } else {
                    $id_klinis = Tx::insertData('trx_klinis', $request['dk']);
                }
                // komplikasi
                if (!empty($request['dkom']['komplikasi'][0])) {
                    $dtKomplikasi = $request['dkom'];
                    $rKomplikasi = [];
                    if (!empty($dtKomplikasi['komplikasi'])) {
                        foreach ($dtKomplikasi['komplikasi'] as $k => $v) {
                            $rKomplikasi[] = [
                                'id_trx_klinis' => $id_klinis,
                                'name' => $v,
                                'other_komplikasi' => null,
                            ];
                        }
                    }
                    if (!empty($dtKomplikasi['otherKomplikasi'])) {
                        $dko = explode(',', $dtKomplikasi['otherKomplikasi']);
                        foreach ($dko as $k => $v) {
                            if (trim($v) != '') {
                                $rKomplikasi[] = [
                                    'id_trx_klinis' => $id_klinis,
                                    'name' => null,
                                    'other_komplikasi' => $v,
                                ];
                            }
                        }
                    }
                    $komplikasi = Tx::truncateInsert('trx_komplikasi', ['id_trx_klinis' => $id_klinis], $rKomplikasi);
                }
                // gejala
                if (!empty($request['dg']['gejala'][0])) {
                    $dtGejala = $request['dg'];
                    $rGejala = [];
                    foreach ($dtGejala['gejala'] as $k => $v) {
                        $rGejala[] = array(
                            'id_trx_klinis' => $id_klinis,
                            'id_gejala' => $v,
                            'tgl_kejadian' => !empty($dtGejala['tgl_gejala'][$k]) ? Helper::dateFormat($dtGejala['tgl_gejala'][$k]) : null,
                        );
                    }
                    $gejala = Tx::truncateInsert('trx_gejala', ['id_trx_klinis' => $id_klinis], $rGejala);
                }
                // spesimen
                if (!empty($request['ds']['spesimen'][0])) {
                    $dtSpesimen = $request['ds'];
                    $rSpesimen = [];
                    $hasillab = '';
                    foreach ($dtSpesimen['spesimen'] as $k => $v) {
                        $d = explode('|', $v);
                        if (!empty($dtSpesimen['hasil_lab'][$k])) {
                            $hasillab = $dtSpesimen['hasil_lab'][$k];
                        }
                        $rSpesimen[] = array(
                            'id_trx_case' => $id_trx_case,
                            'jenis_pemeriksaan' => $d[0],
                            'jenis_spesimen' => $d[1],
                            'tgl_ambil_spesimen' => Helper::dateFormat($d[2]),
                            'hasil' => $hasillab,
                            'case' => 'campak',
                        );
                    }
                    $spesimen = Tx::truncateInsert('trx_spesimen', ['id_trx_case' => $id_trx_case], $rSpesimen);
                }
                // notification
                $code_wilayah_faskes = $request['code_wilayah_faskes'];
                $dtNotification = [];
                if ($request['dk']['jenis_kasus'] = 1) {
                    // notif klb
                    $dtNotification[] = array(
                        'type' => 'klb',
                        'id_trx_case' => $id_trx_case,
                        'from_faskes' => $code_wilayah_faskes,
                        'to_faskes' => $code_kecamatan_pasien,
                    );
                }
                if (!empty($request['dc']['id_role'])) {
                    $id_role = $request['dc']['id_role'];
                    if ($id_role == '1') {
                        if ($code_kecamatan_pasien != $code_wilayah_faskes) {
                            $dtNotification[] = array(
                                'type' => 'cross',
                                'id_trx_case' => $id_trx_case,
                                'from_faskes' => $code_wilayah_faskes,
                                'to_faskes' => $code_kecamatan_pasien,
                            );
                        }
                    } else if ($id_role == '2') {
                        if ($code_kabupaten_pasien != $code_wilayah_faskes) {
                            $dtNotification[] = array(
                                'type' => 'cross',
                                'id_trx_case' => $id_trx_case,
                                'from_faskes' => $code_wilayah_faskes,
                                'to_faskes' => $code_kabupaten_pasien,
                            );
                        }
                    }
                    $notification = Tx::truncateInsert('trx_notification', ['id_trx_case' => $id_trx_case], $dtNotification);
                }
                $success = true;
                $code = 200;
                $trx_case = Tx::getData('trx_case', ['id' => $id_trx_case], ['select' => ['created_at', 'updated_at']])->first();
                $response[] = [
                    'id' => $id_trx_case,
                    'created_at' => date('d-m-Y H:i:s', strtotime($trx_case->created_at)),
                    'updated_at' => date('d-m-Y H:i:s', strtotime($trx_case->updated_at)),
                    'no_epid' => $no_epid,
                    'no_epid_klb' => $no_epid_klb,
                ];
                Helper::session_flash('success', 'Berhasil', 'Data berhasil disimpan');
            }
        }
        return Response::json(array(
            'success' => $success,
            'response' => $response,
        ), $code);
    }

    public function postSpesimen()
    {
        $r = Request::json()->all();
        $rSpesimen = array(
            'id_trx_case' => $r['id_trx_case'],
            'jenis_pemeriksaan' => $r['jenis_pemeriksaan'],
            'jenis_spesimen' => $r['jenis_spesimen'],
            'tgl_ambil_spesimen' => Helper::dateFormat($r['tgl_ambil_spesimen']),
            'tgl_kirim_lab' => Helper::dateFormat($r['tgl_kirim_lab']),
            'tgl_terima_lab' => Helper::dateFormat($r['tgl_terima_lab']),
            'sampel_ke' => $r['sampel_ke'],
            'case' => 'campak',
        );
        $response = DB::table('trx_spesimen')->insertGetId($rSpesimen);
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function viewPeriksa($id_trx_case)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $tx_case = TxCase::getDataCampak(array('id' => $id_trx_case));
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Pemeriksaan Lab Penyakit ' . $case,
            'data' => array(
                'id_trx_case' => $id_trx_case,
                'id_trx_pe' => null,
                'status_kasus' => $tx_case[0]->status_kasus,
            ),
        );
        return view('lab.case.' . $index . '.periksa.index', $compact);
    }

    public function viewPe($id_trx_case)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $tx_case = TxCase::getDataCampak(array('id' => $id_trx_case));
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Input data penyelidikan epidemologi kasus ' . $case,
            'data' => array(
                'id_trx_case' => $id_trx_case,
                'id_trx_pe' => null,
                'status_kasus' => !empty($tx_case[0]->status_kasus) ? $tx_case[0]->status_kasus : null,
            ),
        );
        return view('case.' . $index . '.pe.index', $compact);
    }

    public function postZeroReport()
    {
        $r = Request::json()->all();
        DB::beginTransaction();
        try {
            if (!empty($r['dt'])) {
                $start = $r['from_year'] . '-' . $r['from_month'] . '-';
                $start .= !empty($r['from_day']) ? $r['from_day'] : '01';
                $end = $r['to_year'] . '-' . $r['to_month'] . '-';
                $end .= !empty($r['to_day']) ? $r['to_day'] : date('t', mktime(0, 0, 0, $r['to_month'], 1, $r['to_year']));
                $r['dt']['trx_zero_report_start'] = $start;
                $r['dt']['trx_zero_report_end'] = $end;
                if (DB::table('trx_zero_report')->select('trx_zero_report_id')->where($r['dt'])->first()) {
                    $response = false;
                } else {
                    $response = Tx::insertData('trx_zero_report', $r['dt']);
                }
                DB::commit();
                return Response::json(array(
                    'success' => $response,
                ), 200);
            }
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function postPe($post = '')
    {
        if ($post) {
            $req = $post;
        } else {
            $req = Request::json()->all();
        }

        $response = [];
        foreach ($req as $key => $request) {
            $id_trx_case = $request['id_trx_case'];
            $id_trx_pe = $request['id_trx_pe'];
            if (empty($id_trx_case)) {
                $success = false;
                $response[] = ["error" => "id_trx_case is empty"];
                $code = 400;
                Helper::session_flash('warning', 'Gagal', 'Data gagal di simpan');
            } else {
                $request['dpe']['id_trx_case'] = $id_trx_case;
                $request['dpe']['tgl_penyelidikan'] = (!empty($request['dpe']['tgl_penyelidikan'])) ? Helper::dateFormat($request['dpe']['tgl_penyelidikan']) : date('Y-m-d');
                (!empty($request['dpe']['created_at'])) ? $request['dpe']['created_at'] = date('Y-m-d H:i:s', strtotime($request['dpe']['created_at'])) : null;
                (!empty($request['dpe']['updated_at'])) ? $request['dpe']['updated_at'] = date('Y-m-d H:i:s', strtotime($request['dpe']['updated_at'])) : null;
                if ($id_trx_pe = $request['id_trx_pe']) {
                    Tx::updateData('trx_pe', ['id' => $id_trx_pe], $request['dpe']);
                } else {
                    $id_trx_pe = Tx::insertData('trx_pe', $request['dpe']);
                }
                $request['drp']['id_trx_pe'] = $id_trx_pe;
                $request['drp']['tgl_pengobatan_pertama_kali'] = (!empty($request['drp']['tgl_pengobatan_pertama_kali'])) ? Helper::dateFormat($request['drp']['tgl_pengobatan_pertama_kali']) : null;
                if ($request['id_trx_pe']) {
                    Tx::updateData('trx_pe_riwayat_pengobatan', ['id_trx_pe' => $id_trx_pe], $request['drp']);
                } else {
                    Tx::insertData('trx_pe_riwayat_pengobatan', $request['drp']);
                }
                $request['drk']['id_trx_pe'] = $id_trx_pe;
                $request['drk']['tgl_penyakit_sama_dirumah'] = (!empty($request['drk']['tgl_penyakit_sama_dirumah'])) ? Helper::dateFormat($request['drk']['tgl_penyakit_sama_dirumah']) : null;
                $request['drk']['tgl_penyakit_sama_disekolah'] = (!empty($request['drk']['tgl_penyakit_sama_disekolah'])) ? Helper::dateFormat($request['drk']['tgl_penyakit_sama_disekolah']) : null;
                if ($request['id_trx_pe']) {
                    Tx::updateData('trx_pe_riwayat_kontak', ['id_trx_pe' => $id_trx_pe], $request['drk']);
                } else {
                    Tx::insertData('trx_pe_riwayat_kontak', $request['drk']);
                }
                if (!empty($request['dk'])) {
                    $dk = $request['dk'];
                    $rowdk = [];
                    foreach ($dk['lokasi'] as $k => $v) {
                        if (!empty($v) && !empty($dk['tgl_pe'][$k])) {
                            $tgl_pe = (!empty($dk['tgl_pe'][$k])) ? Helper::dateFormat($dk['tgl_pe'][$k]) : null;
                            $rowdk[] = array(
                                'id_trx_pe' => $id_trx_pe,
                                'lokasi' => $v,
                                'keterangan' => $dk['keterangan'][$k],
                                'tgl_pe' => $tgl_pe,
                                'jml_kasus' => $dk['jml_kasus'][$k],
                                'case' => 'campak',
                            );
                        }
                    }
                    Tx::truncateInsert('trx_pe_kasus_tambahan', ['id_trx_pe' => $id_trx_pe], $rowdk);
                }
                if (!empty($request['dp'])) {
                    $dp = $request['dp'];
                    $rowdp = [];
                    foreach ($dp['pelaksana'] as $k => $v) {
                        if (!empty($v)) {
                            $rowdp[] = array(
                                'id_trx_pe' => $id_trx_pe,
                                'nama_pelaksana' => $v,
                            );
                        }
                    }
                    Tx::truncateInsert('trx_pe_pelaksana', ['id_trx_pe' => $id_trx_pe], $rowdp);
                }
                $success = true;
                $code = 200;
                $trx_pe = Tx::getData('trx_pe', ['id' => $id_trx_pe], ['select' => ['created_at', 'updated_at']])->first();
                $response[] = [
                    'id' => $id_trx_pe,
                    'created_at' => date('d-m-Y H:i:s', strtotime($trx_pe->created_at)),
                    'updated_at' => date('d-m-Y H:i:s', strtotime($trx_pe->updated_at)),
                ];
                Helper::session_flash('success', 'Berhasil', 'Data berhasil disimpan');
            }
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getZeroReport()
    {
        $role = Helper::role();
        $d = DB::table("view_list_zero_report");
        if ($role->id_role == 1 or $role->id_role == 2) {
            $d->where('id_faskes', $role->id_faskes);
        } else if ($role->id_role == 5) {
            $d->where('code_provinsi_faskes', $role->code_provinsi_faskes);
        } else if ($role->id_role == 6) {
            $d->where('code_kabupaten_faskes', $role->code_kabupaten_faskes);
        }
        $q = Datatables::of($d);
        $q->addIndexColumn();
        $q->addColumn('action', function ($dt) {
            $roleUser = Helper::role();
            $action = "<div class='text-center'>";
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'provinsi', 'pusat'])) {
                $action .= '<a action="' . url('case/campak/zeroreport/delete/' . $dt->trx_zero_report_id) . '" title="Delete data"  class="btn btn-sm btn-flat btn-danger delete" data-toggle="modal" data-target=".modaldelete"><i class="fa fa-remove"></i></a>';
            };
            $action .= '</div>';
            return $action;
        });
        return $q->make(true);
    }

    public function getRekapitulasiData()
    {
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Rekapitulasi Data',
            'content' => []
        );
        echo '<pre>';
        print_r($compact);
        echo '</pre>';
        die();
        return view('case.campak.rekapitulasi_data', $compact);
    }

    public function deleteZeroReport($id = '')
    {
        $rId = Request::json()->all();
        if ($id != null) {
            $rId['query']['delete'][] = $id;
        }
        $response = [];
        foreach ($rId['query']['delete'] as $key => $val) {
            $response[$val] = Tx::postDelete('trx_zero_report', ['trx_zero_report_id' => $val]);
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getDataPeCampak()
    {
        $request = Request::all();
        $param['roleUser'] = Helper::role();
        $param['query']['where'] = ['id_case' => 1];
        $d = Sql::getData("view_list_pe", $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            foreach ($request['columns'] as $key => $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->editColumn('name_pasien', function ($dt) {
            $name_pasien = ucwords($dt->name_pasien);
            if ($dt->status_kasus == '1') {
                $name_pasien .= '<br><span class="label label-danger">Index</span>';
            }
            return $name_pasien;
        });
        $q->editColumn('nama_ortu', function ($dt) {
            return ucwords($dt->nama_ortu);
        });
        $q->editColumn('no_epid', function ($dt) {
            $no_epid = ($dt->no_epid_klb) ? $dt->no_epid_klb : $dt->no_epid;
            if ($dt->jenis_input == '1') {
                $no_epid .= '<br><span class="label label-info">Web</span>';
            } else if ($dt->jenis_input == '2') {
                $no_epid .= '<br><span class="label label-success">Android</span>';
            } else if ($dt->jenis_input == '3') {
                $no_epid .= '<br><span class="label label-warning">Import</span>';
            }
            return $no_epid;
        });
        $q->addColumn('action', function ($dt) {
            $roleUser = Helper::role();
            $action = "<div class='text-center'>";
            $action .= "<a href=" . url("case/campak/pe/detail/" . $dt->id_pe) . " title='Detail data' data-toggle='tooltip' class='btn btn-sm btn-flat btn-success'><i class='fa fa-asterisk'></i></a>";
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'kabupaten'])) {
                $action .= "<a href=" . url("case/campak/pe/update/" . $dt->id_pe) . " title='Edit data' data-toggle='tooltip' class='btn btn-sm btn-flat btn-warning'><i class='fa fa-pencil'></i></a>";
            };
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'provinsi', 'pusat'])) {
                $action .= '<a action="' . url('case/campak/pe/delete/' . $dt->id_pe) . '" title="Delete data"  class="btn btn-sm btn-flat btn-danger delete" data-toggle="modal" data-target=".modaldelete"><i class="fa fa-remove"></i></a>';
            };
            $action .= '</div>';
            return $action;
        });
        return $q->make(true);
    }

    public function viewDetailPe($id_trx_pe)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $response = $this->getDetailPe($id_trx_pe);
        $dt = json_decode($response->getContent());
        $data = $dt->response[0];
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Detail Pasien Kasus Penyelidikan Epidemologi Penyakit ' . $case,
            'data' => array(
                'index' => $index,
                'response' => $data,
            ),
        );
        return view('case.' . $index . '.pe.detail', $compact);
    }

    public function viewUpdatePe($id_pe)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $cc = Tx::getData('campak', ['id_pe' => $id_pe], ['select' => ['id', 'status_kasus']])->first();
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Update data Penyelidikan Epidemologi kasus ' . $case,
            'data' => array(
                'id_trx_pe' => $id_pe,
                'id_trx_case' => $cc->id,
                'status_kasus' => $cc->status_kasus,
            ),
        );
        return view('case.' . $index . '.pe.index', $compact);
    }

    public function getDataBasedAreaLab()
    {
        $request = Request::all();
        $param['query']['select'] = array('id_trx_case', 'no_epid', 'no_epid_klb', 'name_pasien', 'full_address', 'nama_ortu', 'thn_sakit', 'status_kasus', 'name_faskes', 'code_faskes');
        if (!empty($request['area'])) {
            $filter = json_decode($request['area']);
            $param['query']['whereIn'] = array('code_provinsi_pasien' => $filter);
        }
        $param['query']['where'] = ['id_case' => 1];
        $d = Sql::getData('view_list_case', $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            foreach ($request['columns'] as $key => $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->addColumn('action', function ($dt) {
            $action = "<div class='btn-group'>";
            $action .= '<a href="campak/periksa/' . $dt->id_trx_case . '" title="Pemeriksaan Laboratorium" data-toggle="tooltip" class="btn btn-info">Tambah Pemeriksaan</a>';
            $action .= '</div>';
            return $action;
        });
        return $q->make(true);
    }

    public function getDataSampel()
    {
        $request = Request::all();
        $param['query']['select'] = array('id_trx_case', 'no_epid', 'no_epid_klb', 'name_pasien', 'full_address', 'nama_ortu', 'thn_sakit', 'status_kasus', 'name_faskes', 'code_faskes');
        if (!empty($request['area'])) {
            $filter = json_decode($request['area']);
            $param['query']['whereIn'] = array('code_provinsi_pasien' => $filter);
        }
        $d = Sql::getData('view_list_case_lab', $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            foreach ($request['columns'] as $key => $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->addColumn('action', function ($dt) {
            $action = "<div class='btn-group'>";
            $action .= '<a href="campak/periksa/' . $dt->id_trx_case . '" title="Pemeriksaan Laboratorium" data-toggle="tooltip" class="btn btn-info">Tambah Pemeriksaan</a>';
            $action .= '</div>';
            return $action;
        });
        return $q->make(true);
    }

    public function _getDataBasedAreaLab()
    {
        $config['table'] = 'campak AS a';
        $config['coloumn'] = array('id', 'no_epid', 'no_epid_klb', 'name_pasien', 'full_address', 'nama_ortu', 'thn_tgl_sakit', 'status_kasus', 'name_faskes', 'code_faskes');
        $config['columnSelect'] = array('a.id', 'a.no_epid', 'a.no_epid_klb', 'a.name_pasien', 'a.nama_ortu', 'a.full_address', 'a.thn_tgl_sakit', 'a.status_kasus', 'a.name_faskes', 'a.code_faskes');
        $config['columnFormat'] = array('number', '', '', '', '', '', '', '', '', '');
        $config['searchColumn'] = array('a.name_pasien', 'a.no_epid');
        $config['key'] = 'a.id AS id';

        if (Request::get('area')) {
            $filter = json_decode(Request::get('area'));
            if (!empty($filter)) {
                $config['whereIn'] = array('a.code_provinsi_pasien' => $filter);
            }
        }
        if (Request::get('dt')) {
            $filter = json_decode(Request::get('dt'));
            // filter district
            if (!empty($filter->filter)) {
                $district = $filter->filter;
                foreach ($district as $key => $val) {
                    if (!empty($val)) {
                        $config['where'][] = array('a.' . $key => $val);
                    }
                }
            }
            // filter time
            $time = $filter->range;
            if ($time) {
                $from = $filter->from;
                $fy = (!empty($from->year)) ? $from->year : null;
                $fm = (!empty($from->month)) ? $from->month : null;
                $fd = (!empty($from->day)) ? $from->day : null;
                $to = $filter->to;
                $ty = (!empty($to->year)) ? $to->year : null;
                $tm = (!empty($to->month)) ? $to->month : null;
                $td = (!empty($to->day)) ? $to->day : null;
                if ($time == '1') {
                    $start = $fy . '-' . $fm . '-' . $fd;
                    $to = $ty . '-' . $tm . '-' . $td;
                    if ($fy && $fm && $fd && $ty && $tm && $td) {
                        $config['whereBetween'] = array('a.tgl_sakit' => array($start, $to));
                    }
                } else if ($time == '2') {
                    $start = $fy . '-' . $fm . '-' . '1';
                    $to = $ty . '-' . $tm . '-' . date('t', mktime(0, 0, 0, $tm, 1, $ty));
                    if ($fy && $fm && $ty && $tm) {
                        $config['whereBetween'] = array('a.tgl_sakit' => array($start, $to));
                    }
                } else if ($time == '3') {
                    $start = $fy;
                    $to = $ty;
                    if ($fy && $ty) {
                        $config['whereBetween'] = array('a.thn_tgl_sakit' => array($start, $to));
                    }
                }
            }
        }

        $Datatable = Datatables::getData($config);
        if (count($Datatable['data']) > 0) {
            foreach ($Datatable['data'] as $key => $val) {
                $no_epid = (empty($val['no_epid_klb'])) ? $val['no_epid'] : $val['no_epid_klb'];
                $name = $val['name_pasien'];
                if ($val['status_kasus'] == 1) {
                    $name = $val['name_pasien'] . '<br><span class="label label-danger">Index</span>';
                }
                $aksi = '<a href="campak/periksa/' . $val['id'] . '" title="Pemeriksaan Laboratorium" data-toggle="tooltip" class="btn btn-info">Tambah Pemeriksaan</a>';
                $dSampel = TxSampelLab::getDataSampel(array('id' => $val['id']), $this->index);
                $status = (!empty($dSampel)) ? "Sudah Periksa" : "Belum Periksa";
                $dt[] = array(
                    'no' => $val['no'],
                    'id' => $val['id'],
                    'no_epid' => $no_epid,
                    'name_pasien' => $name,
                    'nama_ortu' => $val['nama_ortu'],
                    'alamat' => $val['full_address'],
                    'rujukan' => $val['name_faskes'],
                    'status' => $status,
                    'action' => $aksi,
                );
            }
            $Datatable['data'] = $dt;
        }
        echo json_encode($Datatable);
    }

    public function _getDataSampel()
    {
        $config['table'] = 'view_lab_campak AS a';
        $config['coloumn'] = array('id_spesimen', 'id', 'no_epid', 'no_epid_klb', 'name', 'umur_thn', 'umur_bln', 'umur_hari', 'jenis_kelamin_txt', 'keadaan_akhir_txt', 'jenis_pemeriksaan_txt', 'jenis_spesimen_txt', 'tgl_ambil_spesimen', 'hasil', 'kabupaten_name', 'hasil_igm_campak', 'hasil_igm_rubella', 'hasil_pcr', 'hasil_kultur', 'hasil_sqc', 'tgl_periksa_igm_campak', 'tgl_periksa_igm_rubella', 'tgl_periksa_kultur', 'tgl_periksa_pcr', 'tgl_periksa_sqc');
        $config['columnSelect'] = array('a.id_spesimen', 'a.id', 'a.no_epid', 'a.no_epid_klb', 'a.name', 'a.umur_thn', 'a.umur_bln', 'a.umur_hari', 'a.jenis_kelamin_txt', 'a.keadaan_akhir_txt', 'a.jenis_pemeriksaan_txt', 'a.jenis_spesimen_txt', 'a.tgl_ambil_spesimen', 'a.hasil', 'a.kabupaten_name', 'a.hasil_igm_campak', 'a.hasil_igm_rubella', 'a.hasil_pcr', 'a.hasil_kultur', 'a.hasil_sqc', 'a.tgl_periksa_igm_campak', 'a.tgl_periksa_igm_rubella', 'a.tgl_periksa_kultur', 'a.tgl_periksa_pcr', 'a.tgl_periksa_sqc');
        $config['columnFormat'] = array('number', 'number', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
        $config['searchColumn'] = array('a.name', 'a.no_epid');
        $config['key'] = 'a.id_spesimen';
        $config['action'][] = array(
            'action' => 'detail',
            'url' => url('lab/case/campak/detail'),
            'key' => 'id',
        );
        $config['action'][] = array(
            'action' => 'edit',
            'url' => url('lab/case/campak/update'),
            'key' => 'id',
        );
        $config['action'][] = array(
            'action' => 'delete',
            'url' => url('lab/case/campak/delete'),
            'key' => 'id',
        );

        if (Request::get('area')) {
            $filter = json_decode(Request::get('area'));
            if (!empty($filter)) {
                $config['whereIn'] = array('a.code_provinsi_pasien' => $filter);
            }
        }
        if (Request::get('dt')) {
            $filter = json_decode(Request::get('dt'));
            // filter district
            if (!empty($filter->district)) {
                $district = $filter->district;
                foreach ($district as $key => $val) {
                    if (!empty($val)) {
                        $config['where'][] = array('a.' . $key => $val);
                    }
                }
            }
            // filter time
            $time = $filter->filter->range;
            if ($time) {
                $from = $filter->from;
                $fy = (!empty($from->year)) ? $from->year : null;
                $fm = (!empty($from->month)) ? $from->month : null;
                $fd = (!empty($from->day)) ? $from->day : null;
                $to = $filter->to;
                $ty = (!empty($to->year)) ? $to->year : null;
                $tm = (!empty($to->month)) ? $to->month : null;
                $td = (!empty($to->day)) ? $to->day : null;
                if ($time == '1') {
                    $start = $fy . '-' . $fm . '-' . $fd;
                    $to = $ty . '-' . $tm . '-' . $td;
                    if ($fy && $fm && $fd && $ty && $tm && $td) {
                        $config['whereBetween'] = array('a.tgl_sakit' => array($start, $to));
                    }
                } else if ($time == '2') {
                    $start = $fy . '-' . $fm . '-' . '1';
                    $to = $ty . '-' . $tm . '-' . date('t', mktime(0, 0, 0, $tm, 1, $ty));
                    if ($fy && $fm && $ty && $tm) {
                        $config['whereBetween'] = array('a.tgl_sakit' => array($start, $to));
                    }
                } else if ($time == '3') {
                    $start = $fy;
                    $to = $ty;
                    if ($fy && $ty) {
                        $config['whereBetween'] = array('a.thn_tgl_sakit' => array($start, $to));
                    }
                }
            }
        }
        $Datatable = Datatables::getData($config);
        // $lol = $Datatable['data'];
        // foreach ($Datatable['data'] as $key => $value) {
        //     $lol[]=$value;
        // }

        $daftarsampel = collect($Datatable['data'])->groupBy('id');
        $hasil = '';
        $jenis_pemeriksaan = '';
        $jenis_sampel = '';
        $tgl_periksa = '';
        $no = 1;

        if (count($Datatable['data']) > 0) {
            // foreach ($Datatable['data'] as $key => $val) {
            foreach ($daftarsampel as $keys => $value) {
                $umur_thn = (!empty($value[0]['umur_thn'])) ? $value[0]['umur_thn'] . ' Th ' : null;
                $umur_bln = (!empty($value[0]['umur_bln'])) ? $value[0]['umur_bln'] . ' Bln ' : null;
                $umur_hari = (!empty($value[0]['umur_hari'])) ? $value[0]['umur_hari'] . ' Hari ' : null;
                $no_epid = (empty($value[0]['no_epid_klb'])) ? $value[0]['no_epid'] : $value[0]['no_epid_klb'];
                $name = $value[0]['name'];
                foreach ($value as $key => $val) {
                    if (!empty($val['tgl_periksa_igm_campak'])) {
                        $hasil .= 'Igm Campak : ' . $val['hasil_igm_campak'] . '<br>';
                        $jenis_pemeriksaan .= $val['jenis_pemeriksaan_txt'] . '<br>';
                        $jenis_sampel .= $val['jenis_spesimen_txt'] . '<br>';
                        $tgl_periksa .= $val['tgl_periksa_igm_campak'] . '<br>';
                    }
                    if (!empty($val['tgl_periksa_igm_rubella'])) {
                        $hasil .= 'Igm Rubella : ' . $val['hasil_igm_rubella'] . '<br>';
                        $jenis_pemeriksaan .= $val['jenis_pemeriksaan_txt'] . '<br>';
                        $jenis_sampel .= $val['jenis_spesimen_txt'] . '<br>';
                        $tgl_periksa .= $val['tgl_periksa_igm_rubella'] . '<br>';
                    }
                    if (!empty($val['tgl_periksa_kultur'])) {
                        $hasil .= 'Kultur : ' . $val['hasil_kultur'] . '<br>';
                        $jenis_pemeriksaan .= $val['jenis_pemeriksaan_txt'] . '<br>';
                        $jenis_sampel .= $val['jenis_spesimen_txt'] . '<br>';
                        $tgl_periksa .= $val['tgl_periksa_kultur'] . '<br>';
                    }
                    if (!empty($val['tgl_periksa_pcr'])) {
                        $hasil .= 'PCR : ' . $val['hasil_pcr'] . '<br>';
                        $jenis_pemeriksaan .= $val['jenis_pemeriksaan_txt'] . '<br>';
                        $jenis_sampel .= $val['jenis_spesimen_txt'] . '<br>';
                        $tgl_periksa .= $val['tgl_periksa_pcr'] . '<br>';
                    }
                    if (!empty($val['tgl_periksa_sqc'])) {
                        $hasil .= 'Sequencing : ' . $val['hasil_sqc'] . '<br>';
                        $jenis_pemeriksaan .= $val['jenis_pemeriksaan_txt'] . '<br>';
                        $jenis_sampel .= $val['jenis_spesimen_txt'] . '<br>';
                        $tgl_periksa .= $val['tgl_periksa_sqc'] . '<br>';
                    }
                    if (!empty($val['tgl_ambil_spesimen'])) {
                        $hasil .= $val['hasil'] . '<br>';
                        $jenis_pemeriksaan .= $val['jenis_pemeriksaan_txt'] . '<br>';
                        $jenis_sampel .= $val['jenis_spesimen_txt'] . '<br>';
                        $tgl_periksa .= $val['tgl_ambil_spesimen'] . '<br>';
                    }
                }
                $dt[] = array(
                    'no' => $no,
                    'id' => $value[0]['id_spesimen'],
                    'no_epid' => $no_epid,
                    'name_pasien' => $name,
                    'umur_thn' => $umur_thn,
                    'umur_bln' => $umur_bln,
                    'umur_hari' => $umur_hari,
                    'jenis_kelamin' => $value[0]['jenis_kelamin_txt'],
                    'kabupaten' => $value[0]['kabupaten_name'],
                    'jenis_pemeriksaan' => $jenis_pemeriksaan,
                    'jenis_sampel' => $jenis_sampel,
                    'tgl_periksa' => $tgl_periksa,
                    'hasil' => $hasil,
                    'keadaan_akhir_txt' => $value[0]['keadaan_akhir_txt'],
                    'action' => $value[0]['action'],
                );
                $hasil = '';
                $jenis_pemeriksaan = '';
                $jenis_sampel = '';
                $tgl_periksa = '';
                $no++;
            }
            $Datatable['data'] = $dt;
        }
        // echo json_encode($lol);
        echo json_encode($Datatable);
    }

    public function review()
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Review Data Kasus Penyakit ' . $case,
            // 'data'          => array(
            // 'id_trx_case'=>$id,
            // )
        );
        return view('case.' . $index . '.txt_review', $compact);
    }

    public function getDumpData()
    {
        if (Request::get('dt')) {
            $filter = json_decode(Request::get('dt'));
            // filter district
            $district = $filter->district;
            if (!empty($district->id_puskesmas) && $district->id_puskesmas) {
                $config['where'][] = array('a.id_faskes' => $district->id_puskesmas);
            } else if (!empty($district->id_kecamatan) && $district->id_kecamatan) {
                $config['where'][] = array('a.code_kecamatan_pasien' => $district->id_kecamatan);
            } else if (!empty($district->id_rs) && $district->id_rs) {
                $config['where'][] = array('a.id_faskes' => $district->id_rs);
            } else if (!empty($district->id_kabupaten) && $district->id_kabupaten) {
                $config['where'][] = array('a.code_kabupaten_pasien' => $district->id_kabupaten);
            } else if (!empty($district->id_provinsi) && $district->id_provinsi) {
                $config['where'][] = array('a.code_provinsi_pasien' => $district->id_provinsi);
            }
            // filter time
            $time = $filter->filter;
            if ($time->range) {
                $from = $filter->from;
                $fy = (!empty($from->year)) ? $from->year : null;
                $fm = (!empty($from->month)) ? $from->month : null;
                $fd = (!empty($from->day)) ? $from->day : null;
                $to = $filter->to;
                $ty = (!empty($to->year)) ? $to->year : null;
                $tm = (!empty($to->month)) ? $to->month : null;
                $td = (!empty($to->day)) ? $to->day : null;
                if ($time->range == '1') {
                    $start = $fy . '-' . $fm . '-' . $fd;
                    $to = $ty . '-' . $tm . '-' . $td;
                    if ($fy && $fm && $fd && $ty && $tm && $td) {
                        $config['whereBetween'] = array('a.tgl_sakit' => array($start, $to));
                    }
                } else if ($time->range == '2') {
                    $start = $fy . '-' . $fm . '-' . '1';
                    $to = $ty . '-' . $tm . '-' . date('t', mktime(0, 0, 0, $tm, 1, $ty));
                    if ($fy && $fm && $ty && $tm) {
                        $config['whereBetween'] = array('a.tgl_sakit' => array($start, $to));
                    }
                } else if ($time->range == '3') {
                    $start = $fy;
                    $to = $ty;
                    if ($fy && $ty) {
                        $config['whereBetween'] = array('a.thn_tgl_sakit' => array($start, $to));
                    }
                }
            }
            if (!empty($config)) {
                $dt_case = $config;
                $dt_lab = $config;
            }
        }

        $dt_case['table'] = 'campak AS a';
        $dt_case['coloumn'] = array('id', 'no_epid', 'no_epid_klb', 'name_pasien', 'tgl_lahir', 'umur_thn', 'name_provinsi_pasien', 'name_kabupaten_pasien', 'keadaan_akhir_txt', 'klasifikasi_final', 'id_pe', 'thn_tgl_sakit', 'status_kasus');
        $dt_case['columnSelect'] = array('a.id', 'a.no_epid', 'a.no_epid_klb', 'a.name_pasien', 'a.tgl_lahir', 'a.umur_thn', 'a.name_provinsi_pasien', 'a.name_kabupaten_pasien', 'a.keadaan_akhir_txt', 'a.klasifikasi_final', 'a.id_pe', 'a.thn_tgl_sakit', 'a.status_kasus');
        $dt_case['columnFormat'] = array('number', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

        $dt_lab['table'] = 'view_lab_campak AS a';
        $dt_lab['coloumn'] = array('id', 'id_spesimen', 'id_outbreak', 'no_epid', 'no_epid_klb', 'name', 'jenis_kelamin_txt', 'jenis_pemeriksaan_txt', 'jenis_spesimen_txt', 'tgl_ambil_spesimen', 'hasil', 'kabupaten_name', 'hasil_igm_campak', 'hasil_igm_rubella', 'hasil_pcr', 'hasil_kultur', 'hasil_sqc', 'tgl_periksa_igm_campak', 'tgl_periksa_igm_rubella', 'tgl_periksa_kultur', 'tgl_periksa_pcr', 'tgl_periksa_sqc');
        $dt_lab['columnSelect'] = array('a.id', 'a.id_spesimen', 'a.id_outbreak', 'a.no_epid', 'a.no_epid_klb', 'a.name', 'a.jenis_kelamin_txt', 'a.jenis_pemeriksaan_txt', 'a.jenis_spesimen_txt', 'a.tgl_ambil_spesimen', 'a.hasil', 'a.kabupaten_name', 'a.hasil_igm_campak', 'a.hasil_igm_rubella', 'a.hasil_pcr', 'a.hasil_kultur', 'a.hasil_sqc', 'a.tgl_periksa_igm_campak', 'a.tgl_periksa_igm_rubella', 'a.tgl_periksa_kultur', 'a.tgl_periksa_pcr', 'a.tgl_periksa_sqc');
        $dt_lab['columnFormat'] = array('number', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

        $dataTableCase = Datatables::getData($dt_case);
        $dataTableLab = Datatables::getData($dt_lab);
        // $dumpData = [];
        $dumpData = collect($dataTableCase['data']);
        foreach ($dataTableLab['data'] as $tmp) {
            $dumpData->push($tmp);
        }
        $dumpData = collect($dumpData)->groupBy('id');
        $no = 1;
        if (count($dumpData) > 0) {
            foreach ($dumpData as $tmpData) {
                $id_outbreak = $igm_campak = $igm_rubella = $kultur = $sqc = $pcr = null;
                foreach ($tmpData as $key => $value) {
                    if (!empty($value['id_spesimen'])) {
                        $id = $value['id'];
                        $id_outbreak = (!empty($value['id_outbreak'])) ? $value['id_outbreak'] : null;
                        $jenis_kelamin = $value['jenis_kelamin_txt'];
                        if (!empty($value['hasil_igm_campak'])) {
                            $igm_campak = $value['hasil_igm_campak'];
                        }
                        if (!empty($value['hasil_igm_rubella'])) {
                            $igm_rubella = $value['hasil_igm_rubella'];
                        }
                        if (!empty($value['hasil_kultur'])) {
                            $kultur = $value['hasil_kultur'];
                        }
                        if (!empty($value['hasil_sqc'])) {
                            $sqc = $value['hasil_sqc'];
                        }
                        if (!empty($value['hasil_pcr'])) {
                            $pcr = $value['hasil_pcr'];
                        }
                    } else {
                        $id = $value['id'];
                        $umur_thn = (!empty($value['umur_thn']) || strlen($value['umur_thn'])) ? $value['umur_thn'] . ' Th ' : null;
                        $no_epid = (empty($value['no_epid_klb'])) ? $value['no_epid'] : $value['no_epid_klb'];
                        $name = $value['name_pasien'];
                        $tgl_lahir = $value['tgl_lahir'];
                        $provinsi = $value['name_provinsi_pasien'];
                        $kabupaten = $value['name_kabupaten_pasien'];
                        $keadaan_akhir = $value['keadaan_akhir_txt'];
                        $klasifikasi_final = $value['klasifikasi_final'];
                    }
                }
                $dt[] = array(
                    'no' => $no,
                    'id' => $id,
                    'no_epid' => $no_epid,
                    'no_outbreak' => $id_outbreak,
                    'negara' => 'IDN',
                    'provinsi' => $provinsi,
                    'kabupaten' => $kabupaten,
                    'name_pasien' => $name,
                    'jenis_kelamin' => $jenis_kelamin,
                    'tgl_lahir' => $tgl_lahir,
                    'umur_thn' => $umur_thn,
                    'igm_campak' => $igm_campak,
                    'igm_rubella' => $igm_rubella,
                    'kultur' => $kultur,
                    'sqc' => $sqc,
                    'pcr' => $pcr,
                    'keadaan_akhir_txt' => $keadaan_akhir,
                    'klasifikasi_final' => $klasifikasi_final,
                );
                $Datatable['iTotalRecords'] = $no;
                $Datatable['iTotalDisplayRecords'] = $no;
                $no++;
            }
            $Datatable['data'] = $dt;
        }
        echo json_encode($Datatable);
        // echo json_encode($dumpData);
    }
}

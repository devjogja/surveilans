<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Role;
use Response;

class RoleController extends Controller
{
	public function viewRole()
	{
		$compact = array(
			'title'         => 'Manage Rule',
			'contentTitle'  => 'Manage Rule',
			'data' => array()
			);
		return view('role.index',$compact);
	}

	public function getData()
	{
		$response = Role::getData();
		return Response::json(array(
			'success'=>true,
			'response'=>$response
		), 200);
	}	
}

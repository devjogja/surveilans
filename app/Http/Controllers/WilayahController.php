<?php

namespace App\Http\Controllers;

use Request;
use Response;
use DB;
use App\Models\Wilayah;

class WilayahController extends Controller
{
	public function getArea()
	{
		$key = Request::get('term');
		$where['a.full_address'] = $key;
		$query = Wilayah::getDataArea($where);
		echo json_encode($query);
	}

	public function getExpandWilayah()
	{
		if ($_GET['code'] == '#') {
			$response[] = [
				'id' => '1_1',
				'text' => 'INDONESIA',
				'children' => true
			];
		} else {
			$x = explode('_', $_GET['code']);
			if ($x[0] == 1) {
				$prov = DB::table('mst_provinsi')->select('code', 'name')->get();
				foreach ($prov as $key => $val) {
					$response[] = [
						'id' => '2_' . $val->code,
						'text' => strtoupper($val->name),
						'children' => true
					];
				}
			} elseif ($x[0] == 2) {
				$kab = DB::table('mst_kabupaten')->select('code', 'name')->where('code_provinsi', $x[1])->get();
				foreach ($kab as $key => $val) {
					$response[] = [
						'id' => '3_' . $val->code,
						'text' => 'KAB. '. strtoupper($val->name),
						'children' => true
					];
				}
			} elseif ($x[0] == 3) {
				$kec = DB::table('mst_kecamatan')->select('code', 'name')->where('code_kabupaten', $x[1])->get();
				foreach ($kec as $key => $val) {
					$response[] = [
						'id' => '4_' . $val->code,
						'text' => 'KEC. '. strtoupper($val->name),
						'children' => true
					];
				}
			} elseif ($x[0] == 4) {
				$pkm = DB::table('mst_puskesmas')->select('id', 'name')->where('code_kecamatan', $x[1])->get();
				foreach ($pkm as $key => $val) {
					$response[] = [
						'id' => '5_' . $val->id,
						'text' => 'PKM '.$val->name,
					];
				}
			}
		}
		return Response::json($response);
	}

	public function getKabupaten()
	{
		$idProvinsi = Request::get('id_provinsi');
		$data = Wilayah::getKabupaten(array('code_provinsi' => $idProvinsi));
		$response = array();
		foreach ($data as $key => $val) {
			$response[$key] = $val->name;
		}
		echo json_encode($response);
	}

	public function getDetailProvinsi($id)
	{
		$data = Wilayah::getProvince(array('code' => $id));
		echo json_encode($data);
	}

	public function getDetailKabupaten($id)
	{
		$data = Wilayah::getKabupaten(array('code' => $id));
		echo json_encode($data);
	}

	public function getKecamatan()
	{
		$idKabupaten = Request::get('id_kabupaten');
		$data = Wilayah::getKecamatan(array('code_kabupaten' => $idKabupaten));
		$response = array();
		foreach ($data as $key => $val) {
			$response[$key] = $val->name;
		}
		echo json_encode($response);
	}

	public function getKelurahan()
	{
		$idKecamatan = Request::get('id_kecamatan');
		$data = Wilayah::getKelurahan(array('code_kecamatan' => $idKecamatan));
		$response = array();
		foreach ($data as $key => $val) {
			$response[$key] = $val->name;
		}
		echo json_encode($response);
	}

	public function viewKabupaten()
	{
		$compact = array(
			'title'         => 'Surveilans PD3I',
			'contentTitle'  => 'Kabupaten',
			'data'          => array()
		);
		return view('district.kabupaten', $compact);
	}

	public function viewKecamatan()
	{
		$compact = array(
			'title'         => 'Surveilans PD3I',
			'contentTitle'  => 'Kecamatan',
			'data'          => array()
		);
		return view('district.kecamatan', $compact);
	}

	public function viewDesa()
	{
		$compact = array(
			'title'         => 'Surveilans PD3I',
			'contentTitle'  => 'Desa',
			'data'          => array()
		);
		return view('district.desa', $compact);
	}
}

<?php

namespace App\Http\Controllers;

use App\Models\Sql;
use App\Models\Tx;
use App\Models\TxCase;
use Carbon\Carbon;
use Datatables;
use DB;
use Excel;
use Helper;
use Request;
use Response;
use Validator;

class TetanusController extends Controller
{
    protected $index;
    public function __construct()
    {
        $this->index = 'tetanus';
    }

    public function index()
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Penyakit ' . $dc[0]->name,
            'data' => array(
                'id_trx_case' => null,
                'index' => $index,
            ),
        );
        return view('case.' . $index . '.index', $compact);
    }

    public function exportExampleImportKasus()
    {
        $file = public_path() . "/download/tetanus/template import tetanus.xls";
        return Response::download($file, 'Contoh format import kasus tetanus.xls');
    }

    public function importCase()
    {
        $path = Request::file('import');
        if (!empty($path)) {
            $data = Excel::load($path, function ($reader) {})->get();
            $role = Helper::role();
            $row = array();
            if (!empty($data) && $data->count()) {
                foreach ($data->toArray() as $key => $val) {
                    $cd = DB::table('view_list_case')->where(['id_case'=>4,'name_pasien' => $val['nama_pasien'], 'code_kelurahan_pasien' => $val['wilayah'], 'tgl_sakit_date' => Helper::dateFormat($val['tgl_mulai_sakit']), 'nik' => $val['nik']])->count();
                    if ($cd == 0) {
                        $ut = $val['thn'];
                        $ub = $val['bln'];
                        $uh = $val['hari'];
                        if ($val['tanggal_lahir'] && $val['tgl_mulai_sakit']) {
                            $age = Helper::getAge(['tgl_sakit' => $val['tgl_mulai_sakit'], 'tgl_lahir' => $val['tanggal_lahir']]);
                            $ut = $age['years'];
                            $ub = $age['month'];
                            $uh = $age['day'];
                        }
                        $wilayah = DB::table('view_area')->where(['code_kelurahan' => $val['wilayah']])->first();
                        $cekpasien = DB::table('ref_pasien')->where(['name' => $val['nama_pasien'], 'nik' => $val['nik'], 'code_kelurahan' => $val['wilayah']])->first();
                        $row[] = [
                            "dc" => [
                                'id_faskes' => $role->id_faskes,
                                'id_role' => $role->id_role,
                                "jenis_input" => "3",
                                "no_rm" => $val['norm'],
                                "no_epid_lama" => $val['noepid_lama'],
                                "klasifikasi_final" => $val['klasifikasi_final'],
                            ],
                            'code_wilayah_faskes' => $role->code_wilayah_faskes,
                            'id_trx_case' => '',
                            'dp' => [
                                "name" => $val['nama_pasien'],
                                "nik" => $val['nik'],
                                "agama" => $val['agama'],
                                "jenis_kelamin" => $val['jenis_kelamin'],
                                "tgl_lahir" => $val['tanggal_lahir'],
                                "umur_thn" => $ut,
                                "umur_bln" => $ub,
                                "umur_hari" => $uh,
                                "alamat" => $val['alamat'],
                                "code_kelurahan" => $val['wilayah'],
                                "code_kecamatan" => (!empty($wilayah)) ? $wilayah->code_kecamatan : null,
                                "code_kabupaten" => (!empty($wilayah)) ? $wilayah->code_kabupaten : null,
                                "code_provinsi" => (!empty($wilayah)) ? $wilayah->code_provinsi : null,
                            ],
                            'id_pasien' => ($cekpasien) ? $cekpasien->id : '',
                            'df' => [
                                'name' => $val['nama_orang_tua'],
                            ],
                            "dk" => [
                                "tgl_mulai_sakit" => $val['tgl_mulai_sakit'],
                                "tgl_laporan_diterima" => $val['tgl_laporan_diterima'],
                                "tgl_pelacakan" => $val['tgl_pelacakan'],
                                "antenatal_care" => $val['antenatal_care'],
                                "status_imunisasi_ibu" => $val['status_imunisasi_ibu'],
                                "penolong_persalinan" => $val['penolong_persalinan'],
                                "alat_pemotong_tali_pusat" => $val['alat_pemotong_tali_pusat'],
                                "perawatan_tali_pusat" => $val['perawatan_tali_pusat'],
                                "rawat_rumah_sakit" => $val['rawat_rumah_sakit'],
                                "keadaan_akhir" => $val['keadaan_akhir'],
                            ],
                        ];
                    }
                }
            }
            if ($row) {
                $tetanus = $this->postCase($row);
                $response = json_decode($tetanus->getContent())->response;
            }
            echo true;
        }
    }

    public function export($request = '')
    {
        $param['filter'] = json_decode($request);
        if (!empty($param['filter']->code_role)) {
            $param['roleUser'] = (object) array(
                'code_role' => $param['filter']->code_role,
                'id_faskes' => $param['filter']->id_faskes,
                'code_faskes' => $param['filter']->code_faskes,
            );
        }
        $q = Sql::getData("tetanus", $param);
        $q->join('tetanus_pe AS c', 'a.id_trx_case', '=', 'c.id_trx_case', 'left');
        $q->groupBy('a.id_trx_case');
        $r = $q->get();
        $compact['dd'] = $r;
        // return view('case.tetanus.export', $compact);
        $export = view('case.tetanus.export', $compact);
        $filename = 'export_tetanus-' . date('YmdHi') . '.xls';

        header("Content-type: application/vnd.ms-excel; name='excel'");
        header("Cache-Control: max-age=0");
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Content-Transfer-Encoding: text');
        echo $export;
    }

    public function moveCase($id = null)
    {
        if (!empty($id)) {
            $role = Helper::role();
            $case = Tx::getData('tetanus', ['id' => $id], ['select' => ['code_kelurahan_pasien', 'id_faskes']])->first();
            $code_kelurahan_pasien = $case->code_kelurahan_pasien;
            $code_faskes_old = $case->id_faskes;
            $code_faskes = Tx::getData('mst_wilayah_kerja_puskesmas', ['code_kelurahan' => $code_kelurahan_pasien], ['code_faskes'])->first();
            if (!empty($code_faskes)) {
                $faskes = Tx::getData('mst_puskesmas', ['code_faskes' => $code_faskes->code_faskes])->first();
                $dt = [
                    'id_faskes' => $faskes->id,
                    'id_faskes_old' => $code_faskes_old,
                    'id_role' => '1',
                    'id_role_old' => $role->id_role,
                ];
                $status = Tx::movedData('trx_case', ['id' => $id], $dt);
            } else {
                $status = false;
            }
        }
        return Response::json(array(
            'success' => $status,
        ), 200);
    }

    public function viewAnalisa()
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Analisa Penyakit ' . $dc[0]->name,
            'data' => array(
                'id_trx_case' => null,
                'index' => $index,
            ),
        );
        return view('case.' . $index . '.analisa_mobile', $compact);
    }

    public function getDataTetanus()
    {
        $request = Request::all();
        $param['filter'] = json_decode($request['dt']);
        $param['roleUser'] = Helper::role();
        $param['query']['select'] = ['id_trx_case', 'no_epid', 'name_pasien', 'umur_hari', 'umur_bln', 'umur_thn', 'code_kecamatan_pasien', 'full_address', 'keadaan_akhir_txt', 'klasifikasi_final_txt', 'id_role', 'name_faskes', 'code_kecamatan_faskes', 'jenis_input', 'id_pe', 'id_faskes_old', 'id_role_old', 'tgl_input', 'tgl_update', 'jenis_input_text', 'pe_text'];
        $param['query']['where'] = ['id_case' => 4];
        $d = Sql::getData("view_list_case", $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            foreach ($request['columns'] as $key => $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->editColumn('no_epid', function ($dt) {
            $no_epid = $dt->no_epid;
            if ($dt->jenis_input == '1') {
                $no_epid .= '<br><span class="label label-info">Web</span>';
            } else if ($dt->jenis_input == '2') {
                $no_epid .= '<br><span class="label label-success">Android</span>';
            } else if ($dt->jenis_input == '3') {
                $no_epid .= '<br><span class="label label-warning">Import</span>';
            }
            if ($dt->id_role == '2') {
                $no_epid .= "<span class='label label-primary' style='background-color:#b503b5 !important;'> Data dari rumahsakit </span>";
            }
            if ($dt->id_faskes_old) {
                $fo = '';
                if ($dt->id_role_old == '1') {
                    $fo = 'PUSKESMAS ' . DB::table('mst_puskesmas')->select(['name'])->where('id', $dt->id_faskes_old)->first()->name;
                } else if ($dt->id_role_old == '2') {
                    $fo = DB::table('mst_rumahsakit')->select(['name'])->where('id', $dt->id_faskes_old)->first()->name;
                }
                $no_epid .= "<span class='label label-primary'> Data kiriman dari " . $fo . "</span>";
            }
            return $no_epid;
        });
        $q->editColumn('id_pe', function ($dt) {
            $roleUser = Helper::role();
            $disabled = '';
            if (!in_array($roleUser->code_role, ['puskesmas', 'rs'])) {
                $disabled = "disabled='disabled' onclick='return false;'";
            }
            $pe = '<a href="' . url('case/tetanus/pe/' . $dt->id_trx_case) . '"'.$disabled.' title="Penyelidikan Epidemologi" data-toggle="tooltip" class="btn btn-flat btn-sm btn-success">PE</a>';
            if ($dt->id_pe) {
                $pe = '<a title="Penyelidikan Epidemologi" data-toggle="tooltip" class="btn btn-flat btn-sm btn-danger">Sudah PE</a>';
            }
            return $pe;
        });
        $q->addColumn('action', function ($dt) {
            $roleUser = Helper::role();
            $action = "<div class='text-center'>";
            $action .= "<a href=" . url("case/tetanus/detail/" . $dt->id_trx_case) . " title='Detail data' data-toggle='tooltip' class='btn btn-flat btn-sm btn-success'><i class='fa fa-asterisk'></i></a>";
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'kabupaten'])) {
                $action .= "<a href=" . url("case/tetanus/update/" . $dt->id_trx_case) . " title='Edit data' data-toggle='tooltip' class='btn btn-flat btn-sm btn-warning'><i class='fa fa-pencil'></i></a>";
            };
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'provinsi', 'pusat'])) {
                $action .= '<a action="' . url('case/tetanus/delete/' . $dt->id_trx_case) . '" title="Delete data"  class="btn btn-flat btn-sm btn-danger delete" data-toggle="modal" data-target=".modaldelete"><i class="fa fa-remove"></i></a>';
            };
            if (in_array($roleUser->code_role, ['puskesmas']) and $dt->id_role == '1') {
                if ($dt->code_kecamatan_faskes != $dt->code_kecamatan_pasien) {
                    $action .= '<a action="' . url('case/tetanus/move/' . $dt->id_trx_case) . '" title="Move data" data-toggle="modal" class="btn btn-flat btn-sm btn-primary move" data-target=".modalmove"><i class="fa fa-step-forward"></i></a>';
                }
            }
            $action .= '</div>';
            return $action;
        });
        return $q->make(true);
    }

    public function getEpid($param = [])
    {
        if (!empty($param)) {
            $request = $param;
        } else {
            $request = Request::json()->all();
        }
        $case = Helper::getCase(array('alias' => $this->index));
        $code = $case[0]->code;
        $id_kelurahan = $request['id_kelurahan'];
        $thn_case = $cthn = '';
        $q = DB::table('view_list_case');
        $q->where('id_case', 4);
        $q->select(DB::raw('MAX(no_epid) AS no_epid'));
        if ($request['tgl_sakit']) {
            $thn_case = \Carbon\Carbon::parse($request['tgl_sakit'])->format('Y');
            $cthn = \Carbon\Carbon::parse($request['tgl_sakit'])->format('y');
            $q->where('thn_sakit', $thn_case);
        }
        $q->where('code_kelurahan_pasien', $id_kelurahan);
        $cekEpid = $q->first();

        if (!empty($cekEpid->no_epid)) {
            $cc = substr($cekEpid->no_epid, -3);
            $no = str_pad($cc + 1, 3, 0, STR_PAD_LEFT);
        } else {
            $no = '001';
        }
        $no_epid = $code . $id_kelurahan . $cthn . $no;

        return Response::json(array(
            'response' => $no_epid,
        ));
    }

    public function postDelete($id = null)
    {
        $rId = Request::json()->all();
        if ($id != null) {
            $rId['query']['delete'][] = $id;
        }
        $response = [];
        foreach ($rId['query']['delete'] as $key => $val) {
            $response[$val] = Tx::deleteData('trx_case', ['id' => $val]);
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function viewDetail($id)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $response = $this->getDetail($id);
        $dt = json_decode($response->getContent());
        $data = $dt->response[0];

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Detail Pasien Kasus Penyakit ' . $case,
            'data' => array(
                'index' => $index,
                'response' => $data,
            ),
        );
        return view('case.' . $index . '.detail', $compact);
    }

    public function getDetail($id = [])
    {
        if ($id) {
            $request = [
                'where' => ['id' => [$id]],
                'query' => ['select' => ['*']],
            ];
        } else {
            $request = Request::json()->all();
        }
        $qQuery = [];
        if (isset($request['where'])) {
            foreach ($request['where'] as $key => $val) {
                if ($val[0] == '*') {
                    $qQuery['where'][$key] = '*';
                } else {
                    $qQuery['whereIn'][$key] = $val;
                }
            }
            if (isset($request['where']['code_roles'])) {
                if ($request['where']['code_roles'][0] == 'puskesmas') {
                    $c = [];
                    foreach ($request['where']['id_faskes'] as $key => $val) {
                        $codeKelurahanKerjaPkm = Tx::getData('view_wilayah_kerja_puskesmas', ['id_faskes' => $val], ['select' => ['code_kelurahan']])->get('code_kelurahan');
                        foreach ($codeKelurahanKerjaPkm as $k => $v) {
                            $c[] = $v->code_kelurahan;
                        }
                    }
                    $qQuery['orWhere']['whereIn'] = ['code_kelurahan_pasien' => $c, 'code_roles' => ['rs']];
                }
            }
        }
        if (isset($request['query'])) {
            foreach ($request['query'] as $key => $val) {
                foreach ($val as $v) {
                    if (!in_array($v, [])) {
                        $var[] = $v;
                    }
                }
                $qQuery[$key] = $var;
            }
        }
        if (!empty($request['pagination'])) {
            $qQuery['pagination'] = $request['pagination'];
        }
        $resp = Tx::getData('tetanus', [], $qQuery)->get();
        $dResp = $response = [];
        foreach ($resp as $key => $val) {
            $dt = (array) $val;
            $response[] = $dt;
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getDetailPe($id_pe = '')
    {
        if ($id_pe) {
            $request = [
                'where' => ['id' => [$id_pe]],
                'query' => ['select' => ['*', 'dtPemeriksaKehamilan', 'dtPenolongPersalinan', 'dtPelacak']],
            ];
        } else {
            $request = Request::json()->all();
        }
        $qQuery = [];
        if (isset($request['where'])) {
            foreach ($request['where'] as $key => $val) {
                if ($val[0] == '*') {
                    $qQuery['where'][$key] = '*';
                } else {
                    $qQuery['whereIn'][$key] = $val;
                }
            }
        }
        if (isset($request['query'])) {
            foreach ($request['query'] as $key => $val) {
                foreach ($val as $v) {
                    if (!in_array($v, ["dtPemeriksaKehamilan", "dtPenolongPersalinan", "dtPelacak"])) {
                        $var[] = $v;
                    }
                }
                $qQuery[$key] = $var;
            }
        }
        if (!empty($request['pagination'])) {
            $qQuery['pagination'] = $request['pagination'];
        }
        $resp = Tx::getData('view_pe_tetanus', [], $qQuery)->get();
        $dResp = $response = [];
        foreach ($resp as $key => $val) {
            $dt = (array) $val;
            if (isset($request['query'])) {
                foreach ($request['query'] as $k => $v) {
                    foreach ($v as $var) {
                        ($var == 'dtPemeriksaKehamilan') ? $dt['dtPemeriksaKehamilan'] = Tx::getData('view_pemeriksa_kehamilan', ['id_trx_pe' => $val->id], ['select' => ['nama', 'profesi', 'profesi_txt', 'alamat', 'frekuensi']])->get() : null;
                        ($var == 'dtPenolongPersalinan') ? $dt['dtPenolongPersalinan'] = Tx::getData('trx_pe_penolong_persalinan', ['id_trx_pe' => $val->id], ['select' => ['profesi', 'nama', 'alamat', 'tempat_persalinan']])->get() : null;
                        ($var == 'dtPelacak') ? $dt['dtPelacak'] = Tx::getData('trx_pe_pelacak', ['id_trx_pe' => $val->id], ['select' => ['nama', 'jabatan']])->get() : null;
                    }
                }
            }
            $response[] = $dt;
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function viewUpdate($id)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Update Kasus Penyakit ' . $case,
            'data' => array(
                'id_trx_case' => $id,
            ),
        );
        return view('case.' . $index . '.update', $compact);
    }

    public function postCase($post = [])
    {
        if ($post) {
            $req = $post;
        } else {
            $req = Request::json()->all();
        }
        $dc = Helper::getCase(array('alias' => $this->index));
        $response = [];
        foreach ($req as $key => $request) {
            $vk = Validator::make($request['dk'], [
            ]);
            if ($vk->fails()) {
                $success = false;
                $response[] = array_merge($vk->errors()->all());
                $code = 400;
                Helper::session_flash('warning', 'Gagal', 'Data gagal disimpan');
            } else {
                $request['dp']['tgl_lahir'] = (isset($request['dp']['tgl_lahir'])) ? Helper::dateFormat($request['dp']['tgl_lahir']) : null;
                if ($id_pasien = $request['id_pasien']) {
                    if (isset($request['dp']) && isset($request['df'])) {
                        Tx::updateData('ref_pasien', ['id' => $id_pasien], $request['dp']);
                        Tx::updateData('ref_family', ['id_pasien' => $id_pasien], $request['df']);
                    }
                    $id_family = Tx::getData('ref_family', ['id_pasien' => $id_pasien])->first()->id;
                } else {
                    $id_pasien = Tx::insertData('ref_pasien', $request['dp']);
                    $request['df']['id_pasien'] = $id_pasien;
                    $id_family = Tx::insertData('ref_family', $request['df']);
                }
                $al = DB::table('ref_pasien AS a');
                $al->join('view_area AS b', 'a.code_kelurahan', '=', 'b.code_kelurahan');
                $al->select('b.code_kecamatan', 'b.code_kelurahan', 'b.code_kabupaten');
                $al->where('a.id', $id_pasien);
                $alamat_pasien = $al->first();
                $code_kabupaten_pasien = empty($alamat_pasien->code_kabupaten) ?: $alamat_pasien->code_kabupaten;
                $code_kecamatan_pasien = empty($alamat_pasien->code_kecamatan) ?: $alamat_pasien->code_kecamatan;
                $code_kelurahan_pasien = empty($alamat_pasien->code_kelurahan) ?: $alamat_pasien->code_kelurahan;
                // data tx case
                $tgl_sakit = $request['dk']['tgl_mulai_sakit'];
                $epid = $this->getEpid(['tgl_sakit' => $tgl_sakit, 'id_kelurahan' => $code_kelurahan_pasien]);
                $no_epid = json_decode($epid->getContent())->response;
                $request['dc']['id_case'] = $dc[0]->id;
                $request['dc']['id_pasien'] = $id_pasien;
                if (empty($request['id_trx_case'])) {
                    $request['dc']['no_epid'] = $no_epid;
                }
                $request['dc']['klasifikasi_final'] = !empty($request['dc']['klasifikasi_final']) ? $request['dc']['klasifikasi_final'] : null;
                (isset($request['dc']['created_at'])) ? $request['dc']['created_at'] = date('Y-m-d H:i:s', strtotime($request['dc']['created_at'])) : null;
                (isset($request['dc']['updated_at'])) ? $request['dc']['updated_at'] = date('Y-m-d H:i:s', strtotime($request['dc']['updated_at'])) : null;
                if ($id_trx_case = $request['id_trx_case']) {
                    Tx::updateData('trx_case', ['id' => $id_trx_case], $request['dc']);
                } else {
                    $id_trx_case = Tx::insertData('trx_case', $request['dc']);
                }
                // data klinis
                $request['dk']['id_trx_case'] = $id_trx_case;
                $request['dk']['tgl_mulai_sakit'] = (isset($request['dk']['tgl_mulai_sakit'])) ? Helper::dateFormat($request['dk']['tgl_mulai_sakit']) : null;
                $request['dk']['tgl_laporan_diterima'] = (isset($request['dk']['tgl_laporan_diterima'])) ? Helper::dateFormat($request['dk']['tgl_laporan_diterima']) : null;
                $request['dk']['tgl_pelacakan'] = (isset($request['dk']['tgl_pelacakan'])) ? Helper::dateFormat($request['dk']['tgl_pelacakan']) : null;
                if ($request['id_trx_case']) {
                    Tx::updateData('trx_klinis', ['id_trx_case' => $id_trx_case], $request['dk']);
                    $id_klinis = Tx::getData('trx_klinis', ['id_trx_case' => $id_trx_case])->first()->id;
                } else {
                    $id_klinis = Tx::insertData('trx_klinis', $request['dk']);
                }
                // data notification
                $code_wilayah_faskes = $request['code_wilayah_faskes'];
                // notif klb
                $dtNotification[] = array(
                    'type' => 'klb',
                    'id_trx_case' => $id_trx_case,
                    'from_faskes' => $code_wilayah_faskes,
                    'to_faskes' => $code_kecamatan_pasien,
                );
                $id_role = $request['dc']['id_role'];
                if ($id_role == '1') {
                    if ($code_kecamatan_pasien != $code_wilayah_faskes) {
                        $dtNotification[] = array(
                            'type' => 'cross',
                            'id_trx_case' => $id_trx_case,
                            'from_faskes' => $code_wilayah_faskes,
                            'to_faskes' => $code_kecamatan_pasien,
                        );
                    }
                } else if ($id_role == '2') {
                    if ($code_kabupaten_pasien != $code_wilayah_faskes) {
                        $dtNotification[] = array(
                            'type' => 'cross',
                            'id_trx_case' => $id_trx_case,
                            'from_faskes' => $code_wilayah_faskes,
                            'to_faskes' => $code_kabupaten_pasien,
                        );
                    }
                }
                $notification = Tx::insertData('trx_notification', ['id_trx_case' => $id_trx_case], $dtNotification);
                $success = true;
                $code = 200;
                $trx_case = Tx::getData('trx_case', ['id' => $id_trx_case], ['select' => ['created_at', 'updated_at']])->first();
                $response[] = [
                    'id' => $id_trx_case,
                    'created_at' => date('d-m-Y H:i:s', strtotime($trx_case->created_at)),
                    'updated_at' => date('d-m-Y H:i:s', strtotime($trx_case->updated_at)),
                    'no_epid' => $no_epid,
                ];
                Helper::session_flash('success', 'Berhasil', 'Data berhasil disimpan');
            }
        }

        return Response::json(array(
            'success' => $success,
            'response' => $response,
        ), $code);
    }

    public function viewPe($id_trx_case)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Input data penyelidikan epidemologi kasus ' . $case,
            'data' => array(
                'id_trx_case' => $id_trx_case,
                'id_trx_pe' => null,
            ),
        );
        return view('case.' . $index . '.pe.index', $compact);
    }

    public function postPe($post = '')
    {
        if ($post) {
            $req = $post;
        } else {
            $req = Request::json()->all();
        }
        $response = [];
        foreach ($req as $key => $request) {
            $id_trx_case = $request['id_trx_case'];
            $id_trx_pe = $request['id_trx_pe'];
            if (empty($id_trx_case)) {
                $success = false;
                $response[] = ["error" => "id_trx_case is empty"];
                $code = 400;
                Helper::session_flash('warning', 'Gagal', 'Data gagal di simpan');
            } else {
                // pe
                $request['dpe']['id_trx_case'] = $id_trx_case;
                $request['dpe']['tgl_penyelidikan'] = (isset($request['dpel']['tgl_investigasi'])) ? Helper::dateFormat($request['dpel']['tgl_investigasi']) : date('Y-m-d');
                (isset($request['dpe']['created_at'])) ? $request['dpe']['created_at'] = date('Y-m-d H:i:s', strtotime($request['dpe']['created_at'])) : null;
                (isset($request['dpe']['updated_at'])) ? $request['dpe']['updated_at'] = date('Y-m-d H:i:s', strtotime($request['dpe']['updated_at'])) : null;
                if ($id_trx_pe = $request['id_trx_pe']) {
                    Tx::updateData('trx_pe', ['id' => $id_trx_pe], $request['dpe']);
                } else {
                    $id_trx_pe = Tx::insertData('trx_pe', $request['dpe']);
                }
                // pelapor
                $request['dpel']['id_trx_case'] = $id_trx_case;
                $request['dpel']['id_trx_pe'] = $id_trx_pe;
                $request['dpel']['tgl_laporan'] = (isset($request['dpel']['tgl_laporan'])) ? Helper::dateFormat($request['dpel']['tgl_laporan']) : null;
                $request['dpel']['tgl_investigasi'] = (isset($request['dpel']['tgl_investigasi'])) ? Helper::dateFormat($request['dpel']['tgl_investigasi']) : null;
                if ($request['id_trx_pe']) {
                    Tx::updateData('trx_pelapor', ['id_trx_pe' => $id_trx_pe], $request['dpel']);
                } else {
                    Tx::insertData('trx_pelapor', $request['dpel']);
                }
                // pasien + family
                $request['dp']['tgl_lahir'] = (isset($request['dp']['tgl_lahir'])) ? Helper::dateFormat($request['dp']['tgl_lahir']) : null;
                if ($id_pasien = $request['id_pasien']) {
                    if (isset($request['dp']) && isset($request['df'])) {
                        Tx::updateData('ref_pasien', ['id' => $id_pasien], $request['dp']);
                        Tx::updateData('ref_family', ['id_pasien' => $id_pasien], $request['df']);
                    }
                    $id_family = Tx::getData('ref_family', ['id_pasien' => $id_pasien])->first()->id;
                } else {
                    $id_pasien = Tx::insertData('ref_pasien', $request['dp']);
                    $request['df']['id_pasien'] = $id_pasien;
                    $id_family = Tx::insertData('ref_family', $request['df']);
                }
                // riwayat kesakitan
                $request['dirk']['id_trx_pe'] = $id_trx_pe;
                $request['dirk']['tgl_lahir_bayi'] = (isset($request['dirk']['tgl_lahir_bayi'])) ? Helper::dateFormat($request['dirk']['tgl_lahir_bayi']) : null;
                $request['dirk']['tgl_meninggal'] = (isset($request['dirk']['tgl_meninggal'])) ? Helper::dateFormat($request['dirk']['tgl_meninggal']) : null;
                $request['dirk']['tgl_tidak_mau_menetek'] = (isset($request['dirk']['tgl_tidak_mau_menetek'])) ? Helper::dateFormat($request['dirk']['tgl_tidak_mau_menetek']) : null;
                $request['dirk']['tgl_mulai_kejang'] = (isset($request['dirk']['tgl_mulai_kejang'])) ? Helper::dateFormat($request['dirk']['tgl_mulai_kejang']) : null;
                $request['dirk']['tgl_dirawat'] = (isset($request['dirk']['tgl_dirawat'])) ? Helper::dateFormat($request['dirk']['tgl_dirawat']) : null;
                if ($request['id_trx_pe']) {
                    Tx::updateData('trx_pe_riwayat_kesakitan', ['id_trx_pe' => $id_trx_pe], $request['dirk']);
                } else {
                    Tx::insertData('trx_pe_riwayat_kesakitan', $request['dirk']);
                }
                // riwayat kehamilan
                $request['drk']['id_trx_pe'] = $id_trx_pe;
                $request['drk']['tgl_imunisasi_ibu_tt_pertama'] = (isset($request['drk']['tgl_imunisasi_ibu_tt_pertama'])) ? Helper::dateFormat($request['drk']['tgl_imunisasi_ibu_tt_pertama']) : null;
                $request['drk']['tgl_imunisasi_ibu_tt_kedua'] = (isset($request['drk']['tgl_imunisasi_ibu_tt_kedua'])) ? Helper::dateFormat($request['drk']['tgl_imunisasi_ibu_tt_kedua']) : null;
                if ($request['id_trx_pe']) {
                    Tx::updateData('trx_pe_riwayat_kehamilan', ['id_trx_pe' => $id_trx_pe], $request['drk']);
                } else {
                    Tx::insertData('trx_pe_riwayat_kehamilan', $request['drk']);
                }
                // pemeriksa kehamilan
                if (isset($request['drkp'])) {
                    Tx::postDelete('trx_pe_pemeriksa_kehamilan', ['id_trx_pe' => $id_trx_pe]);
                    $drkp = $request['drkp'];
                    $rowdrkp = [];
                    foreach ($drkp['nama'] as $k => $v) {
                        if (!empty($v)) {
                            $rowdrkp[] = array(
                                'id_trx_pe' => $id_trx_pe,
                                'nama' => $v,
                                'profesi' => $drkp['profesi'][$k],
                                'alamat' => $drkp['alamat'][$k],
                                'frekuensi' => $drkp['frekuensi'][$k],
                            );
                        }
                    }
                    Tx::insertArray('trx_pe_pemeriksa_kehamilan', $rowdrkp);
                }
                // penolong persalinan
                if (isset($request['drpr'])) {
                    Tx::postDelete('trx_pe_penolong_persalinan', ['id_trx_pe' => $id_trx_pe]);
                    $drpr = $request['drpr'];
                    // echo "<pre>";print_r($request);echo "</pre>";
                    $rowdrpr = [];
                    foreach ($drpr['nama'] as $k => $v) {
                        if (!empty($v)) {
                            $rowdrpr[] = array(
                                'id_trx_pe' => $id_trx_pe,
                                'nama' => $v,
                                'profesi' => $drpr['profesi'][$k],
                                'alamat' => $drpr['alamat'][$k],
                                'tempat_persalinan' => $drpr['tempat_persalinan'][$k],
                            );
                        }
                    }
                    Tx::insertArray('trx_pe_penolong_persalinan', $rowdrpr);
                }
                // riwayat persalinan
                $request['drp']['id_trx_pe'] = $id_trx_pe;
                if ($request['id_trx_pe']) {
                    Tx::updateData('trx_pe_riwayat_persalinan', ['id_trx_pe' => $id_trx_pe], $request['drp']);
                } else {
                    Tx::insertData('trx_pe_riwayat_persalinan', $request['drp']);
                }
                // data cakupan
                $request['dtc']['id_trx_pe'] = $id_trx_pe;
                if ($request['id_trx_pe']) {
                    Tx::updateData('trx_pe_data_cakupan', ['id_trx_pe' => $id_trx_pe], $request['dtc']);
                } else {
                    Tx::insertData('trx_pe_data_cakupan', $request['dtc']);
                }
                // pelacak
                if (isset($request['dtpel'])) {
                    Tx::postDelete('trx_pe_pelacak', ['id_trx_pe' => $id_trx_pe]);
                    $dtpel = $request['dtpel'];
                    $rowdtpel = [];
                    foreach ($dtpel['nama'] as $k => $v) {
                        if (!empty($v)) {
                            $rowdtpel[] = array(
                                'id_trx_pe' => $id_trx_pe,
                                'nama' => $v,
                                'jabatan' => $dtpel['jabatan'][$k],
                            );
                        }
                    }
                    Tx::insertArray('trx_pe_pelacak', $rowdtpel);
                }
                $success = true;
                $code = 200;
                $trx_pe = Tx::getData('trx_pe', ['id' => $id_trx_pe], ['select' => ['created_at', 'updated_at']])->first();
                $response[] = [
                    'id' => $id_trx_pe,
                    'created_at' => date('d-m-Y H:i:s', strtotime($trx_pe->created_at)),
                    'updated_at' => date('d-m-Y H:i:s', strtotime($trx_pe->updated_at)),
                ];
                Helper::session_flash('success', 'Berhasil', 'Data berhasil disimpan');
            }
            return Response::json(array(
                'success' => true,
                'response' => $response,
            ), 200);
        }
    }

    public function getDataPeTetanus()
    {
        $request = Request::all();
        $param['roleUser'] = Helper::role();
        $param['query']['where'] = ['id_case' => 4];
        $d = Sql::getData("view_list_pe", $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            foreach ($request['columns'] as $key => $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->editColumn('name_pasien', function ($dt) {
            return ucwords($dt->name_pasien);
        });
        $q->editColumn('nama_ortu', function ($dt) {
            return ucwords($dt->nama_ortu);
        });
        $q->editColumn('no_epid', function ($dt) {
            $no_epid = $dt->no_epid;
            if ($dt->jenis_input == '1') {
                $no_epid .= '<br><span class="label label-info">Web</span>';
            } else if ($dt->jenis_input == '2') {
                $no_epid .= '<br><span class="label label-success">Android</span>';
            } else if ($dt->jenis_input == '3') {
                $no_epid .= '<br><span class="label label-warning">Import</span>';
            }
            return $no_epid;
        });
        $q->addColumn('action', function ($dt) {
            $roleUser = Helper::role();
            $action = "<div class='text-center'>";
            $action .= "<a href=" . url("case/tetanus/pe/detail/" . $dt->id_pe) . " title='Detail data' data-toggle='tooltip' class='btn btn-flat btn-sm btn-success'><i class='fa fa-asterisk'></i></a>";
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'kabupaten'])) {
                $action .= "<a href=" . url("case/tetanus/pe/update/" . $dt->id_pe) . " title='Edit data' data-toggle='tooltip' class='btn btn-flat btn-sm btn-warning'><i class='fa fa-pencil'></i></a>";
            };
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'provinsi', 'pusat'])) {
                $action .= '<a action="' . url('case/tetanus/pe/delete/' . $dt->id_pe) . '" title="Delete data"  class="btn btn-flat btn-sm btn-danger delete" data-toggle="modal" data-target=".modaldelete"><i class="fa fa-remove"></i></a>';
            };
            $action .= '</div>';
            return $action;
        });
        return $q->make(true);
    }

    public function viewDetailPe($id_trx_pe)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $response = $this->getDetailPe($id_trx_pe);
        $dt = json_decode($response->getContent());
        $data = (array) $dt->response[0];
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Detail Pasien Kasus Penyakit ' . $case,
            'data' => array(
                'index' => $index,
                'response' => $data,
            ),
        );
        return view('case.' . $index . '.pe.detail', $compact);
    }

    public function postDeletePe($id = null)
    {
        $rId = Request::json()->all();
        if ($id != null) {
            $rId['query']['delete'][] = $id;
        }
        $response = [];
        foreach ($rId['query']['delete'] as $key => $val) {
            $response[$val] = Tx::deleteData('trx_pe', ['id' => $val]);
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function viewUpdatePe($id_pe)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $cc = Tx::getData('tetanus', ['id_pe' => $id_pe], ['select' => ['id']])->first();
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Update data penyelidikan epidemologi kasus ' . $case,
            'data' => array(
                'id_trx_pe' => $id_pe,
                'id_trx_case' => $cc->id,
            ),
        );
        return view('case.' . $index . '.pe.index', $compact);
    }

    public function getAnalisa($id = '')
    {
        $request = Request::json()->all();
        $index = $this->index;
        if (!empty($request['range'])) {
            $dt = $request['from'];
            if (empty($dt['month'])) {
                $dt['month'] = 0;
            }if (empty($dt['day'])) {
                $dt['day'] = 0;
            }
            $rangeFrom = \Carbon\Carbon::create($dt['year'], $dt['month'], $dt['day']);
            $dt = $request['to'];
            if (empty($dt['month'])) {
                $dt['month'] = 0;
            }if (empty($dt['day'])) {
                $dt['day'] = 0;
            }
            $rangeTo = \Carbon\Carbon::create($dt['year'], $dt['month'], $dt['day']);
            $range = [$rangeFrom, $rangeTo];
            $data['lol'] = $range;
            // $dt = TxCase::getDataAnalisa($request['filter'],$index);
            $dt = TxCase::getDataAnalisaTime($request['filter'], $index, 'tgl_sakit', $range);
        } else {
            $dt = TxCase::getDataAnalisa($request['filter'], $index);
        }
        if (!empty($dt)) {
            // ---getdata for jenis kelamin graph---
            $kelamin = collect($dt)->groupBy('jenis_kelamin');
            foreach ($kelamin as $key => $val) {
                if ($key == 'L') {
                    $data['jenis_kelamin'][] = ['Laki-laki', count($val)];
                } else if ($key == "P") {
                    $data['jenis_kelamin'][] = ['Perempuan', count($val)];
                } else {
                    $data['jenis_kelamin'][] = ['Tidak Jelas', count($val)];
                }
            }

            // ---getdata for waktu graph---
            $waktu = collect($dt)->groupBy(function ($val) {
                return \Carbon\Carbon::parse($val->tgl_mulai_sakit)->format('Y'); // grouping by months
            });
            foreach ($waktu as $keys => $value) {
                $tahun['name'] = 'Tahun ' . $keys;
                $val1 = collect($value)->groupBy(function ($val) {
                    return Carbon::parse($val->tgl_mulai_sakit)->format('m'); // grouping by months
                });
                foreach ($val1 as $k => $v) {
                    $tmp1[intval($k)] = $v; // change month value to int for readable later
                }
                $gettahun = Carbon::parse($value[0]->tgl_mulai_sakit)->format('Y'); //get the year
                for ($i = 01; $i <= 12; $i++) {
                    $bulan['name'] = Carbon::createFromFormat('!m', $i)->format('M');
                    $bulan['drilldown'] = $bulan['name'] . $gettahun;
                    $daysinmonth = intval(cal_days_in_month(CAL_GREGORIAN, $i, $gettahun)); //get days in selected month
                    if (!empty($tmp1[$i])) {
                        $bulan['y'] = count($tmp1[$i]);
                        $tanggal['id'] = $bulan['drilldown'];
                        $tanggal['name'] = $gettahun;
                        // $tanggal['name']=Carbon::createFromFormat('!m', $i)->format('F').' '.$gettahun;
                        $val2 = collect($tmp1[$i])->groupBy(function ($val) {
                            return Carbon::parse($val->tgl_mulai_sakit)->format('d'); // grouping by months
                        });
                        $wew[] = $val2;
                        foreach ($val2 as $k => $v) {
                            $tmp2[intval($k)] = $v; // change days value to int for readable later
                        }

                        for ($j = 1; $j <= $daysinmonth; $j++) {
                            if (!empty($tmp2[$j])) {
                                $tanggal['data'][] = [$j . ' ' . $bulan['name'], count($tmp2[$j])];
                            } else {
                                $tanggal['data'][] = [$j . ' ' . $bulan['name'], 0];
                            }
                        }
                        $tmp2 = [];
                    } else {
                        $bulan['y'] = 0;
                        $tanggal['id'] = $bulan['drilldown'];
                        $tanggal['name'] = Carbon::createFromFormat('!m', $i)->format('F') . ' ' . $gettahun;
                        for ($j = 1; $j <= $daysinmonth; $j++) {
                            $tanggal['data'][] = [$j . ' ' . $bulan['name'], 0];
                        }
                    }
                    $drilldown[] = $tanggal;
                    $tahun['data'][] = $bulan;
                    $bulan = []; //flush array
                    $tanggal = []; //flush array

                }
                $datawaktu[] = $tahun;
                $tahun = [];
                $tmp1 = [];
            }
            $data['waktu']['data'] = $datawaktu;
            $data['waktu']['drilldown'] = $drilldown;
            // ----------iki statik e------------
            // foreach ($waktu as $keys=>$value) {
            //     $tw['name']=$keys;
            //     $val=collect($value)->groupBy(function($val) {
            //             return \Carbon\Carbon::parse($val->tgl_mulai_sakit)->format('m'); // grouping by months
            //     });
            //     foreach($val as $k=>$v) {
            //         $tmpW[intval($k)]=$v;
            //     }
            //     for ($i=1; $i <=12 ; $i++) {
            //         if(!empty($tmpW[$i])){
            //             $tw['data'][]=count($tmpW[$i]);
            //         }else{
            //             $tw['data'][]=0;
            //         }
            //     }
            //     $tempWaktu[]=$tw;
            //     $tw=[];
            //     $tmpW=[];
            // }
            // $data['waktu']=$tempWaktu;

            // ---getdata for umur graph---
            $umur = collect($dt)->groupBy('umur_hari')->sortBy('umur_hari');
            $tempUmur['name'] = 'Umur';
            $tempUmur['colorByPoint'] = false;
            foreach ($umur as $key => $val) {
                if ($key < 5) {
                    $tmpe[0][] = $val;
                } else if ($key >= 5 && $key < 10) {
                    $tmpe[1][] = $val;
                } else if ($key >= 10 && $key < 15) {
                    $tmpe[2][] = $val;
                } else if ($key >= 15 && $key < 29) {
                    $tmpe[3][] = $val;
                } else if ($key >= 29) {
                    $tmpe[4][] = $val;
                }
            }
            for ($i = 0; $i < 5; $i++) {
                if (!empty($tmpe[$i])) {
                    $tempUmur['data'][$i] = count($tmpe[$i][0]);
                } else {
                    $tempUmur['data'][$i] = 0;
                }
            }
            $data['umur'][] = $tempUmur;

            // ---getdata for status imun graph---
            $statimun = collect($dt)->groupBy('status_imunisasi_ibu');
            foreach ($statimun as $key => $val) {
                if ($key == "1") {
                    $data['stat_imun'][] = ['TT2+', count($val)];
                } else if ($key == "2") {
                    $data['stat_imun'][] = ['TT1', count($val)];
                } else if ($key == "3") {
                    $data['stat_imun'][] = ['Tidak Imunisasi', count($val)];
                } else if ($key == "4") {
                    $data['stat_imun'][] = ['Tidak Jelas', count($val)];
                } else {
                    $data['stat_imun'][] = ['Tidak diisi', count($val)];
                }
            }

            // ---getdata for klasifikasi_final graph---
            $klafinal = collect($dt)->groupBy('klasifikasi_final');
            foreach ($klafinal as $key => $val) {
                if ($key == "") {
                    $data['klasifikasi_final'][] = ['Belum di isi', count($val)];
                } else if ($key == "1") {
                    $data['klasifikasi_final'][] = ['Konfirm TN', count($val)];
                } else if ($key == "2") {
                    $data['klasifikasi_final'][] = ['Tersangka TN', count($val)];
                } else if ($key == "3") {
                    $data['klasifikasi_final'][] = ['Bukan TN', count($val)];
                }
            }
        } else {
            $temp['data'][] = array('name' => 'Tahun ' . Carbon::now()->year,
                'data' => [['Jan', 0], ['Feb', 0], ['Mar', 0], ['Apr', 0], ['May', 0], ['Jun', 0], ['Jul', 0], ['Aug', 0], ['Sep', 0], ['Oct', 0], ['Dec', 0]]);
            $data['waktu'] = $temp;
            $data['jenis_kelamin'][] = ['Tidak Ada Data', 0];
            $data['stat_imun'][] = ['Tidak Ada Data', 0];
            $data['klasifikasi_final'][] = ['Tidak Ada Data', 0];
            $data['umur'][] = array(
                'name' => 'Umur',
                'data' => [0, 0, 0, 0, 0],
            );
        }
        $response = $data;
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }
}

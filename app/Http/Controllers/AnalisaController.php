<?php

namespace App\Http\Controllers;

use App\Models\Sql;
use App\Models\Wilayah;
use DB, Response, Request, Carbon\Carbon, Helper;

class AnalisaController extends Controller
{
	function filterAnalisa($case, $request)
	{
		$param['filter'] = json_decode($request['dt']);
		if (!empty($param['filter']->code_role)) {
			$param['roleUser'] = (object) [
				'code_role' => $param['filter']->code_role,
				'id_faskes' => $param['filter']->id_faskes,
				'code_faskes' => $param['filter']->code_faskes
			];
		}
		$fd = json_decode($request['fdata']);
		if (!empty($fd->jenis_kasus)) {
			$param['query']['where'] = [
				'jenis_kasus' => $fd->jenis_kasus,
			];
		}
		if (!empty($fd->jenis_data)) {
			if ($case == 'campak') {
				if ($fd->jenis_data == '2') {
					// Campak (Lab,Epid, dan Klinis)
					$param['query']['whereIn'] = ['klasifikasi_final' => ['1', '2', '3']];
				} elseif ($fd->jenis_data == '6') {
					// Rubella
					$param['query']['whereIn'] = ['klasifikasi_final' => ['4']];
				} elseif ($fd->jenis_data == '3') {
					// Campak Lab
					$param['query']['whereIn'] = ['klasifikasi_final' => ['2']];
				} elseif ($fd->jenis_data == '4') {
					// Campak Epid
					$param['query']['whereIn'] = ['klasifikasi_final' => ['3']];
				} elseif ($fd->jenis_data == '5') {
					// Campak Klinis
					$param['query']['whereIn'] = ['klasifikasi_final' => ['1']];
				}
			} elseif ($case == 'afp') {
				if ($fd->jenis_data == '1') {
					$param['query']['where'] = ['klasifikasi_final' => 1];
				} elseif ($fd->jenis_data == '2') {
					$param['query']['where'] = ['klasifikasi_final' => 2];
				}
			} elseif ($case == 'difteri') {
				if ($fd->jenis_data == '2') {
					// Difteri (Konfirm dan Probable)
					$param['query']['whereIn'] = ['klasifikasi_final' => [1, 2]];
				} elseif ($fd->jenis_data == '3') {
					// Difteri Konfirm
					$param['query']['whereIn'] = ['klasifikasi_final' => [1]];
				} elseif ($fd->jenis_data == '4') {
					// Difteri Probable
					$param['query']['whereIn'] = ['klasifikasi_final' => [2]];
				}
			} elseif ($case == 'tetanus') {
				if ($fd->jenis_data == '2') {
					$param['query']['whereIn'] = ['klasifikasi_final' => [1]];
				}
			} elseif ($case == 'crs') {
				if ($fd->jenis_data == '1') {
					$param['query']['whereIn'] = ['klasifikasi_final' => [1, 2]];
				} elseif ($fd->jenis_data == '2') {
					$param['query']['where'] = ['klasifikasi_final' => 1];
				} elseif ($fd->jenis_data == '3') {
					$param['query']['where'] = ['klasifikasi_final' => 2];
				}
			}
		}
		$ps = ['id_trx_case', 'jenis_kelamin', 'tgl_sakit_date', 'umur_thn', 'klasifikasi_final', 'klasifikasi_final_txt', 'code_provinsi_pasien', 'code_kabupaten_pasien', 'code_kecamatan_pasien', 'code_kelurahan_pasien', 'code_provinsi_faskes', 'code_kabupaten_faskes', 'code_kecamatan_faskes', 'code_faskes', 'jml_imunisasi', 'pin_mopup_ori_biaspolio'];
		$param['query']['select'] = $ps;
		$param['query']['where']['case_type'] = $case;

		return Sql::getData('view_graph', $param)->get();
	}

	function getGraphJenisKelamin($case)
	{
		$request = Request::all();
		$dt = $this->filterAnalisa($case, $request);
		if (!empty($dt)) {
			// ---getdata for jenis kelamin graph---
			$kelamin = collect($dt)->groupBy('jenis_kelamin');
			foreach ($kelamin as $key => $val) {
				if ($key == 'L') {
					$data['jenis_kelamin'][] = ['Laki-laki', count($val)];
				} elseif ($key == 'P') {
					$data['jenis_kelamin'][] = ['Perempuan', count($val)];
				} elseif ($key == 'T') {
					$data['jenis_kelamin'][] = ['Tidak Jelas', count($val)];
				} elseif ($key == null) {
					$data['jenis_kelamin'][] = ['Belum diisi', count($val)];
				}
			}
		} else {
			$data['jenis_kelamin'][] = ['Tidak Ada Data', 0];
		}
		$data['export'] = $data['jenis_kelamin'];
		return Response::json(array(
			'success' => true,
			'response' => $data
		), 200);
	}

	function getGraphWaktu($case)
	{
		$request = Request::all();
		$dt = $this->filterAnalisa($case, $request);
		if (!empty($dt)) {
			// ---getdata for waktu graph---
			$waktu = collect($dt)->groupBy(function ($val) {
				if (!empty($val->tgl_sakit_date)) {
					return \Carbon\Carbon::parse($val->tgl_sakit_date)->format('Y');
				}
			});
			$datawaktu = [];
			$drilldown = [];
			$ex = [];
			foreach ($waktu as $keys => $value) {
				if ($keys) {
					$tahun['name'] = 'Tahun ' . $keys;
					$val1 = collect($value)->groupBy(function ($val) {
						return Carbon::parse($val->tgl_sakit_date)->format('m');
					});
					foreach ($val1 as $k => $v) {
						$tmp1[intval($k)] = $v;
					}
					$gettahun = Carbon::parse($value[0]->tgl_sakit_date)->format('Y');
					for ($i = 01; $i <= 12; $i++) {
						$bulan['name'] = Carbon::createFromFormat('!m', $i)->format('M');
						$bulan['drilldown'] = $bulan['name'] . $gettahun;
						$daysinmonth = intval(cal_days_in_month(CAL_GREGORIAN, $i, $gettahun));
						if (!empty($tmp1[$i])) {
							$bulan['y'] = count($tmp1[$i]);
							$tanggal['id'] = $bulan['drilldown'];
							$tanggal['name'] = $gettahun;
							$val2 = collect($tmp1[$i])->groupBy(function ($val) {
								return Carbon::parse($val->tgl_sakit_date)->format('d');
							});
							foreach ($val2 as $k => $v) {
								$tmp2[intval($k)] = $v;
							}
							for ($j = 1; $j <= $daysinmonth; $j++) {
								if (!empty($tmp2[$j])) {
									$tanggal['data'][] = [$j . ' ' . $bulan['name'], count($tmp2[$j])];
								} else {
									$tanggal['data'][] = [$j . ' ' . $bulan['name'], 0];
								}
							}
							$tmp2 = [];
							$ex[] = [$bulan['name'], $bulan['y']];
						} else {
							$bulan['y'] = 0;
							$tanggal['id'] = $bulan['drilldown'];
							$tanggal['name'] = Carbon::createFromFormat('!m', $i)->format('F') . ' ' . $gettahun;
							for ($j = 1; $j <= $daysinmonth; $j++) {
								$tanggal['data'][] = [$j . ' ' . $bulan['name'], 0];
							}
							$ex[] = [$bulan['name'], $bulan['y']];
						}
						$drilldown[] = $tanggal;
						$tahun['data'][] = $bulan;
						$bulan = [];
						$tanggal = [];
					}
					$datawaktu[] = $tahun;
					$tahun = [];
					$tmp1 = [];
				}
			}
			$data['waktu']['data'] = $datawaktu;
			$data['waktu']['drilldown'] = $drilldown;
			$data['export'] = $ex;
		} else {
			$temp['data'][] = array(
				'name' => 'Tahun ' . Carbon::now()->year,
				'data' => [['Jan', 0], ['Feb', 0], ['Mar', 0], ['Apr', 0], ['May', 0], ['Jun', 0], ['Jul', 0], ['Aug', 0], ['Sep', 0], ['Oct', 0], ['Dec', 0]]
			);
			$data['waktu']	= $temp;
			$data['export'] = [['Jan', 0], ['Feb', 0], ['Mar', 0], ['Apr', 0], ['May', 0], ['Jun', 0], ['Jul', 0], ['Aug', 0], ['Sep', 0], ['Oct', 0], ['Dec', 0]];
		}
		return Response::json(array(
			'success' => true,
			'response' => $data
		), 200);
	}

	function getGraphUmur($case)
	{
		$request = Request::all();
		$dt = $this->filterAnalisa($case, $request);
		$u = Helper::getCategoryUmur($case);
		if (!empty($dt)) {
			// ---getdata for umur graph---
			$umur = collect($dt)->groupBy('umur_thn')->sortBy('umur_thn');
			$tempUmur['name'] = 'Umur';
			$tempUmur['colorByPoint']	= false;
			if ($case == 'crs') {
				foreach ($umur as $key => $val) {
					if ($key < 6) {
						$tmpe[0][] = $val;
					} else if ($key >= 6) {
						$tmpe[1][] = $val;
					}
				}
			} else if ($case == 'tetanus') {
				foreach ($umur as $key => $val) {
					if ($key >= 0 && $key < 5) {
						$tmpe[0][] = $val;
					} else if ($key >= 5 && $key < 10) {
						$tmpe[1][] = $val;
					} else if ($key >= 10 && $key < 15) {
						$tmpe[2][] = $val;
					} else if ($key >= 15 && $key < 28) {
						$tmpe[3][] = $val;
					} else if ($key >= 28) {
						$tmpe[4][] = $val;
					}
				}
			} else {
				foreach ($umur as $key => $val) {
					if ($key < 1) {
						$tmpe[0][] = $val;
					} else if ($key >= 1 && $key < 5) {
						$tmpe[1][] = $val;
					} else if ($key >= 5 && $key < 10) {
						$tmpe[2][] = $val;
					} else if ($key >= 10 && $key < 15) {
						$tmpe[3][] = $val;
					} else if ($key >= 15) {
						$tmpe[4][] = $val;
					}
				}
			}
			for ($i = 0; $i < count($u); $i++) {
				if (!empty($tmpe[$i])) {
					$tempUmur['data'][$i] = count($tmpe[$i][0]);
					$ex[] = [$u[$i], count($tmpe[$i][0])];
				} else {
					$tempUmur['data'][$i] = 0;
					$ex[] = [$u[$i], 0];
				}
			}
			$data['umur'][] = $tempUmur;
			$data['export'] = $ex;
		} else {
			if ($case == 'crs') {
				$du = [0, 0];
				$de = [[$u[0], 0], [$u[1], 0]];
			} else {
				$du = [0, 0, 0, 0, 0];
				$de = [[$u[0], 0], [$u[1], 0], [$u[2], 0], [$u[3], 0], [$u[4], 0]];
			}
			$data['umur'][] =	array(
				'name' => 'Umur',
				'data' => $du
			);
			$data['export'] = $de;
		}
		return Response::json(array(
			'success' => true,
			'response' => $data
		), 200);
	}

	function getGraphStatusImunisasi($case)
	{
		$request = Request::all();
		$dt = $this->filterAnalisa($case, $request);
		if (!empty($dt)) {
			// ---getdata for status imun graph---
			$statimun = collect($dt)->groupBy('jml_imunisasi');
			if ($case == 'afp') {
				$x = json_decode($request['fdata']);
				if ($x->jenis_imun == 2) {
					$statimun = collect($dt)->groupBy('jml_pin_mopup_ori_biaspolio');
				}
			}
			if ($case == 'tetanus') {
				foreach ($statimun as $key => $val) {
					if ($key == "1") {
						$data['stat_imun'][] = ['TT2+', count($val)];
					} else if ($key == "2") {
						$data['stat_imun'][] = ['TT1', count($val)];
					} else if ($key == "3") {
						$data['stat_imun'][] = ['Tidak Imunisasi', count($val)];
					} else if ($key == "3") {
						$data['stat_imun'][] = ['Belum di isi', count($val)];
					} else {
						$data['stat_imun'][] = ['Tidak Jelas', count($val)];
					}
				}
			} else {
				foreach ($statimun as $key => $val) {
					if ($key == "7") {
						$data['stat_imun'][] = ['Tidak', count($val)];
					} else if ($key == "8") {
						$data['stat_imun'][] = ['Tidak tahu', count($val)];
					} else if ($key == "9") {
						$data['stat_imun'][] = ['Belum pernah', count($val)];
					} else if ($key == "") {
						$data['stat_imun'][] = ['Belum di isi', count($val)];
					} else {
						$data['stat_imun'][] = [$key . ' kali', count($val)];
					}
				}
			}
		} else {
			$data['stat_imun'][]	= ['Tidak Ada Data', 0];
		}
		$data['export'] = $data['stat_imun'];
		return Response::json(array(
			'success' => true,
			'response' => $data
		), 200);
	}

	function getGraphKlasifikasiFinal($case)
	{
		$request = Request::all();
		$dt = $this->filterAnalisa($case, $request);
		if (!empty($dt)) {
			// ---getdata for klasifikasi_final graph---
			$klafinal = collect($dt)->groupBy('klasifikasi_final');
			foreach ($klafinal as $key => $val) {
				if ($key == "" or $key == '0') {
					$data['klasifikasi_final'][] = ['Belum di isi', count($val)];
				} else {
					$data['klasifikasi_final'][] = [$val[0]->klasifikasi_final_txt, count($val)];
				}
			}
		} else {
			$data['klasifikasi_final'][]	= ['Tidak Ada Data', 0];
		}
		$data['export'] = $data['klasifikasi_final'];
		return Response::json(array(
			'success' => true,
			'response' => $data
		), 200);
	}

	function getGraphWilayah($case)
	{
		$request = Request::all();
		$dt = $this->filterAnalisa($case, $request);
		$filter = json_decode($request['dt']);
		if ($filter->filter_type == '1') {
			$filterprov = 'code_provinsi_pasien';
			$filterkab = 'code_kabupaten_pasien';
			$filterkec = 'code_kecamatan_pasien';
			$filterkel = 'code_kelurahan_pasien';
		} else {
			$filterprov = 'code_provinsi_faskes';
			$filterkab = 'code_kabupaten_faskes';
			$filterkec = 'code_kecamatan_faskes';
			$filterpus = 'code_faskes';
		}
		if (!empty($dt)) {
			// ---getdata for wilayah nasional---
			$wilayahnasional = collect($dt)->groupBy($filterprov);
			$listprovince = Helper::getProvince();
			foreach ($listprovince as $keys => $values) {
				if (!empty($wilayahnasional[$keys])) {
					$senasional['name'] = $values;
					$senasional['y'] = count($wilayahnasional[$keys]);
					$senasional['drilldown'] = $values;
					//----get data per provinsi (drilldown liat data kabupaten)----
					$wilayahprov = collect($wilayahnasional[$keys])->groupBy($filterkab);
					$listkab = Wilayah::getKabupaten(array('code_provinsi' => $keys));
					$province['name']	= $values;
					$province['id'] = $values;
					foreach ($listkab as $key => $value) {
						if (!empty($wilayahprov[$key])) {
							$kab['name'] = $value->name;
							$kab['y'] = count($wilayahprov[$key]);
							$kab['drilldown'] = $value->name;
							$wilayahkab = collect($wilayahprov[$key])->groupBy($filterkec);
							$listkec = Wilayah::getKecamatan(array('code_kabupaten' => $key));
							$kabupaten['name'] = $value->name;
							$kabupaten['id'] = $value->name;
							foreach ($listkec as $ky => $val) {
								if (!empty($wilayahkab[$ky])) {
									$kec['name'] = $val->name;
									$kec['y'] = count($wilayahkab[$ky]);
									$kec['drilldown'] = $val->name;
									if ($filter->filter_type == '1') {
										$wilayahkec = collect($wilayahkab[$ky])->groupBy($filterkel);
										$lists = Wilayah::getKelurahan(array('code_kecamatan' => $ky));
									} else {
										$wilayahkec = collect($wilayahkab[$ky])->groupBy($filterpus);
										$lists = Wilayah::getPuskesmas(array('code_kecamatan' => $ky));
									}

									$kecamatan['name'] = $val->name;
									$kecamatan['id'] = $val->name;
									foreach ($lists as $k => $v) {
										if (!empty($wilayahkec[$k])) {
											$kel['name'] = $v->name;
											$kel['y'] = count($wilayahkec[$k]);
										} else {
											$kel['name'] = $v->name;
											$kel['y'] = 0;
										}
										$kecamatan['data'][] = $kel;
										$kel = [];
									}
									$data['lol'] = $wilayahkec;
									$drilldown[] = $kecamatan;
									$kecamatan = [];
								} else {
									$kec['name'] = $val->name;
									$kec['y'] = 0;
								}
								$kabupaten['data'][] = $kec;
								$kec = [];
							}
							$drilldown[] = $kabupaten;
							$kabupaten = [];
						} else {
							$kab['name'] = $value->name;
							$kab['y'] = 0;
						}
						$province['data'][] = $kab;
						$kab = [];
					}
					$drilldown[] = $province;
					$province = [];
				} else {
					$senasional['name'] = $values;
					$senasional['y'] = 0;
					$drilldown[] = [0];
				}

				$datawilayah['data'][] = $senasional;
				$senasional = [];
			}
			$datawilayah['name'] = 'Indonesia';
			$datawilayah['colorByPoint'] = false;
			$tmpwil['data'][] = $datawilayah;
			$tmpwil['drilldown'] = $drilldown;
			$data['data'] = $tmpwil;
		} else {
			$listprovince = Helper::getProvince();
			foreach ($listprovince as $keys => $value) {
				$senasional['name'] = $value;
				$senasional['y'] = 0;
				$datawilayah['data'][] = $senasional;
				$senasional = [];
			}
			$datawilayah['name']	= 'Indonesia';
			$datawilayah['colorByPoint'] = false;
			$data['data'][] = $datawilayah;
		}
		return Response::json(array(
			'success' => true,
			'response' => $data
		), 200);
	}
}

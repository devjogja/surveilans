<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Request, Response, DB;
use App\Models\Tx;
use App\Models\Rumahsakit;
use App\Libraries\Datatable;

class RumahsakitController extends Controller
{
	public function index()
	{
		$compact = array(
			'title'         => 'Surveilans PD3I',
			'contentTitle'  => 'Master Rumah sakit',
			'data'          => array()
			);
		return view('faskes.rumahsakit',$compact);
	}

	public function getData()
	{
		$config['table'] = 'view_rumahsakit AS a';
		$config['coloumn'] = ['id','code_faskes','name','alamat','telp','konfirm_code'];
		$config['columnSelect'] = ['a.id','a.code_faskes','a.name','a.alamat','a.telp','a.konfirm_code'];
		$config['columnFormat'] = ['number','','','','',''];
		$config['searchColumn'] = ['a.code_faskes','a.name'];
		$config['key'] = 'a.id';
		$config['action'][]		= array(
			'action'	=> 'editMaster',
			'url' 		=> url('faskes/rumahsakit/edit'),
			'key'		=> 'id',
			);
		$config['action'][]		= array(
			'action'	=> 'delete',
			'url'		=> url('faskes/rumahsakit/delete'),
			'key'		=> 'id',
			);
		$data = Datatable::getData($config);
		echo json_encode($data);
	}

	public function postData()
	{
		$id = Request::get('id');
		$data 	= Request::get('dt');
		if(empty($id)){
			$result = DB::table('mst_rumahsakit')->insert($data);
		}else{
			$result = DB::table('mst_rumahsakit')->where(['id'=>$id])->update($data);
		}
		if($result){
			$response 	= array(
				'success'	=> true,
				'messageType'	=> 'info',
				'title'		=> 'Berhasil!',
				'message'	=> 'Data Rumahsakit berhasil disimpan',
				);
		}else{
			$response 	= array(
				'success'	=> false,
				'messageType'	=> 'danger',
				'title'		=> 'Gagal!',
				'message'	=> 'Data Rumahsakit gagal disimpan',

				);
		}
		echo(json_encode($response));
	}

	public function postDelete($id)
	{
		if(Tx::deleteData('mst_rumahsakit',['id'=>$id]))
		{
			$response 	= array(
				'success'	=> 1,
				'messageType'	=> 'info',
				'title'		=> 'Berhasil!',
				'message'	=> 'Data Rumahsakit berhasil dihapus',
				);
		}else{
			$response 	= array(
				'success'	=> 0,
				'messageType'	=> 'danger',
				'title'		=> 'Gagal!',
				'message'	=> 'Data Rumahsakit gagal dihapus',
				);
		}
		echo(json_encode($response));
	}

	public function getRS()
	{
        $where['a.code_provinsi'] = Request::get('code_provinsi');
		$where['a.code_kabupaten'] = Request::get('code_kabupaten');
		$query = Rumahsakit::getData($where);
		echo json_encode($query);
	}

	public function getDetail($id)
	{
		$response = Tx::getData('view_rumahsakit',['id'=>$id])->first();
		return Response::json(array(
			'success'=>true,
			'response'=>$response
			), 200);
	}
}

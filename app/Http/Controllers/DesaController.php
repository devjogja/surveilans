<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Tx;
use App\Libraries\Datatable;

class DesaController extends Controller
{
	public function index()
	{
		$compact = array(
			'title'         => 'Surveilans PD3I',
			'contentTitle'  => 'Master Desa/Kelurahan',
			'data'          => array()
			);
		return view('district.desa',$compact);
	}

	public function getData()
	{
		$config['table'] = 'mst_kelurahan AS a';
		$config['coloumn'] = ['code','code_kecamatan','code','name'];
		$config['columnSelect'] = ['a.code','a.code_kecamatan','a.code','a.name'];
		$config['columnFormat'] = ['number','','',''];
		$config['searchColumn'] = ['a.code_kecamatan','a.code','a.name'];
		$config['where'][] = ['a.deleted_at'=>null];
		$config['key'] = 'a.code';
		$config['action'][]		= array(
			'action'	=> 'editMaster',
			'url' 		=> url('district/desa/edit'),
			'key'		=> 'code',
			);
		$config['action'][]		= array(
			'action'	=> 'delete',
			'url'		=> url('district/desa/delete'),
			'key'		=> 'code',
			);
		$data = Datatable::getData($config);
		echo json_encode($data);
	}

	public function postData()
	{
		$id = Request::get('id');
		$data 	= Request::get('dt');
		if(empty($id)){
			$result = Tx::insertData('mst_kelurahan',$data);
		}else{
			if (Tx::getData('mst_kelurahan',['code'=>$data['code']])->first()) {
				unset($data['code']);
			}
			$result = Tx::updateData('mst_kelurahan',['code'=>$id], $data);
		}
		if($result){
			$response 	= array(
				'success'	=> true,
				'messageType'	=> 'info',
				'title'		=> 'Berhasil!',
				'message'	=> 'Data Desa berhasil disimpan',
			);
		}else{
			$response 	= array(
				'success'	=> false,
				'messageType'	=> 'danger',
				'title'		=> 'Gagal!',
				'message'	=> 'Data Desa gagal disimpan',

			);
		}
		echo(json_encode($response));
	}

	public function getDetail($id='')
	{
		$rDesa = Tx::getData('mst_kelurahan',['code'=>$id])->first();
		$rMapping_area = Tx::getData('view_area',['code_kecamatan'=>$rDesa->code_kecamatan])->first();
		$response = [
			'code_provinsi'=>$rMapping_area->code_provinsi,
			'code_kabupaten'=>$rMapping_area->code_kabupaten,
			'name_kabupaten'=>$rMapping_area->name_kabupaten,
			'code_kecamatan'=>$rMapping_area->code_kecamatan,
			'name_kecamatan'=>$rMapping_area->name_kecamatan,
			'code'=>$rDesa->code,
			'name'=>$rDesa->name,
		];
		echo(json_encode($response));
	}

	public function postDelete($id)
	{
		if(Tx::postDelete('mst_kelurahan',['code'=>$id]))
		{
			$response 	= array(
				'success'	=> 1,
				'messageType'	=> 'info',
				'title'		=> 'Berhasil!',
				'message'	=> 'Data Desa berhasil dihapus',
			);
		}else{
			$response 	= array(
				'success'	=> 0,
				'messageType'	=> 'danger',
				'title'		=> 'Gagal!',
				'message'	=> 'Data Desa gagal dihapus',
			);
		}
		echo(json_encode($response));
	}
}
<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Faq;
use Mail, DB;

class HelpController extends Controller {
	public function index() {
		$data['faq']    = Faq::getData();
		$data['course'] = Course::getData();
		$compact        = array(
			'title'        => 'Surveilans PD3I',
			'contentTitle' => null,
			'dataFaq'      => $data['faq'],
			'dataCourse'   => $data['course'],
			);
		return view('help.index', $compact);
	}

	public function konfirmCode()
	{
		$puskesmas = DB::table('mst_puskesmas')->select('id')->get();
		foreach ($puskesmas as $key => $val) {
			DB::table('mst_puskesmas')->where('id',$val->id)->update(['konfirm_code'=>'12345']);
		}
		$kabupaten = DB::table('mst_kabupaten')->select('id')->get();
		foreach ($kabupaten as $key => $val) {
			DB::table('mst_kabupaten')->where('id',$val->id)->update(['konfirm_code'=>'12345']);
		}
		$provinsi = DB::table('mst_provinsi')->select('id')->get();
		foreach ($provinsi as $key => $val) {
			DB::table('mst_provinsi')->where('id',$val->id)->update(['konfirm_code'=>'12345']);
		}
	}

	public function sendMail()
	{
		$data['a'] = 'a';
		Mail::send('welcome', $data, function ($message) {
			$message->from('dev.jogja@gmail.com', 'Laravel');
			$message->to('boerhan.jogja@gmail.com');
		});
	}

	public function dropTransaksi()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0');
		DB::table('trx_case')->truncate();
		DB::table('trx_gejala')->truncate();
		DB::table('trx_gejala_family')->truncate();
		DB::table('trx_hasil_lab')->truncate();
		DB::table('trx_hkf')->truncate();
		DB::table('trx_informasi_kunjungan_ulang')->truncate();
		DB::table('trx_itd')->truncate();
		DB::table('trx_klinis')->truncate();
		DB::table('trx_klinis_family')->truncate();
		DB::table('trx_komplikasi')->truncate();
		DB::table('trx_ku60')->truncate();
		DB::table('trx_kultur_sel')->truncate();
		DB::table('trx_lab_campak')->truncate();
		DB::table('trx_notification')->truncate();
		DB::table('trx_pe')->truncate();
		DB::table('trx_pe_data_cakupan')->truncate();
		DB::table('trx_pe_hasil_pemeriksaan')->truncate();
		DB::table('trx_pe_kasus_tambahan')->truncate();
		DB::table('trx_pe_kontak_kasus')->truncate();
		DB::table('trx_pe_pelacak')->truncate();
		DB::table('trx_pe_pelaksana')->truncate();
		DB::table('trx_pe_pemeriksa_kehamilan')->truncate();
		DB::table('trx_pe_penolong_persalinan')->truncate();
		DB::table('trx_pe_riwayat_kehamilan')->truncate();
		DB::table('trx_pe_riwayat_kesakitan')->truncate();
		DB::table('trx_pe_riwayat_kontak')->truncate();
		DB::table('trx_pe_riwayat_pengobatan')->truncate();
		DB::table('trx_pe_riwayat_persalinan')->truncate();
		DB::table('trx_pe_sumber_informasi')->truncate();
		DB::table('trx_pelapor')->truncate();
		DB::table('trx_pemeriksaan_igm')->truncate();
		DB::table('trx_pemeriksaan_itd')->truncate();
		DB::table('trx_pemeriksaan_kultur')->truncate();
		DB::table('trx_pemeriksaan_kultur_sel')->truncate();
		DB::table('trx_pemeriksaan_pcr')->truncate();
		DB::table('trx_pemeriksaan_sequencing')->truncate();
		DB::table('trx_spesimen')->truncate();
		DB::table('ref_pasien')->truncate();
		DB::table('ref_family')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1');
		echo true;
	}
}

<?php

namespace App\Http\Controllers;

use App\Models\Sql;
use App\Models\Tx;
use App\Models\TxCase;
use App\Models\TxSampelLab;
use Carbon\Carbon;
use Datatables;
use DB;
use Excel;
use Helper;
use Request;
use Response;
use Validator;

class AfpController extends Controller
{
    protected $index;
    public function __construct()
    {
        $this->index = 'afp';
    }

    public function index()
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Penyakit ' . $dc[0]->name,
            'data' => array(
                'id_trx_case' => null,
                'index' => $index,
            ),
        );
        return view('case.' . $index . '.index', $compact);
    }

    public function export($request = '')
    {
        $param['filter'] = json_decode($request);
        if (!empty($param['filter']->code_role)) {
            $param['roleUser'] = (object) array(
                'code_role' => $param['filter']->code_role,
                'id_faskes' => $param['filter']->id_faskes,
                'code_faskes' => $param['filter']->code_faskes,
            );
        }
        $q = Sql::getData("afp", $param);
        $q->join("afp_spesimen AS b", "a.id_trx_case", '=', "b.id_trx_case", 'left');
        $q->join("afp_pe AS c", "a.id_trx_case", '=', "c.id_trx_case", 'left');
        $r = $q->get();

        $dd = $arr = [];
        foreach ($r as $key => $val) {
            $val = (array) $val;
            array_push($dd, $val);

            if (!isset($arr[$val['id_trx_case']])) {
                $arr[$val['id_trx_case']]['rowspan'] = 0;
            }
            $arr[$val['id_trx_case']]['p'] = 'no';
            $arr[$val['id_trx_case']]['rowspan'] += 1;
        }

        $compact['dd'] = $dd;
        $compact['arr'] = $arr;
        // return view('case.afp.export', $compact);
        $export = view('case.afp.export', $compact);
        $filename = 'export_afp-' . date('YmdHi') . '.xls';

        header("Content-type: application/vnd.ms-excel; name='excel'");
        header("Cache-Control: max-age=0");
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Content-Transfer-Encoding: text');
        echo $export;
    }

    public function indexLab()
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Laboratorium Penyakit ' . $dc[0]->name,
            'data' => array(
                'id_trx_case' => null,
                'index' => $index,
            ),
        );
        return view('lab.case.' . $index . '.index', $compact);
    }

    public function viewAnalisa()
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Analisa Penyakit ' . $dc[0]->name,
            'data' => array(
                'id_trx_case' => null,
                'index' => $index,
            ),
        );
        return view('case.' . $index . '.analisa_mobile', $compact);
    }

    public function moveCase($id = null)
    {
        if (!empty($id)) {
            $role = Helper::role();
            $case = Tx::getData('afp', ['id' => $id], ['select' => ['code_kelurahan_pasien', 'id_faskes']])->first();
            $code_kelurahan_pasien = $case->code_kelurahan_pasien;
            $code_faskes_old = $case->id_faskes;
            $code_faskes = Tx::getData('mst_wilayah_kerja_puskesmas', ['code_kelurahan' => $code_kelurahan_pasien], ['code_faskes'])->first();
            if (!empty($code_faskes)) {
                $faskes = Tx::getData('mst_puskesmas', ['code_faskes' => $code_faskes->code_faskes])->first();
                $dt = [
                    'id_faskes' => $faskes->id,
                    'id_faskes_old' => $code_faskes_old,
                    'id_role' => '1',
                    'id_role_old' => $role->id_role,
                ];
                $status = Tx::movedData('trx_case', ['id' => $id], $dt);
            } else {
                $status = false;
            }
        }
        return Response::json(array(
            'success' => $status,
        ), 200);
    }

    public function getDataAfp()
    {
        $request = Request::all();
        $param['filter'] = json_decode($request['dt']);
        $param['roleUser'] = Helper::role();
        $param['query']['select'] = ['id_trx_case', 'no_epid', 'name_pasien', 'umur_hari', 'umur_bln', 'umur_thn', 'code_kecamatan_pasien', 'full_address', 'keadaan_akhir_txt', 'klasifikasi_final_txt', 'id_role', 'name_faskes', 'code_kecamatan_faskes', 'jenis_input', 'status_kasus', 'id_pe', 'id_faskes_old', 'id_role_old', 'id_ku60', 'id_hkf', 'tgl_input', 'tgl_update', 'jenis_input_text', 'pe_text', 'hot_case', 'hot_case_txt'];
        $param['query']['where'] = ['id_case' => 2];
        $d = Sql::getData("view_list_case", $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            foreach ($request['columns'] as $key => $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->editColumn('name_pasien', function ($dt) {
            $name_pasien = ucwords($dt->name_pasien);
            if ($dt->hot_case == '1') {
                $name_pasien .= '<br><span class="label label-danger">Hot Case</span>';
            }
            return $name_pasien;
        });
        $q->editColumn('no_epid', function ($dt) {
            $no_epid = $dt->no_epid;
            if ($dt->jenis_input == '1') {
                $no_epid .= '<br><span class="label label-info">Web</span>';
            } else if ($dt->jenis_input == '2') {
                $no_epid .= '<br><span class="label label-success">Android</span>';
            } else if ($dt->jenis_input == '3') {
                $no_epid .= '<br><span class="label label-warning">Import</span>';
            }
            if ($dt->id_role == '2') {
                $no_epid .= "<span class='label label-primary' style='background-color:#b503b5 !important;'> Data dari rumahsakit </span>";
            }
            if ($dt->id_faskes_old) {
                $fo = '';
                if ($dt->id_role_old == '1') {
                    $fo = 'PUSKESMAS ' . DB::table('mst_puskesmas')->select(['name'])->where('id', $dt->id_faskes_old)->first()->name;
                } else if ($dt->id_role_old == '2') {
                    $fo = DB::table('mst_rumahsakit')->select(['name'])->where('id', $dt->id_faskes_old)->first()->name;
                }
                $no_epid .= "<span class='label green'> Data kiriman dari " . $fo . "</span>";
            }
            return $no_epid;
        });
        $q->editColumn('id_pe', function ($dt) {
            $roleUser = Helper::role();
            $disabled = '';
            if (!in_array($roleUser->code_role, ['puskesmas', 'rs'])) {
                $disabled = "disabled='disabled' onclick='return false;'";
            }
            $pe = '<a href="' . url('case/afp/pe/' . $dt->id_trx_case) . '"' . $disabled . ' title="Penyelidikan Epidemologi" data-toggle="tooltip" class="btn btn-sm btn-flat btn-success">PE</a>';
            if ($dt->id_pe) {
                $pe = '<a title="Penyelidikan Epidemologi" data-toggle="tooltip" class="btn btn-sm btn-flat btn-danger">Sudah PE</a>';
            }
            return $pe;
        });
        $q->editColumn('id_ku60', function ($dt) {
            $ku60 = '<a href="' . url('case/afp/ku60/' . $dt->id_trx_case) . '" title="Penyelidikan KU60" data-toggle="tooltip" class="btn btn-sm btn-flat btn-success">KU60</a>';
            if ($dt->id_ku60) {
                $ku60 = '<a title="Penyelidikan KU60" data-toggle="tooltip" class="btn btn-sm btn-flat btn-danger">Sudah KU60</a>';
            }
            return $ku60;
        });
        $q->editColumn('id_hkf', function ($dt) {
            $hkf = '<a href="' . url('case/afp/hkf/' . $dt->id_trx_case) . '" title="Form Hasil Klasifikasi Final" data-toggle="tooltip" class="btn btn-sm btn-flat btn-success">HKF</a>';
            if ($dt->id_hkf) {
                $hkf = '<a title="Form Hasil Klasifikasi Final" data-toggle="tooltip" class="btn btn-sm btn-flat btn-danger">Sudah HKF</a>';
            }
            return $hkf;
        });
        $q->addColumn('action', function ($dt) {
            $roleUser = Helper::role();
            $action = "<a href=" . url("case/afp/detail/" . $dt->id_trx_case) . " title='Detail data' data-toggle='tooltip' class='btn btn-sm btn-flat btn-success'><i class='fa fa-asterisk'></i></a>";
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'kabupaten'])) {
                $action .= "<a href=" . url("case/afp/update/" . $dt->id_trx_case) . " title='Edit data' data-toggle='tooltip' class='btn btn-sm btn-flat btn-warning'><i class='fa fa-pencil'></i></a>";
            };
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'provinsi', 'pusat'])) {
                $action .= '<a action="' . url('case/afp/delete/' . $dt->id_trx_case) . '" title="Delete data"  class="btn btn-sm btn-flat btn-danger delete" data-toggle="modal" data-target=".modaldelete"><i class="fa fa-remove"></i></a>';
            };
            if (in_array($roleUser->code_role, ['puskesmas']) and $dt->id_role == '1') {
                if ($dt->code_kecamatan_faskes != $dt->code_kecamatan_pasien) {
                    $action .= '<a action="' . url('case/afp/move/' . $dt->id_trx_case) . '" title="Move data" data-toggle="modal" class="btn btn-sm btn-flat btn-primary move" data-target=".modalmove"><i class="fa fa-step-forward"></i></a>';
                }
            }
            return $action;
        });
        return $q->make(true);
    }

    public function getEpid($param = [])
    {
        if (!empty($param)) {
            $request = $param;
        } else {
            $request = Request::json()->all();
        }
        $case = Helper::getCase(array('alias' => $this->index));
        $code = $case[0]->code;
        $id_kelurahan = $request['id_kelurahan'];
        $thn_case = $cthn = '';
        $q = DB::table('view_list_case');
        $q->where('id_case', 2);
        $q->select(DB::raw('MAX(no_epid) AS no_epid'));
        if ($request['tgl_sakit']) {
            $thn_case = \Carbon\Carbon::parse($request['tgl_sakit'])->format('Y');
            $cthn = \Carbon\Carbon::parse($request['tgl_sakit'])->format('y');
            $q->where('thn_sakit', $thn_case);
        }
        $q->where('code_kelurahan_pasien', $id_kelurahan);
        $cekEpid = $q->first();

        if (!empty($cekEpid->no_epid)) {
            $cc = substr($cekEpid->no_epid, -3);
            $no = str_pad($cc + 1, 3, 0, STR_PAD_LEFT);
        } else {
            $no = '001';
        }
        $no_epid = $code . $id_kelurahan . $cthn . $no;

        return Response::json(array(
            'response' => $no_epid,
        ));
    }

    public function postDelete($id = null)
    {
        $rId = Request::json()->all();
        if ($id != null) {
            $rId['query']['delete'][] = $id;
        }
        $response = [];
        foreach ($rId['query']['delete'] as $key => $val) {
            $response[$val] = Tx::deleteData('trx_case', ['id' => $val]);
            // Tx::deleteData('trx_klinis',['id_trx_case'=>$val]);
            // Tx::deleteData('trx_pe',['id_trx_case'=>$val]);
            // Tx::deleteData('trx_spesimen',['id_trx_case'=>$val]);
            // Tx::postDelete('trx_notification',['id_trx_case'=>$val]);
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function postDeleteHkf($id = null)
    {
        $rId = Request::json()->all();
        if ($id != null) {
            $rId['query']['delete'][] = $id;
        }
        $response = [];
        foreach ($rId['query']['delete'] as $key => $val) {
            $response[$val] = Tx::deleteData('trx_hkf', ['id' => $val]);
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function postDeleteKu60($id = null)
    {
        $rId = Request::json()->all();
        if ($id != null) {
            $rId['query']['delete'][] = $id;
        }
        $response = [];
        foreach ($rId['query']['delete'] as $key => $val) {
            $response[$val] = Tx::deleteData('trx_ku60', ['id' => $val]);
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function postDeleteSampel($id)
    {
        $request = array('id' => $id);
        if ($resp = TxSampelLab::postDeleteSampel($id)) {
            $success = true;
            $response = $resp;
            $code = '200';
        } else {
            $success = false;
            $response = $resp;
            $code = '401';
        }

        return Response::json(array(
            'success' => $success,
            'request' => $request,
            'response' => $response,
        ), $code);
    }

    public function viewDetail($id)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $response = $this->getDetail($id);
        $dt = json_decode($response->getContent());
        $data = $dt->response[0];
        $dtGejala = [];
        $i = 1;
        foreach ($data->dtGejala as $key => $val) {
            if ($val->id_gejala) {
                $dtGejala[$val->id_gejala] = $val;
            } else {
                $dtGejala[$i++] = $val;
            }
        }
        $data->dtGejala = $dtGejala;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Detail Pasien Kasus Penyakit ' . $case,
            'data' => array(
                'index' => $index,
                'response' => $data,
            ),
        );
        return view('case.' . $index . '.detail', $compact);
    }

    public function getDetail($id = '')
    {
        if ($id) {
            $request = [
                'where' => ['id' => [$id]],
                'query' => ['select' => ['*', 'dtGejala', 'dtSpesimen']],
            ];
        } else {
            $request = Request::json()->all();
        }
        $qQuery = [];
        if (isset($request['where'])) {
            foreach ($request['where'] as $key => $val) {
                if ($val[0] == '*') {
                    $qQuery['where'][$key] = '*';
                } else {
                    $qQuery['whereIn'][$key] = $val;
                }
            }
            if (isset($request['where']['code_roles'])) {
                if ($request['where']['code_roles'][0] == 'puskesmas') {
                    $c = [];
                    foreach ($request['where']['id_faskes'] as $key => $val) {
                        $codeKelurahanKerjaPkm = Tx::getData('view_wilayah_kerja_puskesmas', ['id_faskes' => $val], ['select' => ['code_kelurahan']])->get('code_kelurahan');
                        foreach ($codeKelurahanKerjaPkm as $k => $v) {
                            $c[] = $v->code_kelurahan;
                        }
                    }
                    $qQuery['orWhere']['whereIn'] = ['code_kelurahan_pasien' => $c, 'code_roles' => ['rs']];
                }
            }
        }
        if (isset($request['query'])) {
            foreach ($request['query'] as $key => $val) {
                foreach ($val as $v) {
                    if (!in_array($v, ["dtGejala", "dtSpesimen"])) {
                        $var[] = $v;
                    }
                }
                $qQuery[$key] = $var;
            }
        }
        if (!empty($request['pagination'])) {
            $qQuery['pagination'] = $request['pagination'];
        }
        $resp = Tx::getData('afp', [], $qQuery)->get();
        $response = [];
        foreach ($resp as $key => $val) {
            $dt = (array) $val;
            if (isset($request['query'])) {
                foreach ($request['query'] as $k => $v) {
                    foreach ($v as $var) {
                        ($var == 'dtGejala') ? $dt['dtGejala'] = Tx::getData('view_gejala', ['id_trx_klinis' => $val->id_trx_klinis], ['select' => ['id_gejala', 'name', 'val_kelumpuhan', 'val_kelumpuhan_txt', 'val_gangguan_raba', 'val_gangguan_raba_txt', 'other_gejala']])->get() : null;
                        ($var == 'dtSpesimen') ? $dt['dtSpesimen'] = Tx::getData('afp_spesimen', ['id_trx_case' => $val->id, 'id_trx_pe' => null], ['select' => ['jenis_spesimen', 'jenis_spesimen_txt', 'jenis_pemeriksaan', 'jenis_pemeriksaan_txt', 'tgl_ambil_spesimen', 'hasil']])->get() : null;
                    }
                }
            }
            $response[] = $dt;
        }

        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getDetailHkf($id = null)
    {
        if ($id) {
            $request['where']['id'] = [$id];
        } else {
            $request = Request::json()->all();
        }
        $qQuery = [];
        if (isset($request['where'])) {
            foreach ($request['where'] as $key => $val) {
                if ($val[0] == '*') {
                    $qQuery['where'][$key] = '*';
                } else {
                    $qQuery['whereIn'][$key] = $val;
                }
            }
        }
        if (isset($request['query'])) {
            foreach ($request['query'] as $key => $val) {
                $qQuery[$key] = $val;
            }
        }
        if (!empty($request['pagination'])) {
            $qQuery['pagination'] = $request['pagination'];
        }
        $resp = Tx::getData('view_hkf', [], $qQuery)->get();
        $response = [];
        foreach ($resp as $key => $val) {
            $dt = (array) $val;
            $response[] = $dt;
        }

        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getDetailKu60($id_ku60 = '')
    {
        if ($id_ku60) {
            $request = [
                'where' => ['id_ku60' => [$id_ku60]],
                'query' => ['select' => ['*', 'dtGejala', 'dtPelacak']],
            ];
        } else {
            $request = Request::json()->all();
        }
        $qQuery = [];
        if (isset($request['where'])) {
            foreach ($request['where'] as $key => $val) {
                if ($val[0] == '*') {
                    $qQuery['where'][$key] = '*';
                } else {
                    $qQuery['whereIn'][$key] = $val;
                }
            }
        }
        if (isset($request['query'])) {
            foreach ($request['query'] as $key => $val) {
                foreach ($val as $v) {
                    if (!in_array($v, ["dtGejala", "dtPelacak"])) {
                        $var[] = $v;
                    }
                }
                $qQuery[$key] = $var;
            }
        }
        if (!empty($request['pagination'])) {
            $qQuery['pagination'] = $request['pagination'];
        }
        $resp = Tx::getData('afp_ku60', [], $qQuery)->get();
        $response = [];
        foreach ($resp as $key => $val) {
            $dt = (array) $val;
            if (isset($request['query'])) {
                foreach ($request['query'] as $k => $v) {
                    foreach ($v as $var) {
                        ($var == 'dtGejala') ? $dt['dtGejala'] = Tx::getData('view_gejala', ['id_trx_ku60' => $val->id], ['select' => ['name', 'other_gejala', 'id_gejala', 'val_paralisis_residual', 'val_paralisis_residual_txt', 'val_gangguan_raba', 'val_gangguan_raba_txt']])->get() : null;
                        ($var == 'dtPelacak') ? $dt['dtPelacak'] = Tx::getData('trx_pe_pelacak', ['id_trx_ku60' => $val->id], ['select' => ['nama']])->get() : null;
                    }
                }
            }
            $response[] = $dt;
        }

        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getDetailPe($id_pe = '')
    {
        if ($id_pe) {
            $request = [
                'where' => ['id' => [$id_pe]],
                'query' => ['select' => ['*', 'dtGejala', 'dtSpesimen', 'dtPelaksana']],
            ];
        } else {
            $request = Request::json()->all();
        }
        $qQuery = [];
        if (isset($request['where'])) {
            foreach ($request['where'] as $key => $val) {
                if ($val[0] == '*') {
                    $qQuery['where'][$key] = '*';
                } else {
                    $qQuery['whereIn'][$key] = $val;
                }
            }
        }
        if (isset($request['query'])) {
            foreach ($request['query'] as $key => $val) {
                foreach ($val as $v) {
                    if (!in_array($v, ["dtGejala", "dtSpesimen", "dtPelaksana"])) {
                        $var[] = $v;
                    }
                }
                $qQuery[$key] = $var;
            }
        }
        if (!empty($request['pagination'])) {
            $qQuery['pagination'] = $request['pagination'];
        }
        $resp = Tx::getData('afp_pe', [], $qQuery)->get();
        $response = [];
        foreach ($resp as $key => $val) {
            $dt = (array) $val;
            if (isset($request['query'])) {
                foreach ($request['query'] as $k => $v) {
                    foreach ($v as $var) {
                        ($var == 'dtGejala') ? $dt['dtGejala'] = Tx::getData('view_gejala', ['id_trx_klinis' => $val->id_trx_klinis], ['select' => ['id_gejala', 'name', 'val_kelumpuhan', 'val_kelumpuhan_txt', 'val_gangguan_raba', 'val_gangguan_raba_txt', 'other_gejala']])->get() : null;
                        ($var == 'dtSpesimen') ? $dt['dtSpesimen'] = Tx::getData('afp_spesimen', ['id_trx_case' => $val->id_trx_case, 'id_trx_pe' => $val->id], ['select' => ['jenis_spesimen', 'jenis_spesimen_txt', 'tgl_ambil_spesimen', 'tgl_kirim_kab', 'tgl_kirim_prov', 'tgl_kirim_lab']])->get() : null;
                        ($var == 'dtPelaksana') ? $dt['dtPelaksana'] = Tx::getData('trx_pe_pelaksana', ['id_trx_pe' => $val->id], ['select' => ['nama_pelaksana']])->get() : null;
                    }
                }
            }
            $response[] = $dt;
        }

        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function viewUpdate($id)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Update Kasus Penyakit ' . $case,
            'data' => array(
                'id_trx_case' => $id,
            ),
        );
        return view('case.' . $index . '.update', $compact);
    }

    public function viewUpdateLab($id)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Update Kasus Penyakit ' . $case,
            'data' => array(
                'id_trx_case' => $id,
            ),
        );
        return view('lab.case.' . $index . '.update', $compact);
    }

    public function postCase($post = [])
    {
        if ($post) {
            $req = $post;
        } else {
            $req = Request::json()->all();
        }
        $dc = Helper::getCase(array('alias' => $this->index));
        $response = [];
        foreach ($req as $key => $request) {
            $vk = Validator::make($request['dk'], [
                'tgl_mulai_lumpuh' => 'required',
            ]);
            if ($vk->fails()) {
                $success = false;
                $response[] = array_merge($vk->errors()->all());
                $code = 400;
                Helper::session_flash('warning', 'Gagal', 'Data gagal disimpan');
            } else {
                $request['dp']['tgl_lahir'] = (!empty($request['dp']['tgl_lahir'])) ? Helper::dateFormat($request['dp']['tgl_lahir']) : null;
                if ($id_pasien = $request['id_pasien']) {
                    if (!empty($request['dp']) && !empty($request['df'])) {
                        Tx::updateData('ref_pasien', ['id' => $id_pasien], $request['dp']);
                        Tx::updateData('ref_family', ['id_pasien' => $id_pasien], $request['df']);
                    }
                    $id_family = Tx::getData('ref_family', ['id_pasien' => $id_pasien])->first()->id;
                } else {
                    $id_pasien = Tx::insertData('ref_pasien', $request['dp']);
                    $request['df']['id_pasien'] = $id_pasien;
                    $id_family = Tx::insertData('ref_family', $request['df']);
                }
                $al = DB::table('ref_pasien AS a');
                $al->join('view_area AS b', 'a.code_kelurahan', '=', 'b.code_kelurahan');
                $al->select('b.code_kecamatan', 'b.code_kelurahan', 'b.code_kabupaten');
                $al->where('a.id', $id_pasien);
                $alamat_pasien = $al->first();

                $code_kabupaten_pasien = empty($alamat_pasien->code_kabupaten) ?: $alamat_pasien->code_kabupaten;
                $code_kecamatan_pasien = empty($alamat_pasien->code_kecamatan) ?: $alamat_pasien->code_kecamatan;
                $code_kelurahan_pasien = empty($alamat_pasien->code_kelurahan) ?: $alamat_pasien->code_kelurahan;
                $tgl_sakit = $request['dk']['tgl_mulai_lumpuh'];
                $epid = $this->getEpid(['tgl_sakit' => $tgl_sakit, 'id_kelurahan' => $code_kelurahan_pasien]);
                $no_epid = json_decode($epid->getContent())->response;
                $request['dc']['id_case'] = $dc[0]->id;
                $request['dc']['id_pasien'] = $id_pasien;
                if (empty($request['id_trx_case'])) {
                    $request['dc']['no_epid'] = $no_epid;
                }
                $request['dc']['klasifikasi_final'] = !empty($request['dc']['klasifikasi_final']) ? $request['dc']['klasifikasi_final'] : null;
                (!empty($request['dc']['created_at'])) ? $request['dc']['created_at'] = date('Y-m-d H:i:s', strtotime($request['dc']['created_at'])) : null;
                (!empty($request['dc']['updated_at'])) ? $request['dc']['updated_at'] = date('Y-m-d H:i:s', strtotime($request['dc']['updated_at'])) : null;
                if ($id_trx_case = $request['id_trx_case']) {
                    Tx::updateData('trx_case', ['id' => $id_trx_case], $request['dc']);
                } else {
                    $id_trx_case = Tx::insertData('trx_case', $request['dc']);
                }
                // data klinis
                $request['dk']['id_trx_case'] = $id_trx_case;
                $request['dk']['tgl_mulai_lumpuh'] = (!empty($request['dk']['tgl_mulai_lumpuh'])) ? Helper::dateFormat($request['dk']['tgl_mulai_lumpuh']) : null;
                $request['dk']['tgl_imunisasi_polio_terakhir'] = (!empty($request['dk']['tgl_imunisasi_polio_terakhir'])) ? Helper::dateFormat($request['dk']['tgl_imunisasi_polio_terakhir']) : null;
                $request['dk']['tgl_laporan_diterima'] = (!empty($request['dk']['tgl_laporan_diterima'])) ? Helper::dateFormat($request['dk']['tgl_laporan_diterima']) : null;
                $request['dk']['tgl_pelacakan'] = (!empty($request['dk']['tgl_pelacakan'])) ? Helper::dateFormat($request['dk']['tgl_pelacakan']) : null;
                if ($request['id_trx_case']) {
                    Tx::updateData('trx_klinis', ['id_trx_case' => $id_trx_case], $request['dk']);
                    $id_klinis = Tx::getData('trx_klinis', ['id_trx_case' => $id_trx_case])->first()->id;
                } else {
                    $id_klinis = Tx::insertData('trx_klinis', $request['dk']);
                }
                // gejala
                Tx::postDelete('trx_gejala', ['id_trx_klinis' => $id_klinis]);
                if (!empty($request['dg']['kelumpuhan'])) {
                    $dg = $request['dg'];
                    $rowdg = [];
                    foreach ($dg['kelumpuhan'] as $k => $v) {
                        $rowdg[] = array(
                            'id_trx_klinis' => $id_klinis,
                            'id_gejala' => $k,
                            'val_kelumpuhan' => $v,
                        );
                    }
                    foreach ($dg['gangguan_raba'] as $kg => $vg) {
                        $rowdg[] = array(
                            'id_trx_klinis' => $id_klinis,
                            'id_gejala' => $kg,
                            'val_gangguan_raba' => $vg,
                        );
                    }
                    if (!empty($dg['other'])) {
                        foreach ($dg['other'] as $ko => $vo) {
                            if (!empty($vo)) {
                                $rowdg[] = array(
                                    'id_trx_klinis' => $id_klinis,
                                    'other_gejala' => $vo,
                                    'val_kelumpuhan' => $dg['kelumpuhan_other'][$ko],
                                    'val_gangguan_raba' => $dg["gangguan_raba_other"][$ko],
                                );
                            }
                        }
                    }
                    Tx::insertArray('trx_gejala', $rowdg);
                }
                // data spesimen
                Tx::postDelete('trx_spesimen', ['id_trx_case' => $id_trx_case]);
                if (!empty($request['ds']['spesimen'][0])) {
                    $dtSpesimen = $request['ds'];
                    $rSpesimen = [];
                    foreach ($dtSpesimen['spesimen'] as $key => $val) {
                        $d = explode('|', $val);
                        $rSpesimen[] = array(
                            'id_trx_case' => $id_trx_case,
                            'jenis_spesimen' => $d[0],
                            'tgl_ambil_spesimen' => Helper::dateFormat($d[1]),
                            'jenis_pemeriksaan' => $d[2],
                            'hasil' => $d[3],
                            'case' => 'afp',
                        );
                    }
                    $spesimen = Tx::insertArray('trx_spesimen', $rSpesimen);
                }
                // notif klb
                Tx::postDelete('trx_notification', ['id_trx_case' => $id_trx_case]);
                $code_wilayah_faskes = $request['code_wilayah_faskes'];
                $dtNotification[] = array(
                    'type' => 'klb',
                    'id_trx_case' => $id_trx_case,
                    'from_faskes' => $code_wilayah_faskes,
                    'to_faskes' => $code_kecamatan_pasien,
                );
                $id_role = $request['dc']['id_role'];
                if ($id_role == '1') {
                    // notif puskesmas
                    if ($code_kecamatan_pasien != $code_wilayah_faskes) {
                        $dtNotification[] = array(
                            'type' => 'cross',
                            'id_trx_case' => $id_trx_case,
                            'from_faskes' => $code_wilayah_faskes,
                            'to_faskes' => $code_kecamatan_pasien,
                        );
                    }
                } else if ($id_role == '2') {
                    // notif rumahsakit
                    if ($code_kabupaten_pasien != $code_wilayah_faskes) {
                        $dtNotification[] = array(
                            'type' => 'cross',
                            'id_trx_case' => $id_trx_case,
                            'from_faskes' => $code_wilayah_faskes,
                            'to_faskes' => $code_kabupaten_pasien,
                        );
                    }
                }
                Tx::insertArray('trx_notification', $dtNotification);
                $success = true;
                $code = 200;
                $trx_case = Tx::getData('trx_case', ['id' => $id_trx_case], ['select' => ['created_at', 'updated_at', 'no_epid']])->first();
                $response[] = [
                    'id' => $id_trx_case,
                    'no_epid' => $trx_case->no_epid,
                    'created_at' => date('d-m-Y H:i:s', strtotime($trx_case->created_at)),
                    'updated_at' => date('d-m-Y H:i:s', strtotime($trx_case->updated_at)),
                ];
                Helper::session_flash('success', 'Berhasil', 'Data berhasil disimpan');
            }
        }

        return Response::json(array(
            'success' => $success,
            'response' => $response,
        ), $code);
    }

    public function viewPeriksa($id_trx_case)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $tx_case = TxCase::getDataAfp(array('id' => $id_trx_case));
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Pemeriksaan Lab Penyakit ' . $case,
            'data' => array(
                'id_trx_case' => $id_trx_case,
                'id_trx_pe' => null,
                'status_kasus' => $tx_case[0]->status_kasus,
            ),
        );
        return view('lab.case.' . $index . '.periksa.index', $compact);
    }

    public function viewPe($id_trx_case)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Input data penyelidikan epidemologi kasus ' . $case,
            'data' => array(
                'id_trx_case' => $id_trx_case,
                'id_trx_pe' => null,
            ),
        );
        return view('case.' . $index . '.pe.index', $compact);
    }

    public function postKu60()
    {
        $req = Request::json()->all();
        $response = [];
        foreach ($req as $key => $request) {
            $id_trx_case = $request['id_trx_case'];
            $id_trx_ku60 = $request['id_trx_ku60'];
            $id_trx_klinis = Tx::getData('trx_klinis', ['id_trx_case' => $id_trx_case], ['select' => ['id']])->first()->id;
            if (empty($id_trx_case)) {
                $success = false;
                $response = ["error" => "id_trx_case is empty"];
                $code = 400;
                Helper::session_flash('warning', 'Gagal', 'Data gagal di simpan');
            } else {
                // ku60
                $request['dku']['id_trx_case'] = $id_trx_case;
                (!empty($request['dku']['created_at'])) ? $request['dku']['created_at'] = date('Y-m-d H:i:s', strtotime($request['dku']['created_at'])) : null;
                (!empty($request['dku']['updated_at'])) ? $request['dku']['updated_at'] = date('Y-m-d H:i:s', strtotime($request['dku']['updated_at'])) : null;
                if ($id_trx_ku60) {
                    Tx::updateData('trx_ku60', ['id' => $id_trx_ku60], $request['dku']);
                } else {
                    $id_trx_ku60 = Tx::insertData('trx_ku60', $request['dku']);
                }
                // informasi kunjungan ulang
                $request['dik']['id_trx_ku60'] = $id_trx_ku60;
                (!empty($request['dik']['tgl_kunjungan_ulang_seharusnya'])) ? $request['dik']['tgl_kunjungan_ulang_seharusnya'] = date('Y-m-d H:i:s', strtotime($request['dik']['tgl_kunjungan_ulang_seharusnya'])) : null;
                (!empty($request['dik']['tgl_kunjungan_ulang'])) ? $request['dik']['tgl_kunjungan_ulang'] = date('Y-m-d H:i:s', strtotime($request['dik']['tgl_kunjungan_ulang'])) : null;
                (!empty($request['dik']['tgl_meninggal'])) ? $request['dik']['tgl_meninggal'] = date('Y-m-d H:i:s', strtotime($request['dik']['tgl_meninggal'])) : null;
                if ($request['id_trx_ku60']) {
                    Tx::updateData('trx_informasi_kunjungan_ulang', ['id' => $id_trx_ku60], $request['dik']);
                } else {
                    Tx::insertData('trx_informasi_kunjungan_ulang', $request['dik']);
                }
                // klinis
                if ($request['id_trx_case']) {
                    Tx::updateData('trx_klinis', ['id_trx_case' => $id_trx_case], $request['drs']);
                } else {
                    Tx::insertData('trx_klinis', $request['drs']);
                }
                // gejala
                Tx::postDelete('trx_gejala', ['id_trx_ku60' => $id_trx_ku60]);
                if (!empty($request['dg']['paralisis_residual'])) {
                    $dg = $request['dg'];
                    $rowdg = [];
                    foreach ($dg['paralisis_residual'] as $k => $v) {
                        if (!empty($v)) {
                            $rowdg[] = array(
                                'id_trx_ku60' => $id_trx_ku60,
                                'id_gejala' => $k,
                                'val_paralisis_residual' => $v,
                            );
                        }
                    }
                    foreach ($dg['gangguan_raba'] as $kg => $vg) {
                        if (!empty($vg)) {
                            $rowdg[] = array(
                                'id_trx_ku60' => $id_trx_ku60,
                                'id_gejala' => $kg,
                                'val_gangguan_raba' => $vg,
                            );
                        }
                    }
                    foreach ($dg['other'] as $ko => $vo) {
                        if (!empty($vo)) {
                            $rowdg[] = array(
                                'id_trx_ku60' => $id_trx_ku60,
                                'other_gejala' => $vo,
                                'val_paralisis_residual' => $dg['paralisis_residual_other'][$ko],
                                'val_gangguan_raba' => $dg["gangguan_raba_other"][$ko],
                            );
                        }
                    }
                    Tx::insertArray('trx_gejala', $rowdg);
                }
                // pe hasil pemeriksaan
                $request['dhp']['id_trx_ku60'] = $id_trx_ku60;
                if ($request['id_trx_ku60']) {
                    Tx::updateData('trx_pe_hasil_pemeriksaan', ['id_trx_ku60' => $id_trx_ku60], $request['dhp']);
                } else {
                    Tx::insertData('trx_pe_hasil_pemeriksaan', $request['dhp']);
                }
                // pe pelacak
                Tx::postDelete('trx_pe_pelacak', ['id_trx_ku60' => $id_trx_ku60]);
                if (!empty($request['dpel']['nama'])) {
                    $dpel = $request['dpel'];
                    $rowdpel = [];
                    foreach ($dpel['nama'] as $k => $v) {
                        if (!empty($v)) {
                            $rowdpel[] = array(
                                'id_trx_ku60' => $id_trx_ku60,
                                'nama' => $v,
                            );
                        }
                    }
                    Tx::insertArray('trx_pe_pelacak', $rowdpel);
                }

                $success = true;
                $code = 200;
                $trx_ku60 = Tx::getData('trx_ku60', ['id' => $id_trx_ku60], ['select' => ['created_at', 'updated_at']])->first();
                $response[] = [
                    'id' => $id_trx_ku60,
                    'created_at' => date('d-m-Y H:i:s', strtotime($trx_ku60->created_at)),
                    'updated_at' => date('d-m-Y H:i:s', strtotime($trx_ku60->updated_at)),
                ];
                Helper::session_flash('success', 'Berhasil', 'Data berhasil disimpan');
            }
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function postHkf()
    {
        $req = Request::json()->all();
        $response = [];
        foreach ($req as $key => $request) {
            $id_trx_case = $request['id_trx_case'];
            if (empty($id_trx_case)) {
                $success = false;
                $response = ["error" => "id_trx_case is empty"];
                $code = 400;
                Helper::session_flash('warning', 'Gagal', 'Data gagal di simpan');
            } else {
                $request['dkf']['id_trx_case'] = $id_trx_case;
                (isset($request['dkf']['created_at'])) ? $request['dkf']['created_at'] = date('Y-m-d H:i:s', strtotime($request['dkf']['created_at'])) : null;
                (isset($request['dkf']['updated_at'])) ? $request['dkf']['updated_at'] = date('Y-m-d H:i:s', strtotime($request['dkf']['updated_at'])) : null;
                $id_hkf = $request['id_hkf'];
                if ($id_hkf) {
                    Tx::updateData('trx_hkf', ['id' => $id_hkf], $request['dkf']);
                } else {
                    $id_hkf = Tx::insertData('trx_hkf', $request['dkf']);
                }
                $success = true;
                $code = 200;
                $trx_hkf = Tx::getData('trx_hkf', ['id' => $id_hkf], ['select' => ['created_at', 'updated_at']])->first();
                $response[] = [
                    'id' => $id_hkf,
                    'created_at' => date('d-m-Y H:i:s', strtotime($trx_hkf->created_at)),
                    'updated_at' => date('d-m-Y H:i:s', strtotime($trx_hkf->updated_at)),
                ];
                Helper::session_flash('success', 'Berhasil', 'Data berhasil disimpan');
            }
        }

        return Response::json(array(
            'success' => $success,
            'response' => $response,
        ), $code);
    }

    public function postPe($post = '')
    {
        if ($post) {
            $req = $post;
        } else {
            $req = Request::json()->all();
        }
        $response = [];
        foreach ($req as $key => $request) {
            $id_trx_case = $request['id_trx_case'];
            $id_trx_klinis = Tx::getData('trx_klinis', ['id_trx_case' => $id_trx_case], ['select' => ['id']])->first()->id;
            $id_trx_pe = $request['id_trx_pe'];
            if (empty($id_trx_case)) {
                $success = false;
                $response = ["error" => "id_trx_case is empty"];
                $code = 400;
                Helper::session_flash('warning', 'Gagal', 'Data gagal di simpan');
            } else {
                // pe
                $request['dpe']['id_trx_case'] = $id_trx_case;
                $request['dpe']['tgl_penyelidikan'] = date('Y-m-d H:i:s');
                (isset($request['dpe']['created_at'])) ? $request['dpe']['created_at'] = date('Y-m-d H:i:s', strtotime($request['dpe']['created_at'])) : null;
                (isset($request['dpe']['updated_at'])) ? $request['dpe']['updated_at'] = date('Y-m-d H:i:s', strtotime($request['dpe']['updated_at'])) : null;
                if ($id_trx_pe = $request['id_trx_pe']) {
                    Tx::updateData('trx_pe', ['id' => $id_trx_pe], $request['dpe']);
                } else {
                    $id_trx_pe = Tx::insertData('trx_pe', $request['dpe']);
                }
                // pasien + family
                $request['dp']['tgl_lahir'] = (isset($request['dp']['tgl_lahir'])) ? Helper::dateFormat($request['dp']['tgl_lahir']) : null;
                if ($id_pasien = $request['id_pasien']) {
                    if (isset($request['dp']) && isset($request['df'])) {
                        Tx::updateData('ref_pasien', ['id' => $id_pasien], $request['dp']);
                        Tx::updateData('ref_family', ['id_pasien' => $id_pasien], $request['df']);
                    }
                    $id_family = Tx::getData('ref_family', ['id_pasien' => $id_pasien])->first()->id;
                } else {
                    $id_pasien = Tx::insertData('ref_pasien', $request['dp']);
                    $request['df']['id_pasien'] = $id_pasien;
                    $id_family = Tx::insertData('ref_family', $request['df']);
                }
                // pe sumber informasi
                $request['dsi']['id_trx_pe'] = $id_trx_pe;
                if ($request['id_trx_pe']) {
                    Tx::updateData('trx_pe_sumber_informasi', ['id_trx_pe' => $id_trx_pe], $request['dsi']);
                } else {
                    Tx::insertData('trx_pe_sumber_informasi', $request['dsi']);
                }
                // klinis
                $request['drs']['demam_sebelum_lumpuh'] = $request['dg']['demam_sebelum_lumpuh'];
                $request['drs']['tgl_sakit'] = (isset($request['drs']['tgl_sakit'])) ? Helper::dateFormat($request['drs']['tgl_sakit']) : null;
                $request['drs']['tgl_mulai_lumpuh'] = (isset($request['drs']['tgl_mulai_lumpuh'])) ? Helper::dateFormat($request['drs']['tgl_mulai_lumpuh']) : null;
                $request['drs']['tgl_meninggal'] = (isset($request['drs']['tgl_meninggal'])) ? Helper::dateFormat($request['drs']['tgl_meninggal']) : null;
                $request['drs']['tgl_berobat'] = (isset($request['drs']['tgl_berobat'])) ? Helper::dateFormat($request['drs']['tgl_berobat']) : null;
                $request['drs']['tgl_imunisasi_polio_terakhir'] = (isset($request['drs']['tgl_imunisasi_polio_terakhir'])) ? Helper::dateFormat($request['drs']['tgl_imunisasi_polio_terakhir']) : null;
                if ($id_trx_klinis) {
                    Tx::updateData('trx_klinis', ['id' => $id_trx_klinis], $request['drs']);
                } else {
                    Tx::insertData('trx_klinis', $request['drs']);
                }
                // gejala
                if (isset($request['dg']['kelumpuhan'])) {
                    Tx::postDelete('trx_gejala', ['id_trx_klinis' => $id_trx_klinis]);
                    $dg = $request['dg'];
                    $rowdg = [];
                    foreach ($dg['kelumpuhan'] as $k => $v) {
                        if (!empty($v)) {
                            $rowdg[] = array(
                                'id_trx_klinis' => $id_trx_klinis,
                                'id_gejala' => $k,
                                'val_kelumpuhan' => $v,
                            );
                        }
                    }
                    foreach ($dg['gangguan_raba'] as $kg => $vg) {
                        if (!empty($vg)) {
                            $rowdg[] = array(
                                'id_trx_klinis' => $id_trx_klinis,
                                'id_gejala' => $kg,
                                'val_gangguan_raba' => $vg,
                            );
                        }
                    }
                    if (!empty($dg['other'])) {
                        foreach ($dg['other'] as $ko => $vo) {
                            if (!empty($vo)) {
                                $rowdg[] = array(
                                    'id_trx_klinis' => $id_trx_klinis,
                                    'other_gejala' => $vo,
                                    'val_kelumpuhan' => $dg['kelumpuhan_other'][$ko],
                                    'val_gangguan_raba' => $dg["gangguan_raba_other"][$ko],
                                );
                            }
                        }
                    }
                    Tx::insertArray('trx_gejala', $rowdg);
                }
                // pe riwayat kontak
                $request['drk']['id_trx_pe'] = $id_trx_pe;
                $request['drk']['tgl_pergi_sebelum_sakit'] = (isset($request['drk']['tgl_pergi_sebelum_sakit'])) ? Helper::dateFormat($request['drk']['tgl_pergi_sebelum_sakit']) : null;
                if ($request['id_trx_pe']) {
                    Tx::updateData('trx_pe_riwayat_kontak', ['id_trx_pe' => $id_trx_pe], $request['drk']);
                } else {
                    Tx::insertData('trx_pe_riwayat_kontak', $request['drk']);
                }
                $request['dc']['pengambilan_spesimen'] = $request['ds']['pengambilan_spesimen'];
                $request['dc']['alasan_spesimen_tidak_diambil'] = (!empty($request['ds']['alasan_spesimen_tidak_diambil'])) ? $request['ds']['alasan_spesimen_tidak_diambil'] : null;
                if ($request['id_trx_case']) {
                    Tx::updateData('trx_case', ['id' => $id_trx_case], $request['dc']);
                }
                // spesimen
                if (isset($request['ds']['spesimen'])) {
                    Tx::postDelete('trx_spesimen', ['id_trx_case' => $id_trx_case, 'id_trx_pe' => $id_trx_pe]);
                    $dtSpesimen = $request['ds'];
                    $rSpesimen = [];
                    foreach ($dtSpesimen['spesimen'] as $k => $v) {
                        $d = explode('|', $v);
                        $tgl_ambil_spesimen = (isset($d[1])) ? Helper::dateFormat($d[1]) : null;
                        $tgl_kirim_kab = (isset($d[2])) ? Helper::dateFormat($d[2]) : null;
                        $tgl_kirim_prov = (isset($d[3])) ? Helper::dateFormat($d[3]) : null;
                        $tgl_kirim_lab = (isset($d[4])) ? Helper::dateFormat($d[4]) : null;
                        $rSpesimen[] = array(
                            'id_trx_case' => $id_trx_case,
                            'id_trx_pe' => $id_trx_pe,
                            'jenis_spesimen' => $d[0],
                            'tgl_ambil_spesimen' => $tgl_ambil_spesimen,
                            'tgl_kirim_kab' => $tgl_kirim_kab,
                            'tgl_kirim_prov' => $tgl_kirim_prov,
                            'tgl_kirim_lab' => $tgl_kirim_lab,
                            'case' => 'afp',
                        );
                    }
                    Tx::insertArray('trx_spesimen', $rSpesimen);
                }
                // pelaksana
                Tx::postDelete('trx_pe_pelaksana', ['id_trx_pe' => $id_trx_pe]);
                if (isset($request['dpel'])) {
                    $dpel = $request['dpel'];
                    $rowdp = [];
                    foreach ($dpel['pelaksana'] as $k => $v) {
                        $rowdp[] = array(
                            'id_trx_pe' => $id_trx_pe,
                            'nama_pelaksana' => $v,
                        );
                    }
                    Tx::insertArray('trx_pe_pelaksana', $rowdp);
                }
                // pe hasil pemeriksaan
                $request['dh']['id_trx_pe'] = $id_trx_pe;
                if ($request['id_trx_pe']) {
                    Tx::updateData('trx_pe_hasil_pemeriksaan', ['id_trx_pe' => $id_trx_pe], $request['dh']);
                } else {
                    Tx::insertData('trx_pe_hasil_pemeriksaan', $request['dh']);
                }
                $success = true;
                $code = 200;
                $trx_pe = Tx::getData('trx_pe', ['id' => $id_trx_pe], ['select' => ['created_at', 'updated_at']])->first();
                $response[] = [
                    'id' => $id_trx_pe,
                    'created_at' => date('d-m-Y H:i:s', strtotime($trx_pe->created_at)),
                    'updated_at' => date('d-m-Y H:i:s', strtotime($trx_pe->updated_at)),
                ];
                Helper::session_flash('success', 'Berhasil', 'Data berhasil disimpan');
            }
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getDataKu60()
    {
        $request = Request::all();
        $param['roleUser'] = Helper::role();
        $param['query']['where'] = ['id_case' => 2];
        $d = Sql::getData("view_list_ku60", $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            $q->filterColumn('no', 'whereRaw', '@no  + 1 like ?', ["%{$keyword}%"]);
            foreach ($request['columns'] as $key => $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->editColumn('name_pasien', function ($dt) {
            $name_pasien = ucwords($dt->name_pasien);
            if ($dt->status_kasus == '1') {
                $name_pasien .= '<br><span class="label label-danger">Index</span>';
            }
            return $name_pasien;
        });
        $q->editColumn('nama_ortu', function ($dt) {
            return ucwords($dt->nama_ortu);
        });
        $q->editColumn('no_epid', function ($dt) {
            $no_epid = $dt->no_epid;
            if ($dt->jenis_input == '1') {
                $no_epid .= '<br><span class="label label-info">Web</span>';
            } else if ($dt->jenis_input == '2') {
                $no_epid .= '<br><span class="label label-success">Android</span>';
            } else if ($dt->jenis_input == '3') {
                $no_epid .= '<br><span class="label label-warning">Import</span>';
            }
            return $no_epid;
        });
        $q->addColumn('action', function ($dt) {
            $roleUser = Helper::role();
            $action = "<a href=" . url("case/afp/ku60/detail/" . $dt->id_ku60) . " title='Detail data' data-toggle='tooltip' class='btn btn-sm btn-flat btn-success'><i class='fa fa-asterisk'></i></a>";
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'kabupaten'])) {
                $action .= "<a href=" . url("case/afp/ku60/update/" . $dt->id_ku60) . " title='Edit data' data-toggle='tooltip' class='btn btn-sm btn-flat btn-warning'><i class='fa fa-pencil'></i></a>";
            };
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'provinsi', 'pusat'])) {
                $action .= '<a action="' . url('case/afp/ku60/delete/' . $dt->id_ku60) . '" title="Delete data"  class="btn btn-sm btn-flat btn-danger delete" data-toggle="modal" data-target=".modaldelete"><i class="fa fa-remove"></i></a>';
            };
            return $action;
        });
        return $q->make(true);
    }

    public function getDataPeAfp()
    {
        $request = Request::all();
        $param['roleUser'] = Helper::role();
        $param['query']['where'] = ['id_case' => 2];
        $d = Sql::getData("view_list_pe", $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            foreach ($request['columns'] as $key => $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->editColumn('name_pasien', function ($dt) {
            return ucwords($dt->name_pasien);
        });
        $q->editColumn('nama_ortu', function ($dt) {
            return ucwords($dt->nama_ortu);
        });
        $q->editColumn('no_epid', function ($dt) {
            $no_epid = $dt->no_epid;
            if ($dt->jenis_input == '1') {
                $no_epid .= '<br><span class="label label-info">Web</span>';
            } else if ($dt->jenis_input == '2') {
                $no_epid .= '<br><span class="label label-success">Android</span>';
            } else if ($dt->jenis_input == '3') {
                $no_epid .= '<br><span class="label label-warning">Import</span>';
            }
            return $no_epid;
        });
        $q->addColumn('action', function ($dt) {
            $roleUser = Helper::role();
            $action = "<a href=" . url("case/afp/pe/detail/" . $dt->id_pe) . " title='Detail data' data-toggle='tooltip' class='btn btn-sm btn-flat btn-success'><i class='fa fa-asterisk'></i></a>";
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'kabupaten'])) {
                $action .= "<a href=" . url("case/afp/pe/update/" . $dt->id_pe) . " title='Edit data' data-toggle='tooltip' class='btn btn-sm btn-flat btn-warning'><i class='fa fa-pencil'></i></a>";
            };
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'provinsi', 'pusat'])) {
                $action .= '<a action="' . url('case/afp/pe/delete/' . $dt->id_pe) . '" title="Delete data"  class="btn btn-sm btn-flat btn-danger delete" data-toggle="modal" data-target=".modaldelete"><i class="fa fa-remove"></i></a>';
            };
            return $action;
        });
        return $q->make(true);
    }

    public function viewDetailPe($id_trx_pe)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $response = $this->getDetailPe($id_trx_pe);
        $dt = json_decode($response->getContent());
        $data = $dt->response[0];
        $dtGejala = [];
        $i = 1;
        foreach ($data->dtGejala as $key => $val) {
            if ($val->id_gejala) {
                $dtGejala[$val->id_gejala] = $val;
            } else {
                $dtGejala[$i++] = $val;
            }
        }
        $data->dtGejala = $dtGejala;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Detail Pasien Kasus Penyelidikan Epidemologi Penyakit ' . $case,
            'data' => array(
                'index' => $index,
                'response' => $data,
            ),
        );
        return view('case.' . $index . '.pe.detail', $compact);
    }

    public function postDeletePe($id = '')
    {
        $rId = Request::json()->all();
        if ($id != null) {
            $rId['query']['delete'][] = $id;
        }
        $response = [];
        foreach ($rId['query']['delete'] as $key => $val) {
            $response[$val] = Tx::deleteData('trx_pe', ['id' => $val]);
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function viewUpdatePe($id_pe)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $cc = Tx::getData('afp', ['id_pe' => $id_pe], ['select' => ['id']])->first();
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Update data penyelidikan epidemologi kasus ' . $case,
            'data' => array(
                'id_trx_pe' => $id_pe,
                'id_trx_case' => $cc->id,
            ),
        );
        return view('case.' . $index . '.pe.index', $compact);
    }

    public function viewKu60($id_trx_case)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $cc = Tx::getData('trx_ku60', ['id_trx_case' => $id_trx_case], ['select' => ['id', 'id_trx_case']])->first();
        $id_ku60 = !empty($cc) ? $cc->id : '';
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Input Data Kunjungan Ulang 60 Hari Kasus ' . $case,
            'data' => array(
                'id_trx_ku60' => $id_ku60,
                'id_trx_case' => $id_trx_case,
            ),
        );
        return view('case.' . $index . '.ku60.index', $compact);
    }

    public function viewDetailKu60($id_ku60)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $response = $this->getDetailKu60($id_ku60);
        $dt = json_decode($response->getContent());
        $data = $dt->response[0];
        $dtGejala = [];
        $i = 1;
        foreach ($data->dtGejala as $key => $val) {
            if ($val->id_gejala) {
                $dtGejala[$val->id_gejala] = $val;
            } else {
                $dtGejala[$i++] = $val;
            }
        }
        $data->dtGejala = $dtGejala;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Detail Data Kunjungan Ulang 60 Hari Kasus ' . $case,
            'data' => array(
                'response' => $data,
            ),
        );
        return view('case.' . $index . '.ku60.detail', $compact);
    }

    public function viewHkf($id_trx_case)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $tx_case = TxCase::getDataAfp(array('id' => $id_trx_case));
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Input Data Hasil Klasifikasi Final Kasus oleh Kelompok Kerja Ahli Surveilans Kasus ' . $case,
            'data' => array(
                'id_trx_case' => $id_trx_case,
                'id_hkf' => null,
            ),
        );
        return view('case.' . $index . '.hkf.index', $compact);
    }

    public function exportExampleImportKasus()
    {
        $file = public_path() . "/download/afp/template import afp.xls";
        return Response::download($file, 'Contoh format import kasus afp.xls');
    }

    public function importCase()
    {
        $path = Request::file('import');
        if (!empty($path)) {
            $data = Excel::load($path, function ($reader) {
            })->get();
            $role = Helper::role();
            $row = array();
            if (!empty($data) && $data->count()) {
                foreach ($data->toArray() as $key => $val) {
                    $cd = DB::table('view_list_case')->where(['id_case' => 2, 'name_pasien' => $val['nama_pasien'], 'code_kelurahan_pasien' => $val['wilayah'], 'tgl_sakit_date' => Helper::dateFormat($val['tgl_mulai_lumpuh']), 'nik' => $val['nik']])->count();
                    if ($cd == 0) {
                        $ut = $val['thn'];
                        $ub = $val['bln'];
                        $uh = $val['hari'];
                        if ($val['tanggal_lahir'] && $val['tgl_mulai_lumpuh']) {
                            $age = Helper::getAge(['tgl_sakit' => $val['tgl_mulai_lumpuh'], 'tgl_lahir' => $val['tanggal_lahir']]);
                            $ut = $age['years'];
                            $ub = $age['month'];
                            $uh = $age['day'];
                        }
                        $wilayah = DB::table('view_area')->where(['code_kelurahan' => $val['wilayah']])->first();
                        $cekpasien = DB::table('ref_pasien')->where(['name' => $val['nama_pasien'], 'nik' => $val['nik'], 'code_kelurahan' => $val['wilayah']])->first();
                        $row[] = [
                            "dc" => [
                                'id_faskes' => $role->id_faskes,
                                'id_role' => $role->id_role,
                                "jenis_input" => "3",
                                "no_rm" => $val['norm'],
                                "no_epid_lama" => $val['noepid_lama'],
                                "klasifikasi_final" => $val['klasifikasi_final'],
                            ],
                            'code_wilayah_faskes' => $role->code_wilayah_faskes,
                            'id_trx_case' => '',
                            'dp' => [
                                "name" => $val['nama_pasien'],
                                "nik" => $val['nik'],
                                "agama" => $val['agama'],
                                "jenis_kelamin" => $val['jenis_kelamin'],
                                "tgl_lahir" => $val['tanggal_lahir'],
                                "umur_thn" => $ut,
                                "umur_bln" => $ub,
                                "umur_hari" => $uh,
                                "alamat" => $val['alamat'],
                                "code_kelurahan" => $val['wilayah'],
                                "code_kecamatan" => (!empty($wilayah)) ? $wilayah->code_kecamatan : null,
                                "code_kabupaten" => (!empty($wilayah)) ? $wilayah->code_kabupaten : null,
                                "code_provinsi" => (!empty($wilayah)) ? $wilayah->code_provinsi : null,
                            ],
                            'id_pasien' => ($cekpasien) ? $cekpasien->id : '',
                            'df' => [
                                'name' => $val['nama_orang_tua'],
                            ],
                            "dk" => [
                                "tgl_mulai_lumpuh" => $val['tgl_mulai_lumpuh'],
                                "demam_sebelum_lumpuh" => $val['demam_sebelum_lumpuh'],
                                "imunisasi_rutin_polio_sebelum_sakit" => $val['jml_dosis_imunisasi_rutin_polio'],
                                "informasi_imunisasi_rutin_polio_sebelum_sakit" => $val['sumber_informasi_rutin'],
                                "pin_mopup_ori_biaspolio" => $val['jml_dosis_pin_mopup_ori_bias'],
                                "informasi_pin_mopup_ori_biaspolio" => $val['sumber_informasi_pin_mopup_ori_bias'],
                                "tgl_imunisasi_polio_terakhir" => $val['tgl_imunisasi_polio_terakhir'],
                                "tgl_laporan_diterima" => $val['tgl_laporan_diterima'],
                                "tgl_pelacakan" => $val['tgl_pelacakan'],
                                "kontak" => $val['kontak'],
                                "keadaan_akhir" => $val['keadaan_akhir'],
                            ],
                            "dg" => [
                                "kelumpuhan" => [
                                    "31" => $val['kelumpuhan_tungkai_kanan'],
                                    "32" => $val['kelumpuhan_tungkai_kiri'],
                                    "33" => $val['kelumpuhan_lengan_kanan'],
                                    "34" => $val['kelumpuhan_lengan_kiri'],
                                ],
                                "gangguan_raba" => [
                                    "39" => $val['gangguan_raba_tungkai_kanan'],
                                    "40" => $val['gangguan_raba_tungkai_kiri'],
                                    "41" => $val['gangguan_raba_lengan_kanan'],
                                    "42" => $val['gangguan_raba_lengan_kiri'],
                                ],
                            ],
                            "ds" => [
                                "spesimen" => [
                                    $val['spesimen'] . '|' . $val['tanggal_ambil_spesimen'] . '|' . $val['jenis_pemeriksaan'] . '|' . $val['hasil'],
                                ],
                            ],
                        ];
                    }
                }
            }
            if ($row) {
                $afp = $this->postCase($row);
                $response = json_decode($afp->getContent())->response;
            }
            echo true;
        }
    }

    public function getDataBasedAreaLab()
    {
        $request = Request::all();
        $param['query']['select'] = array('id_trx_case', 'no_epid', 'name_pasien', 'full_address', 'nama_ortu', 'thn_sakit', 'status_kasus', 'name_faskes', 'code_faskes');
        if (!empty($request['area'])) {
            $filter = json_decode($request['area']);
            if (!empty($filter)) {
                $param['query']['whereIn'] = array('code_provinsi_pasien' => $filter);
            }
        }
        $param['query']['where'] = 2;
        $d = Sql::getData('view_list_case', $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            foreach ($request['columns'] as $key => $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->addColumn('action', function ($dt) {
            $action = "<div class='btn-group'>";
            $action .= '<a href="afp/periksa/' . $dt->id_trx_case . '" title="Pemeriksaan Laboratorium" data-toggle="tooltip" class="btn btn-info">Tambah Pemeriksaan</a>';
            $action .= '</div>';
            return $action;
        });
        return $q->make(true);
    }

    public function _getDataBasedAreaLab()
    {
        $config['table'] = 'afp AS a';
        $config['coloumn'] = array('id', 'no_epid', 'name_pasien', 'full_address', 'nama_ortu', 'thn_tgl_sakit', 'name_faskes');
        $config['columnSelect'] = array('a.id', 'a.no_epid', 'a.name_pasien', 'a.nama_ortu', 'a.full_address', 'a.thn_tgl_sakit', 'a.name_faskes');
        $config['columnFormat'] = array('number', '', '', '', '', '', '');
        $config['searchColumn'] = array('a.name_pasien', 'a.no_epid');
        $config['key'] = 'a.id AS id';

        if (Request::get('area')) {
            $filter = json_decode(Request::get('area'));
            if (!empty($filter)) {
                $config['whereIn'] = array('a.code_provinsi_pasien' => $filter);
            }
        }
        if (Request::get('dt')) {
            $filter = json_decode(Request::get('dt'));
            // filter district
            if (isset($filter->filter)) {
                $district = $filter->filter;
                foreach ($district as $key => $val) {
                    if (!empty($val)) {
                        $config['where'][] = array('a.' . $key => $val);
                    }
                }
            }
            // filter time
            $time = $filter->range;
            if ($time) {
                $from = $filter->from;
                $fy = (isset($from->year)) ? $from->year : null;
                $fm = (isset($from->month)) ? $from->month : null;
                $fd = (isset($from->day)) ? $from->day : null;
                $to = $filter->to;
                $ty = (isset($to->year)) ? $to->year : null;
                $tm = (isset($to->month)) ? $to->month : null;
                $td = (isset($to->day)) ? $to->day : null;
                if ($time == '1') {
                    $start = $fy . '-' . $fm . '-' . $fd;
                    $to = $ty . '-' . $tm . '-' . $td;
                    if ($fy && $fm && $fd && $ty && $tm && $td) {
                        $config['whereBetween'] = array('a.tgl_sakit' => array($start, $to));
                    }
                } else if ($time == '2') {
                    $start = $fy . '-' . $fm . '-' . '1';
                    $to = $ty . '-' . $tm . '-' . date('t', mktime(0, 0, 0, $tm, 1, $ty));
                    if ($fy && $fm && $ty && $tm) {
                        $config['whereBetween'] = array('a.tgl_sakit' => array($start, $to));
                    }
                } else if ($time == '3') {
                    $start = $fy;
                    $to = $ty;
                    if ($fy && $ty) {
                        $config['whereBetween'] = array('a.thn_tgl_sakit' => array($start, $to));
                    }
                }
            }
        }

        $Datatable = Datatables::getData($config);
        if (count($Datatable['data']) > 0) {
            foreach ($Datatable['data'] as $key => $val) {
                $no_epid = $val['no_epid'];
                $name = $val['name_pasien'];
                $aksi = '<a href="afp/periksa/' . $val['id'] . '" title="Pemeriksaan Laboratorium" data-toggle="tooltip" class="btn btn-info">Tambah Pemeriksaan</a>';
                $dSampel = TxSampelLab::getDataSampel(array('id' => $val['id']), $this->index);
                $status = (!empty($dSampel)) ? "Sudah Periksa" : "Belum Periksa";
                $dt[] = array(
                    'no' => $val['no'],
                    'id' => $val['id'],
                    'no_epid' => $no_epid,
                    'name_pasien' => $name,
                    'nama_ortu' => $val['nama_ortu'],
                    'alamat' => $val['full_address'],
                    'rujukan' => $val['name_faskes'],
                    'status' => $status,
                    'action' => $aksi,
                );
            }
            $Datatable['data'] = $dt;
        }
        echo json_encode($Datatable);
    }

    public function getDataSampel()
    {
        $config['table'] = 'view_lab_afp AS a';
        $config['coloumn'] = array('id_spesimen', 'id', 'no_epid', 'name', 'umur_thn', 'umur_bln', 'umur_hari', 'jenis_kelamin_txt', 'keadaan_akhir_txt', 'tgl_ambil_spesimen', 'kabupaten_name', 'hasil_final_kultur_txt', 'hasil_sqc_p1', 'hasil_sqc_p2', 'hasil_sqc_p3', 'hasil_isolat_p1', 'hasil_isolat_p2', 'hasil_isolat_p3', 'tgl_mulai_periksa_kultur', 'tgl_isolat_pertama_diterima', 'tgl_periksa_sqc');
        $config['columnSelect'] = array('a.id_spesimen', 'a.id', 'a.no_epid', 'a.name', 'a.umur_thn', 'a.umur_bln', 'a.umur_hari', 'a.jenis_kelamin_txt', 'a.keadaan_akhir_txt', 'a.tgl_ambil_spesimen', 'a.kabupaten_name', 'a.hasil_final_kultur_txt', 'a.hasil_sqc_p1', 'a.hasil_sqc_p2', 'a.hasil_sqc_p3', 'a.hasil_isolat_p1', 'a.hasil_isolat_p2', 'a.hasil_isolat_p3', 'a.tgl_mulai_periksa_kultur', 'a.tgl_isolat_pertama_diterima', 'a.tgl_periksa_sqc');
        $config['columnFormat'] = array('number', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
        $config['searchColumn'] = array('a.name', 'a.no_epid');
        $config['key'] = 'a.id AS id';
        $config['action'][] = array(
            'action' => 'detail',
            'url' => url('lab/case/afp/detail'),
            'key' => 'id',
        );
        $config['action'][] = array(
            'action' => 'edit',
            'url' => url('lab/case/afp/update'),
            'key' => 'id',
        );
        $config['action'][] = array(
            'action' => 'delete',
            'url' => url('lab/case/afp/delete'),
            'key' => 'id',
        );

        if (Request::get('area')) {
            $filter = json_decode(Request::get('area'));
            if (!empty($filter)) {
                $config['whereIn'] = array('a.code_provinsi_pasien' => $filter);
            }
        }
        if (Request::get('dt')) {
            $filter = json_decode(Request::get('dt'));
            // filter district
            if (isset($filter->district)) {
                $district = $filter->district;
                foreach ($district as $key => $val) {
                    if (!empty($val)) {
                        $config['where'][] = array('a.' . $key => $val);
                    }
                }
            }
            // filter time
            $time = $filter->filter->range;
            if ($time) {
                $from = $filter->from;
                $fy = (isset($from->year)) ? $from->year : null;
                $fm = (isset($from->month)) ? $from->month : null;
                $fd = (isset($from->day)) ? $from->day : null;
                $to = $filter->to;
                $ty = (isset($to->year)) ? $to->year : null;
                $tm = (isset($to->month)) ? $to->month : null;
                $td = (isset($to->day)) ? $to->day : null;
                if ($time == '1') {
                    $start = $fy . '-' . $fm . '-' . $fd;
                    $to = $ty . '-' . $tm . '-' . $td;
                    if ($fy && $fm && $fd && $ty && $tm && $td) {
                        $config['whereBetween'] = array('a.tgl_sakit' => array($start, $to));
                    }
                } else if ($time == '2') {
                    $start = $fy . '-' . $fm . '-' . '1';
                    $to = $ty . '-' . $tm . '-' . date('t', mktime(0, 0, 0, $tm, 1, $ty));
                    if ($fy && $fm && $ty && $tm) {
                        $config['whereBetween'] = array('a.tgl_sakit' => array($start, $to));
                    }
                } else if ($time == '3') {
                    $start = $fy;
                    $to = $ty;
                    if ($fy && $ty) {
                        $config['whereBetween'] = array('a.thn_tgl_sakit' => array($start, $to));
                    }
                }
            }
        }
        $Datatable = Datatables::getData($config);

        $daftarsampel = collect($Datatable['data'])->groupBy('id');
        $hasil = '';
        $jenis_pemeriksaan = '';
        $jenis_sampel = '';
        $tgl_periksa = '';
        $no = 1;

        if (count($Datatable['data']) > 0) {
            foreach ($daftarsampel as $keys => $value) {
                $umur_thn = (!empty($value[0]['umur_thn'])) ? $value[0]['umur_thn'] . ' Th ' : null;
                $umur_bln = (!empty($value[0]['umur_bln'])) ? $value[0]['umur_bln'] . ' Bln ' : null;
                $umur_hari = (!empty($value[0]['umur_hari'])) ? $value[0]['umur_hari'] . ' Hari ' : null;
                $no_epid = (empty($value[0]['no_epid_klb'])) ? $value[0]['no_epid'] : null;
                $name = $value[0]['name'];
                foreach ($value as $key => $val) {
                    if (!empty($val['tgl_mulai_periksa_kultur'])) {
                        $hasil .= 'Kultur : ' . $val['hasil_final_kultur_txt'] . '<br>';
                        $jenis_pemeriksaan .= 'Kultur Sel' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= $val['tgl_mulai_periksa_kultur'] . '<br>';
                    }
                    if (!empty($val['hasil_isolat_p1'])) {
                        $hasil .= 'Isolat P1 : ' . $val['hasil_isolat_p1'] . '<br>';
                        $jenis_pemeriksaan .= 'ITD' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= $val['tgl_isolat_pertama_diterima'] . '<br>';
                    }
                    if (!empty($val['hasil_isolat_p2'])) {
                        $hasil .= 'Isolat P2 : ' . $val['hasil_isolat_p2'] . '<br>';
                        $jenis_pemeriksaan .= 'ITD' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= $val['tgl_isolat_pertama_diterima'] . '<br>';
                    }
                    if (!empty($val['hasil_isolat_p3'])) {
                        $hasil .= 'Isolat P3 : ' . $val['hasil_isolat_p3'] . '<br>';
                        $jenis_pemeriksaan .= 'ITD' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= $val['tgl_isolat_pertama_diterima'] . '<br>';
                    }
                    if (!empty($val['hasil_sqc_p1'])) {
                        $hasil .= 'Sequencing P1 : ' . $val['hasil_sqc_p1'] . '<br>';
                        $jenis_pemeriksaan .= 'Sequencing' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= $val['tgl_periksa_sqc'] . '<br>';
                    }
                    if (!empty($val['hasil_sqc_p2'])) {
                        $hasil .= 'Sequencing P2 : ' . $val['hasil_sqc_p2'] . '<br>';
                        $jenis_pemeriksaan .= 'Sequencing' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= $val['tgl_periksa_sqc'] . '<br>';
                    }
                    if (!empty($val['hasil_sqc_p3'])) {
                        $hasil .= 'Sequencing P3 : ' . $val['hasil_sqc_p3'] . '<br>';
                        $jenis_pemeriksaan .= 'Sequencing' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= $val['tgl_periksa_sqc'] . '<br>';
                    }
                    if (!empty($val['hasil_polio_p1'])) {
                        $hasil .= 'Polio P1 : ' . $val['hasil_polio_p1'] . '<br>';
                        $jenis_pemeriksaan .= '-' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= '-' . '<br>';
                    }
                    if (!empty($val['hasil_polio_p2'])) {
                        $hasil .= 'Polio P2 : ' . $val['hasil_polio_p2'] . '<br>';
                        $jenis_pemeriksaan .= '-' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= '-' . '<br>';
                    }
                    if (!empty($val['hasil_polio_p3'])) {
                        $hasil .= 'Polio P3 : ' . $val['hasil_polio_p3'] . '<br>';
                        $jenis_pemeriksaan .= '-' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= '-' . '<br>';
                    }
                    if (!empty($val['hasil_npev'])) {
                        $hasil .= 'NPEV : ' . $val['hasil_npev'] . '<br>';
                        $jenis_pemeriksaan .= '-' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= '-' . '<br>';
                    }
                    if (!empty($val['hasil_entero_virus'])) {
                        $hasil .= 'Entero Virus : ' . $val['hasil_entero_virus'] . '<br>';
                        $jenis_pemeriksaan .= '-' . '<br>';
                        $jenis_sampel .= '-' . '<br>';
                        $tgl_periksa .= '-' . '<br>';
                    }
                }
                $dt[] = array(
                    'no' => $no,
                    'id' => $value[0]['id_spesimen'],
                    'no_epid' => $no_epid,
                    'name_pasien' => $name,
                    'umur_thn' => $umur_thn,
                    'umur_bln' => $umur_bln,
                    'umur_hari' => $umur_hari,
                    'jenis_kelamin' => $value[0]['jenis_kelamin_txt'],
                    'kabupaten' => $value[0]['kabupaten_name'],
                    'jenis_pemeriksaan' => $jenis_pemeriksaan,
                    'jenis_sampel' => $jenis_sampel,
                    'tgl_periksa' => $tgl_periksa,
                    'hasil' => $hasil,
                    'keadaan_akhir_txt' => $value[0]['keadaan_akhir_txt'],
                    'action' => $value[0]['action'],
                );
                $hasil = '';
                $jenis_pemeriksaan = '';
                $jenis_sampel = '';
                $tgl_periksa = '';
                $no++;
            }
            $Datatable['data'] = $dt;
        }
        echo json_encode($Datatable);
    }
}

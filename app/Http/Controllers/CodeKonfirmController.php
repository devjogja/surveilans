<?php

namespace App\Http\Controllers;
use DB, Session;

class CodeKonfirmController extends Controller
{
	public function konfirmpuskesmas($type)
	{
		if ($type == '1') {
			DB::statement("UPDATE mst_puskesmas SET konfirm_code='12345'");
		}else{
			DB::statement("UPDATE mst_puskesmas SET konfirm_code=SUBSTR(CONCAT(MD5(RAND()),MD5(RAND())),1,5)");
		}
		$q = DB::table('mst_puskesmas AS a');
		$q->select('d.name AS provinsi','c.name AS kabupaten','b.name AS kecamatan','a.code_faskes AS puskesmas_code_faskes','a.name AS puskesmas_name','a.konfirm_code AS konfirmasi');
		$q->join('mst_kecamatan AS b','a.code_kecamatan','=','b.code');
		$q->join('mst_kabupaten AS c','a.code_kabupaten','=','c.code');
		$q->join('mst_provinsi AS d','a.code_provinsi','=','d.code');
		$dt = $q->get();
		$sent   = "<table border=1>";
		$sent .= "<tr>
		<td>Provinsi</td>
		<td>Kabupaten</td>
		<td>Kecamatan</td>
		<td>Kode Faskes</td>
		<td>Nama Faskes</td>
		<td>Konfirmasi</td>
		</tr>";
		foreach ($dt as $key => $val) {
			$sent .= "<tr>";
			$sent .= "<td>$val->provinsi</td>";
			$sent .= "<td>$val->kabupaten</td>";
			$sent .= "<td>$val->kecamatan</td>";
			$sent .= "<td>$val->puskesmas_code_faskes</td>";
			$sent .= "<td>$val->puskesmas_name</td>";
			$sent .= "<td>$val->konfirmasi</td>";
			$sent .= "</tr>";
		}
		$sent .= "</table>";
		return $sent;
	}

	public function konfirmrs($type) {
		if ($type=='1') {
			DB::statement("UPDATE mst_rumahsakit SET konfirm_code='12345'");
		}else{
			DB::statement("UPDATE mst_rumahsakit SET konfirm_code=SUBSTR(CONCAT(MD5(RAND()),MD5(RAND())),1,5)");
		}
		$q = DB::table('mst_rumahsakit AS a');
		$q->select('d.name AS provinsi','c.name AS kabupaten','a.code_faskes AS rs_code_faskes','a.name AS rs_name','a.konfirm_code AS konfirmasi');
		$q->join('mst_kabupaten AS c','a.code_kabupaten','=','c.code');
		$q->join('mst_provinsi AS d','a.code_provinsi','=','d.code');
		$dt = $q->get();
		$sent   = "<table border=1>";
		$sent .= "<tr>
		<td>Provinsi</td>
		<td>Kabupaten</td>
		<td>Kode Faskes</td>
		<td>Nama Faskes</td>
		<td>Konfirmasi</td>
		</tr>";
		foreach ($dt as $key => $val) {
			$sent .= "<tr>";
			$sent .= "<td>$val->provinsi</td>";
			$sent .= "<td>$val->kabupaten</td>";
			$sent .= "<td>$val->rs_code_faskes</td>";
			$sent .= "<td>$val->rs_name</td>";
			$sent .= "<td>$val->konfirmasi</td>";
			$sent .= "</tr>";
		}
		$sent .= "</table>";
		return $sent;
	}
	public function konfirmkabupaten($type) {
		if ($type=='1') {
			DB::statement("UPDATE mst_kabupaten SET konfirm_code='12345'");
		}else{
			DB::statement("UPDATE mst_kabupaten SET konfirm_code=SUBSTR(CONCAT(MD5(RAND()),MD5(RAND())),1,5)");
		}
		$q = DB::table('mst_kabupaten AS a');
		$q->select('d.name AS provinsi','a.name AS kabupaten','a.code','a.konfirm_code AS konfirmasi');
		$q->join('mst_provinsi AS d','a.code_provinsi','=','d.code');
		$dt = $q->get();
		$sent   = "<table border=1>";
		$sent .= "<tr>
		<td>Provinsi</td>
		<td>Kabupaten</td>
		<td>Kode</td>
		<td>Konfirmasi</td>
		</tr>";
		foreach ($dt as $key => $val) {
			$sent .= "<tr>";
			$sent .= "<td>$val->provinsi</td>";
			$sent .= "<td>$val->kabupaten</td>";
			$sent .= "<td>$val->code</td>";
			$sent .= "<td>$val->konfirmasi</td>";
			$sent .= "</tr>";
		}
		$sent .= "</table>";
		return $sent;
	}
	public function konfirmprovinsi($type) {
		if ($type=='1') {
			DB::statement("UPDATE mst_provinsi SET konfirm_code='12345'");
		}else{
			DB::statement("UPDATE mst_provinsi SET konfirm_code=SUBSTR(CONCAT(MD5(RAND()),MD5(RAND())),1,5)");
		}
		$q = DB::table('mst_provinsi AS a');
		$q->select('a.name AS provinsi','a.code','a.konfirm_code AS konfirmasi');
		$dt = $q->get();
		$sent   = "<table border=1>";
		$sent .= "<tr>
		<td>Provinsi</td>
		<td>Kode</td>
		<td>Konfirmasi</td>
		</tr>";
		foreach ($dt as $key => $val) {
			$sent .= "<tr>";
			$sent .= "<td>$val->provinsi</td>";
			$sent .= "<td>$val->code</td>";
			$sent .= "<td>$val->konfirmasi</td>";
			$sent .= "</tr>";
		}
		$sent .= "</table>";
		return $sent;
	}
}

<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Response, Request;


class CourseController extends Controller
{
    //
    public function viewAdd()
    {
      $compact = array(
        'title'         => 'Surveilans PD3I',
        'contentTitle'  => 'Panduan Belajar Mandiri',
        'data'          => array()
        );
      return view('help.addCourse',$compact);
    }

    public function viewEdit($id)
    {
      $dtCourse = Course::getDataCourse(array('id'=>$id));
      $compact = array(
        'title'         => 'Surveilans PD3I',
        'contentTitle'  => 'Perubahan Panduan Belajar Mandiri',
        'data'          => $dtCourse[0]
        );
      return view('help.editCourse',$compact);
    }

    public function postCourse()
  	{
      $request = Request::json()->all();
      $data['id']=$request[0];
      $data['course_judul']=$request[1];
      $data['course_konten']=$request[2];
  		$success = true;
  		$code = 200;
      if(empty($data['id'])){
        Course::insert($data);
			}else{
				Course::pull(array('id'=>$data['id']),$data);
			}
      return Response::json(array(
  			'success'=>$success,
  			'request'=>$request,
  			),$code);
    }

    public function postDelete($id)
    {
      $request = array('id'=>$id);
      if($resp = Course::postDelete($id))
      {
        $success = true;
        $response = $resp;
        $code = '200';
      }else{
        $success = false;
        $response = $resp;
        $code = '401';
      }

      return Response::json(array(
        'success'=>$success,
        'request'=>$request,
        'response'=>$response
        ),$code);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Contactus;
use Response, Request;


class ContactUsController extends Controller
{
    //
    public function index()
    {
      $compact = array(
        'title'         => 'Surveilans PD3I',
        'contentTitle'  => 'Hubungi Kami',
        'data'          => array()
        );
      return view('help.contact_us',$compact);
    }

    public function postContactUs()
  	{
      $request = Request::json()->all();
  		$success = true;
  		$code = 200;
  		ContactUs::insert($request['cu']);
      return Response::json(array(
  			'success'=>$success,
  			'request'=>$request,
  			),$code);
    }
}

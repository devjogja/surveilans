<?php

namespace App\Http\Controllers;

use App\Models\Dashboard;
use App\Models\Tx;
use App\Models\TxCase;
use App\Models\TxNotification;
use App\Models\Wilayah;
use Carbon\Carbon;
use Helper;
use Request;
use Response;
use View;
use DB;
use Session;

class DashboardController extends Controller
{
    public function viewDashboard()
    {
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => null,
            'data' => array(),
        );
        return view('dashboard.index', $compact);
    }

    function dd()
    {
        $a = DB::select("
            SELECT
            a.id_faskes,
            i.name AS nama_pkm,
            i.name_provinsi AS nama_prov_pkm,
            i.name_kabupaten AS nama_kab_pkm,
            j.name AS nama_rs,
            j.name_provinsi AS nama_prov_rs,
            j.name_kabupaten AS nama_kab_rs
            FROM
            trx_case a
            LEFT JOIN view_puskesmas i ON a.id_faskes = i.id AND a.id_role = 1
            LEFT JOIN view_rumahsakit j ON a.id_faskes = j.id AND a.id_role = 2
            WHERE
            a.id_case = '1' AND a.deleted_at IS NULL
            ");
        $dd = [];
        foreach ($a as $key => $val) {
            $val = (array) $val;
            $dd['prov_pkm'][$val['nama_prov_pkm']][$val['nama_kab_pkm']][] = $val;
            $dd['prov_rs'][$val['nama_prov_rs']][$val['nama_kab_rs']][] = $val;
        }
    }

    public function moveCase()
    {
        $req = Request::json()->all();
        $status = false;
        if (!empty($req['query']['id'][0])) {
            $id = $req['query']['id'][0];
            $q = DB::table("trx_case AS a");
            $q->join("ref_pasien AS b", 'a.id_pasien', '=', 'b.id');
            $q->select('b.code_kelurahan', 'a.id_faskes');
            $q->where('a.id', $id);
            $case = $q->first();
            $code_kelurahan_pasien = $case->code_kelurahan;
            $code_faskes_old = $case->id_faskes;

            $code_faskes = DB::table('mst_wilayah_kerja_puskesmas')->select('code_faskes')->where('code_kelurahan', $code_kelurahan_pasien)->first();
            if (!empty($code_faskes)) {
                $faskes = DB::table('mst_puskesmas')->select('id')->where('code_faskes', $code_faskes->code_faskes)->first();
                $dt = [
                    'id_faskes' => $faskes->id,
                    'id_faskes_old' => $code_faskes_old,
                    'id_role' => 1,
                    'id_role_old' => 1,
                ];
                $status = Tx::movedData('trx_case', ['id' => $id], $dt);
            }
        }

        return Response::json(array(
            'success' => $status,
        ), 200);
    }

    public function viewDashboardInstansi()
    {
        if (Helper::role()->id_role == '4') {
            $dt['campak'] = DB::table('view_list_spesimen')->where('id_case', 1)->count();
            $dt['afp'] = DB::table('view_list_spesimen')->where('id_case', 2)->count();
            $dt['crs'] = DB::table('view_list_spesimen')->where('id_case', 5)->count();
            $compact = array(
                'title' => 'Surveilans PD3I',
                'contentTitle' => 'Dashboard Laboratorium',
                'data' => $dt,
            );
            return view('lab.dashboard', $compact);
        } else {
            $role = Helper::role();
            if ($role->code_role == 'puskesmas') {
                $wakses = ['a.id_role' => '1', 'a.id_faskes' => $role->id_faskes, 'a.thn_sakit' => date('Y')];
                $wCross = ['to_faskes' => $role->code_kecamatan_faskes, 'type_notification' => 'cross'];
                $wKlb = ['type_notification' => 'klb'];
                // $wKlb = ['to_faskes' => $role->code_kecamatan_faskes, 'type_notification' => 'klb'];
                $wRs = ['code_roles' => 'rs', 'code_kecamatan_pasien' => $role->code_kecamatan_faskes];
            } else if ($role->code_role == 'rs') {
                $wakses = ['a.id_role' => '2', 'a.id_faskes' => $role->id_faskes];
                $wCross = [];
                $wKlb = ['type_notification' => 'klb'];
                // $wKlb = ['to_faskes' => $role->code_kecamatan_faskes, 'type_notification' => 'klb'];
                $wRs = ['code_roles' => 'rs', 'code_kecamatan_pasien' => $role->code_kecamatan_faskes];
            } else if ($role->code_role == 'kabupaten') {
                $wakses = ['a.code_kabupaten_faskes' => $role->id_faskes];
                $wCross = ['to_faskes_kabupaten' => $role->id_faskes, 'type_notification' => 'cross'];
                $wKlb = ['type_notification' => 'klb'];
                // $wKlb = ['to_faskes_kabupaten' => $role->id_faskes, 'type_notification' => 'klb'];
                $wRs = ['code_roles' => 'rs', 'code_kabupaten_pasien' => $role->id_faskes];
            } else if ($role->code_role == 'provinsi') {
                $wakses = ['a.code_provinsi_faskes' => $role->id_faskes];
                $wCross = ['to_faskes_provinsi' => $role->id_faskes, 'type_notification' => 'cross'];
                $wKlb = ['type_notification' => 'klb'];
                // $wKlb = ['to_faskes_provinsi' => $role->id_faskes, 'type_notification' => 'klb'];
                $wRs = ['code_roles' => 'rs', 'code_provinsi_pasien' => $role->id_faskes];
            } else {
                $wakses = [];
                $wCross = ['type_notification' => 'cross'];
                $wKlb = ['type_notification' => 'klb'];
                $wRs = ['code_roles' => 'rs'];
            }
            $spesimen = Dashboard::getSpesimen();
            $cross_notif = TxNotification::getData($wCross)->get();
            $klb_notif = TxNotification::getData($wKlb)->get();
            // $rs_notif = Tx::getData('view_faskes_pasien', $wRs)->get();
            $rs_notif = 0;
            $compact = array(
                'title' => 'Surveilans PD3I',
                'contentTitle' => 'Dashboard Instansi',
                'data' => [
                    'cross_notif' => $cross_notif,
                    'klb_notif' => $klb_notif,
                    'rs_notif' => $rs_notif,
                    'spesimen' => $spesimen,
                ],
            );
            return view('dashboard.dashboard', $compact);
        }
    }

    public function getAnalisaWilayah()
    {
        $request = Request::json()->all();
        $index = $request['case_type'];
        if ($index == 'campak') {
            $tgl_sakit = 'tgl_mulai_rash';
        } else {
            $tgl_sakit = 'tgl_sakit';
        }
        if (!empty($request['range'])) {
            $dt = $request['from'];
            if (empty($dt['month'])) {
                $dt['month'] = 0;
            }
            if (empty($dt['day'])) {
                $dt['day'] = 0;
            }
            $rangeFrom = Carbon::create($dt['year'], $dt['month'], $dt['day']);
            $dt = $request['to'];
            if (empty($dt['month'])) {
                $dt['month'] = 0;
            }
            if (empty($dt['day'])) {
                $dt['day'] = 0;
            }
            $rangeTo = Carbon::create($dt['year'], $dt['month'], $dt['day']);
            $range = [$rangeFrom, $rangeTo];
            $dt = TxCase::getDataAnalisaTime($request['filter'], $index, $tgl_sakit, $range);
        } else {
            $dt = TxCase::getDataAnalisa($request['filter'], $index);
        }
        if ($request['filter_type'] == '1') {
            $filterprov = 'code_provinsi_pasien';
            $filterkab = 'code_kabupaten_pasien';
            $filterkec = 'code_kecamatan_pasien';
            $filterkel = 'code_kelurahan_pasien';
        } else {
            $filterprov = 'code_provinsi_faskes';
            $filterkab = 'code_kabupaten_faskes';
            $filterkec = 'code_kecamatan_faskes';
            $filterpus = 'code_faskes';
        }
        if (!empty($dt)) {
            // ---getdata for wilayah nasional---
            $wilayahnasional = collect($dt)->groupBy($filterprov);
            $listprovince = Helper::getProvince();
            foreach ($listprovince as $keys => $values) {
                if (!empty($wilayahnasional[$keys])) {
                    $senasional['name'] = $values;
                    $senasional['y'] = count($wilayahnasional[$keys]);
                    $senasional['drilldown'] = $values;

                    //----get data per provinsi (drilldown liat data kabupaten)----
                    $wilayahprov = collect($wilayahnasional[$keys])->groupBy($filterkab);
                    $listkab = Wilayah::getKabupaten(array('code_provinsi' => $keys));
                    $province['name'] = $values;
                    $province['id'] = $values;
                    foreach ($listkab as $key => $value) {
                        if (!empty($wilayahprov[$key])) {
                            $kab['name'] = $value->name;
                            $kab['y'] = count($wilayahprov[$key]);
                            $kab['drilldown'] = $value->name;
                            $wilayahkab = collect($wilayahprov[$key])->groupBy($filterkec);
                            $listkec = Wilayah::getKecamatan(array('code_kabupaten' => $key));
                            $kabupaten['name'] = $value->name;
                            $kabupaten['id'] = $value->name;
                            foreach ($listkec as $ky => $val) {
                                if (!empty($wilayahkab[$ky])) {
                                    $kec['name'] = $val->name;
                                    $kec['y'] = count($wilayahkab[$ky]);
                                    $kec['drilldown'] = $val->name;
                                    if ($request['filter_type'] == '1') {
                                        $wilayahkec = collect($wilayahkab[$ky])->groupBy($filterkel);
                                        $lists = Wilayah::getKelurahan(array('code_kecamatan' => $ky));
                                    } else {
                                        $wilayahkec = collect($wilayahkab[$ky])->groupBy($filterpus);
                                        $lists = Wilayah::getPuskesmas(array('code_kecamatan' => $ky));
                                    }

                                    $kecamatan['name'] = $val->name;
                                    $kecamatan['id'] = $val->name;
                                    foreach ($lists as $k => $v) {
                                        if (!empty($wilayahkec[$k])) {
                                            $kel['name'] = $v->name;
                                            $kel['y'] = count($wilayahkec[$k]);
                                        } else {
                                            $kel['name'] = $v->name;
                                            $kel['y'] = 0;
                                        }
                                        $kecamatan['data'][] = $kel;
                                        $kel = [];
                                    }
                                    $data['lol'] = $wilayahkec;
                                    $drilldown[] = $kecamatan;
                                    $kecamatan = [];
                                } else {
                                    $kec['name'] = $val->name;
                                    $kec['y'] = 0;
                                }
                                $kabupaten['data'][] = $kec;
                                $kec = [];
                            }
                            $drilldown[] = $kabupaten;
                            $kabupaten = [];
                        } else {
                            $kab['name'] = $value->name;
                            $kab['y'] = 0;
                        }
                        $province['data'][] = $kab;
                        $kab = [];
                    }
                    $drilldown[] = $province;
                    $province = [];
                } else {
                    $senasional['name'] = $values;
                    $senasional['y'] = 0;
                    $drilldown[] = [0];
                }

                $datawilayah['data'][] = $senasional;
                $senasional = [];
            }
            $datawilayah['name'] = 'Indonesia';
            $datawilayah['colorByPoint'] = false;
            $tmpwil['data'][] = $datawilayah;
            $tmpwil['drilldown'] = $drilldown;
            $data['data'] = $tmpwil;
        } else {
            $listprovince = Helper::getProvince();
            foreach ($listprovince as $keys => $value) {
                $senasional['name'] = $value;
                $senasional['y'] = 0;
                $datawilayah['data'][] = $senasional;
                $senasional = [];
            }
            $datawilayah['name'] = 'Indonesia';
            $datawilayah['colorByPoint'] = false;
            $data['data'][] = $datawilayah;
        }
        $response = $data;
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }
}

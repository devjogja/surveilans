<?php

namespace App\Http\Controllers;

use Request, DB;
use App\Models\Tx;
use App\Libraries\Datatable;


class ProvinsiController extends Controller
{
	public function index()
	{
		$compact = array(
			'title' => 'Surveilans PD3I',
			'contentTitle'  => 'Provinsi',
			'data' => array()
		);
		return view('district.provinsi', $compact);
	}

	public function getData()
	{
		$config['table'] = 'mst_provinsi AS a';
		$config['coloumn'] = ['code', 'code', 'name', 'longitude', 'latitude', 'konfirm_code'];
		$config['columnSelect'] = ['a.code', 'a.code', 'a.name', 'a.longitude', 'a.latitude', 'a.konfirm_code'];
		$config['columnFormat'] = ['number', '', '', '', '', ''];
		$config['searchColumn'] = ['a.code', 'a.name'];
		$config['where'][] = ['a.deleted_at' => null];
		$config['key'] = 'a.code';
		$config['action'][]		= array(
			'action'	=> 'editMaster',
			'url' 		=> url('district/provinsi/edit'),
			'key'		=> 'code',
		);
		$config['action'][]		= array(
			'action'	=> 'delete',
			'url'		=> url('district/provinsi/delete'),
			'key'		=> 'code',
		);
		$data = Datatable::getData($config);
		echo json_encode($data);
	}

	public function postData()
	{
		$id = Request::get('id');
		$data 	= Request::get('dt');
		if (empty($id)) {
			$result = DB::table('mst_provinsi')->insert($data);
		} else {
			if (Tx::getData('mst_provinsi', ['code' => $data['code']])->first()) {
				unset($data['code']);
			}
			$result = DB::table('mst_provinsi')->where(['code' => $id])->update($data);
		}
		if ($result) {
			$response 	= array(
				'success'	=> true,
				'messageType'	=> 'info',
				'title'		=> 'Berhasil!',
				'message'	=> 'Data Provinsi berhasil disimpan',
			);
		} else {
			$response 	= array(
				'success'	=> false,
				'messageType'	=> 'danger',
				'title'		=> 'Gagal!',
				'message'	=> 'Data Provinsi gagal disimpan',

			);
		}
		echo (json_encode($response));
	}

	public function getDetail($id = '')
	{
		$response = Tx::getData('mst_provinsi', ['code' => $id])->first();
		echo (json_encode($response));
	}

	public function postDelete($id)
	{
		if (Tx::postDelete('mst_provinsi', ['code' => $id])) {
			$response 	= array(
				'success'	=> 1,
				'messageType'	=> 'info',
				'title'		=> 'Berhasil!',
				'message'	=> 'Data Provinsi berhasil dihapus',
			);
		} else {
			$response 	= array(
				'success'	=> 0,
				'messageType'	=> 'danger',
				'title'		=> 'Gagal!',
				'message'	=> 'Data Provinsi gagal dihapus',
			);
		}
		echo (json_encode($response));
	}
}

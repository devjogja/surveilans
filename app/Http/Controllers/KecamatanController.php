<?php

namespace App\Http\Controllers;

use Request, DB;
use App\Models\Tx;
use App\Libraries\Datatable;

class KecamatanController extends Controller
{
	public function index()
	{
		$compact = array(
			'title'         => 'Surveilans PD3I',
			'contentTitle'  => 'Master Kecamatan',
			'data'          => array()
			);
		return view('district.kecamatan',$compact);
	}

	public function getData()
	{
		$config['table'] = 'mst_kecamatan AS a';
		$config['coloumn'] = ['code','code_kabupaten','code','name'];
		$config['columnSelect'] = ['a.code','a.code_kabupaten','a.code','a.name'];
		$config['columnFormat'] = ['number','','',''];
		$config['searchColumn'] = ['a.code_kabupaten','a.code','a.name'];
		$config['where'][] = ['a.deleted_at'=>null];
		$config['key'] = 'a.code';
		$config['action'][]		= array(
			'action'	=> 'editMaster',
			'url' 		=> url('district/kecamatan/edit'),
			'key'		=> 'code',
			);
		$config['action'][]		= array(
			'action'	=> 'delete',
			'url'		=> url('district/kecamatan/delete'),
			'key'		=> 'code',
			);
		$data = Datatable::getData($config);
		echo json_encode($data);
	}

	public function postData()
	{
		$id = Request::get('id');
		$data 	= Request::get('dt');
		if(empty($id)){
			$result = DB::table('mst_kecamatan')->insert($data);
		}else{
			if (Tx::getData('mst_kecamatan',['code'=>$data['code']])->first()) {
				unset($data['code']);
			}
			$result = DB::table('mst_kecamatan')->where(['code'=>$id])->update($data);
		}
		if($result){
			$response 	= array(
				'success'	=> true,
				'messageType'	=> 'info',
				'title'		=> 'Berhasil!',
				'message'	=> 'Data Kecamatan berhasil disimpan',
			);
		}else{
			$response 	= array(
				'success'	=> false,
				'messageType'	=> 'danger',
				'title'		=> 'Gagal!',
				'message'	=> 'Data Kecamatan gagal disimpan',

			);
		}
		echo(json_encode($response));
	}

	public function getDetail($id='')
	{
		$rkecamatan = Tx::getData('mst_kecamatan',['code'=>$id])->first();
		$rMapping_area = Tx::getData('view_area',['code_kabupaten'=>$rkecamatan->code_kabupaten])->first();
		$response = [
			'code_provinsi'=>$rMapping_area->code_provinsi,
			'code_kabupaten'=>$rMapping_area->code_kabupaten,
			'name_kabupaten'=>$rMapping_area->name_kabupaten,
			'code'=>$rkecamatan->code,
			'name'=>$rkecamatan->name,
		];
		echo(json_encode($response));
	}

	public function postDelete($id)
	{
		if(Tx::postDelete('mst_kecamatan',['code'=>$id]))
		{
			$response 	= array(
				'success'	=> 1,
				'messageType'	=> 'info',
				'title'		=> 'Berhasil!',
				'message'	=> 'Data Kecamatan berhasil dihapus',
			);
		}else{
			$response 	= array(
				'success'	=> 0,
				'messageType'	=> 'danger',
				'title'		=> 'Gagal!',
				'message'	=> 'Data Kecamatan gagal dihapus',
			);
		}
		echo(json_encode($response));
	}
}
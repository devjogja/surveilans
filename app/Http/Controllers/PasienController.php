<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Request, Response, Helper;
use App\Models\Pasien;
use App\Models\Tx;

class PasienController extends Controller
{
	public function getAge()
	{
		$request = Request::json()->all();
		$tgl_lahir = strtotime($request['tgl_lahir']);
		$tgl_sakit = strtotime($request['tgl_sakit']);
		// $umur = $request['umur'];
		// $umur_bln = $request['umur_bln'];
		// $umur_hari = $request['umur_hari'];
		// if($tgl_lahir!=''){
		$response = Helper::getAge(['tgl_sakit' => $tgl_sakit, 'tgl_lahir' => $tgl_lahir]);
		/*}else{
			$y = substr($tgs,0,4);
			$m = substr($tgs,5,2);
			$d = substr($tgs,8,2);
			$tgl_lahir = date('d/m/Y', mktime(1,1,1,$m-$umur_bln,$d-$umur_hari,$y-$umur));
			$response = array('tgl_lahir'=>$tgl_lahir);
		}*/
		return Response::json(array(
			'request' => $request,
			'response' => $response
		));
	}

	public function getData()
	{
		$key = Request::get('term');
		$where = array('name' => $key);
		$response = Pasien::getData($where);
		echo json_encode($response);
	}

	public function getDataPasien($id_pasien = null)
	{
		if ($id_pasien) {
			$request['where']['id_pasien'] = [$id_pasien];
		} else {
			$request = Request::json()->all();
		}
		if (isset($request['where'])) {
			foreach ($request['where'] as $key => $val) {
				if ($val[0] == '*') {
					$qQuery['where'][$key] = '*';
				} else {
					$qQuery['whereIn'][$key] = $val;
				}
			}
		}
		if (isset($request['query'])) {
			foreach ($request['query'] as $key => $val) {
				$qQuery[$key] = $val;
			}
		}
		if (!empty($request['pagination'])) {
			$qQuery['pagination'] = $request['pagination'];
		}
		$response = Tx::getData('view_pasien', [], $qQuery)->get();
		return Response::json(array(
			'success' => true,
			'response' => $response
		), 200);
	}

	public function getDataFamily()
	{
		$request = Request::json()->all();
		if (isset($request['where'])) {
			foreach ($request['where'] as $key => $val) {
				if ($val[0] == '*') {
					$qQuery['where'][$key] = '*';
				} else {
					$qQuery['whereIn'][$key] = $val;
				}
			}
		}
		if (isset($request['query'])) {
			foreach ($request['query'] as $key => $val) {
				$qQuery[$key] = $val;
			}
		}
		if (!empty($request['pagination'])) {
			$qQuery['pagination'] = $request['pagination'];
		}
		$response = Tx::getData('view_family', [], $qQuery)->get();
		return Response::json(array(
			'success' => true,
			'response' => $response
		), 200);
	}

	public function postPasien()
	{
		$req = Request::json()->all();
		$response = [];
		foreach ($req as $key => $request) {
			(isset($request['dp']['created_at'])) ? $request['dp']['created_at'] = date('Y-m-d H:i:s', strtotime($request['dp']['created_at'])) : null;
			(isset($request['dp']['updated_at'])) ? $request['dp']['updated_at'] = date('Y-m-d H:i:s', strtotime($request['dp']['updated_at'])) : null;
			$request['dp']['tgl_lahir'] = (isset($request['dp']['tgl_lahir'])) ? Helper::dateFormat($request['dp']['tgl_lahir']) : null;
			if (!empty($request['dp']['id'])) {
				$id_pasien = $request['dp']['id'];
				Tx::updateData('ref_pasien', ['id' => $id_pasien], $request['dp']);
			} else {
				$id_pasien = Tx::insertData('ref_pasien', $request['dp']);
			}
			$pasien = Tx::getData('view_pasien', ['id' => $id_pasien], ['select' => ['created_at', 'updated_at']])->first();
			$response[] = [
				'id' => $id_pasien,
				'created_at' => $pasien->created_at,
				'updated_at' => $pasien->updated_at,
			];
		}

		return Response::json(array(
			'success' => true,
			'response' => $response
		), 200);
	}

	public function postDeletePasien()
	{
		$rId = Request::json()->all();
		$response = [];
		foreach ($rId['query']['delete'] as $key => $val) {
			$response[$val] = Tx::deleteData('ref_pasien', ['id' => $val]);
		}
		return Response::json(array(
			'success' => true,
			'response' => $response
		), 200);
	}

	public function postFamily()
	{
		$req = Request::json()->all();
		$response = [];
		foreach ($req as $key => $request) {
			(isset($request['df']['created_at'])) ? $request['df']['created_at'] = date('Y-m-d H:i:s', strtotime($request['df']['created_at'])) : null;
			(isset($request['df']['updated_at'])) ? $request['df']['updated_at'] = date('Y-m-d H:i:s', strtotime($request['df']['updated_at'])) : null;
			if (!empty($request['df']['id'])) {
				$id_family = $request['df']['id'];
				Tx::updateData('ref_family', ['id' => $id_family], $request['df']);
			} else {
				$id_family = Tx::insertData('ref_family', $request['df']);
			}
			$family = Tx::getData('view_family', ['id' => $id_family], ['select' => ['created_at', 'updated_at']])->first();
			$response[] = [
				'id' => $id_family,
				'created_at' => $family->created_at,
				'updated_at' => $family->updated_at,
			];
		}

		return Response::json(array(
			'success' => true,
			'response' => $response
		), 200);
	}

	public function postDeleteFamily()
	{
		$rId = Request::json()->all();
		$response = [];
		foreach ($rId['query']['delete'] as $key => $val) {
			$response[$val] = Tx::deleteData('ref_family', ['id' => $val]);
		}
		return Response::json(array(
			'success' => true,
			'response' => $response
		), 200);
	}

	public function getDataLab()
	{
		$case = Request::get('case');
		$key = Request::get('term');
		$response = Pasien::getDataLab(['key' => $key], $case);
		return Response::json($response);
	}
}

<?php

namespace App\Http\Controllers;

use App\Models\ViewRoleUser;
use App\Models\Tx;
use App\Http\Requests;
use Response, Datatables, DB, Request, Sentinel;

class UserController extends Controller
{
	public function sycuser(){
		$dt = file_get_contents('http://localhost/sisfo/surveilans_v1/user/getdata');
		foreach ($dt as $key => $val) {
			$a = json_decode($val);
		}
		foreach ($da as $key => $val) {
			// echo"<pre>";print_r($val);echo"</pre>";die();
		}
	}

	public function getData($id)
	{
		$request = Request::all();
		$d = DB::table('view_list_petugas');
		$d->where('role_id',$id);
		$q = Datatables::of($d);
		if ($keyword = $request['search']['value']) {
			$q->filterColumn('no', 'whereRaw', '@no  + 1 like ?', ["%{$keyword}%"]);
			foreach ($request['columns'] as $key => $val) {
				if(!empty($val['name'])){
					$q->filterColumn($val['name'], 'whereRaw', $val['name'].' like ?', ["%{$keyword}%"]);
				}
			}
		}
        $q->addIndexColumn();
		$q->addColumn('action', function ($dt) {
			$action = "<div class='btn-group'>";
			$action .= "<a action=".url("user/resetpassword/".$dt->id_user)." title='Reset Password' data-toggle='modal' data-target='.modalalert' class='btn-sm btn-warning resetpassword'><i class='fa fa-expeditedssl'></i></a>";
			$action .= '<a action="'.url('user/delete/'.$dt->id_role_user).'" title="Delete data" class="btn-sm btn-danger delete" data-toggle="modal" data-target=".modaldelete"><i class="fa fa-remove"></i></a>';
			$action .= '</div>';
			return $action;
		});
		return $q->make(true);
	}

	public function postDelete($id=null)
	{
		$rId = Request::json()->all();
		if ($id!=null) {
			$rId['query']['delete'][] = $id;
		}
		$response = [];
		foreach ($rId['query']['delete'] as $key => $val) {
			$response[$val] = Tx::deleteData('role_users',['id'=>$val]);
		}
		return Response::json(array(
			'success'=>true,
			'response'=>$response
			),200);
	}
	public function postResetPassword($id=null)
	{
		$request['password'] = '$2y$10$yboH9RkvGbN5OUesW9wH0Oqc4ghKQxzNHPbeVl3t8DcqMsLMYXHAO';
		$d = Tx::updateData('users', ['id' => $id], $request);
		return Response::json(array(
			'success'=>true,
			),200);
	}
}

<?php

namespace App\Http\Controllers;

use App\Models\Sql;
use App\Models\Tx;
use App\Models\TxCase;
use Carbon\Carbon;
use Datatables;
use DB;
use Excel;
use Helper;
use Request;
use Response;
use Validator;

class DifteriController extends Controller
{
    protected $index;
    public function __construct()
    {
        $this->index = 'difteri';
    }

    public function index()
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Penyakit ' . $dc[0]->name,
            'data' => array(
                'id_trx_case' => null,
                'index' => $index,
            ),
        );
        return view('case.' . $index . '.index', $compact);
    }

    public function exportExampleImportKasus()
    {
        $file = public_path() . "/download/difteri/template import difteri.xls";
        return Response::download($file, 'Contoh format import kasus difteri.xls');
    }

    public function importCase()
    {
        $path = Request::file('import');
        if (!empty($path)) {
            $data = Excel::load($path, function ($reader) {
            })->get();
            $role = Helper::role();
            $row = array();
            if (!empty($data) && $data->count()) {
                foreach ($data->toArray() as $key => $val) {
                    $cd = DB::table('view_list_case')->where(['id_case' => 3, 'name_pasien' => $val['nama_pasien'], 'code_kelurahan_pasien' => $val['wilayah'], 'tgl_sakit_date' => Helper::dateFormat($val['tgl_mulai_demam']), 'nik' => $val['nik']])->count();
                    if ($cd == 0) {
                        $ut = $val['thn'];
                        $ub = $val['bln'];
                        $uh = $val['hari'];
                        if ($val['tanggal_lahir'] && $val['tgl_mulai_demam']) {
                            $age = Helper::getAge(['tgl_sakit' => $val['tgl_mulai_demam'], 'tgl_lahir' => $val['tanggal_lahir']]);
                            $ut = $age['years'];
                            $ub = $age['month'];
                            $uh = $age['day'];
                        }
                        $wilayah = DB::table('view_area')->where(['code_kelurahan' => $val['wilayah']])->first();
                        $cekpasien = DB::table('ref_pasien')->where(['name' => $val['nama_pasien'], 'nik' => $val['nik'], 'code_kelurahan' => $val['wilayah']])->first();
                        $row[] = [
                            "dc" => [
                                'id_faskes' => $role->id_faskes,
                                'id_role' => $role->id_role,
                                "jenis_input" => "3",
                                "no_rm" => $val['norm'],
                                "no_epid_lama" => $val['noepid_lama'],
                                "klasifikasi_final" => $val['klasifikasi_final'],
                            ],
                            'code_wilayah_faskes' => $role->code_wilayah_faskes,
                            'id_trx_case' => '',
                            'dp' => [
                                "name" => $val['nama_pasien'],
                                "nik" => $val['nik'],
                                "agama" => $val['agama'],
                                "jenis_kelamin" => $val['jenis_kelamin'],
                                "tgl_lahir" => $val['tanggal_lahir'],
                                "umur_thn" => $ut,
                                "umur_bln" => $ub,
                                "umur_hari" => $uh,
                                "alamat" => $val['alamat'],
                                "code_kelurahan" => $val['wilayah'],
                                "code_kecamatan" => (!empty($wilayah)) ? $wilayah->code_kecamatan : null,
                                "code_kabupaten" => (!empty($wilayah)) ? $wilayah->code_kabupaten : null,
                                "code_provinsi" => (!empty($wilayah)) ? $wilayah->code_provinsi : null,
                            ],
                            'id_pasien' => ($cekpasien) ? $cekpasien->id : '',
                            'df' => [
                                'name' => $val['nama_orang_tua'],
                            ],
                            "dk" => [
                                "tgl_mulai_demam" => $val['tgl_mulai_demam'],
                                "jml_imunisasi_dpt" => $val['jml_imunisasi_dpt'],
                                "tgl_imunisasi_difteri" => $val['tgl_imunisasi_difteri'],
                                "tgl_pelacakan" => $val['tgl_pelacakan'],
                                "gejala_lain" => $val['gejala_lain'],
                                "jml_kontak" => $val['jml_kontak'],
                                "diambil_spec_kontak" => $val['diambil_spec_kontak'],
                                "positif_kontak" => $val['positif_kontak'],
                                "keadaan_akhir" => $val['keadaan_akhir'],
                            ],
                            "ds" => [
                                "spesimen" => [
                                    $val['jenis_spesimen'] . '|' . $val['tgl_ambil_spesimen'] . '|' . $val['jenis_pemeriksaan'] . '|' . $val['kode_spesimen'] . '|' . $val['hasil'],
                                ],
                            ],
                        ];
                    }
                }
            }
            if ($row) {
                $difteri = $this->postCase($row);
                $response = json_decode($difteri->getContent())->response;
            }
            echo true;
        }
    }

    public function export($request = '')
    {
        $param['filter'] = json_decode($request);
        if (!empty($param['filter']->code_role)) {
            $param['roleUser'] = (object) array(
                'code_role' => $param['filter']->code_role,
                'id_faskes' => $param['filter']->id_faskes,
                'code_faskes' => $param['filter']->code_faskes,
            );
        }
        $q = Sql::getData("difteri", $param);
        $q->join('difteri_pe AS c', 'a.id_trx_case', '=', 'c.id_trx_case', 'left');
        $q->groupBy('a.id_trx_case');
        $r = $q->get();

        $compact['dd'] = $r;
        // return view('case.difteri.export', $compact);
        $export = view('case.difteri.export', $compact);
        $filename = 'export_difteri-' . date('YmdHi') . '.xls';

        header("Content-type: application/vnd.ms-excel; name='excel'");
        header("Cache-Control: max-age=0");
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Content-Transfer-Encoding: text');
        echo $export;
    }

    public function moveCase($id = null)
    {
        if (!empty($id)) {
            $role = Helper::role();
            $case = Tx::getData('difteri', ['id' => $id], ['select' => ['code_kelurahan_pasien', 'id_faskes']])->first();
            $code_kelurahan_pasien = $case->code_kelurahan_pasien;
            $code_faskes_old = $case->id_faskes;
            $code_faskes = Tx::getData('mst_wilayah_kerja_puskesmas', ['code_kelurahan' => $code_kelurahan_pasien], ['code_faskes'])->first();
            if (!empty($code_faskes)) {
                $faskes = Tx::getData('mst_puskesmas', ['code_faskes' => $code_faskes->code_faskes])->first();
                $dt = [
                    'id_faskes' => $faskes->id,
                    'id_faskes_old' => $code_faskes_old,
                    'id_role' => '1',
                    'id_role_old' => $role->id_role,
                ];
                $status = Tx::movedData('trx_case', ['id' => $id], $dt);
            } else {
                $status = false;
            }
        }
        return Response::json(array(
            'success' => $status,
        ), 200);
    }

    public function viewAnalisa()
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Analisa Penyakit ' . $dc[0]->name,
            'data' => array(
                'id_trx_case' => null,
                'index' => $index,
            ),
        );
        return view('case.' . $index . '.analisa_mobile', $compact);
    }

    public function getData()
    {
        $request = Request::json()->all();
        $qWhere = $qQuery = [];
        if (isset($request['where'])) {
            foreach ($request['where'] as $key => $val) {
                $qQuery['whereIn'][$key] = $val;
            }
        }
        if (isset($request['query'])) {
            foreach ($request['query'] as $key => $val) {
                $qQuery[$key] = $val;
            }
        }
        $response = Tx::getData('difteri', $qWhere, $qQuery)->get();
        return Response::json(array(
            'success' => true,
            'request' => $request,
            'response' => $response,
        ), 200);
    }

    public function getDataDifteri()
    {
        $request = Request::all();
        $param['filter'] = json_decode($request['dt']);
        $param['roleUser'] = Helper::role();
        $param['query']['select'] = ['id_trx_case', 'no_epid', 'name_pasien', 'umur_hari', 'umur_bln', 'umur_thn', 'code_kecamatan_pasien', 'full_address', 'keadaan_akhir_txt', 'klasifikasi_final_txt', 'id_role', 'name_faskes', 'code_kecamatan_faskes', 'jenis_input', 'id_pe', 'id_faskes_old', 'id_role_old', 'tgl_input', 'tgl_update', 'jenis_input_text', 'pe_text'];
        $param['query']['where'] = ['id_case' => 3];
        $d = Sql::getData("view_list_case", $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            foreach ($request['columns'] as $key => $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->editColumn('no_epid', function ($dt) {
            $no_epid = $dt->no_epid;
            if ($dt->jenis_input == '1') {
                $no_epid .= '<br><span class="label label-info">Web</span>';
            } else if ($dt->jenis_input == '2') {
                $no_epid .= '<br><span class="label label-success">Android</span>';
            } else if ($dt->jenis_input == '3') {
                $no_epid .= '<br><span class="label label-warning">Import</span>';
            }
            if ($dt->id_role == '2') {
                $no_epid .= "<span class='label label-primary' style='background-color:#b503b5 !important;'> Data dari rumahsakit </span>";
            }
            if ($dt->id_faskes_old) {
                $fo = '';
                if ($dt->id_role_old == '1') {
                    $fo = 'PUSKESMAS ' . DB::table('mst_puskesmas')->select(['name'])->where('id', $dt->id_faskes_old)->first()->name;
                } else if ($dt->id_role_old == '2') {
                    $fo = DB::table('mst_rumahsakit')->select(['name'])->where('id', $dt->id_faskes_old)->first()->name;
                }
                $no_epid .= "<span class='label label-primary'> Data kiriman dari " . $fo . "</span>";
            }
            return $no_epid;
        });
        $q->editColumn('id_pe', function ($dt) {
            $roleUser = Helper::role();
            $disabled = '';
            if (!in_array($roleUser->code_role, ['puskesmas', 'rs'])) {
                $disabled = "disabled='disabled' onclick='return false;'";
            }
            $pe = '<a href="' . url('case/difteri/pe/' . $dt->id_trx_case) . '"' . $disabled . ' title="Penyelidikan Epidemologi" data-toggle="tooltip" class="btn btn-sm btn-flat btn-success">PE</a>';
            if ($dt->id_pe) {
                $pe = '<a title="Penyelidikan Epidemologi" data-toggle="tooltip" class="btn btn-sm btn-flat btn-danger">Sudah PE</a>';
            }
            return $pe;
        });
        $q->addColumn('action', function ($dt) {
            $roleUser = Helper::role();
            $action = "<div class='text-center'>";
            $action .= "<a href=" . url("case/difteri/detail/" . $dt->id_trx_case) . " title='Detail data' data-toggle='tooltip' class='btn btn-sm btn-flat btn-success'><i class='fa fa-asterisk'></i></a>";
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'kabupaten'])) {
                $action .= "<a href=" . url("case/difteri/update/" . $dt->id_trx_case) . " title='Edit data' data-toggle='tooltip' class='btn btn-sm btn-flat btn-warning'><i class='fa fa-pencil'></i></a>";
            };
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'provinsi', 'pusat'])) {
                $action .= '<a action="' . url('case/difteri/delete/' . $dt->id_trx_case) . '" title="Delete data"  class="btn btn-sm btn-flat btn-danger delete" data-toggle="modal" data-target=".modaldelete"><i class="fa fa-remove"></i></a>';
            };
            if (in_array($roleUser->code_role, ['puskesmas']) and $dt->id_role == '1') {
                if ($dt->code_kecamatan_faskes != $dt->code_kecamatan_pasien) {
                    $action .= '<a action="' . url('case/difteri/move/' . $dt->id_trx_case) . '" title="Move data" data-toggle="modal" class="btn btn-sm btn-flat btn-primary move" data-target=".modalmove"><i class="fa fa-step-forward"></i></a>';
                }
            }
            $action .= '</div>';
            return $action;
        });
        return $q->make(true);
    }

    public function getEpid($param = [])
    {
        if (!empty($param)) {
            $request = $param;
        } else {
            $request = Request::json()->all();
        }
        $case = Helper::getCase(array('alias' => $this->index));
        $code = $case[0]->code;
        $id_kelurahan = $request['id_kelurahan'];
        $thn_case = $cthn = '';
        $q = DB::table('view_list_case');
        $q->where('id_case', 3);
        $q->select(DB::raw('MAX(no_epid) AS no_epid'));
        if ($request['tgl_sakit']) {
            $thn_case = \Carbon\Carbon::parse($request['tgl_sakit'])->format('Y');
            $cthn = \Carbon\Carbon::parse($request['tgl_sakit'])->format('y');
            $q->where('thn_sakit', $thn_case);
        }
        $q->where('code_kelurahan_pasien', $id_kelurahan);
        $cekEpid = $q->first();
        if (!empty($cekEpid->no_epid)) {
            $cc = substr($cekEpid->no_epid, -3);
            $no = str_pad($cc + 1, 3, 0, STR_PAD_LEFT);
        } else {
            $no = '001';
        }
        $no_epid = $code . $id_kelurahan . $cthn . $no;
        return Response::json(array(
            'response' => $no_epid,
        ));
    }

    public function postDelete($id = null)
    {
        $rId = Request::json()->all();
        if ($id != null) {
            $rId['query']['delete'][] = $id;
        }
        $response = [];
        foreach ($rId['query']['delete'] as $key => $val) {
            $response[$val] = Tx::deleteData('trx_case', ['id' => $val]);
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function viewDetail($id)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $response = $this->getDetail($id);
        $dt = json_decode($response->getContent());
        $data = $dt->response[0];
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Detail Pasien Kasus Penyakit ' . $case,
            'data' => array(
                'index' => $index,
                'response' => $data,
            ),
        );
        return view('case.' . $index . '.detail', $compact);
    }

    public function getDetail($id = '')
    {
        if ($id) {
            $request = [
                'where' => ['id' => [$id]],
                'query' => ['select' => ['*', 'dtSpesimen']],
            ];
        } else {
            $request = Request::json()->all();
        }
        $qQuery = [];
        if (isset($request['where'])) {
            foreach ($request['where'] as $key => $val) {
                if ($val[0] == '*') {
                    $qQuery['where'][$key] = '*';
                } else {
                    $qQuery['whereIn'][$key] = $val;
                }
            }
            if (isset($request['where']['code_roles'])) {
                if ($request['where']['code_roles'][0] == 'puskesmas') {
                    $c = [];
                    foreach ($request['where']['id_faskes'] as $key => $val) {
                        $codeKelurahanKerjaPkm = Tx::getData('view_wilayah_kerja_puskesmas', ['id_faskes' => $val], ['select' => ['code_kelurahan']])->get('code_kelurahan');
                        foreach ($codeKelurahanKerjaPkm as $k => $v) {
                            $c[] = $v->code_kelurahan;
                        }
                    }
                    $qQuery['orWhere']['whereIn'] = ['code_kelurahan_pasien' => $c, 'code_roles' => ['rs']];
                }
            }
        }
        if (isset($request['query'])) {
            foreach ($request['query'] as $key => $val) {
                foreach ($val as $v) {
                    if (!in_array($v, ["dtSpesimen"])) {
                        $var[] = $v;
                    }
                }
                $qQuery[$key] = $var;
            }
        }
        if (!empty($request['pagination'])) {
            $qQuery['pagination'] = $request['pagination'];
        }
        $resp = Tx::getData('difteri', [], $qQuery)->get();
        $dResp = $response = [];
        foreach ($resp as $key => $val) {
            $dt = (array) $val;
            if (isset($request['query'])) {
                foreach ($request['query'] as $k => $v) {
                    foreach ($v as $var) {
                        ($var == 'dtSpesimen') ? $dt['dtSpesimen'] = Tx::getData('view_spesimen_case_difteri', ['id_trx_case' => $val->id, 'id_trx_pe' => null], ['jenis_spesimen', 'jenis_spesimen_txt', 'tgl_ambil_spesimen', 'jenis_pemeriksaan', 'jenis_pemeriksaan_txt', 'kode_spesimen', 'hasil', 'hasil_txt'])->get() : null;
                    }
                }
            }
            $response[] = $dt;
        }

        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getDetailPe($id_pe = '')
    {
        if ($id_pe) {
            $request = [
                'where' => ['id' => [$id_pe]],
                'query' => ['select' => ['*', 'dtGejala', 'dtSpesimen', 'dtKontakKasus']],
            ];
        } else {
            $request = Request::json()->all();
        }
        $qQuery = [];
        if (isset($request['where'])) {
            foreach ($request['where'] as $key => $val) {
                if ($val[0] == '*') {
                    $qQuery['where'][$key] = '*';
                } else {
                    $qQuery['whereIn'][$key] = $val;
                }
            }
        }
        if (isset($request['query'])) {
            foreach ($request['query'] as $key => $val) {
                foreach ($val as $v) {
                    if (!in_array($v, ["dtGejala", "dtSpesimen", "dtKontakKasus"])) {
                        $var[] = $v;
                    }
                }
                $qQuery[$key] = $var;
            }
        }
        if (!empty($request['pagination'])) {
            $qQuery['pagination'] = $request['pagination'];
        }
        $resp = Tx::getData('view_pe_difteri', [], $qQuery)->get();
        $response = [];
        foreach ($resp as $key => $val) {
            $dt = (array) $val;
            if (isset($request['query'])) {
                foreach ($request['query'] as $k => $v) {
                    foreach ($v as $var) {
                        ($var == 'dtGejala') ? $dt['dtGejala'] = Tx::getData('view_gejala', ['id_trx_pe' => $val->id], ['select' => ['id_gejala', 'name', 'tgl_kejadian', 'other_gejala', 'val_gejala']])->get() : null;
                        ($var == 'dtSpesimen') ? $dt['dtSpesimen'] = Tx::getData('view_spesimen_case_difteri', ['id_trx_pe' => $val->id], ['select' => ['jenis_spesimen', 'jenis_spesimen_txt', 'tgl_ambil_spesimen', 'jenis_pemeriksaan', 'jenis_pemeriksaan_txt', 'kode_spesimen', 'hasil', 'hasil_txt']])->get() : null;
                        ($var == 'dtKontakKasus') ? $dt['dtKontakKasus'] = Tx::getData('view_kontak_kasus', ['id_trx_pe' => $val->id], ['select' => ['*']])->get() : null;
                    }
                }
            }
            $response[] = $dt;
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function viewUpdate($id)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Update Kasus Penyakit ' . $case,
            'data' => array(
                'id_trx_case' => $id,
            ),
        );
        return view('case.' . $index . '.update', $compact);
    }

    public function postCase($post = [])
    {
        if ($post) {
            $req = $post;
        } else {
            $req = Request::json()->all();
        }
        $dc = Helper::getCase(array('alias' => $this->index));
        $response = [];
        foreach ($req as $key => $request) {
            $vk = Validator::make($request['dk'], []);
            if ($vk->fails()) {
                $success = false;
                $response[] = array_merge($vk->errors()->all());
                $code = 400;
                Helper::session_flash('warning', 'Gagal', 'Data gagal disimpan');
            } else {
                $request['dp']['tgl_lahir'] = (isset($request['dp']['tgl_lahir'])) ? Helper::dateFormat($request['dp']['tgl_lahir']) : null;
                if ($id_pasien = $request['id_pasien']) {
                    if (isset($request['dp']) && isset($request['df'])) {
                        Tx::updateData('ref_pasien', ['id' => $id_pasien], $request['dp']);
                        Tx::updateData('ref_family', ['id_pasien' => $id_pasien], $request['df']);
                    }
                    $id_family = Tx::getData('ref_family', ['id_pasien' => $id_pasien])->first()->id;
                } else {
                    $id_pasien = Tx::insertData('ref_pasien', $request['dp']);
                    $request['df']['id_pasien'] = $id_pasien;
                    $id_family = Tx::insertData('ref_family', $request['df']);
                }
                $al = DB::table('ref_pasien AS a');
                $al->join('view_area AS b', 'a.code_kelurahan', '=', 'b.code_kelurahan');
                $al->select('b.code_kecamatan', 'b.code_kelurahan', 'b.code_kabupaten');
                $al->where('a.id', $id_pasien);
                $alamat_pasien = $al->first();
                $code_kabupaten_pasien = empty($alamat_pasien->code_kabupaten) ?: $alamat_pasien->code_kabupaten;
                $code_kecamatan_pasien = empty($alamat_pasien->code_kecamatan) ?: $alamat_pasien->code_kecamatan;
                $code_kelurahan_pasien = empty($alamat_pasien->code_kelurahan) ?: $alamat_pasien->code_kelurahan;
                // tx case
                $tgl_sakit = $request['dk']['tgl_mulai_demam'];
                $epid = $this->getEpid(['tgl_sakit' => $tgl_sakit, 'id_kelurahan' => $code_kelurahan_pasien]);
                $no_epid = json_decode($epid->getContent())->response;
                $request['dc']['id_case'] = $dc[0]->id;
                $request['dc']['id_pasien'] = $id_pasien;
                if (empty($request['id_trx_case'])) {
                    $request['dc']['no_epid'] = $no_epid;
                }
                $request['dc']['klasifikasi_final'] = !empty($request['dc']['klasifikasi_final']) ? $request['dc']['klasifikasi_final'] : null;
                (isset($request['dc']['created_at'])) ? $request['dc']['created_at'] = date('Y-m-d H:i:s', strtotime($request['dc']['created_at'])) : null;
                (isset($request['dc']['updated_at'])) ? $request['dc']['updated_at'] = date('Y-m-d H:i:s', strtotime($request['dc']['updated_at'])) : null;
                if ($id_trx_case = $request['id_trx_case']) {
                    Tx::updateData('trx_case', ['id' => $id_trx_case], $request['dc']);
                } else {
                    $id_trx_case = Tx::insertData('trx_case', $request['dc']);
                }
                // data klinis
                $request['dk']['id_trx_case'] = $id_trx_case;
                $request['dk']['tgl_mulai_demam'] = $request['dk']['tgl_mulai_sakit'] = (isset($request['dk']['tgl_mulai_demam'])) ? Helper::dateFormat($request['dk']['tgl_mulai_demam']) : null;
                $request['dk']['tgl_imunisasi_difteri'] = (isset($request['dk']['tgl_imunisasi_difteri'])) ? Helper::dateFormat($request['dk']['tgl_imunisasi_difteri']) : null;
                $request['dk']['tgl_pelacakan'] = (isset($request['dk']['tgl_pelacakan'])) ? Helper::dateFormat($request['dk']['tgl_pelacakan']) : null;
                if ($request['id_trx_case']) {
                    Tx::updateData('trx_klinis', ['id_trx_case' => $id_trx_case], $request['dk']);
                } else {
                    $id_klinis = Tx::insertData('trx_klinis', $request['dk']);
                }
                // data spesimen
                Tx::postDelete('trx_spesimen', ['id_trx_case' => $id_trx_case]);
                if (!empty($request['ds']['spesimen'][0])) {
                    $dtSpesimen = $request['ds'];
                    $rSpesimen = [];
                    foreach ($dtSpesimen['spesimen'] as $key => $val) {
                        $d = explode('|', $val);
                        $rSpesimen[] = array(
                            'id_trx_case' => $id_trx_case,
                            'jenis_spesimen' => $d[0],
                            'tgl_ambil_spesimen' => Helper::dateFormat($d[1]),
                            'jenis_pemeriksaan' => $d[2],
                            'kode_spesimen' => $d[3],
                            'hasil' => $d[4],
                            'case' => 'difteri',
                        );
                    }
                    Tx::insertArray('trx_spesimen', $rSpesimen);
                }
                // data notification
                Tx::postDelete('trx_notification', ['id_trx_case' => $id_trx_case]);
                $code_wilayah_faskes = $request['code_wilayah_faskes'];
                $dtNotification[] = array(
                    'type' => 'klb',
                    'id_trx_case' => $id_trx_case,
                    'from_faskes' => $code_wilayah_faskes,
                    'to_faskes' => $code_kecamatan_pasien,
                );
                if ($request['dc']['id_role'] == '1') {
                    // notif puskesmas
                    if ($code_kecamatan_pasien != $code_wilayah_faskes) {
                        $dtNotification[] = array(
                            'type' => 'cross',
                            'id_trx_case' => $id_trx_case,
                            'from_faskes' => $code_wilayah_faskes,
                            'to_faskes' => $code_kecamatan_pasien,
                        );
                    }
                } else if ($request['dc']['id_role'] == '2') {
                    // notif rumahsakit
                    if ($code_kabupaten_pasien != $code_wilayah_faskes) {
                        $dtNotification[] = array(
                            'type' => 'cross',
                            'id_trx_case' => $id_trx_case,
                            'from_faskes' => $code_wilayah_faskes,
                            'to_faskes' => $code_kabupaten_pasien,
                        );
                    }
                }
                Tx::insertArray('trx_notification', $dtNotification);
                $success = true;
                $code = 200;
                $trx_case = Tx::getData('trx_case', ['id' => $id_trx_case], ['select' => ['created_at', 'updated_at']])->first();
                $response[] = [
                    'id' => $id_trx_case,
                    'created_at' => date('d-m-Y H:i:s', strtotime($trx_case->created_at)),
                    'updated_at' => date('d-m-Y H:i:s', strtotime($trx_case->updated_at)),
                    'no_epid' => $no_epid,
                ];
                Helper::session_flash('success', 'Berhasil', 'Data berhasil disimpan');
            }
        }
        return Response::json(array(
            'success' => $success,
            'response' => $response,
        ), $code);
    }

    public function viewPe($id_trx_case)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Input data penyelidikan epidemologi kasus ' . $case,
            'data' => array(
                'id_trx_case' => $id_trx_case,
                'id_trx_pe' => null,
            ),
        );
        return view('case.' . $index . '.pe.index', $compact);
    }

    public function postPe($post = '')
    {
        if ($post) {
            $req = $post;
        } else {
            $req = Request::json()->all();
        }
        $response = [];
        foreach ($req as $kpe => $request) {
            $id_trx_case = $request['id_trx_case'];
            $id_trx_klinis = Tx::getData('trx_klinis', ['id_trx_case' => $id_trx_case], ['select' => ['id']])->first()->id;
            $id_trx_pe = $request['id_trx_pe'];
            if (empty($id_trx_case)) {
                $success = false;
                $response[] = ["error" => "id_trx_case is empty"];
                $code = 400;
                Helper::session_flash('warning', 'Gagal', 'Data gagal di simpan');
            } else {
                $request['dpe']['id_trx_case'] = $id_trx_case;
                $request['dpe']['tgl_penyelidikan'] = (isset($request['dpel']['tgl_penyelidikan'])) ? Helper::dateFormat($request['dpel']['tgl_penyelidikan']) : date('Y-m-d');
                (isset($request['dpe']['created_at'])) ? $request['dpe']['created_at'] = date('Y-m-d H:i:s', strtotime($request['dpe']['created_at'])) : null;
                (isset($request['dpe']['updated_at'])) ? $request['dpe']['updated_at'] = date('Y-m-d H:i:s', strtotime($request['dpe']['updated_at'])) : null;
                if ($id_trx_pe = $request['id_trx_pe']) {
                    Tx::updateData('trx_pe', ['id' => $id_trx_pe], $request['dpe']);
                } else {
                    $id_trx_pe = Tx::insertData('trx_pe', $request['dpe']);
                }
                // pelapor
                $request['dpel']['id_trx_case'] = $id_trx_case;
                $request['dpel']['tgl_laporan'] = (isset($request['dpel']['tgl_laporan'])) ? Helper::dateFormat($request['dpel']['tgl_laporan']) : null;
                if ($request['id_trx_pe']) {
                    Tx::updateData('trx_pelapor', ['id_trx_case' => $id_trx_case], $request['dpel']);
                } else {
                    Tx::insertData('trx_pelapor', $request['dpel']);
                }
                // pasien + ortu
                $request['dp']['tgl_lahir'] = (isset($request['dp']['tgl_lahir'])) ? Helper::dateFormat($request['dp']['tgl_lahir']) : null;
                if ($id_pasien = $request['id_pasien']) {
                    if (isset($request['dp']) && isset($request['df'])) {
                        Tx::updateData('ref_pasien', ['id' => $id_pasien], $request['dp']);
                        Tx::updateData('ref_family', ['id_pasien' => $id_pasien], $request['df']);
                    }
                    $id_family = Tx::getData('ref_family', ['id_pasien' => $id_pasien])->first()->id;
                } else {
                    $id_pasien = Tx::insertData('ref_pasien', $request['dp']);
                    $request['df']['id_pasien'] = $id_pasien;
                    $id_family = Tx::insertData('ref_family', $request['df']);
                }
                // klinis
                $request['drs']['id_trx_case'] = $id_trx_case;
                $request['drs']['tgl_mulai_sakit'] = (isset($request['drs']['tgl_mulai_sakit'])) ? Helper::dateFormat($request['drs']['tgl_mulai_sakit']) : null;
                if ($id_trx_case) {
                    Tx::updateData('trx_klinis', ['id_trx_case' => $id_trx_case], $request['drs']);
                } else {
                    Tx::insertData('trx_klinis', $request['drs']);
                }
                // gejala
                if (isset($request['dg']['gejala'])) {
                    Tx::postDelete('trx_gejala', ['id_trx_klinis' => $id_trx_klinis, 'id_trx_pe' => $id_trx_pe]);
                    $dg = $request['dg'];
                    $rowdg = [];
                    foreach ($dg['gejala'] as $k => $v) {
                        if (!empty($v)) {
                            $rowdg[] = array(
                                'id_trx_klinis' => $id_trx_klinis,
                                'id_trx_pe' => $id_trx_pe,
                                'id_gejala' => $v,
                                'tgl_kejadian' => (isset($dg['tgl_gejala'][$k])) ? Helper::dateFormat($dg['tgl_gejala'][$k]) : null,
                            );
                        }
                    }
                    if (!empty($dg['other'])) {
                        foreach ($dg['other'] as $ko => $vo) {
                            if (!empty($vo)) {
                                $rowdg[] = array(
                                    'id_trx_klinis' => $id_trx_klinis,
                                    'id_trx_pe' => $id_trx_pe,
                                    'other_gejala' => $vo,
                                    'val_gejala' => $dg['other_val'][$ko],
                                );
                            }
                        }
                    }
                    Tx::insertArray('trx_gejala', $rowdg);
                }
                // spesimen
                if (isset($request['ds']['spesimen'])) {
                    Tx::postDelete('trx_spesimen', ['id_trx_case' => $id_trx_case, 'id_trx_pe' => $id_trx_pe]);
                    $dtSpesimen = $request['ds'];
                    $rSpesimen = [];
                    foreach ($dtSpesimen['spesimen'] as $k => $v) {
                        $d = explode('|', $v);
                        $tgl_ambil_spesimen = (isset($d[1])) ? Helper::dateFormat($d[1]) : null;
                        $rSpesimen[] = array(
                            'id_trx_case' => $id_trx_case,
                            'id_trx_pe' => $id_trx_pe,
                            'jenis_spesimen' => $d[0],
                            'tgl_ambil_spesimen' => $tgl_ambil_spesimen,
                            'jenis_pemeriksaan' => $d[2],
                            'kode_spesimen' => $d[3],
                            'hasil' => $d[4],
                            'case' => 'difteri',
                        );
                    }
                    Tx::insertArray('trx_spesimen', $rSpesimen);
                }
                // riwayat pengobatan
                $request['drp']['id_trx_pe'] = $id_trx_pe;
                if ($request['id_trx_pe']) {
                    Tx::updateData('trx_pe_riwayat_pengobatan', ['id_trx_pe' => $id_trx_pe], $request['drp']);
                } else {
                    Tx::insertData('trx_pe_riwayat_pengobatan', $request['drp']);
                }
                // riwayat kontak
                $request['drk']['id_trx_pe'] = $id_trx_pe;
                if ($request['id_trx_pe']) {
                    Tx::updateData('trx_pe_riwayat_kontak', ['id_trx_pe' => $id_trx_pe], $request['drk']);
                } else {
                    Tx::insertData('trx_pe_riwayat_kontak', $request['drk']);
                }
                // kontak kasus
                if (isset($request['dkk']['kontak_kasus'])) {
                    Tx::postDelete('trx_pe_kontak_kasus', ['id_trx_pe' => $id_trx_pe]);
                    $dtKontakKasus = $request['dkk'];
                    $rKontakKasus = [];
                    foreach ($dtKontakKasus['kontak_kasus'] as $k => $v) {
                        $d = explode('|', $v);
                        $tgl_ambil_tenggorokan = (isset($d[4])) ? Helper::dateFormat($d[4]) : null;
                        $tgl_ambil_hidung = (isset($d[6])) ? Helper::dateFormat($d[6]) : null;
                        $rKontakKasus[] = array(
                            'id_trx_pe' => $id_trx_pe,
                            'nama' => $d[0],
                            'umur' => $d[1],
                            'hub_dg_kasus' => $d[2],
                            'status_imunisasi' => $d[3],
                            'tgl_ambil_tenggorokan' => $tgl_ambil_tenggorokan,
                            'hasil_tenggorokan' => $d[5],
                            'tgl_ambil_hidung' => $tgl_ambil_hidung,
                            'hasil_hidung' => $d[7],
                            'profilaksis' => $d[8],
                        );
                    }
                    Tx::insertArray('trx_pe_kontak_kasus', $rKontakKasus);
                }
                $success = true;
                $code = 200;
                $trx_pe = Tx::getData('trx_pe', ['id' => $id_trx_pe], ['select' => ['created_at', 'updated_at']])->first();
                $response[] = [
                    'id' => $id_trx_pe,
                    'created_at' => date('d-m-Y H:i:s', strtotime($trx_pe->created_at)),
                    'updated_at' => date('d-m-Y H:i:s', strtotime($trx_pe->updated_at)),
                ];
                Helper::session_flash('success', 'Berhasil', 'Data berhasil disimpan');
            }
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function getDataPeDifteri()
    {
        $request = Request::all();
        $param['roleUser'] = Helper::role();
        $param['query']['where'] = ['id_case' => 3];
        $d = Sql::getData("view_list_pe", $param);
        $q = Datatables::of($d);
        if ($keyword = $request['search']['value'] or $request['search']['value'] == 0) {
            foreach ($request['columns'] as $key => $val) {
                if (!empty($val['name'])) {
                    $q->filterColumn($val['name'], 'whereRaw', $val['name'] . ' like ?', ["%{$keyword}%"]);
                }
            }
        }
        $q->addIndexColumn();
        $q->editColumn('name_pasien', function ($dt) {
            return ucwords($dt->name_pasien);
        });
        $q->editColumn('nama_ortu', function ($dt) {
            return ucwords($dt->nama_ortu);
        });
        $q->editColumn('no_epid', function ($dt) {
            $no_epid = $dt->no_epid;
            if ($dt->jenis_input == '1') {
                $no_epid .= '<br><span class="label label-info">Web</span>';
            } else if ($dt->jenis_input == '2') {
                $no_epid .= '<br><span class="label label-success">Android</span>';
            } else if ($dt->jenis_input == '3') {
                $no_epid .= '<br><span class="label label-warning">Import</span>';
            }
            return $no_epid;
        });
        $q->addColumn('action', function ($dt) {
            $roleUser = Helper::role();
            $action = "<div class='text-center'>";
            $action .= "<a href=" . url("case/difteri/pe/detail/" . $dt->id_pe) . " title='Detail data' data-toggle='tooltip' class='btn btn-sm btn-flat btn-success'><i class='fa fa-asterisk'></i></a>";
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'kabupaten'])) {
                $action .= "<a href=" . url("case/difteri/pe/update/" . $dt->id_pe) . " title='Edit data' data-toggle='tooltip' class='btn btn-sm btn-flat btn-warning'><i class='fa fa-pencil'></i></a>";
            };
            if (in_array($roleUser->code_role, ['puskesmas', 'rs', 'provinsi', 'pusat'])) {
                $action .= '<a action="' . url('case/difteri/pe/delete/' . $dt->id_pe) . '" title="Delete data"  class="btn btn-sm btn-flat btn-danger delete" data-toggle="modal" data-target=".modaldelete"><i class="fa fa-remove"></i></a>';
            };
            $action .= '</div>';
            return $action;
        });
        return $q->make(true);
    }

    public function viewDetailPe($id_trx_pe)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $response = $this->getDetailPe($id_trx_pe);
        $dt = json_decode($response->getContent());
        $data = (array) $dt->response[0];

        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Detail Pasien Kasus Penyakit ' . $case,
            'data' => array(
                'index' => $index,
                'response' => $data,
            ),
        );
        return view('case.' . $index . '.pe.detail', $compact);
    }

    public function postDeletePe($id = '')
    {
        $rId = Request::json()->all();
        if ($id != null) {
            $rId['query']['delete'][] = $id;
        }
        $response = [];
        foreach ($rId['query']['delete'] as $key => $val) {
            $response[$val] = Tx::deleteData('trx_pe', ['id' => $val]);
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }

    public function viewUpdatePe($id_pe)
    {
        $index = $this->index;
        $dc = Helper::getCase(array('alias' => $index));
        $case = $dc[0]->name;
        $cc = Tx::getData('difteri', ['id_pe' => $id_pe], ['select' => ['id']])->first();
        $compact = array(
            'title' => 'Surveilans PD3I',
            'contentTitle' => 'Update data penyelidikan epidemologi kasus ' . $case,
            'data' => array(
                'id_trx_pe' => $id_pe,
                'id_trx_case' => $cc->id,
            ),
        );
        return view('case.' . $index . '.pe.index', $compact);
    }

    public function getAnalisa($id = '')
    {
        $request = Request::json()->all();
        $index = $this->index;
        if (!empty($request['range'])) {
            $dt = $request['from'];
            if (empty($dt['month'])) {
                $dt['month'] = 0;
            }
            if (empty($dt['day'])) {
                $dt['day'] = 0;
            }
            $rangeFrom = \Carbon\Carbon::create($dt['year'], $dt['month'], $dt['day']);
            $dt = $request['to'];
            if (empty($dt['month'])) {
                $dt['month'] = 0;
            }
            if (empty($dt['day'])) {
                $dt['day'] = 0;
            }
            $rangeTo = \Carbon\Carbon::create($dt['year'], $dt['month'], $dt['day']);
            $range = [$rangeFrom, $rangeTo];
            $data['lol'] = $range;
            // $dt = TxCase::getDataAnalisa($request['filter'],$index);
            $dt = TxCase::getDataAnalisaTime($request['filter'], $index, 'tgl_sakit', $range);
        } else {
            $dt = TxCase::getDataAnalisa($request['filter'], $index);
        }
        if (!empty($dt)) {
            // ---getdata for jenis kelamin graph---
            $kelamin = collect($dt)->groupBy('jenis_kelamin');
            foreach ($kelamin as $key => $val) {
                if ($key == 'L') {
                    $data['jenis_kelamin'][] = ['Laki-laki', count($val)];
                } else if ($key == "P") {
                    $data['jenis_kelamin'][] = ['Perempuan', count($val)];
                } else {
                    $data['jenis_kelamin'][] = ['Tidak Jelas', count($val)];
                }
            }

            // ---getdata for waktu graph---
            $waktu = collect($dt)->groupBy(function ($val) {
                return \Carbon\Carbon::parse($val->tgl_sakit)->format('Y'); // grouping by months
            });

            foreach ($waktu as $keys => $value) {
                $tahun['name'] = 'Tahun ' . $keys;
                $val1 = collect($value)->groupBy(function ($val) {
                    return Carbon::parse($val->tgl_sakit)->format('m'); // grouping by months
                });
                foreach ($val1 as $k => $v) {
                    $tmp1[intval($k)] = $v; // change month value to int for readable later
                }
                $gettahun = Carbon::parse($value[0]->tgl_sakit)->format('Y'); //get the year
                for ($i = 01; $i <= 12; $i++) {
                    $bulan['name'] = Carbon::createFromFormat('!m', $i)->format('M');
                    $bulan['drilldown'] = $bulan['name'] . $gettahun;
                    $daysinmonth = intval(cal_days_in_month(CAL_GREGORIAN, $i, $gettahun)); //get days in selected month
                    if (!empty($tmp1[$i])) {
                        $bulan['y'] = count($tmp1[$i]);
                        $tanggal['id'] = $bulan['drilldown'];
                        $tanggal['name'] = $gettahun;
                        // $tanggal['name']=Carbon::createFromFormat('!m', $i)->format('F').' '.$gettahun;
                        $val2 = collect($tmp1[$i])->groupBy(function ($val) {
                            return Carbon::parse($val->tgl_sakit)->format('d'); // grouping by months
                        });
                        $wew[] = $val2;
                        foreach ($val2 as $k => $v) {
                            $tmp2[intval($k)] = $v; // change days value to int for readable later
                        }

                        for ($j = 1; $j <= $daysinmonth; $j++) {
                            if (!empty($tmp2[$j])) {
                                $tanggal['data'][] = [$j . ' ' . $bulan['name'], count($tmp2[$j])];
                            } else {
                                $tanggal['data'][] = [$j . ' ' . $bulan['name'], 0];
                            }
                        }
                        $tmp2 = [];
                    } else {
                        $bulan['y'] = 0;
                        $tanggal['id'] = $bulan['drilldown'];
                        $tanggal['name'] = Carbon::createFromFormat('!m', $i)->format('F') . ' ' . $gettahun;
                        for ($j = 1; $j <= $daysinmonth; $j++) {
                            $tanggal['data'][] = [$j . ' ' . $bulan['name'], 0];
                        }
                    }
                    $drilldown[] = $tanggal;
                    $tahun['data'][] = $bulan;
                    $bulan = []; //flush array
                    $tanggal = []; //flush array

                }
                $datawaktu[] = $tahun;
                $tahun = [];
                $tmp1 = [];
            }
            $data['waktu']['data'] = $datawaktu;
            $data['waktu']['drilldown'] = $drilldown;

            // ---getdata for umur graph---
            $umur = collect($dt)->groupBy('umur_thn')->sortBy('umur_thn');
            $tempUmur['name'] = 'Umur';
            $tempUmur['colorByPoint'] = false;
            foreach ($umur as $key => $val) {
                if ($key < 1) {
                    $tmpe[0][] = $val;
                } else if ($key >= 1 && $key < 5) {
                    $tmpe[1][] = $val;
                } else if ($key >= 5 && $key < 10) {
                    $tmpe[2][] = $val;
                } else if ($key >= 10 && $key < 15) {
                    $tmpe[3][] = $val;
                } else if ($key >= 15) {
                    $tmpe[4][] = $val;
                }
            }
            for ($i = 0; $i < 5; $i++) {
                if (!empty($tmpe[$i])) {
                    $tempUmur['data'][$i] = count($tmpe[$i][0]);
                } else {
                    $tempUmur['data'][$i] = 0;
                }
            }
            $data['umur'][] = $tempUmur;

            // ---getdata for status imun graph---
            $statimun = collect($dt)->groupBy('jml_imunisasi_dpt');
            foreach ($statimun as $key => $val) {
                if ($key == "") {
                    $data['stat_imun'][] = ['Tidak diisi', count($val)];
                } else if ($key == "7") {
                    $data['stat_imun'][] = ['Tidak', count($val)];
                } else if ($key == "8") {
                    $data['stat_imun'][] = ['Tidak tahu', count($val)];
                } else {
                    $data['stat_imun'][] = [$key . ' kali', count($val)];
                }
            }

            // ---getdata for klasifikasi_final graph---
            $klafinal = collect($dt)->groupBy('klasifikasi_final');
            foreach ($klafinal as $key => $val) {
                if ($key == "") {
                    $data['klasifikasi_final'][] = ['Belum di isi', count($val)];
                } else if ($key == "1") {
                    $data['klasifikasi_final'][] = ['Probable', count($val)];
                } else if ($key == "2") {
                    $data['klasifikasi_final'][] = ['Konfirm', count($val)];
                } else if ($key == "3") {
                    $data['klasifikasi_final'][] = ['Negatif', count($val)];
                }
            }
        } else {
            $temp['data'][] = array(
                'name' => 'Tahun ' . Carbon::now()->year,
                'data' => [['Jan', 0], ['Feb', 0], ['Mar', 0], ['Apr', 0], ['May', 0], ['Jun', 0], ['Jul', 0], ['Aug', 0], ['Sep', 0], ['Oct', 0], ['Dec', 0]]
            );
            $data['waktu'] = $temp;
            $data['jenis_kelamin'][] = ['Tidak Ada Data', 0];
            $data['stat_imun'][] = ['Tidak Ada Data', 0];
            $data['klasifikasi_final'][] = ['Tidak Ada Data', 0];
            $data['umur'][] = array(
                'name' => 'Umur',
                'data' => [0, 0, 0, 0, 0],
            );
        }
        $response = $data;
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }
}

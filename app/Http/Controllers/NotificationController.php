<?php

namespace App\Http\Controllers;

use App\Models\Tx;
use Request;
use Response;

class NotificationController extends Controller
{
    public function getNotif()
    {
        $request = Request::json()->all();
        $response = [];
        if (isset($request['where'])) {
            if ($request['where']['type_notification'][0] == 'rs') {
                if ($request['where']['code_role'] == 'puskesmas' or $request['where']['code_role'] == 'rs') {
                    $wRs = ['code_roles' => 'rs', 'code_kecamatan_pasien' => $request['where']['id_faskes']];
                } elseif ($request['where']['code_role'] == 'kabupaten') {
                    $wRs = ['code_roles' => 'rs', 'code_kabupaten_pasien' => $request['where']['id_faskes']];
                } elseif ($request['where']['code_role'] == 'provinsi') {
                    $wRs = ['code_roles' => 'rs', 'code_provinsi_pasien' => $request['where']['id_faskes']];
                }
                $rs = Tx::getData('view_faskes_pasien', $wRs)->get();
                if ($rs) {
                    foreach ($rs as $key => $val) {
                    	$val = (array) $val;
                        $response[] = [
                            "id_trx_notification"=>"",
                            "type_notification"=>"rs",
                            "id_trx_case"=>$val['id_trx_case'],
                            "name_case"=>$val['name_case'],
                            "alias_case"=>$val['alias_case'],
                            "from_faskes"=>$val['code_faskes'],
                            "to_faskes"=>$val['code_faskes'],
                            "district_pasien"=>$val['name_kecamatan_pasien'],
                            "faskes"=>$val['name_faskes'],
                            "id_faskes"=>$val['id_faskes'],
                            "date_submit"=>date('d-m-Y H:i:s', strtotime($val['date_submit'])),
                            "code_roles"=>$val['code_roles'],
                            "id_pasien"=>$val['id_pasien'],
                            "to_faskes_kabupaten"=>$val['code_kabupaten_faskes'],
                            "to_faskes_provinsi"=>$val['code_provinsi_faskes'],
                        ];
                    }
                }
            } else {
                foreach ($request['where'] as $key => $val) {
                    if ($val[0] == '*') {
                        $qQuery['where'][$key] = '*';
                    } else {
                        $qQuery['whereIn'][$key] = $val;
                    }
                }
                $response = Tx::getData('view_notification', [], $qQuery)->get();
            }
        }
        return Response::json(array(
            'success' => true,
            'response' => $response,
        ), 200);
    }
}

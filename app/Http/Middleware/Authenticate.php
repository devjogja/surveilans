<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Sentinel;
use Session;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    /*public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        return $next($request);
    }*/

    public function handle($request, Closure $next)
    {
        $uri = strtolower($request->segment('1'));
        $uri_exception = array('login','register', 'reset_pass', 'reset_password');
        $validate = !in_array($uri, $uri_exception);
        if (!$request->ajax && $validate) {
            if (Sentinel::check()) {
                if ($uri=='' || $uri=='register') {
                    return redirect('viewProfile');
                }
            } else {
                if($uri != ''){
                    $message = array(
                        'status'    => 'warning',
                        'title'     => 'Login',
                        'message'   => 'Youre session expired. Please login first!'
                        );
                    Session::flash('flash_message',$message);
                    return redirect('/');
                }
            }
        }
        return $next($request);
    }
}

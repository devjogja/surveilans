<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
 */
Route::get('sync_all', 'SyncController@sync_all');
Route::get('sync_master_rs', 'SyncController@sync_master_rs');
Route::get('sync_master_puskesmas', 'SyncController@sync_master_puskesmas');
Route::get('sync_wilayah_kerja_puskesmas', 'SyncController@sync_wilayah_kerja_puskesmas');
Route::get('sync_user', 'SyncController@sync_user');
Route::get('sync_role_user/{faskes}', 'SyncController@sync_role_user');
Route::get('sync_campak', 'SyncController@sync_campak');
Route::get('sync_epid_campak', 'SyncController@sync_epid_campak');
Route::get('sync_afp', 'SyncController@sync_afp');
Route::get('sync_epid_afp', 'SyncController@sync_epid_afp');
Route::get('sync_difteri', 'SyncController@sync_difteri');
Route::get('sync_epid_difteri', 'SyncController@sync_epid_difteri');
Route::get('sync_tetanus', 'SyncController@sync_tetanus');
Route::get('sync_epid_tetanus', 'SyncController@sync_epid_tetanus');
Route::get('sync_crs', 'SyncController@sync_crs');
Route::get('sync_epid_crs', 'SyncController@sync_epid_crs');

Route::get('contoh', 'DashboardController@dd');


Route::get('drop', 'HelpController@dropTransaksi');
Route::group(['prefix' => 'api'], function () {
    Route::group(['as' => 'authenticate'], function () {
        Route::post('postLogin', 'AuthenticateController@apiLogin');
    });
    Route::group(['prefix' => 'user'], function () {
        Route::post('register', 'AuthenticateController@postRegister');
    });

    Route::group(['prefix' => 'analisa'], function () {
        Route::post('graphJenisKelamin/{case}', 'AnalisaController@getGraphJenisKelamin');
        Route::post('graphWaktu/{case}', 'AnalisaController@getGraphWaktu');
        Route::post('graphUmur/{case}', 'AnalisaController@getGraphUmur');
        Route::post('graphStatusImunisasi/{case}', 'AnalisaController@getGraphStatusImunisasi');
        Route::post('graphKlasifikasiFinal/{case}', 'AnalisaController@getGraphKlasifikasiFinal');
        Route::post('graphWilayah/{case}', 'AnalisaController@getGraphWilayah');

        Route::post('campak', 'CampakController@getAnalisa');
        Route::post('afp', 'AfpController@getAnalisa');
        Route::post('difteri', 'DifteriController@getAnalisa');
        Route::post('crs', 'CrsController@getAnalisa');
        Route::post('tetanus', 'TetanusController@getAnalisa');
        Route::post('wilayah', 'DashboardController@getAnalisaWilayah');
    });
    Route::get('drop', 'HelpController@dropTransaksi');
    Route::get('konfirmCode', 'HelpController@konfirmCode');
});
Route::group(['prefix' => 'faq'], function () {
    Route::get('/', 'HelpController@index');
});
Route::group(['group' => 'puskesmas'], function () {
    Route::group(['prefix' => 'puskesmas'], function () {
        Route::post('list', 'PuskesmasController@getPuskesmas');
        Route::post('wilayahKerjaPuskesmas', 'PuskesmasController@wilayahKerjaPuskesmas');
        Route::get('detail/{id}', 'PuskesmasController@getDetail');
    });
});
Route::group(['group' => 'wilayah'], function () {
    Route::get('kabupaten/detail/{id}', 'WilayahController@getDetailKabupaten');
    Route::get('provinsi/detail/{id}', 'WilayahController@getDetailProvinsi');
    Route::group(['prefix' => 'wilayah'], function () {
        Route::post('kabupaten', 'WilayahController@getKabupaten');
        Route::post('kecamatan', 'WilayahController@getKecamatan');
        Route::post('kelurahan', 'WilayahController@getKelurahan');
    });
});
Route::group(['group' => 'rs'], function () {
    Route::group(['prefix' => 'rs'], function () {
        Route::post('list', 'RumahsakitController@getRS');
        Route::get('detail/{id}', 'RumahsakitController@getDetail');
    });
});
Route::group(['prefix' => 'api', 'middleware' => 'jwt.auth'], function () {
    Route::post('move_case', 'DashboardController@moveCase');

    Route::group(['as' => 'authenticate'], function () {
        Route::get('getLogout', 'AuthenticateController@apiLogout');
    });
    Route::post('notification', 'NotificationController@getNotif');
    Route::group(['prefix' => 'user'], function () {
        Route::get('getData', 'AuthenticateController@getUser');
        Route::get('getProfileUser', 'AuthenticateController@getProfileUser');
        Route::get('getRoleUser', 'AuthenticateController@getRoleUser');
        Route::post('getFaskesUser', 'AuthenticateController@getFaskesUser');
    });
    Route::group(['prefix' => 'pasien'], function () {
        Route::post('getData', 'PasienController@getDataPasien');
        Route::post('store', 'PasienController@postPasien');
        Route::post('postDelete', 'PasienController@postDeletePasien');
    });
    Route::group(['prefix' => 'family'], function () {
        Route::post('getData', 'PasienController@getDataFamily');
        Route::post('store', 'PasienController@postFamily');
        Route::post('postDelete', 'PasienController@postDeleteFamily');
    });
    Route::group(['prefix' => 'case'], function () {
        Route::group(['prefix' => 'campak'], function () {
            Route::post('getData', 'CampakController@getDetail');
            Route::post('store', 'CampakController@postCase');
            Route::post('getEpid', 'CampakController@getEpid');
            Route::post('getEpidKlb', 'CampakController@getEpidKlb');
            Route::post('postDelete', 'CampakController@postDelete');
            Route::group(['prefix' => 'pe'], function () {
                Route::post('getData', 'CampakController@getDetailPe');
                Route::post('store', 'CampakController@postPe');
                Route::post('postDelete', 'CampakController@postDeletePe');
            });
        });
        Route::group(['prefix' => 'afp'], function () {
            Route::post('getData', 'AfpController@getDetail');
            Route::post('store', 'AfpController@postCase');
            Route::post('getEpid', 'AfpController@getEpid');
            Route::post('postDelete', 'AfpController@postDelete');
            Route::group(['prefix' => 'pe'], function () {
                Route::post('getData', 'AfpController@getDetailPe');
                Route::post('store', 'AfpController@postPe');
                Route::post('postDelete', 'AfpController@postDeletePe');
            });
            Route::group(['prefix' => 'hkf'], function () {
                Route::post('getData', 'AfpController@getDetailHkf');
                Route::post('store', 'AfpController@postHkf');
                Route::post('postDelete', 'AfpController@postDeleteHkf');
            });
            Route::group(['prefix' => 'ku60'], function () {
                Route::post('getData', 'AfpController@getDetailKu60');
                Route::post('store', 'AfpController@postKu60');
                Route::post('postDelete', 'AfpController@postDeleteKu60');
            });
        });
        Route::group(['prefix' => 'difteri'], function () {
            Route::post('getData', 'DifteriController@getDetail');
            Route::post('store', 'DifteriController@postCase');
            Route::post('getEpid', 'DifteriController@getEpid');
            Route::post('postDelete', 'DifteriController@postDelete');
            Route::group(['prefix' => 'pe'], function () {
                Route::post('getData', 'DifteriController@getDetailPe');
                Route::post('store', 'DifteriController@postPe');
                Route::post('postDelete', 'DifteriController@postDeletePe');
            });
        });
        Route::group(['prefix' => 'tetanus'], function () {
            Route::post('getData', 'TetanusController@getDetail');
            Route::post('store', 'TetanusController@postCase');
            Route::post('getEpid', 'TetanusController@getEpid');
            Route::post('postDelete', 'TetanusController@postDelete');
            Route::group(['prefix' => 'pe'], function () {
                Route::post('getData', 'TetanusController@getDetailPe');
                Route::post('store', 'TetanusController@postPe');
                Route::post('postDelete', 'TetanusController@postDeletePe');
            });
        });
        Route::group(['prefix' => 'crs'], function () {
            Route::post('getData', 'CrsController@getDetail');
            Route::post('store', 'CrsController@postCase');
            Route::post('getEpid', 'CrsController@getEpid');
            Route::post('postDelete', 'CrsController@postDelete');
            Route::group(['prefix' => 'pe'], function () {
                Route::post('getData', 'CrsController@getDetailPe');
                Route::post('store', 'CrsController@postPe');
                Route::post('postDelete', 'CrsController@postDeletePe');
            });
        });
    });
    Route::group(['prefix' => 'role'], function () {
        Route::get('getData', 'RoleController@getData');
    });
});

Route::group(['prefix' => 'case'], function () {
    Route::get('campak/analisa', 'CampakController@viewAnalisa');
    Route::get('crs/analisa', 'CrsController@viewAnalisa');
    Route::get('afp/analisa', 'AfpController@viewAnalisa');
    Route::get('difteri/analisa', 'DifteriController@viewAnalisa');
    Route::get('tetanus/analisa', 'TetanusController@viewAnalisa');
});

Route::group(['middleware' => ['web', 'middleware' => 'auth']], function () {
    Route::group(['prefix' => 'kode_konfirm'], function () {
        Route::get('puskesmas/{type}', 'CodeKonfirmController@konfirmpuskesmas');
        Route::get('rs/{type}', 'CodeKonfirmController@konfirmrs');
        Route::get('kabupaten/{type}', 'CodeKonfirmController@konfirmkabupaten');
        Route::get('provinsi/{type}', 'CodeKonfirmController@konfirmprovinsi');
    });

    Route::group(['prefix' => 'export'], function () {
        Route::get('campak/{param}', 'CampakController@export');
        Route::get('afp/{param}', 'AfpController@export');
        Route::get('difteri/{param}', 'DifteriController@export');
        Route::get('tetanus/{param}', 'TetanusController@export');
        Route::get('crs/{param}', 'CrsController@export');
    });

    Route::get('sendMail', 'HelpController@sendMail');
    Route::get('/', 'DashboardController@viewDashboard');
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/', 'DashboardController@viewDashboardInstansi');
    });
    Route::group(['prefix' => 'user'], function () {
        Route::get('edit', 'AuthenticateController@viewEditUser');
        Route::get('getData', 'AuthenticateController@getProfileUser');
        Route::get('faskes/edit', 'AuthenticateController@viewEditFaskesUser');
        Route::get('getFaskesUser', 'AuthenticateController@getFaskesUser');
        Route::get('list', 'AuthenticateController@viewListUser');
        Route::post('getData/{id}', 'UserController@getData');
        Route::post('resetpassword/{id}', 'UserController@postResetPassword');
        Route::post('delete/{id}', 'UserController@postDelete');
    });
    Route::group(['prefix' => 'faskes'], function () {
        Route::group(['prefix' => 'laboratorium'], function () {
            Route::get('/', 'LaboratoriumController@index');
            Route::post('getData', 'LaboratoriumController@getData');
            Route::post('post', 'LaboratoriumController@postData');
            Route::get('getDetail/{id}', 'LaboratoriumController@getDetail');
            Route::post('delete/{id}', 'LaboratoriumController@postDelete');
        });
        Route::group(['prefix' => 'rumahsakit'], function () {
            Route::get('/', 'RumahsakitController@index');
            Route::post('getData', 'RumahsakitController@getData');
            Route::post('post', 'RumahsakitController@postData');
            Route::get('getDetail/{id}', 'RumahsakitController@getDetail');
            Route::post('delete/{id}', 'RumahsakitController@postDelete');
        });
        Route::group(['prefix' => 'puskesmas'], function () {
            Route::get('/', 'PuskesmasController@index');
            Route::post('getData', 'PuskesmasController@getData');
            Route::post('post', 'PuskesmasController@postData');
            Route::get('getDetail/{id}', 'PuskesmasController@getDetail');
            Route::post('delete/{id}', 'PuskesmasController@postDelete');
        });
        Route::group(['prefix' => 'wilayah_kerja_puskesmas'], function () {
            Route::get('/', 'PuskesmasController@viewWilayahKerja');
            Route::post('getData', 'PuskesmasController@getDataWilayahKerja');
            Route::post('import', 'PuskesmasController@importDataWilayahKerja');
            Route::get('contoh_format', 'PuskesmasController@exportExampleWilayahKerjaPuskesmas');
            Route::post('post', 'PuskesmasController@postDataWilayahKerja');
            Route::get('getDetail/{id}', 'PuskesmasController@getDetailWilayahKerja');
            Route::post('delete/{id}', 'PuskesmasController@postDeleteWilayahKerja');
        });
    });
    Route::group(['prefix' => 'lab'], function () {
        Route::get('/', 'DashboardController@viewDashboardLab');
        Route::group(['prefix' => 'case'], function () {
            Route::group(['prefix' => 'campak'], function () {
                Route::get('/', 'CampakController@indexLab');
                Route::post('getData', 'CampakController@getDataCampak');
                Route::post('getDataBasedAreaLab', 'CampakController@getDataBasedAreaLab');
                Route::post('getDataSampel', 'CampakController@getDataSampel');
                Route::get('update/{id}', 'CampakController@viewUpdateLab');
                Route::get('getDetail/{id}', 'CampakController@getDetailLab');
                Route::post('store', 'CampakController@postCase');
                Route::post('getEpid', 'CampakController@getEpid');
                Route::post('getEpidKlb', 'CampakController@getEpidKlb');
                Route::get('detail/{id}', 'CampakController@viewDetail');
                Route::post('postHasilLab', 'CampakController@postHasilLab');
                Route::post('postInputLab', 'CampakController@postInputLab');
                Route::group(['prefix' => 'periksa'], function () {
                    Route::get('{id}', 'CampakController@viewPeriksa');
                    Route::post('store', 'CampakController@postHasilLab');
                    Route::post('getData', 'CampakController@getDataPeCampak');
                    Route::get('detail/{id}', 'CampakController@viewDetailPe');
                    Route::post('delete/{id}', 'CampakController@postDeletePe');
                    Route::get('update/{id}', 'CampakController@viewUpdatePe');
                    Route::get('getDetail/{id}', 'CampakController@getDetailHasilLab');
                });
            });
            Route::group(['prefix' => 'afp'], function () {
                Route::get('/', 'AfpController@indexLab');
                Route::post('getData', 'AfpController@getDataAfp');
                Route::post('getDataBasedAreaLab', 'AfpController@getDataBasedAreaLab');
                Route::post('getDataSampel', 'AfpController@getDataSampel');
                Route::get('update/{id}', 'AfpController@viewUpdateLab');
                Route::get('getDetail/{id}', 'AfpController@getDetail');
                Route::post('store', 'AfpController@postCase');
                Route::post('getEpid', 'AfpController@getEpid');
                Route::get('detail/{id}', 'AfpController@viewDetail');
                Route::post('delete/{id}', 'AfpController@postDeleteSampel');
                Route::group(['prefix' => 'periksa'], function () {
                    Route::get('{id}', 'AfpController@viewPeriksa');
                    Route::post('store', 'AfpController@postPe');
                    Route::post('getData', 'AfpController@getDataPeAfp');
                    Route::get('detail/{id}', 'AfpController@viewDetailPe');
                    Route::post('delete/{id}', 'AfpController@postDeletePe');
                    Route::get('update/{id}', 'AfpController@viewUpdatePe');
                    Route::get('getDetail/{id}', 'AfpController@getDetailPe');
                });
            });
            Route::group(['prefix' => 'crs'], function () {
                Route::get('/', 'CrsController@indexLab');
                Route::post('getData', 'CrsController@getDataCrs');
                Route::post('getDataBasedAreaLab', 'CrsController@getDataBasedAreaLab');
                Route::post('getDataSampel', 'CrsController@getDataSampel');
                Route::get('update/{id}', 'CrsController@viewUpdate');
                Route::get('getDetail/{id}', 'CrsController@getDetail');
                Route::post('store', 'CrsController@postCase');
                Route::post('getEpid', 'CrsController@getEpid');
                Route::get('detail/{id}', 'CrsController@viewDetail');
                Route::post('delete/{id}', 'CrsController@postDelete');
                Route::group(['prefix' => 'periksa'], function () {
                    Route::get('{id}', 'CrsController@viewPeriksa');
                    Route::post('store', 'CrsController@postPe');
                    Route::post('getData', 'CrsController@getDataPeCrs');
                    Route::get('detail/{id}', 'CrsController@viewDetailPe');
                    Route::post('delete/{id}', 'CrsController@postDeletePe');
                    Route::get('update/{id}', 'CrsController@viewUpdatePe');
                    Route::get('getDetail/{id}', 'CrsController@getDetailPe');
                });
            });
        });
    });
    Route::group(['prefix' => 'notification'], function () {
        Route::get('cross', 'NotificationController@getCrosNotif');
        Route::get('klb', 'NotificationController@getKlbNotif');
    });
    Route::group(['group' => 'wilayah'], function () {
        Route::get('getArea', 'WilayahController@getArea');
        Route::get('getExpandWilayah', 'WilayahController@getExpandWilayah');
        Route::group(['prefix' => 'district'], function () {
            Route::group(['prefix' => 'provinsi'], function () {
                Route::get('/', 'ProvinsiController@index');
                Route::post('getData', 'ProvinsiController@getData');
                Route::post('post', 'ProvinsiController@postData');
                Route::get('getDetail/{id}', 'ProvinsiController@getDetail');
                Route::post('delete/{id}', 'ProvinsiController@postDelete');
            });
            Route::group(['prefix' => 'kabupaten'], function () {
                Route::get('/', 'KabupatenController@index');
                Route::post('getData', 'KabupatenController@getData');
                Route::post('post', 'KabupatenController@postData');
                Route::get('getDetail/{id}', 'KabupatenController@getDetail');
                Route::post('delete/{id}', 'KabupatenController@postDelete');
            });
            Route::group(['prefix' => 'kecamatan'], function () {
                Route::get('/', 'KecamatanController@index');
                Route::post('getData', 'KecamatanController@getData');
                Route::post('post', 'KecamatanController@postData');
                Route::get('getDetail/{id}', 'KecamatanController@getDetail');
                Route::post('delete/{id}', 'KecamatanController@postDelete');
            });
            Route::group(['prefix' => 'desa'], function () {
                Route::get('/', 'DesaController@index');
                Route::post('getData', 'DesaController@getData');
                Route::post('post', 'DesaController@postData');
                Route::get('getDetail/{id}', 'DesaController@getDetail');
                Route::post('delete/{id}', 'DesaController@postDelete');
            });
        });
    });
    Route::group(['group' => 'authenticate'], function () {
        Route::group(['prefix' => 'register'], function () {
            Route::get('/', 'AuthenticateController@viewRegister');
            Route::post('post', 'AuthenticateController@postRegister');
        });
        Route::post('reset_pass', 'AuthenticateController@postResetPass');
        Route::post('reset_password/{email}/{code}', 'AuthenticateController@postResetPasswords');
        Route::get('reset_password/{email}/{code}', 'AuthenticateController@getResetPassword');
        Route::post('login', 'AuthenticateController@getLogin');
        Route::get('logout', 'AuthenticateController@getLogout');
    });
    Route::group(['group' => 'roleAccess'], function () {
        Route::get('viewProfile', 'RoleUserController@viewProfile');
        Route::get('selectProfile', 'RoleUserController@selectProfile');
        Route::get('setProfile/{id_role}/{id}', 'RoleUserController@setProfile');
        Route::post('profile/delete/{id_role}', 'RoleUserController@deleteProfile');
        Route::group(['prefix' => 'role'], function () {
            Route::get('level', 'RoleUserController@viewRoleLevel');
            Route::post('profile', 'RoleUserController@getRoleProfile');
            Route::post('post', 'RoleUserController@postRoleProfile');
        });
    });
    Route::group(['prefix' => 'case'], function () {
        Route::group(['prefix' => 'campak'], function () {
            Route::group(['prefix' => 'zeroreport'], function () {
                Route::post('getData', 'CampakController@getZeroReport');
                Route::post('store', 'CampakController@postZeroReport');
                Route::post('delete/{id}', 'CampakController@deleteZeroReport');
            });
            Route::get('contoh_format', 'CampakController@exportExampleImportKasus');
            Route::get('/', 'CampakController@index');
            Route::get('getRekapitulasiData', 'CampakController@getRekapitulasiData');
            Route::post('getData', 'CampakController@getDataCampak');
            Route::post('delete/{id}', 'CampakController@postDelete');
            Route::get('update/{id}', 'CampakController@viewUpdate');
            Route::get('getDetail/{id}', 'CampakController@getDetail');
            Route::post('store', 'CampakController@postCase');
            Route::post('getEpid', 'CampakController@getEpid');
            Route::post('getEpidKlb', 'CampakController@getEpidKlb');
            Route::get('detail/{id}', 'CampakController@viewDetail');
            Route::post('delete/{id}', 'CampakController@postDelete');
            Route::get('export/{param}', 'CampakController@export');
            Route::post('move/{id}', 'CampakController@moveCase');
            Route::post('getDumpData', 'CampakController@getDumpData');
            Route::get('review', 'CampakController@review');
            Route::get('gettxt', 'CampakController@gettxt');
            Route::post('postSpesimen', 'CampakController@postSpesimen');
            Route::post('deleteSpesimen', 'CampakController@deleteSpesimen');
            Route::group(['prefix' => 'pe'], function () {
                Route::get('{id}', 'CampakController@viewPe');
                Route::post('store', 'CampakController@postPe');
                Route::post('getData', 'CampakController@getDataPeCampak');
                Route::get('detail/{id}', 'CampakController@viewDetailPe');
                Route::post('delete/{id}', 'CampakController@postDeletePe');
                Route::get('update/{id}', 'CampakController@viewUpdatePe');
                Route::get('getDetail/{id}', 'CampakController@getDetailPe');
            });
        });
        Route::group(['prefix' => 'afp'], function () {
            Route::get('/', 'AfpController@index');
            // import begin
            Route::get('contoh_format', 'AfpController@exportExampleImportKasus');
            Route::post('import', 'AfpController@importCase');
            // import end
            Route::post('getData', 'AfpController@getDataAfp');
            Route::get('update/{id}', 'AfpController@viewUpdate');
            Route::get('getDetail/{id}', 'AfpController@getDetail');
            Route::get('export/{param}', 'AfpController@export');
            Route::post('store', 'AfpController@postCase');
            Route::post('getEpid', 'AfpController@getEpid');
            Route::get('detail/{id}', 'AfpController@viewDetail');
            Route::post('delete/{id}', 'AfpController@postDelete');
            Route::group(['prefix' => 'pe'], function () {
                Route::get('{id}', 'AfpController@viewPe');
                Route::post('store', 'AfpController@postPe');
                Route::post('getData', 'AfpController@getDataPeAfp');
                Route::get('detail/{id}', 'AfpController@viewDetailPe');
                Route::post('delete/{id}', 'AfpController@postDeletePe');
                Route::get('update/{id}', 'AfpController@viewUpdatePe');
                Route::get('getDetail/{id}', 'AfpController@getDetailPe');
            });
            Route::group(['prefix' => 'hkf'], function () {
                Route::get('{id}', 'AfpController@viewHkf');
                Route::post('store', 'AfpController@postHkf');
                Route::post('getData', 'AfpController@getDataPeAfp');
                Route::get('detail/{id}', 'AfpController@viewDetailPe');
                Route::post('delete/{id}', 'AfpController@postDeletePe');
                Route::get('update/{id}', 'AfpController@viewUpdatePe');
                Route::get('getDetail/{id}', 'AfpController@getDetailPe');
            });
            Route::group(['prefix' => 'ku60'], function () {
                Route::get('{id}', 'AfpController@viewKu60');
                Route::post('store', 'AfpController@postKu60');
                Route::post('getData', 'AfpController@getDataKu60');
                Route::get('detail/{id}', 'AfpController@viewDetailKu60');
                Route::post('delete/{id}', 'AfpController@postDeleteKu60');
                Route::get('update/{id}', 'AfpController@viewKu60');
                Route::get('getDetail/{id}', 'AfpController@getDetailKu60');
            });
        });
        Route::group(['prefix' => 'difteri'], function () {
            Route::get('/', 'DifteriController@index');
            // import begin
            Route::get('contoh_format', 'DifteriController@exportExampleImportKasus');
            Route::post('import', 'DifteriController@importCase');
            // import end
            Route::get('contoh_format', 'DifteriController@exportExampleImportKasus');
            Route::post('getData', 'DifteriController@getDataDifteri');
            Route::get('update/{id}', 'DifteriController@viewUpdate');
            Route::get('getDetail/{id}', 'DifteriController@getDetail');
            Route::post('store', 'DifteriController@postCase');
            Route::post('getEpid', 'DifteriController@getEpid');
            Route::get('detail/{id}', 'DifteriController@viewDetail');
            Route::post('delete/{id}', 'DifteriController@postDelete');
            Route::group(['prefix' => 'pe'], function () {
                Route::get('{id}', 'DifteriController@viewPe');
                Route::post('store', 'DifteriController@postPe');
                Route::post('getData', 'DifteriController@getDataPeDifteri');
                Route::get('detail/{id}', 'DifteriController@viewDetailPe');
                Route::post('delete/{id}', 'DifteriController@postDeletePe');
                Route::get('update/{id}', 'DifteriController@viewUpdatePe');
                Route::get('getDetail/{id}', 'DifteriController@getDetailPe');
            });
        });
        Route::group(['prefix' => 'tetanus'], function () {
            Route::get('/', 'TetanusController@index');
            // import begin
            Route::get('contoh_format', 'TetanusController@exportExampleImportKasus');
            Route::post('import', 'TetanusController@importCase');
            // import end
            Route::get('contoh_format', 'TetanusController@exportExampleImportKasus');
            Route::post('getData', 'TetanusController@getDataTetanus');
            Route::get('update/{id}', 'TetanusController@viewUpdate');
            Route::get('getDetail/{id}', 'TetanusController@getDetail');
            Route::post('store', 'TetanusController@postCase');
            Route::post('getEpid', 'TetanusController@getEpid');
            Route::get('detail/{id}', 'TetanusController@viewDetail');
            Route::post('delete/{id}', 'TetanusController@postDelete');
            Route::group(['prefix' => 'pe'], function () {
                Route::get('{id}', 'TetanusController@viewPe');
                Route::post('store', 'TetanusController@postPe');
                Route::post('getData', 'TetanusController@getDataPeTetanus');
                Route::get('detail/{id}', 'TetanusController@viewDetailPe');
                Route::post('delete/{id}', 'TetanusController@postDeletePe');
                Route::get('update/{id}', 'TetanusController@viewUpdatePe');
                Route::get('getDetail/{id}', 'TetanusController@getDetailPe');
            });
        });
        Route::group(['prefix' => 'crs'], function () {
            Route::get('/', 'CrsController@index');
            // import begin
            Route::get('contoh_format', 'CrsController@exportExampleImportKasus');
            Route::post('import', 'CrsController@importCase');
            // import end
            Route::get('contoh_format', 'CrsController@exportExampleImportKasus');
            Route::post('getData', 'CrsController@getDataCrs');
            Route::get('update/{id}', 'CrsController@viewUpdate');
            Route::get('getDetail/{id}', 'CrsController@getDetail');
            Route::post('store', 'CrsController@postCase');
            Route::post('getEpid', 'CrsController@getEpid');
            Route::get('detail/{id}', 'CrsController@viewDetail');
            Route::post('delete/{id}', 'CrsController@postDelete');
            Route::group(['prefix' => 'pe'], function () {
                Route::get('{id}', 'CrsController@viewPe');
                Route::post('store', 'CrsController@postPe');
                Route::post('getData', 'CrsController@getDataPeCrs');
                Route::get('detail/{id}', 'CrsController@viewDetailPe');
                Route::post('delete/{id}', 'CrsController@postDeletePe');
                Route::get('update/{id}', 'CrsController@viewUpdatePe');
                Route::get('getDetail/{id}', 'CrsController@getDetailPe');
            });
        });
    });
    Route::group(['group' => 'pasien'], function () {
        Route::group(['prefix' => 'pasien'], function () {
            Route::post('getAge', 'PasienController@getAge');
            Route::get('getDataPasien/{id}', 'PasienController@getDataPasien');
            Route::get('getData', 'PasienController@getData');
            Route::get('getDataLab', 'PasienController@getDataLab');
        });
    });
    Route::group(['prefix' => 'help'], function () {
        Route::group(['prefix' => 'faq'], function () {
            Route::get('/', 'HelpController@index');
            Route::get('addFaq', 'FaqController@viewAdd');
            Route::get('edit/{id}', 'FaqController@viewEdit');
            Route::post('store', 'FaqController@postFaq');
            Route::post('delete/{id}', 'FaqController@postDelete');
        });
        Route::group(['prefix' => 'course'], function () {
            Route::get('addCourse', 'CourseController@viewAdd');
            Route::post('store', 'CourseController@postCourse');
            Route::get('edit/{id}', 'CourseController@viewEdit');
            Route::post('delete/{id}', 'CourseController@postDelete');
        });
        Route::group(['prefix' => 'contact'], function () {
            Route::post('store', 'ContactUsController@postContactUs');
        });
    });
});

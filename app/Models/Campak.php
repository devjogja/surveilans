<?php

namespace App\Models;

use App\Models\Puskesmas;
use Illuminate\Database\Eloquent\Model;
use DB;

class Campak extends Model
{
	public static function q()
	{
		return DB::select("SELECT
			a.id,
			a.id AS id_trx_case,
			a.id_role,
			a.id_role_old,
			a.jenis_input,
			a.id_faskes_old,
			a.no_rm,
			a._no_epid,
			a.no_epid,
			a.no_epid_klb,
			a.no_epid_lama,
			a.klasifikasi_final,
			CASE
			WHEN a.klasifikasi_final = 1 THEN 'Campak Klinis'
			WHEN a.klasifikasi_final = 2 THEN 'Campak Lab'
			WHEN a.klasifikasi_final = 3 THEN 'Campak Epid'
			WHEN a.klasifikasi_final = 4 THEN 'Rubella'
			WHEN a.klasifikasi_final = 5 THEN 'Bukan campak atau Rubella (Discarded)'
			WHEN a.klasifikasi_final = 6 THEN 'Mix Rubella positif dan Campak Positif'
			WHEN a.klasifikasi_final = 7 THEN 'Pending'
			ELSE NULL
			END klasifikasi_final_txt,
			CASE
			WHEN a.jenis_input = 1 THEN 'Web'
			WHEN a.jenis_input = 2 THEN 'Android'
			WHEN a.jenis_input = 3 THEN 'Import'
			END AS jenis_input_text,
			a.id_case,
			a.id_faskes,
			DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
			DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
			DATE_FORMAT(a.created_at, '%d-%m-%Y %H:%i:%s') AS created_at,
			DATE_FORMAT(a.updated_at, '%d-%m-%Y %H:%i:%s') AS updated_at,
			DATE_FORMAT(a.deleted_at, '%d-%m-%Y %H:%i:%s') AS deleted_at,
			CASE
			WHEN a.id_role = 1 THEN i.code_faskes
			WHEN a.id_role = 2 THEN j.code_faskes
			END AS code_faskes,
			CASE
			WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',i.name)
			WHEN a.id_role = 2 THEN j.name
			END name_faskes,
			CASE
			WHEN a.id_role = 1 THEN i.name_kecamatan
			WHEN a.id_role = 2 THEN j.name_kecamatan
			END name_kecamatan_faskes,
			CASE
			WHEN a.id_role = 1 THEN i.code_kelurahan
			WHEN a.id_role = 2 THEN j.code_kelurahan
			END code_kelurahan_faskes,
			CASE
			WHEN a.id_role = 1 THEN i.code_kecamatan
			WHEN a.id_role = 2 THEN j.code_kecamatan
			END code_kecamatan_faskes,
			CASE
			WHEN a.id_role = 1 THEN i.name_kabupaten
			WHEN a.id_role = 2 THEN j.name_kabupaten
			END name_kabupaten_faskes,
			CASE
			WHEN a.id_role = 1 THEN i.code_kabupaten
			WHEN a.id_role = 2 THEN j.code_kabupaten
			END code_kabupaten_faskes,
			CASE
			WHEN a.id_role = 1 THEN i.name_provinsi
			WHEN a.id_role = 2 THEN j.name_provinsi
			END name_provinsi_faskes,
			CASE
			WHEN a.id_role = 1 THEN i.code_provinsi
			WHEN a.id_role = 2 THEN j.code_provinsi
			END code_provinsi_faskes,
			n.id AS id_pe,
			CASE
			WHEN n.id IS NULL THEN 'PE'
			ELSE 'Sudah PE'
			END AS 'pe_text',
			a.id_pasien,
			r.code AS code_roles,
			n.total_kasus_tambahan,
			b.*,
			c.*,
			d.*
			FROM
			trx_case a
			LEFT JOIN roles r ON a.id_role = r.id
			JOIN (
			SELECT
			b.name AS name_pasien,
			b.nik,
			c.name AS agama,
			c.name AS religion_text,
			d.name AS nama_ortu,
			b.jenis_kelamin,
			CASE
			WHEN b.jenis_kelamin = 'L' THEN 'Laki-laki'
			WHEN b.jenis_kelamin = 'P' THEN 'Perempuan'
			WHEN b.jenis_kelamin = 'T' THEN 'Tidak Jelas'
			ELSE 'Belum diisi' END AS jenis_kelamin_txt,
			DATE_FORMAT(b.tgl_lahir, '%d-%m-%Y') AS tgl_lahir,
			b.umur_thn,
			b.umur_bln,
			b.umur_hari,
			b.alamat,
			b.tempat_lahir,
			e.code AS code_kelurahan_pasien,
			e.name AS kelurahan_pasien,
			e.name AS name_kelurahan_pasien,
			f.code AS code_kecamatan_pasien,
			f.name AS kecamatan_pasien,
			f.name AS name_kecamatan_pasien,
			g.code AS code_kabupaten_pasien,
			g.name AS kabupaten_pasien,
			g.name AS name_kabupaten_pasien,
			h.code AS code_provinsi_pasien,
			h.name AS provinsi_pasien,
			h.name AS name_provinsi_pasien,
			CONCAT_WS(', ', b.alamat, e.name, f.name, g.name, h.name) AS full_address,
			b.longitude,
			b.latitude,
			d.id AS id_family,
			a.id AS i1
			FROM
			trx_case a
			JOIN ref_pasien b ON a.id_pasien = b.id
			LEFT JOIN mst_religion c ON b.agama = c.id
			JOIN ref_family d ON b.id = d.id_pasien
			LEFT JOIN mst_kelurahan e ON b.code_kelurahan = e.code
			LEFT JOIN mst_kecamatan f ON e.code_kecamatan = f.code
			LEFT JOIN mst_kabupaten g ON f.code_kabupaten = g.code
			LEFT JOIN mst_provinsi h ON g.code_provinsi = h.code
			WHERE
			a.id_case = '1' AND a.deleted_at IS NULL
			GROUP BY a.id
			) b ON a.id = b.i1
			LEFT JOIN view_puskesmas i ON a.id_faskes = i.id AND a.id_role = 1
			LEFT JOIN view_rumahsakit j ON a.id_faskes = j.id AND a.id_role = 2
			JOIN (
			SELECT
			k.id AS id_trx_klinis,
			a.id AS i2,
			DATE_FORMAT(k.tgl_imunisasi_campak, '%d-%m-%Y') AS tgl_imunisasi_campak,
			k.jml_imunisasi_campak,
			k.jml_imunisasi_campak AS jml_imunisasi,
			CASE
			WHEN k.jml_imunisasi_campak = 7 THEN 'Tidak'
			WHEN k.jml_imunisasi_campak = 8 THEN 'Tidak Tahu'
			WHEN k.jml_imunisasi_campak = 9 THEN 'Belum Pernah'
			WHEN k.jml_imunisasi_campak IN ('1','2','3','4','5','6') THEN CONCAT(k.jml_imunisasi_campak, 'x')
			ELSE 'Belum diisi' END AS jml_imunisasi_campak_txt,
			DATE_FORMAT(k.tgl_mulai_demam, '%d-%m-%Y') AS tgl_mulai_demam,
			DATE_FORMAT(k.tgl_mulai_rash, '%d-%m-%Y') AS tgl_mulai_rash,
			k.tgl_mulai_rash AS tgl_sakit_date,
			DATE_FORMAT(k.tgl_mulai_rash, '%d-%m-%Y') AS tgl_sakit,
			YEAR(k.tgl_mulai_rash) AS thn_sakit,
			DATE_FORMAT(k.tgl_laporan_diterima, '%d-%m-%Y') AS tgl_laporan_diterima,
			DATE_FORMAT(k.tgl_pelacakan, '%d-%m-%Y') AS tgl_pelacakan,
			k.vitamin_a,
			CASE
			WHEN k.vitamin_a = 1 THEN 'Ya'
			WHEN k.vitamin_a = 2 THEN 'Tidak'
			END AS vitamin_a_txt,
			k.keadaan_akhir,
			CASE
			WHEN k.keadaan_akhir = 1 THEN 'Hidup'
			WHEN k.keadaan_akhir = 2 THEN 'Meninggal'
			END AS keadaan_akhir_txt,
			k.jenis_kasus,
			CASE
			WHEN k.jenis_kasus = 1 THEN 'KLB'
			WHEN k.jenis_kasus = 2 THEN 'Bukan KLB'
			END AS jenis_kasus_txt,
			k.klb_ke,
			k.status_kasus,
			CASE
			WHEN k.status_kasus = 1 THEN 'Index'
			WHEN k.status_kasus = 2 THEN 'Bukan Index'
			END AS status_kasus_txt,
			GROUP_CONCAT(DISTINCT CONCAT(IFNULL(l.id_gejala,''),'_',IFNULL(DATE_FORMAT(l.tgl_kejadian, '%d-%m-%Y'),'')) ORDER BY l.id_gejala ASC SEPARATOR '|') AS gejala,
			GROUP_CONCAT(DISTINCT m.name SEPARATOR '|') komplikasi,
			GROUP_CONCAT(DISTINCT m.other_komplikasi SEPARATOR ',') other_komplikasi
			FROM
			trx_case a
			JOIN trx_klinis k ON a.id = k.id_trx_case
			LEFT JOIN trx_gejala l ON k.id = l.id_trx_klinis
			LEFT JOIN trx_komplikasi m ON k.id = m.id_trx_klinis
			WHERE
			a.id_case = '1' AND a.deleted_at IS NULL
			GROUP BY a.id) c ON a.id = c.i2
			JOIN (
			SELECT
			a.id AS i3,
			GROUP_CONCAT(DISTINCT CONCAT(IFNULL(o.jenis_pemeriksaan,''),'_',IFNULL(o.jenis_spesimen,''),'_',IFNULL(DATE_FORMAT(o.tgl_ambil_spesimen,'%d-%m-%Y'),''),'_',IFNULL(o.hasil,'')) SEPARATOR '|') AS spesimen
			FROM
			trx_case a
			LEFT JOIN trx_spesimen o ON a.id = o.id_trx_case
			WHERE
			a.id_case = '1' AND a.deleted_at IS NULL
			GROUP BY a.id) d ON a.id = d.i3
			LEFT JOIN trx_pe n ON a.id = n.id_trx_case AND n.deleted_at IS NULL
			WHERE
			a.id_case = '1' AND a.deleted_at IS NULL
			GROUP BY a.id");
	}

	public static function qq()
	{
		$q = "(
		SELECT
			a.id AS id_trx_case,
			a.no_epid,
			a.no_epid_klb,
			b.name AS name_pasien,
			b.umur_hari,
			b.umur_bln,
			b.umur_thn,
			CONCAT_WS(',', IFNULL(b.alamat,''), IFNULL(c.name,''), IFNULL(d.name,''), IFNULL(e.name,''), IFNULL(f.name,'')) AS full_address,
			CASE
			WHEN g.keadaan_akhir = 1 THEN 'Hidup'
			WHEN g.keadaan_akhir = 2 THEN 'Meninggal'
			ELSE ''
			END AS keadaan_akhir_txt,
			CASE
			WHEN a.klasifikasi_final = 1 THEN 'Campak Klinis'
			WHEN a.klasifikasi_final = 2 THEN 'Campak Lab'
			WHEN a.klasifikasi_final = 3 THEN 'Campak Epid'
			WHEN a.klasifikasi_final = 4 THEN 'Rubella'
			WHEN a.klasifikasi_final = 5 THEN 'Bukan campak atau Rubella (Discarded)'
			WHEN a.klasifikasi_final = 6 THEN 'Mix Rubella positif dan Campak Positif'
			WHEN a.klasifikasi_final = 7 THEN 'Pending'
			ELSE NULL
			END AS klasifikasi_final_txt,
			a.id_faskes,
			a.id_role,
			CASE
			WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',h.name)
			WHEN a.id_role = 2 THEN i.name
			WHEN a.id_role = 6 THEN CONCAT('KABUPATEN ',j.name)
			ELSE NULL
			END AS name_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_kecamatan
			ELSE NULL
			END AS code_kecamatan_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_kabupaten
			WHEN a.id_role = 2 THEN i.code_kabupaten
			WHEN a.id_role = 6 THEN j.code
			ELSE NULL
			END AS code_kabupaten_faskes,
			g.tgl_mulai_rash AS tgl_sakit_date,
			DATE_FORMAT(g.tgl_mulai_rash, '%Y')AS thn_sakit,
			CASE
			WHEN a.id_role = 1 THEN h.code_provinsi
			WHEN a.id_role = 2 THEN i.code_provinsi
			WHEN a.id_role = 6 THEN j.code_provinsi
			ELSE NULL
			END AS code_provinsi_faskes,
			a.jenis_input,
			g.status_kasus,
			k.id AS id_pe,
			CASE
			WHEN k.id IS NULL THEN 'PE'
			ELSE 'Sudah PE'
			END AS 'pe_text',
			a.id_faskes_old,
			a.id_role_old,
			DATE_FORMAT(a.created_at, '%d/%m/%Y') AS tgl_input,
			DATE_FORMAT(a.updated_at, '%d/%m/%Y') AS tgl_update,
			CASE
			WHEN a.jenis_input = 1 THEN 'Web'
			WHEN a.jenis_input = 2 THEN 'Android'
			WHEN a.jenis_input = 3 THEN 'Import'
			END AS jenis_input_text,
			CASE
			WHEN g.status_kasus = 1 THEN 'Index'
			WHEN g.status_kasus = 2 THEN 'Bukan'
			END AS status_kasus_txt,
			c.code AS code_kelurahan_pasien,
			d.code AS code_kecamatan_pasien,
			e.code AS code_kabupaten_pasien,
			f.code AS code_provinsi_pasien
		FROM
		trx_case a
		JOIN ref_pasien b ON a.id_pasien = b.id AND b.deleted_at IS NULL
		LEFT JOIN mst_kelurahan c ON b.code_kelurahan = c.code
		LEFT JOIN mst_kecamatan d ON c.code_kecamatan = d.code
		LEFT JOIN mst_kabupaten e ON d.code_kabupaten = e.code
		LEFT JOIN mst_provinsi f ON e.code_provinsi = f.code
		JOIN trx_klinis g ON a.id=g.id_trx_case
		LEFT JOIN (" . Puskesmas::qq() . ") AS h ON a.id_faskes = h.id AND a.id_role='1'
		LEFT JOIN (" . Rumahsakit::qq() . ") i ON a.id_faskes = i.id AND a.id_role='2'
		LEFT JOIN (" . Kabupaten::qq() . ") j ON a.id_faskes = j.code AND a.id_role='6'
		LEFT JOIN trx_pe k ON a.id=k.id_trx_case AND k.deleted_at IS NULL
		WHERE
		a.id_case = 1 AND a.deleted_at IS NULL
		GROUP BY a.id) AS a
		";
		return $q;
	}

	public static function getData()
	{
		$q = "
			(SELECT
			a.id AS id_trx_case,
			a.no_epid,
			a._no_epid,
			a.no_epid_klb,
			a.no_epid_lama,
			a.no_rm,
			b.name AS name_pasien,
			b.nik,
			b.umur_hari,
			b.umur_bln,
			b.umur_thn,
			b.alamat,
			CONCAT(b.alamat,' ',c.name,', ',d.name,', ',e.name,', ',f.name) AS full_address,
			c.name AS name_kelurahan_pasien,
			d.name AS name_kecamatan_pasien,
			e.name AS name_kabupaten_pasien,
			f.name AS name_provinsi_pasien,
			CASE
			WHEN g.keadaan_akhir = 1 THEN 'Hidup'
			WHEN g.keadaan_akhir = 2 THEN 'Meninggal'
			ELSE ''
			END AS keadaan_akhir_txt,
			CASE
			WHEN a.klasifikasi_final = 1 THEN 'Campak Klinis'
			WHEN a.klasifikasi_final = 2 THEN 'Campak Lab'
			WHEN a.klasifikasi_final = 3 THEN 'Campak Epid'
			WHEN a.klasifikasi_final = 4 THEN 'Rubella'
			WHEN a.klasifikasi_final = 5 THEN 'Bukan campak atau Rubella (Discarded)'
			WHEN a.klasifikasi_final = 6 THEN 'Mix Rubella positif dan Campak Positif'
			WHEN a.klasifikasi_final = 7 THEN 'Pending'
			ELSE NULL
			END AS klasifikasi_final_txt,
			a.klasifikasi_final,
			a.id_faskes,
			a.id_role,
			CASE
			WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',h.name)
			WHEN a.id_role = 2 THEN i.name
			WHEN a.id_role = 6 THEN CONCAT('KABUPATEN ',j.name)
			ELSE NULL
			END AS name_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_kecamatan
			ELSE NULL
			END AS code_kecamatan_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.name_kecamatan
			ELSE NULL
			END AS name_kecamatan_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_kabupaten
			WHEN a.id_role = 2 THEN i.code_kabupaten
			WHEN a.id_role = 6 THEN j.code
			ELSE NULL
			END AS code_kabupaten_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.name_kabupaten
			WHEN a.id_role = 2 THEN i.name_kabupaten
			WHEN a.id_role = 6 THEN j.name
			ELSE NULL
			END AS name_kabupaten_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_provinsi
			WHEN a.id_role = 2 THEN i.code_provinsi
			WHEN a.id_role = 6 THEN j.code_provinsi
			ELSE NULL
			END AS code_provinsi_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.name_provinsi
			WHEN a.id_role = 2 THEN i.name_provinsi
			WHEN a.id_role = 6 THEN j.name_provinsi
			ELSE NULL
			END AS name_provinsi_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_faskes
			WHEN a.id_role = 2 THEN i.code_faskes
			ELSE NULL
			END AS code_faskes,
			a.jenis_input,
			CASE
			WHEN a.jenis_input = 1 THEN 'Web'
			WHEN a.jenis_input = 2 THEN 'Android'
			WHEN a.jenis_input = 3 THEN 'Import'
			END AS jenis_input_text,
			g.status_kasus,
			CASE
			WHEN g.status_kasus = 1 THEN 'Index'
			WHEN g.status_kasus = 2 THEN 'Bukan'
			END AS status_kasus_txt,
			k.id AS id_pe,
			CASE
			WHEN k.id IS NULL THEN 'PE'
			ELSE 'Sudah PE'
			END AS 'pe_text',
			c.code AS code_kelurahan_pasien,
			d.code AS code_kecamatan_pasien,
			e.code AS code_kabupaten_pasien,
			f.code AS code_provinsi_pasien,
			g.tgl_mulai_rash AS tgl_sakit_date,
			DATE_FORMAT(g.tgl_mulai_rash, '%Y') AS thn_sakit,
			a.id_faskes_old,
			a.id_role_old,
			DATE_FORMAT(a.created_at, '%d/%m/%Y') AS tgl_input,
			DATE_FORMAT(a.updated_at, '%d/%m/%Y') AS tgl_update,
			b.jenis_kelamin,
			CASE
			WHEN b.jenis_kelamin = 'L' THEN 'Laki-laki'
			WHEN b.jenis_kelamin = 'P' THEN 'Perempuan'
			WHEN b.jenis_kelamin = 'T' THEN 'Tidak Jelas'
			ELSE ''
			END AS jenis_kelamin_txt,
			g.jenis_kasus,
			g.jml_imunisasi_campak AS jml_imunisasi,
			g.klb_ke,
			l.name AS agama_txt,
			m.name AS nama_ortu,
			DATE_FORMAT(b.tgl_lahir, '%d/%m/%Y') AS tgl_lahir,
			DATE_FORMAT(g.tgl_imunisasi_campak, '%d/%m/%Y') AS tgl_imunisasi_campak,
			g.jml_imunisasi_campak AS jml_imunisasi_campak,
			CASE
			WHEN g.jml_imunisasi_campak = 7 THEN 'Tidak'
			WHEN g.jml_imunisasi_campak = 8 THEN 'Tidak tahu'
			WHEN g.jml_imunisasi_campak = 9 THEN 'Belum pernah'
			WHEN g.jml_imunisasi_campak = NULL THEN '-'
			ELSE CONCAT(g.jml_imunisasi_campak, ' x')
			END AS jml_imunisasi_campak_txt,
			DATE_FORMAT(g.tgl_mulai_demam, '%d/%m/%Y') AS tgl_mulai_demam,
			DATE_FORMAT(g.tgl_mulai_rash, '%d/%m/%Y') AS tgl_mulai_rash,
			DATE_FORMAT(g.tgl_laporan_diterima, '%d/%m/%Y') AS tgl_laporan_diterima,
			DATE_FORMAT(g.tgl_pelacakan, '%d/%m/%Y') AS tgl_pelacakan,
			g.vitamin_a,
			CASE
			WHEN g.vitamin_a = 1 THEN 'Ya'
			WHEN g.vitamin_a = 2 THEN 'Tidak'
			ELSE NULL
			END AS vitamin_a_txt,
			g.keadaan_akhir,
			CASE
			WHEN g.jenis_kasus = 1 THEN 'KLB'
			WHEN g.jenis_kasus = 2 THEN 'Bukan KLB'
			ELSE NULL
			END AS jenis_kasus_txt,
			GROUP_CONCAT(DISTINCT CONCAT(n.id_gejala,'_',n.tgl_kejadian) ORDER BY n.id_gejala ASC SEPARATOR '|') AS gejala,
			GROUP_CONCAT(DISTINCT o.name SEPARATOR '|') komplikasi,
			GROUP_CONCAT(DISTINCT o.other_komplikasi SEPARATOR ',') other_komplikasi
		FROM
		trx_case a
		JOIN ref_pasien b ON a.id_pasien = b.id AND b.deleted_at IS NULL
		LEFT JOIN ref_family m ON b.id = m.id_pasien
		LEFT JOIN mst_kelurahan c ON b.code_kelurahan = c.code
		LEFT JOIN mst_kecamatan d ON c.code_kecamatan = d.code
		LEFT JOIN mst_kabupaten e ON d.code_kabupaten = e.code
		LEFT JOIN mst_provinsi f ON e.code_provinsi = f.code
		JOIN trx_klinis g ON a.id=g.id_trx_case
		LEFT JOIN (
			" . Puskesmas::qq() . "
		) AS h ON a.id_faskes = h.id AND a.id_role='1'
		LEFT JOIN (
			" . Rumahsakit::qq() . "
		) i ON a.id_faskes = i.id AND a.id_role='2'
		LEFT JOIN (
			" . Kabupaten::qq() . "
		) j ON a.id_faskes = j.code AND a.id_role='6'
		LEFT JOIN trx_pe k ON a.id=k.id_trx_case AND k.deleted_at IS NULL
		LEFT JOIN mst_religion l ON b.agama = l.id
		LEFT JOIN (
			SELECT
			a.id_trx_klinis,
			DATE_FORMAT (a.tgl_kejadian, '%d/%m/%Y') AS tgl_kejadian,
			c.name,
			c.id AS id_gejala
			FROM trx_gejala a
			JOIN mst_gejala c ON a.id_gejala = c.id AND c.id_case = 1
		) n ON g.id = n.id_trx_klinis
		LEFT JOIN trx_komplikasi o ON g.id = o.id_trx_klinis
		WHERE
		a.id_case = 1 AND a.deleted_at IS NULL
		GROUP BY a.id) AS a
		";
		return $q;
	}
}

<?php

namespace App\Models;

use Auth;
use DB;
use Illuminate\Database\Eloquent\Model;
use Sentinel;

class Course extends Model {
    public static function insert($data) {
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
        $data['updated_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
        $query              = DB::table('course')->insert($data);
        // return DB::table('faq')->insertGetId($data);
        return $query;
    }

    public static function getData() {
        $query = DB::table('course AS a');
        $query->select('a.id', 'a.course_judul', 'a.course_konten')->whereNull('a.deleted_at');
        return $query->get();
    }

    public static function getDataCourse($where = array()) {
        $query = DB::table('course');
        foreach ($where as $key => $val) {
            $query->where($key, $val);
        }
        return $query->get();
    }

    public static function pull($where = array(), $data) {
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['updated_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
        $query              = DB::table('course');
        foreach ($where as $key => $val) {
            $query->where($key, $val);
        }
        return $query->update($data);
    }

    public static function postDelete($id) {
        $data['deleted_at'] = date('Y-m-d H:i:s');
        $data['deleted_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
        return DB::table('course')->where('id', $id)->update($data);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Laboratorium extends Model
{
  protected $table = 'mst_laboratorium';
  protected $fillable = ['id', 'name', 'code', 'alamat', 'konfirm_code', 'created_by'];

  public static function getData($where = array())
  {
    $query = DB::table('mst_laboratorium AS a');
    $query->select('a.id', 'a.name', 'a.code', 'a.alamat', 'a.telepon', 'a.email', 'a.konfirm_code');
    $query->where('a.deleted_at', NULL);
    foreach ($where as $key => $val) {
      if (!empty($val)) {
        $query->where($key, $val);
      }
    }
    return $query->get();
  }

  public static function getLaboratorium($where = array())
  {
    $query = DB::table('mst_laboratorium AS a');
    $query->where('a.deleted_at', NULL);
    foreach ($where as $key => $val) {
      $query->where($key, $val);
    }
    $data = $query->get();
    $response = array();
    foreach ($data as $key => $val) {
      $response[$val->code] = $val;
    }
    return $response;
  }
  
  public static function getUpdate($where = array(), $data = array())
  {
    $query = DB::table('mst_puskesmas');
    $query->where('deleted_at', NULL);
    foreach ($where as $key => $val) {
      $query->where($key, $val);
    }
    return $query->update($data);
  }
}

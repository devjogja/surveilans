<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    public static function qq()
    {
        $q = "
			SELECT
			a.code,
			a.name,
			b.name name_provinsi,
			b.code code_provinsi
			FROM
			mst_kabupaten a
			JOIN mst_provinsi b ON a.code_provinsi = b.code
			";
        return $q;
    }
}

<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Rumahsakit extends Model {
    public static function getData($where = array()) {
        $query = DB::table('view_rumahsakit AS a');
        $query->select('a.id', 'a.name', 'a.code_faskes', 'a.code_faskes_bpjs', 'a.alamat', 'a.longitude', 'a.latitude', 'a.konfirm_code', 'a.code_kelurahan', 'a.code_kecamatan', 'a.code_kabupaten', 'a.code_provinsi', 'a.name_kelurahan', 'a.name_kecamatan', 'a.name_kabupaten', 'a.name_provinsi');
        foreach ($where as $key => $val) {
        	if (!empty($val)) {
            $query->where($key, $val);
        	}
        }
        return $query->get();
    }

    public static function qq(){
        $q = "
        SELECT
        a.id,
        a.code_faskes,
        a.name,
        a.code_kabupaten,
        a.code_provinsi,
        b.name name_kabupaten,
        c.name name_provinsi
        FROM
        mst_rumahsakit a
        JOIN mst_kabupaten b ON a.code_kabupaten = b.code
        JOIN mst_provinsi c ON b.code_provinsi = c.code
        ";
        return $q;
    }
}

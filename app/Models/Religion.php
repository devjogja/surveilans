<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Religion extends Model
{
    public static function getData($where=array())
    {
        $religion = DB::table('mst_religion AS a');
        $religion->select('a.id','a.name');
        $religion->where('a.deleted_at', NULL);
        foreach ($where as $key => $val) {
        	$religion->where($key, $val);
        }
        return $religion->get();
    }
}

<?php

namespace App\Models;

use Auth;
use DB;
use Illuminate\Database\Eloquent\Model;
use Sentinel;

class TxPe extends Model {
    public static function ifExist($table, $where) {
        $query = DB::table($table);
        foreach ($where as $key => $val) {
            $query->where($key, $val);
        }
        return $query;
    }

    public static function pushPE($table, $data) {
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
        $data['updated_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
        return DB::table($table)->insertGetId($data);
    }

    public static function pullPE($table, $where = array(), $data) {
        $query = DB::table($table);
        foreach ($where as $key => $val) {
            $query->where($key, $val);
        }
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['updated_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
        return $query->update($data);
    }

    public static function insertKasusTambahan($_pe = '', $data = array()) {
        // truncate data
        $query = DB::table('trx_pe_kasus_tambahan')->where('id_trx_pe', $id_trx_pe)->delete();
        // insert
        foreach ($data as $key => $val) {
            $val['created_at'] = date('Y-m-d H:i:s');
            $val['updated_at'] = date('Y-m-d H:i:s');
            $val['created_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
            $val['updated_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
            $kasus             = DB::table('trx_pe_kasus_tambahan')->insert($val);
        }
        return $kasus;
    }

    public static function insertPelaksana($id_trx_pe = '', $data = array()) {
        // truncate data
        $query = DB::table('trx_pe_pelaksana')->where('id_trx_pe', $id_trx_pe)->delete();
        // insert
        foreach ($data as $key => $val) {
            $val['created_at'] = date('Y-m-d H:i:s');
            $val['updated_at'] = date('Y-m-d H:i:s');
            $val['created_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
            $val['updated_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
            $pelaksana         = DB::table('trx_pe_pelaksana')->insert($val);
        }
        return $pelaksana;
    }

    public static function getPeCampak($where = array()) {
        $query = DB::table('view_pe_campak');
        foreach ($where as $key => $val) {
            $query->where($key, $val);
        }
        return $query->first();
    }

    public static function getKasusTambahan($where = array()) {
        $query = DB::table('trx_pe_kasus_tambahan');
        $query->select('id_trx_pe', 'lokasi', 'keterangan', 'tgl_pe', 'jml_kasus');
        foreach ($where as $key => $val) {
            $query->where($key, $val);
        }
        return $query->get();
    }

    public static function getPelaksana($where = array()) {
        $query = DB::table('trx_pe_pelaksana');
        $query->select('id_trx_pe', 'nama_pelaksana');
        foreach ($where as $key => $val) {
            $query->where($key, $val);
        }
        return $query->get();
    }

    public static function getPeAfp($where = array()) {
        $query = DB::table('view_pe_afp');
        foreach ($where as $key => $val) {
            $query->where($key, $val);
        }
        return $query->get();
    }

    public static function postDelete($id) {
        $data['deleted_at'] = date('Y-m-d H:i:s');
        $data['deleted_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
        DB::table('trx_pe_riwayat_kontak')->where('id_trx_pe', $id)->update($data);
        DB::table('trx_pe_riwayat_pengobatan')->where('id_trx_pe', $id)->update($data);
        DB::table('trx_pe_kasus_tambahan')->where('id_trx_pe', $id)->update($data);
        return DB::table('trx_pe')->where('id', $id)->update($data);
    }
}

<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class TxGejalaFamily extends Model {
    public static function insert($where = array(), $data) {
        // truncate data
        $query = DB::table('trx_gejala_family')->where($where)->delete();
        // insert data
        return DB::table('trx_gejala_family')->insert($data);
    }

    public static function getData($where = array()) {
        $query = DB::table('trx_gejala_family AS a');
        $query->select('c.id', 'c.name', 'a.tgl_kejadian', 'a.val_gejala');
        $query->join('trx_klinis_family AS b', 'a.id_trx_klinis_family', '=', 'b.id');
        $query->join('mst_gejala AS c', 'a.id_gejala', '=', 'c.id');
        foreach ($where as $key => $val) {
            $query->where($key, $val);
        }
        return $query->get();
    }
}

<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model {
	protected $table    = 'role_users';
	protected $fillable = ['user_id', 'role_id', 'faskes_id', 'penanggung_jawab_1', 'penanggung_jawab_2', 'created_by'];

	public static function getRoleUser($where = array()) {
		$query = DB::table('view_role_user AS a');
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		return $query->get();
	}
}

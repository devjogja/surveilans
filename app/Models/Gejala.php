<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Gejala extends Model {
    public static function getData($where = array()) {
        $dt = DB::table('mst_gejala AS a');
        $dt->select('a.id', 'a.id_case', 'a.group_type', 'a.name');
        $dt->where('a.deleted_at', NULL);
        foreach ($where as $key => $val) {
            $dt->where($key, $val);
        }
        return $dt->get();
    }
}

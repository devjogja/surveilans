<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Occupation extends Model {
	
    public static function getData($where = array()) {
        $dt = DB::table('mst_occupation AS a');
        $dt->select('a.id', 'a.name');
        $dt->where('a.deleted_at', NULL);
        return $dt->get();
    }
}

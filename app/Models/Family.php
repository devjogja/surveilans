<?php

namespace App\Models;

use Auth;
use DB;
use Illuminate\Database\Eloquent\Model;
use Sentinel;

class Family extends Model {
    public static function insert($data) {
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
        $data['updated_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
        return DB::table('ref_family')->insertGetId($data);
    }

    public static function pull($where = array(), $data) {
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['updated_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
        $query              = DB::table('ref_family');
        foreach ($where as $key => $val) {
            $query->where($key, $val);
        }
        return $query->update($data);
    }
}

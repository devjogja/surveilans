<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Afp_spesimen extends Model
{
	public static function getData()
	{
		$q = "
			SELECT
			a.id AS id_spesimen,
			a.id_trx_case,
			a.jenis_spesimen,
			CASE
			WHEN a.jenis_spesimen = 1 THEN 'Spesimen Tool I'
			WHEN a.jenis_spesimen = 2 THEN 'Spesimen Tool II'
			WHEN a.jenis_spesimen = 3 THEN 'Spesimen Tool III'
			WHEN a.jenis_spesimen = 4 THEN 'Spesimen Tool IV'
			WHEN a.jenis_spesimen = 5 THEN 'Spesimen Tool V'
			ELSE NULL
			END AS jenis_spesimen_txt,
			a.jenis_pemeriksaan,
			CASE
			WHEN a.jenis_pemeriksaan = 1 THEN 'Isolasi Virus'
			WHEN a.jenis_pemeriksaan = 2 THEN 'ITD'
			WHEN a.jenis_pemeriksaan = 3 THEN 'Sequencing'
			ELSE NULL
			END AS jenis_pemeriksaan_txt,
			DATE_FORMAT(a.tgl_ambil_spesimen,'%d-%m-%Y') AS tgl_ambil_spesimen,
			a.hasil AS hasil_txt,
			DATE_FORMAT(a.tgl_kirim_kab,'%d-%m-%Y') AS tgl_kirim_kab,
			DATE_FORMAT(a.tgl_kirim_prov,'%d-%m-%Y') AS tgl_kirim_prov,
			DATE_FORMAT(a.tgl_kirim_lab,'%d-%m-%Y') AS tgl_kirim_lab,
			a.id_trx_pe
			FROM trx_spesimen a
			WHERE a.case = 'afp' AND a.deleted_at IS NULL
		";
		return $q;
	}
}
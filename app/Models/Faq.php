<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB, Sentinel, Auth;

class Faq extends Model
{
  static function insert($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = (Sentinel::check())?Sentinel::check()->id : Auth::user()->id;
    $data['updated_by'] = (Sentinel::check())?Sentinel::check()->id : Auth::user()->id;
    $query = DB::table('faq')->insert($data);
    // return DB::table('faq')->insertGetId($data);
    return $query;
  }

  static function getDataFaq($where=array()){
		$query = DB::table('faq');
		foreach ($where as $key => $val) {
			if (!empty($val)) {
				$query->where($key, $val);
			}
		}
		return $query->get();
	}

  static function getData(){
    $query = DB::table('faq AS a');
    $query->select('a.id','a.pertanyaan','a.jawaban')->whereNull('a.deleted_at');
    return $query->get();
  }

  static function pull($where=array(), $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = (Sentinel::check())?Sentinel::check()->id : Auth::user()->id;
    $query = DB::table('faq');
    foreach ($where as $key => $val) {
      if(!empty($val)){
        $query->where($key, $val);
      }
    }
    return $query->update($data);
  }

  static function postDelete($id){
    $data['deleted_at'] = date('Y-m-d H:i:s');
    $data['deleted_by'] = (Sentinel::check())?Sentinel::check()->id : Auth::user()->id;
    return DB::table('faq')->where('id',$id)->update($data);
  }
}

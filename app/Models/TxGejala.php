<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class TxGejala extends Model {
    public static function insertData($where = array(), $data) {
        // truncate data
        $query = DB::table('trx_gejala')->where($where)->delete();
        // insert data
        return DB::table('trx_gejala')->insert($data);
    }

    public static function getData($where = array()) {
        $query = DB::table('trx_gejala AS a');
        $query->select('b.id AS id_trx_klinis','c.id', 'c.name', 'a.tgl_kejadian','a.other_gejala','a.kelumpuhan','a.gangguan_raba');
        $query->join('trx_klinis AS b', 'a.id_trx_klinis', '=', 'b.id');
        $query->leftJoin('mst_gejala AS c', 'a.id_gejala', '=', 'c.id');
        foreach ($where as $key => $val) {
            $query->where($key, $val);
        }
        return $query->get();
    }
}

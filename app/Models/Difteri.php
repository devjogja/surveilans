<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Difteri extends Model
{
    public static function qq()
    {
        $q = "(
			SELECT
			a.id AS id_trx_case,
			a.no_epid,
			a.no_epid_klb,
			b.name AS name_pasien,
			b.umur_hari,
			b.umur_bln,
			b.umur_thn,
			CONCAT(b.alamat,' ',c.name,', ',d.name,', ',e.name,', ',f.name) AS full_address,
			CASE
			WHEN g.keadaan_akhir = 1 THEN 'Hidup'
			WHEN g.keadaan_akhir = 2 THEN 'Meninggal'
			ELSE ''
			END AS keadaan_akhir_txt,
			CASE
			WHEN a.klasifikasi_final = 1 THEN 'Konfirm'
			WHEN a.klasifikasi_final = 2 THEN 'Probable'
			WHEN a.klasifikasi_final = 3 THEN 'Negatif'
			ELSE NULL
			END AS klasifikasi_final_txt,
			a.id_faskes,
			a.id_role,
			CASE
			WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',h.name)
			WHEN a.id_role = 2 THEN i.name
			WHEN a.id_role = 6 THEN CONCAT('KABUPATEN ',j.name)
			ELSE NULL
			END AS name_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_kecamatan
			ELSE NULL
			END AS code_kecamatan_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_kabupaten
			WHEN a.id_role = 2 THEN i.code_kabupaten
			WHEN a.id_role = 6 THEN j.code
			ELSE NULL
			END AS code_kabupaten_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_provinsi
			WHEN a.id_role = 2 THEN i.code_provinsi
			WHEN a.id_role = 6 THEN j.code_provinsi
			ELSE NULL
			END AS code_provinsi_faskes,
			a.jenis_input,
			g.status_kasus,
			k.id AS id_pe,
			CASE
			WHEN k.id IS NULL THEN 'PE'
			ELSE 'Sudah PE'
			END AS 'pe_text',
			a.id_faskes_old,
			a.id_role_old,
			DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
			DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
			CASE
			WHEN a.jenis_input = 1 THEN 'Web'
			WHEN a.jenis_input = 2 THEN 'Android'
			WHEN a.jenis_input = 3 THEN 'Import'
			END AS jenis_input_text,
			CASE
			WHEN g.status_kasus = 1 THEN 'Index'
			WHEN g.status_kasus = 2 THEN 'Bukan'
			END AS status_kasus_txt,
			b.code_kelurahan AS code_kelurahan_pasien,
			g.tgl_mulai_demam AS tgl_sakit_date,
			DATE_FORMAT(g.tgl_mulai_demam, '%Y') AS thn_sakit,
			b.code_kecamatan AS code_kecamatan_pasien
			FROM
			trx_case a
			JOIN ref_pasien b ON a.id_pasien = b.id AND b.deleted_at IS NULL
			LEFT JOIN mst_kelurahan c ON b.code_kelurahan = c.code
			LEFT JOIN mst_kecamatan d ON c.code_kecamatan = d.code
			LEFT JOIN mst_kabupaten e ON d.code_kabupaten = e.code
			LEFT JOIN mst_provinsi f ON e.code_provinsi = f.code
			JOIN trx_klinis g ON a.id=g.id_trx_case
			LEFT JOIN (
				" . Puskesmas::qq() . "
			) AS h ON a.id_faskes = h.id AND a.id_role='1'
			LEFT JOIN (
				" . Rumahsakit::qq() . "
			) i ON a.id_faskes = i.id AND a.id_role='2'
			LEFT JOIN (
				" . Kabupaten::qq() . "
			) j ON a.id_faskes = j.code AND a.id_role='6'
			LEFT JOIN trx_pe k ON a.id=k.id_trx_case AND k.deleted_at IS NULL
			WHERE
			a.id_case = '3' AND a.deleted_at IS NULL
			GROUP BY a.id) AS a
			";
        return $q;
    }

    public static function getData()
    {
        $q = "
			(SELECT
				a.id AS id_trx_case,
				a.no_epid,
				a._no_epid,
				a.no_epid_lama,
				a.no_rm,
				b.name AS name_pasien,
				b.nik,
				b.umur_hari,
				b.umur_bln,
				b.umur_thn,
				b.agama,
				b.alamat,
				CONCAT(b.alamat,' ',c.name,', ',d.name,', ',e.name,', ',f.name) AS full_address,
				c.name AS name_kelurahan_pasien,
				d.name AS name_kecamatan_pasien,
				e.name AS name_kabupaten_pasien,
				f.name AS name_provinsi_pasien,
				DATE_FORMAT(g.tgl_mulai_demam, '%d-%m-%Y') AS tgl_mulai_demam,
				CASE
				WHEN g.jml_imunisasi_dpt = 7 THEN 'Tidak'
				WHEN g.jml_imunisasi_dpt = 8 THEN 'Tidak tahu'
				WHEN g.jml_imunisasi_dpt = 9 THEN 'Belum pernah'
				WHEN g.jml_imunisasi_dpt = NULL THEN '-'
				ELSE concat (g.jml_imunisasi_dpt, ' x')
				END AS jml_imunisasi_dpt_txt,
				date_format (g.tgl_imunisasi_difteri, '%d-%m-%Y') AS tgl_imunisasi_difteri,
				date_format (g.tgl_pelacakan, '%d-%m-%Y') AS tgl_pelacakan,
				g.gejala_lain AS gejala_lain,
				g.jml_kontak AS jml_kontak,
				g.diambil_spec_kontak AS diambil_spec_kontak,
				g.positif_kontak AS positif_kontak,
				CASE
				WHEN g.keadaan_akhir = 1 THEN 'Hidup'
				WHEN g.keadaan_akhir = 2 THEN 'Meninggal'
				ELSE ''
				END AS keadaan_akhir_txt,
				CASE
				WHEN a.klasifikasi_final = 1 THEN 'Konfirm'
				WHEN a.klasifikasi_final = 2 THEN 'Probable'
				WHEN a.klasifikasi_final = 3 THEN 'Negatif'
				ELSE NULL
				END AS klasifikasi_final_txt,
				a.klasifikasi_final,
				a.id_faskes,
				a.id_role,
				CASE
				WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',h.name)
				WHEN a.id_role = 2 THEN i.name
				WHEN a.id_role = 6 THEN CONCAT('KABUPATEN ',j.name)
				ELSE NULL
				END AS name_faskes,
				CASE
				WHEN a.id_role = 1 THEN h.name_kecamatan
				ELSE NULL
				END AS name_kecamatan_faskes,
				CASE
				WHEN a.id_role = 1 THEN h.code_kecamatan
				ELSE NULL
				END AS code_kecamatan_faskes,
				CASE
				WHEN a.id_role = 1 THEN h.code_kabupaten
				WHEN a.id_role = 2 THEN i.code_kabupaten
				WHEN a.id_role = 6 THEN j.code
				ELSE NULL
				END AS code_kabupaten_faskes,
				CASE
				WHEN a.id_role = 1 THEN h.name_kabupaten
				WHEN a.id_role = 2 THEN i.name_kabupaten
				WHEN a.id_role = 6 THEN j.name
				ELSE NULL
				END AS name_kabupaten_faskes,
				CASE
				WHEN a.id_role = 1 THEN h.code_provinsi
				WHEN a.id_role = 2 THEN i.code_provinsi
				WHEN a.id_role = 6 THEN j.code_provinsi
				ELSE NULL
				END AS code_provinsi_faskes,
				CASE
				WHEN a.id_role = 1 THEN h.name_provinsi
				WHEN a.id_role = 2 THEN i.name_provinsi
				WHEN a.id_role = 6 THEN j.name_provinsi
				ELSE NULL
				END AS name_provinsi_faskes,
				CASE
				WHEN a.id_role = 1 THEN h.code_faskes
				WHEN a.id_role = 2 THEN i.code_faskes
				ELSE NULL
				END AS code_faskes,
				a.jenis_input,
				g.hot_case,
				CASE
				WHEN g.hot_case = 1 THEN 'Hot Case'
				WHEN g.hot_case = 2 THEN 'Bukan Hot Case'
				END AS hot_case_txt,
				CASE
				WHEN a.jenis_input = 1 THEN 'Web'
				WHEN a.jenis_input = 2 THEN 'Android'
				WHEN a.jenis_input = 3 THEN 'Import'
				END AS jenis_input_text,
				g.status_kasus,
				k.id AS id_pe,
				CASE
				WHEN k.id IS NULL THEN 'PE'
				ELSE 'Sudah PE'
				END AS 'pe_text',
				b.code_kelurahan AS code_kelurahan_pasien,
				b.code_kecamatan AS code_kecamatan_pasien,
				b.code_kabupaten AS code_kabupaten_pasien,
				b.code_provinsi AS code_provinsi_pasien,
				g.tgl_mulai_demam AS tgl_sakit_date,
				DATE_FORMAT(g.tgl_mulai_demam, '%Y') AS thn_sakit,
				a.id_faskes_old,
				a.id_role_old,
				DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
				DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
				b.jenis_kelamin,
				CASE
				WHEN b.jenis_kelamin = 'L' THEN 'Laki-laki'
				WHEN b.jenis_kelamin = 'P' THEN 'Perempuan'
				WHEN b.jenis_kelamin = 'T' THEN 'Tidak Jelas'
				ELSE ''
				END AS jenis_kelamin_txt,
				g.jenis_kasus,
				n.name AS nama_ortu,
				DATE_FORMAT(b.tgl_lahir, '%d-%m-%Y') AS tgl_lahir,
				g.jml_imunisasi_dpt AS jml_imunisasi
				FROM
				trx_case a
				JOIN ref_pasien b ON a.id_pasien = b.id AND b.deleted_at IS NULL
				LEFT JOIN ref_family n ON b.id = n.id_pasien
				LEFT JOIN mst_kelurahan c ON b.code_kelurahan = c.code
				LEFT JOIN mst_kecamatan d ON c.code_kecamatan = d.code
				LEFT JOIN mst_kabupaten e ON d.code_kabupaten = e.code
				LEFT JOIN mst_provinsi f ON e.code_provinsi = f.code
				JOIN trx_klinis g ON a.id=g.id_trx_case
				LEFT JOIN (
					" . Puskesmas::qq() . "
				) AS h ON a.id_faskes = h.id AND a.id_role='1'
				LEFT JOIN (
					" . Rumahsakit::qq() . "
				) i ON a.id_faskes = i.id AND a.id_role='2'
				LEFT JOIN (
					" . Kabupaten::qq() . "
				) j ON a.id_faskes = j.code AND a.id_role='6'
				LEFT JOIN trx_pe k ON a.id=k.id_trx_case AND k.deleted_at IS NULL
				WHERE
				a.id_case = 3 AND a.deleted_at IS NULL
				GROUP BY a.id) AS a
				";
        return $q;
    }
}

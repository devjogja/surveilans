<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Role extends Model {
    protected $table    = 'roles';
    protected $fillable = ['id', 'name', 'description', 'code', 'created_by', 'slug', 'permissions'];

    public static function getData($where = array()) {
        $role = DB::table('roles AS a');
        $role->select('a.id', 'a.name', 'a.description', 'a.code', 'a.slug', 'a.permissions');
        $role->where('a.deleted_at', NULL);
        foreach ($where as $key => $val) {
            $role->where($key, $val);
        }
        return $role->get();
    }

    public static function getUpdate($where = array(), $data = array()) {
        $role = DB::table('roles');
        $role->where('deleted_at', NULL);
        foreach ($where as $key => $val) {
            $role->where($key, $val);
        }
        return $role->update($data);
    }
}

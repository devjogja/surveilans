<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewWilayahKerjaPuskesmas extends Model
{
	protected $table = 'view_wilayah_kerja_puskesmas';
}

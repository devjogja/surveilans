<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Difteri_spesimen extends Model
{
	public static function getData()
	{
		$q = "
			SELECT
			a.id AS id_spesimen,
			a.id_trx_case,
			a.jenis_spesimen,
  			CASE
  			WHEN a.jenis_spesimen = 1 THEN 'Tenggorokan'
  			WHEN a.jenis_spesimen = 2 THEN 'Hidung'
  			ELSE NULL
  			END AS jenis_spesimen_txt,
			a.jenis_pemeriksaan,
  			CASE
  			WHEN a.jenis_pemeriksaan = 1 THEN 'Kultur'
  			WHEN a.jenis_pemeriksaan = 2 THEN 'Mikroskop'
  			ELSE NULL
  			END AS jenis_pemeriksaan_txt,
  			a.kode_spesimen,
			DATE_FORMAT(a.tgl_ambil_spesimen,'%d-%m-%Y') AS tgl_ambil_spesimen,
  			CASE
  			WHEN a.hasil = 1 THEN 'Positif'
  			WHEN a.hasil = 2 THEN 'Negatif'
  			ELSE NULL
  			END AS hasil_txt,
			a.id_trx_pe
			FROM trx_spesimen a
			WHERE a.case = 'difteri' AND a.deleted_at IS NULL
		";
		return $q;
	}
}
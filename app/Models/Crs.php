<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Crs extends Model
{
    public static function qq()
    {
        $q = "(
			SELECT
			a.id AS id_trx_case,
			a.no_epid,
			b.name AS name_pasien,
			b.umur_hari,
			b.umur_bln,
			b.umur_thn,
			CONCAT(b.alamat,' ',c.name,', ',d.name,', ',e.name,', ',f.name) AS full_address,
			CASE
			WHEN g.keadaan_akhir = 1 THEN 'Hidup'
			WHEN g.keadaan_akhir = 2 THEN 'Meninggal'
			ELSE ''
			END AS keadaan_akhir_txt,
			CASE
			WHEN (a.klasifikasi_final = 1) THEN 'CRS pasti(Lab Positif)'
			WHEN (a.klasifikasi_final = 2) THEN 'CRS Klinis'
			WHEN (a.klasifikasi_final = 3) THEN 'Bukan CRS'
			WHEN (a.klasifikasi_final = 4) THEN 'Suspek CRS'
			ELSE NULL
			END AS klasifikasi_final_txt,
			a.id_faskes,
			a.id_role,
			CASE
			WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',h.name)
			WHEN a.id_role = 2 THEN i.name
			WHEN a.id_role = 6 THEN CONCAT('KABUPATEN ',j.name)
			ELSE NULL
			END AS name_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_kecamatan
			ELSE NULL
			END AS code_kecamatan_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_kabupaten
			WHEN a.id_role = 2 THEN i.code_kabupaten
			WHEN a.id_role = 6 THEN j.code
			ELSE NULL
			END AS code_kabupaten_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_provinsi
			WHEN a.id_role = 2 THEN i.code_provinsi
			WHEN a.id_role = 6 THEN j.code_provinsi
			ELSE NULL
			END AS code_provinsi_faskes,
			a.jenis_input,
			g.status_kasus,
			k.id AS id_pe,
			CASE
			WHEN k.id IS NULL THEN 'PE'
			ELSE 'Sudah PE'
			END AS 'pe_text',
			a.id_faskes_old,
			a.id_role_old,
			DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
			DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
			CASE
			WHEN a.jenis_input = 1 THEN 'Web'
			WHEN a.jenis_input = 2 THEN 'Android'
			WHEN a.jenis_input = 3 THEN 'Import'
			END AS jenis_input_text,
			CASE
			WHEN g.status_kasus = 1 THEN 'Index'
			WHEN g.status_kasus = 2 THEN 'Bukan'
			END AS status_kasus_txt,
			b.code_kelurahan AS code_kelurahan_pasien,
			g.tgl_periksa AS tgl_sakit_date,
			DATE_FORMAT(g.tgl_periksa, '%Y') AS thn_sakit,
			b.code_kecamatan AS code_kecamatan_pasien
			FROM
			trx_case a
			JOIN ref_pasien b ON a.id_pasien = b.id AND b.deleted_at IS NULL
			LEFT JOIN mst_kelurahan c ON b.code_kelurahan = c.code
			LEFT JOIN mst_kecamatan d ON c.code_kecamatan = d.code
			LEFT JOIN mst_kabupaten e ON d.code_kabupaten = e.code
			LEFT JOIN mst_provinsi f ON e.code_provinsi = f.code
			JOIN trx_klinis g ON a.id=g.id_trx_case
			LEFT JOIN (
				" . Puskesmas::qq() . "
			) AS h ON a.id_faskes = h.id AND a.id_role='1'
			LEFT JOIN (
				" . Rumahsakit::qq() . "
			) i ON a.id_faskes = i.id AND a.id_role='2'
			LEFT JOIN (
				" . Kabupaten::qq() . "
			) j ON a.id_faskes = j.code AND a.id_role='6'
			LEFT JOIN trx_pe k ON a.id=k.id_trx_case AND k.deleted_at IS NULL
			WHERE
			a.id_case = 5 AND a.deleted_at IS NULL
			GROUP BY a.id) AS a
			";
        return $q;
    }

    public static function getData()
    {
        $q = "
			(SELECT
			a.id AS id_trx_case,
			a.no_epid,
			a.no_epid_lama,
			a.no_rm,
			b.name AS name_pasien,
			b.nik,
			b.agama,
			DATE_FORMAT(b.tgl_lahir, '%d-%m-%Y') AS tgl_lahir,
			b.umur_hari,
			b.umur_bln,
			b.umur_thn,
			b.alamat,
			CONCAT(b.alamat,' ',c.name,', ',d.name,', ',e.name,', ',f.name) AS full_address,
			c.name AS name_kelurahan_pasien,
			d.name AS name_kecamatan_pasien,
			e.name AS name_kabupaten_pasien,
			f.name AS name_provinsi_pasien,
			CASE
			WHEN g.keadaan_akhir = 1 THEN 'Hidup'
			WHEN g.keadaan_akhir = 2 THEN 'Meninggal'
			ELSE ''
			END AS keadaan_akhir_txt,
			CASE
			WHEN (a.klasifikasi_final = 1) THEN 'CRS pasti(Lab Positif)'
			WHEN (a.klasifikasi_final = 2) THEN 'CRS Klinis'
			WHEN (a.klasifikasi_final = 3) THEN 'Bukan CRS'
			WHEN (a.klasifikasi_final = 4) THEN 'Suspek CRS'
			ELSE NULL
			END AS klasifikasi_final_txt,
			a.klasifikasi_final,
			a.id_faskes,
			a.id_role,
			CASE
			WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',h.name)
			WHEN a.id_role = 2 THEN i.name
			WHEN a.id_role = 6 THEN CONCAT('KABUPATEN ',j.name)
			ELSE NULL
			END AS name_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_kecamatan
			ELSE NULL
			END AS code_kecamatan_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.name_kecamatan
			ELSE NULL
			END AS name_kecamatan_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_kabupaten
			WHEN a.id_role = 2 THEN i.code_kabupaten
			WHEN a.id_role = 6 THEN j.code
			ELSE NULL
			END AS code_kabupaten_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.name_kabupaten
			WHEN a.id_role = 2 THEN i.name_kabupaten
			WHEN a.id_role = 6 THEN j.name
			ELSE NULL
			END AS name_kabupaten_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_provinsi
			WHEN a.id_role = 2 THEN i.code_provinsi
			WHEN a.id_role = 6 THEN j.code_provinsi
			ELSE NULL
			END AS code_provinsi_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.name_provinsi
			WHEN a.id_role = 2 THEN i.name_provinsi
			WHEN a.id_role = 6 THEN j.name_provinsi
			ELSE NULL
			END AS name_provinsi_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_faskes
			WHEN a.id_role = 2 THEN i.code_faskes
			ELSE NULL
			END AS code_faskes,
			a.jenis_input,
			CASE
			WHEN a.jenis_input = 1 THEN 'Web'
			WHEN a.jenis_input = 2 THEN 'Android'
			WHEN a.jenis_input = 3 THEN 'Import'
			END AS jenis_input_text,
			g.status_kasus,
			k.id AS id_pe,
			CASE
			WHEN k.id IS NULL THEN 'PE'
			ELSE 'Sudah PE'
			END AS 'pe_text',
			b.code_kelurahan AS code_kelurahan_pasien,
			b.code_kecamatan AS code_kecamatan_pasien,
			b.code_kabupaten AS code_kabupaten_pasien,
			b.code_provinsi AS code_provinsi_pasien,
			g.tgl_periksa AS tgl_sakit_date,
			DATE_FORMAT(g.tgl_periksa, '%Y') AS thn_sakit,
			a.id_faskes_old,
			a.id_role_old,
			DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
			DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
			b.jenis_kelamin,
			CASE
			WHEN b.jenis_kelamin = 'L' THEN 'Laki-laki'
			WHEN b.jenis_kelamin = 'P' THEN 'Perempuan'
			WHEN b.jenis_kelamin = 'T' THEN 'Tidak Jelas'
			ELSE ''
			END AS jenis_kelamin_txt,
			n.name AS nama_ortu,
			b.tempat_lahir AS tempat_lahir,
			n.no_telp AS no_telp_ortu,
			b.umur_kehamilan_bayi_lahir,
			b.berat_badan_bayi_lahir,
			DATE_FORMAT(g.tgl_laporan_diterima,'%d-%m-%Y') AS tgl_laporan_diterima,
			DATE_FORMAT(g.tgl_pelacakan,'%d-%m-%Y') AS tgl_pelacakan,
			GROUP_CONCAT(CONCAT(m.id_gejala,'_',m.val_gejala) SEPARATOR '|') AS gejala,
			g.nama_dokter_pemeriksa,
			DATE_FORMAT(g.tgl_periksa,'%d-%m-%Y') AS tgl_periksa,
			g.penyebab_meninggal,
			g.jenis_kasus
			FROM
			trx_case a
			JOIN ref_pasien b ON a.id_pasien = b.id AND b.deleted_at IS NULL
			LEFT JOIN ref_family n ON b.id = n.id_pasien
			LEFT JOIN mst_kelurahan c ON b.code_kelurahan = c.code
			LEFT JOIN mst_kecamatan d ON c.code_kecamatan = d.code
			LEFT JOIN mst_kabupaten e ON d.code_kabupaten = e.code
			LEFT JOIN mst_provinsi f ON e.code_provinsi = f.code
			JOIN trx_klinis g ON a.id=g.id_trx_case
			LEFT JOIN (
				" . Puskesmas::qq() . "
			) AS h ON a.id_faskes = h.id AND a.id_role='1'
			LEFT JOIN (
				" . Rumahsakit::qq() . "
			) i ON a.id_faskes = i.id AND a.id_role='2'
			LEFT JOIN (
				" . Kabupaten::qq() . "
			) j ON a.id_faskes = j.code AND a.id_role='6'
			LEFT JOIN trx_pe k ON a.id=k.id_trx_case AND k.deleted_at IS NULL
			JOIN (
				" . Pelapor::qq() . "
			) l ON a.id = l.id_trx_case
			LEFT JOIN (
				SELECT
				a.id_trx_klinis,
				a.val_gejala,
				c.name,
				c.id AS id_gejala,
				a.tgl_kejadian,
				c.group_type
				FROM trx_gejala a
				JOIN mst_gejala c ON a.id_gejala = c.id AND c.id_case = 5
				WHERE a.val_gejala <> ''
			) m ON g.id = m.id_trx_klinis
			WHERE
			a.id_case = 5 AND a.deleted_at IS NULL
			GROUP BY a.id) AS a
			";
        return $q;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB, Sentinel, Auth;

class Logging extends Model
{
    public static function insert($type)
    {
    	$user = (Sentinel::check())?Sentinel::check()->id : Auth::user()->id;
    	$dt = array(
    			'user_id'=>$user,
    			'type'=>$type,
    			'created_at'=>date('Y-m-d H:i:s')
    		);
        return DB::table('logging')->insert($dt);
    }
}

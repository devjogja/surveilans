<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tetanus extends Model
{
	public static function qq()
    {
        $q = "(
			SELECT
			a.id AS id_trx_case,
			a.no_epid,
			a.no_epid_klb,
			b.name AS name_pasien,
			b.umur_hari,
			b.umur_bln,
			b.umur_thn,
			CONCAT(b.alamat,' ',c.name,', ',d.name,', ',e.name,', ',f.name) AS full_address,
			CASE
			WHEN g.keadaan_akhir = 1 THEN 'Hidup'
			WHEN g.keadaan_akhir = 2 THEN 'Meninggal'
			ELSE ''
			END AS keadaan_akhir_txt,
			CASE
			WHEN (a.klasifikasi_final = 1) THEN 'Konfirm TN'
			WHEN (a.klasifikasi_final = 2) THEN 'Tersangka TN'
			WHEN (a.klasifikasi_final = 3) THEN 'Bukan TN'
			ELSE NULL
			END AS klasifikasi_final_txt,
			a.id_faskes,
			a.id_role,
			CASE
			WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',h.name)
			WHEN a.id_role = 2 THEN i.name
			WHEN a.id_role = 6 THEN CONCAT('KABUPATEN ',j.name)
			ELSE NULL
			END AS name_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_kecamatan
			ELSE NULL
			END AS code_kecamatan_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_kabupaten
			WHEN a.id_role = 2 THEN i.code_kabupaten
			WHEN a.id_role = 6 THEN j.code
			ELSE NULL
			END AS code_kabupaten_faskes,
			CASE
			WHEN a.id_role = 1 THEN h.code_provinsi
			WHEN a.id_role = 2 THEN i.code_provinsi
			WHEN a.id_role = 6 THEN j.code_provinsi
			ELSE NULL
			END AS code_provinsi_faskes,
			a.jenis_input,
			g.status_kasus,
			k.id AS id_pe,
			CASE
			WHEN k.id IS NULL THEN 'PE'
			ELSE 'Sudah PE'
			END AS 'pe_text',
			a.id_faskes_old,
			a.id_role_old,
			DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
			DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
			CASE
			WHEN a.jenis_input = 1 THEN 'Web'
			WHEN a.jenis_input = 2 THEN 'Android'
			WHEN a.jenis_input = 3 THEN 'Import'
			END AS jenis_input_text,
			b.code_kelurahan AS code_kelurahan_pasien,
			g.tgl_mulai_sakit AS tgl_sakit_date,
			DATE_FORMAT(g.tgl_mulai_sakit, '%Y') AS thn_sakit,
			b.code_kecamatan AS code_kecamatan_pasien
			FROM
			trx_case a
			JOIN ref_pasien b ON a.id_pasien = b.id AND b.deleted_at IS NULL
			LEFT JOIN mst_kelurahan c ON b.code_kelurahan = c.code
			LEFT JOIN mst_kecamatan d ON c.code_kecamatan = d.code
			LEFT JOIN mst_kabupaten e ON d.code_kabupaten = e.code
			LEFT JOIN mst_provinsi f ON e.code_provinsi = f.code
			JOIN trx_klinis g ON a.id=g.id_trx_case
			LEFT JOIN (
				" . Puskesmas::qq() . "
			) AS h ON a.id_faskes = h.id AND a.id_role='1'
			LEFT JOIN (
				" . Rumahsakit::qq() . "
			) i ON a.id_faskes = i.id AND a.id_role='2'
			LEFT JOIN (
				" . Kabupaten::qq() . "
			) j ON a.id_faskes = j.code AND a.id_role='6'
			LEFT JOIN trx_pe k ON a.id=k.id_trx_case AND k.deleted_at IS NULL
			WHERE
			a.id_case = 4 AND a.deleted_at IS NULL
			GROUP BY a.id) AS a
			";
        return $q;
    }

    public static function getData()
    {
        $q = "
        	(SELECT
				a.id AS id_trx_case,
				a.no_epid,
				a.no_epid_lama,
				b.name AS name_pasien,
				a.no_rm,
				b.nik,
				b.umur_hari,
				b.umur_bln,
				b.umur_thn,
				b.agama,
				b.alamat,
				CONCAT(b.alamat,' ',c.name,', ',d.name,', ',e.name,', ',f.name) AS full_address,
				c.name AS name_kelurahan_pasien,
				d.name AS name_kecamatan_pasien,
				e.name AS name_kabupaten_pasien,
				f.name AS name_provinsi_pasien,
				CASE
				WHEN g.keadaan_akhir = 1 THEN 'Hidup'
				WHEN g.keadaan_akhir = 2 THEN 'Meninggal'
				ELSE NULL
				END AS keadaan_akhir_txt,
				CASE
				WHEN (a.klasifikasi_final = 1) THEN 'Konfirm TN'
				WHEN (a.klasifikasi_final = 2) THEN 'Tersangka TN'
				WHEN (a.klasifikasi_final = 3) THEN 'Bukan TN'
				ELSE NULL
				END AS klasifikasi_final_txt,
				a.klasifikasi_final,
				a.id_faskes,
				a.id_role,
				CASE
				WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',h.name)
				WHEN a.id_role = 2 THEN i.name
				WHEN a.id_role = 6 THEN CONCAT('KABUPATEN ',j.name)
				ELSE NULL
				END AS name_faskes,
				CASE
				WHEN a.id_role = 1 THEN h.name_kecamatan
				ELSE NULL
				END AS name_kecamatan_faskes,
				CASE
				WHEN a.id_role = 1 THEN h.code_kecamatan
				ELSE NULL
				END AS code_kecamatan_faskes,
				CASE
				WHEN a.id_role = 1 THEN h.code_kabupaten
				WHEN a.id_role = 2 THEN i.code_kabupaten
				WHEN a.id_role = 6 THEN j.code
				ELSE NULL
				END AS code_kabupaten_faskes,
				CASE
				WHEN a.id_role = 1 THEN h.name_kabupaten
				WHEN a.id_role = 2 THEN i.name_kabupaten
				WHEN a.id_role = 6 THEN j.name
				ELSE NULL
				END AS name_kabupaten_faskes,
				CASE
				WHEN a.id_role = 1 THEN h.code_provinsi
				WHEN a.id_role = 2 THEN i.code_provinsi
				WHEN a.id_role = 6 THEN j.code_provinsi
				ELSE NULL
				END AS code_provinsi_faskes,
				CASE
				WHEN a.id_role = 1 THEN h.name_provinsi
				WHEN a.id_role = 2 THEN i.name_provinsi
				WHEN a.id_role = 6 THEN j.name_provinsi
				ELSE NULL
				END AS name_provinsi_faskes,
				CASE
				WHEN a.id_role = 1 THEN h.code_faskes
				WHEN a.id_role = 2 THEN i.code_faskes
				ELSE NULL
				END AS code_faskes,
				a.jenis_input,
				CASE
				WHEN a.jenis_input = 1 THEN 'Web'
				WHEN a.jenis_input = 2 THEN 'Android'
				WHEN a.jenis_input = 3 THEN 'Import'
				END AS jenis_input_text,
				g.status_kasus,
				k.id AS id_pe,
				CASE
				WHEN k.id IS NULL THEN 'PE'
				ELSE 'Sudah PE'
				END AS 'pe_text',
				b.code_kelurahan AS code_kelurahan_pasien,
				b.code_kecamatan AS code_kecamatan_pasien,
				b.code_kabupaten AS code_kabupaten_pasien,
				b.code_provinsi AS code_provinsi_pasien,
				g.tgl_mulai_sakit AS tgl_sakit_date,
				DATE_FORMAT(g.tgl_mulai_sakit, '%Y') AS thn_sakit,
       date_format(g.tgl_mulai_sakit, '%d-%m-%Y') AS tgl_mulai_sakit,
       date_format(g.tgl_laporan_diterima, '%d-%m-%Y') AS tgl_laporan_diterima,
       date_format(g.tgl_pelacakan, '%d-%m-%Y') AS tgl_pelacakan,
       CASE
           WHEN (g.antenatal_care = 1) THEN 'Dokter'
           WHEN (g.antenatal_care = 2) THEN 'Bidan/Perawat'
           WHEN (g.antenatal_care = 3) THEN 'Dukun'
           WHEN (g.antenatal_care = 4) THEN 'Tidak ANC'
           WHEN (g.antenatal_care = 5) THEN 'Tidak Jelas'
           ELSE NULL
       END AS antenatal_care_txt,
       CASE
           WHEN (g.status_imunisasi_ibu = 1) THEN 'TT2+'
           WHEN (g.status_imunisasi_ibu = 2) THEN 'TT1'
           WHEN (g.status_imunisasi_ibu = 3) THEN 'Tidak Imunisasi'
           WHEN (g.status_imunisasi_ibu = 4) THEN 'Tidak Jelas'
           ELSE NULL
       END AS status_imunisasi_ibu_txt,
       CASE
           WHEN (g.penolong_persalinan = 1) THEN 'Dokter'
           WHEN (g.penolong_persalinan = 2) THEN 'Bidan/Perawat'
           WHEN (g.penolong_persalinan = 3) THEN 'Dukun'
           WHEN (g.penolong_persalinan = 4) THEN 'Tidak Jelas'
           ELSE NULL
       END AS penolong_persalinan_txt,
       CASE
           WHEN (g.alat_pemotong_tali_pusat = 1) THEN 'Gunting'
           WHEN (g.alat_pemotong_tali_pusat = 2) THEN 'Bambu'
           WHEN (g.alat_pemotong_tali_pusat = 3) THEN 'Silet'
           WHEN (g.alat_pemotong_tali_pusat = 4) THEN 'Pisau'
           WHEN (g.alat_pemotong_tali_pusat = 5) THEN 'Lain-lain'
           WHEN (g.alat_pemotong_tali_pusat = 6) THEN 'Tidak Tahu'
           ELSE NULL
       END AS alat_pemotong_tali_pusat_txt,
       CASE
           WHEN (g.perawatan_tali_pusat = 1) THEN 'Alc/Iod'
           WHEN (g.perawatan_tali_pusat = 2) THEN 'Ramuan'
           WHEN (g.perawatan_tali_pusat = 3) THEN 'Lain-lain'
           WHEN (g.perawatan_tali_pusat = 4) THEN 'Tidak Jelas'
           ELSE NULL
       END AS perawatan_tali_pusat_txt,
       CASE
           WHEN (g.rawat_rumah_sakit = 1) THEN 'Ya'
           WHEN (g.rawat_rumah_sakit = 2) THEN 'Tidak'
           WHEN (g.rawat_rumah_sakit = 3) THEN 'Tidak Jelas'
           ELSE NULL
       END AS rawat_rumah_sakit_txt,
				a.id_faskes_old,
				a.id_role_old,
				n.name AS nama_ortu,
				DATE_FORMAT(b.tgl_lahir, '%d-%m-%Y') AS tgl_lahir,
				DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
				DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
				b.jenis_kelamin,
				CASE
				WHEN b.jenis_kelamin = 'L' THEN 'Laki-laki'
				WHEN b.jenis_kelamin = 'P' THEN 'Perempuan'
				WHEN b.jenis_kelamin = 'T' THEN 'Tidak Jelas'
				ELSE ''
				END AS jenis_kelamin_txt,
				g.jenis_kasus,
				g.status_imunisasi_ibu AS jml_imunisasi
			FROM
			trx_case a
			JOIN ref_pasien b ON a.id_pasien = b.id AND b.deleted_at IS NULL
			LEFT JOIN ref_family n ON b.id = n.id_pasien
			LEFT JOIN mst_kelurahan c ON b.code_kelurahan = c.code
			LEFT JOIN mst_kecamatan d ON c.code_kecamatan = d.code
			LEFT JOIN mst_kabupaten e ON d.code_kabupaten = e.code
			LEFT JOIN mst_provinsi f ON e.code_provinsi = f.code
			JOIN trx_klinis g ON a.id=g.id_trx_case
			LEFT JOIN (
				" . Puskesmas::qq() . "
			) AS h ON a.id_faskes = h.id AND a.id_role='1'
			LEFT JOIN (
				" . Rumahsakit::qq() . "
			) i ON a.id_faskes = i.id AND a.id_role='2'
			LEFT JOIN (
				" . Kabupaten::qq() . "
			) j ON a.id_faskes = j.code AND a.id_role='6'
			LEFT JOIN trx_pe k ON a.id=k.id_trx_case AND k.deleted_at IS NULL
			WHERE
				a.id_case = 4 AND a.deleted_at IS NULL
			GROUP BY a.id) AS a
        ";
        return $q;
    }
}

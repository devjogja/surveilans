<?php

namespace App\Models;

use Auth;
use DB;
use Illuminate\Database\Eloquent\Model;
use Sentinel;

class Tx extends Model {
	public static function getData($table, $where=[], $qQuery=[]) {
		$query = DB::table($table);
		if (!empty($where)) {
			foreach ($where as $key => $val) {
				if (!empty($val)) {
					$query->where($key, $val);
				}
			}
		}
		if (!empty($qQuery['whereIn'])) {
			foreach ($qQuery['whereIn'] as $key => $val) {
				if (!empty($val)) {
					$query->whereIn($key,$val);
				}
			}
		}
		if (!empty($qQuery['orWhere'])) {
			$query->orWhere(function($query) use ($qQuery){
				foreach ($qQuery['orWhere']['whereIn'] as $key => $val) {
					$query->whereIn($key,$val);
				}
			});
		}
		if (!empty($qQuery['select'])) {
			$qSelect = [];
			foreach ($qQuery['select'] as $key => $val) {
				if (!empty($val)) {
					$qSelect[] = $val;
				}
			}
			$query->select($qSelect);
		}
		if(!empty($qQuery['join'])){
			foreach ($qQuery['join'] as $key => $val) {
				$query->join($val[0],$val[1],'=',$val[2]);
			}
		}
		if(!empty($qQuery['leftJoin'])){
			foreach ($qQuery['leftJoin'] as $key => $val) {
				$query->leftJoin($val[0],$val[1],'=',$val[2]);
			}
		}
		if (!empty($qQuery['pagination']['limit'])) {
			$query->take($qQuery['pagination']['limit']);
		}
		if (!empty($qQuery['pagination']['start'])) {
			$query->skip((int)$qQuery['pagination']['start'] - 1);
		}
		return $query;
	}

	public static function insertData($table, $data) {
		$dt = [];
		foreach ($data as $key => $val) {
			if (!empty($val)) {
				$dt[$key] = $val;
			}
		}
		if (empty($data['created_at'])) {
			$dt['created_at'] = date('Y-m-d H:i:s');
		}
		if (empty($data['updated_at'])) {
			$dt['updated_at'] = date('Y-m-d H:i:s');
		}
		$dt['created_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
		$dt['updated_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
		return DB::table($table)->insertGetId($dt);
	}

	public static function insertArray($table='', $data=[]) {
		$result = null;
		foreach ($data as $key => $val) {
			if (empty($val['created_at'])) {
				$val['created_at'] = date('Y-m-d H:i:s');
			}
			if (empty($val['updated_at'])) {
				$val['updated_at'] = date('Y-m-d H:i:s');
			}
			$val['created_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
			$val['updated_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
			$result=DB::table($table)->insert($val);
		}
		return $result;
	}

	public static function truncateInsert($table='', $where=[], $data=[]) {
		$query = DB::table($table);
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		$delData = $query->delete();
		foreach ($data as $key => $val) {
			if (empty($val['created_at'])) {
				$val['created_at'] = date('Y-m-d H:i:s');
			}
			if (empty($val['updated_at'])) {
				$val['updated_at'] = date('Y-m-d H:i:s');
			}
			$val['created_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
			$val['updated_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
			DB::table($table)->insert($val);
		}
		return $delData;
	}

	public static function updateData($table, $where = array(), $data) {
		$query = DB::table($table);
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		$dt = [];
		foreach ($data as $key => $val) {
			if (!empty($val)) {
				$dt[$key] = $val;
			}
		}
		if (empty($data['updated_at'])) {
			$dt['updated_at'] = date('Y-m-d H:i:s');
		}
		$dt['updated_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
		return $query->update($dt);
	}

	public static function movedData($table, $where = array(), $data) {
		$query = DB::table($table);
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		$data['moved_at'] = date('Y-m-d H:i:s');
		$data['moved_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
		return $query->update($data);
	}

	public static function deleteData($table, $where = array()) {
		$query = DB::table($table);
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		$data['deleted_at'] = date('Y-m-d H:i:s');
		$data['deleted_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
		return $query->update($data);
	}

	public static function postDelete($table, $where)
	{
		$query = DB::table($table);
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		return $query->delete();
	}
}
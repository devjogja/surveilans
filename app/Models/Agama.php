<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{
    public static function qq()
    {
        $q = "
			SELECT
			a.id,
			a.name agama_txt
			FROM
			mst_religion a
			";
        return $q;
    }
}

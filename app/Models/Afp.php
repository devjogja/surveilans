<?php

namespace App\Models;

use App\Models\Kabupaten;
use App\Models\Puskesmas;
use App\Models\Rumahsakit;
use Illuminate\Database\Eloquent\Model;
use DB;

class Afp extends Model
{
	public static function q(){
		$q = DB::select();
	}

	public static function qq(){
		$q = "(
		SELECT
		a.id AS id_trx_case,
		a.no_epid,
		a.no_epid_klb,
		b.name AS name_pasien,
		b.umur_hari,
		b.umur_bln,
		b.umur_thn,
		CONCAT(b.alamat,' ',c.name,', ',d.name,', ',e.name,', ',f.name) AS full_address,
		CASE
		WHEN g.keadaan_akhir = 1 THEN 'Hidup'
		WHEN g.keadaan_akhir = 2 THEN 'Meninggal'
		ELSE ''
		END AS keadaan_akhir_txt,
		CASE
		WHEN a.klasifikasi_final = 1 THEN 'Polio'
		WHEN a.klasifikasi_final = 2 THEN 'Polio Kompatibel'
		WHEN a.klasifikasi_final = 3 THEN 'Bukan Polio'
		WHEN a.klasifikasi_final = 4 THEN 'VDVP'
		ELSE NULL
		END AS klasifikasi_final_txt,
		a.id_faskes,
		a.id_role,
		CASE
		WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',h.name)
		WHEN a.id_role = 2 THEN i.name
		WHEN a.id_role = 6 THEN CONCAT('KABUPATEN ',j.name)
		ELSE NULL
		END AS name_faskes,
		CASE
		WHEN a.id_role = 1 THEN h.code_kecamatan
		ELSE NULL
		END AS code_kecamatan_faskes,
		CASE
		WHEN a.id_role = 1 THEN h.code_kabupaten
		WHEN a.id_role = 2 THEN i.code_kabupaten
		WHEN a.id_role = 6 THEN j.code
		ELSE NULL
		END AS code_kabupaten_faskes,
		CASE
		WHEN a.id_role = 1 THEN h.code_provinsi
		WHEN a.id_role = 2 THEN i.code_provinsi
		WHEN a.id_role = 6 THEN j.code_provinsi
		ELSE NULL
		END AS code_provinsi_faskes,
		a.jenis_input,
		g.status_kasus,
		k.id AS id_pe,
		CASE
		WHEN k.id IS NULL THEN 'PE'
		ELSE 'Sudah PE'
		END AS 'pe_text',
		a.id_faskes_old,
		a.id_role_old,
		DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
		DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
		CASE
		WHEN a.jenis_input = 1 THEN 'Web'
		WHEN a.jenis_input = 2 THEN 'Android'
		WHEN a.jenis_input = 3 THEN 'Import'
		END AS jenis_input_text,
		l.id AS id_ku60,
		m.id AS id_hkf,
		g.hot_case,
		CASE
		WHEN g.hot_case = 1 THEN 'Hot Case'
		WHEN g.hot_case = 2 THEN 'Bukan'
		END AS hot_case_txt,
		b.code_kelurahan AS code_kelurahan_pasien,
		g.tgl_mulai_lumpuh AS tgl_sakit_date,
		DATE_FORMAT(g.tgl_mulai_lumpuh, '%Y') AS thn_sakit,
		b.code_kecamatan AS code_kecamatan_pasien
		FROM
		trx_case a
		JOIN ref_pasien b ON a.id_pasien = b.id AND b.deleted_at IS NULL
		LEFT JOIN mst_kelurahan c ON b.code_kelurahan = c.code
		LEFT JOIN mst_kecamatan d ON c.code_kecamatan = d.code
		LEFT JOIN mst_kabupaten e ON d.code_kabupaten = e.code
		LEFT JOIN mst_provinsi f ON e.code_provinsi = f.code
		JOIN trx_klinis g ON a.id=g.id_trx_case
		LEFT JOIN (
		" . Puskesmas::qq() . "
		) AS h ON a.id_faskes = h.id AND a.id_role='1'
		LEFT JOIN (
		" . Rumahsakit::qq() . "
		) i ON a.id_faskes = i.id AND a.id_role='2'
		LEFT JOIN (
		" . Kabupaten::qq() . "
		) j ON a.id_faskes = j.code AND a.id_role='6'
		LEFT JOIN trx_pe k ON a.id=k.id_trx_case AND k.deleted_at IS NULL
		LEFT JOIN trx_ku60 l ON a.id = l.id_trx_case AND l.deleted_at IS NULL
		LEFT JOIN trx_hkf m ON a.id = m.id_trx_case AND m.deleted_at IS NULL
		WHERE
		a.id_case = 2 AND a.deleted_at IS NULL
		GROUP BY a.id) AS a
		";
		return $q;
	}

	public static function getData()
	{
		$q = "
		(SELECT
		a.id AS id_trx_case,
		a.no_epid,
		a.no_epid_klb,
		a.no_rm,
		a.no_epid_lama,
		b.name AS name_pasien,
		b.nik,
		b.umur_hari,
		b.umur_bln,
		b.umur_thn,
		b.alamat,
		CONCAT(b.alamat,' ',c.name,', ',d.name,', ',e.name,', ',f.name) AS full_address,
		c.name AS name_kelurahan_pasien,
		d.name AS name_kecamatan_pasien,
		e.name AS name_kabupaten_pasien,
		f.name AS name_provinsi_pasien,
		CASE
		WHEN g.keadaan_akhir = 1 THEN 'Hidup'
		WHEN g.keadaan_akhir = 2 THEN 'Meninggal'
		ELSE ''
		END AS keadaan_akhir_txt,
		CASE
		WHEN a.klasifikasi_final = 1 THEN 'Polio'
		WHEN a.klasifikasi_final = 2 THEN 'Polio Kompatibel'
		WHEN a.klasifikasi_final = 3 THEN 'Bukan Polio'
		WHEN a.klasifikasi_final = 4 THEN 'VDVP'
		ELSE NULL
		END AS klasifikasi_final_txt,
		a.klasifikasi_final,
		a.id_faskes,
		a.id_role,
		CASE
		WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',h.name)
		WHEN a.id_role = 2 THEN i.name
		WHEN a.id_role = 6 THEN CONCAT('KABUPATEN ',j.name)
		ELSE NULL
		END AS name_faskes,
		CASE
		WHEN a.id_role = 1 THEN h.code_kecamatan
		ELSE NULL
		END AS code_kecamatan_faskes,
		CASE
		WHEN a.id_role = 1 THEN h.name_kecamatan
		ELSE NULL
		END AS name_kecamatan_faskes,
		CASE
		WHEN a.id_role = 1 THEN h.code_kabupaten
		WHEN a.id_role = 2 THEN i.code_kabupaten
		WHEN a.id_role = 6 THEN j.code
		ELSE NULL
		END AS code_kabupaten_faskes,
		CASE
		WHEN a.id_role = 1 THEN h.name_kabupaten
		WHEN a.id_role = 2 THEN i.name_kabupaten
		WHEN a.id_role = 6 THEN j.name
		ELSE NULL
		END AS name_kabupaten_faskes,
		CASE
		WHEN a.id_role = 1 THEN h.code_provinsi
		WHEN a.id_role = 2 THEN i.code_provinsi
		WHEN a.id_role = 6 THEN j.code_provinsi
		ELSE NULL
		END AS code_provinsi_faskes,
		CASE
		WHEN a.id_role = 1 THEN h.name_provinsi
		WHEN a.id_role = 2 THEN i.name_provinsi
		WHEN a.id_role = 6 THEN j.name_provinsi
		ELSE NULL
		END AS name_provinsi_faskes,
		CASE
		WHEN a.id_role = 1 THEN h.code_faskes
		WHEN a.id_role = 2 THEN i.code_faskes
		ELSE NULL
		END AS code_faskes,
		a.jenis_input,
		CASE
		WHEN a.jenis_input = 1 THEN 'Web'
		WHEN a.jenis_input = 2 THEN 'Android'
		WHEN a.jenis_input = 3 THEN 'Import'
		END AS jenis_input_text,
		g.status_kasus,
		k.id AS id_pe,
		CASE
		WHEN k.id IS NULL THEN 'PE'
		ELSE 'Sudah PE'
		END AS 'pe_text',
		l.id AS id_ku60,
		m.id AS id_hkf,
		b.code_kelurahan AS code_kelurahan_pasien,
		b.code_kecamatan AS code_kecamatan_pasien,
		b.code_kabupaten AS code_kabupaten_pasien,
		b.code_provinsi AS code_provinsi_pasien,
		g.tgl_mulai_lumpuh AS tgl_sakit_date,
		DATE_FORMAT(g.tgl_mulai_lumpuh, '%Y') AS thn_sakit,
		DATE_FORMAT(g.tgl_mulai_lumpuh,'%d-%m-%Y') AS tgl_mulai_lumpuh,
		CASE
		WHEN g.demam_sebelum_lumpuh = 1 THEN 'Ya'
		WHEN g.demam_sebelum_lumpuh = 2 THEN 'Tidak'
		ELSE NULL
		END AS demam_sebelum_lumpuh_txt,
		CASE
		WHEN g.imunisasi_rutin_polio_sebelum_sakit = 7 THEN 'Tidak'
		WHEN g.imunisasi_rutin_polio_sebelum_sakit = 8 THEN 'Tidak tahu'
		WHEN g.imunisasi_rutin_polio_sebelum_sakit = 9 THEN 'Belum pernah'
		WHEN g.imunisasi_rutin_polio_sebelum_sakit = NULL THEN '-'
		ELSE concat (g.imunisasi_rutin_polio_sebelum_sakit,' x')
		END AS imunisasi_rutin_polio_sebelum_sakit_txt,
		CASE
		WHEN g.informasi_imunisasi_rutin_polio_sebelum_sakit = 1 THEN 'KMS/Catatan Jurim'
		WHEN g.informasi_imunisasi_rutin_polio_sebelum_sakit = 2 THEN 'Ingatan responden'
		ELSE '-'
		END AS informasi_imunisasi_rutin_polio_sebelum_sakit_txt,
		CASE
		WHEN g.pin_mopup_ori_biaspolio = 7 THEN 'Tidak'
		WHEN g.pin_mopup_ori_biaspolio = 8 THEN 'Tidak tahu'
		WHEN g.pin_mopup_ori_biaspolio = 9 THEN 'Belum pernah'
		WHEN g.pin_mopup_ori_biaspolio = NULL THEN '-'
		ELSE concat (g.pin_mopup_ori_biaspolio, ' x')
		END AS pin_mopup_ori_biaspolio_txt,
		CASE
		WHEN g.informasi_pin_mopup_ori_biaspolio = 1 THEN 'KMS/Catatan Jurim'
		WHEN g.informasi_pin_mopup_ori_biaspolio = 2 THEN 'Ingatan responden'
		ELSE '-'
		END AS informasi_pin_mopup_ori_biaspolio_txt,
		DATE_FORMAT(g.tgl_imunisasi_polio_terakhir,'%d-%m-%Y') AS tgl_imunisasi_polio_terakhir,
		DATE_FORMAT(g.tgl_laporan_diterima,'%d-%m-%Y') AS tgl_laporan_diterima,
		DATE_FORMAT(g.tgl_pelacakan,'%d-%m-%Y') AS tgl_pelacakan,
		CASE
		WHEN g.kontak = 1 THEN 'Ya'
		WHEN g.kontak = 2 THEN 'Tidak'
		ELSE '-'
		END AS kontak_txt,
		a.id_faskes_old,
		a.id_role_old,
		DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
		DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
		b.jenis_kelamin,
		CASE
		WHEN b.jenis_kelamin = 'L' THEN 'Laki-laki'
		WHEN b.jenis_kelamin = 'P' THEN 'Perempuan'
		WHEN b.jenis_kelamin = 'T' THEN 'Tidak Jelas'
		ELSE ''
		END AS jenis_kelamin_txt,
		g.jenis_kasus,
		g.hot_case,
		CASE
		WHEN g.hot_case = 1 THEN 'Hot Case'
		WHEN g.hot_case = 2 THEN 'Bukan'
		END AS hot_case_txt,
		g.imunisasi_rutin_polio_sebelum_sakit AS jml_imunisasi,
		g.pin_mopup_ori_biaspolio AS jml_pin_mopup_ori_biaspolio,
		b.agama,
		n.name AS nama_ortu,
		DATE_FORMAT(b.tgl_lahir, '%d-%m-%Y') AS tgl_lahir,
		GROUP_CONCAT(CONCAT(o.id_gejala,'_',o.val_kelumpuhan) SEPARATOR '|') AS gejala_kelumpuhan,
		GROUP_CONCAT(CONCAT(o.id_gejala,'_',o.val_gangguan_raba) SEPARATOR '|') AS gejala_gangguan_raba
		FROM
		trx_case a
		JOIN ref_pasien b ON a.id_pasien = b.id AND b.deleted_at IS NULL
		LEFT JOIN ref_family n ON b.id = n.id_pasien
		LEFT JOIN mst_kelurahan c ON b.code_kelurahan = c.code
		LEFT JOIN mst_kecamatan d ON c.code_kecamatan = d.code
		LEFT JOIN mst_kabupaten e ON d.code_kabupaten = e.code
		LEFT JOIN mst_provinsi f ON e.code_provinsi = f.code
		JOIN trx_klinis g ON a.id=g.id_trx_case
		LEFT JOIN (
		" . Puskesmas::qq() . "
		) h ON a.id_faskes = h.id AND a.id_role='1'
		LEFT JOIN (
		" . Rumahsakit::qq() . "
		) i ON a.id_faskes = i.id AND a.id_role='2'
		LEFT JOIN (
		" . Kabupaten::qq() . "
		) j ON a.id_faskes = j.code AND a.id_role='6'
		LEFT JOIN trx_pe k ON a.id=k.id_trx_case AND k.deleted_at IS NULL
		LEFT JOIN trx_ku60 l ON a.id = l.id_trx_case AND l.deleted_at IS NULL
		LEFT JOIN trx_hkf m ON a.id = m.id_trx_case AND m.deleted_at IS NULL
		LEFT JOIN (
		SELECT
		a.id_trx_klinis,
		c.name,
		c.id AS id_gejala,
		a.val_kelumpuhan,
		a.val_gangguan_raba,
		a.val_paralisis_residual
		FROM trx_gejala a
		JOIN mst_gejala c ON a.id_gejala = c.id AND c.id_case = 2
		) o ON g.id = o.id_trx_klinis
		WHERE
		a.id_case = 2 AND a.deleted_at IS NULL
		GROUP BY a.id) AS a
		";
		return $q;
	}
}

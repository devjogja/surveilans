<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Analisa extends Model
{
	static function getAnalisa($where,$case,$indicator='',$range=''){
		$query = DB::table('v_'.$case);
		foreach ($where as $key => $val) {
			if (!empty($val)) {
				$query->where($key, $val);
			}
		}
		if ($indicator && $range) {
			$query->whereBetween($indicator,$range);
		}
		return $query->get();
	}
}
<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Q extends Model
{
    public static function getData($table, $param = [])
    {
        $d = DB::table(DB::raw("(SELECT @no := 0) r, {$table}"));
        $d->select(DB::raw('(@no  := @no  + 1) AS no'));
        if (!empty($param['query']['select'])) {
            $d->addSelect($param['query']['select']);
        } else {
            $d->addSelect('a.*');
        }
        if (!empty($param['query']['where'])) {
            foreach ($param['query']['where'] as $key => $val) {
                if (!empty($val)) {
                    $d->where('a.' . $key, $val);
                }
            }
        }
        if (!empty($param['query']['whereIn'])) {
            foreach ($param['query']['whereIn'] as $key => $val) {
                $d->whereIn('a.' . $key, $val);
            }
        }
        if (!empty($param['query']['whereNotIn'])) {
            foreach ($param['query']['whereNotIn'] as $key => $val) {
                $d->whereNotIn('a.' . $key, $val);
            }
        }
        if (!empty($param['roleUser'])) {
            $roleUser = $param['roleUser'];
            if ($roleUser->code_role == 'puskesmas') {
                $d->where(function ($d) use ($roleUser) {
                    $d->where(function ($query) use ($roleUser) {
                        $query->where('a.id_role', '1');
                        $query->where('a.id_faskes', $roleUser->id_faskes);
                    });
                    $d->orWhere(function ($query) use ($roleUser) {
                        $wkp = DB::table('view_wilayah_kerja_puskesmas')->select(['code_kelurahan'])->where(['code_faskes' => $roleUser->code_faskes])->get();
                        $dwkp = array();
                        foreach ($wkp as $key => $val) {
                            $dwkp[] = $val->code_kelurahan;
                        }
                        $query->where('a.id_role', '2');
                        $query->whereIn('a.code_kelurahan_pasien', $dwkp);
                    });
                });
            } elseif ($roleUser->code_role == 'rs') {
                $d->where('a.id_role', '2');
                $d->where('a.id_faskes', $roleUser->id_faskes);
            } elseif ($roleUser->code_role == 'kabupaten') {
                $d->where('a.code_kabupaten_faskes', $roleUser->id_faskes);
            } elseif ($roleUser->code_role == 'provinsi') {
                $d->where('a.code_provinsi_faskes', $roleUser->id_faskes);
            }
        }
        if (!empty($param['filter'])) {
            $filter = $param['filter'];
            $time = $filter->filter->range;
            if ($time) {
                $from = $filter->from;
                $fy = (!empty($from->year)) ? $from->year : null;
                $fm = (!empty($from->month)) ? $from->month : null;
                $fd = (!empty($from->day)) ? $from->day : null;
                $to = $filter->to;
                $ty = (!empty($to->year)) ? $to->year : null;
                $tm = (!empty($to->month)) ? $to->month : null;
                $td = (!empty($to->day)) ? $to->day : null;
                if ($time == '1') {
                    $start = $fy . '-' . $fm . '-' . $fd;
                    $to = $ty . '-' . $tm . '-' . $td;
                    if ($fy && $fm && $fd && $ty && $tm && $td) {
                        $d->whereBetween('a.tgl_sakit_date', [$start, $to]);
                    }
                } else if ($time == '2') {
                    $start = $fy . '-' . $fm . '-' . '01';
                    $to = $ty . '-' . $tm . '-' . date('t', mktime(0, 0, 0, $tm, 1, $ty));
                    if ($fy && $fm && $ty && $tm) {
                        $d->whereBetween('a.tgl_sakit_date', [$start, $to]);
                    }
                } else if ($time == '3') {
                    $start = $fy;
                    $to = $ty;
                    if ($fy && $ty) {
                        $d->whereBetween('a.thn_sakit', [$start, $to]);
                    }
                }
            }
            if ($filter->filter_type == '1') {
                if (!empty($filter->filter->code_kelurahan_pasien)) {
                    $d->where('a.code_kelurahan_pasien', $filter->filter->code_kelurahan_pasien);
                } else if (!empty($filter->filter->code_kecamatan_pasien)) {
                    $d->where('a.code_kecamatan_pasien', $filter->filter->code_kecamatan_pasien);
                } else if (!empty($filter->filter->code_kabupaten_pasien)) {
                    $d->where('a.code_kabupaten_pasien', $filter->filter->code_kabupaten_pasien);
                } else if (!empty($filter->filter->code_provinsi_pasien)) {
                    $d->where('a.code_provinsi_pasien', $filter->filter->code_provinsi_pasien);
                }
            } else if ($filter->filter_type == '2') {
                if (!empty($filter->filter->id_wilayah_kerja)) {
                    $d->where('a.code_kelurahan_pasien', $filter->filter->id_wilayah_kerja);
                }
                if (!empty($filter->filter->puskesmas_id)) {
                    $d->where('a.id_role', '1');
                    if ($filter->filter->puskesmas_id != '-') {
                        $d->where('a.id_faskes', $filter->filter->puskesmas_id);
                    }
                }
                if (!empty($filter->filter->code_kecamatan_faskes)) {
                    $d->where('a.code_kecamatan_faskes', $filter->filter->code_kecamatan_faskes);
                }
                if (!empty($filter->filter->rs_id)) {
                    $d->where('a.id_role', '2');
                    if ($filter->filter->rs_id != '-') {
                        $d->where('a.id_faskes', $filter->filter->rs_id);
                    }
                }
                if (!empty($filter->filter->code_kabupaten_faskes)) {
                    $d->where('a.code_kabupaten_faskes', $filter->filter->code_kabupaten_faskes);
                }
                if (!empty($filter->filter->code_provinsi_faskes)) {
                    $d->where('a.code_provinsi_faskes', $filter->filter->code_provinsi_faskes);
                }
            }
        }
        return $d;
    }
}

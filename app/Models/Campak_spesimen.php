<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campak_spesimen extends Model
{
	public static function getData()
	{
		$q = "
			(SELECT
			a.id AS id_spesimen,
			a.id_trx_case AS id_trx_case,
			a.jenis_pemeriksaan AS jenis_pemeriksaan,
			CASE
			WHEN a.jenis_pemeriksaan = 1 THEN 'Serologi'
			WHEN a.jenis_pemeriksaan = 2 THEN 'Virologi'
			ELSE NULL
			END AS jenis_pemeriksaan_txt,
			a.jenis_spesimen AS jenis_spesimen,
			CASE
			WHEN a.jenis_spesimen = 11 THEN 'Darah'
			WHEN a.jenis_spesimen = 12 THEN 'Urin'
			WHEN a.jenis_spesimen = 21 THEN 'Urin'
			WHEN a.jenis_spesimen = 22 THEN 'Usap Tenggorokan'
			WHEN a.jenis_spesimen = 23 THEN 'Cairan mulut'
			ELSE NULL
			END AS jenis_spesimen_txt,
			date_format(a.tgl_ambil_spesimen, '%d-%m-%Y') AS tgl_ambil_spesimen,
			a.hasil AS hasil,
			CASE
			WHEN a.jenis_pemeriksaan = 1 AND a.hasil = '1' THEN 'IgM Campak Positif'
			WHEN a.jenis_pemeriksaan = 1 AND a.hasil = '2' THEN 'IgM Campak Negatif'
			WHEN a.jenis_pemeriksaan = 1 AND a.hasil = '3' THEN 'IgM Rubella Positif'
			WHEN a.jenis_pemeriksaan = 1 AND a.hasil = '4' THEN 'IgM Rubella Positif'
			WHEN a.jenis_pemeriksaan = 1 AND a.hasil = '5' THEN 'IgM Campak Positif & IgM Rubella Positif'
			WHEN a.jenis_pemeriksaan = 1 AND a.hasil = '6' THEN 'IgM Rubella Equivocal'
			WHEN a.jenis_pemeriksaan = 1 AND a.hasil = '7' THEN 'IgM Campak Equivocal'
			WHEN a.jenis_pemeriksaan = 1 AND a.hasil = '8' THEN 'IgM Campak Negatif & IgM Rubella Negatif'
			WHEN a.jenis_pemeriksaan = 2 THEN a.hasil
			ELSE NULL
			END AS hasil_txt
			FROM trx_spesimen a
			WHERE a.CASE = 'campak' AND a.deleted_at IS NULL) b
		";
		return $q;
	}
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Wilayah extends Model
{
	public static function getDataArea($where=array())
	{
		$query = DB::table('view_area AS a');
		foreach ($where as $key => $val) {
			$query->where($key, 'like', '%'.$val.'%');
		}
		return $query->get();
	}

	public static function getProvince($where=array())
	{
		$query = DB::table('mst_provinsi AS a');
		$query->where('a.deleted_at', NULL);
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		$data = $query->get();
		$response = array();
		foreach ($data as $key => $val) {
			$response[$val->code]=$val;
		}
		return $response;
	}

	public static function getKabupaten($where=array())
	{
		$query = DB::table('mst_kabupaten AS a');
		$query->where('a.deleted_at', NULL);
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		$data = $query->get();
		$response = array();
		foreach ($data as $key => $val) {
			$response[$val->code]=$val;
		}
		return $response;
	}

	public static function getKecamatan($where=array())
	{
		$query = DB::table('mst_kecamatan AS a');
		$query->where('a.deleted_at', NULL);
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		$data = $query->get();
		$response = array();
		foreach ($data as $key => $val) {
			$response[$val->code]=$val;
		}
		return $response;
	}

	public static function getKelurahan($where=array())
	{
		$query = DB::table('mst_kelurahan AS a');
		$query->where('a.deleted_at', NULL);
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		$data = $query->get();
		$response = array();
		foreach ($data as $key => $val) {
			$response[$val->code]=$val;
		}
		return $response;
	}
	public static function getPuskesmas($where=array())
	{
		$query = DB::table('mst_puskesmas AS a');
		$query->where('a.deleted_at', NULL);
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		$data = $query->get();
		$response = array();
		foreach ($data as $key => $val) {
			$response[$val->code_faskes]=$val;
		}
		return $response;
	}
}

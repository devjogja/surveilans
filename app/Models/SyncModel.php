<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class SyncModel extends Model
{
    public static function gUser()
    {
        return DB::select("SELECT
			a.id,
			a.first_name,
			a.last_name,
			a.email,
			a.instansi,
			a.jabatan,
			a.no_telp,
			a.jk
			FROM
			db_pro_surveilans.users a");
    }

    public static function gRoleUser($faskes)
    {
        if ($faskes == 'puskesmas') {
            return DB::select("
				SELECT
				b.kode_prop,
				b.kode_kab,
				b.kode_kec,
				b.puskesmas_code_faskes AS code_faskes,
				a.alamat,
				CASE
				WHEN b.id_new != '' THEN b.id_new
				ELSE b.puskesmas_id
				END AS faskes_id,
				a.penanggungjawab_1,
				a.penanggungjawab_2,
				c.id AS id_user
				FROM
				db_pro_surveilans.profil_puskesmas a
				JOIN db_pro_surveilans.puskesmas b ON a.kode_faskes =b.puskesmas_code_faskes
				LEFT JOIN pd3i_migration.users c ON a.created_by = c.id_user_lama
				WHERE
				b.kode_prop = '34'
				");
        } elseif ($faskes == 'rs') {
            return DB::select("SELECT
				b.kode_prop,
				b.kode_kab,
				b.kode_kec,
				b.kode_faskes AS code_faskes,
				a.alamat,
				CASE
				WHEN b.id_new != '' THEN b.id_new
				ELSE b.id
				END AS faskes_id,
				a.penanggungjawab_1,
				a.penanggungjawab_2,
				c.id AS id_user
				FROM
				db_pro_surveilans.profil_rs a
				JOIN db_pro_surveilans.rumahsakit2 b ON a.kode_faskes = b.kode_faskes
				JOIN pd3i_migration.users c ON a.created_by = c.id_user_lama
				WHERE
				b.kode_prop = '34'
				");
        } elseif ($faskes == 'kabupaten') {
            return DB::select("SELECT
				a.kode_kabupaten AS faskes_id,
				a.penanggungjawab_1,
				a.penanggungjawab_2,
				c.id AS id_user
				FROM
				db_pro_surveilans.profil_dinkes_kabupaten a
				JOIN db_pro_surveilans.kabupaten b ON a.kode_kabupaten = b.id_kabupaten
				JOIN pd3i_migration.users c ON a.created_by = c.id_user_lama
				WHERE
				b.id_provinsi = '34'
				");
        } elseif ($faskes == 'provinsi') {
            return DB::select("SELECT
				a.kode_provinsi AS faskes_id,
				a.penanggungjawab_1,
				a.penanggungjawab_2,
				c.id AS id_user
				FROM
				db_pro_surveilans.profil_dinkes_provinsi a
				JOIN db_pro_surveilans.provinsi b ON a.kode_provinsi = b.id_provinsi
				JOIN pd3i_migration.users c ON a.created_by = c.id_user_lama
				WHERE
				b.id_provinsi = '34'
				");
        } elseif ($faskes == 'pusat') {
            return DB::select("SELECT
				a.id AS faskes_id,
				a.penanggungjawab_1,
				a.penanggungjawab_2,
				c.id AS id_user
				FROM
				db_pro_surveilans.profil_kemenkes a
				JOIN pd3i_migration.users c ON a.created_by = c.id_user_lama");
        }
    }

    public static function gCampak()
    {
        return DB::select("SELECT
			a.id_campak AS id_campak_old,
			DATE_FORMAT(a.created_at, '%d-%m-%Y %H:%i:%s') AS created_at,
			DATE_FORMAT(a.updated_at, '%d-%m-%Y %H:%i:%s') AS updated_at,
			CASE
			WHEN a.kode_faskes = 'puskesmas' THEN g.id_new
			WHEN a.kode_faskes = 'rs' THEN h.id_new
			END AS id_faskes,
			CASE
			WHEN a.kode_faskes = 'puskesmas' THEN g.kode_kec
			WHEN a.kode_faskes = 'rs' THEN h.kode_kab
			ELSE ''
			END AS code_wilayah_faskes,
			CASE
			WHEN a.kode_faskes = 'puskesmas' THEN g.puskesmas_name
			WHEN a.kode_faskes = 'rs' THEN h.nama_faskes
			END AS nama_faskes,
			CASE
			WHEN a.kode_faskes = 'puskesmas' THEN '1'
			WHEN a.kode_faskes = 'rs' THEN '2'
			END AS id_role,
			c.no_rm,
			a.no_epid,
			a.no_epid_klb,
			a.no_epid_lama,
			CASE
			WHEN a.klasifikasi_final = 1 THEN 2
			WHEN a.klasifikasi_final = 2 THEN 3
			WHEN a.klasifikasi_final = 3 THEN 1
			WHEN a.klasifikasi_final = 4 THEN 4
			WHEN a.klasifikasi_final = 5 THEN 5
			WHEN a.klasifikasi_final = 6 THEN 7
			END AS klasifikasi_final,
			c.nama_anak AS name_pasien,
			c.nik,
			CASE
			WHEN c.jenis_kelamin= 1 THEN 'L'
			WHEN c.jenis_kelamin = 2 THEN 'P'
			WHEN c.jenis_kelamin = 3 THEN 'T'
			END AS jenis_kelamin,
			DATE_FORMAT(c.tanggal_lahir, '%d-%m-%Y') AS tgl_lahir,
			c.umur AS umur_thn,
			c.umur_bln AS umur_bln,
			c.umur_hr AS umur_hari,
			c.alamat,
			c.id_kelurahan,
			c.id_kecamatan,
			c.id_kabupaten,
			c.id_provinsi,
			c.id_pasien,
			c.nama_ortu,
			DATE_FORMAT(a.tanggal_imunisasi_terakhir, '%d-%m-%Y') tgl_imunisasi_campak,
			a.vaksin_campak_sebelum_sakit AS jml_imunisasi_campak,
			DATE_FORMAT(a.tanggal_timbul_demam, '%d-%m-%Y') tgl_mulai_demam,
			DATE_FORMAT(a.tanggal_timbul_rash, '%d-%m-%Y') tgl_mulai_rash,
			DATE_FORMAT(a.tanggal_laporan_diterima, '%d-%m-%Y') tgl_laporan_diterima,
			DATE_FORMAT(a.tanggal_pelacakan, '%d-%m-%Y') tgl_pelacakan,
			a.vitamin_A,
			a.keadaan_akhir,
			a.jenis_kasus,
			a.klb_ke,
			a.status_kasus,
			d.gejala,
			e.komplikasi,
			f.spesimen,
			f.hasil_lab
			FROM
			db_pro_surveilans.campak a
			JOIN db_pro_surveilans.hasil_uji_lab_campak b ON a.id_campak = b.id_campak
			JOIN db_pro_surveilans.pasien c ON b.id_pasien = c.id_pasien

			LEFT JOIN (SELECT
			aa.id_campak,
			GROUP_CONCAT(CONCAT_WS('|',IFNULL(aa.id_daftar_gejala_campak,''), IFNULL(aa.tgl_mulai,'')) SEPARATOR '_') AS gejala
			FROM(
			SELECT
			a.id_campak,
			b.id_daftar_gejala_campak,
			DATE_FORMAT(b.tgl_mulai, '%d-%m-%Y') tgl_mulai
			FROM
			db_pro_surveilans.campak a
			JOIN db_pro_surveilans.gejala_campak b ON a.id_campak = b.id_campak
			WHERE a.deleted_at IS NULL) aa
			GROUP BY aa.id_campak) d ON a.id_campak = d.id_campak

			LEFT JOIN (SELECT
			aa.id_campak,
			GROUP_CONCAT(aa.komplikasi SEPARATOR '|') AS komplikasi
			FROM
			(SELECT
			a.id_campak,
			CASE
			WHEN b.komplikasi='0' THEN 'Diare'
			WHEN b.komplikasi='1' THEN 'Pneumonia'
			WHEN b.komplikasi='2' THEN 'Bronchopneumonia'
			WHEN b.komplikasi='3' THEN 'OMA'
			WHEN b.komplikasi='4' THEN 'Ensefalitis'
			ELSE b.komplikasi
			END AS komplikasi
			FROM
			db_pro_surveilans.campak a
			JOIN db_pro_surveilans.komplikasi b ON a.id_campak = b.id_campak
			WHERE a.deleted_at IS NULL) aa
			GROUP BY aa.id_campak) e ON a.id_campak = e.id_campak

			LEFT JOIN (SELECT
			aa.id_campak,
			GROUP_CONCAT(CONCAT_WS('|',IFNULL(aa.jenis_pemeriksaan,''), IFNULL(aa.jenis_sampel,''), IFNULL(aa.tgl_ambil_sampel,'')) SEPARATOR '_') AS spesimen,
			aa.hasil_lab
			FROM(
			SELECT
			a.id_campak,
			CASE
			WHEN b.jenis_pemeriksaan = 0 THEN 1
			WHEN b.jenis_pemeriksaan = 1 THEN 2
			END AS jenis_pemeriksaan,
			CASE
			WHEN b.jenis_pemeriksaan = 0 AND a.hasil_serologi_igm_campak = 1 THEN 1
			WHEN b.jenis_pemeriksaan = 0 AND a.hasil_serologi_igm_campak = 2 THEN 2
			WHEN b.jenis_pemeriksaan = 0 AND a.hasil_serologi_igm_campak = 3 THEN 7
			WHEN b.jenis_pemeriksaan = 0 AND a.hasil_serologi_igm_rubella = 1 THEN 3
			WHEN b.jenis_pemeriksaan = 0 AND a.hasil_serologi_igm_rubella = 2 THEN 4
			WHEN b.jenis_pemeriksaan = 0 AND a.hasil_serologi_igm_rubella = 3 THEN 6
			WHEN b.jenis_pemeriksaan = 0 AND a.hasil_serologi_igm_campak = 1 AND a.hasil_serologi_igm_rubella = 1 THEN 5
			WHEN b.jenis_pemeriksaan = 0 AND a.hasil_serologi_igm_campak = 2 AND a.hasil_serologi_igm_rubella = 2 THEN 8
			WHEN b.jenis_pemeriksaan = 1 AND a.hasil_virologi_igm_campak = 1 THEN 'Virologi IgM Campak Positif'
			WHEN b.jenis_pemeriksaan = 1 AND a.hasil_virologi_igm_campak = 2 THEN 'Virologi IgM Campak Negatif'
			WHEN b.jenis_pemeriksaan = 1 AND a.hasil_virologi_igm_campak = 3 THEN 'Virologi IgM Campak Pending'
			WHEN b.jenis_pemeriksaan = 1 AND a.hasil_virologi_igm_rubella = 1 THEN 'Virologi IgM Rubella Positif'
			WHEN b.jenis_pemeriksaan = 1 AND a.hasil_virologi_igm_rubella = 2 THEN 'Virologi IgM Rubella Negatif'
			WHEN b.jenis_pemeriksaan = 1 AND a.hasil_virologi_igm_rubella = 3 THEN 'Virologi IgM Rubella Pending'
			WHEN b.jenis_pemeriksaan = 1 AND a.hasil_virologi_igm_campak = 1 AND a.hasil_virologi_igm_rubella = 1 THEN 'Virologi IgM Campak Positif & Virologi IgM Rubella Positif'
			WHEN b.jenis_pemeriksaan = 1 AND a.hasil_virologi_igm_campak = 2 AND a.hasil_virologi_igm_rubella = 2 THEN 'Virologi IgM Campak Negatif & Virologi IgM Rubella Negatif'
			END AS hasil_lab,
			CASE
			WHEN b.jenis_pemeriksaan = 0 AND b.jenis_sampel = 0 THEN 11
			WHEN b.jenis_pemeriksaan = 0 AND b.jenis_sampel = 1 THEN 12
			WHEN b.jenis_pemeriksaan = 1 AND b.jenis_sampel = 0 THEN 21
			WHEN b.jenis_pemeriksaan = 1 AND b.jenis_sampel = 1 THEN 22
			WHEN b.jenis_pemeriksaan = 1 AND b.jenis_sampel = 2 THEN 23
			END AS jenis_sampel,
			DATE_FORMAT(b.tgl_ambil_sampel, '%d-%m-%Y') tgl_ambil_sampel
			FROM
			db_pro_surveilans.campak a
			JOIN db_pro_surveilans.uji_spesimen b ON a.id_campak = b.id_campak
			WHERE a.deleted_at IS NULL) aa
			GROUP BY aa.id_campak) f ON a.id_campak = f.id_campak

			LEFT JOIN db_pro_surveilans.puskesmas g ON a.id_tempat_periksa = g.puskesmas_id
			LEFT JOIN db_pro_surveilans.rumahsakit2 h ON a.id_tempat_periksa = h.id

			WHERE a.deleted_at IS NULL
			-- AND (g.kode_prop = '34' OR h.kode_prop = '34') 
			AND a.id_campak > 16351
			GROUP BY a.id_campak
			");
    }

    public static function gEpidCampak()
    {
        return DB::select("
			SELECT
			b.id_campak AS id_campak_old,
			DATE_FORMAT(a.created_at, '%d-%m-%Y %H:%i:%s') AS created_at,
			DATE_FORMAT(a.updated_at, '%d-%m-%Y %H:%i:%s') AS updated_at,
			CASE
			WHEN b.kode_faskes = 'puskesmas' THEN c.id_new
			WHEN b.kode_faskes = 'rs' THEN d.id_new
			END AS faskes_id,
			CASE
			WHEN b.kode_faskes = 'puskesmas' THEN '1'
			WHEN b.kode_faskes = 'rs' THEN '2'
			END AS role_id,
			DATE_FORMAT(a.waktu_pengobatan, '%d-%m-%Y') AS tgl_pengobatan_pertama_kali,
			a.tempat_pengobatan AS tempat_pengobatan_pertama_kali,
			a.obat_yang_diberikan AS obat_yg_diberikan,
			a.dirumah_orang_sakit_sama AS penyakit_sama_dirumah,
			DATE_FORMAT(a.kapan_sakit_dirumah, '%d-%m-%Y') AS tgl_penyakit_sama_dirumah,
			a.disekolah_orang_sakit_sama AS penyakit_sama_disekolah,
			DATE_FORMAT(a.kapan_sakit_disekolah, '%d-%m-%Y') AS tgl_penyakit_sama_disekolah,
			a.kekurangan_gizi AS keadaan_kurang_gizi,
			a.alamat_tinggal_sementara,
			DATE_FORMAT(a.tgl_pe_dialamat_sementara, '%d-%m-%Y') AS tgl_pe_dialamat_sementara,
			a.jumlah_kasus_dialamat_sementara,
			a.alamat_tempat_tinggal,
			DATE_FORMAT(a.tgl_pe_alamat_tempat_tinggal, '%d-%m-%Y') AS tgl_pe_alamat_tempat_tinggal,
			a.jumlah_kasus_dialamat_tempat_tinggal,
			a.sekolah,
			DATE_FORMAT(a.tgl_pe_sekolah, '%d-%m-%Y') AS tgl_pe_sekolah,
			a.jumlah_kasus_disekolah,
			a.tempat_kerja,
			DATE_FORMAT(a.tgl_pe_tempat_kerja, '%d-%m-%Y') AS tgl_pe_tempat_kerja,
			a.jumlah_kasus_tempat_kerja,
			a.lain_lain,
			DATE_FORMAT(a.tgl_pe_lain_lain, '%d-%m-%Y') AS tgl_pe_lain_lain,
			a.jumlah_kasus_lain_lain,
			a.jumlah_total_kasus_tambahan AS total_kasus_tambahan,
			DATE_FORMAT(a.tanggal_penyelidikan, '%d-%m-%Y') AS tgl_penyelidikan,
			a.pelaksana
			FROM
			db_pro_surveilans.pe_campak a
			JOIN db_pro_surveilans.campak b ON a.id = b.id_pe_campak AND b.deleted_at IS NULL
			LEFT JOIN db_pro_surveilans.puskesmas c ON b.id_tempat_periksa = c.puskesmas_id
			LEFT JOIN db_pro_surveilans.rumahsakit2 d ON b.id_tempat_periksa = d.id
			WHERE a.deleted_at IS NULL
			-- AND (c.kode_prop = '34' OR d.kode_prop = '34')
			AND b.id_campak > 16351
			");
    }

    public static function gAfp()
    {
        return DB::select("SELECT
		a.id_afp AS id_afp_old,
		DATE_FORMAT(a.created_at, '%d-%m-%Y %H:%i:%s') AS created_at,
		DATE_FORMAT(a.updated_at, '%d-%m-%Y %H:%i:%s') AS updated_at,
		CASE
		WHEN a.kode_faskes = 'puskesmas' THEN g.id_new
		WHEN a.kode_faskes = 'rs' THEN h.id_new
		END AS id_faskes,
			CASE
			WHEN a.kode_faskes = 'puskesmas' THEN g.kode_kec
			WHEN a.kode_faskes = 'rs' THEN h.kode_kab
			ELSE ''
			END AS code_wilayah_faskes,
		CASE
		WHEN a.kode_faskes = 'puskesmas' THEN CONCAT('Puskesmas ',g.puskesmas_name)
		WHEN a.kode_faskes = 'rs' THEN h.nama_faskes
		END AS nama_faskes,
		CASE
		WHEN a.kode_faskes = 'puskesmas' THEN '1'
		WHEN a.kode_faskes = 'rs' THEN '2'
		END AS id_role,
		c.no_rm,
		a.no_epid,
		a.no_epid_lama,
		CASE
		WHEN a.klasifikasi_final = 1 THEN 1
		WHEN a.klasifikasi_final = 2 THEN 3
		WHEN a.klasifikasi_final = 3 THEN 2
		END AS klasifikasi_final,
		c.nama_anak AS name_pasien,
		c.nik,
		CASE
		WHEN c.jenis_kelamin= 1 THEN 'L'
		WHEN c.jenis_kelamin = 2 THEN 'P'
		WHEN c.jenis_kelamin = 3 THEN 'T'
		END AS jenis_kelamin,
		DATE_FORMAT(c.tanggal_lahir, '%d-%m-%Y') AS tgl_lahir,
		c.umur AS umur_thn,
		c.umur_bln AS umur_bln,
		c.umur_hr AS umur_hari,
		c.alamat,
		c.id_kelurahan,
		c.id_kecamatan,
		c.id_kabupaten,
		c.id_provinsi,
		c.id_pasien,
		c.nama_ortu,
		DATE_FORMAT(a.tanggal_mulai_lumpuh, '%d-%m-%Y') AS tgl_mulai_lumpuh,
		a.demam_sebelum_lumpuh,
		a.kelumpuhan_anggota_gerak_kanan,
		a.kelumpuhan_anggota_gerak_kiri,
		a.gangguan_raba_anggota_gerak_kanan,
		a.gangguan_raba_anggota_gerak_kiri,
		a.imunisasi_polio_sebelum_sakit AS imunisasi_rutin_polio_sebelum_sakit,
		a.sumber_informasi_imunisasi AS informasi_imunisasi_rutin_polio_sebelum_sakit,
		a.imunisasi_polio_lain AS pin_mopup_ori_biaspolio,
		a.sumber_informasi_imunisasi_polio_lain AS informasi_pin_mopup_ori_biaspolio,
		DATE_FORMAT(a.tanggal_vaksinasi_polio, '%d-%m-%Y') AS tgl_imunisasi_polio_terakhir,
		DATE_FORMAT(a.tanggal_laporan_diterima, '%d-%m-%Y') AS tgl_laporan_diterima,
		DATE_FORMAT(a.tanggal_pelacakan, '%d-%m-%Y') AS tgl_pelacakan,
		a.kontak,
		a.keadaan_akhir,
		DATE_FORMAT(a.tanggal_pengambilan_spesimen1, '%d-%m-%Y') AS tanggal_pengambilan_spesimen1,
		DATE_FORMAT(a.tanggal_pengambilan_spesimen2, '%d-%m-%Y') AS tanggal_pengambilan_spesimen2,
		b.jp1,
		b.isolasi_virus_jp1,
		b.ket_isolasi_virus_jp1,
		b.itd_jp1,
		b.ket_itd_jp1,
		b.sequencing_jp1,
		b.ket_sequencing_jp1,
		b.jp2,
		b.isolasi_virus_jp2,
		b.ket_isolasi_virus_jp2,
		b.itd_jp2,
		b.ket_itd_jp2,
		b.sequencing_jp2,
		b.ket_sequencing_jp2
		FROM
		db_pro_surveilans.afp a
		JOIN db_pro_surveilans.hasil_uji_lab_afp b ON a.id_afp = b.id_afp
		JOIN db_pro_surveilans.pasien c ON b.id_pasien = c.id_pasien

		LEFT JOIN db_pro_surveilans.puskesmas g ON a.id_tempat_periksa = g.puskesmas_id
		LEFT JOIN db_pro_surveilans.rumahsakit2 h ON a.id_tempat_periksa = h.id

		WHERE
		a.deleted_at IS NULL
		-- AND (g.kode_prop = '34' OR h.kode_prop = '34')
		AND a.id_afp > 196
		GROUP BY a.id_afp
		");
    }

    public static function gEpidAfp()
    {
        return DB::select("
		SELECT
		b.id_afp AS id_afp_old
		,DATE_FORMAT(a.created_at, '%d-%m-%Y %H:%i:%s') AS created_at
		,DATE_FORMAT(a.updated_at, '%d-%m-%Y %H:%i:%s') AS updated_at
		,CASE
		WHEN b.kode_faskes = 'puskesmas' THEN c.id_new
		WHEN b.kode_faskes = 'rs' THEN d.id_new
		END AS faskes_id
		,CASE
		WHEN b.kode_faskes = 'puskesmas' THEN '1'
		WHEN b.kode_faskes = 'rs' THEN '2'
		END AS role_id
		,a.no_rekam_medis AS no_rm
		,b.no_epid_lama
		,a.propinsi AS code_provinsi_sumber_informasi
		,a.kabupaten AS code_kabupaten_sumber_informasi
		,CASE
		WHEN a.laporan_dari = 'Rumah sakit' THEN '1'
		WHEN a.laporan_dari = 'Puskesmas' THEN '2'
		WHEN a.laporan_dari = 'Dokter praktek' THEN '3'
		WHEN a.laporan_dari = 'Lain' THEN '4'
		END AS sumber_laporan
		,a.ket_sumber_laporan AS keterangan
		,DATE_FORMAT(a.tanggal_mulai_sakit, '%d-%m-%Y') AS tgl_sakit
		,DATE_FORMAT(a.tanggal_mulai_lumpuh, '%d-%m-%Y') AS tgl_mulai_lumpuh
		,DATE_FORMAT(a.tanggal_meninggal, '%d-%m-%Y') AS tgl_meninggal
		,a.berobat_unit_pelayanan AS berobat_ke_unit_pelayanan_lain
		,a.nama_unit_pelayanan
		,DATE_FORMAT(a.tanggal_berobat, '%d-%m-%Y') AS tgl_berobat
		,a.diagnosis
		,a.no_rekam_medis AS no_rm_lama
		,a.kelumpuhan_akut AS kelumpuhan_sifat_akut
		,a.kelumpuhan_layuh AS kelumpuhan_sifat_layuh
		,a.kelumpuhan_ruda AS kelumpuhan_disebabkan_ruda
		,a.jumlah_dosis AS imunisasi_rutin_polio_sebelum_sakit
		,a.sumber_informasi AS informasi_imunisasi_rutin_polio_sebelum_sakit
		,a.jumlah_dosis_bias_polio AS pin_mopup_ori_biaspolio
		,a.sumber_informasi_bias_polio AS informasi_pin_mopup_ori_biaspolio
		,DATE_FORMAT(a.tanggal_imunisasi_polio, '%d-%m-%Y') AS tgl_imunisasi_polio_terakhir
		,a.demam_sebelum_lumpuh
		,a.lumpuh_tungkai_kanan
		,a.raba_tungkai_kanan
		,a.lumpuh_tungkai_kiri
		,a.raba_tungkai_kiri
		,a.lumpuh_lengan_kanan
		,a.raba_lengan_kanan
		,a.lumpuh_lengan_kiri
		,a.raba_lengan_kiri
		,a.sebelum_sakit_berpergian AS bepergian_sebelum_sakit
		,a.lokasi AS lokasi_pergi_sebelum_sakit
		,DATE_FORMAT(a.tanggal_pergi, '%d-%m-%Y') AS tgl_pergi_sebelum_sakit
		,a.berkunjung AS berkunjung_ke_rumah_anak_imunisasi_polio
		,a.catatan_tidak_diambil_spesimen AS alasan_spesimen_tidak_diambil
		,DATE_FORMAT(a.tanggal_ambil_spesimen_I, '%d-%m-%Y') AS tanggal_ambil_spesimen_I
		,DATE_FORMAT(a.tanggal_ambil_spesimen_II, '%d-%m-%Y') AS tanggal_ambil_spesimen_II
		,DATE_FORMAT(a.tanggal_kirim_spesimen_I, '%d-%m-%Y') AS tanggal_kirim_spesimen_I
		,DATE_FORMAT(a.tanggal_kirim_spesimen_II, '%d-%m-%Y') AS tanggal_kirim_spesimen_II
		,DATE_FORMAT(a.tanggal_kirim_spesimen_I_propinsi, '%d-%m-%Y') AS tanggal_kirim_spesimen_I_propinsi
		,DATE_FORMAT(a.tanggal_kirim_spesimen_II_propinsi, '%d-%m-%Y') AS tanggal_kirim_spesimen_II_propinsi
		,a.nama_petugas AS pelaksana
		,a.hasil_diagnosis AS diagnosis
		,a.nama_DSA AS nama_pemeriksa
		,a.telepon_hp AS no_telp
		FROM
		db_pro_surveilans.pe_afp a
		JOIN db_pro_surveilans.afp b ON a.id = b.id_pe_afp AND b.deleted_at IS NULL
		LEFT JOIN db_pro_surveilans.puskesmas c ON b.id_tempat_periksa = c.puskesmas_id
		LEFT JOIN db_pro_surveilans.rumahsakit2 d ON b.id_tempat_periksa = d.id
		WHERE
		a.deleted_at IS NULL
		-- AND (c.kode_prop = '34' OR d.kode_prop = '34')
		AND b.id_afp > 196
		");
    }

    public static function gDifteri()
    {
        return DB::select("SELECT
		a.id_difteri AS id_difteri_old,
		DATE_FORMAT(a.created_at, '%d-%m-%Y %H:%i:%s') AS created_at,
		DATE_FORMAT(a.updated_at, '%d-%m-%Y %H:%i:%s') AS updated_at,
		CASE
		WHEN a.kode_faskes = 'puskesmas' THEN g.id_new
		WHEN a.kode_faskes = 'rs' THEN h.id_new
		END AS id_faskes,
			CASE
			WHEN a.kode_faskes = 'puskesmas' THEN g.kode_kec
			WHEN a.kode_faskes = 'rs' THEN h.kode_kab
			ELSE ''
			END AS code_wilayah_faskes,
		CASE
		WHEN a.kode_faskes = 'puskesmas' THEN CONCAT('Puskesmas ',g.puskesmas_name)
		WHEN a.kode_faskes = 'rs' THEN h.nama_faskes
		END AS nama_faskes,
		CASE
		WHEN a.kode_faskes = 'puskesmas' THEN '1'
		WHEN a.kode_faskes = 'rs' THEN '2'
		END AS id_role,
		c.no_rm,
		a.no_epid,
		a.no_epid_lama,
		CASE
		WHEN a.klasifikasi_final = 1 THEN 2
		WHEN a.klasifikasi_final = 2 THEN 1
		WHEN a.klasifikasi_final = 3 THEN 3
		END AS klasifikasi_final,
		c.nama_anak AS name_pasien,
		c.nik,
		CASE
		WHEN c.jenis_kelamin= 1 THEN 'L'
		WHEN c.jenis_kelamin = 2 THEN 'P'
		WHEN c.jenis_kelamin = 3 THEN 'T'
		END AS jenis_kelamin,
		DATE_FORMAT(c.tanggal_lahir, '%d-%m-%Y') AS tgl_lahir,
		c.umur AS umur_thn,
		c.umur_bln AS umur_bln,
		c.umur_hr AS umur_hari,
		c.alamat,
		c.id_kelurahan,
		c.id_kecamatan,
		c.id_kabupaten,
		c.id_provinsi,
		c.id_pasien,
		c.nama_ortu,
		DATE_FORMAT(a.tanggal_timbul_demam, '%d-%m-%Y') AS tgl_mulai_demam,
		a.vaksin_DPT_sebelum_sakit AS jml_imunisasi_dpt,
		DATE_FORMAT(a.tanggal_vaksinasi_difteri_terakhir, '%d-%m-%Y') AS tgl_imunisasi_difteri,
		DATE_FORMAT(a.tanggal_pelacakan, '%d-%m-%Y') AS tgl_pelacakan,
		a.gejala_lain,
		a.jumlah_kontak AS jml_kontak,
		a.jumlah_swab_hidung AS diambil_spec_kontak,
		a.jumlah_positif AS positif_kontak,
		a.keadaan_akhir,
		f.spesimen,
		DATE_FORMAT(a.tanggal_diambil_spesimen_tenggorokan, '%d-%m-%Y') AS tanggal_diambil_spesimen_tenggorokan,
		DATE_FORMAT(a.tanggal_diambil_spesimen_hidung, '%d-%m-%Y') AS tanggal_diambil_spesimen_hidung,
		a.jenis_pemeriksaan,
		a.hasil_lab_kultur_tenggorokan,
		a.hasil_lab_kultur_hidung,
		a.hasil_lab_mikroskop_tenggorokan,
		a.hasil_lab_mikroskop_hidung
		FROM
		db_pro_surveilans.difteri a
		JOIN db_pro_surveilans.hasil_uji_lab_difteri b ON a.id_difteri = b.id_difteri
		JOIN db_pro_surveilans.pasien c ON b.id_pasien = c.id_pasien

		LEFT JOIN (SELECT
		aa.id_difteri,
		GROUP_CONCAT(CONCAT_WS('|',IFNULL(aa.jenis_spesimen,''),IFNULL(aa.tgl_ambil_spesimen,''),IFNULL(aa.jenis_pemeriksaan,''),CONCAT(''), IFNULL(aa.hasil_lab,'')) SEPARATOR '_') AS spesimen
		FROM (
		SELECT
		a.id_difteri,
		CASE
		WHEN b.jenis_spesimen = 'Tenggorokan' THEN '1'
		WHEN b.jenis_spesimen = 'Hidung' THEN '2'
		END AS jenis_spesimen,
		DATE_FORMAT(b.tgl_ambil_spesimen, '%d-%m-%Y') AS tgl_ambil_spesimen,
		CASE
		WHEN b.jenis_pemeriksaan = 'Kultur' THEN '1'
		WHEN b.jenis_pemeriksaan = 'Mikroskop' THEN '2'
		END AS jenis_pemeriksaan,
		CASE
		WHEN b.hasil_lab = 'Positif' THEN '1'
		WHEN b.hasil_lab = 'Negatif' THEN '2'
		END AS hasil_lab
		FROM
		db_pro_surveilans.difteri a
		JOIN db_pro_surveilans.trx_spesimen b ON a.id_difteri = b.id_case
		WHERE
		a.deleted_at IS NULL) aa
		GROUP BY aa.id_difteri) f ON a.id_difteri = f.id_difteri

		LEFT JOIN db_pro_surveilans.puskesmas g ON a.id_tempat_periksa = g.puskesmas_id
		LEFT JOIN db_pro_surveilans.rumahsakit2 h ON a.id_tempat_periksa = h.id

		WHERE
		a.deleted_at IS NULL
		-- AND (g.kode_prop = '34' OR h.kode_prop = '34')
		AND a.id_difteri >174
		GROUP BY a.id_difteri
		");
    }

    public static function gEpidDifteri()
    {
        return DB::select("SELECT
		b.id_difteri AS id_difteri_old
		,DATE_FORMAT(a.created_at, '%d-%m-%Y %H:%i:%s') AS created_at
		,DATE_FORMAT(a.updated_at, '%d-%m-%Y %H:%i:%s') AS updated_at
		,CASE
		WHEN b.kode_faskes = 'puskesmas' THEN c.id_new
		WHEN b.kode_faskes = 'rs' THEN d.id_new
		END AS faskes_id
		,CASE
		WHEN b.kode_faskes = 'puskesmas' THEN '1'
		WHEN b.kode_faskes = 'rs' THEN '2'
		END AS role_id
		,f.no_rm
		,a.puskesmas AS faskes_tempat_periksa
		,f.nama_anak
		,f.nik
		,f.nama_ortu
		,f.jenis_kelamin
		,DATE_FORMAT(f.tanggal_lahir, '%d-%m-%Y') AS tanggal_lahir
		,f.umur
		,f.umur_bln
		,f.umur_hr
		,f.id_kelurahan AS code_kelurahan_tempat_tinggal_sekarang
		,a.telepon AS no_telp
		,a.tempat_tinggal
		,a.pekerjaan
		,a.alamat_tempat_kerja
		,a.nama_saudara_yang_bisa_dihubungi
		,a.alamat_saudara_yang_bisa_dihubungi AS alamat_saat_ini
		,a.tlp_saudara_yang_bisa_dihubungi AS no_telp_saudara
		,DATE_FORMAT(a.tanggal_mulai_sakit, '%d-%m-%Y') AS tgl_mulai_sakit
		,a.keluhan AS keluhan_untuk_berobat
		,a.status_imunisasi_difteri
		,a.gejala_sakit_demam
		,DATE_FORMAT(a.tanggal_gejala_sakit_demam, '%d-%m-%Y') AS tanggal_gejala_sakit_demam
		,a.gejala_sakit_kerongkongan
		,DATE_FORMAT(a.tanggal_gejala_sakit_kerongkongan, '%d-%m-%Y') AS tanggal_gejala_sakit_kerongkongan
		,a.gejala_leher_bengkak
		,DATE_FORMAT(a.tanggal_gejala_leher_bengkak, '%d-%m-%Y') AS tanggal_gejala_leher_bengkak
		,a.gejala_sesak_nafas
		,DATE_FORMAT(a.tanggal_gejala_sesak_nafas, '%d-%m-%Y') AS tanggal_gejala_sesak_nafas
		,a.gejala_pseudomembran
		,DATE_FORMAT(a.tanggal_gejala_pseudomembran, '%d-%m-%Y') AS tanggal_gejala_pseudomembran
		,a.gejala_lain
		,a.status_imunisasi_difteri
		,a.jenis_spesimen
		,DATE_FORMAT(a.tanggal_ambil_spesimen, '%d-%m-%Y') AS tanggal_ambil_spesimen
		,a.kode_spesimen
		,a.tempat_berobat
		,a.dirawat
		,a.trakeostomi
		,a.antibiotik
		,a.obat_lain
		,a.ads
		,a.kondisi_kasus
		,a.penderita_berpergian
		,a.tempat_berpergian
		,a.penderita_berkunjung
		,a.tempat_berkunjung
		,a.penderita_menerima_tamu
		,a.asal_tamu
		,a.nama AS nama_kontak_kasus
		,a.umur AS umur_kontak_kasus
		,a.hubungan_kasus AS hub_dgn_kontak_kasus
		,a.status_imunisasi AS status_imunisasi_kontak_kasus
		,a.hasil_lab
		,a.profilaksis
		FROM
		db_pro_surveilans.pe_difteri a
		JOIN db_pro_surveilans.difteri b ON a.id = b.id_pe_difteri AND b.deleted_at IS NULL
		JOIN db_pro_surveilans.hasil_uji_lab_difteri e ON b.id_difteri = e.id_difteri
		JOIN db_pro_surveilans.pasien f ON e.id_pasien = f.id_pasien
		LEFT JOIN db_pro_surveilans.puskesmas c ON b.id_tempat_periksa = c.puskesmas_id
		LEFT JOIN db_pro_surveilans.rumahsakit2 d ON b.id_tempat_periksa = d.id
		WHERE
		-- (c.kode_prop = '34' OR d.kode_prop = '34')
		b.id_difteri > 174
		");
    }

    public static function gTetanus()
    {
        return DB::select("SELECT
		a.id_tetanus AS id_tetanus_old,
		DATE_FORMAT(a.created_at, '%d-%m-%Y %H:%i:%s') AS created_at,
		DATE_FORMAT(a.updated_at, '%d-%m-%Y %H:%i:%s') AS updated_at,
		CASE
		WHEN a.kode_faskes = 'puskesmas' THEN g.id_new
		WHEN a.kode_faskes = 'rs' THEN h.id_new
		END AS id_faskes,
			CASE
			WHEN a.kode_faskes = 'puskesmas' THEN g.kode_kec
			WHEN a.kode_faskes = 'rs' THEN h.kode_kab
			ELSE ''
			END AS code_wilayah_faskes,
		CASE
		WHEN a.kode_faskes = 'puskesmas' THEN CONCAT('Puskesmas ',g.puskesmas_name)
		WHEN a.kode_faskes = 'rs' THEN h.nama_faskes
		END AS nama_faskes,
		CASE
		WHEN a.kode_faskes = 'puskesmas' THEN '1'
		WHEN a.kode_faskes = 'rs' THEN '2'
		END AS id_role,
		c.no_rm,
		a.no_epid,
		a.no_epid_lama,
		a.klasifikasi_akhir,
		c.nama_anak AS name_pasien,
		c.nik,
		CASE
		WHEN c.jenis_kelamin = 1 THEN 'L'
		WHEN c.jenis_kelamin = 2 THEN 'P'
		WHEN c.jenis_kelamin = 3 THEN 'T'
		END AS jenis_kelamin,
		DATE_FORMAT(c.tanggal_lahir, '%d-%m-%Y') AS tgl_lahir,
		c.umur AS umur_thn,
		c.umur_bln AS umur_bln,
		c.umur_hr AS umur_hari,
		c.alamat,
		c.id_kelurahan,
		c.id_kecamatan,
		c.id_kabupaten,
		c.id_provinsi,
		c.id_pasien,
		c.nama_ortu,
		DATE_FORMAT(a.tanggal_mulai_sakit, '%d-%m-%Y') AS tgl_mulai_sakit,
		DATE_FORMAT(a.tanggal_laporan_diterima, '%d-%m-%Y') AS tgl_laporan_diterima,
		DATE_FORMAT(a.tanggal_pelacakan, '%d-%m-%Y') AS tgl_pelacakan,
		CASE
		WHEN a.ANC = '0' THEN '1'
		WHEN a.ANC = '1' THEN '2'
		WHEN a.ANC = '2' THEN '3'
		WHEN a.ANC = '3' THEN '4'
		WHEN a.ANC = '4' THEN '5'
		END AS antenatal_care,
		CASE
		WHEN a.status_imunisasi = '0' THEN '1'
		WHEN a.status_imunisasi = '1' THEN '2'
		WHEN a.status_imunisasi = '2' THEN '3'
		WHEN a.status_imunisasi = '3' THEN '4'
		END AS status_imunisasi_ibu,
		CASE
		WHEN a.penolong_persalinan = '0' THEN '1'
		WHEN a.penolong_persalinan = '1' THEN '2'
		WHEN a.penolong_persalinan = '2' THEN '3'
		WHEN a.penolong_persalinan = '3' THEN '4'
		END AS penolong_persalinan,
		CASE
		WHEN a.pemotongan_tali_pusat = '0' THEN '1'
		WHEN a.pemotongan_tali_pusat = '1' THEN '2'
		WHEN a.pemotongan_tali_pusat = '2' THEN '5'
		WHEN a.pemotongan_tali_pusat = '3' THEN '6'
		END AS alat_pemotong_tali_pusat,
		CASE
		WHEN a.rawat_rumah_sakit = '0' THEN '1'
		WHEN a.rawat_rumah_sakit = '1' THEN '2'
		WHEN a.rawat_rumah_sakit = '2' THEN '3'
		END AS rawat_rumah_sakit,
		CASE
		WHEN a.keadaan_akhir = '0' THEN '1'
		WHEN a.keadaan_akhir = '1' THEN '2'
		END AS keadaan_akhir
		FROM
		db_pro_surveilans.tetanus a
		JOIN db_pro_surveilans.pasien_terserang_tetanus b ON a.id_tetanus = b.id_tetanus
		JOIN db_pro_surveilans.pasien c ON b.id_pasien = c.id_pasien

		LEFT JOIN db_pro_surveilans.puskesmas g ON a.id_tempat_periksa = g.puskesmas_id
		LEFT JOIN db_pro_surveilans.rumahsakit2 h ON a.id_tempat_periksa = h.id

		WHERE
		a.deleted_at IS NULL
		-- AND (g.kode_prop = '34' OR h.kode_prop = '34')
		AND a.id_tetanus > 174
		GROUP BY a.id_tetanus
			");
    }

    public static function gCrs()
    {
        return DB::select("SELECT
		a.id_crs AS id_crs_old,
		DATE_FORMAT(a.created_at, '%d-%m-%Y %H:%i:%s') AS created_at,
		DATE_FORMAT(a.updated_at, '%d-%m-%Y %H:%i:%s') AS updated_at,
		CASE
		WHEN a.kode_faskes = 'puskesmas' THEN g.id_new
		WHEN a.kode_faskes = 'rs' THEN h.id_new
		END AS id_faskes,
			CASE
			WHEN a.kode_faskes = 'puskesmas' THEN g.kode_kec
			WHEN a.kode_faskes = 'rs' THEN h.kode_kab
			ELSE ''
			END AS code_wilayah_faskes,
		CASE
		WHEN a.kode_faskes = 'puskesmas' THEN CONCAT('Puskesmas ',g.puskesmas_name)
		WHEN a.kode_faskes = 'rs' THEN h.nama_faskes
		END AS nama_faskes,
		CASE
		WHEN a.kode_faskes = 'puskesmas' THEN '1'
		WHEN a.kode_faskes = 'rs' THEN '2'
		END AS id_role,
		c.no_rm,
		a.no_epid,
		a.no_epid_lama,
		CASE
		WHEN a.klasifikasi_final = 'CRS pasti(Lab Positif)' THEN '1'
		WHEN a.klasifikasi_final = 'CRS Klinis' THEN '2'
		WHEN a.klasifikasi_final = 'Bukan CRS' THEN '3'
		WHEN a.klasifikasi_final = 'Suspek CRS' THEN '4'
		END AS klasifikasi_final,
		a.klasifikasi_final_desc,
		c.nama_anak AS name_pasien,
		c.nik,
		CASE
		WHEN c.jenis_kelamin = 1 THEN 'L'
		WHEN c.jenis_kelamin = 2 THEN 'P'
		WHEN c.jenis_kelamin = 3 THEN 'T'
		END AS jenis_kelamin,
		DATE_FORMAT(c.tanggal_lahir, '%d-%m-%Y') AS tgl_lahir,
		c.umur AS umur_thn,
		c.umur_bln AS umur_bln,
		c.umur_hr AS umur_hari,
		c.tempat_lahir_bayi AS tempat_lahir_bayi,
		c.alamat,
		c.id_kelurahan,
		c.id_kecamatan,
		c.id_kabupaten,
		c.id_provinsi,
		c.id_pasien,
		c.nama_ortu,
		c.no_telp,
		c.umur_kehamilan_bayi,
		c.berat_badan_bayi,
		d.nama_rs,
		d.id_provinsi AS id_provinsi_pelapor,
		d.id_kabupaten AS id_kabupaten_pelapor,
		DATE_FORMAT(d.tanggal_laporan, '%d-%m-%Y') AS tanggal_laporan,
		DATE_FORMAT(d.tanggal_investigasi, '%d-%m-%Y') AS tanggal_investigasi,
		a.nama_dokter_pemeriksa,
		DATE_FORMAT(a.tgl_mulai_sakit, '%d-%m-%Y') AS tgl_mulai_sakit,
		CASE
		WHEN a.keadaan_akhir = 'Hidup' THEN '1'
		WHEN a.keadaan_akhir = 'Meninggal' THEN '2'
		END AS keadaan_akhir,
		a.penyebab_meninggal,
		CASE
		WHEN a.congenital_heart_disease = 'Ya' THEN '1'
		WHEN a.congenital_heart_disease = 'Tidak' THEN '2'
		WHEN a.congenital_heart_disease = 'Tidak Tahu' THEN '3'
		END AS v4,
		CASE
		WHEN a.cataracts = 'Ya' THEN '1'
		WHEN a.cataracts = 'Tidak' THEN '2'
		WHEN a.cataracts = 'Tidak Tahu' THEN '3'
		END AS v5,
		CASE
		WHEN a.congenital_glaucoma = 'Ya' THEN '1'
		WHEN a.congenital_glaucoma = 'Tidak' THEN '2'
		WHEN a.congenital_glaucoma = 'Tidak Tahu' THEN '3'
		END AS v6,
		CASE
		WHEN a.pigmentary_retinopathy = 'Ya' THEN '1'
		WHEN a.pigmentary_retinopathy = 'Tidak' THEN '2'
		WHEN a.pigmentary_retinopathy = 'Tidak Tahu' THEN '3'
		END AS v7,
		CASE
		WHEN a.hearing_impairment = 'Ya' THEN '1'
		WHEN a.hearing_impairment = 'Tidak' THEN '2'
		WHEN a.hearing_impairment = 'Tidak Tahu' THEN '3'
		END AS v8,
		CASE
		WHEN a.purpura = 'Ya' THEN '1'
		WHEN a.purpura = 'Tidak' THEN '2'
		WHEN a.purpura = 'Tidak Tahu' THEN '3'
		END AS v9,
		CASE
		WHEN a.microcephaly = 'Ya' THEN '1'
		WHEN a.microcephaly = 'Tidak' THEN '2'
		WHEN a.microcephaly = 'Tidak Tahu' THEN '3'
		END AS v10,
		CASE
		WHEN a.meningoencephalitis = 'Ya' THEN '1'
		WHEN a.meningoencephalitis = 'Tidak' THEN '2'
		WHEN a.meningoencephalitis = 'Tidak Tahu' THEN '3'
		END AS v11,
		CASE
		WHEN a.ikterik = 'Ya' THEN '1'
		WHEN a.ikterik = 'Tidak' THEN '2'
		WHEN a.ikterik = 'Tidak Tahu' THEN '3'
		END AS v12,
		CASE
		WHEN a.splenomegaly = 'Ya' THEN '1'
		WHEN a.splenomegaly = 'Tidak' THEN '2'
		WHEN a.splenomegaly = 'Tidak Tahu' THEN '3'
		END AS v13,
		CASE
		WHEN a.developmental_delay = 'Ya' THEN '1'
		WHEN a.developmental_delay = 'Tidak' THEN '2'
		WHEN a.developmental_delay = 'Tidak Tahu' THEN '3'
		END AS v14,
		CASE
		WHEN a.radiolucent = 'Ya' THEN '1'
		WHEN a.radiolucent = 'Tidak' THEN '2'
		WHEN a.radiolucent = 'Tidak Tahu' THEN '3'
		END AS v15,
		CASE
		WHEN a.other_abnormal = 'Ya' THEN '1'
		WHEN a.other_abnormal = 'Tidak' THEN '2'
		WHEN a.other_abnormal = 'Tidak Tahu' THEN '3'
		END AS v16,
		a.dt_other_abnormal AS vo16,
		e.jumlah_kehamilan_sebelumnya,
		e.umur_ibu,
		e.hari_pertama_haid,
		CASE
		WHEN e.conjunctivitis = 'Ya' THEN '1'
		WHEN e.conjunctivitis = 'Tidak' THEN '2'
		WHEN e.conjunctivitis = 'Tidak Tahu' THEN '3'
		END AS conjunctivitis,
		DATE_FORMAT(e.conjunctivitis_date, '%d-%m-%Y') AS conjunctivitis_date,
		CASE
		WHEN e.pilek = 'Ya' THEN '1'
		WHEN e.pilek = 'Tidak' THEN '2'
		WHEN e.pilek = 'Tidak Tahu' THEN '3'
		END AS pilek,
		DATE_FORMAT(e.pilek_date, '%d-%m-%Y') AS pilek_date,
		CASE
		WHEN e.batuk = 'Ya' THEN '1'
		WHEN e.batuk = 'Tidak' THEN '2'
		WHEN e.batuk = 'Tidak Tahu' THEN '3'
		END AS batuk,
		DATE_FORMAT(e.batuk_date, '%d-%m-%Y') AS batuk_date,
		CASE
		WHEN e.ruam_makulopapular = 'Ya' THEN '1'
		WHEN e.ruam_makulopapular = 'Tidak' THEN '2'
		WHEN e.ruam_makulopapular = 'Tidak Tahu' THEN '3'
		END AS ruam_makulopapular,
		DATE_FORMAT(e.ruam_makulopapular_date, '%d-%m-%Y') AS ruam_makulopapular_date,
		CASE
		WHEN e.pembengkakan_kelenjar_limfa = 'Ya' THEN '1'
		WHEN e.pembengkakan_kelenjar_limfa = 'Tidak' THEN '2'
		WHEN e.pembengkakan_kelenjar_limfa = 'Tidak Tahu' THEN '3'
		END AS pembengkakan_kelenjar_limfa,
		DATE_FORMAT(e.pembengkakan_kelenjar_limfa_date, '%d-%m-%Y') AS pembengkakan_kelenjar_limfa_date,
		CASE
		WHEN e.demam = 'Ya' THEN '1'
		WHEN e.demam = 'Tidak' THEN '2'
		WHEN e.demam = 'Tidak Tahu' THEN '3'
		END AS demam,
		DATE_FORMAT(e.demam_date, '%d-%m-%Y') AS demam_date,
		CASE
		WHEN e.arthralgia = 'Ya' THEN '1'
		WHEN e.arthralgia = 'Tidak' THEN '2'
		WHEN e.arthralgia = 'Tidak Tahu' THEN '3'
		END AS arthralgia,
		DATE_FORMAT(e.arthralgia_date, '%d-%m-%Y') AS arthralgia_date,
		CASE
		WHEN e.komplikasi_lain = 'Ya' THEN '1'
		WHEN e.komplikasi_lain = 'Tidak' THEN '2'
		WHEN e.komplikasi_lain = 'Tidak Tahu' THEN '3'
		END AS komplikasi_lain,
		DATE_FORMAT(e.komplikasi_lain_date, '%d-%m-%Y') AS komplikasi_lain_date,
		CASE
		WHEN e.vaksinasi_rubella = 'Ya' THEN '1'
		WHEN e.vaksinasi_rubella = 'Tidak' THEN '2'
		WHEN e.vaksinasi_rubella = 'Tidak Tahu' THEN '3'
		END AS vaksinasi_rubella,
		DATE_FORMAT(e.vaksinasi_rubella_date, '%d-%m-%Y') AS vaksinasi_rubella_date,
		CASE
		WHEN e.diagnosa_rubella = 'Ya' THEN '1'
		WHEN e.diagnosa_rubella = 'Tidak' THEN '2'
		WHEN e.diagnosa_rubella = 'Tidak Tahu' THEN '3'
		END AS diagnosa_rubella,
		DATE_FORMAT(e.tgl_diagnosa_rubella, '%d-%m-%Y') AS diagnosa_rubella_date,
		CASE
		WHEN e.kontak_ruam_makulopapular = 'Ya' THEN '1'
		WHEN e.kontak_ruam_makulopapular = 'Tidak' THEN '2'
		WHEN e.kontak_ruam_makulopapular = 'Tidak Tahu' THEN '3'
		END AS kontak_ruam_makulopapular,
		e.umur_hamil_kontak_ruam_makulopapular,
		e.desc_kontak_ruam_makulopapular,
		CASE
		WHEN e.pergi_waktu_hamil = 'Ya' THEN '1'
		WHEN e.pergi_waktu_hamil = 'Tidak' THEN '2'
		WHEN e.pergi_waktu_hamil = 'Tidak Tahu' THEN '3'
		END AS pergi_waktu_hamil,
		e.umur_hamil_waktu_pergi,
		e.desc_pergi_waktu_hamil,
		CASE
		WHEN b.spesimen = 'Ya' THEN '1'
		WHEN b.spesimen = 'Tidak' THEN '2'
		WHEN b.spesimen = 'Tidak Tahu' THEN '3'
		END AS spesimen,
		DATE_FORMAT(b.tgl_ambil_serum1, '%d-%m-%Y') AS tgl_ambil_serum1,
		DATE_FORMAT(b.tgl_kirim_serum1, '%d-%m-%Y') AS tgl_kirim_serum1,
		DATE_FORMAT(b.tgl_tiba_serum1, '%d-%m-%Y') AS tgl_tiba_serum1,
		DATE_FORMAT(b.tgl_ambil_serum2, '%d-%m-%Y') AS tgl_ambil_serum2,
		DATE_FORMAT(b.tgl_kirim_serum2, '%d-%m-%Y') AS tgl_kirim_serum2,
		DATE_FORMAT(b.tgl_tiba_serum2, '%d-%m-%Y') AS tgl_tiba_serum2,
		DATE_FORMAT(b.tgl_ambil_throat_swab, '%d-%m-%Y') AS tgl_ambil_throat_swab,
		DATE_FORMAT(b.tgl_kirim_throat_swab, '%d-%m-%Y') AS tgl_kirim_throat_swab,
		DATE_FORMAT(b.tgl_tiba_throat_swab, '%d-%m-%Y') AS tgl_tiba_throat_swab,
		DATE_FORMAT(b.tgl_ambil_urine, '%d-%m-%Y') AS tgl_ambil_urine,
		DATE_FORMAT(b.tgl_kirim_urine, '%d-%m-%Y') AS tgl_kirim_urine,
		DATE_FORMAT(b.tgl_tiba_urine, '%d-%m-%Y') AS tgl_tiba_urine,
		CASE
		WHEN b.hasil_igm_serum1 = 'Positif' THEN '1'
		WHEN b.hasil_igm_serum1 = 'Negatif' THEN '2'
		END AS hasil_igm_serum1,
		b.virus_igm_serum1,
		DATE_FORMAT(b.tgl_igm_serum1, '%d-%m-%Y') AS tgl_igm_serum1,
		CASE
		WHEN b.hasil_igm_serum2 = 'Positif' THEN '1'
		WHEN b.hasil_igm_serum2 = 'Negatif' THEN '2'
		END AS hasil_igm_serum2,
		b.virus_igm_serum2,
		DATE_FORMAT(b.tgl_igm_serum2, '%d-%m-%Y') AS tgl_igm_serum2,
		CASE
		WHEN b.hasil_igg_serum1 = 'Positif' THEN '1'
		WHEN b.hasil_igg_serum1 = 'Negatif' THEN '2'
		END AS hasil_igg_serum1,
		b.virus_igg_serum1,
		DATE_FORMAT(b.tgl_igg_serum1, '%d-%m-%Y') AS tgl_igg_serum1,
		b.kadar_igg_serum1,
		CASE
		WHEN b.hasil_igg_serum2 = 'Positif' THEN '1'
		WHEN b.hasil_igg_serum2 = 'Negatif' THEN '2'
		END AS hasil_igg_serum2,
		b.virus_igg_serum2,
		DATE_FORMAT(b.tgl_igg_serum2, '%d-%m-%Y') AS tgl_igg_serum2,
		b.kadar_igg_serum2,
		CASE
		WHEN b.hasil_isolasi = 'Positif' THEN '1'
		WHEN b.hasil_isolasi = 'Negatif' THEN '2'
		END AS hasil_isolasi,
		b.virus_isolasi,
		DATE_FORMAT(b.tgl_isolasi, '%d-%m-%Y') AS tgl_isolasi
		FROM
		db_pro_surveilans.crs a
		JOIN db_pro_surveilans.hasil_uji_lab_crs b ON a.id_hasil_uji_lab_crs = b.id_hasil_uji_lab_crs
		JOIN db_pro_surveilans.pasien c ON a.id_pasien = c.id_pasien
		JOIN db_pro_surveilans.pelapor d ON a.id_pelapor = d.id_pelapor
		JOIN db_pro_surveilans.pe_crs e ON a.id_pe_crs = e.id_pe_crs
		LEFT JOIN db_pro_surveilans.puskesmas g ON a.id_tempat_periksa = g.puskesmas_id
		LEFT JOIN db_pro_surveilans.rumahsakit2 h ON a.id_tempat_periksa = h.id
		WHERE
		a.deleted_at IS NULL
		-- AND (g.kode_prop = '34' OR h.kode_prop = '34')
		AND a.id_crs > 2397
		GROUP BY a.id_crs
		");
    }

    public static function gEpidCrs()
    {
        return DB::select("
			SELECT
		a.id_crs AS id_crs_old,
		CASE
		WHEN e.diagnosa_rubella = 'Ya' THEN '1'
		WHEN e.diagnosa_rubella = 'Tidak' THEN '2'
		WHEN e.diagnosa_rubella = 'Tidak Tahu' THEN '3'
		END AS diagnosa_rubella,
		DATE_FORMAT(e.tgl_diagnosa_rubella, '%d-%m-%Y') AS diagnosa_rubella_date,
		CASE
		WHEN e.kontak_ruam_makulopapular = 'Ya' THEN '1'
		WHEN e.kontak_ruam_makulopapular = 'Tidak' THEN '2'
		WHEN e.kontak_ruam_makulopapular = 'Tidak Tahu' THEN '3'
		END AS kontak_ruam_makulopapular,
		e.umur_hamil_kontak_ruam_makulopapular,
		e.desc_kontak_ruam_makulopapular,
		CASE
		WHEN e.pergi_waktu_hamil = 'Ya' THEN '1'
		WHEN e.pergi_waktu_hamil = 'Tidak' THEN '2'
		WHEN e.pergi_waktu_hamil = 'Tidak Tahu' THEN '3'
		END AS pergi_waktu_hamil,
		e.umur_hamil_waktu_pergi,
		e.desc_pergi_waktu_hamil
		FROM
		db_pro_surveilans.crs a
		JOIN db_pro_surveilans.pe_crs e ON a.id_pe_crs = e.id_pe_crs
		LEFT JOIN db_pro_surveilans.puskesmas g ON a.id_tempat_periksa = g.puskesmas_id
		LEFT JOIN db_pro_surveilans.rumahsakit2 h ON a.id_tempat_periksa = h.id
		WHERE
		a.deleted_at IS NULL
		-- AND (g.kode_prop = '34' OR h.kode_prop = '34')
		AND a.id_crs > 2397
		GROUP BY a.id_crs
			");
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB, Sentinel, Auth;

class TxSampelLab extends Model
{
    static function insert($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['created_by'] = (Sentinel::check())?Sentinel::check()->id : Auth::user()->id;
		$data['updated_by'] = (Sentinel::check())?Sentinel::check()->id : Auth::user()->id;
		return DB::table('trx_klinis')->insertGetId($data);
	}

	static function getDataSampel($where=array(),$index){
		$query = DB::table('view_lab_'.$index);
		foreach ($where as $key => $val) {
			if (!empty($val)) {
				$query->where($key, $val);
			}
		}
		return $query->get();
	}

  static function postDeleteSampel($id){
    $data['deleted_at'] = date('Y-m-d H:i:s');
    $data['deleted_by'] = (Sentinel::check())?Sentinel::check()->id : Auth::user()->id;
    return DB::table('trx_spesimen')->where('id_trx_case',$id)->update($data);
  }
	static function pull($where=array(), $data)
	{
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['updated_by'] = (Sentinel::check())?Sentinel::check()->id : Auth::user()->id;
		$query = DB::table('trx_klinis');
		foreach ($where as $key => $val) {
			if(!empty($val)){
				$query->where($key, $val);
			}
		}
		return $query->update($data);
	}
}

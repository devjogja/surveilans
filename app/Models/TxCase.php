<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB, Sentinel, Auth;

class TxCase extends Model
{
	static function getDataAnalisa($where, $case)
	{
		$query = DB::table($case);
		foreach ($where as $key => $val) {
			if (!empty($val)) {
				$query->where($key, $val);
			}
		}
		return $query->get();
	}
	static function getDataAnalisaTime($where, $case, $indicator, $range)
	{
		$query = DB::table($case);
		foreach ($where as $key => $val) {
			if (!empty($val)) {
				$query->where($key, $val);
			}
		}
		$query->whereBetween($indicator, $range);
		return $query->get();
	}

	static function gDataSample($case)
	{
		$query = DB::table('d_lab_'.$case);
		return $query->get();
	}

	static function getDataSample($where, $case)
	{
		$query = DB::table('view_list_spesimen');
		foreach ($where as $key => $val) {
			if (!empty($val)) {
				$query->where($key, $val);
			}
		}
		return $query->get();
	}

	static function getDataCampak($where = array())
	{
		$query = DB::table('campak');
		foreach ($where as $key => $val) {
			if (!empty($val)) {
				$query->where($key, $val);
			}
		}
		return $query->get();
	}

	static function getDataAfp($where = array())
	{
		$query = DB::table('afp');
		foreach ($where as $key => $val) {
			if (!empty($val)) {
				$query->where($key, $val);
			}
		}
		return $query->get();
	}

	public static function getDataDifteri($where = array())
	{
		$query = DB::table('difteri');
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		return $query->get();
	}

	public static function getDataTetanus($where = array())
	{
		$query = DB::table('tetanus');
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		return $query->get();
	}

	public static function getDataCrs($where = array())
	{
		$query = DB::table('crs');
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		return $query->get();
	}

	public static function postDelete($id)
	{
		$data['deleted_at'] = date('Y-m-d H:i:s');
		$data['deleted_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
		DB::table('trx_notification')->where('id_trx_case', $id)->update($data);
		return DB::table('trx_case')->where('id', $id)->update($data);
	}
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pelapor extends Model
{
	public static function qq()
	{
		$q = "
		SELECT
		a.id_trx_case AS id_trx_case,
		a.nama_pelapor,
		c.name AS provinsi_pelapor,
		b.name AS kabupaten_pelapor,
		DATE_FORMAT(a.tgl_laporan,'%d-%m-%Y') AS tgl_laporan_pelapor,
		DATE_FORMAT(a.tgl_investigasi,'%d-%m-%Y') AS tgl_investigasi_pelapor
		FROM
		trx_pelapor a
		JOIN mst_kabupaten b ON a.code_kabupaten_pelapor = b.code
		JOIN mst_provinsi c ON b.code_provinsi = c.code
		GROUP BY a.id
		";
		return $q;
	}
}
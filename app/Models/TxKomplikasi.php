<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class TxKomplikasi extends Model {
	public static function insert($data) {
		// truncate data
		$query = DB::table('trx_komplikasi')->where('id_trx_klinis', $data['id_trx_klinis'])->delete();
		// insert data
		$dt = [];
		if (isset($data['komplikasi']) && count($data['komplikasi']) > 0) {
			foreach ($data['komplikasi'] as $key => $val) {
				$dt[] = array(
					'id_trx_klinis'    => $data['id_trx_klinis'],
					'name'             => $val,
					'other_komplikasi' => null,
					);
			}
		}
		if (isset($data['otherKomplikasi'])) {
			$dx = explode(',', $data['otherKomplikasi']);
			foreach ($dx as $key => $val) {
				if (trim($val) != '') {
					$dt[] = array(
						'id_trx_klinis'    => $data['id_trx_klinis'],
						'name'             => null,
						'other_komplikasi' => $val,
						);
				}
			}
		}
		return DB::table('trx_komplikasi')->insert($dt);
	}

	public static function getData($where = array()) {
		$query = DB::table('trx_komplikasi AS a');
		$query->select('a.name', 'a.other_komplikasi');
		$query->join('trx_klinis AS b', 'a.id_trx_klinis', '=', 'b.id');
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		return $query->get();
	}

}

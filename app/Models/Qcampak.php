<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class QCampak extends Model
{
	public static function view(){
		$q = "(SELECT
					a.id AS id_trx_case,
					a.id_role,
					a.id_faskes,
					b.name AS nama_pasien,
					b.nik,
					a.no_rm,
					c.name AS agama,
					d.name AS nama_ortu,
					b.jenis_kelamin,
					CASE
					WHEN b.jenis_kelamin = 'L' THEN 'Laki-laki'
					WHEN b.jenis_kelamin = 'P' THEN 'Perempuan'
					WHEN b.jenis_kelamin = 'T' THEN 'Tidak Jelas'
					ELSE 'Belum diisi' END AS jenis_kelamin_txt,
					DATE_FORMAT(b.tgl_lahir, '%d-%m-%Y') AS tgl_lahir,
					b.umur_thn,
					b.umur_bln,
					b.umur_hari,
					b.alamat,
					e.code AS code_kelurahan_pasien,
					e.name AS kelurahan_pasien,
					f.code AS code_kecamatan_pasien,
					f.name AS kecamatan_pasien,
					g.code AS code_kabupaten_pasien,
					g.name AS kabupaten_pasien,
					h.code AS code_provinsi_pasien,
					h.name AS provinsi_pasien,
					b.longitude,
					b.latitude,
					CASE
					WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',i.name)
					WHEN a.id_role = 2 THEN j.name
					END name_faskes,
					a.no_epid,
					a.no_epid_lama,
					DATE_FORMAT(k.tgl_imunisasi_campak, '%d-%m-%Y') AS tgl_imunisasi_campak,
					k.jml_imunisasi_campak,
					CASE
					WHEN k.jml_imunisasi_campak = 7 THEN 'Tidak'
					WHEN k.jml_imunisasi_campak = 8 THEN 'Tidak Tahu'
					WHEN k.jml_imunisasi_campak = 9 THEN 'Belum Pernah'
					WHEN k.jml_imunisasi_campak IN ('1','2','3','4','5','6') THEN CONCAT(k.jml_imunisasi_campak, 'x')
					ELSE 'Belum diisi' END AS jml_imunisasi_campak_txt,
					DATE_FORMAT(k.tgl_mulai_demam, '%d-%m-%Y') AS tgl_mulai_demam,
					DATE_FORMAT(k.tgl_mulai_rash, '%d-%m-%Y') AS tgl_mulai_rash,
					DATE_FORMAT(k.tgl_mulai_rash, '%d-%m-%Y') AS tgl_sakit_date,
					YEAR(k.tgl_mulai_rash) AS thn_sakit,
					DATE_FORMAT(k.tgl_laporan_diterima, '%d-%m-%Y') AS tgl_laporan_diterima,
					DATE_FORMAT(k.tgl_pelacakan, '%d-%m-%Y') AS tgl_pelacakan,
					k.vitamin_A,
					CASE
					WHEN k.vitamin_A = 1 THEN 'Ya'
					WHEN k.vitamin_A = 2 THEN 'Tidak'
					END AS vitamin_A_txt,
					k.keadaan_akhir,
					CASE
					WHEN k.keadaan_akhir = 1 THEN 'Hidup'
					WHEN k.keadaan_akhir = 2 THEN 'Meninggal'
					END AS keadaan_akhir_txt,
					k.jenis_kasus,
					CASE
					WHEN k.jenis_kasus = 1 THEN 'KLB'
					WHEN k.jenis_kasus = 2 THEN 'Bukan KLB'
					END AS jenis_kasus_txt,
					k.klb_ke,
					k.status_kasus,
					CASE
					WHEN k.status_kasus = 1 THEN 'Index'
					WHEN k.status_kasus = 2 THEN 'Bukan Index'
					END AS status_kasus_txt,
					GROUP_CONCAT(DISTINCT CONCAT(l.id_gejala,'_',DATE_FORMAT(l.tgl_kejadian, '%d-%m-%Y')) ORDER BY l.id_gejala ASC SEPARATOR '|') AS gejala,
					GROUP_CONCAT(DISTINCT m.name SEPARATOR '|') komplikasi,
					GROUP_CONCAT(DISTINCT m.other_komplikasi SEPARATOR ',') other_komplikasi
					FROM
					trx_case a
					JOIN ref_pasien b ON a.id_pasien = b.id
					LEFT JOIN mst_religion c ON b.agama = c.id
					JOIN ref_family d ON b.id = d.id_pasien
					LEFT JOIN mst_kelurahan e ON b.code_kelurahan = e.code
					LEFT JOIN mst_kecamatan f ON e.code_kecamatan = f.code
					LEFT JOIN mst_kabupaten g ON f.code_kabupaten = g.code
					LEFT JOIN mst_provinsi h ON g.code_provinsi = h.code
					LEFT JOIN view_puskesmas i ON a.id_faskes = i.id AND a.id_role = 1
					LEFT JOIN view_rumahsakit j ON a.id_faskes = j.id AND a.id_role = 2
					JOIN trx_klinis k ON a.id = k.id_trx_case
					LEFT JOIN trx_gejala l ON k.id = l.id_trx_klinis
					LEFT JOIN trx_komplikasi m ON k.id = m.id_trx_klinis
					WHERE
					a.id_case = '1' AND a.deleted_at IS NULL
					GROUP BY a.id
				) AS a";
		return $q;
	}
}
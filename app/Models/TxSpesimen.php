<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class TxSpesimen extends Model {
    public static function insert($where, $data) {
        // truncate data
        $query = DB::table('trx_spesimen')->where($where)->delete();
        // insert data
        return DB::table('trx_spesimen')->insert($data);
    }

    public static function insertHasilLab($where, $data) {
        // truncate data
        $query = DB::table('trx_hasil_lab')->where($where)->delete();
        // insert data
        return DB::table('trx_hasil_lab')->insert($data);
    }

    public static function getData($where = array()) {
        $query = DB::table('trx_spesimen AS a');
        foreach ($where as $key => $val) {
            $query->where($key, $val);
        }
        return $query->get();
    }
}

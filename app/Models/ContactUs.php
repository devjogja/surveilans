<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB, Sentinel, Auth;

class ContactUs extends Model
{
  static function insert($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = (Sentinel::check())?Sentinel::check()->id : Auth::user()->id;
    $data['updated_by'] = (Sentinel::check())?Sentinel::check()->id : Auth::user()->id;
    $query = DB::table('contact_us')->insert($data);
    // return DB::table('faq')->insertGetId($data);
    return $query;
  }

}

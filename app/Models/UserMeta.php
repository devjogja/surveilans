<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserMeta extends Model
{
    protected $table = 'users_meta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_user', 'alamat', 'instansi', 'jabatan', 'no_telp', 'jenis_kelamin'];

    public static function getUpdate($query=array(), $data=array())
    {
        $usersMeta = DB::table('users_meta');
        $usersMeta->where('deleted_at', NULL);
        foreach ($query as $key => $val) {
            $usersMeta->where($key, $val);
        }
        return $usersMeta->update($data);
    }
    public static function insertData($table, $data) {
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        return DB::table($table)->insertGetId($data);
    }
}

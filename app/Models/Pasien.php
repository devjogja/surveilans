<?php

namespace App\Models;

use Auth;
use DB;
use Illuminate\Database\Eloquent\Model;
use Sentinel;

class Pasien extends Model
{
    public static function insert($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
        $data['updated_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
        return DB::table('ref_pasien')->insertGetId($data);
    }

    public static function getData($where)
    {
        $query = DB::table('view_pasien AS a');
        if ($where) {
            foreach ($where as $key => $val) {
                $query->where($key, 'like', '%' . $val . '%');
            }
        }
        $query->limit('20');
        return $query->get();
    }

    public static function getDataLab($where, $case)
    {
        $q = DB::table($case . ' AS a');
        $q->where(function ($query) use ($where) {
            $query->where('a.no_epid', 'like', '%' . $where['key'] . '%');
            $query->orWhere('a.name_pasien', 'like', '%' . $where['key'] . '%');
        });
        return $q->get();
    }

    public static function pull($where = array(), $data)
    {
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['updated_by'] = (Sentinel::check()) ? Sentinel::check()->id : Auth::user()->id;
        $query = DB::table('ref_pasien');
        foreach ($where as $key => $val) {
            $query->where($key, $val);
        }
        return $query->update($data);
    }
}

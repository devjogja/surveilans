<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Kasus extends Model {
    public static function getData($where = array()) {
        $dt = DB::table('mst_case AS a');
        $dt->select('a.id', 'a.code', 'a.alias', 'a.name', 'a.description', 'a.image');
        $dt->where('a.deleted_at', NULL);
        foreach ($where as $key => $val) {
            $dt->where($key, $val);
        }
        return $dt->get();
    }
}

<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Puskesmas extends Model
{
    protected $table = 'mst_puskesmas';
    protected $fillable = ['id', 'name', 'code_faskes', 'alamat', 'location', 'longitude', 'latitude', 'konfirm_code', 'code_kelurahan', 'code_kecamatan', 'code_kabupaten', 'code_provinsi', 'created_by'];

    public static function getData($where = array())
    {
        $query = DB::table('view_puskesmas AS a');
        $query->select('a.id', 'a.name', 'a.code_faskes', 'a.alamat', 'a.location', 'a.longitude', 'a.latitude', 'a.konfirm_code', 'a.code_kelurahan', 'a.code_kecamatan', 'a.code_kabupaten', 'a.code_provinsi', 'a.name_kelurahan', 'a.name_kecamatan', 'a.name_kabupaten', 'a.name_provinsi');
        foreach ($where as $key => $val) {
            if (!empty($val)) {
                $query->where($key, $val);
            }
        }
        return $query->get();
    }

    public static function qq()
    {
        $q = "
        SELECT
            a.id,
            a.code_faskes,
            a.name,
            a.code_kecamatan,
            a.code_kabupaten,
            a.code_provinsi,
            b.name name_kecamatan,
            c.name name_kabupaten,
            d.name name_provinsi
            FROM
            mst_puskesmas a
            JOIN mst_kecamatan b ON a.code_kecamatan = b.code
            JOIN mst_kabupaten c ON b.code_kabupaten = c.code
            JOIN mst_provinsi d ON c.code_provinsi = d.code";
        return $q;
    }

    public static function getUpdate($where = array(), $data = array())
    {
        $query = DB::table('mst_puskesmas');
        $query->where('deleted_at', null);
        foreach ($where as $key => $val) {
            $query->where($key, $val);
        }
        return $query->update($data);
    }
}

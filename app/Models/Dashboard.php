<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB,Helper;

class Dashboard extends Model
{
	static function getSpesimen()
	{
		$roleuser = Helper::role();
		if ($roleuser->code_role=='puskesmas') {
			$wQuery = ['a.id_role'=>'1','a.id_faskes'=>$roleuser->id_faskes];
		}elseif($roleuser->code_role=='rs'){
			$wQuery = ['a.id_role'=>'2','a.id_faskes'=>$roleuser->id_faskes];
		}elseif($roleuser->code_role=='kabupaten'){
			$wQuery = ['a.code_kabupaten_faskes'=>$roleuser->id_faskes];
		}elseif($roleuser->code_role=='provinsi'){
			$wQuery = ['a.code_provinsi_faskes'=>$roleuser->id_faskes];
		}else{
			$wQuery = [];
		}
		$wkp  = DB::table('view_wilayah_kerja_puskesmas')->select(['code_kelurahan'])->where(['code_faskes' => $roleuser->code_faskes])->get();
		$dwkp = array();
		foreach ($wkp as $key => $val) {
			$dwkp[] = $val->code_kelurahan;
		}
		$case = DB::table('mst_case')->whereNull('deleted_at')->get();
		foreach ($case as $key => $val) {
			$p = DB::table('view_list_spesimen AS a')->select('a.id_trx_case');
			$p->where('a.id_case', $val->id);
			$p->where('a.thn_sakit',date('Y'));
			$p->whereNull('a.klasifikasi_final');
			if ($wQuery) {
				$p->where(function ($p) use ($roleuser, $wQuery, $dwkp){
					$p->where($wQuery);
					if ($roleuser->code_role=='puskesmas' AND $dwkp) {
						$p->orWhere(function ($query) use ($dwkp) {
		                    $query->where('a.id_role', '2');
		                    $query->whereIn('a.code_kelurahan_pasien', $dwkp);
		                });
					}
				});
			}
			$p->groupBy('a.id_trx_case');
			$dt[$val->alias]['pending'] = DB::table( DB::raw("({$p->toSql()}) as sub") )->mergeBindings($p)->count();

			$s = DB::table('view_list_spesimen AS a')->select('a.id_trx_case');
			$s->where('a.id_case', $val->id);
			$s->where('a.thn_sakit', date('Y'));
			if ($wQuery) {
				$s->where(function ($s) use ($roleuser, $wQuery, $dwkp){
					$s->where($wQuery);
					if ($roleuser->code_role=='puskesmas' and $dwkp) {
						$s->orWhere(function ($query) use ($dwkp) {
		                    $query->where('a.id_role', '2');
		                    $query->whereIn('a.code_kelurahan_pasien', $dwkp);
		                });
					}
				});
			}
			$s->groupBy('a.id_trx_case');
			$dt[$val->alias]['spesimen'] = DB::table( DB::raw("({$s->toSql()}) as sub") )->mergeBindings($s)->count();

			$t = DB::table('view_list_case AS a')->select('a.id_trx_case');
			$t->where('a.id_case', $val->id);
			$t->where('a.thn_sakit', date('Y'));
			if ($wQuery) {
				$t->where(function ($t) use ($roleuser, $wQuery, $dwkp){
					$t->where($wQuery);
					if ($roleuser->code_role=='puskesmas') {
						$t->orWhere(function ($query) use ($dwkp) {
		                    $query->where('a.id_role', '2');
		                    $query->whereIn('a.code_kelurahan_pasien', $dwkp);
		                });
					}
				});
			}
			$t->groupBy('a.id_trx_case');
			$dt[$val->alias]['total'] = DB::table( DB::raw("({$t->toSql()}) as sub") )->mergeBindings($t)->count();
		}
		return $dt;
	}
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB, Sentinel, Auth;

class TxNotification extends Model
{
	public static function getData($where) {
		$query = DB::table('view_notification');
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		$query->limit('20');
		return $query;
	}

    static function insert($where,$data)
	{
		// truncate data
		$query = DB::table('trx_notification')->where($where)->delete();
		// insert data
		foreach ($data as $key => $val) {
			$val['created_at'] = date('Y-m-d H:i:s');
			$val['updated_at'] = date('Y-m-d H:i:s');
			$val['created_by'] = (Sentinel::check())?Sentinel::check()->id : Auth::user()->id;
			$val['updated_by'] = (Sentinel::check())?Sentinel::check()->id : Auth::user()->id;
			$notif = DB::table('trx_notification')->insertGetId($val);
		}
		return $notif;
	}
}

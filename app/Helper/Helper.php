<?php

/**
 * Data table Class Helper
 * @author      Hanif Burhanudin | _boerhan
 * @link        boerhan.jogja@gmail.com
 */

namespace App\Helper;

use App\Models\Education;
use App\Models\Gejala;
use App\Models\Kasus;
use App\Models\Komplikasi;
use App\Models\Laboratorium;
use App\Models\Occupation;
use App\Models\Religion;
use App\Models\Tx;
use App\Models\Wilayah;
use App\User;
use Sentinel;
use Session;

class Helper
{
    public static function getAge($param)
    {
        $diff = abs($param['tgl_sakit'] - $param['tgl_lahir']);
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
        $response = array(
            'tgl_lahir' => null,
            'years' => $years,
            'month' => $months,
            'day' => $days,
        );
        return $response;
    }

    public static function dateFormat($date)
    {
        if (!empty($date) && $date !== '1970-01-01' && $date !== "0000-00-00" && $date !== "00-00-0000" && $date != 'null') {
            $de = explode('-', $date);
            $ds = explode('/', $date);
            if (count($de) == '3') {
                $response = $de[2] . '-' . $de[1] . '-' . $de[0];
            } else {
                $response = $ds[2] . '-' . $ds[1] . '-' . $ds[0];
            }
            return $response;
        } else {
            return null;
        }
    }

    public static function humanDate($date, $type = null)
    {
        if ($date == '01-01-1970' || $date == null || $date == '1970-01-01' || $date == '00-00-0000' || $date == '0000-00-00' || $date == '-') {
            return '';
        } else {
            if ($type !== null) {
                return date('d-m-Y H:i:s', strtotime($date));
            } else {
                return date('d-m-Y', strtotime($date));
            }
        }
    }

    public static function yesno($id = '')
    {
        $val = '-';
        if ($id == '1') {
            $val = 'Ya';
        } elseif ($id == '2') {
            $val = 'Tidak';
        } elseif ($id == '3') {
            $val = 'Tidak tahu';
        }
        return $val;
    }

    public static function session_flash($status, $title = null, $message = null)
    {
        $message = array(
            'status' => $status,
            'title' => $title,
            'message' => $message,
        );
        Session::flash('flash_message', $message);
    }

    public static function role($faskes = [])
    {
        if (count($faskes) > 0) {
            $code_role = $faskes['code_role'];
            $id_faskes = $faskes['id_faskes'];
        } else {
            $code_role = Session::get('id_role');
            $id_faskes = Session::get('id_faskes');
        }
        $response = [];
        if (!empty($code_role) && !empty($id_faskes)) {
            $role = Tx::getData('roles', ['code' => $code_role])->first();
            $response['id_role'] = $role->id;
            $response['code_role'] = $role->code;
            $response['name_role'] = $role->name;
            $response['id_faskes'] = $id_faskes;
            $response['name_faskes'] = null;
            $response['code_faskes'] = null;
            $response['alamat_faskes'] = null;
            $response['code_wilayah_faskes'] = null;
            $response['code_kecamatan_faskes'] = null;
            $response['name_kecamatan_faskes'] = null;
            $response['code_kabupaten_faskes'] = null;
            $response['name_kabupaten_faskes'] = null;
            $response['code_provinsi_faskes'] = null;
            $response['name_provinsi_faskes'] = null;
            if ($code_role == 'puskesmas') {
                $dPuskesmas = Tx::getData('view_puskesmas', ['id' => $id_faskes])->first();
                $response['name_faskes'] = $dPuskesmas->name;
                $response['code_faskes'] = $dPuskesmas->code_faskes;
                $response['alamat_faskes'] = $dPuskesmas->alamat;
                $response['code_wilayah_faskes'] = $dPuskesmas->code_kecamatan;
                $response['code_kecamatan_faskes'] = $dPuskesmas->code_kecamatan;
                $response['name_kecamatan_faskes'] = $dPuskesmas->name_kecamatan;
                $response['code_kabupaten_faskes'] = $dPuskesmas->code_kabupaten;
                $response['name_kabupaten_faskes'] = $dPuskesmas->name_kabupaten;
                $response['code_provinsi_faskes'] = $dPuskesmas->code_provinsi;
                $response['name_provinsi_faskes'] = $dPuskesmas->name_provinsi;
            } else if ($code_role == 'rs') {
                $dRs = Tx::getData('view_rumahsakit', ['id' => $id_faskes])->first();
                $response['name_faskes'] = $dRs->name;
                $response['code_faskes'] = $dRs->code_faskes;
                $response['alamat_faskes'] = $dRs->alamat;
                $response['code_wilayah_faskes'] = $dRs->code_kabupaten;
                $response['code_kecamatan_faskes'] = null;
                $response['name_kecamatan_faskes'] = null;
                $response['code_kabupaten_faskes'] = $dRs->code_kabupaten;
                $response['name_kabupaten_faskes'] = $dRs->name_kabupaten;
                $response['code_provinsi_faskes'] = $dRs->code_provinsi;
                $response['name_provinsi_faskes'] = $dRs->name_provinsi;
            } else if ($code_role == 'provinsi') {
                $dProvinsi = Tx::getData('mst_provinsi', ['code' => $id_faskes])->first();
                $response['name_faskes'] = $dProvinsi->name;
            } else if ($code_role == 'kabupaten') {
                $dKabupaten = Tx::getData('mst_kabupaten', ['code' => $id_faskes])->first();
                $response['name_faskes'] = $dKabupaten->name;
            } else if ($code_role == 'lab') {
                $dLab = Tx::getData('mst_laboratorium', ['id' => $id_faskes])->first();
                $response['name_faskes'] = $dLab->name;
                $response['code_faskes'] = $dLab->code;
                $response['alamat_faskes'] = $dLab->alamat;
            }
        }
        return (object) $response;
    }

    public static function getUser()
    {
        $response = array();
        if ($user = Sentinel::check()) {
            $r = User::getData(array('a.id' => $user->id));
            $response = $r[0];
        }
        return $response;
    }

    public static function getCase($where = array())
    {
        return Kasus::getData($where);
    }

    public static function getGejala($id_case, $group = null)
    {
        $where = array(
            'id_case' => $id_case,
            'group_type' => $group,
        );
        return Gejala::getData($where);
    }

    public static function getReligion($id_religion = null)
    {
        $where = [];
        if ($id_religion) {
            $where = ['id' => $id_religion];
        }
        $data = Religion::getData($where);
        $response = array();
        foreach ($data as $key => $val) {
            $response[$val->id] = $val->name;
        }
        return $response;
    }

    public static function getOccupation()
    {
        $data = Occupation::getData();
        $response = array();
        foreach ($data as $key => $val) {
            $response[$val->id] = $val->name;
        }
        return $response;
    }

    public static function getEducation()
    {
        $data = Education::getData();
        $response = array();
        foreach ($data as $key => $val) {
            $response[$val->id] = $val->name;
        }
        return $response;
    }

    public static function getKomplikasi($id_case)
    {
        $where = array('id_case' => $id_case);
        return Komplikasi::getData($where);
    }

    public static function getCategoryUmur($case = '')
    {
        if ($case == "crs") {
            $response = ['Kurang dari 6 Bulan', 'Lebih dari sama dengan 6 Bulan'];
        } else if ($case == 'tn') {
            $response = ['0-4 hari', '5-9 Hari', '10-14 Hari', '15-28 Hari', '> 28 Hari'];
        } else {
            $response = ['Dibawah 1 Tahun', '1-4 Tahun', '5-9 Tahun', '10-14 Tahun', 'Lebih dari sama dengan 15 Tahun'];
        }
        return $response;
    }

    public static function getProvince($code_provinsi = '')
    {
        $where = [];
        if ($code_provinsi) {
            $where = ['code' => $code_provinsi];
        }
        $data = Wilayah::getProvince($where);
        $response = array();
        foreach ($data as $key => $val) {
            $response[$key] = $val->name;
        }
        return $response;
    }

    public static function getLaboratorium($code = '')
    {
        $where = [];
        if ($code) {
            $where = ['code' => $code];
        }
        $data = Laboratorium::getData($where);
        $response = array();
        foreach ($data as $val) {
            $response[$val->id] = $val->name;
        }
        return $response;
    }

    public static function getARM($code = '')
    {
        $response = ['1' => 'Pri. Inoculation in L20B(L)', '2' => 'Pri. Inoculation in RD', '3' => 'Pass L-ve in L (LL)', '4' => 'Pass L+ve in R (LR)', '5' => 'Pass L-ve', '6' => 'L+ve in R (LLR)', '7' => 'Pass R -ve in R (RR)', '8' => 'Pass R +ve in L (RL)', '9' => 'Pass R+L+ to R (RLR)', '10' => 'Pass R-ve R+ve to LB20 (RRL)', '11' => 'Pass RRL to R (RRLR)'];
        return $response;
    }

    public static function getHasilSequencing($code = '')
    {
        $response = ['1' => 'Wild', '2' => 'Vaccine', '3' => 'VDPV', '4' => 'Wild+Vaccine', '5' => 'Other'];
        return $response;
    }

    public static function getLabArea($case, $id)
    {
        if ($case == "campak") {
            switch ($id) {
                case '1':
                    $response = [32];
                    break;
                case '2':
                    $response = [11, 12, 13, 14, 15, 16, 17, 18, 19, 36, 31, 61, 62, 63, 64, 65];
                    break;
                case '3':
                    $response = [35, 51, 52, 53, 71, 72, 73, 74, 75, 76, 81, 82, 91, 94];
                    break;
                case '4':
                    $response = [33, 34];
                    break;
            }
        } else if ($case == 'afp') {
            switch ($id) {
                case '1':
                    $response = [32, 33, 34];
                    break;
                case '2':
                    $response = [11, 12, 13, 14, 15, 16, 17, 18, 19, 36, 31, 61, 62, 63, 64, 65];
                    break;
                case '3':
                    $response = [35, 51, 52, 53, 71, 72, 73, 74, 75, 76, 81, 82, 91, 94];
                    break;
                case '4':
                    $response = [];
                    break;
            }
        }
        return $response;
    }

    public static function getMonth()
    {
        $response = [null => '--Pilih--',
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'];
        return $response;
    }
}

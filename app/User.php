<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password','id_user_lama'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getData($query=array())
    {
        $users = DB::table('view_profile_user AS a');
        foreach ($query as $key => $value) {
            $users->where($key, $value);
        }
        return $users->get();
    }

    public static function getUpdate($query=array(), $data=array())
    {
        $users = DB::table('users');
        $users->where('deleted_at', NULL);
        foreach ($query as $key => $value) {
            $users->where($key, $value);
        }
        return $users->update($data);
    }
}

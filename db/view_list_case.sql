SELECT
    a.id_case,
    a.id_faskes,
    a.id AS id_trx_case,
    a.no_epid,
    a.no_epid_klb,
    (CASE
    WHEN k.keadaan_akhir = 1 THEN 'Hidup'
    WHEN k.keadaan_akhir = 2 THEN 'Meninggal'
    END) AS keadaan_akhir_txt,
    d.rkf_name AS klasifikasi_final_txt,
    a.id_role,
    a.id_role_old,
    (CASE
    WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',i.name)
    WHEN a.id_role = 2 THEN j.name
    END) name_faskes,
    (CASE
    WHEN a.id_role = 1 THEN i.code_faskes
    WHEN a.id_role = 2 THEN j.code_faskes
    END) code_faskes,
    (CASE
    WHEN a.id_role = 1 THEN i.code_kecamatan
    WHEN a.id_role = 2 THEN j.code_kecamatan
    END) code_kecamatan_faskes,
    (CASE
    WHEN a.id_role = 1 THEN i.code_kabupaten
    WHEN a.id_role = 2 THEN j.code_kabupaten
    END) code_kabupaten_faskes,
    (CASE
    WHEN a.id_role = 1 THEN i.code_provinsi
    WHEN a.id_role = 2 THEN j.code_provinsi
    END) code_provinsi_faskes,
    a.jenis_input,
    (CASE
    WHEN a.jenis_input = 1 THEN 'Web'
    WHEN a.jenis_input = 2 THEN 'Android'
    WHEN a.jenis_input = 3 THEN 'Import'
    END) AS jenis_input_text,
    b.*,
    k.status_kasus,
    (CASE
    WHEN k.status_kasus = 1 THEN 'Index'
    WHEN k.status_kasus = 2 THEN 'Bukan Index'
    END) AS status_kasus_txt,
    l.id AS id_pe,
    a.id_faskes_old,
    DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
    DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
    (CASE
    WHEN l.id IS NULL THEN 'PE'
    ELSE 'Sudah PE'
    END) AS pe_text,
    (CASE
    WHEN a.id_case = 1 THEN k.tgl_mulai_rash
    WHEN a.id_case = 2 THEN k.tgl_mulai_lumpuh
    WHEN a.id_case = 3 THEN k.tgl_mulai_demam
    WHEN a.id_case = 4 THEN k.tgl_mulai_sakit
    WHEN a.id_case = 5 THEN k.tgl_periksa
    END) AS tgl_sakit_date,
    (CASE
    WHEN a.id_case = 1 THEN YEAR(k.tgl_mulai_rash)
    WHEN a.id_case = 2 THEN YEAR(k.tgl_mulai_lumpuh)
    WHEN a.id_case = 3 THEN YEAR(k.tgl_mulai_demam)
    WHEN a.id_case = 4 THEN YEAR(k.tgl_mulai_sakit)
    WHEN a.id_case = 5 THEN YEAR(k.tgl_periksa)
    END) AS thn_sakit,
    k.jenis_kasus,
    m.id AS id_ku60,
    n.id AS id_hkf,
    k.hot_case,
    (CASE
    WHEN k.hot_case = 1 THEN 'Hot Case'
    WHEN k.hot_case = 2 THEN 'Bukan'
    END) AS hot_case_txt
FROM
    trx_case a
    JOIN
    (SELECT
        b1.id AS i1,
        b1.name AS name_pasien,
        b1.umur_hari,
        b1.umur_bln,
        b1.umur_thn,
        b2.name AS nama_ortu,
        b3.code_kelurahan AS code_kelurahan_pasien,
        b3.code_kecamatan AS code_kecamatan_pasien,
        b3.code_kabupaten AS code_kabupaten_pasien,
        b3.code_provinsi AS code_provinsi_pasien,
        CONCAT_WS(',', b1.alamat, b3.full_address) AS full_address
    FROM
        ref_pasien b1
        JOIN ref_family b2 ON b1.id = b2.id_pasien
        LEFT JOIN view_area b3 ON b1.code_kelurahan = b3.code_kelurahan
    ) b ON a.id_pasien = b.i1
    JOIN trx_klinis k ON a.id = k.id_trx_case
    LEFT JOIN ref_klasifikasi_final d ON a.id_case = d.id_case AND a.klasifikasi_final = d.klasifikasi_final
    LEFT JOIN view_puskesmas i ON a.id_faskes = i.id AND a.id_role = 1
    LEFT JOIN view_rumahsakit j ON a.id_faskes = j.id AND a.id_role = 2
    LEFT JOIN trx_pe l ON a.id = l.id_trx_case AND l.deleted_at IS NULL
    LEFT JOIN trx_ku60 m ON a.id = m.id_trx_case
    LEFT JOIN trx_hkf n ON a.id = n.id_trx_case
WHERE
a.deleted_at IS NULL
ORDER BY a.id ASC
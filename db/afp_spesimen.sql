SELECT
    a.id AS id_trx_case,
    b.id_trx_pe,
    b.jenis_spesimen,
    (CASE
WHEN b.jenis_spesimen = 1 THEN 'Spesimen Tool I'
WHEN b.jenis_spesimen = 2 THEN 'Spesimen Tool II'
WHEN b.jenis_spesimen = 3 THEN 'Spesimen Tool III'
WHEN b.jenis_spesimen = 4 THEN 'Spesimen Tool IV'
WHEN b.jenis_spesimen = 5 THEN 'Spesimen Tool V'
END) AS jenis_spesimen_txt,
    DATE_FORMAT(b.tgl_ambil_spesimen,'%d-%m-%Y') AS tgl_ambil_spesimen,
    DATE_FORMAT(b.tgl_kirim_kab,'%d-%m-%Y') AS tgl_kirim_kab,
    DATE_FORMAT(b.tgl_kirim_prov,'%d-%m-%Y') AS tgl_kirim_prov,
    DATE_FORMAT(b.tgl_kirim_lab,'%d-%m-%Y') AS tgl_kirim_lab,
    b.jenis_pemeriksaan,
    (CASE
WHEN b.jenis_pemeriksaan = 1 THEN 'Isolasi Virus'
WHEN b.jenis_pemeriksaan = 2 THEN 'ITD'
WHEN b.jenis_pemeriksaan = 3 THEN 'Sequencing'
END) AS jenis_pemeriksaan_txt,
    b.hasil
FROM
    trx_case a
    JOIN trx_spesimen b ON a.id = b.id_trx_case
WHERE
a.id_case = 2 AND a.deleted_at IS NULL
ORDER BY b.id ASC
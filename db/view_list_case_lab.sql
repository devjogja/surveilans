
SELECT
    a.id AS id_trx_case,
    a.no_epid,
    a.no_epid_klb,
    (CASE
    WHEN a.id_case = 1 THEN YEAR(k.tgl_mulai_rash)
    WHEN a.id_case = 2 THEN YEAR(k.tgl_mulai_lumpuh)
    WHEN a.id_case = 3 THEN YEAR(k.tgl_mulai_demam)
    WHEN a.id_case = 4 THEN YEAR(k.tgl_mulai_sakit)
    WHEN a.id_case = 5 THEN YEAR(k.tgl_periksa)
    END) AS thn_sakit,
    b.id AS id_trx_lab,
    c.*,
    k.status_kasus,
    (CASE
    WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',i.name)
    WHEN a.id_role = 2 THEN j.name
    END) name_faskes,
    (CASE
    WHEN a.id_role = 1 THEN i.code_faskes
    WHEN a.id_role = 2 THEN j.code_faskes
    END) code_faskes
FROM
    trx_case a
    JOIN trx_lab b ON a.id = b.id_trx_case AND b.deleted_at IS NULL
    JOIN (
	SELECT
        c1.id AS i1,
        c1.name AS name_pasien,
        CONCAT(c1.alamat,' ',c2.full_address) AS full_address,
        c3.name AS nama_ortu
    FROM
        ref_pasien c1
        LEFT JOIN view_area c2 ON c1.code_kelurahan = c2.code_kelurahan
        JOIN ref_family c3 ON c1.id = c3.id_pasien
    ) c ON a.id_pasien = c.i1
    JOIN trx_klinis k ON a.id = k.id_trx_case
    LEFT JOIN view_puskesmas i ON a.id_faskes = i.id AND a.id_role = 1
    LEFT JOIN view_rumahsakit j ON a.id_faskes = j.id AND a.id_role = 2
WHERE
a.deleted_at IS NULL
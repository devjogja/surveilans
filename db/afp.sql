SELECT
a.id,
a.id AS id_trx_case,
a.id_role,
a.id_faskes,
a.id_faskes_old,
a.id_role_old,
a.id_pasien,
a.jenis_input,
(CASE
WHEN a.jenis_input = 1 THEN 'Web'
WHEN a.jenis_input = 2 THEN 'Android'
WHEN a.jenis_input = 3 THEN 'Import'
END) AS jenis_input_text,
(CASE
WHEN a.klasifikasi_final = 1 THEN 'Polio'
WHEN a.klasifikasi_final = 2 THEN 'Polio Kompatibel'
WHEN a.klasifikasi_final = 3 THEN 'Bukan Polio'
WHEN a.klasifikasi_final = 4 THEN 'VDVP'
ELSE NULL
END) AS klasifikasi_final_txt,
a.klasifikasi_final,
DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
DATE_FORMAT (a.created_at,'%d-%m-%Y %H:%i:%s') AS created_at,
DATE_FORMAT (a.updated_at,'%d-%m-%Y %H:%i:%s') AS updated_at,
DATE_FORMAT (a.deleted_at,'%d-%m-%Y %H:%i:%s') AS deleted_at,
a.id_case,
a.no_rm,
a.no_epid,
a._no_epid,
a.no_epid_lama,
(CASE
WHEN a.id_role = 1 THEN i.code_faskes
WHEN a.id_role = 2 THEN j.code_faskes
END) AS code_faskes,
(CASE
WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',i.name)
WHEN a.id_role = 2 THEN j.name
END) name_faskes,
(CASE
WHEN a.id_role = 1 THEN i.code_kelurahan
WHEN a.id_role = 2 THEN j.code_kelurahan
END) code_kelurahan_faskes,
(CASE
WHEN a.id_role = 1 THEN i.name_kelurahan
WHEN a.id_role = 2 THEN j.name_kelurahan
END) name_kelurahan_faskes,
(CASE
WHEN a.id_role = 1 THEN i.name_kecamatan
WHEN a.id_role = 2 THEN j.name_kecamatan
END) name_kecamatan_faskes,
(CASE
WHEN a.id_role = 1 THEN i.code_kecamatan
WHEN a.id_role = 2 THEN j.code_kecamatan
END) code_kecamatan_faskes,
(CASE
WHEN a.id_role = 1 THEN i.name_kabupaten
WHEN a.id_role = 2 THEN j.name_kabupaten
END) name_kabupaten_faskes,
(CASE
WHEN a.id_role = 1 THEN i.code_kabupaten
WHEN a.id_role = 2 THEN j.code_kabupaten
END) code_kabupaten_faskes,
(CASE
WHEN a.id_role = 1 THEN i.name_provinsi
WHEN a.id_role = 2 THEN j.name_provinsi
END) name_provinsi_faskes,
(CASE
WHEN a.id_role = 1 THEN i.code_provinsi
WHEN a.id_role = 2 THEN j.code_provinsi
END) code_provinsi_faskes,
a.pengambilan_spesimen,
(CASE
WHEN a.pengambilan_spesimen = 1 THEN 'Ya'
WHEN a.pengambilan_spesimen = 2 THEN 'Tidak'
ELSE '-'
END) AS pengambilan_spesimen_txt,
a.alasan_spesimen_tidak_diambil,
(CASE
WHEN e.id IS NULL THEN 'PE'
ELSE 'Sudah PE'
END) AS 'pe_text',
b.*,
c.*,
d.code AS code_roles,
e.id AS id_pe,
f.id AS id_ku60,
g.id AS id_hkf
FROM
trx_case a
JOIN (
SELECT
    b1.id AS i1,
    b1.name AS name_pasien,
    b1.nik,
    b1.jenis_kelamin,
    b1.tempat_lahir,
    CASE
WHEN b1.jenis_kelamin = 'L' THEN 'Laki-laki'
WHEN b1.jenis_kelamin = 'P' THEN 'Perempuan'
WHEN b1.jenis_kelamin = 'T' THEN 'Tidak Jelas'
ELSE 'Belum diisi' END AS jenis_kelamin_txt,
    DATE_FORMAT(b1.tgl_lahir, '%d-%m-%Y') AS tgl_lahir,
    b1.umur_thn,
    b1.umur_bln,
    b1.umur_hari,
    b1.alamat,
    b1.longitude,
    b1.latitude,
    b2.name AS agama,
    b2.name AS religion_text,
    b3.id AS id_family,
    b3.name AS nama_ortu,
    b4.code_kelurahan AS code_kelurahan_pasien,
    b4.name_kelurahan AS kelurahan_pasien,
    b4.name_kelurahan AS name_kelurahan_pasien,
    b4.code_kecamatan AS code_kecamatan_pasien,
    b4.name_kecamatan AS kecamatan_pasien,
    b4.name_kecamatan AS name_kecamatan_pasien,
    b4.code_kabupaten AS code_kabupaten_pasien,
    b4.name_kabupaten AS kabupaten_pasien,
    b4.name_kabupaten AS name_kabupaten_pasien,
    b4.code_provinsi AS code_provinsi_pasien,
    b4.name_provinsi AS provinsi_pasien,
    b4.name_provinsi AS name_provinsi_pasien,
    b4.full_address
FROM
    ref_pasien b1
    LEFT JOIN mst_religion b2 ON b1.agama = b2.id
    JOIN ref_family b3 ON b1.id = b3.id_pasien
    LEFT JOIN view_area b4 ON b1.code_kelurahan = b4.code_kelurahan
) AS b ON a.id_pasien = b.i1
JOIN (
    SELECT
c1.id_trx_case AS i2,
c1.id AS id_trx_klinis,
c1.berobat_ke_unit_pelayanan_lain AS berobat_ke_unit_pelayanan_lain,
(CASE
WHEN c1.berobat_ke_unit_pelayanan_lain = 1 THEN 'Ya'
WHEN c1.berobat_ke_unit_pelayanan_lain = 2 THEN 'Tidak'
ELSE '-'
END) AS berobat_ke_unit_pelayanan_lain_txt,
c1.nama_unit_pelayanan,
DATE_FORMAT (c1.tgl_berobat,'%d-%m-%Y') AS tgl_berobat,
c1.diagnosis,
c1.no_rm_lama,
c1.kelumpuhan_sifat_akut,
(CASE
WHEN c1.kelumpuhan_sifat_akut = 1 THEN 'Ya'
WHEN c1.kelumpuhan_sifat_akut = 2 THEN 'Tidak'
WHEN c1.kelumpuhan_sifat_akut = 3 THEN 'Tidak Jelas'
ELSE '-'
END) AS kelumpuhan_sifat_akut_txt,
c1.kelumpuhan_sifat_layuh,
(CASE
WHEN c1.kelumpuhan_sifat_layuh = 1 THEN 'Ya'
WHEN c1.kelumpuhan_sifat_layuh = 2 THEN 'Tidak'
WHEN c1.kelumpuhan_sifat_layuh = 3 THEN 'Tidak Jelas'
ELSE '-'
END) AS kelumpuhan_sifat_layuh_txt,
c1.kelumpuhan_disebabkan_ruda,
(CASE
WHEN c1.kelumpuhan_disebabkan_ruda = 1 THEN 'Ya'
WHEN c1.kelumpuhan_disebabkan_ruda = 2 THEN 'Tidak'
WHEN c1.kelumpuhan_disebabkan_ruda = 3 THEN 'Tidak Jelas'
ELSE '-'
END) AS kelumpuhan_disebabkan_ruda_txt,
c1.diagnosa_dr_rs,
(CASE
WHEN c1.diagnosa_dr_rs = 1 THEN 'Ya'
WHEN c1.diagnosa_dr_rs = 2 THEN 'Tidak'
ELSE '-'
END) AS diagnosa_dr_rs_txt,
c1.paralisis_residual AS paralisis_residual,
(CASE
WHEN c1.paralisis_residual = 1 THEN 'Ya'
WHEN c1.paralisis_residual = 2 THEN 'Tidak'
ELSE '-'
END) AS paralisis_residual_txt,
c1.sifat_layuh AS sifat_layuh,
(CASE
WHEN c1.sifat_layuh = 1 THEN 'Ya'
WHEN c1.sifat_layuh = 2 THEN 'Tidak'
ELSE '-'
END) AS sifat_layuh_txt,
DATE_FORMAT (c1.tgl_sakit,'%d-%m-%Y') AS tgl_sakit_pe,
c1.imunisasi_rutin_polio_sebelum_sakit AS jml_imunisasi,
c1.pin_mopup_ori_biaspolio AS pin_mopup_ori_biaspolio,
c1.tgl_mulai_lumpuh AS tgl_sakit_date,
DATE_FORMAT(c1.tgl_mulai_lumpuh, '%Y') AS thn_sakit,
DATE_FORMAT(c1.tgl_mulai_lumpuh,'%d-%m-%Y') AS tgl_mulai_lumpuh,
c1.demam_sebelum_lumpuh,
(CASE
WHEN c1.demam_sebelum_lumpuh = 1 THEN 'Ya'
WHEN c1.demam_sebelum_lumpuh = 2 THEN 'Tidak'
END) AS demam_sebelum_lumpuh_txt,
c1.imunisasi_rutin_polio_sebelum_sakit,
(CASE
WHEN c1.imunisasi_rutin_polio_sebelum_sakit = 7 THEN 'Tidak'
WHEN c1.imunisasi_rutin_polio_sebelum_sakit = 8 THEN 'Tidak tahu'
WHEN c1.imunisasi_rutin_polio_sebelum_sakit = 9 THEN 'Belum pernah'
WHEN c1.imunisasi_rutin_polio_sebelum_sakit IN ('1','2','3','4','5','6') THEN CONCAT(c1.imunisasi_rutin_polio_sebelum_sakit, 'x')
ELSE NULL
END) AS imunisasi_rutin_polio_sebelum_sakit_txt,
c1.informasi_imunisasi_rutin_polio_sebelum_sakit,
(CASE
WHEN c1.informasi_imunisasi_rutin_polio_sebelum_sakit = 1 THEN 'KMS/Catatan Jurim'
WHEN c1.informasi_imunisasi_rutin_polio_sebelum_sakit = 2 THEN 'Ingatan responden'
END) AS informasi_imunisasi_rutin_polio_sebelum_sakit_txt,
(CASE
WHEN c1.pin_mopup_ori_biaspolio = 7 THEN 'Tidak'
WHEN c1.pin_mopup_ori_biaspolio = 8 THEN 'Tidak tahu'
WHEN c1.pin_mopup_ori_biaspolio = 9 THEN 'Belum pernah'
WHEN c1.pin_mopup_ori_biaspolio IN ('1','2','3','4','5','6') THEN CONCAT(c1.pin_mopup_ori_biaspolio, 'x')
ELSE NULL
END) AS pin_mopup_ori_biaspolio_txt,
c1.informasi_pin_mopup_ori_biaspolio,
(CASE
WHEN c1.informasi_pin_mopup_ori_biaspolio = 1 THEN 'KMS/Catatan Jurim'
WHEN c1.informasi_pin_mopup_ori_biaspolio = 2 THEN 'Ingatan responden'
END) AS informasi_pin_mopup_ori_biaspolio_txt,
DATE_FORMAT(c1.tgl_imunisasi_polio_terakhir,'%d-%m-%Y') AS tgl_imunisasi_polio_terakhir,
DATE_FORMAT(c1.tgl_laporan_diterima,'%d-%m-%Y') AS tgl_laporan_diterima,
DATE_FORMAT(c1.tgl_pelacakan,'%d-%m-%Y') AS tgl_pelacakan,
c1.kontak,
(CASE
WHEN c1.kontak = 1 THEN 'Ya'
WHEN c1.kontak = 2 THEN 'Tidak'
END) AS kontak_txt,
c1.status_kasus,
c1.keadaan_akhir,
DATE_FORMAT(c1.tgl_meninggal,'%d-%m-%Y') AS tgl_meninggal,
(CASE
WHEN c1.keadaan_akhir = 1 THEN 'Hidup'
WHEN c1.keadaan_akhir = 2 THEN 'Meninggal'
END) AS keadaan_akhir_txt,
c1.hot_case,
(CASE
WHEN c1.hot_case = 1 THEN 'Hot Case'
WHEN c1.hot_case = 2 THEN 'Bukan'
END) AS hot_case_txt,
GROUP_CONCAT(DISTINCT CONCAT(IFNULL(c2.id_gejala,''),'_',IFNULL(c21.name,''),'_',IFNULL(c2.val_kelumpuhan,'')) SEPARATOR '|') AS gejala_kelumpuhan,
GROUP_CONCAT(DISTINCT CONCAT(IFNULL(c2.id_gejala,''),'_',IFNULL(c21.name,''),'_',IFNULL(c2.val_gangguan_raba,'')) SEPARATOR '|') AS gejala_gangguan_raba,
GROUP_CONCAT(DISTINCT CONCAT(c2.other_gejala,'_',c2.val_kelumpuhan,'_',c2.val_gangguan_raba) SEPARATOR '|') AS other_gejala
FROM
trx_klinis c1
LEFT JOIN trx_gejala c2 ON c1.id = c2.id_trx_klinis
LEFT JOIN mst_gejala c21 ON c2.id_gejala = c21.id
GROUP BY c1.id_trx_case
) c ON a.id = c.i2
JOIN roles d ON a.id_role = d.id
LEFT JOIN trx_pe e ON a.id = e.id_trx_case AND e.deleted_at IS NULL
LEFT JOIN trx_ku60 f ON a.id = f.id_trx_case AND f.deleted_at IS NULL
LEFT JOIN trx_hkf g ON a.id = g.id_trx_case AND g.deleted_at IS NULL
LEFT JOIN view_puskesmas i ON a.id_faskes = i.id AND a.id_role = 1
LEFT JOIN view_rumahsakit j ON a.id_faskes = j.id AND a.id_role = 2
WHERE
a.id_case = '2' AND a.deleted_at IS NULL
GROUP BY a.id
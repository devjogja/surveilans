SELECT
    a.id AS id_hkf,
    b.id_faskes,
    b.id AS id_trx_case,
    b.code_kelurahan_faskes,
    b.code_kabupaten_faskes,
    b.code_kecamatan_faskes,
    b.code_provinsi_faskes,
    b.name_kelurahan_faskes,
    b.name_kecamatan_faskes,
    b.name_kabupaten_faskes,
    b.name_provinsi_faskes,
    a.hasil_klasifikasi,
    CASE
WHEN a.hasil_klasifikasi=1 THEN 'Bukan kasus polio'
WHEN a.hasil_klasifikasi=2 THEN 'Polio Kompatible'
WHEN a.hasil_klasifikasi=3 THEN 'Vaccine Associated Polio Paralytic (VAPP)'
ELSE NULL
END AS hasil_klasifikasi_txt,
    a.diagnosis,
    a.isolasi_cirus_polio_vaksin,
    CASE
WHEN a.isolasi_cirus_polio_vaksin=1 THEN 'Ya'
ELSE '-'
END AS isolasi_cirus_polio_vaksin_txt,
    a.demam,
    CASE
WHEN a.demam=1 THEN 'Ya'
ELSE '-'
END AS demam_txt,
    a.sifat_kelumpuhan_simetris,
    CASE
WHEN a.sifat_kelumpuhan_simetris=1 THEN 'Ya'
ELSE '-'
END AS sifat_kelumpuhan_simetris_txt,
    a.gangguan_rasa_raba,
    CASE
WHEN a.gangguan_rasa_raba=1 THEN 'Ya'
ELSE '-'
END AS gangguan_rasa_raba_txt,
    a.paralisis_residual,
    CASE
WHEN a.paralisis_residual=1 THEN 'Ya'
ELSE '-'
END AS paralisis_residual_txt,
    a.meninggal,
    CASE
WHEN a.meninggal=1 THEN 'Ya'
ELSE '-'
END AS meninggal_txt,
    a.imunisasi_polio,
    CASE
WHEN a.imunisasi_polio=1 THEN 'Ya'
ELSE '-'
END AS imunisasi_polio_txt,
    a.imunisasi_polio_terakhir,
    CASE
WHEN a.imunisasi_polio_terakhir=1 THEN 'Ya'
ELSE '-'
END AS imunisasi_polio_terakhir_txt,
    a.dosis_imunisasi,
    a.follow_up,
    CASE
WHEN a.follow_up=1 THEN 'Ya'
ELSE '-'
END AS follow_up_txt,
    a.daerah_klb_polio,
    CASE
WHEN a.daerah_klb_polio=1 THEN 'Ya'
ELSE '-'
END AS daerah_klb_polio_txt,
    a.hub_epidemologi_kasus_polio_daerah_klb,
    CASE
WHEN a.hub_epidemologi_kasus_polio_daerah_klb=1 THEN 'Ya'
ELSE '-'
END AS hub_epidemologi_kasus_polio_daerah_klb_txt,
    a.clustering_kasus_afp,
    CASE
WHEN a.clustering_kasus_afp=1 THEN 'Ya'
ELSE '-'
END AS clustering_kasus_afp_txt,
    a.lain_lain,
    CASE
WHEN a.lain_lain=1 THEN 'Ya'
ELSE '-'
END AS lain_lain_txt,
    a.lain_lain_val,
    DATE_FORMAT(a.created_at,'%d-%m-%Y %H:%i:%s') AS created_at,
    DATE_FORMAT(a.updated_at,'%d-%m-%Y %H:%i:%s') AS updated_at,
    DATE_FORMAT(a.deleted_at,'%d-%m-%Y %H:%i:%s') AS deleted_at
FROM
    trx_hkf AS a
    JOIN afp AS b ON a.id_trx_case=b.id_trx_case
WHERE a.deleted_at IS NULL
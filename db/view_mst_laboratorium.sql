SELECT
    a.id,
    a.code,
    a.name,
    a.alamat,
    a.telepon,
    a.fax,
    a.email,
    a.konfirm_code,
    b.code_kelurahan,
    b.name_kelurahan,
    b.code_kecamatan,
    b.name_kecamatan,
    b.code_kabupaten,
    b.name_kabupaten,
    b.code_provinsi,
    b.name_provinsi,
    b.full_address,
    CONCAT(a.alamat,',',b.full_address) AS full_alamat
FROM
    mst_laboratorium AS a
    LEFT JOIN view_area AS b ON a.code_kelurahan=b.code_kelurahan
WHERE a.deleted_at IS NULL
SELECT
    a.id,
    a.name,
    a.code_faskes,
    a.alamat,
    a.location,
    a.longitude,
    a.latitude,
    a.konfirm_code,
    NULL AS code_kelurahan,
    NULL AS name_kelurahan,
    b.code AS code_kecamatan,
    b.name AS name_kecamatan,
    c.code AS code_kabupaten,
    c.name AS name_kabupaten,
    d.code AS code_provinsi,
    d.name AS name_provinsi,
    CONCAT_WS(', ',b.name,c.name,d.name) AS full_address
FROM
    mst_puskesmas a
    JOIN mst_kecamatan b ON a.code_kecamatan = b.code
    JOIN mst_kabupaten c ON b.code_kabupaten = c.code
    JOIN mst_provinsi d ON c.code_provinsi = d.code
WHERE a.deleted_at IS NULL;

SELECT
    a.role_id AS role_id,
    a.id AS id_role_user,
    b.id AS id_user,
    CONCAT(b.first_name,' ',b.last_name) AS full_name,
    c.jabatan AS jabatan,
    c.no_telp AS no_telp,
    b.email AS email,
    (CASE
    WHEN (a.role_id = '1') THEN CONCAT('PUSKESMAS ',d.name)
    WHEN (a.role_id = '2') THEN e.name
    WHEN (a.role_id = '3') THEN 'PUSAT'
    WHEN (a.role_id = '4') THEN h.name
    WHEN (a.role_id = '5') THEN CONCAT('PROVINSI ',g.name)
    WHEN (a.role_id = '6') THEN CONCAT('KABUPATEN ',f.name)
    END) AS faskes_name,
    (CASE
    WHEN (a.role_id = '1') THEN d.name_kabupaten
    WHEN (a.role_id = '2') THEN e.name_kabupaten
    ELSE NULL END) AS name_kabupaten,
    (CASE
    WHEN (a.role_id = '1') THEN d.name_provinsi
    WHEN (a.role_id = '2') THEN e.name_provinsi
    WHEN (a.role_id = '6') THEN f.name_provinsi
    ELSE NULL END) AS name_provinsi
FROM
    role_users a
    JOIN users b ON a.user_id = b.id
    JOIN users_meta c ON b.id = c.id_user
    LEFT JOIN view_puskesmas d ON a.faskes_id = d.id
    LEFT JOIN view_rumahsakit e ON a.faskes_id = e.id
    LEFT JOIN (SELECT
        f1.name AS name_provinsi,
        f2.code,
        f2.name
    FROM
        mst_provinsi f1
        JOIN mst_kabupaten f2 ON f1.code = f2.code_provinsi
    ) f ON a.faskes_id = f.code
    LEFT JOIN mst_provinsi g ON a.faskes_id = g.code
    LEFT JOIN mst_laboratorium h ON a.faskes_id = h.code
WHERE
 a.deleted_at IS NULL AND b.deleted_at IS NULL
ORDER BY b.first_name
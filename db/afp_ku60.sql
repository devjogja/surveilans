SELECT
    b.id AS id_trx_case,
    b.name_pasien AS name_pasien,
    b.id_faskes AS id_faskes,
    b.nik AS nik,
    b.no_rm AS no_rm,
    b.agama AS agama,
    b.religion_text AS religion_text,
    b.nama_ortu AS nama_ortu,
    b.jenis_kelamin AS jenis_kelamin,
    b.jenis_kelamin_txt AS jenis_kelamin_txt,
    b.tgl_lahir AS tgl_lahir,
    b.umur_thn AS umur_thn,
    b.umur_bln AS umur_bln,
    b.umur_hari AS umur_hari,
    b.code_kelurahan_faskes AS code_kelurahan_faskes,
    b.code_kabupaten_faskes AS code_kabupaten_faskes,
    b.code_kecamatan_faskes AS code_kecamatan_faskes,
    b.code_provinsi_faskes AS code_provinsi_faskes,
    b.name_kelurahan_faskes AS name_kelurahan_faskes,
    b.name_kecamatan_faskes AS name_kecamatan_faskes,
    b.name_kabupaten_faskes AS name_kabupaten_faskes,
    b.name_provinsi_faskes AS name_provinsi_faskes,
    b.code_kelurahan_pasien AS code_kelurahan_pasien,
    b.code_kabupaten_pasien AS code_kabupaten_pasien,
    b.code_kecamatan_pasien AS code_kecamatan_pasien,
    b.code_provinsi_pasien AS code_provinsi_pasien,
    b.name_kelurahan_pasien AS name_kelurahan_pasien,
    b.name_kecamatan_pasien AS name_kecamatan_pasien,
    b.name_kabupaten_pasien AS name_kabupaten_pasien,
    b.name_provinsi_pasien AS name_provinsi_pasien,
    b.full_address AS full_address,
    b.alamat AS alamat,
    (CASE
    WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',i.name)
    WHEN a.id_role = 2 THEN j.name
    WHEN a.id_role = 6 THEN CONCAT('KABUPATEN ',k.name)
    ELSE NULL
    END) AS name_faskes,
    b.no_epid AS no_epid,
    b.no_epid_lama AS no_epid_lama,
    b.diagnosa_dr_rs AS diagnosa_dr_rs,
    b.diagnosa_dr_rs_txt AS diagnosa_dr_rs_txt,
    b.diagnosis AS diagnosis,
    b.paralisis_residual AS paralisis_residual,
    b.sifat_layuh AS sifat_layuh,
    b.sifat_layuh_txt AS sifat_layuh_txt,
    a.id AS id,
    a.id AS id_ku60,
    DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
    DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
    DATE_FORMAT(a.created_at, '%d-%m-%Y %H:%i:%s') AS created_at,
    DATE_FORMAT(a.updated_at, '%d-%m-%Y %H:%i:%s') AS updated_at,
    DATE_FORMAT(a.deleted_at, '%d-%m-%Y %H:%i:%s') AS deleted_at,
    c.code_kabupaten_kunjungan_ulang AS code_kabupaten_kunjungan_ulang,
    c.code_provinsi_kunjungan_ulang AS code_provinsi_kunjungan_ulang,
    DATE_FORMAT(c.tgl_kunjungan_ulang_seharusnya, '%d-%m-%Y') AS tgl_kunjungan_ulang_seharusnya,
    c.kunjungan AS kunjungan,
    (CASE
    WHEN c.kunjungan = 1 THEN 'Ya'
    WHEN c.kunjungan = 2 THEN 'Tidak'
    ELSE NULL
    END) AS kunjungan_txt,
    DATE_FORMAT(c.tgl_kunjungan_ulang, '%d-%m-%Y') AS tgl_kunjungan_ulang,
    c.tdk_kunjungan AS tdk_kunjungan,
    (CASE
    WHEN c.tdk_kunjungan = 1 THEN 'Meninggal'
    WHEN c.tdk_kunjungan = 2 THEN 'Pindah Alamat, Alamat tidak jelas'
    WHEN c.tdk_kunjungan = 3 THEN 'Lain-lain'
    ELSE NULL
    END) AS tdk_kunjungan_txt,
    DATE_FORMAT(c.tgl_meninggal, '%d-%m-%Y') AS tgl_meninggal,
    c.alasan_lain_tdk_kunjungan AS alasan_lain_tdk_kunjungan,
    d.name_kabupaten AS name_kabupaten_kunjungan_ulang,
    d.name_provinsi AS name_provinsi_kunjungan_ulang,
    e.diagnosa_akhir AS diagnosa_akhir,
    e.nama_pemeriksa AS nama_pemeriksa,
    e.no_telp AS no_telp
FROM
    trx_ku60 a
    JOIN afp b ON a.id_trx_case = b.id_trx_case
    LEFT JOIN trx_informasi_kunjungan_ulang c ON a.id = c.id_trx_ku60
    LEFT JOIN trx_pe_hasil_pemeriksaan e ON a.id = e.id_trx_ku60
    LEFT JOIN view_area d ON c.code_kabupaten_kunjungan_ulang = d.code_kabupaten
    LEFT JOIN mst_puskesmas i ON a.id_faskes = i.id AND a.id_role='1'
    LEFT JOIN mst_rumahsakit j ON a.id_faskes = j.id AND a.id_role='2'
    LEFT JOIN mst_kabupaten k ON a.id_faskes = k.code AND a.id_role='6'
WHERE a.deleted_at IS NULL
GROUP BY a.id_trx_case
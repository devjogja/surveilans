SELECT
a.id AS id_trx_case,
a.id_faskes,
(CASE
WHEN a.id_role = 1 THEN i.code_kelurahan
WHEN a.id_role = 2 THEN j.code_kelurahan
END) code_kelurahan_faskes,
(CASE
WHEN a.id_role = 1 THEN i.code_kecamatan
WHEN a.id_role = 2 THEN j.code_kecamatan
END) code_kecamatan_faskes,
(CASE
WHEN a.id_role = 1 THEN i.code_kabupaten
WHEN a.id_role = 2 THEN j.code_kabupaten
END) code_kabupaten_faskes,
(CASE
WHEN a.id_role = 1 THEN i.code_provinsi
WHEN a.id_role = 2 THEN j.code_provinsi
END) code_provinsi_faskes,
a.no_epid AS no_epid,
a.no_epid_klb AS no_epid_klb,
b.id AS id_trx_pe,
b.longitude,
b.latitude,
b.jenis_input,
b.total_kasus_tambahan,
DATE_FORMAT(b.tgl_penyelidikan, '%d-%m-%Y') AS tgl_penyelidikan_pe,
b.id AS id,
b.id AS id_pe,
DATE_FORMAT(b.created_at, '%d-%m-%Y %H:%i:%s') AS created_at,
DATE_FORMAT(b.updated_at, '%d-%m-%Y %H:%i:%s') AS updated_at,
DATE_FORMAT(b.deleted_at, '%d-%m-%Y %H:%i:%s') AS deleted_at,
c.*,
d.status_kasus,
DATE_FORMAT(e.tgl_pengobatan_pertama_kali, '%d-%m-%Y') AS tgl_pengobatan_pertama_kali,
e.tempat_pengobatan_pertama_kali,
e.obat_yg_diberikan,
f.penyakit_sama_dirumah,
(CASE
WHEN f.penyakit_sama_dirumah = 1 THEN 'Ya'
WHEN f.penyakit_sama_dirumah = 2 THEN 'Tidak'
END) AS penyakit_sama_dirumah_txt,
DATE_FORMAT(f.tgl_penyakit_sama_dirumah, '%d-%m-%Y') AS tgl_penyakit_sama_dirumah,
f.penyakit_sama_disekolah,
(CASE
WHEN f.penyakit_sama_disekolah = 1 THEN 'Ya'
WHEN f.penyakit_sama_disekolah = 2 THEN 'Tidak'
END) AS penyakit_sama_disekolah_txt,
DATE_FORMAT(f.tgl_penyakit_sama_disekolah, '%d-%m-%Y') AS tgl_penyakit_sama_disekolah,
f.keadaan_kurang_gizi,
(CASE
WHEN f.keadaan_kurang_gizi = 1 THEN 'Ya'
WHEN f.keadaan_kurang_gizi = 2 THEN 'Tidak'
END) AS keadaan_kurang_gizi_txt,
GROUP_CONCAT(DISTINCT CONCAT_WS('_',IFNULL(g.lokasi,''), IFNULL(g.keterangan,''), IFNULL(DATE_FORMAT(g.tgl_pe, '%d-%m-%Y'),''), IFNULL(g.jml_kasus,'')) SEPARATOR '|') AS jml_kasus_tambahan,
GROUP_CONCAT(DISTINCT h.nama_pelaksana SEPARATOR ',') nama_pelaksana
FROM
trx_case a
JOIN trx_pe b ON a.id = b.id_trx_case AND b.deleted_at IS NULL
JOIN (
SELECT
c1.id AS i1,
DATE_FORMAT(c1.tgl_lahir, '%d-%m-%Y') AS tgl_lahir,
c1.umur_thn,
c1.umur_bln,
c1.umur_hari,
c1.name AS name_pasien,
c3.full_address,
c1.alamat,
(CASE
WHEN c1.jenis_kelamin = 'L' THEN 'Laki-laki'
WHEN c1.jenis_kelamin = 'P' THEN 'Perempuan'
WHEN c1.jenis_kelamin = 'T' THEN 'Tidak Jelas'
ELSE 'Belum diisi'
END) AS jenis_kelamin_txt,
c2.name AS nama_ortu
FROM
ref_pasien c1
JOIN ref_family c2 ON c1.id = c2.id_pasien
LEFT JOIN view_area c3 ON c1.code_kelurahan = c3.code_kelurahan
) c ON a.id_pasien = c.i1
JOIN trx_klinis d ON a.id = d.id_trx_case
JOIN trx_pe_riwayat_pengobatan e ON b.id = e.id_trx_pe
JOIN trx_pe_riwayat_kontak f ON b.id = f.id_trx_pe
LEFT JOIN trx_pe_kasus_tambahan g ON b.id = g.id_trx_pe
LEFT JOIN trx_pe_pelaksana h ON b.id = h.id_trx_pe
LEFT JOIN view_puskesmas i ON a.id_faskes = i.id AND a.id_role = 1
LEFT JOIN view_rumahsakit j ON a.id_faskes = j.id AND a.id_role = 2
WHERE
a.id_case = 1 AND ISNULL(a.deleted_at)
GROUP BY b.id
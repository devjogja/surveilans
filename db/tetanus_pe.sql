SELECT
a.id AS id_tx_case,
b.id AS id_txr_pe,
b.longitude,
b.latitude,
CASE
WHEN c.sumber_laporan = 1 THEN 'Rumah Sakit'
WHEN c.sumber_laporan = 2 THEN 'Puskesmas'
WHEN c.sumber_laporan = 3 THEN 'Praktek Swasta'
WHEN c.sumber_laporan = 4 THEN 'Masyarakat'
WHEN c.sumber_laporan = 5 THEN 'Lain-lain'
END AS sumber_laporan_txt,
c.sumber_laporan_lain,
DATE_FORMAT(c.tgl_laporan, '%d-%m-%Y') AS tgl_laporan,
DATE_FORMAT(c.tgl_investigasi, '%d-%m-%Y') AS tgl_investigasi,
d.anak_keberapa,
e.nama_ayah,
e.umur_th_ayah,
e.umur_bln_ayah,
e.umur_hari_ayah,
e.umur_th_ibu,
e.umur_bln_ibu,
e.umur_hari_ibu,
e1.name AS pend_terakhir_ayah_txt,
e2.name AS pekerjaan_ayah_txt,
e.nama_ibu,
e3.name AS pend_terakhir_ibu_txt,
e4.name AS pekerjaan_ibu_txt,
f.nama_yg_diwawancarai,
f.hub_dgn_keluarga_bayi,
f.bayi_lahir_hidup,
DATE_FORMAT(f.tgl_lahir_bayi, '%d-%m-%Y') AS tgl_lahir_bayi,
f.bayi_meninggal,
DATE_FORMAT(f.tgl_meninggal, '%d-%m-%Y') AS tgl_meninggal,
f.umur_meninggal,
f.lahir_bayi_menangis,
f.tanda_kehidupan_lain,
f.bayi_menetek_menghisap_susu_botol_dgn_baik,
f.3_hari_tiba_tiba_mulut_bayi_mencucu_tidak_menetek,
DATE_FORMAT(f.tgl_tidak_mau_menetek, '%d-%m-%Y') AS tgl_tidak_mau_menetek,
f.bayi_mudah_kejang_jika_dsentuh,
DATE_FORMAT(f.tgl_mulai_kejang, '%d-%m-%Y') AS tgl_mulai_kejang,
f.bayi_dirawat,
f.fasilitas_kesehatan,
DATE_FORMAT(f.tgl_dirawat, '%d-%m-%Y') AS tgl_dirawat,
CASE
WHEN f.keadaan_bayi = 1 THEN 'Sembuh'
WHEN f.keadaan_bayi = 2 THEN 'Belum Sembuh'
WHEN f.keadaan_bayi = 3 THEN 'Meninggal'
END AS keadaan_bayi_txt,
g.gpa,
g.periksa_dokter,
g.jml_periksa_dokter,
g.periksa_bidan,
g.jml_periksa_bidan,
g.periksa_dukun,
g.jml_periksa_dukun,
g.tidak_anc,
g.tidak_jelas,
GROUP_CONCAT(DISTINCT CONCAT_WS('_',h.nama, h.profesi, h.alamat, h.frekuensi) SEPARATOR '|') petugas_pemeriksa,
g.imunisasi_tt_ibu_waktu_hamil,
CASE
WHEN g.sumber_informasi_imunisasi_tt = 1 THEN 'Ingatan'
WHEN g.sumber_informasi_imunisasi_tt = 1 THEN 'Buku Catatan'
END AS sumber_informasi_imunisasi_tt_txt,
g.imunisasi_ibu_tt_pertama,
g.umur_kehamilan_ibu_tt_pertama,
DATE_FORMAT(g.tgl_imunisasi_ibu_tt_pertama, '%d-%m-%Y') AS tgl_imunisasi_ibu_tt_pertama,
g.imunisasi_ibu_tt_kedua,
g.umur_kehamilan_ibu_tt_kedua,
DATE_FORMAT(g.tgl_imunisasi_ibu_tt_kedua, '%d-%m-%Y') AS tgl_imunisasi_ibu_tt_kedua,
g.imunisasi_tt_kehamilan_sebelumnya,
g.suntikan_tt_pertama_kehamilan_sebelumnya,
g.suntikan_tt_kedua_kehamilan_sebelumnya,
g.suntikan_tt_calon_pengantin,
g.suntikan_tt_pertama_calon_pengantin,
g.suntikan_tt_kedua_calon_pengantin,
CASE
WHEN g.status_tt_ibu_saat_kehamilan = 1 THEN 'TT1'
WHEN g.status_tt_ibu_saat_kehamilan = 2 THEN 'TT2'
WHEN g.status_tt_ibu_saat_kehamilan = 3 THEN 'TT3'
WHEN g.status_tt_ibu_saat_kehamilan = 4 THEN 'TT4'
WHEN g.status_tt_ibu_saat_kehamilan = 5 THEN 'TT5'
END AS status_tt_ibu_saat_kehamilan_txt,
GROUP_CONCAT(DISTINCT CONCAT_WS('_',i.nama, i.profesi, i.alamat, i.tempat_persalinan) SEPARATOR '|') penolong_persalinan,
CASE
WHEN j.obat_yg_dibubuhkan_setelah_tali_pusat_dipotong = 1 THEN 'Alkohol/Iod'
WHEN j.obat_yg_dibubuhkan_setelah_tali_pusat_dipotong = 2 THEN 'Ramuan Tradisional'
WHEN j.obat_yg_dibubuhkan_setelah_tali_pusat_dipotong = 3 THEN 'Kasa Kering'
WHEN j.obat_yg_dibubuhkan_setelah_tali_pusat_dipotong = 4 THEN 'Lainnya'
END AS obat_yg_dibubuhkan_setelah_tali_pusat_dipotong_txt,
j.sebutkan_obat_bubuhan,
CASE
WHEN j.petugas_perawat_tali_pusat = 1 THEN 'Tenaga Kesehatan'
WHEN j.petugas_perawat_tali_pusat = 2 THEN 'Bukan Tenaga Kesehatan'
END AS petugas_perawat_tali_pusat_txt,
j.obat_saat_merawat_tali_pusat,
CASE
WHEN j.kesimpulan_diagnosis = 1 THEN 'Konfirm TN'
WHEN j.kesimpulan_diagnosis = 2 THEN 'Tersangka TN'
WHEN j.kesimpulan_diagnosis = 3 THEN 'Bukan TN'
END AS kesimpulan_diagnosis_txt,
k.cakupan_imunisasi_tt1,
k.cakupan_imunisasi_tt2,
k.cakupan_imunisasi_tt3,
k.cakupan_imunisasi_tt4,
k.cakupan_imunisasi_tt5,
k.cakupan_persalinan_tenaga_kesehatan,
k.cakupan_kunjungan_neonatus1,
k.cakupan_kunjungan_neonatus2,
GROUP_CONCAT(DISTINCT CONCAT_WS('_',l.nama, l.jabatan) SEPARATOR '|') pelacak
FROM
trx_case a
JOIN trx_pe b ON a.id = b.id_trx_case
JOIN trx_pelapor c ON b.id = c.id_trx_pe
JOIN ref_pasien d ON a.id_pasien = d.id
JOIN ref_family e ON d.id = e.id_pasien
LEFT JOIN mst_education e1 ON e.pend_terakhir_ayah = e1.id
LEFT JOIN mst_occupation e2 ON e.pekerjaan_ayah = e2.id
LEFT JOIN mst_education e3 ON e.pend_terakhir_ibu = e3.id
LEFT JOIN mst_occupation e4 ON e.pekerjaan_ibu = e4.id
JOIN trx_pe_riwayat_kesakitan f ON b.id = f.id_trx_pe
JOIN trx_pe_riwayat_kehamilan g ON b.id = g.id_trx_pe
LEFT JOIN trx_pe_pemeriksa_kehamilan h ON b.id = h.id_trx_pe
LEFT JOIN trx_pe_penolong_persalinan i ON b.id = i.id_trx_pe
JOIN trx_pe_riwayat_persalinan j ON b.id = j.id_trx_pe
JOIN trx_pe_data_cakupan k ON b.id = k.id_trx_pe
LEFT JOIN trx_pe_pelacak l ON b.id = l.id_trx_pe
WHERE
a.id_case = 4 AND a.deleted_at IS NULL AND b.deleted_at IS NULL
GROUP BY a.id
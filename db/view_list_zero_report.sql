SELECT
    a.trx_zero_report_id,
    DATE_FORMAT(a.trx_zero_report_start, '%d-%m-%Y') AS trx_zero_report_start,
    DATE_FORMAT(a.trx_zero_report_end, '%d-%m-%Y') AS trx_zero_report_end,
    a.id_faskes,
    a.id_role,
    (CASE
    WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ', b.name)
    WHEN a.id_role = 2 THEN c.name
    END) AS name_faskes,
    (CASE
    WHEN a.id_role = 1 THEN b.code_kecamatan
    WHEN a.id_role = 2 THEN c.code_kecamatan
    END) AS code_kecamatan_faskes,
    (CASE
    WHEN a.id_role = 1 THEN b.code_kabupaten
    WHEN a.id_role = 2 THEN c.code_kabupaten
    END) AS code_kabupaten_faskes,
    (CASE
    WHEN a.id_role = 1 THEN b.code_provinsi
    WHEN a.id_role = 2 THEN c.code_provinsi
    END) AS code_provinsi_faskes
FROM
    trx_zero_report a
    LEFT JOIN mst_puskesmas b ON a.id_faskes = b.id AND a.id_role = 1
    LEFT JOIN mst_rumahsakit c ON a.id_faskes = c.id AND a.id_role = 2
WHERE
    a.deleted_at IS NULL
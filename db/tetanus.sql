SELECT
    a.id AS id_trx_case,
    a.id_faskes,
    a.no_epid,
    b.name AS name_pasien,
    b.umur_hari,
    b.umur_bln,
    b.umur_thn,
    b.nik,
    b.alamat,
    e.code AS code_kelurahan_pasien,
    f.code AS code_kecamatan_pasien,
    g.code AS code_kabupaten_pasien,
    h.code AS code_provinsi_pasien,
    k.tgl_mulai_sakit AS tgl_sakit_date,
    DATE_FORMAT(k.tgl_mulai_sakit, '%Y') AS thn_sakit,
    CONCAT_WS(', ',b.alamat,e.name,f.name,g.name,h.name) AS full_address,
    CASE
WHEN k.keadaan_akhir = 1 THEN 'Hidup'
WHEN k.keadaan_akhir = 2 THEN 'Meninggal'
END AS keadaan_akhir_txt,
    a.klasifikasi_final,
    CASE
WHEN (a.klasifikasi_final = 1) THEN 'Konfirm TN'
WHEN (a.klasifikasi_final = 2) THEN 'Tersangka TN'
WHEN (a.klasifikasi_final = 3) THEN 'Bukan TN'
END AS klasifikasi_final_txt,
    a.id_role,
    CASE
WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',i.name)
WHEN a.id_role = 2 THEN j.name
END name_faskes,
    CASE
WHEN a.id_role = 1 THEN i.code_faskes
WHEN a.id_role = 2 THEN j.code_faskes
END code_faskes,
    CASE
WHEN a.id_role = 1 THEN i.code_kecamatan
WHEN a.id_role = 2 THEN j.code_kecamatan
END code_kecamatan_faskes,
    CASE
WHEN a.id_role = 1 THEN i.code_kabupaten
WHEN a.id_role = 2 THEN j.code_kabupaten
END code_kabupaten_faskes,
    CASE
WHEN a.id_role = 1 THEN i.code_provinsi
WHEN a.id_role = 2 THEN j.code_provinsi
END code_provinsi_faskes,
    a.jenis_input,
    CASE
WHEN a.jenis_input = 1 THEN 'Web'
WHEN a.jenis_input = 2 THEN 'Android'
WHEN a.jenis_input = 3 THEN 'Import'
END AS jenis_input_text,
    k.status_kasus,
    n.id AS id_pe,
    a.id_faskes_old,
    a.id_role_old,
    DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
    DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
    CASE
WHEN n.id IS NULL THEN 'PE'
ELSE 'Sudah PE'
END AS pe_text,
    b.jenis_kelamin,
    k.jenis_kasus,
    k.status_imunisasi_ibu AS jml_imunisasi,
    k.klb_ke
FROM
    trx_case a
    JOIN ref_pasien b ON a.id_pasien = b.id
    LEFT JOIN mst_kelurahan e ON b.code_kelurahan = e.code
    LEFT JOIN mst_kecamatan f ON e.code_kecamatan = f.code
    LEFT JOIN mst_kabupaten g ON f.code_kabupaten = g.code
    LEFT JOIN mst_provinsi h ON g.code_provinsi = h.code
    LEFT JOIN view_puskesmas i ON a.id_faskes = i.id AND a.id_role = 1
    LEFT JOIN view_rumahsakit j ON a.id_faskes = j.id AND a.id_role = 2
    JOIN trx_klinis k ON a.id = k.id_trx_case
    LEFT JOIN trx_pe n ON a.id = n.id_trx_case AND n.deleted_at IS NULL
WHERE
a.id_case = '4' AND a.deleted_at IS NULL
GROUP BY a.id
ORDER BY a.id ASC
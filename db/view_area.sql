SELECT
    a.code AS code_provinsi,
    a.name AS name_provinsi,
    b.code AS code_kabupaten,
    b.name AS name_kabupaten,
    c.code AS code_kecamatan,
    c.name AS name_kecamatan,
    d.code AS code_kelurahan,
    d.name AS name_kelurahan,
    CONCAT_WS(', ',d.name,c.name,b.name,a.name) AS full_address
FROM mst_provinsi a
    JOIN mst_kabupaten b ON a.code = b.code_provinsi
    JOIN mst_kecamatan c ON b.code = c.code_kabupaten
    JOIN mst_kelurahan d ON c.code = d.code_kecamatan
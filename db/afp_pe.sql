SELECT
a.id AS id_trx_case,
(CASE
WHEN a.pengambilan_spesimen = 1 THEN 'Ya'
WHEN a.pengambilan_spesimen = 2 THEN 'Tidak'
END) pengambilan_spesimen_txt,
a.alasan_spesimen_tidak_diambil,
b.id,
b.id AS id_trx_pe,
b.longitude,
b.latitude,
c.*,
d.*,
e.id AS id_trx_klinis,
DATE_FORMAT(e.tgl_sakit,'%d-%m-%Y') AS tgl_sakit,
DATE_FORMAT(e.tgl_mulai_lumpuh,'%d-%m-%Y') AS tgl_mulai_lumpuh,
DATE_FORMAT(e.tgl_meninggal,'%d-%m-%Y') AS tgl_meninggal,
(CASE
WHEN e.berobat_ke_unit_pelayanan_lain = 1 THEN 'Ya'
WHEN e.berobat_ke_unit_pelayanan_lain = 2 THEN 'Tidak'
END) AS berobat_ke_unit_pelayanan_lain_txt,
e.nama_unit_pelayanan,
DATE_FORMAT(e.tgl_berobat,'%d-%m-%Y') AS tgl_berobat,
e.diagnosis,
e.no_rm_lama,
(CASE
WHEN e.kelumpuhan_sifat_akut = 1 THEN 'Ya'
WHEN e.kelumpuhan_sifat_akut = 2 THEN 'Tidak'
WHEN e.kelumpuhan_sifat_akut = 3 THEN 'Tidak Jelas'
END) kelumpuhan_sifat_akut_txt,
(CASE
WHEN e.kelumpuhan_sifat_layuh = 1 THEN 'Ya'
WHEN e.kelumpuhan_sifat_layuh = 2 THEN 'Tidak'
WHEN e.kelumpuhan_sifat_layuh = 3 THEN 'Tidak Jelas'
END) kelumpuhan_sifat_layuh_txt,
(CASE
WHEN e.kelumpuhan_disebabkan_ruda = 1 THEN 'Ya'
WHEN e.kelumpuhan_disebabkan_ruda = 2 THEN 'Tidak'
WHEN e.kelumpuhan_disebabkan_ruda = 3 THEN 'Tidak Jelas'
END) kelumpuhan_disebabkan_ruda_txt,
(CASE
WHEN f.bepergian_sebelum_sakit = 1 THEN 'Ya'
WHEN f.bepergian_sebelum_sakit = 2 THEN 'Tidak'
WHEN f.bepergian_sebelum_sakit = 3 THEN 'Tidak Jelas'
END) bepergian_sebelum_sakit_txt,
f.lokasi_pergi_sebelum_sakit,
DATE_FORMAT(f.tgl_pergi_sebelum_sakit,'%d-%m-%Y') AS tgl_pergi_sebelum_sakit,
(CASE
WHEN f.berkunjung_ke_rumah_anak_imunisasi_polio = 1 THEN 'Ya'
WHEN f.berkunjung_ke_rumah_anak_imunisasi_polio = 2 THEN 'Tidak'
WHEN f.berkunjung_ke_rumah_anak_imunisasi_polio = 3 THEN 'Tidak Jelas'
END) berkunjung_ke_rumah_anak_imunisasi_polio_txt,
GROUP_CONCAT(DISTINCT CONCAT_WS('_',IFNULL(g.jenis_spesimen,''),IFNULL(DATE_FORMAT(g.tgl_ambil_spesimen,'%d-%m-%Y'),''),IFNULL(DATE_FORMAT(g.tgl_kirim_kab,'%d-%m-%Y'),''),IFNULL(DATE_FORMAT(g.tgl_kirim_prov,'%d-%m-%Y'),''),IFNULL(DATE_FORMAT(g.tgl_kirim_lab,'%d-%m-%Y'),'')) SEPARATOR '|') AS spesimen,
GROUP_CONCAT(DISTINCT h.nama_pelaksana SEPARATOR '<br/>') petugas_pelacak,
i.diagnosis AS diagnosis_hasil_pemeriksaan,
i.nama_pemeriksa,
i.no_telp
FROM
trx_case a
JOIN trx_pe b ON a.id = b.id_trx_case AND b.deleted_at IS NULL
JOIN (
    SELECT
    c1.id AS i1,
    DATE_FORMAT(c1.tgl_lahir, '%d-%m-%Y') AS tgl_lahir,
    c1.umur_thn,
    c1.umur_bln,
    c1.umur_hari,
    c1.name AS name_pasien,
    c3.full_address,
    c1.alamat,
    (CASE
    WHEN c1.jenis_kelamin = 'L' THEN 'Laki-laki'
    WHEN c1.jenis_kelamin = 'P' THEN 'Perempuan'
    WHEN c1.jenis_kelamin = 'T' THEN 'Tidak Jelas'
    ELSE 'Belum diisi'
    END) AS jenis_kelamin_txt,
    c2.name AS nama_ortu
    FROM
    ref_pasien c1
    JOIN ref_family c2 ON c1.id = c2.id_pasien
    LEFT JOIN view_area c3 ON c1.code_kelurahan = c3.code_kelurahan
) c ON a.id_pasien = c.i1
LEFT JOIN (
    SELECT
    d0.id_trx_pe AS i2,
    (CASE
    WHEN d0.sumber_laporan = 1 THEN 'Rumah sakit'
    WHEN d0.sumber_laporan = 2 THEN 'Puskesmas'
    WHEN d0.sumber_laporan = 3 THEN 'Dokter Praktek'
    WHEN d0.sumber_laporan = 4 THEN 'Lainnya'
    END) AS sumber_laporan_txt,
    d0.keterangan,
    d2.name AS name_provinsi_sumber_informasi,
    d1.name AS name_kabupaten_sumber_informasi
    FROM
    trx_pe_sumber_informasi d0
    LEFT JOIN mst_kabupaten d1 ON d0.code_kabupaten_sumber_informasi = d1.code
    LEFT JOIN mst_provinsi d2 ON d1.code_provinsi = d2.code
) d ON b.id = d.i2
LEFT JOIN trx_klinis e ON a.id = e.id_trx_case
LEFT JOIN trx_pe_riwayat_kontak f ON b.id = f.id_trx_pe
LEFT JOIN trx_spesimen g ON a.id = g.id_trx_case
LEFT JOIN trx_pe_pelaksana h ON b.id = h.id_trx_pe
LEFT JOIN trx_pe_hasil_pemeriksaan i ON b.id = i.id_trx_pe
WHERE
a.id_case = 2 AND a.deleted_at IS NULL 
GROUP BY a.id
SELECT
    a.id,
    a.code_kelurahan,
    a.code_faskes,
    b.name AS name_kelurahan,
    c.code AS code_kecamatan,
    c.name AS name_kecamatan,
    d.code AS code_kabupaten,
    d.name AS name_kabupaten,
    e.code AS code_provinsi,
    e.name AS name_provinsi,
    CONCAT_WS(', ', b.name, c.name, d.name, e.name) AS full_address,
    f.name,
    f.id AS id_faskes,
    f.code_kecamatan AS code_kecamatan_faskes,
    f.code_kabupaten AS code_kabupaten_faskes,
    f.code_provinsi AS code_provinsi_faskes
FROM
    mst_wilayah_kerja_puskesmas AS a
    JOIN mst_kelurahan b ON a.code_kelurahan = b.code
    JOIN mst_kecamatan c ON b.code_kecamatan = c.code
    JOIN mst_kabupaten d ON c.code_kabupaten = d.code
    JOIN mst_provinsi e ON d.code_provinsi = e.code
    JOIN mst_puskesmas f ON a.code_faskes = f.code_faskes
WHERE a.deleted_at IS NULL

SELECT
  a.id,
  a.id AS id_role_user,
  a.user_id AS id_user,
  a.role_id AS id_role,
  a.faskes_id AS id_faskes,
  a.penanggung_jawab_1 AS penanggung_jawab_1,
  a.penanggung_jawab_2 AS penanggung_jawab_2,
  b.name AS name_role,
  b.code AS code_role,
  a.deleted_at AS deleted_at,
  (CASE
  WHEN b.slug = 1 THEN c.name 
  WHEN b.slug = 2 THEN d.name 
  WHEN b.slug = 5 THEN e.name 
  WHEN b.slug = 6 THEN f.name 
  WHEN b.slug = 4 THEN g.name 
  ELSE b.name 
  END) AS name_faskes,
  (CASE
  WHEN b.slug = 1 THEN c.code_faskes 
  WHEN b.slug = 2 THEN d.code_faskes 
  ELSE NULL 
  END) AS code_faskes,
  (CASE
  WHEN b.slug = 1 THEN c.alamat 
  WHEN b.slug = 2 THEN d.alamat 
  WHEN b.slug = 4 THEN g.alamat 
  ELSE NULL 
  END) AS alamat_faskes,
  (CASE
  WHEN b.slug = 1 THEN c.code_kecamatan 
  WHEN b.slug = 2 THEN d.code_kabupaten 
  ELSE NULL 
  END) AS code_wilayah_faskes,
  (CASE
  WHEN b.slug = 1 THEN c.code_kecamatan 
  ELSE NULL 
  END) AS code_kecamatan_faskes,
  (CASE
  WHEN b.slug = 1 THEN c.name_kecamatan 
  ELSE NULL 
  END) AS name_kecamatan_faskes,
  (CASE
  WHEN b.slug = 1 THEN c.code_kabupaten 
  WHEN b.slug = 2 THEN d.code_kabupaten 
  ELSE NULL 
  END) AS code_kabupaten_faskes,
  (CASE
  WHEN b.slug = 1 THEN c.name_kabupaten 
  WHEN b.slug = 2 THEN d.name_kabupaten 
  ELSE NULL 
  END) AS name_kabupaten_faskes,
  (CASE
  WHEN b.slug = 1 THEN c.code_provinsi 
  WHEN b.slug = 2 THEN d.code_provinsi 
  WHEN b.slug = 6 THEN f.code_provinsi 
  ELSE NULL 
  END) AS code_provinsi_faskes,
  (CASE
  WHEN b.slug = 1 THEN c.name_provinsi 
  WHEN b.slug = 2 THEN d.name_provinsi 
  WHEN b.slug = 6 THEN f.name_provinsi 
  ELSE NULL 
  END) AS name_provinsi_faskes,
  i.first_name,
  i.last_name,
  CONCAT_WS(' ', i.first_name, i.last_name) full_name,
  j.alamat,
  j.instansi,
  j.jabatan,
  j.no_telp,
  i.email,
  (CASE
  WHEN j.jenis_kelamin = 'L' THEN 'Laki-laki' 
  WHEN j.jenis_kelamin = 'P' THEN 'Perempuan' 
  ELSE '-' END) AS jenis_kelamin_txt
FROM
  role_users a
  JOIN roles b ON a.role_id = b.id
  LEFT JOIN view_puskesmas c ON a.faskes_id = c.id
  LEFT JOIN view_rumahsakit d ON a.faskes_id = d.id
  LEFT JOIN mst_provinsi e ON a.faskes_id = e.code
  LEFT JOIN
  (SELECT
    f1.code AS code_provinsi,
    f1.name AS name_provinsi,
    f2.code,
    f2.name
  FROM
    mst_provinsi f1
    JOIN mst_kabupaten f2
    ON f1.code = f2.code_provinsi) f ON a.faskes_id = f.code
  LEFT JOIN mst_laboratorium g ON a.faskes_id = g.id
  JOIN users i ON a.user_id = i.id
  JOIN users_meta j ON i.id = j.id_user
WHERE a.deleted_at IS NULL 
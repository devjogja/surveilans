SELECT
    a.id AS id_trx_case,
    a.id_role,
    a.id_faskes,
    d.alias AS case_type,
    b.jenis_kelamin,
    (CASE
    WHEN a.id_case = 1 THEN k.tgl_mulai_rash
    WHEN a.id_case = 2 THEN k.tgl_mulai_lumpuh
    WHEN a.id_case = 3 THEN k.tgl_mulai_demam
    WHEN a.id_case = 4 THEN k.tgl_mulai_sakit
    WHEN a.id_case = 5 THEN k.tgl_periksa
    END) AS tgl_sakit_date,
    (CASE
    WHEN a.id_case = 1 THEN YEAR(k.tgl_mulai_rash)
    WHEN a.id_case = 2 THEN YEAR(k.tgl_mulai_lumpuh)
    WHEN a.id_case = 3 THEN YEAR(k.tgl_mulai_demam)
    WHEN a.id_case = 4 THEN YEAR(k.tgl_mulai_sakit)
    WHEN a.id_case = 5 THEN YEAR(k.tgl_periksa)
    END) AS thn_sakit,
    b.umur_thn,
    a.klasifikasi_final,
    c.rkf_name AS klasifikasi_final_txt,
    b.code_provinsi AS code_provinsi_pasien,
    b.code_kabupaten AS code_kabupaten_pasien,
    b.code_kecamatan AS code_kecamatan_pasien,
    b.code_kelurahan AS code_kelurahan_pasien,
    (CASE
WHEN a.id_role = 1 THEN i.code_faskes
WHEN a.id_role = 2 THEN j.code_faskes
END) code_faskes,
    (CASE
WHEN a.id_role = 1 THEN i.code_kecamatan
WHEN a.id_role = 2 THEN j.code_kecamatan
END) code_kecamatan_faskes,
    (CASE
WHEN a.id_role = 1 THEN i.code_kabupaten
WHEN a.id_role = 2 THEN j.code_kabupaten
END) code_kabupaten_faskes,
    (CASE
WHEN a.id_role = 1 THEN i.code_provinsi
WHEN a.id_role = 2 THEN j.code_provinsi
END) code_provinsi_faskes,
    (CASE
    WHEN a.id_case = 1 THEN k.jml_imunisasi_campak
    WHEN a.id_case = 2 THEN k.imunisasi_rutin_polio_sebelum_sakit
    WHEN a.id_case = 3 THEN k.jml_imunisasi_dpt
    WHEN a.id_case = 4 THEN k.status_imunisasi_ibu
    WHEN a.id_case = 5 THEN NULL
    END) AS jml_imunisasi,
    k.jenis_kasus,
    k.pin_mopup_ori_biaspolio
FROM
    trx_case a
    JOIN ref_pasien b ON a.id_pasien = b.id
    JOIN trx_klinis k ON a.id = k.id_trx_case
    JOIN mst_case d ON a.id_case = d.id
    LEFT JOIN ref_klasifikasi_final c ON a.id_case = c.id_case AND a.klasifikasi_final = c.klasifikasi_final
    LEFT JOIN view_puskesmas i ON a.id_faskes = i.id AND a.id_role = 1
    LEFT JOIN view_rumahsakit j ON a.id_faskes = j.id AND a.id_role = 2
WHERE
a.deleted_at IS NULL
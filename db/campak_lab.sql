SELECT
    a.no_epid,
    b.id_trx_case,
    c.sumber_spesimen,
    c.id_outbreak,
    c.laporan_oleh,
    c.id_provinsi_outbreak,
    c.id_kabupaten_outbreak,
    c.id_kecamatan_outbreak,
    c.id_kelurahan_outbreak,
    c.alamat,
    c.code_lab,
    DATE_FORMAT(c.tgl_terima_spesimen,'%d-%m-%Y') AS tgl_terima_spesimen,
    c.kondisi
FROM
    trx_case a
    JOIN trx_lab b ON a.id = b.id_trx_case
    JOIN trx_lab_campak c ON b.id = c.id_trx_lab
WHERE
a.deleted_at IS NULL AND b.deleted_at IS NULL
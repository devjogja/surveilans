SELECT
    a.id AS id_trx_case,
    (CASE
    WHEN a.id_role = 1 THEN CONCAT('Puskesmas ',i.name)
    WHEN a.id_role = 2 THEN j.name
    END) AS faskes,
    a.id_faskes,
    a.id_pasien,
    b.id AS id_trx_notification,
    b.type AS type_notification,
    DATE_FORMAT(b.created_at,'%d-%m-%Y %H:%i:%s') AS date_submit,
    b.from_faskes,
    b.to_faskes,
    c.name AS name_case,
    c.alias AS alias_case,
    CONCAT('Kecamatan ',e.name_kecamatan) AS district_pasien,
    f.code AS code_roles,
    g.code_kabupaten AS to_faskes_kabupaten,
    h.code_provinsi AS to_faskes_provinsi
FROM
    trx_case a
    JOIN trx_notification b ON a.id = b.id_trx_case AND b.deleted_at IS NULL
    JOIN mst_case c ON a.id_case = c.id
    JOIN ref_pasien d ON a.id_pasien = d.id
    LEFT JOIN view_area e ON d.code_kelurahan = e.code_kelurahan
    JOIN roles f ON a.id_role = f.id
    LEFT JOIN mst_puskesmas i ON a.id_faskes = i.id AND a.id_role = 1
    LEFT JOIN mst_rumahsakit j ON a.id_faskes = j.id AND a.id_role = 2
    JOIN mst_kecamatan g ON b.to_faskes = g.code
    JOIN mst_kabupaten h ON g.code_kabupaten = h.code
WHERE
a.deleted_at IS NULL
ORDER BY a.id DESC
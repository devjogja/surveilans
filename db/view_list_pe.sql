SELECT
    a.id AS id_pe,
    b.id_case,
    b.id_role,
    b.id_faskes,
    b.no_epid,
    b.no_epid_klb,
    b.jenis_input,
    c.name AS name_pasien,
    c.umur_hari,
    c.umur_bln,
    c.umur_thn,
    d.name AS nama_ortu,
    (CASE
    WHEN c.jenis_kelamin = 'L' THEN 'Laki-laki'
    WHEN c.jenis_kelamin = 'P' THEN 'Perempuan'
    WHEN c.jenis_kelamin = 'T' THEN 'Tidak Jelas'
    ELSE NULL
    END) AS jenis_kelamin_txt,
    CONCAT_WS(', ', c.alamat, e.full_address) AS full_address,
    a.total_kasus_tambahan,
    (CASE
    WHEN b.id_role = 1 THEN CONCAT('PUSKESMAS ',i.name)
    WHEN b.id_role = 2 THEN j.name
    END) name_faskes,
    DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
    DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
    k.status_kasus,
    (CASE
    WHEN b.id_role = 1 THEN i.code_kecamatan
    WHEN b.id_role = 2 THEN j.code_kecamatan
    END) code_kecamatan_faskes,
    (CASE
    WHEN b.id_role = 1 THEN i.code_kabupaten
    WHEN b.id_role = 2 THEN j.code_kabupaten
    END) code_kabupaten_faskes,
    (CASE
    WHEN b.id_role = 1 THEN i.code_provinsi
    WHEN b.id_role = 2 THEN j.code_provinsi
    END) code_provinsi_faskes,
    e.code_kelurahan AS code_kelurahan_pasien,
    e.code_kecamatan AS code_kecamatan_pasien,
    e.code_kabupaten AS code_kabupaten_pasien,
    e.code_provinsi AS code_provinsi_pasien
FROM
    trx_pe a
    JOIN trx_case b ON a.id_trx_case = b.id AND b.deleted_at IS NULL
    JOIN ref_pasien c ON b.id_pasien = c.id
    JOIN ref_family d ON c.id = d.id_pasien
    LEFT JOIN view_area e ON c.code_kelurahan = e.code_kelurahan
    LEFT JOIN view_puskesmas i ON a.id_faskes = i.id AND a.id_role = 1
    LEFT JOIN view_rumahsakit j ON a.id_faskes = j.id AND a.id_role = 2
    JOIN trx_klinis k ON b.id = k.id_trx_case
WHERE
a.deleted_at IS NULL
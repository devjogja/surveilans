SELECT
    a.id AS id_trx_case,
    a.id_case,
    a.id_role,
    a.id_faskes,
    a.no_epid,
    a.no_epid_klb,
    a.jenis_input,
    b.id AS id_ku60,
    c.name AS name_pasien,
    c.umur_thn,
    c.umur_bln,
    c.umur_hari,
    d.name AS nama_ortu,
    (CASE
    WHEN c.jenis_kelamin = 'L' THEN 'Laki-laki'
    WHEN c.jenis_kelamin = 'P' THEN 'Perempuan'
    WHEN c.jenis_kelamin = 'T' THEN 'Tidak Jelas'
    ELSE NULL
    END) AS jenis_kelamin_txt,
    CONCAT_WS(', ', c.alamat, e.full_address) AS full_address,
    (CASE
    WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',i.name)
    WHEN a.id_role = 2 THEN j.name
    END) name_faskes,
    DATE_FORMAT(b.created_at, '%d-%m-%Y') AS tgl_input,
    DATE_FORMAT(b.updated_at, '%d-%m-%Y') AS tgl_update,
    k.status_kasus,
    (CASE
    WHEN a.id_role = 1 THEN i.code_kecamatan
    WHEN a.id_role = 2 THEN j.code_kecamatan
    END) code_kecamatan_faskes,
    (CASE
    WHEN a.id_role = 1 THEN i.code_kabupaten
    WHEN a.id_role = 2 THEN j.code_kabupaten
    END) code_kabupaten_faskes,
    (CASE
    WHEN a.id_role = 1 THEN i.code_provinsi
    WHEN a.id_role = 2 THEN j.code_provinsi
    END) code_provinsi_faskes,
    e.code_kelurahan AS code_kelurahan_pasien,
    e.code_kecamatan AS code_kecamatan_pasien,
    e.code_kabupaten AS code_kabupaten_pasien,
    e.code_provinsi AS code_provinsi_pasien
FROM
    trx_case a
    JOIN trx_ku60 b ON a.id = b.id_trx_case AND b.deleted_at IS NULL
    JOIN ref_pasien c ON a.id_pasien = c.id
    JOIN ref_family d ON c.id = d.id_pasien
    LEFT JOIN view_area e ON c.code_kelurahan = e.code_kelurahan
    LEFT JOIN view_puskesmas i ON b.id_faskes = i.id AND b.id_role = 1
    LEFT JOIN view_rumahsakit j ON b.id_faskes = j.id AND b.id_role = 2
    JOIN trx_klinis k ON a.id = k.id_trx_case
WHERE
a.deleted_at IS NULL
ORDER BY b.id ASC
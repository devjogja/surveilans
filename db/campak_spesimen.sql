SELECT
a.id AS id_trx_case,
b.id AS id_trx_spesimen,
b.jenis_pemeriksaan,
b.no_spesimen,
(CASE
WHEN b.jenis_pemeriksaan = 1 THEN 'Serologi' 
WHEN b.jenis_pemeriksaan = 2 THEN 'Virologi' 
END) AS jenis_pemeriksaan_txt,
b.jenis_spesimen AS jenis_spesimen,
(CASE
WHEN b.jenis_spesimen = 11 THEN 'Darah' 
WHEN b.jenis_spesimen = 12 THEN 'Urin' 
WHEN b.jenis_spesimen = 21 THEN 'Urin' 
WHEN b.jenis_spesimen = 22 THEN 'Usap Tenggorokan' 
WHEN b.jenis_spesimen = 23 THEN 'Cairan mulut' 
END) AS jenis_spesimen_txt,
DATE_FORMAT(b.tgl_ambil_spesimen,'%d-%m-%Y') AS tgl_ambil_spesimen,
DATE_FORMAT(b.tgl_kirim_lab,'%d-%m-%Y') AS tgl_kirim_lab,
DATE_FORMAT(b.tgl_terima_lab,'%d-%m-%Y') AS tgl_terima_lab,
b.sampel_ke,
b.hasil AS hasil,
(CASE
WHEN b.jenis_pemeriksaan = 1 AND b.hasil = '1' THEN 'IgM Campak Positif' 
WHEN b.jenis_pemeriksaan = 1 AND b.hasil = '2' THEN 'IgM Campak Negatif' 
WHEN b.jenis_pemeriksaan = 1 AND b.hasil = '3' THEN 'IgM Rubella Positif' 
WHEN b.jenis_pemeriksaan = 1 AND b.hasil = '4' THEN 'IgM Rubella Positif' 
WHEN b.jenis_pemeriksaan = 1 AND b.hasil = '5' THEN 'IgM Campak Positif & IgM Rubella Positif' 
WHEN b.jenis_pemeriksaan = 1 AND b.hasil = '6' THEN 'IgM Rubella Equivocal' 
WHEN b.jenis_pemeriksaan = 1 AND b.hasil = '7' THEN 'IgM Campak Equivocal' 
WHEN b.jenis_pemeriksaan = 1 AND b.hasil = '8' THEN 'IgM Campak Negatif & IgM Rubella Negatif' 
WHEN b.jenis_pemeriksaan = 2 THEN b.hasil 
END) AS hasil_txt,
DATE_FORMAT(c.tgl_periksa_kultur,'%d-%m-%Y') AS tgl_periksa_kultur,
DATE_FORMAT(c.tgl_hasil_kultur_tersedia,'%d-%m-%Y') AS tgl_hasil_kultur_tersedia,
c.hasil_kultur,
(CASE
WHEN c.hasil_kultur= 1 THEN 'Positif' 
WHEN c.hasil_kultur= 2 THEN 'Negatif' 
WHEN c.hasil_kultur= 3 THEN 'Pending' 
WHEN c.hasil_kultur= 4 THEN 'Equivocal' 
END) AS hasil_kultur_txt,
DATE_FORMAT(c.tgl_hasil_dilaporkan_kultur,'%d-%m-%Y') AS tgl_hasil_dilaporkan_kultur,
d.onset_kualifikasi_pcr,
d.jenis_pemeriksaan_pcr,
DATE_FORMAT(d.tgl_periksa_pcr,'%d-%m-%Y') AS tgl_periksa_pcr,
DATE_FORMAT(d.tgl_keluar_hasil_pcr,'%d-%m-%Y') AS tgl_keluar_hasil_pcr,
d.metode_pcr,
d.hasil_pcr,
(CASE
WHEN d.hasil_pcr= 1 THEN 'Positif virus campak' 
WHEN d.hasil_pcr= 2 THEN 'Negatif virus campak' 
WHEN d.hasil_pcr= 3 THEN 'Positif virus rubella' 
WHEN d.hasil_pcr= 4 THEN 'Negatif virus rubella' 
WHEN d.hasil_pcr= 5 THEN 'Pending' 
END) AS hasil_pcr_txt,
DATE_FORMAT(d.tgl_hasil_dilaporkan_pcr,'%d-%m-%Y') AS tgl_hasil_dilaporkan_pcr,
e.sampel_dirujuk_sqc,
e.lab_rujukan_sqc,
DATE_FORMAT(e.tgl_dirujuk_sqc,'%d-%m-%Y') AS tgl_dirujuk_sqc,
DATE_FORMAT(e.tgl_pemeriksaan_sqc,'%d-%m-%Y') AS tgl_pemeriksaan_sqc,
DATE_FORMAT(e.tgl_hasil_sqc,'%d-%m-%Y') AS tgl_hasil_sqc,
e.hasil_sqc,
(CASE
WHEN e.hasil_sqc= 1 THEN 'Positif virus campak' 
WHEN e.hasil_sqc= 2 THEN 'Negatif virus campak' 
WHEN e.hasil_sqc= 3 THEN 'Positif virus rubella' 
WHEN e.hasil_sqc= 4 THEN 'Negatif virus rubella' 
WHEN e.hasil_sqc= 5 THEN 'Pending' 
END) AS hasil_sqc_txt,
DATE_FORMAT(e.tgl_hasil_kirim_darilab_sqc,'%d-%m-%Y') AS tgl_hasil_kirim_darilab_sqc,
DATE_FORMAT(e.tgl_hasil_dilaporkan_sqc,'%d-%m-%Y') AS tgl_hasil_dilaporkan_sqc,
e.means_ruben,
DATE_FORMAT(f.tgl_periksa_igm_campak,'%d-%m-%Y') AS tgl_periksa_igm_campak,
f.kit_igm_campak,
f.hasil_igm_campak,
(CASE
WHEN f.hasil_igm_campak= 1 THEN 'Positif' 
WHEN f.hasil_igm_campak= 2 THEN 'Negatif' 
WHEN f.hasil_igm_campak= 3 THEN 'Pending' 
WHEN f.hasil_igm_campak= 4 THEN 'Equivocal' 
END) AS hasil_igm_campak_txt,
DATE_FORMAT(f.tgl_hasil_tersedia_igm_campak,'%d-%m-%Y') AS tgl_hasil_tersedia_igm_campak,
DATE_FORMAT(f.tgl_hasil_dilaporkan_igm_campak,'%d-%m-%Y') AS tgl_hasil_dilaporkan_igm_campak,
DATE_FORMAT(f.tgl_periksa_igm_rubella,'%d-%m-%Y') AS tgl_periksa_igm_rubella,
f.kit_igm_rubella,
f.hasil_igm_rubella,
(CASE
WHEN f.hasil_igm_rubella= 1 THEN 'Positif' 
WHEN f.hasil_igm_rubella= 2 THEN 'Negatif' 
WHEN f.hasil_igm_rubella= 3 THEN 'Pending' 
WHEN f.hasil_igm_rubella= 4 THEN 'Equivocal' 
END) AS hasil_igm_rubella_txt,
DATE_FORMAT(f.tgl_hasil_tersedia_igm_rubella,'%d-%m-%Y') AS tgl_hasil_tersedia_igm_rubella,
DATE_FORMAT(f.tgl_hasil_dilaporkan_igm_rubella,'%d-%m-%Y') AS tgl_hasil_dilaporkan_igm_rubella
FROM
trx_case a
JOIN trx_spesimen b ON a.id = b.id_trx_case
LEFT JOIN trx_pemeriksaan_kultur c ON b.id = c.id_trx_spesimen
LEFT JOIN trx_pemeriksaan_pcr d ON b.id = d.id_trx_spesimen
LEFT JOIN trx_pemeriksaan_sequencing e ON b.id = e.id_trx_spesimen
LEFT JOIN trx_pemeriksaan_igm f ON b.id = f.id_trx_spesimen
WHERE a.id_case = 1 AND a.deleted_at IS NULL
ORDER BY b.id ASC
select
    a.id,
    a.code_faskes,
    a.code_faskes_bpjs,
    a.name,
    a.type,
    a.kepemilikan,
    a.alamat,
    a.kontrak_bpjs,
    a.regional_bpjs,
    a.nama_kc,
    a.regional_lokal,
    a.longitude,
    a.latitude,
    a.telp,
    a.sms,
    a.wilayah_koordinasi,
    a.konfirm_code,
    null as code_kelurahan,
    null as code_kecamatan,
    a.code_kabupaten,
    a.code_provinsi,
    null as name_kelurahan,
    null as name_kecamatan,
    b.name AS name_kabupaten,
    c.name AS name_provinsi,
    CONCAT_WS(', ',b.name,c.name) AS full_address
FROM
    mst_rumahsakit a
    JOIN mst_kabupaten b ON a.code_kabupaten = b.code
    JOIN mst_provinsi c ON b.code_provinsi = c.code
WHERE a.deleted_at IS NULL
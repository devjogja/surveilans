SELECT
a.id AS id_tx_case,
b.id AS id_txr_pe,
b.longitude,
b.latitude,
c.nama_pelapor,
c.nama_kantor,
c.jabatan,
c1.name AS name_provinsi_pelapor,
c2.name AS name_kabupaten_pelapor,
DATE_FORMAT(c.tgl_laporan, '%d-%m-%Y') AS tgl_laporan,
d.no_telp,
d.tempat_tinggal_saat_ini,
d.tempat_tinggal_sesuai_kartu,
d.pekerjaan,
d.alamat_kerja,
b.faskes_tempat_periksa,
e.nama_saudara_yg_dihubungi,
e.alamat AS alamat_saudara,
e1.name AS provinsi_saudara,
e2.name AS kabupaten_saudara,
e3.name AS kecamatan_saudara,
e4.name AS kelurahan_saudara,
e.no_telp AS no_telp_saudara,
DATE_FORMAT(f.tgl_mulai_sakit, '%d-%m-%Y') AS tgl_mulai_sakit,
f.keluhan_untuk_berobat,
GROUP_CONCAT(DISTINCT CONCAT(g.id_gejala,'_',DATE_FORMAT(g.tgl_kejadian, '%d-%m-%Y')) SEPARATOR '|') AS gejala_tanda_sakit,
GROUP_CONCAT(DISTINCT g.val_gejala SEPARATOR '<br/>') gejala_lain_pe,
CASE
WHEN f.status_imunisasi_difteri = 1 THEN 'Belum Pernah'
WHEN f.status_imunisasi_difteri = 2 THEN 'Sudah Pernah'
WHEN f.status_imunisasi_difteri = 3 THEN 'Tidak Tahu'
END AS status_imunisasi_difteri_txt,
f.jml_imunisasi_dpt,
f.tahun_imunisasi_dpt,
h.berobat_ke_rs,
CASE
WHEN h.riwayat_pengobatan_rs = 1 THEN 'Ya'
WHEN h.riwayat_pengobatan_rs = 2 THEN 'Tidak'
ELSE ''
END AS riwayat_pengobatan_rs_txt,
CASE
WHEN h.trakeostomi = 1 THEN 'Ya'
WHEN h.trakeostomi = 2 THEN 'Tidak'
END AS trakeostomi_txt,
h.berobat_ke_puskesmas,
CASE
WHEN h.riwayat_pengobatan_puskesmas = 1 THEN 'Ya'
WHEN h.riwayat_pengobatan_puskesmas = 2 THEN 'Tidak'
END AS riwayat_pengobatan_puskesmas_txt,
h.dokter_praktek_swasta,
h.perawat_mantri_bidan,
h.tidak_berobat,
h.antibiotik,
h.obat_lain,
h.ads,
CASE
WHEN h.kondisi_kasus_saat_ini = 1 THEN 'Masih Sakit'
WHEN h.kondisi_kasus_saat_ini = 2 THEN 'Sembuh'
WHEN h.kondisi_kasus_saat_ini = 2 THEN 'Meninggal'
END AS kondisi_kasus_saat_ini_txt,
CASE
WHEN i.bepergian_dalam_2_minggu = 1 THEN 'Pernah'
WHEN i.bepergian_dalam_2_minggu = 2 THEN 'Tidak Pernah'
WHEN i.bepergian_dalam_2_minggu = 3 THEN 'Tidak Jelas'
END AS bepergian_dalam_2_minggu_txt,
i.tujuan_bepergian_dalam_2_minggu,
CASE
WHEN i.2_minggu_terakhir_berkunjung_ke_org_meninggal_gejala_sama = 1 THEN 'Pernah'
WHEN i.2_minggu_terakhir_berkunjung_ke_org_meninggal_gejala_sama = 2 THEN 'Tidak Pernah'
WHEN i.2_minggu_terakhir_berkunjung_ke_org_meninggal_gejala_sama = 3 THEN 'Tidak Jelas'
END AS 2_minggu_terakhir_berkunjung_ke_org_meninggal_gejala_sama_txt,
i.tujuan_berkunjung_ke_org_meninggal_gejala_sama,
CASE
WHEN i.2_minggu_terakhir_ada_tamu_gejala_sama = 1 THEN 'Pernah'
WHEN i.2_minggu_terakhir_ada_tamu_gejala_sama = 2 THEN 'Tidak Pernah'
WHEN i.2_minggu_terakhir_ada_tamu_gejala_sama = 3 THEN 'Tidak Jelas'
END AS 2_minggu_terakhir_ada_tamu_gejala_sama_txt,
i.asal_tamu_gejala_sama,
GROUP_CONCAT(DISTINCT CONCAT_WS('_',j.nama,j.umur,j.hub_dg_kasus,j.status_imunisasi,DATE_FORMAT(j.tgl_ambil_tenggorokan, '%d-%m-%Y'),j.hasil_tenggorokan,DATE_FORMAT(j.tgl_ambil_hidung, '%d-%m-%Y'),j.hasil_hidung,j.profilaksis) SEPARATOR '|') AS kontak_kasus
FROM
trx_case a
JOIN trx_pe b ON a.id = b.id_trx_case
LEFT JOIN trx_pelapor c ON b.id = c.id_trx_pe
LEFT JOIN mst_provinsi c1 ON c.code_provinsi_pelapor = c1.code
LEFT JOIN mst_kabupaten c2 ON c.code_kabupaten_pelapor = c2.code
JOIN ref_pasien d ON a.id_pasien = d.id
JOIN ref_family e ON d.id = e.id_pasien
LEFT JOIN mst_provinsi e1 ON e.code_provinsi = e1.code
LEFT JOIN mst_kabupaten e2 ON e.code_kabupaten = e2.code
LEFT JOIN mst_kecamatan e3 ON e.code_kecamatan = e3.code
LEFT JOIN mst_kelurahan e4 ON e.code_kelurahan = e4.code
JOIN trx_klinis f ON a.id = f.id_trx_case
LEFT JOIN trx_gejala g ON f.id = g.id_trx_klinis
LEFT JOIN trx_pe_riwayat_pengobatan h ON b.id = h.id_trx_pe
LEFT JOIN trx_pe_riwayat_kontak i ON b.id = i.id_trx_pe
LEFT JOIN trx_pe_kontak_kasus j ON b.id = j.id_trx_pe
WHERE
a.id_case = '3' AND a.deleted_at IS NULL AND b.deleted_at IS NULL
GROUP BY a.id
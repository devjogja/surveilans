SELECT
    a.id_trx_case,
    b.id_case,
    b.klasifikasi_final,
    b.id_role,
    b.id_faskes,
    c.code_kelurahan AS code_kelurahan_pasien,
    (CASE
    WHEN b.id_case = 1 THEN YEAR(k.tgl_mulai_rash)
    WHEN b.id_case = 2 THEN YEAR(k.tgl_mulai_lumpuh)
    WHEN b.id_case = 3 THEN YEAR(k.tgl_mulai_demam)
    WHEN b.id_case = 4 THEN YEAR(k.tgl_mulai_sakit)
    WHEN b.id_case = 5 THEN YEAR(k.tgl_periksa)
    END) AS thn_sakit
FROM
    trx_spesimen a
    JOIN trx_case b ON a.id_trx_case = b.id AND b.deleted_at IS NULL
    JOIN ref_pasien c ON b.id_pasien = c.id
    JOIN trx_klinis k ON b.id = k.id_trx_case
WHERE
a.deleted_at IS NULL
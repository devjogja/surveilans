SELECT
    a.id AS id,
    a.email AS email,
    a.first_name AS first_name,
    a.last_name AS last_name,
    b.alamat AS alamat,
    b.instansi AS instansi,
    b.jabatan AS jabatan,
    b.no_telp AS no_telp,
    b.jenis_kelamin AS jenis_kelamin,
    (CASE
    WHEN (b.jenis_kelamin = 1) THEN 'Laki-laki'
    WHEN (b.jenis_kelamin = 2) THEN 'Perempuan'
    WHEN (b.jenis_kelamin = 3) THEN 'Tidak diketahui'
    ELSE NULL END) AS jenis_kelamin_txt
FROM users a
    JOIN users_meta b ON a.id = b.id_user
WHERE a.deleted_at IS NULL
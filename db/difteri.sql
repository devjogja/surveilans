SELECT  a.id AS id,
a.id AS id_trx_case,
a.id_role,
a.id_role_old,
a.jenis_input,
CASE
WHEN a.jenis_input = 1 THEN 'Web'
WHEN a.jenis_input = 2 THEN 'Android'
WHEN a.jenis_input = 3 THEN 'Import'
END AS jenis_input_text,
a.id_faskes,
a.id_faskes_old,
DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
DATE_FORMAT (a.created_at, '%d-%m-%Y %H:%i:%s') AS created_at,
DATE_FORMAT (a.updated_at, '%d-%m-%Y %H:%i:%s') AS updated_at,
DATE_FORMAT (a.deleted_at, '%d-%m-%Y %H:%i:%s') AS deleted_at,
a.no_rm,
a.no_epid,
a._no_epid,
a.no_epid_lama,
CASE
WHEN a.klasifikasi_final = 1 THEN 'Konfirm'
WHEN a.klasifikasi_final = 2 THEN 'Probable'
WHEN a.klasifikasi_final = 3 THEN 'Negatif'
END klasifikasi_final_txt,
a.klasifikasi_final,
CASE
WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',i.name)
WHEN a.id_role = 2 THEN j.name
END name_faskes,
CASE
WHEN a.id_role = 1 THEN i.code_faskes
WHEN a.id_role = 2 THEN j.code_faskes
END AS code_faskes,
CASE
WHEN a.id_role = 1 THEN i.name_kelurahan
WHEN a.id_role = 2 THEN j.name_kelurahan
END name_kelurahan_faskes,
CASE
WHEN a.id_role = 1 THEN i.code_kelurahan
WHEN a.id_role = 2 THEN j.code_kelurahan
END code_kelurahan_faskes,
CASE
WHEN a.id_role = 1 THEN i.name_kecamatan
WHEN a.id_role = 2 THEN j.name_kecamatan
END name_kecamatan_faskes,
CASE
WHEN a.id_role = 1 THEN i.code_kecamatan
WHEN a.id_role = 2 THEN j.code_kecamatan
END code_kecamatan_faskes,
CASE
WHEN a.id_role = 1 THEN i.name_kabupaten
WHEN a.id_role = 2 THEN j.name_kabupaten
END name_kabupaten_faskes,
CASE
WHEN a.id_role = 1 THEN i.code_kabupaten
WHEN a.id_role = 2 THEN j.code_kabupaten
END code_kabupaten_faskes,
CASE
WHEN a.id_role = 1 THEN i.name_provinsi
WHEN a.id_role = 2 THEN j.name_provinsi
END name_provinsi_faskes,
CASE
WHEN a.id_role = 1 THEN i.code_provinsi
WHEN a.id_role = 2 THEN j.code_provinsi
END code_provinsi_faskes,
DATE_FORMAT(k.tgl_mulai_demam, '%d-%m-%Y') AS tgl_mulai_demam,
k.tgl_mulai_demam AS tgl_sakit_date,
CASE
WHEN k.jml_imunisasi_dpt = 7 THEN 'Tidak'
WHEN k.jml_imunisasi_dpt = 8 THEN 'Tidak Tahu'
WHEN k.jml_imunisasi_dpt = 9 THEN 'Belum Pernah'
WHEN k.jml_imunisasi_dpt IN ('1','2','3','4','5','6') THEN CONCAT(k.jml_imunisasi_dpt, 'x')
ELSE 'Belum diisi' END AS jml_imunisasi_dpt_txt,
k.jml_imunisasi_dpt AS jml_imunisasi,
k.jml_imunisasi_dpt,
DATE_FORMAT(k.tgl_imunisasi_difteri, '%d-%m-%Y') AS tgl_imunisasi_difteri,
DATE_FORMAT(k.tgl_pelacakan, '%d-%m-%Y') AS tgl_pelacakan,
k.gejala_lain,
k.jml_kontak,
k.diambil_spec_kontak,
k.positif_kontak,
k.keadaan_akhir,
k.keluhan_untuk_berobat,
CASE
WHEN k.keadaan_akhir = 1 THEN 'Hidup'
WHEN k.keadaan_akhir = 2 THEN 'Meninggal'
END keadaan_akhir_txt,
GROUP_CONCAT(DISTINCT CONCAT(IFNULL(o.jenis_spesimen,''),'_',IFNULL(DATE_FORMAT(o.tgl_ambil_spesimen,'%d-%m-%Y'),''),'_',IFNULL(o.jenis_pemeriksaan,''),'_',IFNULL(o.kode_spesimen,''),'_',IFNULL(o.hasil,'')) SEPARATOR '|') AS spesimen,
a.id_case,
a.id_pasien,
r.code AS code_roles,
k.id AS id_tx_klinis,
CASE
WHEN k.status_imunisasi_difteri = 1 THEN 'Belum Pernah'
WHEN k.status_imunisasi_difteri = 2 THEN 'Sudah Pernah'
WHEN k.status_imunisasi_difteri = 3 THEN 'Tidak Tahu'
ELSE NULL
END AS status_imunisasi_difteri_txt,
k.tahun_imunisasi_dpt,
n.id AS id_pe,
date_format (k.tgl_mulai_demam, '%d-%m-%Y') AS tgl_mulai_sakit,
date_format (k.tgl_mulai_demam, '%d-%m-%Y') AS tgl_sakit,
YEAR (k.tgl_mulai_demam) AS thn_sakit,
b.*
FROM
trx_case a
LEFT JOIN roles r ON a.id_role = r.id
JOIN (
SELECT
a.id AS i1,
b.name AS name_pasien,
b.nik,
b.jenis_kelamin,
CASE
WHEN b.jenis_kelamin = 'L' THEN 'Laki-laki'
WHEN b.jenis_kelamin = 'P' THEN 'Perempuan'
WHEN b.jenis_kelamin = 'T' THEN 'Tidak Jelas'
ELSE 'Belum diisi' END AS jenis_kelamin_txt,
DATE_FORMAT(b.tgl_lahir, '%d-%m-%Y') AS tgl_lahir,
b.umur_thn,
b.umur_bln,
b.umur_hari,
b.tempat_lahir,
b.alamat,
e.code AS code_kelurahan_pasien,
e.name AS name_kelurahan_pasien,
f.code AS code_kecamatan_pasien,
f.name AS name_kecamatan_pasien,
g.code AS code_kabupaten_pasien,
g.name AS name_kabupaten_pasien,
h.code AS code_provinsi_pasien,
h.name AS name_provinsi_pasien,
CONCAT_WS(', ', b.alamat, e.name, f.name, g.name, h.name) AS full_address,
b.longitude,
b.latitude,
b.tempat_tinggal_saat_ini,
b.tempat_tinggal_sesuai_kartu,
b.no_telp,
b.pekerjaan,
b.alamat_kerja,
b.alamat_saat_ini,
b4.code AS code_provinsi_tempat_tinggal_sekarang,
b4.name AS name_provinsi_tempat_tinggal_sekarang,
b3.code AS code_kabupaten_tempat_tinggal_sekarang,
b3.name AS name_kabupaten_tempat_tinggal_sekarang,
b2.code AS code_kecamatan_tempat_tinggal_sekarang,
b2.name AS name_kecamatan_tempat_tinggal_sekarang,
b1.code AS code_kelurahan_tempat_tinggal_sekarang,
b1.name AS name_kelurahan_tempat_tinggal_sekarang,
d1.code AS code_kelurahan_ortu,
d1.name AS name_kelurahan_ortu,
d2.code AS code_kecamatan_ortu,
d2.name AS name_kecamatan_ortu,
d3.code AS code_kabupaten_ortu,
d3.name AS name_kabupaten_ortu,
d4.code AS code_provinsi_ortu,
d4.name AS name_provinsi_ortu,
d.nama_saudara_yg_dihubungi,
d.no_telp AS no_telp_ortu,
d.alamat AS alamat_ortu,
d.id AS id_family,
d.name AS nama_ortu,
c.id AS agama,
c.name AS religion_text
FROM
trx_case a
JOIN ref_pasien b ON a.id_pasien = b.id
LEFT JOIN mst_religion c ON b.agama = c.id
JOIN ref_family d ON b.id = d.id_pasien
LEFT JOIN mst_kelurahan e ON b.code_kelurahan = e.code
LEFT JOIN mst_kecamatan f ON e.code_kecamatan = f.code
LEFT JOIN mst_kabupaten g ON f.code_kabupaten = g.code
LEFT JOIN mst_provinsi h ON g.code_provinsi = h.code
LEFT JOIN mst_kelurahan b1 ON b.code_kelurahan_tempat_tinggal_sekarang = b1.code
LEFT JOIN mst_kecamatan b2 ON b1.code_kecamatan = b2.code
LEFT JOIN mst_kabupaten b3 ON b2.code_kabupaten = b3.code
LEFT JOIN mst_provinsi b4 ON b3.code_provinsi = b4.code
LEFT JOIN mst_kelurahan d1 ON d.code_kelurahan = d1.code
LEFT JOIN mst_kecamatan d2 ON d1.code_kecamatan = d2.code
LEFT JOIN mst_kabupaten d3 ON d2.code_kabupaten = d3.code
LEFT JOIN mst_provinsi d4 ON d3.code_provinsi = d4.code
WHERE
a.id_case = '3' AND a.deleted_at IS NULL
GROUP BY a.id
) b ON a.id = b.i1
LEFT JOIN view_puskesmas i ON a.id_faskes = i.id AND a.id_role = 1
LEFT JOIN view_rumahsakit j ON a.id_faskes = j.id AND a.id_role = 2
JOIN trx_klinis k ON a.id = k.id_trx_case
LEFT JOIN trx_pe n ON a.id = n.id_trx_case AND n.deleted_at IS NULL
LEFT JOIN trx_spesimen o ON a.id = o.id_trx_case
WHERE
a.id_case = '3' AND a.deleted_at IS NULL
GROUP BY a.id
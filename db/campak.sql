SELECT
a.id,
a.id AS id_trx_case,
a.id_role,
a.id_role_old,
a.id_pasien,
a.jenis_input,
a.id_faskes_old,
a.no_rm,
a._no_epid,
a.no_epid,
a.no_epid_klb,
a.no_epid_lama,
a.klasifikasi_final,
g.rkf_name AS  klasifikasi_final_txt,
(CASE
WHEN a.jenis_input = 1 THEN 'Web'
WHEN a.jenis_input = 2 THEN 'Android'
WHEN a.jenis_input = 3 THEN 'Import'
END) jenis_input_text,
a.id_case,
a.id_faskes,
DATE_FORMAT(a.created_at, '%d-%m-%Y') AS tgl_input,
DATE_FORMAT(a.updated_at, '%d-%m-%Y') AS tgl_update,
DATE_FORMAT(a.created_at, '%d-%m-%Y %H:%i:%s') AS created_at,
DATE_FORMAT(a.updated_at, '%d-%m-%Y %H:%i:%s') AS updated_at,
DATE_FORMAT(a.deleted_at, '%d-%m-%Y %H:%i:%s') AS deleted_at,
(CASE
WHEN a.id_role = 1 THEN i.code_faskes
WHEN a.id_role = 2 THEN j.code_faskes
END) code_faskes,
(CASE
WHEN a.id_role = 1 THEN CONCAT('PUSKESMAS ',i.name)
WHEN a.id_role = 2 THEN j.name
END) name_faskes,
(CASE
WHEN a.id_role = 1 THEN i.name_kecamatan
WHEN a.id_role = 2 THEN j.name_kecamatan
END) name_kecamatan_faskes,
(CASE
WHEN a.id_role = 1 THEN i.code_kelurahan
WHEN a.id_role = 2 THEN j.code_kelurahan
END) code_kelurahan_faskes,
(CASE
WHEN a.id_role = 1 THEN i.code_kecamatan
WHEN a.id_role = 2 THEN j.code_kecamatan
END) code_kecamatan_faskes,
(CASE
WHEN a.id_role = 1 THEN i.name_kabupaten
WHEN a.id_role = 2 THEN j.name_kabupaten
END) name_kabupaten_faskes,
(CASE
WHEN a.id_role = 1 THEN i.code_kabupaten
WHEN a.id_role = 2 THEN j.code_kabupaten
END) code_kabupaten_faskes,
(CASE
WHEN a.id_role = 1 THEN i.name_provinsi
WHEN a.id_role = 2 THEN j.name_provinsi
END) name_provinsi_faskes,
(CASE
WHEN a.id_role = 1 THEN i.code_provinsi
WHEN a.id_role = 2 THEN j.code_provinsi
END) code_provinsi_faskes,
b.*,
c.*,
GROUP_CONCAT(DISTINCT CONCAT(IFNULL(d.jenis_pemeriksaan,''),'_',IFNULL(d.jenis_spesimen,''),'_',IFNULL(DATE_FORMAT(d.tgl_ambil_spesimen,'%d-%m-%Y'),''),'_',IFNULL(d.hasil,'')) SEPARATOR '|') AS spesimen,
e.id AS id_pe,
(CASE
WHEN e.id IS NULL THEN 'PE'
ELSE 'Sudah PE'
END) AS 'pe_text',
e.total_kasus_tambahan,
f.code AS code_roles
FROM
trx_case a
JOIN (
SELECT
    b1.id AS i1,
    b1.name AS name_pasien,
    b1.nik,
    b1.agama,
    b1.jenis_kelamin,
    (CASE
WHEN b1.jenis_kelamin = 'L' THEN 'Laki-laki'
WHEN b1.jenis_kelamin = 'P' THEN 'Perempuan'
WHEN b1.jenis_kelamin = 'T' THEN 'Tidak Jelas'
ELSE 'Belum diisi' END) jenis_kelamin_txt,
    DATE_FORMAT(b1.tgl_lahir, '%d-%m-%Y') AS tgl_lahir,
    b1.umur_thn,
    b1.umur_bln,
    b1.umur_hari,
    b1.alamat,
    b1.tempat_lahir,
    b1.longitude,
    b1.latitude,
    b2.name AS religion_text,
    b3.name AS nama_ortu,
    b3.id AS id_family,
    b4.code_kelurahan AS code_kelurahan_pasien,
    b4.name_kelurahan AS kelurahan_pasien,
    b4.name_kelurahan AS name_kelurahan_pasien,
    b4.code_kecamatan AS code_kecamatan_pasien,
    b4.name_kecamatan AS kecamatan_pasien,
    b4.name_kecamatan AS name_kecamatan_pasien,
    b4.code_kabupaten AS code_kabupaten_pasien,
    b4.name_kabupaten AS kabupaten_pasien,
    b4.name_kabupaten AS name_kabupaten_pasien,
    b4.code_provinsi AS code_provinsi_pasien,
    b4.name_provinsi AS provinsi_pasien,
    b4.name_provinsi AS name_provinsi_pasien,
    b4.full_address
FROM
    ref_pasien b1
    LEFT JOIN mst_religion b2 ON b1.agama = b2.id
    JOIN ref_family b3 ON b1.id = b3.id_pasien
    LEFT JOIN view_area b4 ON b1.code_kelurahan = b4.code_kelurahan
) b ON a.id_pasien = b.i1
JOIN (
SELECT
c1.id_trx_case AS i2,
c1.id AS id_trx_klinis,
DATE_FORMAT(c1.tgl_imunisasi_campak, '%d-%m-%Y') AS tgl_imunisasi_campak,
c1.jml_imunisasi_campak,
c1.jml_imunisasi_campak AS jml_imunisasi,
(CASE
WHEN c1.jml_imunisasi_campak = 7 THEN 'Tidak'
WHEN c1.jml_imunisasi_campak = 8 THEN 'Tidak Tahu'
WHEN c1.jml_imunisasi_campak = 9 THEN 'Belum Pernah'
WHEN c1.jml_imunisasi_campak IN ('1','2','3','4','5','6') THEN CONCAT(c1.jml_imunisasi_campak, 'x')
ELSE 'Belum diisi' END) jml_imunisasi_campak_txt,
DATE_FORMAT(c1.tgl_mulai_demam, '%d-%m-%Y') AS tgl_mulai_demam,
DATE_FORMAT(c1.tgl_mulai_rash, '%d-%m-%Y') AS tgl_mulai_rash,
c1.tgl_mulai_rash AS tgl_sakit_date,
DATE_FORMAT(c1.tgl_mulai_rash, '%d-%m-%Y') AS tgl_sakit,
YEAR(c1.tgl_mulai_rash) AS thn_sakit,
DATE_FORMAT(c1.tgl_laporan_diterima, '%d-%m-%Y') AS tgl_laporan_diterima,
DATE_FORMAT(c1.tgl_pelacakan, '%d-%m-%Y') AS tgl_pelacakan,
c1.vitamin_a,
(CASE
WHEN c1.vitamin_a = 1 THEN 'Ya'
WHEN c1.vitamin_a = 2 THEN 'Tidak'
END) AS vitamin_a_txt,
c1.keadaan_akhir,
(CASE
WHEN c1.keadaan_akhir = 1 THEN 'Hidup'
WHEN c1.keadaan_akhir = 2 THEN 'Meninggal'
END) AS keadaan_akhir_txt,
c1.jenis_kasus,
(CASE
WHEN c1.jenis_kasus = 1 THEN 'KLB'
WHEN c1.jenis_kasus = 2 THEN 'Bukan KLB'
END) AS jenis_kasus_txt,
c1.klb_ke,
c1.status_kasus,
(CASE
WHEN c1.status_kasus = 1 THEN 'Index'
WHEN c1.status_kasus = 2 THEN 'Bukan Index'
END) AS status_kasus_txt,
GROUP_CONCAT(DISTINCT CONCAT(IFNULL(c2.id_gejala,''),'_',IFNULL(c21.name,''),'_',IFNULL(DATE_FORMAT(c2.tgl_kejadian, '%d-%m-%Y'),'')) ORDER BY c2.id_gejala ASC SEPARATOR '|') AS gejala,
GROUP_CONCAT(DISTINCT c3.name SEPARATOR '|') komplikasi,
GROUP_CONCAT(DISTINCT c3.other_komplikasi SEPARATOR ',') other_komplikasi
FROM
trx_klinis c1
LEFT JOIN trx_gejala c2 ON c1.id = c2.id_trx_klinis
LEFT JOIN mst_gejala c21 ON c2.id_gejala = c21.id
LEFT JOIN trx_komplikasi c3 ON c1.id = c3.id_trx_klinis
GROUP BY c1.id_trx_case
) c ON a.id = c.i2
LEFT JOIN trx_spesimen d ON a.id = d.id_trx_case
LEFT JOIN trx_pe e ON a.id = e.id_trx_case AND e.deleted_at IS NULL
JOIN roles f ON a.id_role = f.id
LEFT JOIN ref_klasifikasi_final g ON a.id_case = g.id_case AND a.klasifikasi_final = g.klasifikasi_final
LEFT JOIN view_puskesmas i ON a.id_faskes = i.id AND a.id_role = 1
LEFT JOIN view_rumahsakit j ON a.id_faskes = j.id AND a.id_role = 2
WHERE
a.deleted_at IS NULL AND a.id_case = 1
GROUP BY a.id
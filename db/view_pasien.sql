SELECT 
  a.id AS id_trx_case,
  a.id_pasien AS id_pasien,
  b.name AS `name`,
  DATE_FORMAT(b.tgl_lahir, '%d-%m-%Y') AS tgl_lahir,
  c.id AS id_family,
  b.nik AS nik,
  c.name AS nama_ortu,
  b.agama AS agama,
  b.jenis_kelamin AS jenis_kelamin,
  (CASE
  WHEN b.jenis_kelamin = 'L' THEN 'Laki-laki'
  WHEN b.jenis_kelamin = 'P' THEN 'Perempuan'
  WHEN b.jenis_kelamin = 'T' THEN 'Tidak Jelas'
  ELSE NULL
  END) AS jenis_kelamin_txt,
  b.umur_hari AS umur_hari,
  b.umur_bln AS umur_bln,
  b.umur_thn AS umur_thn,
  CONCAT(b.umur_thn,' Th ',b.umur_bln,' Bln ',b.umur_hari,' Hari') AS umur,
  b.alamat AS alamat,
  d.name_kelurahan,
  d.name_kecamatan,
  d.name_kabupaten,
  d.name_provinsi,
  d.full_address AS full_address,
  b.longitude AS longitude,
  b.latitude AS latitude,
  b.code_kelurahan AS code_kelurahan,
  b.code_kecamatan AS code_kecamatan,
  b.code_kabupaten AS code_kabupaten,
  b.code_provinsi AS code_provinsi 
FROM
  trx_case a 
  JOIN ref_pasien b ON a.id_pasien = b.id 
  LEFT JOIN ref_family c ON b.id = c.id_pasien 
  LEFT JOIN view_area d ON b.code_kelurahan = d.code_kelurahan 
WHERE ISNULL(a.deleted_at)
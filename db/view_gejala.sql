SELECT a.id_trx_klinis,
       a.id_trx_ku60,
       a.id_trx_pe,
       a.id_gejala AS id_gejala,
       a.val_gejala AS val_gejala,
       (CASE
           WHEN a.val_gejala = 1 THEN 'Ya'
           WHEN a.val_gejala = 2 THEN 'Tidak'
           WHEN a.val_gejala = 3 THEN 'Tidak Tahu'
           ELSE NULL
       END) AS val_gejala_txt,
       date_format (a.tgl_kejadian, '%d-%m-%Y') AS tgl_kejadian,
       a.other_gejala AS other_gejala,
       c.group_type AS group_type,
       c.name AS name,
       a.val_kelumpuhan AS val_kelumpuhan,
       (CASE
           WHEN a.val_kelumpuhan = 1 THEN 'Ya'
           WHEN a.val_kelumpuhan = 2 THEN 'Tidak'
           WHEN a.val_kelumpuhan = 3 THEN 'Tidak Tahu'
           ELSE NULL
       END) AS val_kelumpuhan_txt,
       a.val_gangguan_raba AS val_gangguan_raba,
       (CASE
           WHEN a.val_gangguan_raba = 1 THEN 'Ya'
           WHEN a.val_gangguan_raba = 2 THEN 'Tidak'
           WHEN a.val_gangguan_raba = 3 THEN 'Tidak Tahu'
           ELSE NULL
       END) AS val_gangguan_raba_txt,
       a.val_paralisis_residual AS val_paralisis_residual,
       (CASE
           WHEN a.val_paralisis_residual = 1 THEN 'Ya'
           WHEN a.val_paralisis_residual = 2 THEN 'Tidak'
           WHEN a.val_paralisis_residual = 3 THEN 'Tidak Tahu'
           ELSE NULL
       END) AS val_paralisis_residual_txt
FROM trx_gejala a
LEFT JOIN mst_gejala c ON a.id_gejala = c.id
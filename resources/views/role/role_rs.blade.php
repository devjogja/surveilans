@extends('layouts.base')
@section('content')

<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Profil Rumah sakit</h3>
		</div>
		{!! Form::open(['method' => 'POST', 'url' => 'role/post', 'id'=>'form' , 'class' => 'form-horizontal']) !!}
		{!! Form::hidden('xyz', null,['id'=>'xyz']) !!}
		<div class="box-body">
			<div class="form-group">
				{!! Form::label(null, 'Provinsi', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dt[code_provinsi]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control','id'=>'id_provinsi_user','onchange'=>"getKabupaten('_user')"]) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kabupaten', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dt[code_kabupaten]', array(null=>'Pilih Kabupaten'), null, ['class' => 'form-control', 'id'=>'id_kabupaten_user','onchange'=>"getRs('_user')"]) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('rs', 'Rumah sakit', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('role_user[faskes_id]', array(null=>'Pilih Rumah sakit'), null, ['class' => 'form-control', 'id'=>'rs_user']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('kode_rs', 'Kode Rumah sakit', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('kode_rs', null, ['class' => 'form-control','id'=>'kode_rs','placeholder'=>'Kode Rumah sakit','disabled'=>'disabled']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('kode_konfirm', 'Kode Konfirmasi', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('kode_konfirm', null, ['class' => 'form-control','id'=>'kode_konfirm','placeholder'=>'Kode Konfirmasi']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('alamat', 'Alamat', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dt[alamat]', null, ['class' => 'form-control','id'=>'alamat','placeholder'=>'Alamat']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('penanggung_jawab_1', 'Penanggung jawab 1', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('role_user[penanggung_jawab_1]', null, ['class' => 'form-control','id'=>'penanggung_jawab_1','placeholder'=>'Penanggung jawab 1']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('penanggung_jawab_2', 'Penanggung jawab 2', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('role_user[penanggung_jawab_2]', null, ['class' => 'form-control','id'=>'penanggung_jawab_2','placeholder'=>'Penanggung jawab 2']) !!}
				</div>
			</div>
		</div>
		<input type="hidden" name="role_user[role_id]" id="role_id" value="{!! $data['role_id'] !!}">
		<div class="box-footer">
			<div class="row">
				<div class="col-md-offset-4 col-md-8">
					{!! Form::reset("Kembali", ['class' => 'btn btn-default back']) !!}
					{!! Form::reset("Batal", ['class' => 'btn btn-warning']) !!}
					{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
				</div>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('.back').on('click',function(){
			window.location.href = BASE_URL+'role/level';
			return false;
		});

		$("#form").validate({
			rules:{
				'role_user[faskes_id]' : "required",
				'role_user[penanggung_jawab_1]' : "required",
				'role_user[penanggung_jawab_2]' : "required",
				'kode_konfirm' : {
					required : true,
					equalTo  : '#xyz'
				},
			},
			messages:{
				'role_user[faskes_id]' : "Rumah sakit Wajib di isi",
				'role_user[penanggung_jawab_1]' : "Penanggung jawab 1 Wajib di isi",
				'role_user[penanggung_jawab_2]' : "Penanggung jawab 2 Wajib di isi",
				'kode_konfirm' : {
					required 	: "Kode konfimasi harus di isi",
					equalTo     : 'Kode Konfirmasi salah'
				},
			},
			submitHandler: function(){
				var action  = $('#form').attr('action');
				var data = $('#form').serializeJSON();
				$.ajax({
					method  : "POST",
					url     : action,
					data    : JSON.stringify(data),
					dataType: "json",
					beforeSend: function(){
						startProcess();    
					},
				})
				.done(function(response){
					if(response.response==false){
						var url = 'role/level';
					}else{
						var url = 'viewProfile';
					}
					setTimeout( function(){
						endProcess();
						window.location.href = BASE_URL+url;
					}, 200);
				});
				return false;
			}
		});

		$('#rs_user').on('change',function(){
			var id_rs = $('#rs_user').val();
			if(id_rs){
				var url = "{!! url('/rs/detail') !!}"+'/'+id_rs;
				$.get(url, function(data, status){
					if(data.success==1){
						var dt = data.response;
						if(dt){
							$('#faskes_id').val(dt.id);
							$('#kode_rs').val(dt.code_faskes);
							$('#alamat').val(dt.alamat);
							$('#xyz').val(dt.konfirm_code);
						}else{
							$('#kode_rs').val(null);
							$('#kode_konfirm').val(null);
							$('#alamat').val(null);
							$('#penanggung_jawab_1').val(null);
							$('#penanggung_jawab_2').val(null);
							$('#xyz').val(null);
						}
					};
					return false;
				});
			}
		});
	})
</script>
@endsection	
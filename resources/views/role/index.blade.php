@extends('layouts.base')
@section('content')
<div class="col-sm-4">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Form tambah rule</h3>
		</div>
		{!! Form::open(['method' => 'POST', 'url' => 'role/postRole', 'id'=>'form' , 'class' => 'form-horizontal']) !!}
		<div class="box-body">
			<div class="form-group">
				{!! Form::label('level', 'Level', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('id_role', $data['role'], null, ['class' => 'form-control', 'id'=>'id_role']) !!}
				</div>
			</div>
		</div>
		<div class="box-footer">
			<div class="row">
				<div class="col-md-offset-4 col-md-8">
					{!! Form::reset("Kembali", ['class' => 'btn btn-default back']) !!}
					{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
				</div>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection
@extends('layouts.base')
@section('content')

<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Profil Puskesmas</h3>
		</div>
		{!! Form::open(['method' => 'POST', 'url' => 'role/post', 'id'=>'form' , 'class' => 'form-horizontal']) !!}
		{!! Form::hidden('xyz', null,['id'=>'xyz']) !!}
		<div class="box-body">
			<div class="form-group">
				{!! Form::label(null, 'Provinsi', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dt[code_provinsi]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control','id'=>'id_provinsi_user','onchange'=>"getKabupaten('_user')"]) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kabupaten', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dt[code_kabupaten]', array(null=>'Pilih Kabupaten'), null, ['class' => 'form-control', 'id'=>'id_kabupaten_user','onchange'=>"getKecamatan('_user')"]) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kecamatan', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dt[code_kecamatan]', array(null=>'Pilih Kecamatan'), null, ['class' => 'form-control','id'=>'id_kecamatan_user','onchange'=>"getPuskesmas('_user')"]) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('puskesmas', 'Puskesmas', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('role_user[faskes_id]', array(null=>'Pilih Puskesmas'), null, ['class' => 'form-control', 'id'=>'puskesmas_user']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('kode_puskesmas', 'Kode Puskesmas', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dt[code_faskes]', null, ['class' => 'form-control','id'=>'kode_puskesmas','placeholder'=>'Kode Puskesmas','readonly'=>'readonly']) !!}
				</div>
			</div>
			@if(empty($data['id_faskes_user']))
			<div class="form-group">
				{!! Form::label('kode_konfirm', 'Kode Konfirmasi', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('kode_konfirm', null, ['class' => 'form-control','id'=>'kode_konfirm','placeholder'=>'Kode Konfirmasi']) !!}
				</div>
			</div>
			@endif
			<div class="form-group">
				{!! Form::label('alamat', 'Alamat', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dt[alamat]', null, ['class' => 'form-control','id'=>'alamat','placeholder'=>'Alamat']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('penanggung_jawab_1', 'Penanggung jawab 1', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('role_user[penanggung_jawab_1]', null, ['class' => 'form-control','id'=>'penanggung_jawab_1','placeholder'=>'Penanggung jawab 1']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('penanggung_jawab_2', 'Penanggung jawab 2', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('role_user[penanggung_jawab_2]', null, ['class' => 'form-control','id'=>'penanggung_jawab_2','placeholder'=>'Penanggung jawab 2']) !!}
				</div>
			</div>
		</div>
		<input type="hidden" name="id_faskes_user" id="id_faskes_user" value="">
		<input type="hidden" name="role_user[role_id]" id="role_id" value="{!! $data['role_id'] !!}">
		<div class="box-footer">
			<div class="row">
				<div class="row">
					<div class="col-md-offset-4 col-md-8">
						{!! Form::reset("Kembali", ['class' => 'btn btn-default back']) !!}
						{!! Form::reset("Batal", ['class' => 'btn btn-warning']) !!}
						{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
					</div>
				</div>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
<script type="text/javascript">
	function generateData() {
		var action = BASE_URL+'user/getFaskesUser';
		$.getJSON(action, function(result){
			var dt = result.response;
			if (isset(dt)) {
				$('#id_faskes_user').val(dt.id_role_user);
				$('#id_provinsi_user').val(dt.code_provinsi_faskes).select2();
				$('#id_kabupaten_user').empty();
				$('#id_kabupaten_user').append($("<option></option>").attr("value",dt.code_kabupaten_faskes).text(dt.name_kabupaten_faskes)).select2();
				$('#id_kecamatan_user').empty();
				$('#id_kecamatan_user').append($("<option></option>").attr("value",dt.code_kecamatan_faskes).text(dt.name_kecamatan_faskes)).select2();
				$('#puskesmas_user').empty();
				$('#puskesmas_user').append($("<option></option>").attr("value",dt.id_faskes).text(dt.name_faskes)).select2();
				$('#kode_puskesmas').val(dt.code_faskes);
				$('#alamat').val(dt.alamat_faskes);
				$('#penanggung_jawab_1').val(dt.penanggung_jawab_1);
				$('#penanggung_jawab_2').val(dt.penanggung_jawab_2);
			}
		});
	}

	$(function(){
		generateData();
		$('.back').on('click',function(){
			window.location.href = BASE_URL+'role/level';
			return false;
		});

		$("#form").validate({
			rules:{
				'role_user[faskes_id]' : "required",
				'role_user[penanggung_jawab_1]' : "required",
				'role_user[penanggung_jawab_2]' : "required",
				'kode_konfirm' : {
					required : true,
					equalTo  : '#xyz'
				},
			},
			messages:{
				'role_user[faskes_id]' : "Puskesmas Wajib di isi",
				'role_user[penanggung_jawab_1]' : "Penanggung jawab 1 Wajib di isi",
				'role_user[penanggung_jawab_2]' : "Penanggung jawab 2 Wajib di isi",
				'kode_konfirm' : {
					required 	: "Kode konfimasi harus di isi",
					equalTo     : 'Kode Konfirmasi salah'
				},
			},
			submitHandler: function(){
				var action  = $('#form').attr('action');
				var data = $('#form').serializeJSON();
				$.ajax({
					method  : "POST",
					url     : action,
					data    : JSON.stringify(data),
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					if(response.response==false){
						var url = 'role/level';
					}else{
						var url = 'viewProfile';
					}
					setTimeout( function(){
						endProcess();
						window.location.href = BASE_URL+url;
					}, 200);
				});
				return false;
			}
		});

		$('#puskesmas_user').on('change',function(){
			var id_puskesmas = $('#puskesmas_user').select2('val');
			if(id_puskesmas){
				var url = "{!! url('/puskesmas/detail') !!}"+'/'+id_puskesmas;
				$.get(url, function(data, status){
					if(data.success==1){
						var dt = data.response;
						if(dt){
							$('#faskes_id').val(dt.id);
							$('#kode_puskesmas').val(dt.code_faskes);
							$('#alamat').val(dt.alamat);
							$('#xyz').val(dt.konfirm_code);
						}else{
							$('#kode_puskesmas').val(null);
							$('#kode_konfirm').val(null);
							$('#alamat').val(null);
							$('#penanggung_jawab_1').val(null);
							$('#penanggung_jawab_2').val(null);
							$('#xyz').val(null);
						}
					};
					return false;
				});
			}
		});
	})
</script>
@endsection

@extends('layouts.base')
@section('content')
<div class="col-md-4">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Form Desa</h3>
		</div>
		{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form' , 'class' => 'form']) !!}
		{!! Form::hidden('id', null, ['id'=>'id']) !!}
		<div class="box-body">
			<div class="form-group">
				{!! Form::label(null, 'Provinsi', ['class' => 'control-label']) !!}
				{!! Form::select('', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control','id'=>'id_provinsi_master','onchange'=>"getKabupaten('_master')"]) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kabupaten', ['class' => 'control-label']) !!}
				{!! Form::select('', array(null=>'Pilih Kabupaten/Kota'), null, ['class' => 'form-control', 'id'=>'id_kabupaten_master','onchange'=>"getKecamatan('_master')"]) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kecamatan', ['class' => 'control-label']) !!}
				{!! Form::select('dt[code_kecamatan]', array(null=>'Pilih Kecamatan'), null, ['class' => 'form-control', 'id'=>'id_kecamatan_master']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Code Desa/Kelurahan', ['class' => 'control-label']) !!}
				{!! Form::text('dt[code]', null, ['class' => 'form-control','id'=>'code','placeholder'=>'Code Desa/Kelurahan']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Nama Desa/Kelurahan', ['class' => 'control-label']) !!}
				{!! Form::text('dt[name]', null, ['class' => 'form-control','id'=>'name','placeholder'=>'Nama Kecamatan/Kelurahan']) !!}
			</div>
		</div>
		<div class="box-footer">
			{!! Form::reset("Batal", ['class' => 'btn btn-default reset']) !!}
			{!! Form::submit("Simpan", ['class' => 'btn btn-success pull-right']) !!}
		</div>
		{!! Form::close() !!}
	</div>
</div>
<div class="col-md-8">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">List Desa/Kelurahan</h3>
		</div>
		<div class="box-body">
			<table id="table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Code Desa/Kelurahan</th>
						<th>Code</th>
						<th>Nama Desa/Kelurahan</th>
						<th width="15%">Action</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('#table').DataTable({
			"paging": true,
			"lengthChange": true,
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				url     : '{!! URL::to('district/desa/getData') !!}',
				type    : "POST",
			},
			"columns"   : [
			{ "data" : "code_kecamatan" },
			{ "data" : "code" },
			{ "data" : "name" },
			{ "data" : "action" },
			]
		});
		$(".dataTables_filter").addClass('pull-right');
		$('.reset').on('click',function(){
			resetform();
			return false;
		});

		$('#table').on( 'draw.dt', function () {
			$(".edit").unbind();
			$(".cancel").unbind();
			$(".delete").unbind();
			$(".delete-yes").unbind();

			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();    
					},
					success: function(response){
						setTimeout( function(){
							messageAlert(response.messageType, response.title, response.message);
							endProcess();
						}, 0);
						$('#table').DataTable().draw();
					}
				});
				return false;
			});

			$('.edit').on('click',function(){
				var id = $(this).attr('dtId');
				var url= BASE_URL+'district/desa/getDetail/'+id;
				$.getJSON(url,function(d, status){
					if(isset(d)){
						$('#id').val(d.code);
						$('#id_provinsi_master').val(d.code_provinsi).select2();
						$('#id_kabupaten_master').empty().append('<option value="'+d.code_kabupaten+'">'+d.name_kabupaten+'</option>').select2();
						$('#id_kecamatan_master').empty().append('<option value="'+d.code_kecamatan+'">'+d.name_kecamatan+'</option>').select2();
						$('#code').val(d.code);
						$('#name').val(d.name);
					}
				});
				return false;
			});

			$('#form').validate({
				rules:{
					'dt[code_kecamatan]': 'required',
					'dt[code]': 'required',
					'dt[name]' : 'required'
				},
				messages:{
					'dt[code_kecamatan]': 'Code kecamatan wajib di isi',
					'dt[code]': 'Code wajib di isi',
					'dt[name]': 'Name wajib di isi',
				},
				submitHandler: function(){
					var action  = BASE_URL+'district/desa/post';
					var data = $('#form').serialize();
					$.ajax({
						method  : "POST",
						url     : action,
						data    : data,
						dataType: "json",
						beforeSend: function(){
							startProcess();    
						},
						success: function(response){
							if(response.success=='1'){
								setTimeout( function(){
									messageAlert(response.messageType, response.title, response.message);
									endProcess();
								}, 100);
							}
							resetform();
							$('#table').DataTable().draw();
						}
					});
					return false;
				}
			});
		});
	});

	function resetform() {
		$('#id').val(null);
		$('select').val(null).select2();
		$('form').trigger('reset');
		return false;
	}
</script>
@endsection
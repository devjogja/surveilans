@extends('layouts.base')
@section('content')
<div class="col-md-4">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Form Provinsi</h3>
		</div>
		{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form' , 'class' => 'form']) !!}
		{!! Form::hidden('id', null, ['id'=>'id']) !!}
		<div class="box-body">
			<div class="form-group">
				{!! Form::label(null, 'Code Provinsi', ['class' => 'control-label']) !!}
				{!! Form::text('dt[code]', null, ['class' => 'form-control','id'=>'code','placeholder'=>'Code Provinsi']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Nama Provinsi', ['class' => 'control-label']) !!}
				{!! Form::text('dt[name]', null, ['class' => 'form-control','id'=>'name','placeholder'=>'Nama Provinsi']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Longitude', ['class' => 'control-label']) !!}
				{!! Form::text('dt[longitude]', null, ['class' => 'form-control','id'=>'longitude','placeholder'=>'Longitude']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Latitude', ['class' => 'control-label']) !!}
				{!! Form::text('dt[latitude]', null, ['class' => 'form-control','id'=>'latitude','placeholder'=>'Latitude']) !!}
			</div>
		</div>
		<div class="box-footer">
			{!! Form::reset("Batal", ['class' => 'btn btn-default reset']) !!}
			{!! Form::submit("Simpan", ['class' => 'btn btn-success pull-right']) !!}
		</div>
		{!! Form::close() !!}
	</div>
</div>
<div class="col-md-8">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">List Provinsi</h3>
		</div>
		<div class="box-body">
			<table id="table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Code</th>
						<th>Nama Provinsi</th>
						<th>longitude</th>
						<th>Latitude</th>
						@if(Helper::role()->id_role=='pusat')
						<th>Konfirm Code</th>
						@endif
						<th width="15%">Action</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('#table').DataTable({
			"paging": true,
			"lengthChange": true,
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				url     : '{!! URL::to('district/provinsi/getData') !!}',
				type    : "POST",
			},
			"columns"   : [
			{ "data" : "code" },
			{ "data" : "name" },
			{ "data" : "longitude" },
			{ "data" : "latitude" },
			@if(Helper::role()->id_role=='pusat')
			{ "data" : "konfirm_code" },
			@endif
			{ "data" : "action" },
			]
		});
		$(".dataTables_filter").addClass('pull-right');
		$('.reset').on('click',function(){
			resetform();
			return false;
		});

		$('#table').on( 'draw.dt', function () {
			$(".edit").unbind();
			$(".cancel").unbind();
			$(".delete").unbind();
			$(".delete-yes").unbind();

			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();    
					},
					success: function(response){
						setTimeout( function(){
							messageAlert(response.messageType, response.title, response.message);
							endProcess();
						}, 0);
						$('#table').DataTable().draw();
					}
				});
				return false;
			});

			$('.edit').on('click',function(){
				var id = $(this).attr('dtId');
				var url= BASE_URL+'district/provinsi/getDetail/'+id;
				$.getJSON(url,function(d, status){
					$('#id').val(d.code);
					$('#code').val(d.code);
					$('#name').val(d.name);
					$('#longitude').val(d.longitude);
					$('#latitude').val(d.latitude);
					$('#konfirm_code').val(d.konfirm_code);
				});
				return false;
			});

			$('#form').validate({
				rules:{
					'dt[code]': 'required',
					'dt[name]' : 'required'
				},
				messages:{
					'dt[code]': 'Code wajib di isi',
					'dt[name]': 'Name wajib di isi',
				},
				submitHandler: function(){
					var action  = BASE_URL+'district/provinsi/post';
					var data = $('#form').serialize();
					$.ajax({
						method  : "POST",
						url     : action,
						data    : data,
						dataType: "json",
						beforeSend: function(){
							startProcess();    
						},
						success: function(response){
							if(response.success=='1'){
								setTimeout( function(){
									messageAlert(response.messageType, response.title, response.message);
									endProcess();
								}, 100);
							}
							resetform();
							$('#table').DataTable().draw();
						}
					});
					return false;
				}
			});
		});
	});

	function resetform() {
		$('#id').val(null);
		$('form').trigger('reset');
		return false;
	}
</script>
@endsection
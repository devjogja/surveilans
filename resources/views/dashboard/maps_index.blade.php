<div class="box-header with-border">
	<h3 class="box-title">
		Peta
	</h3>
	<div class="pull-right col-md-2">
		<button class="btn btn-primary pull-right" data-target="#filterModal" data-toggle="modal" type="button">
			Pengaturan Peta
			<i class="fa fa-cogs">
			</i>
		</button>
	</div>
</div>
<div class="box-body">
	<div id="container" style="margin: 0 auto;height:400px">
	</div>
</div>
<div class="modal fade" id="filterModal" role="dialog">
	@include('dashboard.filter_map_index')
</div>
<script type="text/javascript">
	$(function () {
		getMapsIndex();
	});
	function getMapsIndex() {
		var data = Highcharts.geojson(Highcharts.maps['indonesia']);
		// Set drilldown pointers
		$.each(data, function (i) {
			this.drilldown = this.properties['SPROVINCE'];
			this.id = this.properties['SPROVINCE'];
		});

// Instanciate the map
Highcharts.mapChart('container', {
	chart: {
		events: {
			drilldown: function (e) {

				if (!e.seriesOptions) {
					var chart = this,
					mapKey = 'countries/us/' + e.point.drilldown + '-all',
                        // Handle error, the timeout is cleared on success
                        fail = setTimeout(function () {
                        	if (!Highcharts.maps[mapKey]) {
                        		chart.showLoading('<i class="icon-frown"></i> Failed loading ' + e.point.name);

                        		fail = setTimeout(function () {
                        			chart.hideLoading();
                        		}, 1000);
                        	}
                        }, 3000);

                    // Show the spinner
                    chart.showLoading('<i class="icon-spinner icon-spin icon-3x"></i>'); // Font Awesome spinner

                    // Load the drilldown map
                    $.getScript('https://code.highcharts.com/mapdata/' + mapKey + '.js', function () {
                    	console.log(mapKey);

                    	data = Highcharts.geojson(Highcharts.maps[mapKey]);

                        // Set a non-random bogus value
                        $.each(data, function (i) {
                        	this.value = i;
                        });

                        // Hide loading and add series
                        chart.hideLoading();
                        clearTimeout(fail);
                        chart.addSeriesAsDrilldown(e.point, {
                        	name: e.point.name,
                        	data: data,
                        	dataLabels: {
                        		enabled: true,
                        		format: '{point.name}'
                        	}
                        });
                      });
                  }


                  this.setTitle(null, { text: e.point.name });
                },
                drillup: function () {
                	this.setTitle(null, { text: 'USA' });
                }
              }
            },

            title: {
            	text: 'Highcharts Map Drilldown'
            },

            subtitle: {
            	text: 'USA',
            	floating: true,
            	align: 'right',
            	y: 50,
            	style: {
            		fontSize: '16px'
            	}
            },
            colorAxis: {
            	min: 0,
            	minColor: '#E6E7E8',
            	maxColor: '#005645'
            },

            mapNavigation: {
            	enabled: true,
            	buttonOptions: {
            		verticalAlign: 'bottom'
            	}
            },

            plotOptions: {
            	map: {
            		states: {
            			hover: {
            				color: '#EEDD66'
            			}
            		}
            	}
            },

            series: [{
            	data: data,
            	name: 'USA',
            	dataLabels: {
            		enabled: true,
            		format: '{point.properties.postal-code}'
            	}
            }],

            drilldown: {
            	activeDataLabelStyle: {
            		color: '#FFFFFF',
            		textDecoration: 'none',
            		textOutline: '1px #000000'
            	},
            	drillUpButton: {
            		relativeTo: 'spacingBox',
            		position: {
            			x: 0,
            			y: 60
            		}
            	}
            }
          });
}
</script>

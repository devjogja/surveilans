<div class="box-header with-border">
	<h3 class="box-title">Grafik Penderita Berdasar Waktu</h3>
	<div class="pull-right col-md-3">
		<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#filterGraph1">Sortir Data &nbsp;<i class="fa fa-cogs"></i></button>
	</div>
</div>
<div class="box-body">
	<div id="graph_bulanan" style="margin: 0 auto"></div>
	<div class="text-center">Total Penderita : <span style="font-weight: bold" id="totalBulan"></span></div>
</div>

<script type="text/javascript">
	function graph1(){
		var case_type = $('#case_type1 option:selected');
		var klasfinal = $('#type1 option:selected');
		var title1 = 'Suspek Campak';
		var kasus = 'campak';
		if(case_type.val() != ''){
			title1 = klasfinal.text();
			kasus = case_type.val();
		}
		var title2 = '';
		if(case_type.val() == 'campak'){
			title2 = $('#jenis_kasus1 option:selected').text();
		}
		var gTitle = titleFilter1();
		var range = gTitle.wil+'<br>'+gTitle.time;
		var fd = {};
		fd['fdata'] = JSON.stringify({"jenis_data":$('#type1').val(),"jenis_kasus":$('#jenis_kasus1').val()});
		fd['dt'] = JSON.stringify($('#filter1').serializeJSON());
		$.ajax({
			method  : "POST",
			url     : BASE_URL+'api/analisa/graphWaktu/'+kasus,
			data    : fd,
			success: function(data){
				if (data.success==true) {
					var dt=data.response;
					graphMonth(title1+' '+title2,range,dt.waktu);
					$('#filterGraph1').modal('hide');
				}else{
					messageAlert('warning', 'Peringatan', 'Data gagal salah');
					$('#filterGraph1').modal('hide');
				}
			}
		});
	}

	function graphMonth(title,range,data) {
		$('#graph_bulanan').highcharts({
			chart: {
				events: {
					load: function(event) {
						var total = 0;
						$.each(this.series, function(p, i){
							$.each(i.yData, function(x, y){
								total += y;
							})
						});
						jmlTotalBulan = total;
					}
				}
			},
			title: {
				text: 'Grafik Penderita '+title+' Berdasar Waktu',
			},
			subtitle: {
				text: range,
			},
			xAxis: {
				type : 'category',
			},
			yAxis: {
				min: 0,
				tickInterval: 1,
				title: {
					text: 'Jumlah Penderita (orang)'
				},
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}]
			},
			tooltip: {
				formatter: function() {
					if (this.point.drilldown) {
						return  'Jumlah kasus bulan '+this.key+' '+this.series.name+': <b>'+this.point.y+' orang</b>';
					}else{
						return  'Jumlah kasus tanggal '+this.key+' '+this.series.name+': <b>'+this.point.y+' orang</b>';
					}
				},
				shared:false
			},
			plotOptions: {
				series: {
					borderWidth: 0,
					dataLabels: {
						enabled: true,
						format: '{point.y} orang',
					}
				}
			},
			lang: {
				drillUpText: '<< Kembali ke- {series.name}'
			},
			legend: {
				enabled:true,
				layout: 'horizontal',
				align: 'left',
				verticalAlign: 'bottom',
				labelFormatter: function() {
					var total=0, string;
					Highcharts.each(this.userOptions.data, function(p, i) {
						if (p.y) {
							tmp = parseInt(p.y | 0);
							total += p.y;
						}else if (p.y==0) {
							tmp = 0;
							total += tmp;
						}else {
							total += p[1];
						}
					});
					string = this.name+'<br>Jumlah Penderita: ' + total+' orang';
					return string;
				}
			},
			credits:{
				enabled:false
			},
			series:data.data,
			drilldown: {
				drillUpButton: {
					relativeTo: 'spacingBox',
					position: {
						y: 0,
						x: 0
					},
					theme: {
						fill: 'white',
						'stroke-width': 1,
						stroke: 'silver',
						r: 0,
						states: {
							hover: {
								fill: '#a4edba'
							},
							select: {
								stroke: '#039',
								fill: '#a4edba'
							}
						}
					}
				},
				series: data.drilldown
			}
		});
		document.getElementById("totalBulan").innerHTML=jmlTotalBulan+" orang";
	}

	$(function(){
		graph1();
		$('#filter1').validate({
			rules:{
				'case_type':'required',
				'filter_type':'required',
				'jenis_data':'required',
			},
			messages:{
				'case_type':'Kasus wajib diisi',
				'filter_type':'Wilayah wajib diisi',
				'jenis_data':'Jenis Data wajib diisi',
			},
			submitHandler: function(){
				graph1();
			}
		});
	});
</script>

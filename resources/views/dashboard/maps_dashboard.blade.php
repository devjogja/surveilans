<div class="box-header with-border">
	<h3 class="box-title">Peta</h3>
	<div class="pull-right col-md-2">
		<button type="button" class="btn btn-default" data-toggle="modal" data-target="#filterModalMaps">Pengaturan Peta &nbsp<i class="fa fa-cogs"></i></button>
	</div>
</div>
<div class="box-body">
	<div id="graph_maps" style="margin: 0 auto;height:400px"></div>
</div>
<div class="modal fade" id="filterModalMaps" role="dialog">
	@include('dashboard.filter_dashboard')
</div>

<script type="text/javascript">
	$(function () {
		getMapsPlot();
	});
</script>

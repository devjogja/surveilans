<div class="box-header with-border">
	<h3 class="box-title">Grafik Penderita Berdasarkan Kelompok Umur</h3>
	<div class="pull-right col-md-2">
		<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#filterGraph3">Sortir Data &nbsp;<i class="fa fa-cogs"></i></button>
	</div>
</div>
<div class="box-body">
	<div id="graph_umur" class="chart" style="margin: 0 auto"></div>
</div>

<script type="text/javascript">
	function graph3(){
		var case_type = $('#case_type3 option:selected');
		var klasfinal = $('#type3 option:selected');
		var title1 = 'Suspek Campak';
		var kasus = 'campak';
		if(case_type.val() != ''){
			title1 = klasfinal.text();
			kasus = case_type.val();
		}
		var category = [];
		if(kasus == "crs"){
			var category = [@foreach (Helper::getCategoryUmur('crs') as $item)
			'{{ $item }}',
			@endforeach ];
		}else if(kasus == 'tetanus'){
			var category = [@foreach (Helper::getCategoryUmur('tn') as $item)
			'{{ $item }}',
			@endforeach ];
		}else{
			var category = [@foreach (Helper::getCategoryUmur() as $item)
			'{{ $item }}',
			@endforeach ];
		}
		var title2 = '';
		if(case_type.val() == 'campak'){
			title2 = $('#jenis_kasus3 option:selected').text();
		}
		var gTitle = titleFilter3();
		var range = gTitle.wil+'<br>'+gTitle.time;
		var fd = {};
		fd['fdata'] = JSON.stringify({"jenis_data":$('#type3').val(),"jenis_kasus":$('#jenis_kasus3').val()});
		fd['dt'] = JSON.stringify($('#filter3').serializeJSON());
		$.ajax({
			method  : "POST",
			url     : BASE_URL+'api/analisa/graphUmur/'+kasus,
			data    : fd,
			success: function(data){
				if (data.success==true) {
					var dt=data.response;
					graphUmur(title1+' '+title2,range,category,dt.umur);
					$('#filterGraph3').modal('hide');
				}else{
					messageAlert('warning', 'Peringatan', 'Data gagal salah');
					$('#filterGraph3').modal('hide');
				}
			}
		});
	}

	function graphUmur(title,range,category,data){
		$('#graph_umur').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: 'Grafik Penderita '+title+' Berdasar Umur'
			},
			xAxis: {
				categories :category
			},
			yAxis: {
				min: 0,
				tickInterval: 1,
				title: {
					text: 'Jumlah Penderita (orang)'
				}
			},
			subtitle: {
				text: range
			},
			legend: {
				enabled: true,
				align: 'center',
				verticalAlign: 'bottom',
				labelFormatter: function() {
					var total=0, string;
					Highcharts.each(this.userOptions.data, function(p, i) {
						total += p;
					});
					string = this.name+'<br>Jumlah Penderita: ' + total+' orang';
					return string;
				}
			},
			tooltip: {
				pointFormat: 'Jumlah Kasus '+title+': <b>{point.y} orang</b>'
			},
			plotOptions: {
				series: {
					borderWidth: 0,
					dataLabels: {
						enabled: true,
						format: '{point.y} orang',
					}
				}
			},
			credits:{
				enabled:false
			},
			series: data
		});
	}

	$(function(){
		graph3();
		$('#filter3').validate({
			rules:{
				'case_type':'required',
				'filter_type':'required',
				'jenis_data':'required',
			},
			messages:{
				'case_type':'Kasus wajib diisi',
				'filter_type':'Wilayah wajib diisi',
				'jenis_data':'Jenis Data wajib diisi',
			},
			submitHandler: function(){
				graph3();
			}
		});
	});
</script>
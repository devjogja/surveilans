<div class="box-body">
	<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#filterGraph">Sortir Data &nbsp<i class="fa fa-cogs"></i></button>
	<div id="graph_klasifikasi_final" style="margin: 0 auto"></div>
	<center><span style="font-size:larger;font-weight:bold">Total Penderita :</span> <span style="font-size:larger" id="total"></span></center>
</div>
<div class="modal fade" id="filterGraph" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Pengaturan Tampilan Data</h4>
				</div>
				<div class="modal-body">
					<div class="box box-success">
						{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>"filter" , 'class' => 'form-horizontal']) !!}
						<div class="box-body">
							<div class="row">
								<div class="col-sm-12">
									<div class="box-body">
										<div class="form-group">
											<label for="cases" class="col-sm-3 control-label">Jenis Kasus</label>
											<div class="col-sm-5">
												@if(Helper::role()->id_role=='4')
												{!! Form::select('case_type', array(null=>'--Pilih--','afp'=>'AFP','campak'=>'Campak','crs'=>'CRS'), null, ['class' => 'form-control', 'id'=>"case_type",'required']) !!}
												@else
												{!! Form::select('case_type', array(null=>'--Pilih--','afp'=>'AFP','campak'=>'Campak','crs'=>'CRS','difteri'=>'Difteri','tetanus'=>'Tetanus Neonatorum'), null, ['class' => 'form-control', 'id'=>"case_type",'required']) !!}
												@endif
											</div>
											<div class="col-sm-4" id="form_campak">
												{!! Form::select('data_type_campak', array(null=>'Rutin dan KLB','2'=>'Rutin','1'=>'KLB'), null, ['class' => 'form-control pilih', 'id'=>"data_type",'disabled']) !!}
											</div>
										</div>
										<div class="form-group" id="jenis_data">
											<label for="input1" class="col-sm-3 control-label">Jenis Data</label>
											<div class="col-sm-5">
												{!! Form::select('jenis_kasus', array(null=>'--Pilih--',), null, ['class' => 'form-control pilih', 'id'=>"type",'required']) !!}
											</div>
										</div>
										<div class="form-group">
											<label for="input1" class="col-sm-3 control-label">Rentang Waktu</label>
											<div class="col-sm-5">
												{!! Form::select('range', array(null=>'All','1'=>'Hari','2'=>'Bulan','3'=>'Tahun'), null, ['class' => 'form-control pilih', 'id'=>"range_time"]) !!}
											</div>
										</div>
										<div class="form-group">
											<label for="input2" class="col-sm-3 control-label">Sejak</label>
											<div class="col-sm-2" style="padding-right: 0px;">
												<select name="from[day]" id="from_day" class="form-control pilih" disabled="disabled">
													<option value="">--Pilih--</option>
													@for($i=1;$i<=31;$i++)
													<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
													@endfor
												</select>
											</div>
											<div class="col-sm-3" style="padding-right: 0px;">
												{!! Form::select('from[month]',Helper::getMonth(), null, ['class'=>'form-control pilih','id'=>"from_month",'disabled']) !!}
											</div>
											<div class="col-sm-3" style="padding-right: 0px;">
												<select name="from[year]" id="from_year" class="form-control pilih" disabled="disabled">
													<option value="">--Pilih--</option>
													@for($i=2010;$i<=2020;$i++)
													<option value="{!! $i !!}">{!! $i !!}</option>
													@endfor
												</select>
											</div>
										</div>
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-3 control-label">Sampai</label>
											<div class="col-sm-2" style="padding-right: 0px;">
												<select name="to[day]" id="to_day" class="form-control pilih" disabled="disabled">
													<option value="">--Pilih--</option>
													@for($i=1;$i<=31;$i++)
													<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
													@endfor
												</select>
											</div>
											<div class="col-sm-3" style="padding-right: 0px;">
												{!! Form::select('to[month]',Helper::getMonth(), null, ['class'=>'form-control pilih','id'=>"to_month",'disabled']) !!}
											</div>
											<div class="col-sm-3" style="padding-right: 0px;">
												<select name="to[year]" id="to_year" class="form-control pilih" disabled="disabled">
													<option value="">--Pilih--</option>
													@for($i=2010;$i<=2020;$i++)
													<option value="{!! $i !!}">{!! $i !!}</option>
													@endfor
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12"  id="filter_wilayah ">
									<div class="box-body">
										<div class="form-group">
											<label for="input1" class="col-sm-3 control-label">Filter</label>
											<div class="col-sm-5">
												{!! Form::select('filter_type', array(null=>'--Pilih Filter Alamat--','1'=> 'Berdasarkan Alamat Pasien','2'=>'Data Instansi'), null, ['class' => 'form-control pilih','id'=>"filter_type"]) !!}
											</div>
										</div>
										<div id="form_wilayah ">
											<div class="form-group">
												<label for="input1" class="col-sm-3 control-label">Provinsi</label>
												<div class="col-sm-5">
													{!! Form::select('district[id_provinsi]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control alamat prov pilih','onchange'=>"getKabupaten('_filter')",'id'=>"id_provinsi_filter",'disabled']) !!}
												</div>
											</div>
											<div class="form-group">
												<label for="input1" class="col-sm-3 control-label">Kabupaten</label>
												<div class="col-sm-5">
													{!! Form::select('district[id_kabupaten]', array(null=>'Pilih Kabupaten'), null, ['class' => 'form-control alamat pilih','onchange'=>"getKecamatan('_filter')",'id'=>"id_kabupaten_filter",'disabled']) !!}
												</div>
											</div>
											<div class="form-group">
												<label for="input1" class="col-sm-3 control-label">Kecamatan</label>
												<div class="col-sm-5">
													{!! Form::select('district[id_kecamatan]', array(null=>'Pilih Kecamatan'), null, ['class' => 'form-control alamat pilih','onchange'=>"getPuskesmas('_filter')",'id'=>"id_kecamatan_filter",'disabled']) !!}
												</div>
											</div>
											<div class="form-group">
												<label for="input1" class="col-sm-3 control-label">Puskesmas</label>
												<div class="col-sm-5">
													{!! Form::select('district[id_puskesmas]', array(null=>'Pilih Puskesmas'), null, ['class' => 'form-control alamat pilih','id'=>"puskesmas_filter",'disabled']) !!}
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer" style="text-align: center">
							{!! Form::submit("Tampilkan", ['class' => 'btn btn-success','id'=>"submit_filter"]) !!}
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$('#filter_type').on('change',function(){
			var val = $(this).val();
			if(val==''){
				$('.alamat').attr('disabled','disabled').select2('val',null);
			}else if(val=='1'){
				$('#id_provinsi_filter').removeAttr('disabled');
				$('#id_provinsi_filter').attr('name',"filter[code_provinsi_pasien]");
				$('#id_kabupaten_filter').attr('name',"filter[code_kabupaten_pasien]");
				$('#id_kecamatan_filter').attr('name',"filter[code_kecamatan_pasien]");
				$('#puskesmas_filter_analisa').attr('name',"filter[code_puskesmas_pasien]");
			}else if(val=='2'){
				$('#id_provinsi_filter').removeAttr('disabled');
				$('#id_provinsi_filter').attr('name',"filter[code_provinsi_faskes]");
				$('#id_kabupaten_filter').attr('name',"filter[code_kabupaten_faskes]");
				$('#id_kecamatan_filter').attr('name',"filter[code_kecamatan_faskes]");
				$('#puskesmas_filter').attr('name',"filter[code_puskesmas_faskes]");
			}
			return false;
		});

		$('#id_provinsi_filter').on('change',function(){
			var val = $(this).val();
			if(val!=''){
				$('#id_kabupaten_filter').removeAttr('disabled');
			}else{
				$('#id_kabupaten_filter').attr('disabled','disabled');
			}
			return false;
		});
		$('#id_kabupaten_filter').on('change',function(){
			var val = $(this).val();
			if(val!=''){
				$('#id_kecamatan_filter').removeAttr('disabled');
			}else{
				$('#id_kecamatan_filter').attr('disabled','disabled');
			}
			return false;
		});
		$('#id_kecamatan_filter').on('change',function(){
			var val = $(this).val();
			if(val!='' && $('#filter_type').val()==2){
				$('#puskesmas_filter').removeAttr('disabled');
			}else{
				$('#puskesmas_filter').attr('disabled','disabled');
			}
			return false;
		});

		$('#form_campak').hide();
		$('#form_campak').val(null);
		$('#case_type').on('change',function(){
			var val = $(this).val();
			var dropdown = $('#type');
			if(val=='campak'){
				dropdown.empty();
				var option ={'--Pilih--':null,'Suspek Campak':'1','Campak (Lab,Epid, dan Klinis)':'2','Campak Lab':'3','Campak Epid':'4','Campak Klinis':'5','Rubella':'6'};
				$.each(option, function(key,value) {
					dropdown.append($("<option></option>").attr("value", value).text(key));
				});
				dropdown.select2('val',null);
			}else if(val=='difteri'){
				dropdown.empty();
				var option ={'--Pilih--':null,'Suspek Dikteri':'1','Difteri (Konfirm dan Probable)':'2','Difteri Konfirm':'3','Difteri Probable':'4'};
				$.each(option, function(key,value) {
					dropdown.append($("<option></option>").attr("value", value).text(key));
				});
				dropdown.select2('val',null);
			}else if(val=='tetanus'){
				dropdown.empty();
				var option ={'--Pilih--':null,'Suspek TN':'1','Tetanus Neonatorum':'2'};
				$.each(option, function(key,value) {
					dropdown.append($("<option></option>").attr("value", value).text(key));
				});
			}else if(val=='afp'){
				dropdown.empty();
				var option ={'--Pilih--':null,'APF':'1','Confirmed Polio':'2','Compatible':'3'};
				$.each(option, function(key,value) {
					dropdown.append($("<option></option>").attr("value", value).text(key));
				});
				dropdown.select2('val',null);
			}else if(val=='crs'){
				dropdown.empty();
				var option ={'--Pilih--':null,'Suspek CRS':'1','CRS ( CRS Pasti dan CRS Klinis)':'2','CRS Pasti':'3','CRS Klinis':'4'};
				$.each(option, function(key,value) {
					dropdown.append($("<option></option>").attr("value", value).text(key));
				});
				dropdown.select2('val',null);
			}else{
				dropdown.empty();
				var option ={'--Pilih--':null};
				$.each(option, function(key,value) {
					dropdown.append($("<option></option>").attr("value", value).text(key));
				});
				dropdown.select2('val',null);
			}
			dropdown.select2('val',null);
			return false;
		});
		$('#form_campak').hide();
		$('#jenis_data').hide();
		$('#type').attr('disabled','disabled');

		$(function(){
			$('#case_type').on('change',function(){
				var type = $(this).val();
				console.log(type);
				if (type=="campak") {
					$('#data_type').removeAttr('disabled');
					$('#form_campak').show();
				}else{
					$('#form_campak').hide();
					$('#data_type').attr('disabled','disabled');
					$('#data_type').select2('val',null);
					$('#data_type').val(null);
				}
			});

			$('#range_time').on('change',function(){
				var val = $(this).val();
				if(val=='1'){
					$('#from_day').removeAttr('disabled');
					$('#from_month').removeAttr('disabled');
					$('#from_year').removeAttr('disabled');
					$('#to_day').removeAttr('disabled');
					$('#to_month').removeAttr('disabled');
					$('#to_year').removeAttr('disabled');
				}else if(val=='2'){
					$('#from_day').attr('disabled','disabled').select2('val',null);
					$('#from_month').removeAttr('disabled');
					$('#from_year').removeAttr('disabled');
					$('#to_day').attr('disabled','disabled').select2('val',null);
					$('#to_month').removeAttr('disabled');
					$('#to_year').removeAttr('disabled');
				}else if(val=='3'){
					$('#from_day').attr('disabled','disabled').select2('val',null);
					$('#from_month').attr('disabled','disabled').select2('val',null);
					$('#from_year').removeAttr('disabled');
					$('#to_day').attr('disabled','disabled').select2('val',null);
					$('#to_month').attr('disabled','disabled').select2('val',null);
					$('#to_year').removeAttr('disabled');
				}else{
					$('#from_day').attr('disabled','disabled').select2('val',null);
					$('#from_month').attr('disabled','disabled').select2('val',null);
					$('#from_year').attr('disabled','disabled').select2('val',null);
					$('#to_day').attr('disabled','disabled').select2('val',null);
					$('#to_month').attr('disabled','disabled').select2('val',null);
					$('#to_year').attr('disabled','disabled').select2('val',null);
				}
				return false;
			});
		});

		function graphKlasifikasi(title,range,data){
			$('#graph_klasifikasi_final').highcharts({
				chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false,
					type: 'pie',
					options3d: {
						enabled: true,
						alpha: 45
					},
					events: {
						load: function(event) {
							var total = 0;
							for(var i=0, len=this.series[0].yData.length; i<len; i++){
								total += this.series[0].yData[i];
							}
							jumlah =  total;
						}
					}
				},
				title: {
					text: 'Grafik Penderita '+title+' Berdasar Klasifikasi Final',
				},
				subtitle: {
					text: range
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				plotOptions: {
					pie: {
						depth: 35,
						innerSize:70,
						cursor: 'pointer',
						dataLabels: {
							enabled: true,
							format: '<span style="font-size:larger">{point.name}</span><br />{point.y:.1f} orang',
							style: {
								color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
							}
						}
					}
				},
				credits:{
					enabled:false
				},
				series: [{
					name: 'Klasifikasi Final',
					colorByPoint: true,
					data: data
				}]
			});
			chart = $('#graph_klasifikasi_final').highcharts();
			chart.setTitle({text:'Grafik Penderita '+title+' Berdasar Klasifikasi Final'}, {text:range})
			document.getElementById("total").innerHTML=jumlah+" orang";
		}
		$(function(){
			var title1 = 'Suspek Campak';
			var title2 = ' Rutin dan KLB';
			var range = 'Nasional'+'<br>Tahun 2010-'+new Date().getFullYear();
			var senddata = {'filter':{'code_provinsi_pasien':''}};
			$.ajax({
				method  : "POST",
				url     : BASE_URL+'api/analisa/campak',
				data    : JSON.stringify(senddata),
				beforeSend: function(){
				},
				success: function(data){
					if (data.success==true) {
						endProcess();
						var dt=data.response;
						graphKlasifikasi(title1+title2,range,dt.klasifikasi_final);
						$('#filterGraph1').modal('hide');
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal salah');
						endProcess();
						$('#filterGraph1').modal('hide');
					}
				}
			});
			senddata=[];

			$('#filter').validate({
				rules:{
					'filter_type':'required',
					'case_type':'required',
				},
				messages:{
					'filter_type':'Jenis data wajib diisi',
					'case_type':'Jenis kasus wajib diisi',
				},
				submitHandler: function(){
					var senddata = $('#filter').serializeJSON();
					senddata['filter']['jenis_kasus']=$('#data_type').val();
					title = $('#case_type option:selected').text();

					range = $('#id_provinsi_filter option:selected').text();
					if(range=='Pilih Provinsi'){range='Nasional';}
					if($('#from_year').val() && $('#to_year').val()){range = range+'<br>'+'Tahun'+$('#from_year').val()+'-'+$('#to_year').val();
				}
				else{
					range=range+'<br>Tahun 2010-'+new Date().getFullYear();
				}
				var kasus = $('#case_type').val();
				if(kasus == "campak"){
					title = title+' '+$('#data_type option:selected').text();
				}
				var action = BASE_URL+'api/analisa/'+kasus;
				console.log(JSON.stringify(senddata));
				$.ajax({
					method  : "POST",
					url     : action,
					data    : JSON.stringify(senddata),
					beforeSend: function(){
					},
					success: function(data){
						if (data.success==true) {
							endProcess();
							var dt=data.response;
							graphKlasifikasi(title,range,dt.klasifikasi_final);
							$('#filterGraph').modal('hide');
						}else{
							messageAlert('warning', 'Peringatan', 'Data gagal salah');
							endProcess();
							$('#filterGraph').modal('hide');
						}
					}
				});
				senddata=[];
				return false;
			}
		});
		});

	</script>

<div class="box-header with-border">
	<h3 class="box-title">Graph</h3>
	{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','onclick'=>'graph2("1");']) !!}
	<div class="pull-right col-md-3">
		<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#filterGraph2">Sortir Data &nbsp;<i class="fa fa-cogs"></i></button>
	</div>
</div>
<div class="box-body">
	<div id="graph_klasifikasi_final" style="margin: 0 auto"></div>
	<div class="text-center">Total Penderita : <span style="font-weight: bold" id="totalKlasifikasiFinal"></span></div>
</div>

<script type="text/javascript">
	function graph2($unduh) {
		var case_type = $('#case_type2 option:selected');
		var title1 = 'Campak';
		var kasus = 'campak';
		if(case_type.val() != ''){
			title1 = case_type.text();
			kasus = case_type.val();
		}
		var title2 = '';
		if(case_type.val() == 'campak'){
			title2 = $('#jenis_kasus2 option:selected').text();
		}
		var gTitle = titleFilter2();
		if (gTitle.wil == 'Nasional') {
			gTitle.wil = '{!! Helper::role()->name_role.' '.Helper::role()->name_faskes; !!}';
		}
		var range = gTitle.wil+'<br>'+gTitle.time;
		var fd = {};
		fd['fdata'] = JSON.stringify({"jenis_data":$('#type2').val(),"jenis_kasus":$('#jenis_kasus2').val()});
		fd['dt'] = JSON.stringify($('#filter2').serializeJSON());
		$.ajax({
			method  : "POST",
			url     : BASE_URL+'api/analisa/graphKlasifikasiFinal/'+kasus,
			data    : fd,
			success: function(data){
				if (data.success==true) {
					var dt=data.response;
					if(isset($unduh)){
						var title = title1+' '+title2+' Berdasar Waktu <br>'+gTitle.wil+'<br>'+gTitle.time;
						var head = ['Klasifikasi Final','Jumlah'];
						var item = dt.export;
						exportXls(title, head, item);
					}else{
						graphKlasifikasi(title1+' '+title2,range,dt.klasifikasi_final);
					}
					$('#filterGraph2').modal('hide');
				}else{
					messageAlert('warning', 'Peringatan', 'Data gagal salah');
					$('#filterGraph2').modal('hide');
				}
			}
		});
	};

	function graphKlasifikasi(title,range,data) {
		$('#graph_klasifikasi_final').highcharts({
			chart: {
				type: 'pie',
				options3d: {
					enabled: true,
					alpha: 45
				},
				events: {
					load: function(event) {
						var total = 0;
						for(var i=0, len=this.series[0].yData.length; i<len; i++){
							total += this.series[0].yData[i];
						}
						jumlah =  total;
					}
				}
			},
			title: {
				text: 'Grafik Penderita '+title+' Berdasar Klasifikasi Final'
			},
			subtitle: {
				text: range
			},
			legend: {
				enabled:true,
				layout: 'vertical',
				align: 'left',
				verticalAlign: 'top',
				floating: true,
				labelFormatter: function() {
					var total=0, string;
					console.log('lol');
					Highcharts.each(this.userOptions.data, function(p, i) {
						console.log(p);
					});
					string = this.name+'<br>Jumlah Penderita: ' + total+' orang';
					return string;
				}

			},
			tooltip: {
				enabled:true,
				headerFormat: '<span style="font-size:larger; font-weight:bold; text-transform:uppercase;">{point.key}</span><br/>',
				pointFormat: 'Jumlah Penderita: <b>{point.y:.1f} orang. ({point.percentage:.1f}%)</b>',
			},
			plotOptions: {
				pie: {
					depth: 45,
					dataLabels: {
						enabled: true,
						distance: 20,
						connectorWidth: 2,
						size:"100%",
						format: '<span style="font-size:larger">{point.name} ({point.percentage:.1f}%)</span><br>{point.y:.1f} orang ',
						style: {
							color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						}
					}
				}
			},
			credits:{
				enabled:false
			},
			series: [{
				name: 'Klasifikasi Final',
				colorByPoint: true,
				data: data
			}]
		});
		chart = $('#graph_klasifikasi_final').highcharts();
		chart.setTitle({text:'Grafik Penderita '+title+' Berdasar Klasifikasi Final'}, {text:range})
		document.getElementById("totalKlasifikasiFinal").innerHTML=jumlah+" orang";
	};

	$(function(){
		graph2();
		$('#jenis_data2').hide();

		$('#filter2').validate({
			rules:{
				'case_type':'required',
				'filter_type':'required',
			},
			messages:{
				'case_type':'Kasus wajib diisi',
				'filter_type':'Wilayah wajib diisi',
			},
			submitHandler: function(){
				graph2();
			}
		});
	})
</script>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Pengaturan Tampilan Peta</h4>
			</div>
			<div class="modal-body">
				<div class="box box-success">
					{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'filter' , 'class' => 'form-horizontal']) !!}
					<div class="box-body">
						<div class="row">
							<div class="col-sm-12">
								<div class="box-body">
									<div class="form-group">
										<label for="cases" class="col-sm-3 control-label">Jenis Kasus</label>
										<div class="col-sm-5">
											{!! Form::select('case_type_maps', array(null=>'--Pilih--','afp'=>'AFP','campak'=>'Campak','crs'=>'CRS','difteri'=>'Difteri','tetanus'=>'Tetanus Neonatorum'), null, ['class' => 'form-control', 'id'=>"case_type_maps"]) !!}
										</div>
										<div class="col-sm-4" id="form_campak_maps">
											{!! Form::select('rutin', array('rutin'=>'Rutin','klb'=>'KLB'), 'rutin', ['class' => 'form-control', 'id'=>"data_type_maps"]) !!}
										</div>
									</div>
									<div class="form-group">
										<label for="input1" class="col-sm-3 control-label">Jenis Data</label>
										<div class="col-sm-5">
											{!! Form::select('filter[range]', array(null=>'--Pilih--',), null, ['class' => 'form-control', 'id'=>"type_maps"]) !!}
										</div>
									</div>
									<div class="form-group">
										<label for="input1" class="col-sm-3 control-label">Rentang Waktu</label>
										<div class="col-sm-5">
											{!! Form::select('filter[range]', array(null=>'All','1'=>'Hari','2'=>'Bulan','3'=>'Tahun'), null, ['class' => 'form-control', 'id'=>"range_time_maps"]) !!}
										</div>
									</div>
									<div class="form-group">
										<label for="input2" class="col-sm-3 control-label">Sejak</label>
										<div class="col-sm-2" style="padding-right: 0px;">
											<select name="from[day]" id="from_day_maps" class="form-control" disabled="disabled">
												<option value="">--Pilih--</option>
												@for($i=1;$i<=31;$i++)
												<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
												@endfor
											</select>
										</div>
										<div class="col-sm-3" style="padding-right: 0px;">
											{!! Form::select('from[month]',
												array(null => '--Pilih--',
													'01'=>'Januari',
													'02'=>'Febuari',
													'03'=>'Maret',
													'04'=>'April',
													'05'=>'Mei',
													'06'=>'Juni',
													'07'=>'Juli',
													'08'=>'Agustus',
													'09'=>'September',
													'10'=>'Oktober',
													'11'=>'November',
													'12'=>'Desember',
													), null, ['class'=>'form-control','id'=>"from_month_maps",'disabled']) !!}
												</div>
												<div class="col-sm-3" style="padding-right: 0px;">
													<select name="from[year]" id="from_year_maps" class="form-control" disabled="disabled">
														<option value="">--Pilih--</option>
														@for($i=2010;$i<=2020;$i++)
														<option value="{!! $i !!}">{!! $i !!}</option>
														@endfor
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-3 control-label">Sampai</label>
												<div class="col-sm-2" style="padding-right: 0px;">
													<select name="to[day]" id="to_day_maps" class="form-control" disabled="disabled">
														<option value="">--Pilih--</option>
														@for($i=1;$i<=31;$i++)
														<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
														@endfor
													</select>
												</div>
												<div class="col-sm-3" style="padding-right: 0px;">
													{!! Form::select('to[month]',
														array(null => '--Pilih--',
															'01'=>'Januari',
															'02'=>'Febuari',
															'03'=>'Maret',
															'04'=>'April',
															'05'=>'Mei',
															'06'=>'Juni',
															'07'=>'Juli',
															'08'=>'Agustus',
															'09'=>'September',
															'10'=>'Oktober',
															'11'=>'November',
															'12'=>'Desember',
															), null, ['class'=>'form-control','id'=>"to_month_maps",'disabled']) !!}
														</div>
														<div class="col-sm-3" style="padding-right: 0px;">
															<select name="to[year]" id="to_year_maps" class="form-control" disabled="disabled">
																<option value="">--Pilih--</option>
																@for($i=2010;$i<=2020;$i++)
																<option value="{!! $i !!}">{!! $i !!}</option>
																@endfor
															</select>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Jenis Peta</label>
														<div class="col-sm-4">
															{!! Form::checkbox('Peta Jumlah Kasus', 'peta1', true,['id' => 'checkbox1']); !!} Peta Jumlah Kasus
														</div>
									<!-- <div class="col-sm-4">
										{!! Form::checkbox('Peta Persebaran Kasus', 'peta2', null,['id' => 'checkbox2']); !!} Peta Persebaran Kasus
									</div> -->
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="box-footer" style="text-align: center">
					{!! Form::submit("Tampilkan", ['class' => 'btn btn-success','id'=>"submit_filter_maps"]) !!}
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('#case_type_maps').on('change',function(){
			var dropdown = $('#type_maps');
			var val = $(this).val();
			if(val=='campak'){
			dropdown.empty(); // remove old options
			var option ={'--Pilih--':null,'Suspek Campak':'1','Campak (Lab,Epid, dan Klinis)':'2','Campak Lab':'3','Campak Epid':'4','Campak Klinis':'5','Rubella':'6'};
			$.each(option, function(key,value) {
				dropdown.append($("<option></option>").attr("value", value).text(key));
			});
		}else if(val=='difteri'){
			dropdown.empty(); // remove old options
			var option ={'--Pilih--':null,'Suspek Dikteri':'1','Difteri (Konfirm dan Probable)':'2','Difteri Konfirm':'3','Difteri Probable':'4'};
			$.each(option, function(key,value) {
				dropdown.append($("<option></option>").attr("value", value).text(key));
			});
		}else if(val=='tetanus'){
			dropdown.empty(); // remove old options
			var option ={'--Pilih--':null,'Suspek TN':'1','Tetanus Neonatorum':'2'};
			$.each(option, function(key,value) {
				dropdown.append($("<option></option>").attr("value", value).text(key));
			});
		}else if(val=='afp'){
			dropdown.empty(); // remove old options
			var option ={'--Pilih--':null,'APF':'1','Confirmed Polio':'2','Compatible':'3'};
			$.each(option, function(key,value) {
				dropdown.append($("<option></option>").attr("value", value).text(key));
			});
		}else if(val=='crs'){
			dropdown.empty(); // remove old options
			var option ={'--Pilih--':null,'Suspek CRS':'1','CRS ( CRS Pasti dan CRS Klinis)':'2','CRS Pasti':'3','CRS Klinis':'4'};
			$.each(option, function(key,value) {
				dropdown.append($("<option></option>").attr("value", value).text(key));
			});
		}else{
			dropdown.empty(); // remove old options
			var option ={'--Pilih--':null};
			$.each(option, function(key,value) {
				dropdown.append($("<option></option>").attr("value", value).text(key));
			});
		}
	});
	});
	$('#form_campak_maps').hide();
	$('#form_campak_maps').val(null);
	$('#form_campak_maps').hide();
	$(function(){
		$('#case_type_maps').on('change',function(){
			var type = $(this).val();
			if (type=="campak") {
				$('#form_campak_maps').show();
			}else{
				$('#form_campak_maps').hide();
				$('#form_campak_maps').val(null);
			}
		});
	});

	$(function(){
		$('#range_time_maps').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#from_day_maps').removeAttr('disabled');
				$('#from_month_maps').removeAttr('disabled');
				$('#from_year_maps').removeAttr('disabled');
				$('#to_day_maps').removeAttr('disabled');
				$('#to_month_maps').removeAttr('disabled');
				$('#to_year_maps').removeAttr('disabled');
			}else if(val=='2'){
				$('#from_day_maps').attr('disabled','disabled').select2('val',null);
				$('#from_month_maps').removeAttr('disabled');
				$('#from_year_maps').removeAttr('disabled');
				$('#to_day_maps').attr('disabled','disabled').select2('val',null);
				$('#to_month_maps').removeAttr('disabled');
				$('#to_year_maps').removeAttr('disabled');
			}else if(val=='3'){
				$('#from_day_maps').attr('disabled','disabled').select2('val',null);
				$('#from_month_maps').attr('disabled','disabled').select2('val',null);
				$('#from_year_maps').removeAttr('disabled');
				$('#to_day_maps').attr('disabled','disabled').select2('val',null);
				$('#to_month_maps').attr('disabled','disabled').select2('val',null);
				$('#to_year_maps').removeAttr('disabled');
			}else{
				$('#from_day_maps').attr('disabled','disabled').select2('val',null);
				$('#from_month_maps').attr('disabled','disabled').select2('val',null);
				$('#from_year_maps').attr('disabled','disabled').select2('val',null);
				$('#to_day_maps').attr('disabled','disabled').select2('val',null);
				$('#to_month_maps').attr('disabled','disabled').select2('val',null);
				$('#to_year_maps').attr('disabled','disabled').select2('val',null);
			}
			return false;
		});
	})
</script>

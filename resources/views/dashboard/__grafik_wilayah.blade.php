<div class="box-header with-border">
	<h3 class="box-title">Grafik Penderita Berdasar Wilayah</h3>
	<div class="pull-right col-md-2">
		<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#filterGraph4">Sortir Data &nbsp;<i class="fa fa-cogs"></i></button>
	</div>
</div>
<div class="box-body">
	<div id="graph_wilayah" style="margin: 0 auto"></div>
</div>

<script type="text/javascript">
	function graphWilayah(title,range,data){
		$('#graph_wilayah').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: 'Grafik Total Penderita '+title+' Per Wilayah'
			},
			subtitle: {
				text: range
			},
			xAxis: {
				type: 'category'
			},
			yAxis: {
				title: {
					text: 'Jumlah Penderita (orang)'
				}
			},
			lang: {
				drillUpText: '<< Kembali ke- {series.name}'
			},
			legend: {
				enabled: true,
				align: 'center',
				verticalAlign: 'bottom',
				labelFormatter: function() {
					var total=0, string;
					Highcharts.each(this.userOptions.data, function(p, i) {
						if (p.y) {
							tmp = parseInt(p.y | 0);
							total += tmp;
						}else if (p.y==0) {
							tmp = 0;
							total += tmp;
						}else {
							total += p[1];
						}
					});
					string = this.name+'<br>Jumlah Penderita: ' + total+' orang';
					return string;
				}
			},
			tooltip: {
				pointFormat: 'JUMLAH KASUS DI WILAYAH {point.name}: <b>{point.y} orang</b>'
			},
			plotOptions: {
				series: {
					borderWidth: 0,
					dataLabels: {
						enabled: true,
						format: '{point.y} orang',
					}
				}
			},
			credits:{
				enabled:false
			},
			series:data.data,
			drilldown: {
				drillUpButton: {
					relativeTo: 'spacingBox',
					position: {
						y: 0,
						x: 0
					},
					theme: {
						fill: 'white',
						'stroke-width': 1,
						stroke: 'silver',
						r: 0,
						states: {
							hover: {
								fill: '#a4edba'
							},
							select: {
								stroke: '#039',
								fill: '#a4edba'
							}
						}
					}
				},
				series: data.drilldown
			}
		});
	}

	$(function () {
		var title1 = 'Suspek Campak';
		var title2 = ' Rutin dan KLB';
		var range = 'Nasional Berdasarkan Data Instansi'+'<br>Tahun 2010-'+new Date().getFullYear();
		var senddata = {'filter':{'code_provinsi_faskes':''},'case_type':'campak','filter_type':'2'};
		$.ajax({
			method  : "POST",
			url     : BASE_URL+'api/analisa/wilayah',
			data    : JSON.stringify(senddata),
			success: function(data){
				if (data.success==true) {
					endProcess();
					var dt=data.response;
					graphWilayah(title1+title2,range,dt.data);
					$('#filterGraph1').modal('hide');
				}else{
					messageAlert('warning', 'Peringatan', 'Data gagal salah');
					endProcess();
					$('#filterGraph1').modal('hide');
				}
			}
		});
		senddata=[];
		$('#filter4').validate({
			rules:{
				'filter_type':'required',
				'jenis_kasus':'required',
				'case_type':'required',
			},
			messages:{
				'filter_type':'Jenis data wajib diisi',
				'jenis_kasus':'Jenis data kasus wajib diisi',
				'case_type':'Jenis kasus wajib diisi',
			},
			submitHandler: function(){
				var title = 'Suspek Campak';
				var range = 'Nasional'+'<br>'+'Tahun 2015-2016';
				var senddata = $('#filter4').serializeJSON();
				var klasfinal = $('#type4').val();
				if (klasfinal != 0) {
					senddata['filter']['klasifikasi_final']=$('#type4').val();
				}
				range='Nasional';
				if($('#from_year4').val() && $('#to_year4').val()){range = range+'<br>'+'Tahun'+$('#from_year4').val()+'-'+$('#to_year4').val();
			}
			else{
				range=range+'<br>Tahun 2010-'+new Date().getFullYear();
			}
			jenis_data = $('#type4 option:selected').text();
			title = jenis_data;
			var kasus = $('#case_type4').val();
			if(kasus == "campak"){
				senddata['filter']['jenis_kasus']=$('#data_type4').val();
				title = title+' '+$('#data_type4 option:selected').text();
			}
			var action = BASE_URL+'api/analisa/wilayah';
			$.ajax({
				method  : "POST",
				url     : action,
				data    : JSON.stringify(senddata),
				beforeSend: function(){
				},
				success: function(data){
					if (data.success==true) {
						endProcess();
						var dt=data.response;
						graphWilayah(title,range,dt.data);
						console.log(data.response);

						$('#filterGraph4').modal('hide');
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal salah');
						endProcess();
						$('#filterGraph4').modal('hide');
					}
				}
			});
			senddata=[];
			return false;
		}
	});
	});
</script>

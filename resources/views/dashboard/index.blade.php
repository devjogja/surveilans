@extends('layouts.base')
@section('content')
@include('dashboard.filter_graph_index')
<div class="col-md-12">
	<div class="row">
		<div class="col-md-8">
			<div class="box box-success" style="padding-bottom: 18px">
				<div class="box-header with-border">
					<h3 class="box-title">Info</h3>
				</div>
				<div class="box-body">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<?php $i=0;?>
							@foreach(Helper::getCase() AS $key=>$val)
							@if($val->alias !== 'crs')
							<li data-target="#carousel-example-generic" data-slide-to="{{ $i++ }}" class=""></li>
							@endif
							@endforeach
						</ol>
						<div class="carousel-inner">
							@foreach(Helper::getCase() AS $key=>$val)
							@if($val->alias !== 'crs')
							<?php $active = ($key==0)?'active':null;?>
							<div class="item {{$active}}">
								<center>
									<img src="{{URL::to("asset/images/slide-$val->alias.jpg")}}" alt="{{$val->name}}">
								</center>
								<div class="carousel-caption">
									<h4>{{$val->name}}</h4>
									<p style="background:black;">{{$val->description}} </p>
								</div>
							</div>
							@endif
							@endforeach
						</div>
						<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
							<span class="fa fa-angle-left"></span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
							<span class="fa fa-angle-right"></span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="box box-success">
				@include('dashboard.login')
			</div>
			<div class="box box-success">
				<div class="box-body">
					<div class="row">
						<div class="col-md-4">
							<a href="http://202.70.136.52/rsonline/report/" target="_blank">
								{!! HTML::image('asset/images/link-data-puskesmas.jpg', 'Puskesmas') !!}
							</a>
						</div>
						<div class="col-md-4">
							<a href="http://www.bankdata.depkes.go.id/puskesmas/" target="_blank">
								{!! HTML::image('asset/images/link-data-rs.jpg', 'Rumah sakit') !!}
							</a>
						</div>
						<div class="col-md-4">
							<a href="http://www.infopenyakit.org/" target="_blank">
								{!! HTML::image('asset/images/link-info-penyakit.jpg', 'Penyakit') !!}
							</a>
						</div>
						<div class="col-md-4">
							<a href="http://www.depkes.go.id/" target="_blank">
								{!! HTML::image('asset/images/link-kemenkes.jpg', 'KEMENKES') !!}
							</a>
						</div>
						<div class="col-md-4">
							<a href="http://www.pppl.kemkes.go.id/" target="_blank">
								{!! HTML::image('asset/images/link-p2pl.jpg', 'P2PL') !!}
							</a>
						</div>
						<div class="col-md-4">
							<a href="http://www.who.int" target="_blank">
								{!! HTML::image('asset/images/link-who.jpg', 'WHO') !!}
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="row">
		<div class="col-md-8">
			<div class="box box-success" style="padding-bottom: 20px">
				@include('dashboard.grafik_bulanan')
			</div>
		</div>
		<div class="col-md-4">
			<div class="box box-success">
				@include('dashboard.grafik_klasifikasi_final')
			</div>
		</div>
	</div>
</div>
<div class="col-sm-12">
	<div class="box box-success">
		@include('dashboard.grafik_umur')
	</div>
</div>
<div class="col-sm-12">
	<div class="box box-success">
		@include('dashboard.grafik_wilayah')
	</div>
</div>

<style type="text/css" media="screen">
.rowlinks{
	width: 100%;
}
.rowlinks tr td{
	text-align: center;
}
.links{
	padding: 12px 0px;
	width: 130px;
}
</style>
@endsection
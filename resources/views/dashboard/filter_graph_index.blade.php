@for($modal=1;$modal<=4;$modal++)
<div class="modal fade" id="filterGraph{!! $modal !!}" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Pengaturan Tampilan Data</h4>
			</div>
			<div class="modal-body">
				<div class="box box-success">
					{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>"filter$modal" , 'class' => 'form-horizontal']) !!}
					@if(!empty(Helper::role()->code_role))
					{!! Form::hidden('code_role',Helper::role()->code_role) !!}
					{!! Form::hidden('id_faskes',Helper::role()->id_faskes) !!}
					{!! Form::hidden('code_faskes',Helper::role()->code_faskes) !!}
					@endif
					<div class="box-body">
						<div class="row">
							<div class="col-sm-12">
								<div class="box-body">
									<div class="form-group">
										<label for="cases" class="col-sm-3 control-label">Jenis Kasus</label>
										<div class="col-sm-5">
											{!! Form::select('case_type', array(''=>'--Pilih--','campak'=>'Campak','difteri'=>'Difteri','tetanus'=>'Tetanus Neonatorum'), null, ['class' => 'form-control', 'id'=>"case_type$modal"]) !!}
										</div>
										<div class="col-sm-4" id="jenis{!! $modal !!}">
											{!! Form::select('jenis_kasus', array(''=>'--Pilih--','0'=>'Rutin dan KLB','2'=>'Rutin','1'=>'KLB'), null, ['class' => 'form-control', 'id'=>"jenis_kasus$modal",'disabled']) !!}
										</div>
									</div>
									<div class="form-group" id="jenis_data{!! $modal !!}">
										<label for="input1" class="col-sm-3 control-label">Jenis Data</label>
										<div class="col-sm-5">
											{!! Form::select('jenis_data', array(null=>'--Pilih--'), null, ['class' => 'form-control', 'id'=>"type$modal",'required']) !!}
										</div>
									</div>
									<div class="form-group">
										<label for="input1" class="col-sm-3 control-label">Rentang Waktu</label>
										<div class="col-sm-5">
											{!! Form::select('filter[range]', array(null=>'All','1'=>'Hari','2'=>'Bulan','3'=>'Tahun'), null, ['class' => 'form-control', 'id'=>"range_time$modal"]) !!}
										</div>
									</div>
									<div class="form-group">
										<label for="input2" class="col-sm-3 control-label">Sejak</label>
										<div class="col-sm-2" style="padding-right: 0px;">
											<select name="from[day]" id="from_day{!! $modal !!}" class="form-control" disabled="disabled">
												<option value="">--Pilih--</option>
												@for($i=1;$i<=31;$i++)
												<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
												@endfor
											</select>
										</div>
										<div class="col-sm-3" style="padding-right: 0px;">
											{!! Form::select('from[month]',Helper::getMonth(), null, ['class'=>'form-control','id'=>"from_month$modal",'disabled']) !!}
										</div>
										<div class="col-sm-3" style="padding-right: 0px;">
											<select name="from[year]" id="from_year{!! $modal !!}" class="form-control" disabled="disabled">
												<option value="">--Pilih--</option>
												@for($i=2010;$i<=date('Y')+5;$i++)
												<option value="{!! $i !!}">{!! $i !!}</option>
												@endfor
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label">Sampai</label>
										<div class="col-sm-2" style="padding-right: 0px;">
											<select name="to[day]" id="to_day{!! $modal !!}" class="form-control" disabled="disabled">
												<option value="">--Pilih--</option>
												@for($i=1;$i<=31;$i++)
												<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
												@endfor
											</select>
										</div>
										<div class="col-sm-3" style="padding-right: 0px;">
											{!! Form::select('to[month]',Helper::getMonth(), null, ['class'=>'form-control','id'=>"to_month$modal",'disabled']) !!}
										</div>
										<div class="col-sm-3" style="padding-right: 0px;">
											<select name="to[year]" id="to_year{!! $modal !!}" class="form-control" disabled="disabled">
												<option value="">--Pilih--</option>
												@for($i=2010;$i<=date('Y')+5;$i++)
												<option value="{!! $i !!}">{!! $i !!}</option>
												@endfor
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12"  id="filter_wilayah{!! $modal !!}">
								<div class="box-body">
									<div class="form-group">
										<label for="input1" class="col-sm-3 control-label">Filter</label>
										<div class="col-sm-5">
											{!! Form::select('filter_type', array(null=>'--Pilih Filter Alamat--','1'=> 'Berdasarkan Alamat Pasien','2'=>'Data Instansi'), null, ['class' => 'form-control','id'=>"filter_type$modal"]) !!}
										</div>
									</div>
									<div id="form_wilayah{!! $modal !!}">
										<div class="form-group">
											<label for="input1" class="col-sm-3 control-label">Provinsi</label>
											<div class="col-sm-5">
												{!! Form::select('filter[id_provinsi]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control alamat','id'=>"id_provinsi_filter$modal",'disabled']) !!}
											</div>
										</div>
										<div class="form-group">
											<label for="input1" class="col-sm-3 control-label">Kabupaten</label>
											<div class="col-sm-5">
												{!! Form::select('filter[id_kabupaten]', array(null=>'Pilih Kabupaten'), null, ['class' => 'form-control alamat','id'=>"id_kabupaten_filter$modal",'disabled']) !!}
											</div>
											<div class="col-sm-4 instansi{!! $modal !!}">
												{!! Form::select('filter[id_rumah_sakit]', array(null=>'Pilih Rumah Sakit'), null, ['class' => 'form-control alamat','id'=>"rs_filter$modal",'disabled']) !!}
											</div>
										</div>
										<div class="form-group">
											<label for="input1" class="col-sm-3 control-label">Kecamatan</label>
											<div class="col-sm-5">
												{!! Form::select('filter[id_kecamatan]', array(null=>'Pilih Kecamatan'), null, ['class' => 'form-control alamat','id'=>"id_kecamatan_filter$modal",'disabled']) !!}
											</div>
											<div class="col-sm-4 instansi{!! $modal !!}">
												{!! Form::select('filter[id_puskesmas]', array(null=>'Pilih Puskesmas'), null, ['class' => 'form-control alamat','id'=>"puskesmas_filter$modal",'disabled']) !!}
											</div>
										</div>
										<div class="form-group alpasien{!! $modal !!}">
											<label for="input1" class="col-sm-3 control-label">Kelurahan/Desa</label>
											<div class="col-sm-5">
												{!! Form::select('filter[id_kelurahan]', array(null=>'Pilih Kelurahan'), null, ['class' => 'form-control alamat','id'=>"id_kelurahan_filter$modal",'disabled']) !!}
											</div>
										</div>
										<div class="form-group instansi{!! $modal !!}">
											<label class="col-sm-3 control-label">Wilayah Kerja Puskesmas</label>
											<div class="col-sm-5">
												{!! Form::select('filter[id_wilayah_kerja]', [null=>'Pilih Wilayah Kerja Puskesmas'], null, ['class' => 'form-control alamat','id'=>"id_wilayah_kerja_filter$modal"]) !!}
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box-footer" style="text-align: center">
						{!! Form::submit("Tampilkan", ['class' => 'btn btn-success','id'=>"submit_filter$modal"]) !!}
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function titleFilter{{$modal}}(){
		var wil = 'Nasional';
		var fw = $('#filter_type{{$modal}}').val();
		var prov = $('#id_provinsi_filter{{$modal}} option:selected');
		var kab = $('#id_kabupaten_filter{{$modal}} option:selected');
		var kec = $('#id_kecamatan_filter{{$modal}} option:selected');
		var rs = $('#rs_filter{{$modal}} option:selected');
		var pkm = $('#puskesmas_filter{{$modal}} option:selected');
		if(isset(fw) && isset(prov.val())){
			wil = '';
			if(isset(prov.val())){
				wil += prov.text()+', ';
				if(fw=='2'){
					wil = 'DINAS KESEHATAN PROVINSI '+prov.text();
				}
			}
			if(isset(kab.val())){
				wil += kab.text()+', ';
				if(fw=='2'){
					wil = 'DINAS KESEHATAN KABUPATEN '+kab.text()+', PROVINSI '+prov.text();
				}
			}
			if(isset(kec.val())){ wil += kec.text()+', '; }
			if(isset(rs.val())){ wil = rs.text(); }
			if(isset(pkm.val())){ wil = 'PUSKESMAS '+pkm.text(); }
		}

		var time = 'Tahun 2010 s/d '+new Date().getFullYear();
		var ft = $('#range_time{{$modal}}').val();
		var fd = $('#from_day{{$modal}} option:selected');
		var fm = $('#from_month{{$modal}} option:selected');
		var fy = $('#from_year{{$modal}} option:selected');
		var td = $('#to_day{{$modal}} option:selected');
		var tm = $('#to_month{{$modal}} option:selected');
		var ty = $('#to_year{{$modal}} option:selected');
		if(isset(ft)){
			time = '';
			if(isset(fd.val())){ time += fd.text()+' '};
			if(isset(fm.val())){ time += fm.text()+' '};
			if(isset(fy.val())){ time += fy.text()};
			time += ' s/d ';
			if(isset(td.val())){ time += td.text()+' '};
			if(isset(tm.val())){ time += tm.text()+' '};
			if(isset(ty.val())){ time += ty.text()};
		}
		return {"wil":wil,"time":time};
	}

	$(function(){
		$('#case_type{{$modal}}').on('change',function(){
			var type = $(this).val();
			if (type=="campak") {
				$('#jenis_kasus{{$modal}}').removeAttr('disabled');
				$('#jenis{{$modal}}').show();
			}else{
				$('#jenis{{$modal}}').hide();
				$('#jenis_kasus{{$modal}}').attr('disabled','disabled');
				$('#jenis_kasus{{$modal}}').val('').trigger('change');
			}
		});

		$('.instansi{{$modal}}').hide();
		$('.alpasien{{$modal}}').hide();
		$('#filter_type{{$modal}}').on('change',function(){
			var val = $(this).val();
			$('.alamat').attr('disabled','disabled').val(null).trigger('change');
			$('#id_provinsi_filter{{$modal}}').removeAttr('disabled');
			if(val=='1'){
				$('.alpasien{{$modal}}').show();
				$('.instansi{{$modal}}').hide();
				$('#id_provinsi_filter{{$modal}}').attr('name',"filter[code_provinsi_pasien]");
				$('#id_kabupaten_filter{{$modal}}').attr('name',"filter[code_kabupaten_pasien]");
				$('#id_kecamatan_filter{{$modal}}').attr('name',"filter[code_kecamatan_pasien]");
				$('#id_kelurahan_filter{{$modal}}').attr('name',"filter[code_kelurahan_pasien]");
			}else if(val=='2'){
				$('.alpasien{{$modal}}').hide();
				$('.instansi{{$modal}}').show();
				$('#id_provinsi_filter{{$modal}}').attr('name',"filter[code_provinsi_faskes]");
				$('#id_kabupaten_filter{{$modal}}').attr('name',"filter[code_kabupaten_faskes]");
				$('#rs_filter{{$modal}}').attr('name',"filter[rs_id]");
				$('#id_kecamatan_filter{{$modal}}').attr('name',"filter[code_kecamatan_faskes]");
				$('#puskesmas_filter{{$modal}}').attr('name',"filter[puskesmas_id]");
			}
			return false;
		});
		$('#range_time{{$modal}}').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#from_day{{$modal}}, #from_month{{$modal}}, #from_year{{$modal}}, #to_day{{$modal}}, #to_month{{$modal}}, #to_year{{$modal}}').removeAttr('disabled');
			}else if(val=='2'){
				$('#from_month{{$modal}}, #from_year{{$modal}}, #to_month{{$modal}}, #to_year{{$modal}}').removeAttr('disabled');
				$('#from_day{{$modal}}, #to_day{{$modal}}').attr('disabled','disabled').val(null).trigger('change');
			}else if(val=='3'){
				$('#from_year{{$modal}}, #to_year{{$modal}}').removeAttr('disabled');
				$('#from_day{{$modal}}, #from_month{{$modal}}, #to_day{{$modal}}, #to_month{{$modal}}').attr('disabled','disabled').val(null).trigger('change');
			}else{
				$('#from_day{{$modal}}, #from_month{{$modal}}, #from_year{{$modal}}, #to_day{{$modal}}, #to_month{{$modal}}, #to_year{{$modal}}').attr('disabled','disabled').val(null).trigger('change');
			}
			return false;
		});

		$('#id_provinsi_filter{{$modal}}').on('change',function(){
			getKabupaten('_filter{{$modal}}');
			return false;
		});
		$('#id_kabupaten_filter{{$modal}}').on('change',function(){
			var val = $('#filter_type{{$modal}}').val();
			if(val=='2'){
				getRs('_filter{{$modal}}');
			}
			getKecamatan('_filter{{$modal}}');
			return false;
		});
		$('#id_kecamatan_filter{{$modal}}').on('change',function(){
			var val = $('#filter_type{{$modal}}').val();
			if(val=='1'){
				getKelurahan('_filter{{$modal}}');
			}else if(val=='2'){
				getPuskesmas('_filter{{$modal}}');
			}
			return false;
		});
		$('#puskesmas_filter{{$modal}}').on('change',function(){
			getWilayahKerja('_filter{{$modal}}');
			return false;
		});
		$('#jenis{{$modal}}').hide();
		$('#case_type{{$modal}}').on('change',function(){
			var val = $(this).val();
			var dropdown = $('#type{{$modal}}');
			if(val=='campak'){
				dropdown.empty();
				var option ={'--Pilih--':'','Suspek Campak':'1','Campak (Lab,Epid, dan Klinis)':'2','Campak Lab':'3','Campak Epid':'4','Campak Klinis':'5','Rubella':'6'};
				$.each(option, function(key,value) {
					dropdown.append($("<option></option>").attr("value", value).text(key));
				});
				dropdown.val('').trigger('change');
			}else if(val=='difteri'){
				dropdown.empty();
				var option ={'--Pilih--':'','Suspek Difteri':'1','Difteri (Konfirm dan Probable)':'2','Difteri Konfirm':'3','Difteri Probable':'4'};
				$.each(option, function(key,value) {
					dropdown.append($("<option></option>").attr("value", value).text(key));
				});
				dropdown.val('').trigger('change');
			}else if(val=='tetanus'){
				dropdown.empty();
				var option ={'--Pilih--':'','Suspek TN':'1','Tetanus Neonatorum (Konfirm TN)':'2'};
				$.each(option, function(key,value) {
					dropdown.append($("<option></option>").attr("value", value).text(key));
				});
				dropdown.val('').trigger('change');
			}else{
				dropdown.empty();
				var option ={'--Pilih--':''};
				$.each(option, function(key,value) {
					dropdown.append($("<option></option>").attr("value", value).text(key));
				});
				dropdown.val('').trigger('change');
			}
			return false;
		});
	})
</script>
@endfor

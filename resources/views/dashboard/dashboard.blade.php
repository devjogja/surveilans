@extends('layouts.base')
@section('content')
@include('dashboard.filter_graph_index')
<style type="text/css" media="screen">
.caption{
	padding: 12px !important;
}	
</style>
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			<div class="row">
				<div class="col-sm-8">
					<?php
					$name_role = null;
					$role = Helper::role();
					if($role->code_role=='provinsi' || $role->code_role=='kabupaten'){
						$name_role = 'DINAS KESEHATAN '.$role->name_role.' '.$role->name_faskes;
					}else {
						$name_role = $role->name_role.' '.$role->name_faskes;
					}
					?>
					SELAMAT DATANG DI {!! $name_role; !!}
					<a href='{!! URL::to('selectProfile'); !!}' class="btn btn-success btn-flat">Pilih Profil</a>
				</div>
				<div class="col-sm-4">
					<a href='{!! URL::to('user/list'); !!}' class="btn btn-primary btn-flat pull-right">Daftar Petugas Surveilans</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-sm-12">
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="item active">
				<div class="row-fluid col-sm-10 col-sm-offset-1">
					<center>
						@foreach(Helper::getCase() AS $key=>$val)
						<div class="col-sm-2">
							<div class="thumbnail"  style="height:20rem;padding-top:3rem">
								<a href="{!! URL::to("case/$val->alias")!!}" >
									<img src="{!!URL::to("asset/images/$val->image")!!}" alt="Image" class="img-circle" style="max-width:50%;" />
									<div class="caption">{!!$val->name!!}</div>
								</a>
							</div>
						</div>
						@endforeach
					</center>
				</div>
			</div>
			<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
				<span class="fa fa-angle-left blue"></span>
			</a>
			<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
				<span class="fa fa-angle-right blue"></span>
			</a>
		</div>
	</div>
</div>
<div class="col-sm-9">
	<div class="box box-success" style="height:500px">
		@include('dashboard.graph_dashboard')
	</div>
</div>
<div class="col-md-3">
	<div class="box box-success" style="height:500px">
		<div class="box-body">
			<div class="col-md-12">
				<span class="btn btn-block btn-default btn-flat" style="border-color: white;">Kasus PD3I<br>Pending / Jml Spesimen / Total Suspek<br>{!!date('Y')!!}</span>
			</div>
			@foreach(Helper::getCase() AS $key=>$val)
			@if($val->id !== '4')
			<div class="col-md-12">
				<span class="btn btn-block btn-info btn-flat" style="border-color: white;">{!!$val->name!!}
					<br>{!! $data['spesimen'][$val->alias]['pending'].'/'.$data['spesimen'][$val->alias]['spesimen'].'/'.$data['spesimen'][$val->alias]['total'] !!}
				</span>
			</div>
			@endif
			@endforeach
			<div class="col-md-12">
				<span class="btn btn-block btn-default btn-flat" style="border-color: white;">Kasus PD3I<br>Total Suspek<br>{!!date('Y')!!}</span>
			</div>
			@foreach(Helper::getCase() AS $key=>$val)
			@if($val->id == '4')
			<div class="col-md-12">
				<span class="btn btn-block btn-info btn-flat" style="border-color: white;">{!!$val->name!!}
					<br>{!! $data['spesimen'][$val->alias]['total'] !!}
				</span>
			</div>
			@endif
			@endforeach
		</div>
	</div>
</div>

<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Notifikasi (Pemberitahuan)</h3>
		</div>
		<div class="box-body">
			@if(Helper::role()->code_role !== 'rs')
			<div class="col-sm-4">
				<button class="btn btn-block btn-default btn-flat">Cross</button>
				<div class="timeline" style="overflow-y:scroll;overflow-x: hidden;height:440px;">
					@foreach($data['cross_notif'] AS $key=>$val)
					<article class="panel panel-primary">
						<div class="panel-heading icon">
							<i class="icon-bullhorn"></i>
						</div>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-8">
									<h2 class="panel-title">Dari {!! $val->faskes; !!}</h2>
								</div>
								<div class="col-sm-4">
									<button class="btn btn-block btn-success btn-flat gpasien" dIdPasien="{!! $val->id_pasien !!}" dAliasCase="{!! $val->alias_case; !!}">Lihat pasien</button>
								</div>
							</div>
						</div>
						<div class="panel-body">
							Ada Penderita {!! $val->name_case; !!} dari {!! $val->district_pasien; !!} yang periksa di {!! $val->faskes; !!}
						</div>
						<div class="panel-footer" style="text-align: right;">
							<small>Tanggal input : {!! date('d-M-Y',strtotime($val->date_submit)); !!}</small>
						</div>
					</article>
					@endforeach
				</div>
			</div>
			@endif
			<div class="col-sm-4">
				<button class="btn btn-block btn-default btn-flat">Alert KLB</button>
				<div class="timeline" style="overflow-y:scroll;overflow-x: hidden;height:440px;">
					@foreach($data['klb_notif'] AS $key=>$val)
					<article class="panel panel-primary">
						<div class="panel-heading icon">
							<i class="icon-bullhorn"></i>
						</div>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-8">
									<h2 class="panel-title">{!! $val->district_pasien; !!}</h2>
								</div>
								<div class="col-sm-4">
									<button class="btn btn-block btn-success btn-flat gpasien" dklb='1' dIdPasien="{!! $val->id_pasien !!}" dAliasCase="{!! $val->alias_case; !!}">Lihat pasien</button>
								</div>
							</div>
						</div>
						<div class="panel-body">
							Terjadi KLB {!! $val->name_case; !!} di {!! $val->district_pasien; !!}
						</div>
						<div class="panel-footer" style="text-align: right;">
							<small>Tanggal input KLB {!! $val->name_case; !!} : {!! date('d-M-Y',strtotime($val->date_submit)); !!}</small>
						</div>
					</article>
					@endforeach
				</div>
			</div>
			{{-- <div class="col-sm-4">
				<button class="btn btn-block btn-default btn-flat">Notif dari Rumahsakit</button>
				<div class="timeline" style="overflow-y:scroll;overflow-x: hidden;height:440px;">
					@foreach($data['rs_notif'] AS $key=>$val)
					<article class="panel panel-primary">
						<div class="panel-heading icon">
							<i class="icon-bullhorn"></i>
						</div>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-8">
									<h2 class="panel-title">{!! $val->name_faskes; !!}</h2>
								</div>
								<div class="col-sm-4">
									<button class="btn btn-block btn-success btn-flat gpasien" dIdPasien="{!! $val->id_pasien !!}" dAliasCase="{!! $val->alias_case; !!}">Lihat pasien</button>
								</div>
							</div>
						</div>
						<div class="panel-body">
							Ada Penderita {!! $val->name_case; !!} dari {!! $val->name_kecamatan_pasien; !!} yang periksa di {!! $val->name_faskes; !!}
						</div>
						<div class="panel-footer" style="text-align: right;">
							<small>Tanggal periksa : {!! date('d-M-Y',strtotime($val->date_submit)); !!}</small>
						</div>
					</article>
					@endforeach
				</div>
			</div> --}}
		</div>
	</div>
</div>

<div class="modal fade" id="view_pasien" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Data pasien</h4>
			</div>
			<div class="modal-body">
				<div class="box box-success">
					<table class="table table-striped">
						<tr>
							<td width="25%">Nama</td>
							<td>: <span class="name"></span></td>
						</tr>
						<tr>
							<td>Nama Orang tua</td>
							<td>: <span class="nama_ortu"></span></td>
						</tr>
						<tr>
							<td>Tanggal Lahir</td>
							<td>: <span class="tgl_lahir"></span></td>
						</tr>
						<tr>
							<td>Umur</td>
							<td>: <span class="umur"></span></td>
						</tr>
						<tr>
							<td>Jenis Kelamin</td>
							<td>: <span class="jenis_kelamin"></span></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td>: <span class="alamat"></span> <span class="name_kelurahan"></span> <span class="name_kecamatan"></span> <span class="name_kabupaten"></span> <span class="name_provinsi"></span></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<a href="" class="btn btn-info to_daftar">Daftar Kasus</a>
				<a action="" class="btn btn-default" data-dismiss="modal">Tutup</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('.gpasien').click(function(){
			$('#view_pasien').modal('show');
			var dklb = $(this).attr('dklb');
			if (dklb==1) {
				$('.to_daftar').hide();
			}else{
				$('.to_daftar').show();
			}
			var id_pasien = $(this).attr('dIdPasien');
			var acase = $(this).attr('dAliasCase');
			var action = BASE_URL+'pasien/getDataPasien/'+id_pasien;
			$.getJSON(action, function(result){
				var raw = result.response;
				var u = BASE_URL+'case/'+acase;
				$('.to_daftar').attr('href',u);
				$('.name, .nama_ortu, .tgl_lahir, .umur, .jenis_kelamin, .alamat, .name_kelurahan, .name_kecamatan, .name_kabupaten, .name_provinsi').html('');
				if (isset(raw)) {
					$.each(raw, function(k, dt){
						$('.name').html(dt.name);
						$('.nama_ortu').html(dt.nama_ortu);
						$('.tgl_lahir').html(dt.tgl_lahir);
						$('.umur').html(dt.umur);
						$('.jenis_kelamin').html(dt.jenis_kelamin_txt);
						$('.alamat').html(dt.alamat);
						$('.name_kelurahan').html(dt.name_kelurahan);
						$('.name_kecamatan').html(dt.name_kecamatan);
						$('.name_kabupaten').html(dt.name_kabupaten);
						$('.name_provinsi').html(dt.name_provinsi);
					});
				};
			});
			return false;
		});
	});
</script>

@endsection
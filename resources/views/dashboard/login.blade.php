<div class="signin">
	<div class="box-header with-border">
		<h3 class="box-title">Sign In</h3>
	</div>
	{!! Form::open(['method' => 'POST', 'url' => 'login', 'class' => 'form-horizontal']) !!}
		<div class="box-body">
			<div class="form-group">
			    {!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-9">
			    	{!! Form::text('email', null, ['class' => 'form-control','placeholder'=>'Email']) !!}
				</div>
			</div>
			<div class="form-group">
			    {!! Form::label('password', 'Password', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-9">
					<input type="password" name="password" class='form-control' placeholder="Password">
				</div>
			</div>
			<div class="text-right">
				<a href="" id="resetpass">Lupa kata sandi ?</a>
			</div>
		</div>
	    <div class="box-footer">
	        {!! Form::submit("Login", ['class' => 'btn btn-success pull-right']) !!}
	    </div>
	{!! Form::close() !!}
</div>
<div class="resetpass" style="display: none;">
	<div class="box-header with-border">
		<h3 class="box-title">Lupa kata sandi</h3>
	</div>
	{!! Form::open(['method' => 'POST', 'url' => 'reset_pass', 'class' => 'form-horizontal']) !!}
		<div class="box-body">
			<div class="form-group">
			    {!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-9">
			    	{!! Form::text('email', null, ['class' => 'form-control','placeholder'=>'Email', 'required']) !!}
				</div>
			</div>
		</div>
	    <div class="box-footer">
	        {!! Form::submit("Lupa kata sandi", ['class' => 'btn btn-success pull-right']) !!}
	        {!! Form::button("Login", ['class' => 'btn btn-info pull-right', 'id'=>'_signin']) !!}
	    </div>
	{!! Form::close() !!}
</div>

<script type="text/javascript">
		$(function(){
			$('#resetpass').click(function(){
				$('.signin').hide();
				$('.resetpass').show();
				return false;
			});
			$('#_signin').click(function(){
				$('.resetpass').hide();
				$('.signin').show();
				return false;
			});
		})
</script>

<div class="box-header with-border">
	<h3 class="box-title">Grafik Penderita Berdasar Wilayah</h3>
	<div class="pull-right col-md-2">
		<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#filterGraph4">Sortir Data &nbsp;<i class="fa fa-cogs"></i></button>
	</div>
</div>
<div class="box-body">
	<div id="graph_wilayah" style="margin: 0 auto"></div>
</div>

<script type="text/javascript">
	function graph4() {
		var case_type = $('#case_type4 option:selected');
		var title1 = 'Campak';
		var kasus = 'campak';
		if(case_type.val() != ''){
			title1 = case_type.text();
			kasus = case_type.val();
		}
		var title2 = '';
		if(case_type.val() == 'campak'){
			title2 = $('#jenis_kasus4 option:selected').text();
		}
		var gTitle = titleFilter4();
		var range = gTitle.wil+'<br>'+gTitle.time;
		var fd = {};
		fd['fdata'] = JSON.stringify({"jenis_data":$('#type4').val(),"jenis_kasus":$('#jenis_kasus4').val()});
		fd['dt'] = JSON.stringify($('#filter4').serializeJSON());
		$.ajax({
			method  : "POST",
			url     : BASE_URL+'api/analisa/graphWilayah/'+kasus,
			data    : fd,
			success: function(data){
				if (data.success==true) {
					var dt=data.response;
					graphWilayah(title1+' '+title2,range,dt.data);
					$('#filterGraph4').modal('hide');
				}else{
					messageAlert('warning', 'Peringatan', 'Data gagal salah');
					$('#filterGraph4').modal('hide');
				}
			}
		});
	};

	function graphWilayah(title,range,data){
		$('#graph_wilayah').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: 'Grafik Total Penderita '+title+' Per Wilayah'
			},
			subtitle: {
				text: range
			},
			xAxis: {
				type: 'category'
			},
			yAxis: {
				title: {
					text: 'Jumlah Penderita (orang)'
				}
			},
			lang: {
				drillUpText: '<< Kembali ke- {series.name}'
			},
			legend: {
				enabled: true,
				align: 'center',
				verticalAlign: 'bottom',
				labelFormatter: function() {
					var total=0, string;
					Highcharts.each(this.userOptions.data, function(p, i) {
						if (p.y) {
							tmp = parseInt(p.y | 0);
							total += tmp;
						}else if (p.y==0) {
							tmp = 0;
							total += tmp;
						}else {
							total += p[1];
						}
					});
					string = this.name+'<br>Jumlah Penderita: ' + total+' orang';
					return string;
				}
			},
			tooltip: {
				pointFormat: 'JUMLAH KASUS DI WILAYAH {point.name}: <b>{point.y} orang</b>'
			},
			plotOptions: {
				series: {
					borderWidth: 0,
					dataLabels: {
						enabled: true,
						format: '{point.y} orang',
					}
				}
			},
			credits:{
				enabled:false
			},
			series:data.data,
			drilldown: {
				drillUpButton: {
					relativeTo: 'spacingBox',
					position: {
						y: 22,
						x: 0
					},
					theme: {
						fill: 'white',
						'stroke-width': 1,
						stroke: 'silver',
						r: 0,
						states: {
							hover: {
								fill: '#a4edba'
							},
							select: {
								stroke: '#039',
								fill: '#a4edba'
							}
						}
					}
				},
				series: data.drilldown
			}
		});
	}

	$(function(){
		graph4();
		$('#filter4').validate({
			rules:{
				'case_type':'required',
				'filter_type':'required',
			},
			messages:{
				'case_type':'Kasus wajib diisi',
				'filter_type':'Wilayah wajib diisi',
			},
			submitHandler: function(){
				graph4();
			}
		});
	})
</script>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Pengaturan Tampilan Peta</h4>
		</div>
		<div class="modal-body">
			<div class="box box-success">
				{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'filter' , 'class' => 'form-horizontal']) !!}
				<div class="box-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="box-body">
								<div class="form-group">
									<label for="cases" class="col-sm-3 control-label">Jenis Kasus</label>
									<div class="col-sm-5">
										{!! Form::select('filter_kasus', array(null=>'All','campak'=>'Campak','difteri'=>'Difteri','tetanus'=>'Tetanus'), null, ['class' => 'form-control', 'id'=>'case_type']) !!}
									</div>
									<div class="col-sm-4" id="form_campak">
										{!! Form::select('filter_campak', array(null=>'Rutin dan KLB','2'=>'Rutin','1'=>'KLB'), null, ['class' => 'form-control', 'id'=>'campak_type']) !!}
									</div>
								</div>
								<div class="form-group">
									<label for="input1" class="col-sm-3 control-label">Jenis Data</label>
									<div class="col-sm-5">
										{!! Form::select('filter[range]', array(null=>'--Pilih--',), null, ['class' => 'form-control', 'id'=>"type_map"]) !!}
								</div>
								</div>
								<div class="form-group">
									<label for="input1" class="col-sm-3 control-label">Rentang Waktu</label>
									<div class="col-sm-5">
										{!! Form::select('filter[range]', array(null=>'All','1'=>'Hari','2'=>'Bulan','3'=>'Tahun'), null, ['class' => 'form-control', 'id'=>'range_time']) !!}
									</div>
								</div>
								<div class="form-group">
									<label for="input2" class="col-sm-3 control-label">Sejak</label>
									<div class="col-sm-2" style="padding-right: 0px;">
										<select name="from[day]" id="from_day" class="form-control" disabled="disabled">
											<option value="">--Pilih--</option>
											@for($i=1;$i<=31;$i++)
												<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
											@endfor
										</select>
									</div>
									<div class="col-sm-3" style="padding-right: 0px;">
										{!! Form::select('from[month]',
											array(null => '--Pilih--',
															'01'=>'Januari',
															'02'=>'Febuari',
															'03'=>'Maret',
															'04'=>'April',
															'05'=>'Mei',
															'06'=>'Juni',
															'07'=>'Juli',
															'08'=>'Agustus',
															'09'=>'September',
															'10'=>'Oktober',
															'11'=>'November',
															'12'=>'Desember',
															), null, ['class'=>'form-control','id'=>'from_month','disabled']) !!}
									</div>
									<div class="col-sm-3" style="padding-right: 0px;">
										<select name="from[year]" id="from_year" class="form-control" disabled="disabled">
											<option value="">--Pilih--</option>
											@for($i=2010;$i<=2020;$i++)
												<option value="{!! $i !!}">{!! $i !!}</option>
											@endfor
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Sampai</label>
									<div class="col-sm-2" style="padding-right: 0px;">
										<select name="to[day]" id="to_day" class="form-control" disabled="disabled">
											<option value="">--Pilih--</option>
											@for($i=1;$i<=31;$i++)
												<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
											@endfor
										</select>
									</div>
									<div class="col-sm-3" style="padding-right: 0px;">
										{!! Form::select('to[month]',
											array(null => '--Pilih--',
															'01'=>'Januari',
															'02'=>'Febuari',
															'03'=>'Maret',
															'04'=>'April',
															'05'=>'Mei',
															'06'=>'Juni',
															'07'=>'Juli',
															'08'=>'Agustus',
															'09'=>'September',
															'10'=>'Oktober',
															'11'=>'November',
															'12'=>'Desember',
															), null, ['class'=>'form-control','id'=>'to_month','disabled']) !!}
									</div>
									<div class="col-sm-3" style="padding-right: 0px;">
										<select name="to[year]" id="to_year" class="form-control" disabled="disabled">
											<option value="">--Pilih--</option>
											@for($i=2010;$i<=2020;$i++)
												<option value="{!! $i !!}">{!! $i !!}</option>
											@endfor
										</select>
									</div>
								</div>
								<!-- <div class="form-group">
									<label class="col-sm-3 control-label">Jenis Peta</label>
									<div class="col-sm-4">
										{!! Form::checkbox('Peta Jumlah Kasus', 'peta1', true,['id' => 'checkbox1']); !!} Peta Jumlah Kasus
									</div>
									<div class="col-sm-4">
										{!! Form::checkbox('Peta Persebaran Kasus', 'peta2', null,['id' => 'checkbox2']); !!} Peta Persebaran Kasus
									</div>
								</div> -->
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer" style="text-align: center">
					{!! Form::submit("Tampilkan", ['class' => 'btn btn-success','id'=>'submit_filter']) !!}
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#form_campak').hide();
	$(function(){
	$('#case_type').on('change',function(){
		var val = $(this).val();
		var dropdown = $('#type_map');
		if(val=='campak'){
			dropdown.empty(); // remove old options
			var option ={'--Pilih--':null,'Suspek Campak':'0','Campak (Lab,Epid, dan Klinis)':'2','Campak Lab':'3','Campak Epid':'4','Campak Klinis':'5','Rubella':'6'};
			$.each(option, function(key,value) {
			  dropdown.append($("<option></option>").attr("value", value).text(key));
			});
			dropdown.select2('val',null);
		}else if(val=='difteri'){
			dropdown.empty(); // remove old options
			var option ={'--Pilih--':null,'Suspek Difteri':'0','Difteri (Konfirm dan Probable)':'0','Difteri Konfirm':'2','Difteri Probable':'1'};
			$.each(option, function(key,value) {
			  dropdown.append($("<option></option>").attr("value", value).text(key));
			});
			dropdown.select2('val',null);
		}else if(val=='tetanus'){
			dropdown.empty(); // remove old options
			var option ={'--Pilih--':null,'Suspek TN':'0','Tetanus Neonatorum':'1'};
			$.each(option, function(key,value) {
			  dropdown.append($("<option></option>").attr("value", value).text(key));
			});
			dropdown.select2('val',null);
		}else{
			dropdown.empty(); // remove old options
			var option ={'--Pilih--':null};
			$.each(option, function(key,value) {
				dropdown.append($("<option></option>").attr("value", value).text(key));
			});
			dropdown.select2('val',null);
		}
		return false;
	});
});
$('#form_campak').hide();
$('#form_campak').val(null);


	$(function(){
		$('#range_time').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#from_day').removeAttr('disabled');
				$('#from_month').removeAttr('disabled');
				$('#from_year').removeAttr('disabled');
				$('#to_day').removeAttr('disabled');
				$('#to_month').removeAttr('disabled');
				$('#to_year').removeAttr('disabled');
			}else if(val=='2'){
				$('#from_day').attr('disabled','disabled').select2('val',null);
				$('#from_month').removeAttr('disabled');
				$('#from_year').removeAttr('disabled');
				$('#to_day').attr('disabled','disabled').select2('val',null);
				$('#to_month').removeAttr('disabled');
				$('#to_year').removeAttr('disabled');
			}else if(val=='3'){
				$('#from_day').attr('disabled','disabled').select2('val',null);
				$('#from_month').attr('disabled','disabled').select2('val',null);
				$('#from_year').removeAttr('disabled');
				$('#to_day').attr('disabled','disabled').select2('val',null);
				$('#to_month').attr('disabled','disabled').select2('val',null);
				$('#to_year').removeAttr('disabled');
			}else{
				$('#from_day').attr('disabled','disabled').select2('val',null);
				$('#from_month').attr('disabled','disabled').select2('val',null);
				$('#from_year').attr('disabled','disabled').select2('val',null);
				$('#to_day').attr('disabled','disabled').select2('val',null);
				$('#to_month').attr('disabled','disabled').select2('val',null);
				$('#to_year').attr('disabled','disabled').select2('val',null);
			}
			return false;
		});
	})
</script>

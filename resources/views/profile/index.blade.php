@extends('layouts.base')
@section('content')
<style media="screen">
.profile{
	text-align: right;
}
.form-group{
	border-bottom: 1px solid #f5f5f5;
	padding-bottom: 12px;
}
.coll{
	margin-top: 8px;
}
.collheader{
	text-align: center;
}
</style>
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">List Profile</h3>
		</div>
		<div class="box-body">
			@foreach($data['role'] AS $key=>$val)
			<div class='form-group'>
				<div class='row'>
					<div class="col-sm-6 profile">
						<label>
							<a href="{!! URL::to('setProfile').'/'.$val['code_role'].'/'.$val['id_faskes']; !!}">
								@if($val['code_role']=='pusat')
								{!! $val['nama_faskes'] !!}<br><i>{!! $val['alamat_faskes'] !!}</i>
								@else
								{!! $val['role_faskes'].' '.$val['nama_faskes'] !!}<br><i>{!! $val['alamat_faskes'] !!}</i>
								@endif
							</a>
						</label>
					</div>
					<div class="col-sm-6">
						<span>0 Admin</span>
						<a href="#" onclick="konfirmasiAdmin({!! $val['code_role'] !!},{!! $key !!});" class="btn btn-success">Jadikan Saya Admin</a>
						<a iRole='{!! $key !!}' class="btn btn-danger delprofile">Hapus Profil</a>
					</div>
				</div>
				@if(!empty($val['kemenkes']))
				<div class="box-group coll" id="pusat{!! $val['id_role'].'_'.$val['id_faskes']; !!}" >
					<div class="panel box box-success">
						<div class="box-header with-border collheader">
							<h5 class="box-title">
								<a data-toggle="collapse" data-parent="#pusat{!! $val['id_role'].'_'.$val['id_faskes']; !!}" href="#coll_pusat{!! $val['id_role'].'_'.$val['id_faskes']; !!}">
									{!! $val['nama_faskes']; !!}
								</a>
							</h5>
						</div>
						<div id="coll_pusat{!! $val['id_role'].'_'.$val['id_faskes']; !!}" class="panel-collapse collapse">
							<div class="box-body">
								<div class="box-group coll" id="lab{!! $val['id_role'].'_'.$val['id_faskes']; !!}" >
									<div class="panel box box-success">
										<div class="box-header with-border collheader">
											<h5 class="box-title">
												<a data-toggle="collapse" data-parent="#lab{!! $val['id_role'].'_'.$val['id_faskes']; !!}" href="#coll_lab{!! $val['id_role'].'_'.$val['id_faskes']; !!}">
													Laboratorium
												</a>
											</h5>
										</div>
										<div id="coll_lab{!! $val['id_role'].'_'.$val['id_faskes']; !!}" class="panel-collapse collapse">
											<div class="box-body">
												@foreach($val['lab'] AS $klab=>$vlab)
												<a href="{!! URL::to('setProfile').'/lab/'.$vlab->id !!}" type="button" class="btn btn-block btn-success btn-flat">Masuk sebagai petugas {!! $vlab->name; !!} </a>
												@endforeach
											</div>
										</div>
									</div>
								</div>
								@foreach($val['kemenkes'] AS $kprov=>$vprov)
								<div class="box-group coll" id="pusat_prov{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kprov; !!}" >
									<div class="panel box box-success">
										<div class="box-header with-border collheader">
											<h5 class="box-title">
												<a data-toggle="collapse" data-parent="#pusat_prov{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kprov; !!}" href="#coll_pusat_prov{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kprov; !!}">
													{!! $vprov['name_provinsi_faskes']; !!}
												</a>
											</h5>
										</div>
										<div id="coll_pusat_prov{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kprov; !!}" class="panel-collapse collapse">
											<div class="box-body">
												<a href="{!! URL::to('setProfile').'/provinsi/'.$kprov; !!}" type="button" class="btn btn-block btn-success btn-flat">Masuk sebagai petugas surveilans DKP {!! $vprov['name_provinsi_faskes']; !!}</a>
												@foreach($vprov['kabupaten'] AS $kkab=>$vkab)
												<div class="box-group coll" id="pusat_kab{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkab; !!}" >
													<div class="panel box box-success">
														<div class="box-header with-border collheader">
															<h5 class="box-title">
																<a data-toggle="collapse" data-parent="#pusat_kab{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkab; !!}" href="#coll_pusat_kab{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkab; !!}">
																	{!! $vkab['name_kabupaten_faskes']; !!}
																</a>
															</h5>
														</div>
														<div id="coll_pusat_kab{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkab; !!}" class="panel-collapse collapse">
															<div class="box-body">
																<a href="{!! URL::to('setProfile').'/kabupaten/'.$kkab !!}" type="button" class="btn btn-block btn-success btn-flat">Masuk sebagai petugas surveilans DKK {!! $vkab['name_kabupaten_faskes']; !!}</a>
																@if(!empty($vkab['rs']))
																<div class="box-group coll" id="pusat_rs{!! $val['id_role'].'_'.$val['id_faskes']; !!}" >
																	<div class="panel box box-success">
																		<div class="box-header with-border collheader">
																			<h5 class="box-title">
																				<a data-toggle="collapse" data-parent="#pusat_rs{!! $val['id_role'].'_'.$val['id_faskes']; !!}" href="#coll_pusat_rs{!! $val['id_role'].'_'.$val['id_faskes']; !!}">
																					RUMAH SAKIT
																				</a>
																			</h5>
																		</div>
																		<div id="coll_pusat_rs{!! $val['id_role'].'_'.$val['id_faskes']; !!}" class="panel-collapse collapse">
																			<div class="box-body">
																				@foreach($vkab['rs'] AS $krs=>$vrs)
																				<a href="{!! URL::to('setProfile').'/rs/'.$krs !!}" type="button" class="btn btn-block btn-success btn-flat">Masuk sebagai petugas surveilans {!! $vrs['name_rs_faskes']; !!}</a>
																				@endforeach
																			</div>
																		</div>
																	</div>
																</div>
																@endif
																@if(!empty($vkab['kecamatan']))
																@foreach($vkab['kecamatan'] AS $kkec=>$vkec)
																<div class="box-group coll" id="pusat_kec{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkec; !!}" >
																	<div class="panel box box-success">
																		<div class="box-header with-border collheader">
																			<h5 class="box-title">
																				<a data-toggle="collapse" data-parent="#pusat_kec{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkec; !!}" href="#coll_pusat_kec{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkec; !!}">
																					{!! $vkec['name_kecamatan_faskes']; !!}
																				</a>
																			</h5>
																		</div>
																		<div id="coll_pusat_kec{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkec; !!}" class="panel-collapse collapse">
																			<div class="box-body">
																				@foreach($vkec['puskesmas'] AS $kpkm=>$vpkm)
																				<a href="{!! URL::to('setProfile').'/puskesmas/'.$kpkm !!}" type="button" class="btn btn-block btn-success btn-flat">Masuk sebagai petugas surveilans {!! $vpkm['name_puskesmas_faskes']; !!}</a>
																				@endforeach
																			</div>
																		</div>
																	</div>
																</div>
																@endforeach
																@endif
															</div>
														</div>
													</div>
												</div>
												@endforeach
											</div>
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
				@endif
				@if(!empty($val['provinsi']))
				@foreach($val['provinsi'] AS $kprov=>$vprov)
				<div class="box-group coll" id="prov_prov{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kprov; !!}" >
					<div class="panel box box-success">
						<div class="box-header with-border collheader">
							<h5 class="box-title">
								<a data-toggle="collapse" data-parent="#prov_prov{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kprov; !!}" href="#coll_prov_prov{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kprov; !!}">
									{!! $vprov['name_provinsi_faskes']; !!}
								</a>
							</h5>
						</div>
						<div id="coll_prov_prov{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kprov; !!}" class="panel-collapse collapse">
							<div class="box-body">
								<a href="{!! URL::to('setProfile').'/provinsi/'.$kprov; !!}" type="button" class="btn btn-block btn-success btn-flat">Masuk sebagai petugas surveilans DKP {!! $vprov['name_provinsi_faskes']; !!}</a>
								@foreach($vprov['kabupaten'] AS $kkab=>$vkab)
								<div class="box-group coll" id="prov_kab{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkab; !!}" >
									<div class="panel box box-success">
										<div class="box-header with-border collheader">
											<h5 class="box-title">
												<a data-toggle="collapse" data-parent="#prov_kab{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkab; !!}" href="#coll_prov_kab{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkab; !!}">
													{!! $vkab['name_kabupaten_faskes']; !!}
												</a>
											</h5>
										</div>
										<div id="coll_prov_kab{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkab; !!}" class="panel-collapse collapse">
											<div class="box-body">
												<a href="{!! URL::to('setProfile').'/kabupaten/'.$kkab !!}" type="button" class="btn btn-block btn-success btn-flat">Masuk sebagai petugas surveilans DKK {!! $vkab['name_kabupaten_faskes']; !!}</a>
												@if(!empty($vkab['rs']))
												<div class="box-group coll" id="prov_rs{!! $val['id_role'].'_'.$val['id_faskes']; !!}" >
													<div class="panel box box-success">
														<div class="box-header with-border collheader">
															<h5 class="box-title">
																<a data-toggle="collapse" data-parent="#prov_rs{!! $val['id_role'].'_'.$val['id_faskes']; !!}" href="#coll_prov_rs{!! $val['id_role'].'_'.$val['id_faskes']; !!}">
																	RUMAH SAKIT
																</a>
															</h5>
														</div>
														<div id="coll_prov_rs{!! $val['id_role'].'_'.$val['id_faskes']; !!}" class="panel-collapse collapse">
															<div class="box-body">
																@foreach($vkab['rs'] AS $krs=>$vrs)
																<a href="{!! URL::to('setProfile').'/rs/'.$krs !!}" type="button" class="btn btn-block btn-success btn-flat">Masuk sebagai petugas surveilans {!! $vrs['name_rs_faskes']; !!}</a>
																@endforeach
															</div>
														</div>
													</div>
												</div>
												@endif
												@if(!empty($vkab['kecamatan']))
												@foreach($vkab['kecamatan'] AS $kkec=>$vkec)
												<div class="box-group coll" id="prov_kec{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkec; !!}" >
													<div class="panel box box-success">
														<div class="box-header with-border collheader">
															<h5 class="box-title">
																<a data-toggle="collapse" data-parent="#prov_kec{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkec; !!}" href="#coll_prov_kec{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkec; !!}">
																	{!! $vkec['name_kecamatan_faskes']; !!}
																</a>
															</h5>
														</div>
														<div id="coll_prov_kec{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkec; !!}" class="panel-collapse collapse">
															<div class="box-body">
																@foreach($vkec['puskesmas'] AS $kpkm=>$vpkm)
																<a href="{!! URL::to('setProfile').'/puskesmas/'.$kpkm !!}" type="button" class="btn btn-block btn-success btn-flat">Masuk sebagai petugas surveilans {!! $vpkm['name_puskesmas_faskes']; !!}</a>
																@endforeach
															</div>
														</div>
													</div>
												</div>
												@endforeach
												@endif
											</div>
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
				@endforeach
				@endif
				@if(!empty($val['kabupaten']))
				@foreach($val['kabupaten'] AS $kkab=>$vkab)
				<div class="box-group coll" id="kab_kab{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkab; !!}" >
					<div class="panel box box-success">
						<div class="box-header with-border collheader">
							<h5 class="box-title">
								<a data-toggle="collapse" data-parent="#kab_kab{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkab; !!}" href="#coll_kab_kab{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkab; !!}">
									{!! $vkab['name_kabupaten_faskes']; !!}
								</a>
							</h5>
						</div>
						<div id="coll_kab_kab{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkab; !!}" class="panel-collapse collapse">
							<div class="box-body">
								<a href="{!! URL::to('setProfile').'/kabupaten/'.$kkab !!}" type="button" class="btn btn-block btn-success btn-flat">Masuk sebagai petugas surveilans DKK {!! $vkab['name_kabupaten_faskes']; !!}</a>
								@if(!empty($vkab['rs']))
								<div class="box-group coll" id="kab_rs{!! $val['id_role'].'_'.$val['id_faskes']; !!}" >
									<div class="panel box box-success">
										<div class="box-header with-border collheader">
											<h5 class="box-title">
												<a data-toggle="collapse" data-parent="#kab_rs{!! $val['id_role'].'_'.$val['id_faskes']; !!}" href="#coll_kab_rs{!! $val['id_role'].'_'.$val['id_faskes']; !!}">
													RUMAH SAKIT
												</a>
											</h5>
										</div>
										<div id="coll_kab_rs{!! $val['id_role'].'_'.$val['id_faskes']; !!}" class="panel-collapse collapse">
											<div class="box-body">
												@foreach($vkab['rs'] AS $krs=>$vrs)
												<a href="{!! URL::to('setProfile').'/rs/'.$krs !!}" type="button" class="btn btn-block btn-success btn-flat">Masuk sebagai petugas surveilans {!! $vrs['name_rs_faskes']; !!}</a>
												@endforeach
											</div>
										</div>
									</div>
								</div>
								@endif
								@if(!empty($vkab['kecamatan']))
								@foreach($vkab['kecamatan'] AS $kkec=>$vkec)
								<div class="box-group coll" id="kab_kec{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkec; !!}" >
									<div class="panel box box-success">
										<div class="box-header with-border collheader">
											<h5 class="box-title">
												<a data-toggle="collapse" data-parent="#kab_kec{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkec; !!}" href="#coll_kab_kec{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkec; !!}">
													{!! $vkec['name_kecamatan_faskes']; !!}
												</a>
											</h5>
										</div>
										<div id="coll_kab_kec{!! $val['id_role'].'_'.$val['id_faskes'].'_'.$kkec; !!}" class="panel-collapse collapse">
											<div class="box-body">
												@foreach($vkec['puskesmas'] AS $kpkm=>$vpkm)
												<a href="{!! URL::to('setProfile').'/puskesmas/'.$kpkm !!}" type="button" class="btn btn-block btn-success btn-flat">Masuk sebagai petugas surveilans {!! $vpkm['name_puskesmas_faskes']; !!}</a>
												@endforeach
											</div>
										</div>
									</div>
								</div>
								@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				@endforeach
				@endif
			</div>
			@endforeach
		</div>
		<div class="box-footer" style="text-align: center">
			<a href="{!! URL::to('role/level') !!}" class="btn btn-success">Tambah Profil</a>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('.delprofile').on('click',function(){
			$('.modaldelete').modal('toggle');
			var id_role = $(this).attr('iRole');
			var action = '{!! url('profile/delete'); !!}/'+id_role;
			$('.delete-yes').attr('action',action);
		});
		$('.delete-yes').on('click', function(){
			$('.modaldelete').modal('toggle');
			var action  = $(this).attr('action');
			$.ajax({
				method  : "POST",
				url     : action,
				data    : null,
				dataType: "json",
				beforeSend: function(){
					startProcess();
				},
			})
			.done(function(response){
				if(response.success){
					setTimeout( function(){
						window.location.reload();
					}, 0);
				}
			});
			return false;
		});
	})
</script>
@endsection

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Identitas penderita</h3>
	</div>
	<div class='form-horizontal'>
		<div class="box-body">
			<div class="form-group">
				{!! Form::label('name', 'Nama Penderita', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-7">
					<div class="input-group">
						{!! Form::text('dp[name]', null, ['class' => 'form-control','id'=>'name','placeholder'=>'Nama Penderita']) !!}
						{!! Form::hidden('id_pasien', null, ['class' => 'form-control','id'=>'id_pasien']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('nik', 'NIK', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-4">
					{!! Form::text('dp[nik]', null, ['class' => 'form-control','id'=>'nik','placeholder'=>'NIK']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('name', 'Nama Orang Tua', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-7">
					<div class="input-group">
						{!! Form::text('df[name]', null, ['class' => 'form-control','placeholder'=>'Nama Orang Tua','id'=>'nama_ortu']) !!}
						{!! Form::hidden('id_family', null, ['class' => 'form-control','id'=>'id_family']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('jenis_kelamin', 'Jenis Kelamin', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-4">
					<div class="input-group">
						{!! Form::select('dp[jenis_kelamin]', array(null=>'--Pilih--','L'=>'Laki-laki','P'=>'Perempuan','T'=>'Tidak Jelas'), null, ['class' => 'form-control', 'id'=>'jenis_kelamin']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label">
					<input type="radio" name="tgl_lahir" id="tgl_lahir1" value="1" class="col-sm-1">&nbsp;Tanggal Lahir
				</label>
				<div class="col-sm-4">
					{!! Form::text('dp[tgl_lahir]', null, ['class' => 'form-control tgl_lahir1 datemax','placeholder'=>'Tanggal Lahir','id'=>'tgl_lahir','disabled']) !!}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label">
					<input type="radio" name="tgl_lahir" id="tgl_lahir2" value="2" class="col-sm-1">&nbsp;Umur
				</label>
				<div class="col-sm-7">
					<div class="row">
						<div class="col-sm-4" style="padding-right: 0px;">
							<div class="input-group">
								{!! Form::text('dp[umur_thn]', null, ['class' => 'form-control tgl_lahir2','disabled','id'=>'umur']) !!}
								<span class="input-group-addon">Thn</span>
							</div>
						</div>
						<div class="col-sm-4" style="padding-right: 0px;">
							<div class="input-group">
								{!! Form::text('dp[umur_bln]', null, ['class' => 'form-control tgl_lahir2','disabled','id'=>'umur_bln']) !!}
								<span class="input-group-addon">Bln</span>
							</div>
						</div>
						<div class="col-sm-4" style="padding-right: 0px;">
							<div class="input-group">
								{!! Form::text('dp[umur_hari]', null, ['class' => 'form-control tgl_lahir2','disabled','id'=>'umur_hari']) !!}
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Provinsi', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::select('dp[code_provinsi]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control','id'=>'id_provinsi_pasien','onchange'=>"getKabupaten('_pasien')"]) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kabupaten', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::select('dp[code_kabupaten]', array(null=>'Pilih Kabupaten'), null, ['class' => 'form-control', 'id'=>'id_kabupaten_pasien','onchange'=>"getKecamatan('_pasien')"]) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kecamatan', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::select('dp[code_kecamatan]', array(null=>'Pilih Kecamatan'), null, ['class' => 'form-control','id'=>'id_kecamatan_pasien','onchange'=>"getKelurahan('_pasien')"]) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kelurahan', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					<div class="input-group">
						{!! Form::select('dp[code_kelurahan]', array(null=>'Pilih Kelurahan'), null, ['class' => 'form-control','id'=>'id_kelurahan_pasien','onchange'=>'getEpid(); getEpidKlb();']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('alamat', 'Alamat', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-7">
					{!! Form::textarea('dp[alamat]',null, ['class' => 'form-control','id'=>'alamat','rows'=>'3','placeholder'=>'Hanya diisi nama jalan, no. rumah dan no. RT - RW']) !!}
				</div>
			</div>
			@if($role = Helper::role())
			<div class="form-group">
				{!! Form::label('nama_faskes', 'Nama Faskes saat periksa', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::text('', strtoupper($role->name_role).' '.$role->name_faskes, ['class' => 'form-control','id'=>'nama_faskes','placeholder'=>'Nama Faskes saat periksa','disabled']) !!}
				</div>
			</div>
			@endif
			<div class="form-group">
				{!! Form::label('no_epid', 'No.Epidemologi', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::text('dc[no_epid]', null, ['class' => 'form-control','id'=>'no_epid','placeholder'=>'No.Epidemologi','readonly']) !!}
				</div>
			</div>
			<div class="form-group hide" id="klb">
				{!! Form::label('no_epid_klb', 'No.Epidemologi KLB', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::text('dc[no_epid_klb]', null, ['class' => 'form-control','id'=>'no_epid_klb','placeholder'=>'No.Epidemologi KLB','readonly']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('no_epid_lama', 'No.Epidemologi lama', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::text('dc[no_epid_lama]', null, ['class' => 'form-control','id'=>'no_epid_lama','placeholder'=>'No.Epidemologi Lama']) !!}
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('#nik').mask('999999999999999999');

		$("input[name='tgl_lahir']").on('ifChecked',function(){
			var val = $(this).val();
			if(val=='1'){
				active('tgl_lahir','1');
				deactive('tgl_lahir','2');
			}else{
				active('tgl_lahir','2');
				deactive('tgl_lahir','1');
			}
		});

		$('#name').autocomplete({
			source: BASE_URL+'pasien/getData',
			minLength:3,
			focus: function(event, ui){
				$('#name').val(ui.item.name);
				return false;
			},
			select:function(evt, ui){
				if(ui){
					var dt = ui.item;
					$('#id_pasien').val(dt.id);
					$('#id_family').val(dt.id_family);
					$('#nik').val(dt.nik);
					$('#nama_ortu').val(dt.nama_ortu);
					$('#jenis_kelamin').select2('val',dt.jenis_kelamin);
					if(isset(dt.tgl_lahir)){
						$('#tgl_lahir').val(dt.tgl_lahir).removeAttr('disabled');
					}
					if(isset(dt.umur_thn) || isset(dt.umur_bln) || isset(dt.umur_hari)){
						$('#umur').val(dt.umur_thn).removeAttr('disabled');
						$('#umur_bln').val(dt.umur_bln).removeAttr('disabled');
						$('#umur_hari').val(dt.umur_hari).removeAttr('disabled');
					}
					$('#id_provinsi_pasien').val(dt.code_provinsi).select2();
					$('#id_kabupaten_pasien').empty().append('<option value="'+dt.code_kabupaten+'">'+dt.name_kabupaten+'</option>').select2();
					$('#id_kecamatan_pasien').empty().append('<option value="'+dt.code_kecamatan+'">'+dt.name_kecamatan+'</option>').select2();
					$('#id_kelurahan_pasien').empty().append('<option value="'+dt.code_kelurahan+'">'+dt.name_kelurahan+'</option>').trigger('change');
					$('#alamat').val(dt.alamat);
				}
				return false;
			},
			messages:{
				noResults: function(argument){
					messageAlert("danger", "Info", "Data pasien tidak ditemukan.");
					$('#no_epid').val(null);
					$('#id_pasien').val(null);
					$('#id_family').val(null);
					$('#nik').val(null);
					$('#nama_ortu').val(null);
					$('#jenis_kelamin').select2('val','');
					$('#tgl_lahir').val(null).attr('disabled','disabled');
					$('#umur').val(null).attr('disabled','disabled');
					$('#umur_bln').val(null).attr('disabled','disabled');
					$('#umur_hari').val(null).attr('disabled','disabled');
					$('#id_provinsi_pasien').val('').select2();
					$('#id_kabupaten_pasien').empty().append('<option value="">Pilih Kabupaten</option>').select2();
					$('#id_kecamatan_pasien').empty().append('<option value="">Pilih Kecamatan</option>').select2();
					$('#id_kelurahan_pasien').empty().append('<option value="">Pilih Kelurahan</option>').select2();
					$('#alamat').val(null);
					return false;
				}
			}
		})
		.data("ui-autocomplete")._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append("<a>"+ item.name +"<br><small>"+
				"NIK: <i>"+ item.nik +"</i><br>"+
				"Nama orang tua: <i>"+ item.nama_ortu +"</i><br>"+
				"Alamat: "+item.full_address+"</small></a>")
			.appendTo( ul );
		};
	})
</script>

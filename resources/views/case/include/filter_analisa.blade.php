<div class="box box-success">
	{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'filter_analisa' , 'class' => 'form-horizontal']) !!}
	<div class="box-body">
		<div class="col-sm-5">
			<div class="form-group">
				<label class="col-sm-3 control-label">Rentang Waktu</label>
				<div class="col-sm-3">
					{!! Form::select('filter[range]', array(null=>'All','1'=>'Hari','2'=>'Bulan','3'=>'Tahun'), null, ['class' => 'form-control', 'id'=>'range_time_analisa']) !!}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Sejak</label>
				<div class="col-sm-3" >
					<select class="form-control" id="from_day_analisa" name="from[day]">
						<option value="">--Pilih--</option>
						@for($i=1;$i<=31;$i++)
						<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
						@endfor
					</select>
				</div>
				<div class="col-sm-3" >
					{!! Form::select('from[month]',Helper::getMonth(), null, ['class'=>'form-control','id'=>'from_month_analisa']) !!}
				</div>
				<div class="col-sm-3" >
					<select class="form-control" id="from_year_analisa" name="from[year]">
						<option value="">--Pilih--</option>
						@for($i=2010;$i<=date('Y')+5;$i++)
						<option value="{!! $i !!}">{!! $i !!}</option>
						@endfor
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Sampai</label>
				<div class="col-sm-3" >
					<select class="form-control" id="to_day_analisa" name="to[day]">
						<option value="">--Pilih--</option>
						@for($i=1;$i<=31;$i++)
						<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
						@endfor
					</select>
				</div>
				<div class="col-sm-3" >
					{!! Form::select('to[month]',Helper::getMonth(), null, ['class'=>'form-control','id'=>'to_month_analisa']) !!}
				</div>
				<div class="col-sm-3" >
					<select class="form-control" id="to_year_analisa" name="to[year]">
						<option value="">--Pilih--</option>
						@for($i=2010;$i<=2020;$i++)
						<option value="{!! $i !!}">{!! $i !!}</option>
						@endfor
					</select>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<label class="col-sm-6 control-label">Filter</label>
				<div class="col-sm-6">
					{!! Form::select('filter_type', array(null=>'--Pilih Filter--','1'=> 'Berdasarkan Alamat Pasien','2'=>'Data Instansi'), null, ['class' => 'form-control','id'=>'filter_type_daftar_kasus_analisa']) !!}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-6 control-label">Provinsi</label>
				<div class="col-sm-6">
					{!! Form::select('filter[id_provinsi]', [null=>'Pilih Provinsi']+Helper::getProvince(), null, ['class' => 'form-control alamat','id'=>'id_provinsi_filter_analisa']) !!}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-6 control-label">Kabupaten</label>
				<div class="col-sm-6">
					{!! Form::select('filter[id_kabupaten]', [null=>'Pilih Kabupaten'], null, ['class' => 'form-control alamat','id'=>'id_kabupaten_filter_analisa']) !!}
				</div>
			</div>
			<div class="form-group kec_analisa">
				<label class="col-sm-6 control-label">Kecamatan</label>
				<div class="col-sm-6">
					{!! Form::select('filter[id_kecamatan]', [null=>'Pilih Kecamatan'], null, ['class' => 'form-control alamat','id'=>'id_kecamatan_filter_analisa']) !!}
				</div>
			</div>
			<div class="form-group alpasien_analisa hide">
				<label class="col-sm-6 control-label">Kelurahan</label>
				<div class="col-sm-6">
					{!! Form::select('filter[id_kelurahan]', [null=>'Pilih Kelurahan/Desa'], null, ['class' => 'form-control alamat','id'=>'id_kelurahan_filter_analisa']) !!}
				</div>
			</div>
			<div class="form-group instansi_analisa hide">
				<label class="col-sm-6 control-label">Wilayah Kerja Puskesmas</label>
				<div class="col-sm-6">
					{!! Form::select('filter[id_wilayah_kerja]', [null=>'Pilih Wilayah Kerja Puskesmas'], null, ['class' => 'form-control alamat','id'=>'id_wilayah_kerja_filter_analisa']) !!}
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<h4 class="text-center" style="font-weight: 600">Instansi</h4>
			<hr>
			<div class="form-group">
				<label class="col-sm-4 control-label">Faskes</label>
				<div class="col-sm-8">
					<?php
						$faskes[null] = '--Pilih--';
						if(Helper::role()->code_role != 'puskesmas'){
							$faskes[1] = 'Rumah Sakit';
						}
						if(Helper::role()->code_role != 'rs'){
							$faskes[2] = 'Puskesmas';
						}
					?>
					{!! Form::select('faskes_filter', $faskes, null, ['class' => 'form-control alamat','id'=>'faskes_filter_analisa']) !!}
				</div>
			</div>
			@if(Helper::role()->code_role != 'puskesmas')
			<div class="form-group">
				<label class="col-sm-4 control-label">Rumah Sakit</label>
				<div class="col-sm-8">
					{!! Form::select('filter[id_rumah_sakit]', [null=>'Pilih Rumah Sakit'], null, ['class' => 'form-control alamat','id'=>'rs_filter_analisa']) !!}
				</div>
			</div>
			@endif
			@if(Helper::role()->code_role != 'rs')
			<div class="form-group">
				<label class="col-sm-4 control-label">Puskesmas</label>
				<div class="col-sm-8">
					{!! Form::select('filter[id_puskesmas]', [null=>'Pilih Puskesmas'], null, ['class' => 'form-control alamat','id'=>'puskesmas_filter_analisa']) !!}
				</div>
			</div>
			@endif
		</div>
	</div>
	<div class="box-footer" style="text-align: center">
		{!! Form::submit("Tampilkan", ['class' => 'btn btn-success','id'=>'submit_filter_analisa']) !!}
		{!! Form::close() !!}
		{!! Form::button("Export Excel", ['class' => 'btn btn-info','id'=>'export_excel_analisa']) !!}
	</div>
</div>

<script type="text/javascript">
	function activeexport_analisa(){
		var type = $('#filter_type_daftar_kasus_analisa').val();
		if(type=='2'){
			var faskes = "{!! Helper::role()->id_faskes; !!}";
			var role = "{!! Helper::role()->id_role; !!}";
			if(role=='1'){
				val = $('#puskesmas_filter_analisa').val();
			}else if(role=='2'){
				val = $('#rs_filter_analisa').val();
			}else if(role=='6'){
				val = $('#id_kabupaten_filter_analisa').val();
			}else if(role=='5'){
				val = $('#id_provinsi_filter_analisa').val();
			}else{
				val = '1';
			}
			if(val==faskes){
				$('#export_excel_analisa').removeAttr('disabled');
			}else{
				$('#export_excel_analisa').attr('disabled','disabled');
			}
			return false;
		}
	}

	$(function(){
		activeexport_analisa();
		$('#from_day_analisa,#from_month_analisa,#from_year_analisa,#to_day_analisa,#to_month_analisa,#to_year_analisa,#id_provinsi_filter_analisa,#id_kabupaten_filter_analisa,#rs_filter_analisa,#id_kecamatan_filter_analisa,#id_kelurahan_filter_analisa,#puskesmas_filter_analisa,#id_wilayah_kerja_filter_analisa,#export_excel_analisa,#faskes_filter_analisa').attr('disabled', 'disabled');

		$('#range_time_analisa').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#from_day_analisa, #from_month_analisa, #from_year_analisa, #to_day_analisa, #to_month_analisa, #to_year_analisa').removeAttr('disabled');
			}else if(val=='2'){
				$('#from_month_analisa, #from_year_analisa, #to_month_analisa, #to_year_analisa').removeAttr('disabled');
				$('#from_day_analisa, #to_day_analisa').attr('disabled','disabled').val(null).trigger('change');
			}else if(val=='3'){
				$('#from_year_analisa, #to_year_analisa').removeAttr('disabled');
				$('#from_day_analisa, #from_month_analisa, #to_day_analisa, #to_month_analisa').attr('disabled','disabled').val(null).trigger('change');
			}else{
				$('#from_day_analisa, #from_month_analisa, #from_year_analisa, #to_day_analisa, #to_month_analisa, #to_year_analisa').attr('disabled','disabled').val(null).trigger('change');
			}
			return false;
		});

		$('#export_excel_analisa').on('click',function(){
			var params = JSON.stringify($('#filter_analisa').serializeJSON());
			var url = "{!! url('export/'.Request::segment(2)) !!}/"+params;
			window.open(url, '_blank');
		});

		$('#faskes_filter_analisa').on('change',function(){
			var val = $(this).val();
			$('#rs_filter_analisa, #puskesmas_filter_analisa').val('').trigger('change');
			$('#rs_filter_analisa, #puskesmas_filter_analisa').attr('disabled','');
			if (val==1) {
				getRs('_filter_analisa');
				$('#rs_filter_analisa').removeAttr('disabled');
			}else if(val==2){
				getPuskesmas('_filter_analisa');
				$('#puskesmas_filter_analisa').removeAttr('disabled');
			}
		});

		$('#filter_type_daftar_kasus_analisa').on('change',function(){
			var val = $('#filter_type_daftar_kasus_analisa').val();
			$('.alamat_analisa').attr('disabled','disabled').val(null).trigger('change');
			$('#id_provinsi_filter_analisa').removeAttr('disabled');
			if(val=='1'){
				$('#faskes_filter_analisa').attr('disabled','');
				$('.alpasien_analisa').removeClass('hide');
				$('.instansi_analisa').addClass('hide');
				@if(Helper::role()->code_role=='rs')
				$('.kec_analisa').removeClass('hide');
				@endif
				$('#id_provinsi_filter_analisa').attr('name',"filter[code_provinsi_pasien]");
				$('#id_kabupaten_filter_analisa').attr('name',"filter[code_kabupaten_pasien]");
				$('#id_kecamatan_filter_analisa').attr('name',"filter[code_kecamatan_pasien]");
				$('#id_kelurahan_filter_analisa').attr('name',"filter[code_kelurahan_pasien]");
				$('#export_excel_analisa').removeAttr('disabled');
			}else if(val=='2'){
				$('#faskes_filter_analisa').removeAttr('disabled');
				$('.alpasien_analisa').addClass('hide');
				$('.instansi_analisa').removeClass('hide');
				@if(Helper::role()->code_role=='rs')
				$('.kec_analisa').addClass('hide');
				@endif
				$('#id_provinsi_filter_analisa').attr('name',"filter[code_provinsi_faskes]");
				$('#id_kabupaten_filter_analisa').attr('name',"filter[code_kabupaten_faskes]");
				$('#rs_filter_analisa').attr('name',"filter[rs_id]");
				$('#id_kecamatan_filter_analisa').attr('name',"filter[code_kecamatan_faskes]");
				$('#puskesmas_filter_analisa').attr('name',"filter[puskesmas_id]");
				$('#id_wilayah_kerja_filter_analisa').attr('name',"filter[id_wilayah_kerja]");
				@if(Helper::role()->code_role != 'pusat')
					$('#export_excel_analisa').attr('disabled','disabled');
				@endif
			}
		});

		$('#id_provinsi_filter_analisa').on('change',function(){
			getKabupaten('_filter_analisa');
			activeexport_analisa();
			return false;
		});
		$('#id_kabupaten_filter_analisa').on('change',function(){
			var val = $('#filter_type_daftar_kasus_analisa').val();
			getKecamatan('_filter_analisa');
			activeexport_analisa();
			return false;
		});
		$('#id_kecamatan_filter_analisa').on('change',function(){
			var val = $('#filter_type_daftar_kasus_analisa').val();
			if(val=='1'){
				getKelurahan('_filter_analisa');
			}
			activeexport_analisa();
			return false;
		});
		$('#rs_filter_analisa').on('change',function(){
			activeexport_analisa();
			return false;
		});
		$('#puskesmas_filter_analisa').on('change',function(){
			getWilayahKerja('_filter_analisa');
			activeexport_analisa();
			return false;
		});
	});
</script>
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Unggah Data Individual Kasus</h3>
		<a href="{!! url('case/'.Request::segment(2).'/contoh_format'); !!}" class="btn btn-info pull-right" type="button">Unduh Template Impor &nbsp;<i class="fa fa-download"></i></a>
	</div>
	<div class="box-body">
		<div class="col-sm-12">
			{!! Form::file("import", ['class' => 'file-loading','id'=>'import','multiple', 'files' => true, 'data-show-preview' => true,'data-allowed-file-extensions'=>'["csv","xls","xlsx"]']) !!}
			<div id="errorBlock" class="help-block"></div>
		</div>
	</div>
</div>
<script>
	$(document).on('ready', function() {
		$("#import").fileinput({
			uploadUrl: BASE_URL+'case/'+'{!! Request::segment(2); !!}'+'/import',
			maxFilePreviewSize: 10240
		}).on('fileuploaded', function(e, params) {
			$('#table').DataTable().draw();
		});
	});
</script>

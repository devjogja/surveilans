<div class="box box-success">
	{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'filter_dk' , 'class' => 'form-horizontal']) !!}
	{!! Form::hidden('code_role',Helper::role()->code_role) !!}
	{!! Form::hidden('id_faskes',Helper::role()->id_faskes) !!}
	{!! Form::hidden('code_faskes',Helper::role()->code_faskes) !!}
	<div class="box-body">
		<div class="col-sm-5">
			<div class="form-group">
				<label class="col-sm-3 control-label">Rentang Waktu</label>
				<div class="col-sm-3">
					{!! Form::select('filter[range]', array(null=>'All','1'=>'Hari','2'=>'Bulan','3'=>'Tahun'), null, ['class' => 'form-control', 'id'=>'range_time_dk']) !!}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Sejak</label>
				<div class="col-sm-3" >
					<select class="form-control" id="from_day_dk" name="from[day]">
						<option value="">--Pilih--</option>
						@for($i=1;$i<=31;$i++)
						<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
						@endfor
					</select>
				</div>
				<div class="col-sm-3" >
					{!! Form::select('from[month]',Helper::getMonth(), null, ['class'=>'form-control','id'=>'from_month_dk']) !!}
				</div>
				<div class="col-sm-3" >
					<select class="form-control" id="from_year_dk" name="from[year]">
						<option value="">--Pilih--</option>
						@for($i=2010;$i<=date('Y')+5;$i++)
						<option value="{!! $i !!}">{!! $i !!}</option>
						@endfor
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Sampai</label>
				<div class="col-sm-3" >
					<select class="form-control" id="to_day_dk" name="to[day]">
						<option value="">--Pilih--</option>
						@for($i=1;$i<=31;$i++)
						<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
						@endfor
					</select>
				</div>
				<div class="col-sm-3" >
					{!! Form::select('to[month]',Helper::getMonth(), null, ['class'=>'form-control','id'=>'to_month_dk']) !!}
				</div>
				<div class="col-sm-3" >
					<select class="form-control" id="to_year_dk" name="to[year]">
						<option value="">--Pilih--</option>
						@for($i=2010;$i<=2020;$i++)
						<option value="{!! $i !!}">{!! $i !!}</option>
						@endfor
					</select>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<label class="col-sm-6 control-label">Filter</label>
				<div class="col-sm-6">
					{!! Form::select('filter_type', array(null=>'--Pilih Filter--','1'=> 'Berdasarkan Alamat Pasien','2'=>'Data Instansi'), null, ['class' => 'form-control','id'=>'filter_type_daftar_kasus_dk']) !!}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-6 control-label">Provinsi</label>
				<div class="col-sm-6">
					{!! Form::select('filter[id_provinsi]', [null=>'Pilih Provinsi']+Helper::getProvince(), null, ['class' => 'form-control alamat','id'=>'id_provinsi_filter_dk']) !!}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-6 control-label">Kabupaten</label>
				<div class="col-sm-6">
					{!! Form::select('filter[id_kabupaten]', [null=>'Pilih Kabupaten'], null, ['class' => 'form-control alamat','id'=>'id_kabupaten_filter_dk']) !!}
				</div>
			</div>
			<div class="form-group kec_dk">
				<label class="col-sm-6 control-label">Kecamatan</label>
				<div class="col-sm-6">
					{!! Form::select('filter[id_kecamatan]', [null=>'Pilih Kecamatan'], null, ['class' => 'form-control alamat','id'=>'id_kecamatan_filter_dk']) !!}
				</div>
			</div>
			<div class="form-group alpasien_dk hide">
				<label class="col-sm-6 control-label">Kelurahan</label>
				<div class="col-sm-6">
					{!! Form::select('filter[id_kelurahan]', [null=>'Pilih Kelurahan/Desa'], null, ['class' => 'form-control alamat','id'=>'id_kelurahan_filter_dk']) !!}
				</div>
			</div>
			<div class="form-group instansi_dk hide">
				<label class="col-sm-6 control-label">Wilayah Kerja Puskesmas</label>
				<div class="col-sm-6">
					{!! Form::select('filter[id_wilayah_kerja]', [null=>'Pilih Wilayah Kerja Puskesmas'], null, ['class' => 'form-control alamat','id'=>'id_wilayah_kerja_filter_dk']) !!}
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<h4 class="text-center" style="font-weight: 600">Instansi</h4>
			<hr>
			<div class="form-group">
				<label class="col-sm-4 control-label">Faskes</label>
				<div class="col-sm-8">
					<?php
						$faskes[null] = '--Pilih--';
						if(Helper::role()->code_role != 'puskesmas'){
							$faskes[1] = 'Rumah Sakit';
						}
						if(Helper::role()->code_role != 'rs'){
							$faskes[2] = 'Puskesmas';
						}
					?>
					{!! Form::select('faskes_filter', $faskes, null, ['class' => 'form-control alamat','id'=>'faskes_filter_dk']) !!}
				</div>
			</div>
			@if(Helper::role()->code_role != 'puskesmas')
			<div class="form-group">
				<label class="col-sm-4 control-label">Rumah Sakit</label>
				<div class="col-sm-8">
					{!! Form::select('filter[id_rumah_sakit]', [null=>'Pilih Rumah Sakit'], null, ['class' => 'form-control alamat','id'=>'rs_filter_dk']) !!}
				</div>
			</div>
			@endif
			@if(Helper::role()->code_role != 'rs')
			<div class="form-group">
				<label class="col-sm-4 control-label">Puskesmas</label>
				<div class="col-sm-8">
					{!! Form::select('filter[id_puskesmas]', [null=>'Pilih Puskesmas'], null, ['class' => 'form-control alamat','id'=>'puskesmas_filter_dk']) !!}
				</div>
			</div>
			@endif
		</div>
	</div>
	<div class="box-footer" style="text-align: center">
		{!! Form::submit("Tampilkan", ['class' => 'btn btn-success','id'=>'submit_filter_dk']) !!}
		{!! Form::close() !!}
		{!! Form::button("Export Excel", ['class' => 'btn btn-info','id'=>'export_excel_dk']) !!}
	</div>
</div>

<script type="text/javascript">
	function activeexport_dk(){
		var type = $('#filter_type_daftar_kasus_dk').val();
		if(type=='2'){
			var faskes = "{!! Helper::role()->id_faskes; !!}";
			var role = "{!! Helper::role()->id_role; !!}";
			if(role=='1'){
				val = $('#puskesmas_filter_dk').val();
			}else if(role=='2'){
				val = $('#rs_filter_dk').val();
			}else if(role=='6'){
				val = $('#id_kabupaten_filter_dk').val();
			}else if(role=='5'){
				val = $('#id_provinsi_filter_dk').val();
			}else{
				val = '1';
			}
			if(val==faskes){
				// $('#export_excel_dk').removeAttr('disabled');
			}else{
				// $('#export_excel_dk').attr('disabled','disabled');
			}
			return false;
		}
	}

	$(function(){
		$('#from_day_dk,#from_month_dk,#from_year_dk,#to_day_dk,#to_month_dk,#to_year_dk,#id_provinsi_filter_dk,#id_kabupaten_filter_dk,#rs_filter_dk,#id_kecamatan_filter_dk,#id_kelurahan_filter_dk,#puskesmas_filter_dk,#id_wilayah_kerja_filter_dk,#faskes_filter_dk').attr('disabled', 'disabled');

		$('#range_time_dk').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#from_day_dk, #from_month_dk, #from_year_dk, #to_day_dk, #to_month_dk, #to_year_dk').removeAttr('disabled');
			}else if(val=='2'){
				$('#from_month_dk, #from_year_dk, #to_month_dk, #to_year_dk').removeAttr('disabled');
				$('#from_day_dk, #to_day_dk').attr('disabled','disabled').val(null).trigger('change');
			}else if(val=='3'){
				$('#from_year_dk, #to_year_dk').removeAttr('disabled');
				$('#from_day_dk, #from_month_dk, #to_day_dk, #to_month_dk').attr('disabled','disabled').val(null).trigger('change');
			}else{
				$('#from_day_dk, #from_month_dk, #from_year_dk, #to_day_dk, #to_month_dk, #to_year_dk').attr('disabled','disabled').val(null).trigger('change');
			}
			return false;
		});

		$('#export_excel_dk').on('click',function(){
			var params = JSON.stringify($('#filter_dk').serializeJSON());
			var url = "{!! url('export/'.Request::segment(2)) !!}/"+params;
			window.open(url, '_blank');
		});

		$('#faskes_filter_dk').on('change',function(){
			var val = $(this).val();
			$('#rs_filter_dk, #puskesmas_filter_dk').val('').trigger('change');
			$('#rs_filter_dk, #puskesmas_filter_dk').attr('disabled','');
			if (val==1) {
				getRs('_filter_dk');
				$('#rs_filter_dk').removeAttr('disabled');
			}else if(val==2){
				getPuskesmas('_filter_dk');
				$('#puskesmas_filter_dk').removeAttr('disabled');
			}
		});

		$('#filter_type_daftar_kasus_dk').on('change',function(){
			var val = $('#filter_type_daftar_kasus_dk').val();
			$('#faskes_filter_dk').val('').change();
			$('.alamat_dk').attr('disabled','disabled').val(null).trigger('change');
			$('#id_provinsi_filter_dk').removeAttr('disabled');
			if(val=='1'){
				$('#faskes_filter_dk').attr('disabled','');
				$('.alpasien_dk').removeClass('hide');
				$('.instansi_dk').addClass('hide');
				@if(Helper::role()->code_role=='rs')
				$('.kec_dk').removeClass('hide');
				@endif
				$('#id_provinsi_filter_dk').attr('name',"filter[code_provinsi_pasien]");
				$('#id_kabupaten_filter_dk').attr('name',"filter[code_kabupaten_pasien]");
				$('#id_kecamatan_filter_dk').attr('name',"filter[code_kecamatan_pasien]");
				$('#id_kelurahan_filter_dk').attr('name',"filter[code_kelurahan_pasien]");
				// $('#export_excel_dk').removeAttr('disabled');
			}else if(val=='2'){
				$('#faskes_filter_dk').removeAttr('disabled');
				$('.alpasien_dk').addClass('hide');
				$('.instansi_dk').removeClass('hide');
				@if(Helper::role()->code_role=='rs')
				$('.kec_dk').addClass('hide');
				@endif
				$('#id_provinsi_filter_dk').attr('name',"filter[code_provinsi_faskes]");
				$('#id_kabupaten_filter_dk').attr('name',"filter[code_kabupaten_faskes]");
				$('#rs_filter_dk').attr('name',"filter[rs_id]");
				$('#id_kecamatan_filter_dk').attr('name',"filter[code_kecamatan_faskes]");
				$('#puskesmas_filter_dk').attr('name',"filter[puskesmas_id]");
				$('#id_wilayah_kerja_filter_dk').attr('name',"filter[id_wilayah_kerja]");
				@if(Helper::role()->code_role != 'pusat')
					// $('#export_excel_dk').attr('disabled','disabled');
				@endif
			}
		});

		$('#id_provinsi_filter_dk').on('change',function(){
			getKabupaten('_filter_dk');
			$('#faskes_filter_dk').val('').change();
			// activeexport_dk();
			return false;
		});
		$('#id_kabupaten_filter_dk').on('change',function(){
			var val = $('#filter_type_daftar_kasus_dk').val();
			getKecamatan('_filter_dk');
			$('#faskes_filter_dk').val('').change();
			// activeexport_dk();
			return false;
		});
		$('#id_kecamatan_filter_dk').on('change',function(){
			var val = $('#filter_type_daftar_kasus_dk').val();
			if(val=='1'){
				getKelurahan('_filter_dk');
			}
			$('#faskes_filter_dk').val('').change();
			// activeexport_dk();
			return false;
		});
		$('#rs_filter_dk').on('change',function(){
			// activeexport_dk();
			return false;
		});
		$('#puskesmas_filter_dk').on('change',function(){
			getWilayahKerja('_filter_dk');
			// activeexport_dk();
			return false;
		});
	});
</script>
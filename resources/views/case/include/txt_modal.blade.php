<div class="modal fade" id="filter_modal" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Export data txt</h4>
			</div>
			<div class="modal-body">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Data Klinis Campak</h3>
					</div>
					<div class="box-body" style="overflow-x:scroll;overflow-y:scroll;">
						<table id="table_txt" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>No</th>
									<th>No.Epid</th>
									<th>No.Outbreak</th>
									<th>Negara</th>
									<th>Provinsi</th>
									<th>Kabupaten</th>
									<th>Nama Pasien</th>
									<th>Jenis Kelamin</th>
									<th>Tanggal Lahir</th>
									<th>Umur tahun</th>
									<th>Igm Campak</th>
									<th>Igm Rubella</th>
									<th>Kultur</th>
									<th>Sequencing</th>
									<th>PCR</th>
									<th>Keadaan akhir</th>
									<th>Klasifikasi final</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
      </div>
			<div class="modal-footer" style="text-align: center">
				{!! Form::button("Export TXT", ['class' => 'btn btn-success','id'=>'confirm_export']) !!}
			</div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(function(){
		$('#confirm_export').on('click',function(){
			var dt = $('#table_txt').DataTable().rows().data().to$();
			JSONToTXTConvertor(dt, 'Campak', true);
		});
			$('#table_txt').DataTable({
				// "paging": true,
				"lengthChange": true,
				"autoWidth": true,
				"processing": true,
				"serverSide": true,
				"ajax": {
					url     : '{!! URL::to('case/campak/getDumpData') !!}',
					type    : "POST",
					data 	: function(d){
						d.dt = JSON.stringify($('#filter').serializeJSON());
					},
				},
				"columns"   : [
				{ "data" : "no" },
				{ "data" : "no_epid" },
				{ "data" : "no_outbreak" },
				{ "data" : "negara" },
				{ "data" : "provinsi" },
				{ "data" : "kabupaten" },
				{ "data" : "name_pasien" },
				{ "data" : "jenis_kelamin" },
				{ "data" : "tgl_lahir" },
				{ "data" : "umur_thn" },
				{ "data" : "igm_campak" },
				{ "data" : "igm_rubella" },
				{ "data" : "kultur" },
				{ "data" : "sqc" },
				{ "data" : "pcr" },
				{ "data" : "keadaan_akhir_txt" },
				{ "data" : "klasifikasi_final" },
				]
			});
			// $(".dataTables_filter").addClass('pull-right');
			//
			// $('#submit_filter').on('click',function(){
			// 	$('#table').DataTable().draw();
			// 	return false;
			// });
			$('#table_txt').on('draw.dt', function(){

				$(".delete").unbind();
				$(".delete-yes").unbind();
				$('.delete').on('click',function(){
					var action = $(this).attr('action');
					$('.delete-yes').attr('action',action);
				});
				$('.delete-yes').on('click', function(){
					$('.modaldelete').modal('toggle');
					var action  = $(this).attr('action');
					$.ajax({
						method  : "POST",
						url     : action,
						data    : null,
						dataType: "json",
						beforeSend: function(){
							startProcess();
						},
					})
					.done(function(response){
						if(response.success){
							setTimeout( function(){
								messageAlert('info', 'Berhasil.', 'Data kasus berhasil di hapus.');
								endProcess();
							}, 0);
						}
						$('#table').DataTable().draw();
					});
					return false;
				});
			});

	});
</script>

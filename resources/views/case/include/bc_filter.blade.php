{{-- @include('case.include.txt_modal') --}}
<div class="box box-success">
	{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'filter' , 'class' => 'form-horizontal']) !!}
	<div class="box-body">
		<div class="row">
			<div class="col-sm-6">
				<div class="box-body">
					<div class="form-group">
						<label class="col-sm-3 control-label">
							Rentang Waktu
						</label>
						<div class="col-sm-5">
							{!! Form::select('filter[range]', array(null=>'All','1'=>'Hari','2'=>'Bulan','3'=>'Tahun'), null, ['class' => 'form-control', 'id'=>'range_time']) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="input2">
							Sejak
						</label>
						<div class="col-sm-2" style="padding-right: 0px;">
							<select class="form-control" disabled="disabled" id="from_day" name="from[day]">
								<option value="">
									--Pilih--
								</option>
								@for($i=1;$i<=31;$i++)
								<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">
									{!! $i !!}
								</option>
								@endfor
							</select>
						</div>
						<div class="col-sm-3" style="padding-right: 0px;">
							{!! Form::select('from[month]',
								array(null => '--Pilih--',
									'01'=>'Januari',
									'02'=>'Febuari',
									'03'=>'Maret',
									'04'=>'April',
									'05'=>'Mei',
									'06'=>'Juni',
									'07'=>'Juli',
									'08'=>'Agustus',
									'09'=>'September',
									'10'=>'Oktober',
									'11'=>'November',
									'12'=>'Desember',
									), null, ['class'=>'form-control','id'=>'from_month','disabled']) !!}
								</div>
								<div class="col-sm-3" style="padding-right: 0px;">
									<select class="form-control" disabled="disabled" id="from_year" name="from[year]">
										<option value="">
											--Pilih--
										</option>
										@for($i=2010;$i<=2020;$i++)
										<option value="{!! $i !!}">
											{!! $i !!}
										</option>
										@endfor
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="inputEmail3">
									Sampai
								</label>
								<div class="col-sm-2" style="padding-right: 0px;">
									<select class="form-control" disabled="disabled" id="to_day" name="to[day]">
										<option value="">
											--Pilih--
										</option>
										@for($i=1;$i<=31;$i++)
										<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">
											{!! $i !!}
										</option>
										@endfor
									</select>
								</div>
								<div class="col-sm-3" style="padding-right: 0px;">
									{!! Form::select('to[month]',Helper::getMonth(), null, ['class'=>'form-control','id'=>'to_month','disabled']) !!}
								</div>
								<div class="col-sm-3" style="padding-right: 0px;">
									<select class="form-control" disabled="disabled" id="to_year" name="to[year]">
										<option value="">
											--Pilih--
										</option>
										@for($i=2010;$i<=2020;$i++)
										<option value="{!! $i !!}">
											{!! $i !!}
										</option>
										@endfor
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="box-body">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="input1">
									Filter
								</label>
								<div class="col-sm-5">
									{!! Form::select('filter_type', array(null=>'--Pilih Filter Alamat--','1'=> 'Berdasarkan Alamat Pasien','2'=>'Data Instansi'), null, ['class' => 'form-control','id'=>'filter_type_daftar_kasus']) !!}
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="input1">
									Provinsi
								</label>
								<div class="col-sm-5">
									{!! Form::select('district[id_provinsi]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control alamat','onchange'=>"getKabupaten('_filter')",'id'=>'id_provinsi_filter','disabled']) !!}
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="input1">
									Kabupaten
								</label>
								<div class="col-sm-5">
									{!! Form::select('district[id_kabupaten]', array(null=>'Pilih Kabupaten'), null, ['class' => 'form-control alamat','onchange'=>"getKecamatan('_filter')",'id'=>'id_kabupaten_filter','disabled']) !!}
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="input1">
									Kecamatan
								</label>
								<div class="col-sm-5">
									{!! Form::select('district[id_kecamatan]', array(null=>'Pilih Kecamatan'), null, ['class' => 'form-control alamat','onchange'=>"getPuskesmas('_filter')",'id'=>'id_kecamatan_filter','disabled']) !!}
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="input1">
									Puskesmas
								</label>
								<div class="col-sm-5">
									{!! Form::select('district[id_puskesmas]', array(null=>'Pilih Puskesmas'), null, ['class' => 'form-control alamat','id'=>'puskesmas_filter','disabled']) !!}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer" style="text-align: center">
				{!! Form::submit("Tampilkan", ['class' => 'btn btn-success','id'=>'submit_filter']) !!}
				{!! Form::close() !!}
				{!! Form::button("Export Excel", ['class' => 'btn btn-info','id'=>'export_excel','disabled']) !!}
				@if(Helper::role()->code_role=='pusat')
				{!! Form::button("Export to txt file", ['class' => 'btn btn-primary','id'=>'export_txt','data-target'=>'#filter_modal','data-toggle'=>'modal']) !!}
				@endif
			</div>
		</div>
		<script type="text/javascript">
			$(function(){
				$('#puskesmas_filter').on('change',function(){
					var val = $(this).val();
					var id_faskes = "{!! Helper::role()->id_faskes !!}";
					if(val==id_faskes){
						$('#export_excel').removeAttr('disabled');
					}else{
						$('#export_excel').attr('disabled','disabled');
					}
					return false;
				});
				$('#export_excel').on('click',function(){
					var params = JSON.stringify($('#filter').serializeArray());
					var url = "{!! URL::to('case/'.Request::segment(2).'/export') !!}/"+params;
					window.open(url, '_blank');
				});
				$('#range_time').on('change',function(){
					var val = $(this).val();
					if(val=='1'){
						$('#from_day').removeAttr('disabled');
						$('#from_month').removeAttr('disabled');
						$('#from_year').removeAttr('disabled');
						$('#to_day').removeAttr('disabled');
						$('#to_month').removeAttr('disabled');
						$('#to_year').removeAttr('disabled');
					}else if(val=='2'){
						$('#from_day').attr('disabled','disabled').select2('val',null);
						$('#from_month').removeAttr('disabled');
						$('#from_year').removeAttr('disabled');
						$('#to_day').attr('disabled','disabled').select2('val',null);
						$('#to_month').removeAttr('disabled');
						$('#to_year').removeAttr('disabled');
					}else if(val=='3'){
						$('#from_day').attr('disabled','disabled').select2('val',null);
						$('#from_month').attr('disabled','disabled').select2('val',null);
						$('#from_year').removeAttr('disabled');
						$('#to_day').attr('disabled','disabled').select2('val',null);
						$('#to_month').attr('disabled','disabled').select2('val',null);
						$('#to_year').removeAttr('disabled');
					}else{
						$('#from_day').attr('disabled','disabled').select2('val',null);
						$('#from_month').attr('disabled','disabled').select2('val',null);
						$('#from_year').attr('disabled','disabled').select2('val',null);
						$('#to_day').attr('disabled','disabled').select2('val',null);
						$('#to_month').attr('disabled','disabled').select2('val',null);
						$('#to_year').attr('disabled','disabled').select2('val',null);
					}
					return false;
				});
				$('#filter_type_daftar_kasus').on('change',function(){
					var val = $(this).val();
					if(val==''){
						$('.alamat').attr('disabled','disabled').select2('val',null);
					}else if(val=='1'){
						$('#id_provinsi_filter').removeAttr('disabled');
						$('#id_provinsi_filter').attr('name',"filter[code_provinsi_pasien]");
						$('#id_kabupaten_filter').attr('name',"filter[code_kabupaten_pasien]");
						$('#id_kecamatan_filter').attr('name',"filter[code_kecamatan_pasien]");
						$('#puskesmas_filter').attr('name',"filter[code_puskesmas_pasien]");
					}else if(val=='2'){
						$('#id_provinsi_filter').removeAttr('disabled');
						$('#id_provinsi_filter').attr('name',"filter[code_provinsi_faskes]");
						$('#id_kabupaten_filter').attr('name',"filter[code_kabupaten_faskes]");
						$('#id_kecamatan_filter').attr('name',"filter[code_kecamatan_faskes]");
						$('#puskesmas_filter').attr('name',"filter[code_puskesmas_faskes]");
					}
					return false;
				});
				$('#id_provinsi_filter').on('change',function(){
					var val = $(this).val();
					if(val!=''){
						$('#id_kabupaten_filter').removeAttr('disabled');
					}else{
						$('#id_kabupaten_filter').attr('disabled','disabled');
					}
					return false;
				});
				$('#id_kabupaten_filter').on('change',function(){
					var val = $(this).val();
					if(val!=''){
						$('#id_kecamatan_filter').removeAttr('disabled');
					}else{
						$('#id_kecamatan_filter').attr('disabled','disabled');
					}
					return false;
				});
				$('#id_kecamatan_filter').on('change',function(){
					var val = $(this).val();
					if(val!='' && $('#filter_type_daftar_kasus').val()==2){
						$('#puskesmas_filter').removeAttr('disabled');
					}else{
						$('#puskesmas_filter').attr('disabled','disabled');
					}
					return false;
				});
			})
		</script>

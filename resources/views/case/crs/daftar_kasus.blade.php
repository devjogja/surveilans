@include('case.include.filter')
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Data Klinis CRS</h3>
	</div>
	<div class="box-body">
		<table id="table" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">No.Epid</th>
					<th rowspan="2" width="10%">Nama Pasien</th>
					<th colspan="3">Usia</th>
					<th rowspan="2">Alamat</th>
					<th rowspan="2">Keadaan akhir</th>
					<th rowspan="2">Klasifikasi final</th>
					<th rowspan="2">Faskes</th>
					<th rowspan="2" width="6%">Tgl Input</th>
					<th rowspan="2" width="6%">Tgl Update</th>
					<th rowspan="2">PE</th>
					<th rowspan="2" width="12%">Aksi</th>
				</tr>
				<tr>
					<th>Thn</th>
					<th>Bln</th>
					<th>Hari</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		var tUrl = "{!! url('case/crs/getData'); !!}";
		var tData = [
		{ data: 'DT_Row_Index', searchable:false, orderable:false},
		{ data: 'no_epid', name:'no_epid'},
		{ data: 'name_pasien', name:'name_pasien'},
		{ data: 'umur_thn', name:'umur_thn'},
		{ data: 'umur_bln', name:'umur_bln'},
		{ data: 'umur_hari', name:'umur_hari'},
		{ data: 'full_address', name:'full_address'},
		{ data: 'keadaan_akhir_txt', name:'keadaan_akhir_txt'},
		{ data: 'klasifikasi_final_txt', name:'klasifikasi_final_txt'},
		{ data: 'name_faskes', name:'name_faskes'},
		{ data: 'tgl_input', name:'tgl_input'},
		{ data: 'tgl_update', name:'tgl_update'},
		{ data: 'id_pe', searchable:false, orderable:false},
		{ data: 'action', searchable:false, orderable:false},
		{ data: 'jenis_input_text', name:'jenis_input_text',visible:false},
		{ data: 'pe_text', name:'pe_text',visible:false},
		];
		var dtable = $('#table').DataTable({
			paging: true,
			lengthChange: true,
			searching: true,
			ordering: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			ajax: {
				url     : tUrl,
				type    : "POST",
				data 	: function(d){
					d.dt = JSON.stringify($('#filter_dk').serializeJSON());
				},
			},
			buttons: [],
			columns: tData,
		});
		$(".dataTables_filter_dk").addClass('pull-right');
		$('#table').parent().css({"overflow":"auto"});
		$('#submit_filter_dk').on('click',function(){
			startProcess();
			$('#table').DataTable().draw();
			setTimeout( function(){
				endProcess();
			}, 100);
			return false;
		});

		$('#table').on('draw.dt', function(){
			$(".delete").unbind();
			$(".delete-yes").unbind();
			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					if(response.success){
						setTimeout( function(){
							messageAlert('info', 'Berhasil.', 'Data kasus berhasil di hapus.');
							endProcess();
						}, 0);
					}
					$('#table').DataTable().draw();
				});
				return false;
			});
			$(".move").unbind();
			$(".move-yes").unbind();
			$('.move').on('click',function(){
				var action = $(this).attr('action');
				$('.move-yes').attr('action',action);
			});
			$('.move-yes').on('click', function(){
				$('.modalmove').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					if(response.success){
						setTimeout( function(){
							messageAlert('info', 'Berhasil.', 'Data kasus berhasil di kirim.');
							endProcess();
						}, 0);
					}else{
						setTimeout( function(){
							messageAlert('warning', 'Failed.', 'Data kasus gagal di kirim. Belum ada wilayah kerja puskesmas');
							endProcess();
						}, 0);
					}
					$('#table').DataTable().draw();
				});
				return false;
			});
		});
	});
</script>
@extends('layouts.base')
@section('content')
@if($dt = $data['response'])
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			<table class="table table-striped">
				<caption>Data pelapor</caption>
				<tbody>
					<tr>
						<td class="title">Nama Pelapor</td>
						<td>{!! $dt->nama_pelapor or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Provinsi</td>
						<td>{!! $dt->provinsi_pelapor or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Kabupaten</td>
						<td>{!! $dt->kabupaten_pelapor or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Laporan</td>
						<td>{!! $dt->tgl_laporan_pelapor !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Investigasi</td>
						<td>{!! $dt->tgl_investigasi_pelapor !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Identitas penderita</caption>
				<tbody>
					<tr>
						<td class="title">No.Epidemologi</td>
						<td>{!! $dt->no_epid or '-' !!} { {!! $dt->_no_epid or '-' !!} }</td>
					</tr>
					<tr>
						<td class="title">No RM</td>
						<td>{!! $dt->no_rm or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Bayi</td>
						<td>{!! $dt->name_pasien or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Jenis kelamin</td>
						<td>{!! $dt->jenis_kelamin_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Lahir(Umur)</td>
						<td>
							<?php
							$tgl_lahir = $dt->tgl_lahir ? $dt->tgl_lahir : null;
							$umur_thn = ($dt->umur_thn)?$dt->umur_thn.' Th':'';
							$umur_bln = ($dt->umur_bln)?$dt->umur_bln.' Bln':'';
							$umur_hari = ($dt->umur_hari)?$dt->umur_hari.' Hari':'';
							?>
							{!! $tgl_lahir !!} ({!!$umur_thn!!} {!!$umur_bln !!} {!!$umur_hari!!})
						</td>
					</tr>
					<tr>
						<td class="title">Alamat</td>
						<td>{!! $dt->alamat or '' !!} {!! $dt->full_address or '' !!}</td>
					</tr>
					<tr>
						<td class="title">Tempat bayi di lahirkan</td>
						<td>{!! $dt->tempat_lahir or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Ibu</td>
						<td>{!! $dt->nama_ortu or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nomer Telp</td>
						<td>{!! $dt->no_telp_ortu or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Umur kehamilan saat bayi dilahirkan</td>
						<td>{!! $dt->umur_kehamilan_bayi_lahir or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Berat badan bayi baru lahir</td>
						<td>{!! $dt->berat_badan_bayi_lahir or '-' !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Data Klinis Pasien</caption>
				<tbody>
					<tr><td colspan="2">Group A</td></tr>
					@foreach(Helper::getGejala('5','1') AS $key=>$val)
					<tr>
						<td class="title">{!! $val->name !!}</td>
						<?php
						$vga = '-';
						if(isset($dt->dtGejala[$val->id]->val_gejala_txt)){$vga = $dt->dtGejala[$val->id]->val_gejala_txt;}
						?>
						<td>{!! $vga !!}</td>
					</tr>
					@endforeach
					<tr><td colspan="2"></td></tr>
					<tr><td colspan="2">Group B</td></tr>
					@foreach(Helper::getGejala('5','2') AS $key=>$val)
					<tr>
						<td class="title">{!! $val->name !!}</td>
						<?php
						$vgb = '-';
						if(isset($dt->dtGejala[$val->id]->val_gejala_txt)){
							$vgb = $dt->dtGejala[$val->id]->val_gejala_txt;
						}
						$vgbo = '';
						if(isset($dt->dtGejala[$val->id]->other_gejala)){
							$vgbo = ', '.$dt->dtGejala[$val->id]->other_gejala;
						}
						?>
						<td>{!! $vgb.$vgbo !!}</td>
					</tr>
					@endforeach
					<tr><td colspan="2"></td></tr>
					<tr>
						<td class="title">Nama dokter pemeriksa</td>
						<td>{!! $dt->nama_dokter_pemeriksa or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal periksa</td>
						<td>{!! $dt->tgl_periksa !!}</td>
					</tr>
					<tr>
						<td class="title">Keadaan bayi saat ini</td>
						<?php
						$penyebab_meninggal = '';
						if($dt->keadaan_akhir=='2'){
							$penyebab_meninggal = ' ,Penyebab Meninggal '.$dt->penyebab_meninggal;
						}
						?>
						<td>{!! $dt->keadaan_akhir_txt or '-' !!} {!! $dt->penyebab_meninggal !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Data Riwayat Kehamilan Ibu</caption>
				<tbody>
					<tr>
						<td class="title">Jumlah kehamilan sebelumnya</td>
						<td colspan="2">{!! $dt->jml_kehamilan_sebelumnya or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Umur Ibu (dalam tahun)</td>
						<td colspan="2">{!! $dt->umur_ibu or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Hari Pertama haid terakhir (HPHT)</td>
						<td colspan="2">{!! $dt->hpht or '-' !!}</td>
					</tr>
					<tr><td colspan="3">Apakah selama kehamilan terakhir ini ibu pernah mengalami</td></tr>
					@foreach(Helper::getGejala('5','3') AS $key=>$val)
					<tr>
						<td class="title">{!! $val->name !!}</td>
						<?php
						$vgc = $vgct = '-';
						if(isset($dt->dtGejalaFamily[$val->id]->val_gejala_txt)){
							$vgc = $dt->dtGejalaFamily[$val->id]->val_gejala_txt;
							$vgct = $dt->dtGejalaFamily[$val->id]->tgl_kejadian;
						}
						?>
						<td>{!! $vgc or '-' !!},</td>
						<td>Tanggal Kejadian : {!! $vgct !!}</td>
					</tr>
					@endforeach
					<tr>
						<td class="title">Diagnosa rubella dengan konfirmasi lab</td>
						<td>{!! $dt->ibu_diagnosa_rubella_txt !!}</td>
						<?php
						$tgl_ibu_diagnosa_rubella = '';
						if($dt->ibu_diagnosa_rubella_txt=='Ya'){
							$tgl_ibu_diagnosa_rubella = 'Tanggal di diagnosa : '.$dt->tgl_ibu_diagnosa_rubella;
						}
						?>
						<td>{!! $tgl_ibu_diagnosa_rubella !!}</td>
					</tr>
					<tr>
						<td class="title">Apakah selama kehamilan ini ibu pernah terpapar atau berkontak dengan orang yang menderita ruam makulopapular</td>
						<td colspan="2">{!! $dt->kontak_ruam_makulopapular_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Umur Kehamilan saat terpapar dengan penderita ruam makulopapular</td>
						<td>{!! $dt->umur_kehamilan_kontak_ruam_makulopapular or '-' !!}</td>
						<?php
						$lokasi_kontak_ruam_makulopapular = '';
						if($dt->umur_kehamilan_kontak_ruam_makulopapular!=''){
							$lokasi_kontak_ruam_makulopapular = ' Lokasi terkena kontak : '.$dt->lokasi_kontak_ruam_makulopapular;
						}
						?>
						<td>{!! $lokasi_kontak_ruam_makulopapular !!}</td>
					</tr>
					<tr>
						<td class="title">Apakah ibu bepergian selama kehamilan terakhir ini</td>
						<td colspan="2">{!! $dt->bepergian_waktu_hamil_txt !!}</td>
					</tr>
					<tr>
						<td class="title">Umur kehamilan ketika bepergian</td>
						<td>{!! $dt->umur_hamil_waktu_pergi or '-' !!}</td>
						<?php
						$lokasi_bepergian = '';
						if($dt->umur_hamil_waktu_pergi!=''){
							$lokasi_bepergian = ' Bepergian kemana : '.$dt->lokasi_bepergian;
						}
						?>
						<td>{!! $lokasi_bepergian !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Pemeriksaan Laboratorium</caption>
				<tbody>
					<tr>
						<td class="title">Apakah spesimen diambil</td>
						<td colspan="2">{!! $dt->pengambilan_spesimen_txt or '-' !!}</td>
					</tr>
				</tbody>
			</table>
			<table class="table table-striped">
				<tbody>
					<tr>
						<td>Jenis Spesimen</td>
						<td>Tanggal ambil</td>
						<td>Tanggal kirim lab</td>
						<td>Tanggal tiba di lab</td>
					</tr>
					@if(empty($dt->dtPemeriksaanLab))
					<tr><td colspan="4">Tidak ada pemeriksaan laboratorium.</td></tr>
					@else
					@foreach($dt->dtPemeriksaanLab AS $k=>$v)
					<tr>
						<td>{!! $v->jenis_spesimen_txt or '-' !!}</td>
						<td>{!! $v->tgl_ambil_spesimen !!}</td>
						<td>{!! $v->tgl_kirim_lab !!}</td>
						<td>{!! $v->tgl_terima_lab !!}</td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Hasil Pemeriksaan Laboratorium</caption>
				<tbody>
					<tr>
						<td>Jenis Pemeriksaan</td>
						<td>Hasil</td>
						<td>Jenis virus</td>
						<td>Tanggal hasil lab</td>
						<td>Kadar IgG</td>
					</tr>
					@if(empty($dt->dtHasilLab))
					<tr><td colspan="5">Tidak ada hasil pemeriksaan laboratorium.</td></tr>
					@else
					@foreach($dt->dtHasilLab AS $k=>$v)
					<tr>
						<td>{!! $v->jenis_pemeriksaan_txt or '-' !!}</td>
						<td>{!! $v->hasil_txt or '-' !!}</td>
						<td>{!! $v->jenis_virus or '-' !!}</td>
						<td>{!! $v->tgl_hasil_lab !!}</td>
						<td>{!! $v->kadar_igG or '-' !!}</td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Klasifikasi Final</caption>
				<tbody>
					<tr>
						<td class="title">Klasifikasi Final</td>
						<td>{!! $dt->klasifikasi_final_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Deskripsi Klasifikasi Final</td>
						<td>{!! $dt->desc_klasifikasi_final or '-' !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endif
<div class="col-sm-12">
	<div class="footer">
		<a href="{!!URL::to('case/crs')!!}" class="btn btn-primary">Kembali</a>
	</div>
</div>
@endsection
@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			@include('case.crs.input_kasus')
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		getDetail();
	});

	function getDetail() {
		var id = $('#id_trx_case').val();
		var action = BASE_URL+'case/crs/getDetail/'+id;
		$.getJSON(action, function(result){
			var raw = result.response;
			if (isset(raw)) {
				$.each(raw, function(k, dt){
					$('#id_faskes').val(dt.id_faskes);
					$('#id_role').val(dt.id_role);
					$('#jenis_input').val(dt.jenis_input);
					$('#id_trx_case').val(dt.id);
					$('#nama_pelapor').val(dt.nama_pelapor);
					$('#id_provinsi_pelapor').val(dt.code_provinsi_pelapor);
					$('#id_kabupaten_pelapor').val(dt.code_kabupaten_pelapor);
					$('#tgl_laporan_diterima').val(dt.tgl_laporan_diterima);
					$('#tgl_pelacakan').val(dt.tgl_pelacakan);
					$('#name').val(dt.name_pasien);
					$('#id_pasien').val(dt.id_pasien);
					$('#nik').val(dt.nik);
					$('#no_rm').val(dt.no_rm);
					$('#agama').val(dt.agama).trigger('change');
					$('#nama_ortu').val(dt.nama_ortu);
					$('#id_family').val(dt.id_family);
					$('#jenis_kelamin').val(dt.jenis_kelamin).trigger('change');
					if(isset(dt.tgl_lahir)){$('#tgl_lahir').val(dt.tgl_lahir).removeAttr('disabled');}
					if(isset(dt.umur_hari)){$('#umur_hari').val(dt.umur_hari).removeAttr('disabled');}
					if(isset(dt.umur_bln)){$('#umur_bln').val(dt.umur_bln).removeAttr('disabled');}
					if(isset(dt.umur_thn)){$('#umur_thn').val(dt.umur_thn).removeAttr('disabled');}
					$('#alamat').val(dt.alamat);
					$('#wilayah').val(dt.full_address);
					$('#id_kelurahan_pasien').val(dt.code_kelurahan_pasien);
					$('#id_kecamatan_pasien').val(dt.code_kecamatan_pasien);
					$('#id_kabupaten_pasien').val(dt.code_kabupaten_pasien);
					$('#id_provinsi_pasien').val(dt.code_provinsi_pasien);
					$('#no_epid').val(dt.no_epid);
					$('#tempat_lahir').val(dt.tempat_lahir);
					$('#no_telp').val(dt.no_telp_ortu);
					$('#umur_kehamilan_bayi_lahir').val(dt.umur_kehamilan_bayi_lahir).trigger('change');
					$('#berat_badan_bayi_lahir').val(dt.berat_badan_bayi_lahir);
					var dtGejala = dt.dtGejala;
					$.each(dtGejala, function(key, val){
						$('#gejala_klinis'+val.id_gejala).val(val.val_gejala).trigger('change');
						if(val.id_gejala=='16'){
							$('#other').val(val.val_gejala).trigger('change');
							$('#other_gejala').val(val.other_gejala).removeAttr('disabled');
						}
					});
					$('#nama_dokter_pemeriksa').val(dt.nama_dokter_pemeriksa);
					$('#tgl_periksa').val(dt.tgl_periksa);
					$('#keadaan_akhir').val(dt.keadaan_akhir).trigger('change');
					$('#penyebab_meninggal').val(dt.penyebab_meninggal).removeAttr('disabled');
					$('#jml_kehamilan_sebelumnya').val(dt.jml_kehamilan_sebelumnya);
					$('#umur_ibu').val(dt.umur_ibu);
					$('#hpht').val(dt.hpht);
					var dtGejalaFamily = dt.dtGejalaFamily;
					$.each(dtGejalaFamily, function(key, val){
						$('#gejala'+val.id_gejala).val(val.val_gejala).trigger('change');
						$('#tgl_kejadian'+val.id_gejala).val(val.tgl_kejadian).removeAttr('disabled');
					});
					$('#ibu_diagnosa_rubella').val(dt.ibu_diagnosa_rubella).trigger('change');
					$('#tgl_ibu_diagnosa_rubella').val(dt.tgl_ibu_diagnosa_rubella).removeAttr('disabled');
					$('#kontak_ruam_makulopapular').val(dt.kontak_ruam_makulopapular).trigger('change');
					$('#umur_kehamilan_kontak_ruam_makulopapular').val(dt.umur_kehamilan_kontak_ruam_makulopapular).trigger('change');
					$('#lokasi_kontak_ruam_makulopapular').val(dt.lokasi_kontak_ruam_makulopapular);
					$('#bepergian_waktu_hamil').val(dt.bepergian_waktu_hamil).trigger('change');
					$('#umur_hamil_waktu_pergi').val(dt.umur_hamil_waktu_pergi).trigger('change');
					$('#lokasi_bepergian').val(dt.lokasi_bepergian).removeAttr('disabled');
					$('#pengambilan_spesimen').val(dt.pengambilan_spesimen).trigger('change');
					var dtPemeriksaanLab = dt.dtPemeriksaanLab;
					$.each(dtPemeriksaanLab, function(key, val){
						$('#tgl_ambil_spesimen'+val.jenis_spesimen).val(val.tgl_ambil_spesimen);
						$('#tgl_kirim_lab'+val.jenis_spesimen).val(val.tgl_kirim_lab);
						$('#tgl_terima_lab'+val.jenis_spesimen).val(val.tgl_terima_lab);
					});
					var dtHasilLab = dt.dtHasilLab;
					$.each(dtHasilLab, function(key, val){
						$('#hasil'+val.jenis_pemeriksaan).val(val.hasil).trigger('change');
						$('#jenis_virus'+val.jenis_pemeriksaan).val(val.jenis_virus);
						$('#tgl_hasil_lab'+val.jenis_pemeriksaan).val(val.tgl_hasil_lab);
						$('#kadar_igG'+val.jenis_pemeriksaan).val(val.kadar_igG);
					});
					$('#klasifikasi_final').val(dt.klasifikasi_final).trigger('change');
					if(dt.desc_klasifikasi_final){
						$('#desc_klasifikasi_final').val(dt.desc_klasifikasi_final).removeAttr('disabled');
					}
				});
			return false;
		};
	});
}
</script>

@endsection

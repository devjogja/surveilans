<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">
			Data Klinis Pasien
		</h3>
	</div>
	<div class="form-horizontal">
		<div class="box-body">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">
						Group A (Lengkapi semua tanda dan gejala yang ada)
					</h3>
				</div>
				<div class="box-body">
					@foreach(Helper::getGejala('5','1') AS $key=>$val)
					<div class="form-group">
						{!! Form::label(null, $val->name, ['class' => 'col-sm-4 control-label']) !!}
						<div class="col-sm-4">
						{!! Form::select("dg[gejala][$val->id]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak tahu'), null, ['class' => 'form-control','id'=>"gejala_klinis$val->id"]) !!}
						</div>
					</div>
					@endforeach
				</div>
			</div>
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">
						Group B (Lengkapi semua tanda dan gejala yang ada)
					</h3>
				</div>
				<div class="box-body">
					@foreach(Helper::getGejala('5','2') AS $key=>$val)
					<div class="form-group">
						{!! Form::label(null, $val->name, ['class' => 'col-sm-4 control-label']) !!}
						@if($val->name=='Other abnormalities')
						<div class="col-sm-4">
							{!! Form::select("dg[gejala][$val->id]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak tahu'), null, ['class' => 'form-control','id'=>'other']) !!}
						</div>
						<div class="col-sm-4">
							{!! Form::text("dg[other_gejala][$val->id]", null, ['class' => 'form-control','id'=>'other_gejala','placeholder'=>'Other Abnormalities','disabled']) !!}
						</div>
						@else
						<div class="col-sm-4">
							{!! Form::select("dg[gejala][$val->id]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak tahu'), null, ['class' => 'form-control','id'=>"gejala_klinis$val->id"]) !!}
						</div>
						@endif
					</div>
					@endforeach
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Nama dokter pemeriksa', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-8">
					{!! Form::textarea('dk[nama_dokter_pemeriksa]', null, ['class' => 'form-control','id'=>'nama_dokter_pemeriksa','placeholder'=>'Nama dokter pemeriksa','rows'=>'3']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Tanggal periksa', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dk[tgl_periksa]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal periksa','id'=>'tgl_periksa','onchange'=>'getEpid();getAge();']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Keadaan bayi saat ini', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-3">
					{!! Form::select('dk[keadaan_akhir]', array(null=>'--Pilih--','1'=>'Hidup','2'=>'Meninggal'), null, ['class' => 'form-control','id'=>'keadaan_akhir']) !!}
				</div>
				<div class="col-sm-5">
					{!! Form::text('dk[penyebab_meninggal]', null, ['class' => 'form-control','placeholder'=>'Penyebab meninggal','id'=>'penyebab_meninggal','disabled']) !!}
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('#other').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#other_gejala').removeAttr('disabled');
			}else{
				$('#other_gejala').val(null).attr('disabled','disabled');
			}
		});
		$('#keadaan_akhir').on('change',function(){
			var val = $(this).val();
			if(val=='2'){
				$('#penyebab_meninggal').removeAttr('disabled');
			}else{
				$('#penyebab_meninggal').val(null).attr('disabled','disabled');
			}
		});
	});
</script>
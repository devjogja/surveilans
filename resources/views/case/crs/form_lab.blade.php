<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Data Pemeriksaan dan Hasil Laboratorium pada bayi</h3>
	</div>
	<div class="form-horizontal">
		<div class="box-body">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Pemeriksaan Laboratorium</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						{!! Form::label(null, 'Apakah spesimen diambil', ['class' => 'col-sm-2 control-label']) !!}
						<div class="col-sm-2">
							{!! Form::select('dc[pengambilan_spesimen]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak tahu'), null, ['class' => 'form-control','id'=>'pengambilan_spesimen']) !!}
						</div>
					</div>
					<table class="table table-striped">
						<tr>
							<td>{!! Form::label(null, 'Jenis Spesimen', ['class' => 'control-label']) !!}</td>
							<td>
								{!! Form::label(null, 'Serum 1', ['class' => 'control-label']) !!}
								{!! Form::hidden('ds[jenis_spesimen][]', '1', ['class' => 'form-control']) !!}
							</td>
							<td>{!! Form::text("ds[tgl_ambil_spesimen][]", null, ['class' => 'form-control datemax','id'=>'tgl_ambil_spesimen1','placeholder'=>'Tanggal ambil']) !!}</td>
							<td>{!! Form::text('ds[tgl_kirim_lab][]', null, ['class' => 'form-control dates','id'=>'tgl_kirim_lab1','placeholder'=>'Tanggal dikirim ke lab/di isi oleh Kab','disabled']) !!}</td>
							<td>{!! Form::text('ds[tgl_terima_lab][]', null, ['class' => 'form-control dates','id'=>'tgl_terima_lab1','placeholder'=>'Tanggal tiba di lab','disabled']) !!}</td>
						</tr>
						<tr>
							<td></td>
							<td>
								{!! Form::label(null, 'Serum 2', ['class' => 'control-label']) !!}
								{!! Form::hidden('ds[jenis_spesimen][]', '2', ['class' => 'form-control']) !!}
							</td>
							<td>{!! Form::text("ds[tgl_ambil_spesimen][]", null, ['class' => 'form-control datemax','id'=>'tgl_ambil_spesimen2','placeholder'=>'Tanggal ambil']) !!}</td>
							<td>{!! Form::text('ds[tgl_kirim_lab][]', null, ['class' => 'form-control dates','id'=>'tgl_kirim_lab2','placeholder'=>'Tanggal dikirim ke lab/di isi oleh Kab','disabled']) !!}</td>
							<td>{!! Form::text('ds[tgl_terima_lab][]', null, ['class' => 'form-control dates','id'=>'tgl_terima_lab2','placeholder'=>'Tanggal tiba di lab','disabled']) !!}</td>
						</tr>
						<tr>
							<td></td>
							<td>
								{!! Form::label(null, 'Throat Swab', ['class' => 'control-label']) !!}
								{!! Form::hidden('ds[jenis_spesimen][]', '3', ['class' => 'form-control']) !!}
							</td>
							<td>{!! Form::text("ds[tgl_ambil_spesimen][]", null, ['class' => 'form-control datemax','id'=>'tgl_ambil_spesimen3','placeholder'=>'Tanggal ambil']) !!}</td>
							<td>{!! Form::text('ds[tgl_kirim_lab][]', null, ['class' => 'form-control dates','id'=>'tgl_kirim_lab3','placeholder'=>'Tanggal dikirim ke lab/di isi oleh Kab','disabled']) !!}</td>
							<td>{!! Form::text('ds[tgl_terima_lab][]', null, ['class' => 'form-control dates','id'=>'tgl_terima_lab3','placeholder'=>'Tanggal tiba di lab','disabled']) !!}</td>
						</tr>
						<tr>
							<td></td>
							<td>
								{!! Form::label(null, 'Urine', ['class' => 'control-label']) !!}
								{!! Form::hidden('ds[jenis_spesimen][]', '4', ['class' => 'form-control']) !!}
							</td>
							<td>{!! Form::text("ds[tgl_ambil_spesimen][]", null, ['class' => 'form-control datemax','id'=>'tgl_ambil_spesimen4','placeholder'=>'Tanggal ambil']) !!}</td>
							<td>{!! Form::text('ds[tgl_kirim_lab][]', null, ['class' => 'form-control dates','id'=>'tgl_kirim_lab4','placeholder'=>'Tanggal dikirim ke lab/di isi oleh Kab','disabled']) !!}</td>
							<td>{!! Form::text('ds[tgl_terima_lab][]', null, ['class' => 'form-control dates','id'=>'tgl_terima_lab4','placeholder'=>'Tanggal tiba di lab','disabled']) !!}</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Hasil Pemeriksaan Laboratorium</h3>
				</div>
				<div class="box-body">
					<table class="table table-striped">
						<tr>
							<td>{!! Form::label(null, 'Jenis Pemeriksaan', ['class' => 'control-label']) !!}</td>
							<td>{!! Form::label(null, 'Hasil', ['class' => 'control-label']) !!}</td>
							<td colspan="4"></td>
						</tr>
						<tr>
							<td>
								{!! Form::label(null, 'IgM serum ke 1', ['class' => 'control-label']) !!}
								{!! Form::hidden('dhs[jenis_pemeriksaan][]', '1', ['class' => 'form-control']) !!}
							</td>
							<td>{!! Form::select('dhs[hasil][]', array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif'), null, ['class' => 'form-control','id'=>'hasil1']) !!}</td>
							<td>{!! Form::text('dhs[jenis_virus][]', null, ['class' => 'form-control','placeholder'=>'Jenis Virus','id'=>'jenis_virus1']) !!}</td>
							<td>{!! Form::text("dhs[tgl_hasil][]", null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Hasil Lab','id'=>'tgl_hasil_lab1']) !!}</td>
							<td>{!! Form::hidden('dhs[kadar_igG][]', null, ['class' => 'form-control','placeholder'=>'Kadar IgG','id'=>'kadar_igG1']) !!}</td>
						</tr>
						<tr>
							<td>
								{!! Form::label(null, 'IgM serum 2 (ulangan)', ['class' => 'control-label']) !!}
								{!! Form::hidden('dhs[jenis_pemeriksaan][]', '2', ['class' => 'form-control']) !!}
							</td>
							<td>{!! Form::select('dhs[hasil][]', array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif'), null, ['class' => 'form-control','id'=>'hasil2']) !!}</td>
							<td>{!! Form::text('dhs[jenis_virus][]', null, ['class' => 'form-control','placeholder'=>'Jenis Virus','id'=>'jenis_virus2']) !!}</td>
							<td>{!! Form::text("dhs[tgl_hasil][]", null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Hasil Lab','id'=>'tgl_hasil_lab2']) !!}</td>
							<td>{!! Form::hidden('dhs[kadar_igG][]', null, ['class' => 'form-control','placeholder'=>'Kadar IgG','id'=>'kadar_igG2']) !!}</td>
						</tr>
						<tr>
							<td>
								{!! Form::label(null, 'IgG serum 1', ['class' => 'control-label']) !!}
								{!! Form::hidden('dhs[jenis_pemeriksaan][]', '3', ['class' => 'form-control']) !!}
							</td>
							<td>{!! Form::select('dhs[hasil][]', array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif'), null, ['class' => 'form-control','id'=>'hasil3']) !!}</td>
							<td>{!! Form::text('dhs[jenis_virus][]', null, ['class' => 'form-control','placeholder'=>'Jenis Virus','id'=>'jenis_virus3']) !!}</td>
							<td>{!! Form::text("dhs[tgl_hasil][]", null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Hasil Lab','id'=>'tgl_hasil_lab3']) !!}</td>
							<td>{!! Form::text('dhs[kadar_igG][]', null, ['class' => 'form-control','placeholder'=>'Kadar IgG','id'=>'kadar_igG3']) !!}</td>
						</tr>
						<tr>
							<td>
								{!! Form::label(null, 'IgG serum 2 (ulangan)', ['class' => 'control-label']) !!}
								{!! Form::hidden('dhs[jenis_pemeriksaan][]', '4', ['class' => 'form-control']) !!}
							</td>
							<td>{!! Form::select('dhs[hasil][]', array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif'), null, ['class' => 'form-control','id'=>'hasil4']) !!}</td>
							<td>{!! Form::text('dhs[jenis_virus][]', null, ['class' => 'form-control','placeholder'=>'Jenis Virus','id'=>'jenis_virus4']) !!}</td>
							<td>{!! Form::text("dhs[tgl_hasil][]", null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Hasil Lab','id'=>'tgl_hasil_lab4']) !!}</td>
							<td>{!! Form::text('dhs[kadar_igG][]', null, ['class' => 'form-control','placeholder'=>'Kadar IgG','id'=>'kadar_igG4']) !!}</td>
						</tr>
						<tr>
							<td>
								{!! Form::label(null, 'Isolasi', ['class' => 'control-label']) !!}
								{!! Form::hidden('dhs[jenis_pemeriksaan][]', '5', ['class' => 'form-control']) !!}
							</td>
							<td>{!! Form::select('dhs[hasil][]', array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif'), null, ['class' => 'form-control','id'=>'hasil5']) !!}</td>
							<td>{!! Form::text('dhs[jenis_virus][]', null, ['class' => 'form-control','placeholder'=>'Jenis Virus','id'=>'jenis_virus5']) !!}</td>
							<td>{!! Form::text("dhs[tgl_hasil][]", null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Hasil Lab','id'=>'tgl_hasil_lab5']) !!}</td>
							<td>{!! Form::hidden('dhs[kadar_igG][]', null, ['class' => 'form-control','placeholder'=>'Kadar IgG','id'=>'kadar_igG5']) !!}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

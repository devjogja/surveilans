<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Klasifikasi final</h3>
	</div>
	<div class="form-horizontal">
		<div class="box-body">
			<div class="form-group">
				{!! Form::label(null, 'Klasifikasi final', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-3">
					{!! Form::select('dc[klasifikasi_final]', array(null=>'--Pilih--','1'=>'CRS pasti(Lab Positif)','2'=>'CRS Klinis','3'=>'Bukan CRS','4'=>'Suspek CRS'), null, ['class' => 'form-control', 'id'=>'klasifikasi_final']) !!}
				</div>
				<div class="col-sm-3">
					{!! Form::textarea('dc[desc_klasifikasi_final]', null, ['class' => 'form-control','placeholder'=>'Deskripsi klasifikasi final','id'=>'desc_klasifikasi_final','disabled','rows'=>'3']) !!}
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$('#klasifikasi_final').on('change',function(){
			var val = $(this).val();
			if(val=='3'){
				$('#desc_klasifikasi_final').removeAttr('disabled');
			}else{
				$('#desc_klasifikasi_final').val(null).attr('disabled','disabled');
			}
		});
	})
</script>
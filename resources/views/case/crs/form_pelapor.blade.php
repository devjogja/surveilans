<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">
			Data pelapor
		</h3>
	</div>
	<div class="form-horizontal">
		@if($role = Helper::role())
		<div class="box-body">
			<div class="form-group">
				{!! Form::label(null, 'Nama Faskes', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::text('dpel[nama_pelapor]', strtoupper($role->name_role).' '.$role->name_faskes, ['class' => 'form-control','placeholder'=>'Nama pelapor','id'=>'nama_pelapor']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Provinsi', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::select('dpel[code_provinsi_pelapor]', array(null=>'Pilih Provinsi')+Helper::getProvince(), $role->code_provinsi_faskes, ['class' => 'form-control','id'=>'id_provinsi_pelapor','onchange'=>"getKabupaten('_pelapor')"]) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kabupaten/Kota', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::select('dpel[code_kabupaten_pelapor]', array($role->code_kabupaten_faskes=>$role->name_kabupaten_faskes), null, ['class' => 'form-control', 'id'=>'id_kabupaten_pelapor']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Tanggal Laporan', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::text('dk[tgl_laporan_diterima]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Laporan','id'=>'tgl_laporan_diterima']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Tanggal Investigasi', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::text('dk[tgl_pelacakan]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Investigasi','id'=>'tgl_pelacakan']) !!}
				</div>
			</div>
		</div>
		@endif
	</div>
</div>
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">
			Data Riwayat Kehamilan Ibu
		</h3>
	</div>
	<div class="form-horizontal">
		<div class="box-body">
			<div class="form-group">
				{!! Form::label(null, 'Jumlah kehamilan sebelumnya', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-3">
					{!! Form::text('rf[jml_kehamilan_sebelumnya]', null, ['class' => 'form-control','placeholder'=>'Jumlah kehamilan sebelumnya','id'=>'jml_kehamilan_sebelumnya']) !!}
				</div>
				<span class="col-sm-4">
					*(Dalam bentuk angka)
				</span>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Umur Ibu (dalam tahun)', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-3">
					{!! Form::text('rf[umur_ibu]', null, ['class' => 'form-control','placeholder'=>'Umur Ibu (dalam tahun)','id'=>'umur_ibu']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Hari Pertama haid terakhir (HPHT)', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-3">
					{!! Form::text('rf[hpht]', null, ['class' => 'form-control','placeholder'=>'Hari Pertama haid terakhir (HPHT)','id'=>'hpht']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Apakah selama kehamilan terakhir ini ibu pernah mengalami ', ['class' => 'col-sm-6 control-label']) !!}
			</div>
			@foreach(Helper::getGejala('5','3') AS $key=>$val)
			<div class="form-group">
				{!! Form::label(null, $val->name, ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-2">
					{!! Form::select("dgf[gejala][$val->id]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak tahu'), $val->id, ['class' => 'form-control','id'=>"gejala$val->id",'onchange'=>"active('$val->id')"]) !!}
				</div>
				<div class="col-sm-3">
					{!! Form::text("dgf[tgl_kejadian][$val->id]", null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Kejadian','disabled','id'=>"tgl_kejadian$val->id"]) !!}
				</div>
			</div>
			@endforeach
			<div class="form-group">
				{!! Form::label(null, 'Apakah selama kehamilan ini ibu pernah didiagnosa rubella dengan konfirmasi lab', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-2">
					{!! Form::select('rf[ibu_diagnosa_rubella]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak tahu'), null, ['class' => 'form-control','id'=>'ibu_diagnosa_rubella']) !!}
				</div>
				<div class="col-sm-3">
					{!! Form::text('rf[tgl_ibu_diagnosa_rubella]', null, ['class' => 'form-control datemax','placeholder'=>'Kapan diagnosa dilakukan','id'=>'tgl_ibu_diagnosa_rubella','disabled']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Apakah selama kehamilan ini ibu pernah terpapar atau berkontak dengan orang yang menderita ruam makulopapular ?', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-2">
					{!! Form::select('rf[kontak_ruam_makulopapular]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak tahu'), null, ['class' => 'form-control','id'=>'kontak_ruam_makulopapular']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Umur Kehamilan saat terpapar dengan penderita ruam makulopapular', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-2">
					{!! Form::select('rf[umur_kehamilan_kontak_ruam_makulopapular]', array(null=>'--Pilih--')+range(0,60), null, ['class' => 'form-control','id'=>'umur_kehamilan_kontak_ruam_makulopapular']) !!}
				</div>
				<div class="col-sm-3">
					{!! Form::textarea('rf[lokasi_kontak_ruam_makulopapular]', null, ['class' => 'form-control','placeholder'=>'Dimana terkena kontak','rows'=>'3','id'=>'lokasi_kontak_ruam_makulopapular','disabled']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Apakah ibu bepergian selama kehamilan terakhir ini', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-2">
					{!! Form::select('rf[bepergian_waktu_hamil]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak tahu'), null, ['class' => 'form-control','id'=>'bepergian_waktu_hamil']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Umur kehamilan ketika bepergian', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-2">
					{!! Form::select('rf[umur_hamil_waktu_pergi]', array(null=>'--Pilih--')+range(0,60), null, ['class' => 'form-control','id'=>'umur_hamil_waktu_pergi']) !!}
				</div>
				<div class="col-sm-3">
					{!! Form::textarea('rf[lokasi_bepergian]', null, ['class' => 'form-control','placeholder'=>'Bepergian kemana','rows'=>'3','id'=>'lokasi_bepergian','disabled']) !!}
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function active(id) {
		if(isset(id)){
			var val = $('#gejala'+id).val();
			if(val=='1'){
				$('#tgl_kejadian'+id).removeAttr('disabled');
			}else{
				$('#tgl_kejadian'+id).val(null).attr('disabled','disabled');
			}
		}
		return false;
	}

	$(function(){
		$('#ibu_diagnosa_rubella').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#tgl_ibu_diagnosa_rubella').removeAttr('disabled');
			}else{
				$('#tgl_ibu_diagnosa_rubella').val(null).attr('disabled','disabled');
			}
		});
		$('#umur_kehamilan_kontak_ruam_makulopapular').on('change',function(){
			var val = $(this).val();
			if(val!=''){
				$('#lokasi_kontak_ruam_makulopapular').removeAttr('disabled');
			}else{
				$('#lokasi_kontak_ruam_makulopapular').val(null).attr('disabled','disabled');
			}
		});
		$('#umur_hamil_waktu_pergi').on('change',function(){
			var val = $(this).val();
			if(val!=''){
				$('#lokasi_bepergian').removeAttr('disabled');
			}else{
				$('#lokasi_bepergian').val(null).attr('disabled','disabled');
			}
		});
	});
</script>
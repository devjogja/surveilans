@extends('layouts.base')
@section('content')
@include('case.include.filter_analisa_webview')
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita CRS Berdasar Jenis Kelamin</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(1);']) !!}
		<div class="pull-right col-md-2">
			{!! Form::select(null, array(null=>'Suspek CRS','[1,2]'=>'CRS ( CRS Pasti dan CRS Klinis)','1'=>'CRS Pasti (Lab Positif)','2'=>'CRS Klinis'), 'crs', ['class' => 'form-control', 'id'=>'hasilLabJenisKelaminCRS','onchange'=>'hasilLabJenisKelaminCRS();']) !!}
		</div>
	</div>
	@include('case.chart.chart_gender')
</div>

<!-- <div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita CRS Berdasar Waktu</h3>
		{!! Form::button('Unduh Data &nbsp<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','disabled']) !!}
		<div class="pull-right col-md-2">
			{!! Form::select(null, array('crs'=>'Suspek CRS','crs_all'=>'CRS ( CRS Pasti dan CRS Klinis)','crs_pasti'=>'CRS Pasti (Lab Positif)','crs_klinis'=>'CRS Klinis'), 'crs', ['class' => 'form-control', 'id'=>'hasilLabWaktuCRS','onchange'=>'hasilLabWaktuCRS();']) !!}
		</div>
	</div>
	@include('case.chart.chart_waktu')
</div> -->

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita CRS Berdasar Umur</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(3);']) !!}
		<div class="pull-right col-md-2">
			{!! Form::select(null, array(null=>'Suspek CRS','[1,2]'=>'CRS ( CRS Pasti dan CRS Klinis)','1'=>'CRS Pasti (Lab Positif)','2'=>'CRS Klinis'), 'crs', ['class' => 'form-control', 'id'=>'hasilLabUmurCRS','onchange'=>'hasilLabUmurCRS();']) !!}
		</div>
	</div>
	@include('case.chart.chart_umur')
</div>

<!-- <div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita CRS Berdasar Status Imunisasi</h3>
		{!! Form::button('Unduh Data &nbsp<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','disabled']) !!}
		<div class="pull-right col-md-2">
			{!! Form::select(null, array('crs'=>'Suspek CRS','crs_all'=>'CRS ( CRS Pasti dan CRS Klinis)','crs_pasti'=>'CRS Pasti (Lab Positif)','crs_klinis'=>'CRS Klinis'), 'crs', ['class' => 'form-control', 'id'=>'hasilLabStatImunCRS','onchange'=>'hasilLabStatImunCRS();']) !!}
		</div>
	</div>
	@include('case.chart.chart_stat_imun')

</div> -->
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita CRS Berdasar Klasifikasi Final</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(5);']) !!}

	</div>
	@include('case.chart.chart_final')
</div>

<script type="text/javascript">
var dataquery;
function unduh(val) {
	var tmp=[];
	var data=[];
		switch (val) {
			case 1:
			var rawdata = dataquery.jenis_kelamin;
			var title 	= 'Jenis Kelamin CRS';
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Jenis kelamin'] 	= rawdata[i][0];
					tmp['Jumlah'] 				= rawdata[i][1];
					data.push(tmp);
					tmp=[];
				}
				break;
			case 2:
			var rawdata = dataquery.waktu.data[0].data;
			var title 	= 'Waktu '+dataquery.waktu.data[0].name+' CRS';
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Bulan'] 	= (rawdata[i]['name'])?rawdata[i]['name']:rawdata[i][0];
					tmp['Jumlah'] = (rawdata[i]['y'])?rawdata[i]['y']:rawdata[i][1];
					data.push(tmp);
					tmp=[];
				}
				break;
			case 3:
			var rawdata = dataquery.umur[0].data;
			var title 	= dataquery.umur[0].name+' CRS';
			var category = [@foreach (Helper::getCategoryUmur('crs') as $item)
			    '{{ $item }}',
			@endforeach ];
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Umur'] 	= category[i];
					tmp['Jumlah'] = rawdata[i];
					data.push(tmp);
					tmp=[];
				}
				break;
			case 4:
			var rawdata = dataquery.stat_imun;
			var title 	= 'Status Imunisasi CRS';
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Status Imunisasi'] = rawdata[i][0];
					tmp['Jumlah'] 					= rawdata[i][1];
					data.push(tmp);
					tmp=[];
				}
				break;
			case 5:
			var rawdata = dataquery.klasifikasi_final;
			var title 	= 'Klasifikasi Final CRS';
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Klasifikasi Final'] = rawdata[i][0];
					tmp['Jumlah'] 					 = rawdata[i][1];
					data.push(tmp);
					tmp=[];
				}
				break;
		}
    // if(data == '')
    //     return;

    JSONToCSVConvertor(data, title, true);
};


$('#id_provinsi_filter_analisa').on('change',function(){
	var val = $(this).val();
	if(val!=''){
		$('#id_kabupaten_filter_analisa').removeAttr('disabled');
	}else{
		$('#id_kabupaten_filter_analisa').attr('disabled','disabled');
	}
	return false;
});
$('#id_kabupaten_filter_analisa').on('change',function(){
	var val = $(this).val();
	if(val!=''){
		$('#id_kecamatan_filter_analisa').removeAttr('disabled');
	}else{
		$('#id_kecamatan_filter_analisa').attr('disabled','disabled');
	}
	return false;
});
$('#id_kecamatan_filter_analisa').on('change',function(){
	var val = $(this).val();
	if(val!='' && $('#filter_type').val()==2){
		$('#puskesmas_filter_analisa').removeAttr('disabled');
	}else{
		$('#puskesmas_filter_analisa').attr('disabled','disabled');
	}
	return false;
});

var category = [@foreach (Helper::getCategoryUmur('crs') as $item)
    '{{ $item }}',
@endforeach ];
var title = 'Suspek CRS';
var range = 'Nasional'+'<br>Tahun 2010-'+new Date().getFullYear();
var senddata = {'filter':{'code_provinsi_pasien':''}};
$.ajax({
	method  : "POST",
	url     : BASE_URL+'api/analisa/crs',
	data    : JSON.stringify(senddata),
	// dataType: "json",
	beforeSend: function(){
		// startProcess();
	},
	success: function(data){
		if (data.success==true) {
			endProcess();
			var dt=data.response;
			console.log(dt);
			dataquery = dt;
			graphJenisKelamin(title,null,range,dt.jenis_kelamin);
			graphFinal(title,range,dt.klasifikasi_final);
			graphUmur(title,null,range,category,dt.umur);
		}else{
			messageAlert('warning', 'Peringatan', 'Data gagal salah');
		}
	}
});
var action = BASE_URL+'api/analisa/crs';
$('#filter_analisa').validate({
	rules:{
		'filter_type':'required',
	},
	messages:{
		'filter_type':'Jenis data wajib diisi',
	},
  submitHandler: function(){
		var data = $('#filter_analisa').serializeJSON();
		range = $('#id_provinsi_filter_analisa option:selected').text();
    console.log(JSON.stringify(data));
    $.ajax({
      method  : "POST",
      url     : action,
      data    : JSON.stringify(data),
      // dataType: "json",
      beforeSend: function(){
        // startProcess();
      },
      success: function(data){
        if (data.success==true) {
          endProcess();
					console.log(data.response);
					// console.log(JSON.stringify(data.response));
					var dt=data.response;
					dataquery = dt;
					// range = range+'<br>'+dt.waktu[0].name;
					graphJenisKelamin(title,null,range,dt.jenis_kelamin);
					graphFinal(title,range,dt.klasifikasi_final);
					graphUmur(title,null,range,category,dt.umur);
        }else{
          messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
          endProcess();
        }
      }
    });
    return false;
  }
});
function hasilLabJenisKelaminCRS() {
	if($('#filter_type').val()!=''){
			jenis_kasus = $('#hasilLabJenisKelaminCRS').val();
			title = 'Suspek CRS';
			if(jenis_kasus!=''){
				title = $('#hasilLabJenisKelaminCRS option:selected').text();;
			}
			var data = $('#filter_analisa').serializeJSON();
			data['filter']['klasifikasi_final']=jenis_kasus;
			range = $('#id_provinsi_filter_analisa option:selected').text();
				if(range=='Pilih Provinsi'){range='Nasional';}
			if($('#from_year_analisa').val() && $('#to_year_analisa').val()){range = range+'<br>'+'Tahun'+$('#from_year_analisa').val()+'-'+$('#to_year_analisa').val();
				}
			else{
				range=range+'<br>Tahun 2010-'+new Date().getFullYear();
			}
			console.log(JSON.stringify(data));
			$.ajax({
				method  : "POST",
				url     : action,
				data    : JSON.stringify(data),
				// dataType: "json",
				beforeSend: function(){
					startProcess();
				},
				success: function(data){
					if (data.success==true) {
						var dt=data.response;
dataquery = dt;
						console.log(data.response);
						graphJenisKelamin(title,null,range,dt.jenis_kelamin);
						endProcess();
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
						endProcess();
					}
				}
			});
	}else{
		messageAlert('warning', 'Peringatan', 'Pilihan filter harus diisi');
	}
}

// function hasilLabWaktuCRS() {
// 		var hasil_lab = $('#hasilLabWaktuCRS').val();
// 		var title = 'Suspek CRS';
// 		if(hasil_lab=='crs_all'){
// 			title = 'CRS ( CRS Pasti dan CRS Klinis)';
// 		}else if(hasil_lab=='crs_pasti'){
// 			title = 'CRS Pasti (Lab Positif)';
// 		}else if(hasil_lab=='crs_klinis'){
// 			title = 'CRS Klinis';
// 		}
// 		graphWaktu(title,null,range);
// }

function hasilLabUmurCRS() {
	if($('#filter_type').val()!=''){
			jenis_kasus = $('#hasilLabUmurCRS').val();
			title = 'Suspek CRS';
			if(jenis_kasus!=''){
				title = $('#hasilLabUmurCRS option:selected').text();;
			}
			var data = $('#filter_analisa').serializeJSON();
			data['filter']['klasifikasi_final']=jenis_kasus;
			range = $('#id_provinsi_filter_analisa option:selected').text();
				if(range=='Pilih Provinsi'){range='Nasional';}
			if($('#from_year_analisa').val() && $('#to_year_analisa').val()){range = range+'<br>'+'Tahun'+$('#from_year_analisa').val()+'-'+$('#to_year_analisa').val();
				}
			else{
				range=range+'<br>Tahun 2010-'+new Date().getFullYear();
			}
			console.log(JSON.stringify(data));
			$.ajax({
				method  : "POST",
				url     : action,
				data    : JSON.stringify(data),
				// dataType: "json",
				beforeSend: function(){
					startProcess();
				},
				success: function(data){
					if (data.success==true) {
						var dt=data.response;
dataquery = dt;
						console.log(data.response);
						graphUmur(title,null,range,category,dt.umur);
						endProcess();
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
						endProcess();
					}
				}
			});
	}else{
		messageAlert('warning', 'Peringatan', 'Pilihan filter harus diisi');
	}
}

// function hasilLabStatImunCRS() {
// 		var hasil_lab = $('#hasilLabStatImunCRS').val();
// 		var title = 'Suspek CRS';
// 		if(hasil_lab=='crs_all'){
// 			title = 'CRS ( CRS Pasti dan CRS Klinis)';
// 		}else if(hasil_lab=='crs_pasti'){
// 			title = 'CRS Pasti (Lab Positif)';
// 		}else if(hasil_lab=='crs_klinis'){
// 			title = 'CRS Klinis';
// 		}
// 		graphStatImun(title,null,range);
// }

function hasilLabFinalCRS() {
		var hasil_lab = $('#hasilLabFinalCRS').val();
		var title = 'Suspek CRS';
		if(hasil_lab=='crs_all'){
			title = 'CRS ( CRS Pasti dan CRS Klinis)';
		}else if(hasil_lab=='crs_pasti'){
			title = 'CRS Pasti (Lab Positif)';
		}else if(hasil_lab=='crs_klinis'){
			title = 'CRS Klinis';
		}

		graphFinal(title,range,data);
}
</script>

@endsection

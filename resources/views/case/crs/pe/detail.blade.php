@extends('layouts.base')
@section('content')
@if($dt = $data['response'])
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			<table class="table table-striped">
				<caption>Riwayat Kontak</caption>
				<tbody>
					<tr>
						<td class="title">Apakah selama kehamilan ini ibu pernah didiagnosa rubella dengan konfirmasi lab</td>
						<td colspan="2">{!! $dt->ibu_diagnosa_rubella_txt or '-' !!}, Tgl Diagnosa Rubella Konfirmasi Lab : {!! $dt->tgl_ibu_diagnosa_rubella or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Apakah selama kehamilan ini ibu pernah terpapar atau berkontak dengan orang yang menderita ruam makulopapular</td>
						<td colspan="2">{!! $dt->kontak_ruam_makulopapular_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Umur Kehamilan saat terpapar dengan penderita ruam makulopapular</td>
						<td>{!! $dt->umur_kehamilan_kontak_ruam_makulopapular or '-' !!}</td>
						<?php
						$lokasi_kontak_ruam_makulopapular = '';
						if($dt->umur_kehamilan_kontak_ruam_makulopapular!=''){
							$lokasi_kontak_ruam_makulopapular = ' Lokasi kontak : '.$dt->lokasi_kontak_ruam_makulopapular;
						}
						?>
						<td>{!! $lokasi_kontak_ruam_makulopapular !!}</td>
					</tr>
					<tr>
						<td class="title">Apakah ibu bepergian selama kehamilan terakhir ini</td>
						<td colspan="2">{!! $dt->bepergian_waktu_hamil_txt !!}</td>
					</tr>
					<tr>
						<td class="title">Umur kehamilan ketika bepergian</td>
						<td>{!! $dt->umur_hamil_waktu_pergi or '-' !!}</td>
						<?php
						$lokasi_bepergian = '';
						if($dt->umur_hamil_waktu_pergi!=''){
							$lokasi_bepergian = ' Tujuan bepergian : '.$dt->lokasi_bepergian;
						}
						?>
						<td>{!! $lokasi_bepergian !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endif
<div class="col-sm-12">
	<div class="footer">
		<a href="{!!URL::to('case/crs#tab_4')!!}" class="btn btn-primary">Kembali</a>
	</div>
</div>
@endsection
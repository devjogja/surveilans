@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="alert alert-notif alert-dismissable">
		<i class="icon fa fa-warning"></i> Text input yang bertanda bintang (*) wajib di isi
	</div>

	<div class="row">
		{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form', 'class' => 'form-horizontal']) !!}
		{!! Form::hidden('id_trx_case', $data['id_trx_case'], ['id'=>'id_trx_case']) !!}
		{!! Form::hidden('id_trx_pe', $data['id_trx_pe'], ['id'=>'id_trx_pe']) !!}
		{!! Form::hidden('dpe[id_faskes]', Helper::role()->id_faskes,['id'=>'id_faskes']) !!}
		{!! Form::hidden('dpe[id_role]', Helper::role()->id_role,['id'=>'id_role']) !!}
		{!! Form::hidden('dpe[jenis_input]', '1',['id'=>'jenis_input']) !!}

		<div class="col-sm-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">
						Riwayat Kontak
					</h3>
				</div>
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Apakah selama kehamilan ini ibu pernah didiagnosa rubella dengan konfirmasi lab', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-2">
								{!! Form::select('rf[ibu_diagnosa_rubella]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak tahu'), null, ['class' => 'form-control','id'=>'ibu_diagnosa_rubella']) !!}
							</div>
							<div class="col-sm-3">
								{!! Form::text('rf[tgl_ibu_diagnosa_rubella]', null, ['class' => 'form-control datemax','placeholder'=>'Kapan diagnosa dilakukan','id'=>'tgl_ibu_diagnosa_rubella','disabled']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Apakah selama kehamilan ini ibu pernah terpapar atau berkontak dengan orang yang menderita ruam makulopapular ?', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-2">
								{!! Form::select('rf[kontak_ruam_makulopapular]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak tahu'), null, ['class' => 'form-control','id'=>'kontak_ruam_makulopapular']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Umur Kehamilan saat terpapar dengan penderita ruam makulopapular', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-2">
								{!! Form::select('rf[umur_kehamilan_kontak_ruam_makulopapular]', array(null=>'--Pilih--')+range(0,60), null, ['class' => 'form-control','id'=>'umur_kehamilan_kontak_ruam_makulopapular']) !!}
							</div>
							<div class="col-sm-3">
								{!! Form::textarea('rf[lokasi_kontak_ruam_makulopapular]', null, ['class' => 'form-control','placeholder'=>'Dimana terkena kontak','rows'=>'3','id'=>'lokasi_kontak_ruam_makulopapular','disabled']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Apakah ibu bepergian selama kehamilan terakhir ini', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-2">
								{!! Form::select('rf[bepergian_waktu_hamil]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak tahu'), null, ['class' => 'form-control','id'=>'bepergian_waktu_hamil']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Umur kehamilan ketika bepergian', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-2">
								{!! Form::select('rf[umur_hamil_waktu_pergi]', array(null=>'--Pilih--')+range(0,60), null, ['class' => 'form-control','id'=>'umur_hamil_waktu_pergi']) !!}
							</div>
							<div class="col-sm-3">
								{!! Form::textarea('rf[lokasi_bepergian]', null, ['class' => 'form-control','placeholder'=>'Bepergian kemana','rows'=>'3','id'=>'lokasi_bepergian','disabled']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="footer">
				{!! Form::reset("Batal", ['class' => 'btn btn-warning batal']) !!}
				{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<script type="text/javascript">
	function getDetail() {
		var id = $('#id_trx_case').val();
		if(isset(id)){
			var action = BASE_URL+'case/crs/getDetail/'+id;
			$.getJSON(action, function(result){
				var raw = result.response;
				if (isset(raw)) {
					$.each(raw, function(k, dt){
						$('#ibu_diagnosa_rubella').val(dt.ibu_diagnosa_rubella).trigger('change');
						if(isset(dt.tgl_ibu_diagnosa_rubella)){
							$('#tgl_ibu_diagnosa_rubella').val(dt.tgl_ibu_diagnosa_rubella).removeAttr('disabled');
						}
						$('#kontak_ruam_makulopapular').val(dt.kontak_ruam_makulopapular).trigger('change');
						$('#umur_kehamilan_kontak_ruam_makulopapular').val(dt.umur_kehamilan_kontak_ruam_makulopapular).trigger('change');
						if(isset(dt.lokasi_kontak_ruam_makulopapular)){
							$('#lokasi_kontak_ruam_makulopapular').val(dt.lokasi_kontak_ruam_makulopapular).removeAttr('disabled');
						}
						$('#bepergian_waktu_hamil').val(dt.bepergian_waktu_hamil).trigger('change');
						$('#umur_hamil_waktu_pergi').val(dt.umur_hamil_waktu_pergi).trigger('change');
						if(isset(dt.lokasi_bepergian)){
							$('#lokasi_bepergian').val(dt.lokasi_bepergian).removeAttr('disabled');
						}
					});
				};
			});
		}
	}

	$(function(){
		getDetail();
		$('.batal').on('click',function(){
			window.location.href = '{!! url('case/crs#tab_4'); !!}';
			return false;
		});
		$('#ibu_diagnosa_rubella').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#tgl_ibu_diagnosa_rubella').removeAttr('disabled');
			}else{
				$('#tgl_ibu_diagnosa_rubella').val(null).attr('disabled','disabled');
			}
		});
		$('#umur_kehamilan_kontak_ruam_makulopapular').on('change',function(){
			var val = $(this).val();
			if(val!=''){
				$('#lokasi_kontak_ruam_makulopapular').removeAttr('disabled');
			}else{
				$('#lokasi_kontak_ruam_makulopapular').val(null).attr('disabled','disabled');
			}
		});
		$('#umur_hamil_waktu_pergi').on('change',function(){
			var val = $(this).val();
			if(val!=''){
				$('#lokasi_bepergian').removeAttr('disabled');
			}else{
				$('#lokasi_bepergian').val(null).attr('disabled','disabled');
			}
		});
		$('#form').validate({
			rules:{},
			messages:{},
			submitHandler: function(){
				var action = BASE_URL+'case/crs/pe/store';
				var data = $('#form').serializeJSON();
				$.ajax({
					method  : "POST",
					url     : action,
					data    : JSON.stringify([data]),
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
					success: function(data, status){
						if (data.success==true) {
							window.location.href = '{!! url('case/crs#tab_4'); !!}';
						}else{
							messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
							endProcess();
						}
					}
				});
				return false;
			}
		});
	});
</script>

@endsection

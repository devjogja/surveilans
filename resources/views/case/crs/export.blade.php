<table border="1">
	<thead>
		<tr>
			<th rowspan="5">No</th>
			<th rowspan="5">Tgl Input</th>
			<th rowspan="5">Tgl Update</th>
			<th rowspan="5">No.RM</th>
			<th rowspan="5">No.Epid PD3I v04.01</th>
			<th rowspan="5">No.Epid PD3I v04.00</th>
			<th rowspan="5">No.Epid Lama</th>
			<th rowspan="5">Nama Faskes</th>
			<th rowspan="5">Kecamatan</th>
			<th rowspan="5">Kabupaten/Kota</th>
			<th rowspan="5">Provinsi</th>
			<th colspan="17">Identitas Pasien</th>
			<th colspan="2">Titik Koordinat Pasien</th>
			<th colspan="5">Data pelapor</th>
			<th colspan="18">Data Klinis Pasien</th>
			<th colspan="29">Data Riwayat Kehamilan Ibu</th>
			<th colspan="30">Data Pemeriksaan dan Hasil Laboratorium pada bayi</th>
			<th rowspan="5">Klasifikasi final</th>
			<th rowspan="5">Deskripsi Klasifikasi final</th>
		</tr>
		<tr>
			<th rowspan="4">Nama Pasien</th>
			<th rowspan="4">NIK</th>
			<th rowspan="4">Jenis Kelamin</th>
			<th rowspan="4">Tanggal Lahir</th>
			<th colspan="3">Umur</th>
			<th colspan="5">Alamat</th>
			<th rowspan="4">Tempat bayi di lahirkan</th>
			<th rowspan="4">Nama Ibu</th>
			<th rowspan="4">Nomer Telp</th>
			<th rowspan="4">Umur kehamilan saat bayi dilahirkan</th>
			<th rowspan="4">Berat badan bayi baru lahir</th>
			<th rowspan="4">Longitude</th>
			<th rowspan="4">Latitude</th>
			<th rowspan="4">Nama Faskes</th>
			<th rowspan="4">Provinsi</th>
			<th rowspan="4">Kabupaten/Kota</th>
			<th rowspan="4">Tanggal Laporan</th>
			<th rowspan="4">Tanggal Investigasi</th>
			<th colspan="5">Group A </th>
			<th colspan="9">Group B</th>
			<th rowspan="4">Nama dokter pemeriksa</th>
			<th rowspan="4">Tanggal periksa</th>
			<th rowspan="4">Keadaan bayi saat ini</th>
			<th rowspan="4">Penyebab Meninggal</th>
			<th rowspan="4">Jumlah kehamilan sebelumnya</th>
			<th rowspan="4">Umur Ibu (dalam tahun)</th>
			<th rowspan="4">Hari Pertama haid terakhir (HPHT)</th>
			<th rowspan="4">Conjunctivitis</th>
			<th rowspan="4">Tgl Kejadian</th>
			<th rowspan="4">Pilek</th>
			<th rowspan="4">Tgl Kejadian</th>
			<th rowspan="4">Batuk</th>
			<th rowspan="4">Tgl Kejadian</th>
			<th rowspan="4">Ruam makulopapular</th>
			<th rowspan="4">Tgl Kejadian</th>
			<th rowspan="4">Pembengkakan kelenjar limfa</th>
			<th rowspan="4">Tgl Kejadian</th>
			<th rowspan="4">Demam</th>
			<th rowspan="4">Tgl Kejadian</th>
			<th rowspan="4">Arthralgia/arthritis</th>
			<th rowspan="4">Tgl Kejadian</th>
			<th rowspan="4">Komplikasi lain</th>
			<th rowspan="4">Tgl Kejadian</th>
			<th rowspan="4">Mendapat Vaksinasi rubella</th>
			<th rowspan="4">Tgl Kejadian</th>
			<th rowspan="4">Apakah selama kehamilan ini ibu pernah didiagnosa rubella dengan konfirmasi lab</th>
			<th rowspan="4">Tgl Kejadian</th>
			<th rowspan="4">Apakah selama kehamilan ini ibu pernah terpapar atau berkontak dengan orang yang menderita ruam makulopapular ?</th>
			<th rowspan="4">Umur Kehamilan saat terpapar dengan penderita ruam makulopapular</th>
			<th rowspan="4">Lokasi Kontak ruam Makulopapular</th>
			<th rowspan="4">ibu bepergian selama kehamilan terakhir ini</th>
			<th rowspan="4">Umur kehamilan ketika bepergian</th>
			<th rowspan="4">Tujuan Bepergian</th>
			<th rowspan="4">Apakah spesimen diambil</th>
			<th colspan="12">Pemeriksaan Laboratorium</th>
			<th colspan="17">Hasil Pemeriksaan Laboratorium</th>
		</tr>
		<tr>
			<th rowspan="3">Tahun</th>
			<th rowspan="3">Bulan</th>
			<th rowspan="3">Hari</th>
			<th rowspan="3">Alamat</th>
			<th rowspan="3">Kelurahan</th>
			<th rowspan="3">Kecamatan</th>
			<th rowspan="3">Kabupaten</th>
			<th rowspan="3">Provinsi</th>
			<th rowspan="3">Congenital heart disease</th>
			<th rowspan="3">Cataracts</th>
			<th rowspan="3">Congenital glaucoma</th>
			<th rowspan="3">Pigmentary retinopathy</th>
			<th rowspan="3">Hearing impairment</th>
			<th rowspan="3">Purpura</th>
			<th rowspan="3">Microcephaly</th>
			<th rowspan="3">Meningoencephalitis</th>
			<th rowspan="3">Ikterik 24 jam post partum</th>
			<th rowspan="3">Splenomegaly</th>
			<th rowspan="3">Developmental delay</th>
			<th rowspan="3">Radiolucent bone disease</th>
			<th rowspan="3">Other abnormalities</th>
			<th rowspan="3">Keterangan Other abnormalities</th>
			<th colspan="12">Jenis Spesimen</th>
			<th colspan="17">Jenis Pemeriksaan</th>
		</tr>
		<tr>
			<th colspan="3">Serum 1</th>
			<th colspan="3">Serum 2</th>
			<th colspan="3">Throat Swab</th>
			<th colspan="3">Urine</th>
			<th colspan="3">IgM serum ke 1</th>
			<th colspan="3">IgM serum 2 (ulangan)</th>
			<th colspan="4">IgG serum 1</th>
			<th colspan="4">IgG serum 2 (ulangan)</th>
			<th colspan="3">Isolasi</th>
		</tr>
		<tr>
			<th>Tgl ambil</th>
			<th>Tgl kirim lab</th>
			<th>Tgl Tiba di Lab</th>
			<th>Tgl ambil</th>
			<th>Tgl kirim lab</th>
			<th>Tgl Tiba di Lab</th>
			<th>Tgl ambil</th>
			<th>Tgl kirim lab</th>
			<th>Tgl Tiba di Lab</th>
			<th>Tgl ambil</th>
			<th>Tgl kirim lab</th>
			<th>Tgl Tiba di Lab</th>
			<th>Hasil</th>
			<th>Jenis Virus</th>
			<th>Tgl Hasil Lab</th>
			<th>Hasil</th>
			<th>Jenis Virus</th>
			<th>Tgl Hasil Lab</th>
			<th>Hasil</th>
			<th>Jenis Virus</th>
			<th>Tgl Hasil Lab</th>
			<th>Kadar IgG</th>
			<th>Hasil</th>
			<th>Jenis Virus</th>
			<th>Tgl Hasil Lab</th>
			<th>Kadar IgG</th>
			<th>Hasil</th>
			<th>Jenis Virus</th>
			<th>Tgl Hasil Lab</th>
		</tr>
	</thead>
	<tbody>
		@if($dd)
			<?php $no = 1;?>
			@foreach($dd AS $key=>$val)
				<?php $val = (array) $val;?>
				<tr>
					<td>{!! $no++; !!}</td>
					<td>{!! $val['tgl_input'] !!}</td>
					<td>{!! $val['tgl_update'] !!}</td>
					<td>{!! $val['no_rm'] !!}</td>
					<td>{!! $val['no_epid'] !!}</td>
					<td>{!! $val['_no_epid'] !!}</td>
					<td>{!! $val['no_epid_lama'] !!}</td>
					<td>{!! $val['name_faskes'] !!}</td>
					<td>{!! $val['name_kecamatan_faskes'] !!}</td>
					<td>{!! $val['name_kabupaten_faskes'] !!}</td>
					<td>{!! $val['name_provinsi_faskes'] !!}</td>
					<td>{!! $val['name_pasien'] !!}</td>
					<td>{!! $val['nik'] !!}</td>
					<td>{!! $val['jenis_kelamin_txt'] !!}</td>
					<td>{!! $val['tgl_lahir'] !!}</td>
					<td>{!! $val['umur_thn'] !!}</td>
					<td>{!! $val['umur_bln'] !!}</td>
					<td>{!! $val['umur_hari'] !!}</td>
					<td>{!! $val['alamat'] !!}</td>
					<td>{!! $val['kelurahan_pasien'] !!}</td>
					<td>{!! $val['kecamatan_pasien'] !!}</td>
					<td>{!! $val['kabupaten_pasien'] !!}</td>
					<td>{!! $val['provinsi_pasien'] !!}</td>
					<td>{!! $val['tempat_lahir'] !!}</td>
					<td>{!! $val['nama_ortu'] !!}</td>
					<td>{!! $val['no_telp'] !!}</td>
					<td>{!! $val['umur_kehamilan_bayi_lahir'] !!}</td>
					<td>{!! $val['berat_badan_bayi_lahir'] !!}</td>
					<td>{!! $val['longitude'] !!}</td>
					<td>{!! $val['latitude'] !!}</td>
					<td>{!! $val['nama_pelapor'] !!}</td>
					<td>{!! $val['name_provinsi_pelapor'] !!}</td>
					<td>{!! $val['name_kabupaten_pelapor'] !!}</td>
					<td>{!! $val['tgl_laporan_diterima'] !!}</td>
					<td>{!! $val['tgl_pelacakan'] !!}</td>
					<?php
					$vd = [
						'1'=>'Ya',
						'2'=>'Tidak',
						'3'=>'Tidak Tahu',
					];
					$g1 = explode('|', $val['gejala']);
					$gd = [];
					foreach ($g1 as $vg) {
						$vg1 = explode('_', $vg);
						$gd[$vg1[0]] = !empty($vg1[1])?empty($vd[$vg1[1]])?'':$vd[$vg1[1]]:'';
					}
					?>
					<td>{!! empty($gd['10'])?'':$gd['10']; !!}</td>
					<td>{!! empty($gd['11'])?'':$gd['11']; !!}</td>
					<td>{!! empty($gd['12'])?'':$gd['12']; !!}</td>
					<td>{!! empty($gd['13'])?'':$gd['13']; !!}</td>
					<td>{!! empty($gd['14'])?'':$gd['14']; !!}</td>
					<td>{!! empty($gd['15'])?'':$gd['15']; !!}</td>
					<td>{!! empty($gd['16'])?'':$gd['16']; !!}</td>
					<td>{!! empty($gd['4'])?'':$gd['4']; !!}</td>
					<td>{!! empty($gd['5'])?'':$gd['5']; !!}</td>
					<td>{!! empty($gd['6'])?'':$gd['6']; !!}</td>
					<td>{!! empty($gd['7'])?'':$gd['7']; !!}</td>
					<td>{!! empty($gd['8'])?'':$gd['8']; !!}</td>
					<td>{!! empty($gd['9'])?'':$gd['9']; !!}</td>
					<td>{!! $val['other_gejala'] !!}</td>
					<td>{!! $val['nama_dokter_pemeriksa'] !!}</td>
					<td>{!! $val['tgl_periksa'] !!}</td>
					<td>{!! $val['keadaan_akhir_txt'] !!}</td>
					<td>{!! $val['penyebab_meninggal'] !!}</td>
					<td>{!! $val['jml_kehamilan_sebelumnya'] !!}</td>
					<td>{!! $val['umur_ibu'] !!}</td>
					<td>{!! $val['hpht'] !!}</td>
					<?php
					$g2 = explode('|', $val['gejala_family']);
					$gdf = [];
					foreach ($g2 as $vg1) {
						$vg2 = explode('_', $vg1);
						$gdf[$vg2[0]]['isi'] = !empty($vg2[1])?empty($vd[$vg2[1]])?'':$vd[$vg2[1]]:'';
						$gdf[$vg2[0]]['tgl_kejadian'] = empty($vg2[2])?'':$vg2[2];
					}
					?>
					<td>{!! empty($gdf['17']['isi'])?'':$gdf['17']['isi']; !!}</td>
					<td>{!! empty($gdf['17']['tgl_kejadian'])?'':$gdf['17']['tgl_kejadian']; !!}</td>
					<td>{!! empty($gdf['18']['isi'])?'':$gdf['18']['isi']; !!}</td>
					<td>{!! empty($gdf['18']['tgl_kejadian'])?'':$gdf['18']['tgl_kejadian']; !!}</td>
					<td>{!! empty($gdf['19']['isi'])?'':$gdf['19']['isi']; !!}</td>
					<td>{!! empty($gdf['19']['tgl_kejadian'])?'':$gdf['19']['tgl_kejadian']; !!}</td>
					<td>{!! empty($gdf['20']['isi'])?'':$gdf['20']['isi']; !!}</td>
					<td>{!! empty($gdf['20']['tgl_kejadian'])?'':$gdf['20']['tgl_kejadian']; !!}</td>
					<td>{!! empty($gdf['21']['isi'])?'':$gdf['21']['isi']; !!}</td>
					<td>{!! empty($gdf['21']['tgl_kejadian'])?'':$gdf['21']['tgl_kejadian']; !!}</td>
					<td>{!! empty($gdf['22']['isi'])?'':$gdf['22']['isi']; !!}</td>
					<td>{!! empty($gdf['22']['tgl_kejadian'])?'':$gdf['22']['tgl_kejadian']; !!}</td>
					<td>{!! empty($gdf['23']['isi'])?'':$gdf['23']['isi']; !!}</td>
					<td>{!! empty($gdf['23']['tgl_kejadian'])?'':$gdf['23']['tgl_kejadian']; !!}</td>
					<td>{!! empty($gdf['24']['isi'])?'':$gdf['24']['isi']; !!}</td>
					<td>{!! empty($gdf['24']['tgl_kejadian'])?'':$gdf['24']['tgl_kejadian']; !!}</td>
					<td>{!! empty($gdf['25']['isi'])?'':$gdf['25']['isi']; !!}</td>
					<td>{!! empty($gdf['25']['tgl_kejadian'])?'':$gdf['25']['tgl_kejadian']; !!}</td>
					<td>{!! empty($vd[$val['ibu_diagnosa_rubella']])?'':$vd[$val['ibu_diagnosa_rubella']] !!}</td>
					<td>{!! $val['tgl_ibu_diagnosa_rubella'] !!}</td>
					<td>{!! empty($vd[$val['kontak_ruam_makulopapular']])?'':$vd[$val['kontak_ruam_makulopapular']] !!}</td>
					<td>{!! $val['umur_kehamilan_kontak_ruam_makulopapular'] !!}</td>
					<td>{!! $val['lokasi_kontak_ruam_makulopapular'] !!}</td>
					<td>{!! empty($vd[$val['bepergian_waktu_hamil']])?'':$vd[$val['bepergian_waktu_hamil']] !!}</td>
					<td>{!! $val['umur_hamil_waktu_pergi'] !!}</td>
					<td>{!! $val['lokasi_bepergian'] !!}</td>
					<td>{!! empty($vd[$val['pengambilan_spesimen']])?'':$vd[$val['pengambilan_spesimen']] !!}</td>
					<?php
					$g3 = explode('|', $val['spesimen']);
					$g31 = [];
					foreach ($g3 as $g32) {
						$g33 = explode('_', $g32);
						$g31[$g33[0]]['tgl_ambil_spesimen'] = empty($g33[1])?'':$g33[1];
						$g31[$g33[0]]['tgl_kirim_lab'] = empty($g33[2])?'':$g33[2];
						$g31[$g33[0]]['tgl_terima_lab'] = empty($g33[3])?'':$g33[3];
					}
					?>
					<td>{!! empty($g31['1']['tgl_ambil_spesimen'])?'':$g31['1']['tgl_ambil_spesimen']; !!}</td>
					<td>{!! empty($g31['1']['tgl_kirim_lab'])?'':$g31['1']['tgl_kirim_lab']; !!}</td>
					<td>{!! empty($g31['1']['tgl_terima_lab'])?'':$g31['1']['tgl_terima_lab']; !!}</td>
					<td>{!! empty($g31['2']['tgl_ambil_spesimen'])?'':$g31['2']['tgl_ambil_spesimen']; !!}</td>
					<td>{!! empty($g31['2']['tgl_kirim_lab'])?'':$g31['2']['tgl_kirim_lab']; !!}</td>
					<td>{!! empty($g31['2']['tgl_terima_lab'])?'':$g31['2']['tgl_terima_lab']; !!}</td>
					<td>{!! empty($g31['3']['tgl_ambil_spesimen'])?'':$g31['3']['tgl_ambil_spesimen']; !!}</td>
					<td>{!! empty($g31['3']['tgl_kirim_lab'])?'':$g31['3']['tgl_kirim_lab']; !!}</td>
					<td>{!! empty($g31['3']['tgl_terima_lab'])?'':$g31['3']['tgl_terima_lab']; !!}</td>
					<td>{!! empty($g31['4']['tgl_ambil_spesimen'])?'':$g31['4']['tgl_ambil_spesimen']; !!}</td>
					<td>{!! empty($g31['4']['tgl_kirim_lab'])?'':$g31['4']['tgl_kirim_lab']; !!}</td>
					<td>{!! empty($g31['4']['tgl_terima_lab'])?'':$g31['4']['tgl_terima_lab']; !!}</td>
					<?php
					$h = [
						'1'=>'Positif',
						'2'=>'Negatif',
					];
					$g4 = explode('|', $val['hasil_lab']);
					$g41 = [];
					foreach ($g4 as $g42) {
						$g43 = explode('_', $g42);
						$g41[$g43[0]]['hasil'] = empty($h[$g43[1]])?'':$h[$g43[1]];
						$g41[$g43[0]]['jenis_virus'] = empty($g43[2])?'':$g43[2];
						$g41[$g43[0]]['tgl_hasil'] = empty($g43[3])?'':$g43[3];
						$g41[$g43[0]]['kadar_igG'] = empty($g43[4])?'':$g43[4];
					}
					?>
					<td>{!! empty($g41['1']['hasil'])?'':$g41['1']['hasil']; !!}</td>
					<td>{!! empty($g41['1']['jenis_virus'])?'':$g41['1']['jenis_virus']; !!}</td>
					<td>{!! empty($g41['1']['tgl_hasil'])?'':$g41['1']['tgl_hasil']; !!}</td>
					<td>{!! empty($g41['2']['hasil'])?'':$g41['2']['hasil']; !!}</td>
					<td>{!! empty($g41['2']['jenis_virus'])?'':$g41['2']['jenis_virus']; !!}</td>
					<td>{!! empty($g41['2']['tgl_hasil'])?'':$g41['2']['tgl_hasil']; !!}</td>
					<td>{!! empty($g41['3']['hasil'])?'':$g41['3']['hasil']; !!}</td>
					<td>{!! empty($g41['3']['jenis_virus'])?'':$g41['3']['jenis_virus']; !!}</td>
					<td>{!! empty($g41['3']['tgl_hasil'])?'':$g41['3']['tgl_hasil']; !!}</td>
					<td>{!! empty($g41['3']['kadar_igG'])?'':$g41['3']['kadar_igG']; !!}</td>
					<td>{!! empty($g41['4']['hasil'])?'':$g41['4']['hasil']; !!}</td>
					<td>{!! empty($g41['4']['jenis_virus'])?'':$g41['4']['jenis_virus']; !!}</td>
					<td>{!! empty($g41['4']['tgl_hasil'])?'':$g41['4']['tgl_hasil']; !!}</td>
					<td>{!! empty($g41['4']['kadar_igG'])?'':$g41['4']['kadar_igG']; !!}</td>
					<td>{!! empty($g41['5']['hasil'])?'':$g41['5']['hasil']; !!}</td>
					<td>{!! empty($g41['5']['jenis_virus'])?'':$g41['5']['jenis_virus']; !!}</td>
					<td>{!! empty($g41['5']['tgl_hasil'])?'':$g41['5']['tgl_hasil']; !!}</td>
					<td>{!! $val['klasifikasi_final_txt'] !!}</td>
					<td>{!! $val['desc_klasifikasi_final'] !!}</td>
				</tr>
			@endforeach
		@endif
	</tbody>
</table>
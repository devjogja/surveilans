<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">
			Identitas penderita
		</h3>
	</div>
	<div class="form-horizontal">
		<div class="box-body">
			<div class="form-group">
				{!! Form::label('name', 'Nama Penderita', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-7">
					<div class="input-group">
						{!! Form::text('dp[name]', null, ['class' => 'form-control','id'=>'name','placeholder'=>'Nama Penderita']) !!}
						{!! Form::hidden('id_pasien', null, ['class' => 'form-control','id'=>'id_pasien']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('nik', 'NIK', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-4">
					{!! Form::text('dp[nik]', null, ['class' => 'form-control','id'=>'nik','placeholder'=>'NIK']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('no_rm', 'No. RM', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::text('dc[no_rm]', null, ['class' => 'form-control','id'=>'no_rm','placeholder'=>'No. RM']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('agama', 'Agama', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-4">
					{!! Form::select('dp[agama]', array(null=>'--Pilih--')+Helper::getReligion(),null, ['class' => 'form-control','id'=>'agama','placeholder'=>'Agama']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('name', 'Nama Orang Tua', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-7">
					<div class="input-group">
						{!! Form::text('df[name]', null, ['class' => 'form-control','placeholder'=>'Nama Orang Tua','id'=>'nama_ortu']) !!}
						{!! Form::hidden('id_family', null, ['class' => 'form-control','id'=>'id_family']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('jenis_kelamin', 'Jenis Kelamin', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-4">
					<div class="input-group">
						{!! Form::select('dp[jenis_kelamin]', array(null=>'--Pilih--','L'=>'Laki-laki','P'=>'Perempuan','T'=>'Tidak Jelas'), null, ['class' => 'form-control', 'id'=>'jenis_kelamin']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label">
					<input class="age" name="tgl_lahir" type="radio" value="1">Tanggal Lahir</input>
				</label>
				<div class="col-sm-4">
					{!! Form::text('dp[tgl_lahir]', null, ['class' => 'form-control tgl_lahir datemax',"onchange"=>"getAge()",'placeholder'=>'Tanggal Lahir','id'=>'tgl_lahir','disabled']) !!}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label">
					<input class="age" name="tgl_lahir" type="radio" value="2">Umur</input>
				</label>
				<div class="col-sm-7">
					<div class="row">
						<div class="col-sm-4" style="padding-right: 0px;">
							<div class="input-group">
								{!! Form::text('dp[umur_thn]', null, ['class' => 'form-control umur','disabled','id'=>'umur_thn']) !!}
								<span class="input-group-addon">Thn</span>
							</div>
						</div>
						<div class="col-sm-4" style="padding-right: 0px;">
							<div class="input-group">
								{!! Form::text('dp[umur_bln]', null, ['class' => 'form-control umur','disabled','id'=>'umur_bln']) !!}
								<span class="input-group-addon">Bln</span>
							</div>
						</div>
						<div class="col-sm-4" style="padding-right: 0px;">
							<div class="input-group">
								{!! Form::text('dp[umur_hari]', null, ['class' => 'form-control umur','disabled','id'=>'umur_hari']) !!}
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('alamat', 'Alamat', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::textarea('dp[alamat]',null, ['class' => 'form-control','id'=>'alamat','rows'=>'2','placeholder'=>'Hanya diisi nama jalan, no. rumah dan no. RT - RW']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('wilayah', 'Wilayah Alamat Pasien', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					<div class="input-group">
						{!! Form::text('wilayah', null, ['class' => 'form-control','id'=>'wilayah','placeholder'=>'Desa, Kecamatan, Kabupaten, Provinsi']) !!}
						{!! Form::hidden('dp[code_kelurahan]', null, ['class' => 'form-control','id'=>'id_kelurahan_pasien']) !!}
						{!! Form::hidden('dp[code_kecamatan]', null, ['class' => 'form-control','id'=>'id_kecamatan_pasien']) !!}
						{!! Form::hidden('dp[code_kabupaten]', null, ['class' => 'form-control','id'=>'id_kabupaten_pasien']) !!}
						{!! Form::hidden('dp[code_provinsi]', null, ['class' => 'form-control','id'=>'id_provinsi_pasien']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('longitude', 'Longitude', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::text('dp[longitude]', null, ['class' => 'form-control','id'=>'longitude','placeholder'=>'Longitude']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('latitude', 'Latitude', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::text('dp[latitude]', null, ['class' => 'form-control','id'=>'latitude','placeholder'=>'Latitude']) !!}
				</div>
			</div>
			@if($role = Helper::role())
			<div class="form-group">
				{!! Form::label('nama_faskes', 'Nama Faskes saat periksa', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::text('', strtoupper($role->name_role).' '.$role->name_faskes, ['class' => 'form-control','id'=>'nama_faskes','placeholder'=>'Nama Faskes saat periksa','disabled']) !!}
				</div>
			</div>
			@endif
			<div class="form-group">
				{!! Form::label('no_epid', 'No.Epidemologi', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::text('dc[no_epid]', null, ['class' => 'form-control','id'=>'no_epid','placeholder'=>'No.Epidemologi','readonly']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('no_epid_lama', 'No.Epidemologi lama', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-6">
					{!! Form::text('dc[no_epid_lama]', null, ['class' => 'form-control','id'=>'no_epid_lama','placeholder'=>'No.Epidemologi Lama']) !!}
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('#nik').mask('999999999999999999');

		$("input[class='age']").on('ifChecked',function(){
			var val = $(this).val();
			if(val=='1'){
				$('.tgl_lahir').removeAttr('disabled');
				$('.umur').attr('disabled','disabled').val(null);
			}else{
				$('.umur').removeAttr('disabled');
				$('.tgl_lahir').attr('disabled','disabled').val(null);
			}
		});
		$('#wilayah').autocomplete({
			source: '{!! url('getArea'); !!}',
			minLength:3,
			focus: function(event, ui){
				$('#wilayah').val(ui.item.full_address);
				return false;
			},
			select:function(evt, ui){
				if(ui){
					var dt = ui.item;
					$('#id_kelurahan_pasien').val(dt.code_kelurahan);
					$('#id_kecamatan_pasien').val(dt.code_kecamatan);
					$('#id_kabupaten_pasien').val(dt.code_kabupaten);
					$('#id_provinsi_pasien').val(dt.code_provinsi);
				}
				getEpid();
				return false;
			}
		})
		.data("ui-autocomplete")._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append("<a>"+ item.name_kelurahan +"<br><small>"+
				"Kecamatan: <i>"+ item.name_kecamatan +"</i><br>"+
				"Kabupaten: <i>"+ item.name_kabupaten +"</i><br>"+
				"Provinsi: "+item.name_provinsi+"</small></a>")
			.appendTo( ul );
		};

		$('#name').autocomplete({
			source: BASE_URL+'pasien/getData',
			minLength:3,
			focus: function(event, ui){
				$('#name').val(ui.item.name);
				return false;
			},
			select:function(evt, ui){
				if(ui){
					var dt = ui.item;
					$('#id_pasien').val(dt.id);
					$('#id_family').val(dt.id_family);
					$('#nik').val(dt.nik);
					$('#nama_ortu').val(dt.nama_ortu);
					$('#agama').val(dt.agama).trigger('change');
					$('#jenis_kelamin').val(dt.jenis_kelamin).trigger('change');
					if(isset(dt.tgl_lahir)){$('#tgl_lahir').val(dt.tgl_lahir).removeAttr('disabled');}
					if(isset(dt.umur_hari)){$('#umur_hari').val(dt.umur_hari).removeAttr('disabled');}
					if(isset(dt.umur_bln)){$('#umur_bln').val(dt.umur_bln).removeAttr('disabled');}
					if(isset(dt.umur_thn)){$('#umur_thn').val(dt.umur_thn).removeAttr('disabled');}
					$('#alamat').val(dt.alamat);
					$('#wilayah').val(dt.full_address);
					$('#longitude').val(dt.longitude);
					$('#latitude').val(dt.latitude);
					$('#id_kelurahan_pasien').val(dt.code_kelurahan);
					$('#id_kecamatan_pasien').val(dt.code_kecamatan);
					$('#id_kabupaten_pasien').val(dt.code_kabupaten);
					$('#id_provinsi_pasien').val(dt.code_provinsi);
					getEpid();
				}
				return false;
			},
			messages:{
				noResults: function(argument){
					messageAlert("danger", "Info", "Data pasien tidak ditemukan.");
					$('#no_epid').val('');
					$('#id_pasien').val('');
					$('#id_family').val('');
					$('#nik').val('');
					$('#nama_ortu').val('');
					$('#agama').val('').trigger('change');
					$('#jenis_kelamin').val('').trigger('change');
					$('#tgl_lahir').val('').attr('disabled','disabled');
					$('#umur_thn').val('').attr('disabled','disabled');
					$('#umur_bln').val('').attr('disabled','disabled');
					$('#umur_hari').val('').attr('disabled','disabled');
					$('#wilayah').val('');
					$('#longitude').val('');
					$('#latitude').val('');
					$('#id_kelurahan_pasien').val('');
					$('#id_kecamatan_pasien').val('');
					$('#id_kabupaten_pasien').val('');
					$('#id_provinsi_pasien').val('');
					$('#alamat').val('');
					return false;
				}
			}
		})
		.data("ui-autocomplete")._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append("<a>"+ item.name +"<br><small>"+
				"NIK: <i>"+ item.nik +"</i><br>"+
				"Nama orang tua: <i>"+ item.nama_ortu +"</i><br>"+
				"Alamat: "+item.full_address+"</small></a>")
			.appendTo( ul );
		};
	});
</script>

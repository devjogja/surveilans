<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">
			Data spesimen dan hasil laboratorium
		</h3>
	</div>
	<div class="form-horizontal">
		<div class="box-body">
			<table class="table table-striped table-bordered" id="spesimen">
				<thead>
					<tr>
						<th>Spesimen</th>
						<th>Tanggal Pengambilan Spesimen</th>
						<th>Jenis Pemeriksaan Laboratorium</th>
						<th>Hasil</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			<div style="margin: 14px 0px;">
				<button class="btn btn-success btn-flat" id="addSpesimen" type="button">
					<i class="fa fa-plus-circle">
					</i>
					Tambah Sampel
				</button>
			</div>
			<div id="div_sampel" style="margin-top: 2%">
				<table class="table table-bordered" id="data_sampel">
					<tr>
						<td>Spesimen</td>
						<td>Tanggal Pengambilan Spesimen</td>
						<td>Jenis Pemeriksaan Laboratorium</td>
						<td>Hasil</td>
					</tr>
					<tr>
						<td>
							{!! Form::select(null, array(null=>'--Pilih--','1'=>'Spesimen Tool I','2'=>'Spesimen Tool II','3'=>'Spesimen Tool III','4'=>'Spesimen Tool IV','5'=>'Spesimen Tool V'), null, ['class' => 'form-control','id'=>'spesimen_tool']) !!}
						</td>
						<td>
							{!! Form::text(null, null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Pengambilan Spesimen','id'=>'tgl_ambil_spesimen','disabled']) !!}
						</td>
						<td>
							{!! Form::select(null, array(null=>'--Pilih--','1'=>'Isolasi virus','2'=>'ITD','3'=>'Sequencing'), null, ['class' => 'form-control','id'=>'jenis_pemeriksaan','disabled']) !!}
						</td>
						<td>
							{!! Form::text(null, null, ['class' => 'form-control','placeholder'=>'Hasil Pemeriksaan','id'=>'hasil','disabled']) !!}
						</td>
					</tr>
				</table>
				<div class="row">
					<div class="col-md-offset-4 col-md-8">
						<button type="button" class="btn btn-default" id="tutup_sampel">
							Tutup
						</button>
						<button type="button" class="btn btn-success" disabled="disabled" id="tambah_spesimen">
							Tambah
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('#div_sampel').hide();
		$('#tutup_sampel').on('click',function(){
			$('#div_sampel').hide(500);
			$('#addSpesimen').show();
			return false;
		});
		$('#addSpesimen').on('click',function(){
			$('#div_sampel').show(500);
			$('#addSpesimen').hide();
			return false;
		});
		$('#spesimen_tool').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#tgl_ambil_spesimen').removeAttr('disabled');
			}else{
				$('#tgl_ambil_spesimen').attr('disabled','disabled');
			}
			$('#hasil').attr('disabled','disabled').val(null);
			return false;
		});
		$('#tgl_ambil_spesimen').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#jenis_pemeriksaan').removeAttr('disabled');
			}else{
				$('#jenis_pemeriksaan').attr('disabled','disabled').select2('val','');
			}
			if(isset(val)){
				$('#tambah_spesimen').removeAttr('disabled');
			}else{
				$('#tambah_spesimen').attr('disabled','disabled');
			}
			$('#hasil').attr('disabled','disabled').val(null);
			return false;
		});
		$('#jenis_pemeriksaan').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#hasil').removeAttr('disabled');
			}else{
				$('#hasil').attr('disabled','disabled').val(null);
			}
			return false;
		});
		$('#tambah_spesimen').on('click',function(){
			var st = $('#spesimen_tool option:selected').text();
			var s = $('#spesimen_tool').val();
			var ts = $('#tgl_ambil_spesimen').val();
			var jpt = $('#jenis_pemeriksaan option:selected').text();
			var jp = $('#jenis_pemeriksaan').val();
			var h = $('#hasil').val();
			$('#spesimen tbody').append('<tr>'+
				'<td>'+st+'</td>'+
				'<td>'+ts+'</td>'+
				'<td>'+jpt+'</td>'+
				'<td>'+h+'</td>'+
				'<td><a href="javascript:void(0);" class="rm btn-sm btn-danger"><i class="fa fa-remove"></i></a>'+
				'<input type="hidden" name="ds[spesimen][]" value="'+s+'|'+ts+'|'+jp+'|'+h+'"></td>'+
				'</tr>'
				);
			$('#spesimen_tool').select2('val','');
			$('#tgl_ambil_spesimen').val(null).attr('disabled','disabled');
			$('#jenis_pemeriksaan').select2('val','');
			$('#jenis_pemeriksaan').attr('disabled','disabled');
			$('#hasil').val(null).attr('disabled','disabled');
			$('#tambah_spesimen').attr('disabled','disabled');
			$('.rm').on('click',function(){
				$(this).parent().parent().remove();
			});
			return false;
		});
	});
</script>

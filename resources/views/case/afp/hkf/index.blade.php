@extends('layouts.base')
@section('content')
<style type="text/css">
	.gejala tr td{
		width: 50%;
		padding-bottom: 4px;
	}
</style>
<div class="col-sm-12">
	<div class="row">
		{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form', 'class' => 'form-horizontal']) !!}
		{!! Form::hidden('id_trx_case', $data['id_trx_case'], ['id'=>'id_trx_case']) !!}
		{!! Form::hidden('id_hkf', $data['id_hkf'], ['id'=>'id_hkf']) !!}
		<div class="col-sm-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">
						Identitas Penderita
					</h3>
				</div>
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group">
							<div class="form-group">
								{!! Form::label(null, 'No. Epidemologi', ['class' => 'col-sm-3 control-label']) !!}
								<div class="col-sm-4">
									{!! Form::text('', null, ['class' => 'form-control','id'=>'no_epid','placeholder'=>'No. Epidemologi']) !!}
								</div>
							</div>
							<div class="form-group">
								{!! Form::label(null, 'Nama', ['class' => 'col-sm-3 control-label']) !!}
								<div class="col-sm-4">
									{!! Form::text('', null, ['class' => 'form-control','id'=>'name','placeholder'=>'Nama Pasien']) !!}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">
						Klasifikasi Final
					</h3>
				</div>
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group">
							<div class="form-group">
								{!! Form::label(null, 'Hasil Klasifikasi Final', ['class' => 'col-sm-3 control-label']) !!}
								<div class="col-sm-5">
									<div class="form-group">
										<label class="col-sm-8">
											<input class="hasil_klasifikasi" name="dkf[hasil_klasifikasi]" type="radio" value="1">
											Bukan Kasus Polio
										</input>
									</label>
								</div>
								<div class="form-group">
									<label class="col-sm-8">
										<input class="hasil_klasifikasi" name="dkf[hasil_klasifikasi]" type="radio" value="2">
										Polio Kompatibel
									</input>
								</label>
							</div>
							<div class="form-group">
								<label class="col-sm-8">
									<input class="hasil_klasifikasi" name="dkf[hasil_klasifikasi]" type="radio" value="3">
									Vaccine Associated Polio Paralytic (VAPP)
								</input>
							</label>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Diagnosa', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-8">
								{!! Form::text('dkf[diagnosis]', null, ['class' => 'form-control','placeholder'=>'Diagnosa','id'=>'diagnosis',]) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kriteria untuk menentukan klasifikasi final', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-6">
					<table>
						<tr height="30px">
							<td width="4%">
								<input class="col-sm-1" id="isolasi_cirus_polio_vaksin" name="dkf[isolasi_cirus_polio_vaksin]" value="1" type="checkbox"/>
							</td>
							<td width="40%">
								Isolasi cirus-polio vaksin dari spesimen
							</td>
							<td>
							</td>
						</tr>
						<tr height="30px">
							<td>
								<input class="col-sm-1" id="demam" name="dkf[demam]" value="1" type="checkbox"/>
							</td>
							<td>
								Ada demam
							</td>
							<td>
							</td>
						</tr>
						<tr height="30px">
							<td>
								<input class="col-sm-1" id="sifat_kelumpuhan_simetris" name="dkf[sifat_kelumpuhan_simetris]" value="1" type="checkbox"/>
							</td>
							<td>
								Sifat kelumpuhan simetris/asimestris
							</td>
							<td>
							</td>
						</tr>
						<tr height="30px">
							<td>
								<input class="col-sm-1" id="gangguan_rasa_raba" name="dkf[gangguan_rasa_raba]" value="1" type="checkbox"/>
							</td>
							<td>
								Ada gangguan rasa raba
							</td>
							<td>
							</td>
						</tr>
						<tr height="30px">
							<td>
								<input class="col-sm-1" id="paralisis_residual" name="dkf[paralisis_residual]" value="1" type="checkbox"/>
							</td>
							<td>
								Paralisis residual pada kunjungan ulang 60 hari
							</td>
							<td>
							</td>
						</tr>
						<tr height="30px">
							<td>
								<input class="col-sm-1" id="meninggal" name="dkf[meninggal]" value="1" type="checkbox"/>
							</td>
							<td>
								Meninggal
							</td>
							<td>
							</td>
						</tr>
						<tr height="30px">
							<td>
								<input class="col-sm-1" id="imunisasi_polio" name="dkf[imunisasi_polio]" value="1" type="checkbox"/>
							</td>
							<td>
								Mendapat imunisasi polio
							</td>
							<td style="padding-left: 35px;">
								{!! Form::text('dkf[dosis_imunisasi]', null, ['class' => 'form-control','id'=>"dosis_imunisasi",'placeholder'=>'Dosis imunisasi']) !!}
							</td>
						</tr>
						<tr height="30px">
							<td>
								<input class="col-sm-1" id="imunisasi_polio_terakhir" name="dkf[imunisasi_polio_terakhir]" value="1" type="checkbox"/>
							</td>
							<td>
								Tanggal imunisasi polio terakhir 4-35 hari / 4-75 hari sebelum lumpuh
							</td>
							<td>
							</td>
						</tr>
						<tr height="30px">
							<td>
								<input class="col-sm-1" id="follow_up" name="dkf[follow_up]" value="1" type="checkbox"/>
							</td>
							<td>
								Tak dapat di-<i>follow up</i>
							</td>
							<td>
							</td>
						</tr>
						<tr height="30px">
							<td>
								<input class="col-sm-1" id="daerah_klb_polio" name="dkf[daerah_klb_polio]" value="1" type="checkbox"/>
							</td>
							<td>
								Daerah KLB Polio
							</td>
							<td>
							</td>
						</tr>
						<tr height="30px">
							<td>
								<input class="col-sm-1" id="hub_epidemologi_kasus_polio_daerah_klb" name="dkf[hub_epidemologi_kasus_polio_daerah_klb]" value="1" type="checkbox"/>
							</td>
							<td>
								Ada hubungan epidemiologi dengan kasus polio di daerah KLB
							</td>
							<td>
							</td>
						</tr>
						<tr height="30px">
							<td>
								<input class="col-sm-1" id="clustering_kasus_afp" name="dkf[clustering_kasus_afp]" value="1" type="checkbox"/>
							</td>
							<td>
								<i>Clustering</i>kasus AFP
							</td>
							<td>
							</td>
						</tr>
						<tr height="30px">
							<td>
								<input class="col-sm-1" id="lain_lain" name="dkf[lain_lain]" value="1" type="checkbox"/>
							</td>
							<td>
								Lain-lain,sebutkan
							</td>
							<td style="padding-left: 35px;">
								{!! Form::text('dkf[lain_lain_val]', null, ['class' => 'form-control','id'=>"lain_lain_val",'placeholder'=>'Kriteria lain']) !!}
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="footer">
			{!! Form::reset("Batal", ['class' => 'btn btn-warning batal']) !!}
			{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
		</div>
	</div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
	$('#imunisasi_polio').on('change',function(){
		var val = $(this).val();
		if(val==1){
			$('#jml_dosis').removeAttr('disabled');
		}else{
			$('#jml_dosis').attr('disabled','disabled').val(null);
		}
		return false;
	});
	$(function(){
		$('.batal').on('click',function(){
			window.location.href = '{!! url('case/afp'); !!}';
			return false;
		});
		$('#form').validate({
			rules:{
				'dp[name]':'required',
			},
			messages:{
				'dp[name]':'Nama pasien wajib di isi',
			},
			submitHandler: function(){
				var action = BASE_URL+'case/afp/hkf/store';
				var data = $('#form').serializeJSON();
				$.ajax({
					method  : "POST",
					url     : action,
					data    : JSON.stringify([data]),
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
					success: function(data, status){
						if (data.success==true) {
							window.location.href = '{!! url('case/afp'); !!}';
						}else{
							messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
							endProcess();
						}
					}
				});
				return false;
			}
		});
	});
</script>
@endsection

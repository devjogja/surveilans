<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">
			Data surveilans AFP
		</h3>
	</div>
	<div class="form-horizontal">
		<div class="box-body">
			<div class="form-group">
				{!! Form::label(null, 'Tanggal mulai lumpuh (bukan karena ruda paksa)', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					<div class="input-group">
						{!! Form::text('dk[tgl_mulai_lumpuh]', null, ['class' => 'form-control datemax','id'=>'tgl_mulai_lumpuh',"onchange"=>"getAge();getEpid();",'placeholder'=>'Tanggal mulai lumpuh (bukan karena ruda paksa)']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Demam sebelum lumpuh (bukan karena ruda paksa)', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[demam_sebelum_lumpuh]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control','id'=>'demam_sebelum_lumpuh']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('imunisasi_rutin_polio_sebelum_sakit', 'Imunisasi rutin polio sebelum sakit', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-3">
					{!! Form::select('dk[imunisasi_rutin_polio_sebelum_sakit]', array(null=>'Jumlah Dosis','1'=>'1x','2'=>'2x','3'=>'3x','4'=>'4x','5'=>'5x','6'=>'6x','7'=>'Tidak','8'=>'Tidak tahu','9'=>'Belum pernah'), null, ['class' => 'form-control','id'=>'imunisasi_rutin_polio_sebelum_sakit','onchange'=>"activeinformasi('imunisasi_rutin_polio_sebelum_sakit')"]) !!}
				</div>
				<div class="col-sm-4">
					{!! Form::select('dk[informasi_imunisasi_rutin_polio_sebelum_sakit]', array('0'=>'Sumber Informasi','1'=>'KMS/Catatan Jurim','2'=>'Ingatan Responden','3'=>'Tidak Tahu'), null, ['class' => 'form-control','id'=>'informasi_imunisasi_rutin_polio_sebelum_sakit','disabled']) !!}
				</div>
				<span class="col-sm-1 al">
					(*)
				</span>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'PIN, Mop-up, ORI, BIAS Polio', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-3">
					{!! Form::select('dk[pin_mopup_ori_biaspolio]', array(null=>'Jumlah Dosis','1'=>'1x','2'=>'2x','3'=>'3x','4'=>'4x','5'=>'5x','6'=>'6x','7'=>'Tidak','8'=>'Tidak tahu','9'=>'Belum pernah'), null, ['class' => 'form-control','id'=>'pin_mopup_ori_biaspolio','onchange'=>"activeinformasi('pin_mopup_ori_biaspolio')"]) !!}
				</div>
				<div class="col-sm-4">
					{!! Form::select('dk[informasi_pin_mopup_ori_biaspolio]', array('0'=>'Sumber Informasi','1'=>'KMS/Catatan Jurim','2'=>'Ingatan Responden','3'=>'Tidak Tahu'), null, ['class' => 'form-control','id'=>'informasi_pin_mopup_ori_biaspolio','disabled']) !!}
				</div>
				<span class="col-sm-1 al">
					(*)
				</span>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Tanggal imunisasi polio terakhir', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dk[tgl_imunisasi_polio_terakhir]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal imunisasi polio terakhir','id'=>'tgl_imunisasi_polio_terakhir']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Tanggal laporan diterima', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dk[tgl_laporan_diterima]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal laporan diterima','id'=>'tgl_laporan_diterima']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Tanggal pelacakan', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dk[tgl_pelacakan]', null, ['class' => 'form-control dates','placeholder'=>'Tanggal pelacakan','id'=>'tgl_pelacakan']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Riwayat Kontak', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[kontak]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control','id'=>'kontak']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Keadaan akhir', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[keadaan_akhir]', array(null=>'--Pilih--','1'=>'Hidup','2'=>'Meninggal'), null, ['class' => 'form-control', 'id'=>'keadaan_akhir']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Status Kasus', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					@if(Helper::role()->id_role=='1' || Helper::role()->id_role=='2')
					{!! Form::select('dk[hot_case]', array(null=>'--Pilih--','1'=>'Hot Case','2'=>'Bukan Hot Case'), null, ['class' => 'form-control', 'id'=>'hot_case','disabled']) !!}
					@else
					{!! Form::select('dk[hot_case]', array(null=>'--Pilih--','1'=>'Hot Case','2'=>'Bukan Hot Case'), null, ['class' => 'form-control', 'id'=>'hot_case']) !!}
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function activeinformasi(id) {
		var val = $('#'+id).val();
		if(val=='0'){
			$('#informasi_'+id).attr('disabled','disabled').select2('val','0');
		}else{
			$('#informasi_'+id).removeAttr('disabled');
		}
		return false;
	}
</script>

@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			@include('case.afp.input_kasus')
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		getDetail();
	});

	function getDetail() {
		var id = $('#id_trx_case').val();
		var action = BASE_URL+'case/afp/getDetail/'+id;
		$.getJSON(action, function(result){
			var raw = result.response;
			if (isset(raw)) {
				$.each(raw, function(k, dt){
					$('#id_faskes').val(dt.id_faskes);
					$('#id_role').val(dt.id_role);
					$('#jenis_input').val(dt.jenis_input);
					$('#id_trx_case').val(dt.id);
					$('#name').val(dt.name_pasien);
					$('#id_pasien').val(dt.id_pasien);
					$('#nik').val(dt.nik);
					$('#no_rm').val(dt.no_rm);
					$('#agama').val(dt.agama).trigger('change');
					$('#nama_ortu').val(dt.nama_ortu);
					$('#id_family').val(dt.id_family);
					$('#jenis_kelamin').val(dt.jenis_kelamin).trigger('change');
					if(isset(dt.tgl_lahir)){$('#tgl_lahir').val(dt.tgl_lahir).removeAttr('disabled');}
					if(isset(dt.umur_hari)){$('#umur_hari').val(dt.umur_hari).removeAttr('disabled');}
					if(isset(dt.umur_bln)){$('#umur_bln').val(dt.umur_bln).removeAttr('disabled');}
					if(isset(dt.umur_thn)){$('#umur_thn').val(dt.umur_thn).removeAttr('disabled');}
					$('#alamat').val(dt.alamat);
					$('#wilayah').val(dt.full_address);
					$('#longitude').val(dt.longitude);
					$('#latitude').val(dt.latitude);
					$('#id_kelurahan_pasien').val(dt.code_kelurahan_pasien);
					$('#id_kecamatan_pasien').val(dt.code_kecamatan_pasien);
					$('#id_kabupaten_pasien').val(dt.code_kabupaten_pasien);
					$('#id_provinsi_pasien').val(dt.code_provinsi_pasien);
					$('#name_faskes').val(dt.name_faskes);
					$('#no_epid').val(dt.no_epid);
					$('#no_epid_lama').val(dt.no_epid_lama);
					$('#tgl_mulai_lumpuh').val(dt.tgl_mulai_lumpuh);
					$('#demam_sebelum_lumpuh').val(dt.demam_sebelum_lumpuh).trigger('change');
					$('#imunisasi_rutin_polio_sebelum_sakit').val(dt.imunisasi_rutin_polio_sebelum_sakit).trigger('change');
					$('#informasi_imunisasi_rutin_polio_sebelum_sakit').val(dt.informasi_imunisasi_rutin_polio_sebelum_sakit).trigger('change');
					$('#pin_mopup_ori_biaspolio').val(dt.pin_mopup_ori_biaspolio).trigger('change');
					$('#informasi_pin_mopup_ori_biaspolio').val(dt.informasi_pin_mopup_ori_biaspolio).trigger('change');
					$('#tgl_imunisasi_polio_terakhir').val(dt.tgl_imunisasi_polio_terakhir);
					$('#tgl_laporan_diterima').val(dt.tgl_laporan_diterima);
					$('#tgl_pelacakan').val(dt.tgl_pelacakan);
					$('#kontak').val(dt.kontak).trigger('change');
					$('#keadaan_akhir').val(dt.keadaan_akhir).trigger('change');
					$('#hot_case').val(dt.hot_case).trigger('change');

					var dtGejala = dt.dtGejala;
					$.each(dtGejala, function(key, val){
						$('#kelumpuhan_'+val.id_gejala).val(val.val_kelumpuhan).trigger('change');
						$('#gangguan_raba_'+val.id_gejala).val(val.val_gangguan_raba).trigger('change');
						if(val.id_gejala==null){
							add_gejala(key,val);
						}
					});
					var dtSpesimen = dt.dtSpesimen;
					$.each(dtSpesimen, function(key, val){
						var jp = val.jenis_pemeriksaan_txt != null ? val.jenis_pemeriksaan_txt : '';
						$('#spesimen tbody').append('<tr>'+
							'<td>'+val.jenis_spesimen_txt+'</td>'+
							'<td>'+val.tgl_ambil_spesimen+'</td>'+
							'<td>'+jp+'</td>'+
							'<td>'+val.hasil+'</td>'+
							'<td class="text-center"><a href="javascript:void(0);" class="rm btn-sm btn-danger"><i class="fa fa-remove"></i></a>'+
							'<input type="hidden" name="ds[spesimen][]" value="'+val.jenis_spesimen+'|'+val.tgl_ambil_spesimen+'|'+val.jenis_pemeriksaan+'|'+val.hasil+'"></td>'+
							'</tr>'
							);
					});
					$('.rm').on('click',function(){
						$(this).parent().parent().remove();
						return false;
					});
					$('#klasifikasi_final').val(dt.klasifikasi_final).trigger('change');
				});
				return false;
			};
		});
	}
</script>

@endsection

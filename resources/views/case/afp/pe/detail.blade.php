@extends('layouts.base')
@section('content')
@if($dt = $data['response'])
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			<table class="table table-striped">
				<caption>Identitas Penderita</caption>
				<tbody>
					<tr>
						<td class="title">No Epidemologi</td>
						<td>{!! $dt->no_epid or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">No Epidemologi Lama</td>
						<td>{!! $dt->no_epid_lama or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Penderita</td>
						<td>{!! $dt->name_pasien or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">NIK</td>
						<td>{!! $dt->nik or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">No. RM</td>
						<td>{!! $dt->no_rm or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Agama</td>
						<td>{!! $dt->religion_text or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Orang Tua</td>
						<td>{!! $dt->nama_ortu or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Jenis Kelamin</td>
						<td>{!! $dt->jenis_kelamin_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Lahir(Umur)</td>
						<td><?php
							$tgl_lahir = ($dt->tgl_lahir)?$dt->tgl_lahir:'-';
							$umur_thn = ($dt->umur_thn)?$dt->umur_thn.' Th':'';
							$umur_bln = ($dt->umur_bln)?$dt->umur_bln.' Bln':'';
							$umur_hari = ($dt->umur_hari)?$dt->umur_hari.' Hari':'';
							?>
							{!! $tgl_lahir !!} ({!!$umur_thn!!} {!!$umur_bln !!} {!!$umur_hari!!})</td>
						</tr>
						<tr>
							<td class="title">Alamat</td>
							<td>{!! $dt->alamat or '' !!} {!! $dt->full_address or '' !!}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<caption>Sumber Informasi</caption>
					<tbody>
						<tr>
							<td class="title">Provinsi</td>
							<td>{!! $dt->name_provinsi_sumber_informasi or '-' !!}</td>
						</tr>
						<tr>
							<td class="title">Kabupaten</td>
							<td>{!! $dt->name_kabupaten_sumber_informasi or '-' !!}</td>
						</tr>
						<tr>
							<td class="title">Laporan dari</td>
							<td>{!! $dt->sumber_laporan_txt or '-' !!}</td>
						</tr>
						<tr>
							<td class="title">Keterangan sumber laporan</td>
							<td>{!! $dt->keterangan or '-' !!}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<caption>Riwayat Sakit</caption>
					<tbody>
						<tr>
							<td class="title">Tanggal mulai sakit</td>
							<td colspan="2">{!! $dt->tgl_sakit_pe or '-' !!}</td>
						</tr>
						<tr>
							<td class="title">Tanggal mulai lumpuh</td>
							<td colspan="2">{!! $dt->tgl_mulai_lumpuh or '-' !!}</td>
						</tr>
						<tr>
							<td class="title">Tanggal meninggal (bila penderita meninggal)</td>
							<td colspan="2">{!! $dt->tgl_meninggal or '-' !!}</td>
						</tr>
						<tr>
							<td class="title">Sebelum dilaporkan, apakah penderita berobat ke unit pelayanan lain?</td>
							<td>{!! $dt->berobat_ke_unit_pelayanan_lain_txt or '-' !!}</td>
							<td>
								<table class="table table-striped">
									<tbody>
										<tr>
											<td class="title">Nama unit pelayanan</td>
											<td>{!! $dt->nama_unit_pelayanan or '-' !!}</td>
										</tr>
										<tr>
											<td class="title">Tanggal berobat</td>
											<td>{!! $dt->tgl_berobat or '-' !!}</td>
										</tr>
										<tr>
											<td class="title">Diagnosis</td>
											<td>{!! $dt->diagnosis or '-' !!}</td>
										</tr>
										<tr>
											<td class="title">No.Rekam medis</td>
											<td>{!! $dt->no_rm_lama or '-' !!}</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td class="title">Apakah kelumpuhan sifatnya akut (1-14 hari)?</td>
							<td colspan="2">{!! $dt->kelumpuhan_sifat_akut_txt or '-' !!}</td>
						</tr>
						<tr>
							<td class="title">Apakah kelumpuhan sifatnya layuh (flaccid)?</td>
							<td colspan="2">{!! $dt->kelumpuhan_sifat_layuh_txt or '-' !!}</td>
						</tr>
						<tr>
							<td class="title">Apakah kelumpuhan disebabkan ruda?</td>
							<td colspan="2">{!! $dt->kelumpuhan_disebabkan_ruda_txt or '-' !!}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<caption>Gejala</caption>
					<tbody>
						<tr>
							<td class="title">Apakah penderita demam sebelum lumpuh ?</td>
							<td>{!! $dt->demam_sebelum_lumpuh_txt or '-' !!}</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-striped">
					<thead>
						<tr>
							<th></th>
							<th>Kelumpuhan</th>
							<th>Gangguan Raba</th>
						</tr>
					</thead>
					<tbody>
						@if(empty($dt->dtGejala))
						<tr><td colspan="3">Tidak ada gejala.</td></tr>
						@else
						<tr>
							<td>Tungkai Kanan</td>
							<?php
							$v31_kelumpuhan =$v43_kelumpuhan = '';
							if(isset($dt->dtGejala[31]->val_kelumpuhan_txt)){$v31_kelumpuhan = $dt->dtGejala[31]->val_kelumpuhan_txt;}
							if(isset($dt->dtGejala[43]->val_gangguan_raba_txt)){$v43_kelumpuhan = $dt->dtGejala[43]->val_gangguan_raba_txt;}
							?>
							<td>{!! $v31_kelumpuhan !!}</td>
							<td>{!! $v43_kelumpuhan !!}</td>
						</tr>
						<tr>
							<td>Tungkai Kiri</td>
							<?php
							$v32_kelumpuhan =$v44_kelumpuhan = '';
							if(isset($dt->dtGejala[32]->val_kelumpuhan_txt)){$v32_kelumpuhan = $dt->dtGejala[32]->val_kelumpuhan_txt;}
							if(isset($dt->dtGejala[44]->val_gangguan_raba_txt)){$v44_kelumpuhan = $dt->dtGejala[44]->val_gangguan_raba_txt;}
							?>
							<td>{!! $v32_kelumpuhan !!}</td>
							<td>{!! $v44_kelumpuhan !!}</td>
						</tr>
						<tr>
							<td>Lengan Kanan</td>
							<?php
							$v33_kelumpuhan =$v45_kelumpuhan = '';
							if(isset($dt->dtGejala[33]->val_kelumpuhan_txt)){$v33_kelumpuhan = $dt->dtGejala[33]->val_kelumpuhan_txt;}
							if(isset($dt->dtGejala[45]->val_gangguan_raba_txt)){$v45_kelumpuhan = $dt->dtGejala[45]->val_gangguan_raba_txt;}
							?>
							<td>{!! $v33_kelumpuhan !!}</td>
							<td>{!! $v45_kelumpuhan !!}</td>
						</tr>
						<tr>
							<td>Lengan Kiri</td>
							<?php
							$v34_kelumpuhan =$v46_kelumpuhan = '';
							if(isset($dt->dtGejala[34]->val_kelumpuhan_txt)){$v34_kelumpuhan = $dt->dtGejala[34]->val_kelumpuhan_txt;}
							if(isset($dt->dtGejala[46]->val_gangguan_raba_txt)){$v46_kelumpuhan = $dt->dtGejala[46]->val_gangguan_raba_txt;}
							?>
							<td>{!! $v34_kelumpuhan !!}</td>
							<td>{!! $v46_kelumpuhan !!}</td>
						</tr>
						@foreach($dt->dtGejala AS $k=>$v)
						@if(empty($v->id_gejala))
						<tr>
							<td>{!! $v->other_gejala !!}</td>
							<td>{!! $v->val_kelumpuhan_txt !!}</td>
							<td>{!! $v->val_gangguan_raba_txt !!}</td>
						</tr>
						@endif
						@endforeach
						@endif
					</tbody>
				</table>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<caption>Riwayat Kontak</caption>
					<tbody>
						<tr>
							<td class="title">Dalam satu bulan terakhir sebelum sakit, apakah penderita pernah bepergian ?</td>
							<td>{!! $dt->bepergian_sebelum_sakit_txt or '-' !!}</td>
							<td>
								<table class="table table-striped">
									<tbody>
										<tr>
											<td class="title">Lokasi</td>
											<td>{!! $dt->lokasi_pergi_sebelum_sakit or '-' !!}</td>
										</tr>
										<tr>
											<td class="title">Tanggal pergi</td>
											<td>{!! $dt->tgl_pergi_sebelum_sakit or '-' !!}</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td class="title">Dalam satu bulan terakhir sebelum sakit, apakah penderita pernah berkunjung ke rumah anak yang baru mendapat imunisasi polio ?</td>
							<td colspan="2">{!! $dt->berkunjung_ke_rumah_anak_imunisasi_polio_txt or '-' !!}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<caption>Status imunisasi polio</caption>
					<tbody>
						<tr>
							<td class="title">Imunisasi rutin</td>
							<td>{!! $dt->imunisasi_rutin_polio_sebelum_sakit_txt or '-' !!}</td>
							<td class="title">Sumber Imunisasi rutin</td>
							<td>{!! $dt->informasi_imunisasi_rutin_polio_sebelum_sakit_txt or '-' !!}</td>
						</tr>
						<tr>
							<td class="title">PIN, Mop-up, ORI, BIAS Polio</td>
							<td>{!! $dt->pin_mopup_ori_biaspolio_txt or '-' !!}</td>
							<td class="title">Sumber PIN, Mop-up, ORI, BIAS Polio</td>
							<td>{!! $dt->informasi_pin_mopup_ori_biaspolio_txt or '-' !!}</td>
						</tr>
						<tr>
							<td class="title">Tanggal imunisasi polio terakhir</td>
							<td colspan="3">{!! $dt->tgl_imunisasi_polio_terakhir or '-' !!}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<caption>Pengumpulan spesimen</caption>
					<tbody>
						<tr>
							<td class="title">Apakah spesimen diambil</td>
							<td>{!! $dt->pengambilan_spesimen_txt or '-' !!}</td>
						</tr>
						<tr>
							<td class="title">Tidak diambil spesimen, berikan alasannya</td>
							<td>{!! $dt->alasan_spesimen_tidak_diambil or '-' !!}</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-striped">
					<thead>
						<tr>
							<td>Spesimen</td>
							<td>Tanggal ambil</td>
							<td>Tanggal kirim ke Kabupaten/Kota</td>
							<td>Tanggal kirim provinsi</td>
							<td>Tanggal Kirim Laboratorium</td>
						</tr>
					</thead>
					<tbody>
						@if(empty($dt->dtSpesimen))
						<tr><td colspan="5">Tidak terdapat data spesimen yang diambil.</td></tr>
						@else
						@foreach($dt->dtSpesimen AS $k=>$vs)
						<tr>
							<td>{!! $vs->jenis_spesimen_txt or '-' !!}</td>
							<td>{!! $vs->tgl_ambil_spesimen or '-' !!}</td>
							<td>{!! $vs->tgl_kirim_kab or '-' !!}</td>
							<td>{!! $vs->tgl_kirim_prov or '-' !!}</td>
							<td>{!! $vs->tgl_kirim_lab or '-' !!}</td>
						</tr>
						@endforeach
						@endif
					</tbody>
				</table>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<caption>Petugas Pelacak</caption>
					<tbody>
						<tr>
							<td class="title">Nama Petugas</td>
							<td>
								<table class="table table-striped">
									<tbody>
										@if(empty($dt->dtPelaksana))
										<tr><td>Tidak ada petugas pelacak.</td></tr>
										@else
										@foreach($dt->dtPelaksana AS $k=>$v)
										<tr>
											<td>{!! $v->nama_pelaksana or '-' !!}</td>
										</tr>
										@endforeach
										@endif
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<caption>Hasil pemeriksaan</caption>
					<tbody>
						<tr>
							<td class="title">Diagnosis</td>
							<td>{!! $dt->diagnosis_hasil_pemeriksaan or '-' !!}</td>
						</tr>
						<tr>
							<td class="title">Nama DSA/DSS/DRM/Dr/pemeriksa lain</td>
							<td>{!! $dt->nama_pemeriksa or '-' !!}</td>
						</tr>
						<tr>
							<td class="title">No.Telp</td>
							<td>{!! $dt->no_telp or '-' !!}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@endif
	<div class="col-sm-12">
		<div class="footer">
			<a href="{!!URL::to('case/afp#tab_4')!!}" class="btn btn-primary">Kembali</a>
		</div>
	</div>
	@endsection
@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="alert alert-notif alert-dismissable">
		<i class="icon fa fa-warning"></i> Text input yang bertanda bintang (*) wajib di isi
	</div>
	{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form', 'class' => 'form-horizontal']) !!}
	{!! Form::hidden('id_trx_case', $data['id_trx_case'], ['id'=>'id_trx_case']) !!}
	{!! Form::hidden('id_trx_pe', $data['id_trx_pe'], ['id'=>'id_trx_pe']) !!}
	{!! Form::hidden('dpe[id_faskes]', Helper::role()->id_faskes,['id'=>'id_faskes']) !!}
	{!! Form::hidden('dpe[id_role]', Helper::role()->id_role,['id'=>'id_role']) !!}
	{!! Form::hidden('dpe[jenis_input]', '1',['id'=>'jenis_input']) !!}
	<div class="row">
		<div class="col-sm-6">
			@include('case.afp.form_pasien')
		</div>
		<div class="col-sm-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">
						Sumber Informasi
					</h3>
				</div>
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Provinsi', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dsi[code_provinsi_sumber_informasi]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control','id'=>'id_provinsi_sumber_informasi','onchange'=>"getKabupaten('_sumber_informasi')"]) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Kabupaten', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dsi[code_kabupaten_sumber_informasi]', array(null=>'Pilih Kabupaten'), null, ['class' => 'form-control', 'id'=>'id_kabupaten_sumber_informasi']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Laporan dari', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dsi[sumber_laporan]', array(null=>'--Pilih--','1'=>'Rumah sakit','2'=>'Puskesmas','3'=>'Dokter Praktek','4'=>'Lainnya'), null, ['class' => 'form-control', 'id'=>'laporan_dari']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Keterangan sumber laporan', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::textarea('dsi[keterangan]', null, ['class' => 'form-control','id'=>'ket_sumber_laporan','placeholder'=>'Keterangan sumber laporan','rows'=>'3']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">
						Riwayat Sakit
					</h3>
				</div>
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Tanggal mulai sakit', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drs[tgl_sakit]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal mulai sakit','id'=>'tgl_sakit_pe']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Tanggal mulai lumpuh', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drs[tgl_mulai_lumpuh]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal mulai lumpuh','id'=>'tgl_mulai_lumpuh']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Tanggal meninggal (bila penderita meninggal)', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drs[tgl_meninggal]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal meninggal','id'=>'tgl_meninggal']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Sebelum dilaporkan, apakah penderita berobat ke unit pelayanan lain?', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-2">
								{!! Form::select('drs[berobat_ke_unit_pelayanan_lain]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'berobat_ke_unit_pelayanan_lain']) !!}
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									{!! Form::label(null, 'Nama unit pelayanan', ['class' => 'col-sm-4 control-label']) !!}
									<div class="col-sm-7">
										{!! Form::text('drs[nama_unit_pelayanan]', null, ['class' => 'form-control pelayananlain','placeholder'=>'Nama unit pelayanan','id'=>'nama_unit_pelayanan','disabled']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label(null, 'Tanggal berobat', ['class' => 'col-sm-4 control-label']) !!}
									<div class="col-sm-7">
										{!! Form::text('drs[tgl_berobat]', null, ['class' => 'form-control datemax pelayananlain','placeholder'=>'Tanggal berobat','id'=>'tgl_berobat','disabled']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label(null, 'Diagnosis', ['class' => 'col-sm-4 control-label']) !!}
									<div class="col-sm-7">
										{!! Form::text('drs[diagnosis]', null, ['class' => 'form-control pelayananlain','placeholder'=>'Diagnosis','id'=>'diagnosis','disabled']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label(null, 'No.Rekam medis', ['class' => 'col-sm-4 control-label']) !!}
									<div class="col-sm-7">
										{!! Form::text('drs[no_rm_lama]', null, ['class' => 'form-control pelayananlain','placeholder'=>'No.Rekam medis','id'=>'no_rekmed','disabled']) !!}
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Apakah kelumpuhan sifatnya akut (1-14 hari)?', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('drs[kelumpuhan_sifat_akut]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak Jelas'), null, ['class' => 'form-control', 'id'=>'kelumpuhan_sifat_akut']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Apakah kelumpuhan sifatnya layuh (flaccid)?', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('drs[kelumpuhan_sifat_layuh]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak Jelas'), null, ['class' => 'form-control', 'id'=>'kelumpuhan_sifat_layuh']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Apakah kelumpuhan disebabkan ruda?', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('drs[kelumpuhan_disebabkan_ruda]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak Jelas'), null, ['class' => 'form-control', 'id'=>'kelumpuhan_disebabkan_ruda']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">
						Gejala Tanda
					</h3>
				</div>
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Apakah penderita demam sebelum lumpuh ?', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-2">
								{!! Form::select('dg[demam_sebelum_lumpuh]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'demam_sebelum_lumpuh']) !!}
							</div>
						</div>
						<table class="table table-striped" id="gejala">
							<thead style="background: #4caf50;color: white;">
								<tr>
									<th></th>
									<th width="30%">Kelumpuhan</th>
									<th width="30%">Gangguan Raba</th>
									<th width='5%'></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Tungkai Kanan</td>
									<td>
										{!! Form::select("dg[kelumpuhan][31]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'kelumpuhan_31']) !!}
									</td>
									<td>
										{!! Form::select("dg[gangguan_raba][39]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'gangguan_raba_39']) !!}
									</td>
									<td></td>
								</tr>
								<tr>	
									<td>Tungkai Kiri</td>
									<td>
										{!! Form::select("dg[kelumpuhan][32]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'kelumpuhan_32']) !!}
									</td>
									<td>
										{!! Form::select("dg[gangguan_raba][40]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'gangguan_raba_40']) !!}
									</td>
									<td></td>
								</tr>
								<tr>
									<td>Lengan Kanan</td>
									<td>
										{!! Form::select("dg[kelumpuhan][33]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'kelumpuhan_33']) !!}
									</td>
									<td>
										{!! Form::select("dg[gangguan_raba][41]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'gangguan_raba_41']) !!}
									</td>
									<td></td>
								</tr>
								<tr>
									<td>Lengan Kiri</td>
									<td>
										{!! Form::select("dg[kelumpuhan][34]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'kelumpuhan_34']) !!}
									</td>
									<td>
										{!! Form::select("dg[gangguan_raba][42]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'gangguan_raba_42']) !!}
									</td>
									<td></td>
								</tr>
								<tr>
									<td>
										{!!Form::text("dg[other][]", null, ['class' => 'form-control','id'=>'other', 'placeholder'=>'Lainnya, Sebutkan'])!!}
									</td>
									<td>
										{!! Form::select("dg[kelumpuhan_other][]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control','id'=>'kelumpuhanother']) !!}
									</td>
									<td>
										{!! Form::select("dg[gangguan_raba_other][]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control','id'=>'gangguanrabaother']) !!}
									</td>
									<td>
										<a class="btn-sm btn-success" data-toggle="tooltip" onclick="add_gejala()" title="Tambah Gejala">
											<i class="fa fa-plus"></i>
										</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">
						Riwayat Kontak
					</h3>
				</div>
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Dalam satu bulan terakhir sebelum sakit, apakah penderita pernah bepergian ?', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-2">
								{!! Form::select('drk[bepergian_sebelum_sakit]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak tahu'), null, ['class' => 'form-control', 'id'=>'bepergian_sebelum_sakit']) !!}
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									{!! Form::label(null, 'Lokasi', ['class' => 'col-sm-4 control-label']) !!}
									<div class="col-sm-7">
										{!! Form::text('drk[lokasi_pergi_sebelum_sakit]', null, ['class' => 'form-control','placeholder'=>'Lokasi','id'=>'lokasi','disabled']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label(null, 'Tanggal pergi', ['class' => 'col-sm-4 control-label']) !!}
									<div class="col-sm-7">
										{!! Form::text('drk[tgl_pergi_sebelum_sakit]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal pergi','id'=>'tgl_pergi','disabled']) !!}
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Dalam satu bulan terakhir sebelum sakit, apakah penderita pernah berkunjung ke rumah anak yang baru mendapat imunisasi polio ?', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('drk[berkunjung_ke_rumah_anak_imunisasi_polio]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak tahu'), null, ['class' => 'form-control', 'id'=>'berkunjung_ke_rumah_anak_imunisasi_polio']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">
						Status imunisasi polio
					</h3>
				</div>
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Imunisasi rutin', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-3">
								{!! Form::select('drs[imunisasi_rutin_polio_sebelum_sakit]', array(null=>'Jumlah Dosis','1'=>'1x','2'=>'2x','3'=>'3x','4'=>'4x','5'=>'5x','6'=>'6x','7'=>'Tidak','8'=>'Tidak tahu','9'=>'Belum pernah'), null, ['class' => 'form-control','id'=>'imunisasi_rutin_polio_sebelum_sakit','onchange'=>"activeinformasi('imunisasi_rutin_polio_sebelum_sakit')"]) !!}
							</div>
							<div class="col-sm-4">
								{!! Form::select('drs[informasi_imunisasi_rutin_polio_sebelum_sakit]', array(null=>'Sumber Informasi','1'=>'KMS/Catatan Jurim','2'=>'Ingatan Responden','3'=>'Tidak tahu'), null, ['class' => 'form-control','id'=>'informasi_imunisasi_rutin_polio_sebelum_sakit','disabled']) !!}
							</div>
							<span class="col-sm-1 al">
								(*)
							</span>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'PIN, Mop-up, ORI, BIAS Polio', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-3">
								{!! Form::select('drs[pin_mopup_ori_biaspolio]', array(null=>'Jumlah Dosis','1'=>'1x','2'=>'2x','3'=>'3x','4'=>'4x','5'=>'5x','6'=>'6x','7'=>'Tidak','8'=>'Tidak tahu','8'=>'Belum pernah'), null, ['class' => 'form-control','id'=>'pin_mopup_ori_biaspolio','onchange'=>"activeinformasi('pin_mopup_ori_biaspolio')"]) !!}
							</div>
							<div class="col-sm-4">
								{!! Form::select('drs[informasi_pin_mopup_ori_biaspolio]', array(null=>'Sumber Informasi','1'=>'KMS/Catatan Jurim','2'=>'Ingatan Responden','3'=>'Tidak tahu'), null, ['class' => 'form-control','id'=>'informasi_pin_mopup_ori_biaspolio','disabled']) !!}
							</div>
							<span class="col-sm-1 al">
								(*)
							</span>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Tanggal imunisasi polio terakhir', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drs[tgl_imunisasi_polio_terakhir]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal imunisasi polio terakhir','id'=>'tgl_imunisasi_polio_terakhir']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Pengumpulan spesimen</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Apakah spesimen diambil', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-2">
								{!! Form::select('ds[pengambilan_spesimen]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'pengambilan_spesimen']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Tidak diambil spesimen, berikan alasannya', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-4">
								{!! Form::textarea('ds[alasan_spesimen_tidak_diambil]', null, ['class' => 'form-control','disabled','id'=>'alasan_spesimen_tidak_diambil','placeholder'=>'Alasan spesimen tidak diambil','rows'=>'3']) !!}
							</div>
						</div>
						<table class="table table-striped" id="spesimen">
							<thead style="background: #4caf50;color: white;">
								<tr>
									<th width="20%">Spesimen</th>
									<th>Tanggal ambil</th>
									<th>Tanggal Kirim ke kabupaten/kota</th>
									<th>Tanggal Kirim ke provinsi</th>
									<th>Tanggal Kirim ke laboratorium</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<div style="margin: 14px 0px;">
							<button type="button" class="btn btn-success btn-flat" id="addSpesimen" disabled=""><i class="fa fa-plus-circle"></i> Tambah Sampel</button>
						</div>
						<div id="div_sampel" style="margin-top: 2%">
							<table class="table table-bordered" id="data_sampel">
								<tr>
									<td width="20%">Spesimen</td>
									<td>Tanggal ambil</td>
									<td>Tanggal Kirim ke kabupaten/kota</td>
									<td>Tanggal Kirim ke provinsi</td>
									<td>Tanggal Kirim ke laboratorium</td>
								</tr>
								<tr>
									<td>{!! Form::select(null, array(null=>'--Pilih--','1'=>'Spesimen Tool I','2'=>'Spesimen Tool II','3'=>'Spesimen Tool III','4'=>'Spesimen Tool IV','5'=>'Spesimen Tool V'), null, ['class' => 'form-control','id'=>'spesimen_tool']) !!}</td>
									<td>{!! Form::text(null, null, ['class' => 'form-control dates','placeholder'=>'Tanggal Pengambilan Spesimen','id'=>'tgl_ambil','disabled']) !!}</td>
									<td>{!! Form::text(null, null, ['class' => 'form-control dates','placeholder'=>'Tanggal Kirim ke Kabupaten/Kota','id'=>'tgl_kirim_kab','disabled']) !!}</td>
									<td>{!! Form::text(null, null, ['class' => 'form-control dates','placeholder'=>'Tanggal Kirim ke Provinsi','id'=>'tgl_kirim_prov','disabled']) !!}</td>
									<td>{!! Form::text(null, null, ['class' => 'form-control dates','placeholder'=>'Tanggal Kirim ke Laboratorium','id'=>'tgl_kirim_lab','disabled']) !!}</td>
								</tr>
							</table>
							<div class="row">
								<div class="col-md-offset-4 col-md-8">
									<button type="button" class="btn btn-default" id='tutup_sampel'>Tutup</button>
									<button type="button" class="btn btn-success" disabled="disabled" id="tambah_spesimen" onclick="add_spesimen()">Tambah</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">
						Petugas pelacak
					</h3>
				</div>
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Nama Petugas', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-6">
								<table class="table table-striped" id="list_petugas">
									<tr>
										<td><input name="dpel[pelaksana][]" type="text" class="form-control" id="pelaksana" placeholder="Nama Petugas" value=""></td>
										<td><a data-toggle="tooltip" title="Tambah Pelaksana" class="btn-sm btn-success" onclick="add_petugas()"><i class="fa fa-plus"></i></a></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">
						Hasil pemeriksaan
					</h3>
				</div>
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Diagnosis', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-7">
								{!! Form::text('dh[diagnosis]', null, ['class' => 'form-control','placeholder'=>'Diagnosis','id'=>'diagnosis_hasil_pemeriksaan']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Nama DSA/DSS/DRM/Dr/pemeriksa lain', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-7">
								{!! Form::text('dh[nama_pemeriksa]', null, ['class' => 'form-control','placeholder'=>'Nama DSA/DSS/DRM/Dr/pemeriksa lain','id'=>'nama_pemeriksa']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'No.Telp', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-7">
								{!! Form::text('dh[no_telp]', null, ['class' => 'form-control','placeholder'=>'No.Telp','id'=>'no_telp']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="footer">
				{!! Form::reset("Batal", ['class' => 'btn btn-warning batal']) !!}
				{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
			</div>
		</div>
	</div>
	{!! Form::close() !!}
</div>
<style type="text/css">
table.gejala tr td{
	padding: 15px 8px;
}
</style>
<script type="text/javascript">
	function generateData() {
		var id = $('#id_trx_case').val();
		if(isset(id)){
			var action = BASE_URL+'case/afp/getDetail/'+id;
			$.getJSON(action, function(result){
				var raw = result.response;
				if(isset(raw)){
					$.each(raw, function(k, dt){
						$('#name').val(dt.name_pasien);
						$('#id_pasien').val(dt.id_pasien);
						$('#id_family').val(dt.id_family);
						$('#nik').val(dt.nik);
						$('#no_rm').val(dt.no_rm);
						$('#agama').val(dt.agama).trigger('change');
						$('#nama_ortu').val(dt.nama_ortu);
						$('#jenis_kelamin').val(dt.jenis_kelamin).trigger('change');
						if(isset(dt.tgl_lahir)){$('#tgl_lahir').val(dt.tgl_lahir).removeAttr('disabled');}
						if(isset(dt.umur_hari)){$('#umur_hari').val(dt.umur_hari).removeAttr('disabled');}
						if(isset(dt.umur_bln)){$('#umur_bln').val(dt.umur_bln).removeAttr('disabled');}
						if(isset(dt.umur_thn)){$('#umur_thn').val(dt.umur_thn).removeAttr('disabled');}
						$('#alamat').val(dt.alamat);
						$('#wilayah').val(dt.full_address);
						$('#id_kelurahan_pasien').val(dt.code_kelurahan_pasien);
						$('#id_kecamatan_pasien').val(dt.code_kecamatan_pasien);
						$('#id_kabupaten_pasien').val(dt.code_kabupaten_pasien);
						$('#id_provinsi_pasien').val(dt.code_provinsi_pasien);
						$('#name_faskes').val(dt.name_faskes);
						$('#no_epid').val(dt.no_epid);
						$('#no_epid_lama').val(dt.no_epid_lama);
						if (dt.keadaan_akhir==1) {$('#tgl_meninggal').attr('disabled','disabled');}
						$('#demam_sebelum_lumpuh').val(dt.demam_sebelum_lumpuh).trigger('change');
						$('#tgl_mulai_lumpuh').val(dt.tgl_mulai_lumpuh);
						$('#imunisasi_rutin_polio_sebelum_sakit').val(dt.imunisasi_rutin_polio_sebelum_sakit).trigger('change');
						$('#informasi_imunisasi_rutin_polio_sebelum_sakit').val(dt.informasi_imunisasi_rutin_polio_sebelum_sakit).trigger('change');
						$('#pin_mopup_ori_biaspolio').val(dt.pin_mopup_ori_biaspolio).trigger('change');
						$('#informasi_pin_mopup_ori_biaspolio').val(dt.informasi_pin_mopup_ori_biaspolio).trigger('change');
						$('#tgl_imunisasi_polio_terakhir').val(dt.tgl_imunisasi_polio_terakhir);
						var dtGejala = dt.dtGejala;
						$.each(dtGejala, function(key, val){
							$('#kelumpuhan_'+val.id_gejala).val(val.val_kelumpuhan).trigger('change');
							$('#gangguan_raba_'+val.id_gejala).val(val.val_gangguan_raba).trigger('change');
							if(val.id_gejala==null){
								add_gejala(key,val);
							}
						});
						$.each(dt.dtSpesimen, function(ks, vs){
							if(isset(vs)){
								add_spesimen(vs);
							}
						});
					});
				}
			});
		}

		var id_pe = $('#id_trx_pe').val();
		if(isset(id_pe)){
			var action = BASE_URL+'case/afp/pe/getDetail/'+id_pe;
			$.getJSON(action, function(result){
				var raw = result.response;
				if(isset(raw)){
					$.each(raw, function(k, dt){
						$('#id_provinsi_sumber_informasi').val(dt.code_provinsi_sumber_informasi).trigger('change');
						if(isset(dt.code_kabupaten_sumber_informasi)){
							$('#id_kabupaten_sumber_informasi').html('<option value="'+dt.code_kabupaten_sumber_informasi+'">'+dt.name_kabupaten_sumber_informasi+'</option>').trigger('change');
						}
						$('#laporan_dari').val(dt.sumber_laporan).trigger('change');
						$('#ket_sumber_laporan').val(dt.keterangan);
						$('#tgl_sakit_pe').val(dt.tgl_sakit_pe);
						$('#berobat_ke_unit_pelayanan_lain').val(dt.berobat_ke_unit_pelayanan_lain).trigger('change');
						if(isset(dt.nama_unit_pelayanan)){$('#nama_unit_pelayanan').val(dt.nama_unit_pelayanan).removeAttr('disabled');}
						if(isset(dt.tgl_berobat)){$('#tgl_berobat').val(dt.tgl_berobat).removeAttr('disabled');}
						if(isset(dt.diagnosis)){$('#diagnosis').val(dt.diagnosis).removeAttr('disabled');}
						if(isset(dt.no_rm_lama)){$('#no_rekmed').val(dt.no_rm_lama).removeAttr('disabled');}
						$('#kelumpuhan_sifat_akut').val(dt.kelumpuhan_sifat_akut).trigger('change');
						$('#kelumpuhan_sifat_layuh').val(dt.kelumpuhan_sifat_layuh).trigger('change');
						$('#kelumpuhan_disebabkan_ruda').val(dt.kelumpuhan_disebabkan_ruda).trigger('change');
						$('#bepergian_sebelum_sakit').val(dt.bepergian_sebelum_sakit).trigger('change');
						if(isset(dt.lokasi_pergi_sebelum_sakit)){$('#lokasi').val(dt.lokasi_pergi_sebelum_sakit).removeAttr('disabled');}
						if(isset(dt.tgl_pergi_sebelum_sakit)){$('#tgl_pergi').val(dt.tgl_pergi_sebelum_sakit).removeAttr('disabled');}
						$('#berkunjung_ke_rumah_anak_imunisasi_polio').val(dt.berkunjung_ke_rumah_anak_imunisasi_polio).trigger('change');
						$('#pengambilan_spesimen').val(dt.pengambilan_spesimen).trigger('change');
						$('#alasan_spesimen_tidak_diambil').val(dt.alasan_spesimen_tidak_diambil);
						$('#diagnosis_hasil_pemeriksaan').val(dt.diagnosis_hasil_pemeriksaan);
						$('#nama_pemeriksa').val(dt.nama_pemeriksa);
						$('#no_telp').val(dt.no_telp);
						$.each(dt.dtPelaksana, function(kp, vp){
							if(isset(vp.nama_pelaksana)){
								add_petugas(vp);
							}
						});
						
						$('.rm').on('click',function(){
							$(this).parent().parent().remove();
						});
					});
				}
			});
		}
		return false;
	}
	
	$(function(){
		$('#pengambilan_spesimen').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#addSpesimen').removeAttr('disabled');
				$('#alasan_spesimen_tidak_diambil').attr('disabled', 'disabled');
			}else{
				$('#addSpesimen').attr('disabled','disabled');
				$('#alasan_spesimen_tidak_diambil').removeAttr('disabled');
			}
		});
		$('#div_sampel').hide();
		$('#tutup_sampel').on('click',function(){
			$('#div_sampel').hide(500);
			$('#addSpesimen').show();
			return false;
		});

		$('#addSpesimen').on('click',function(){
			$('#div_sampel').show(500);
			$('#addSpesimen').hide();
			return false;
		});

		$('#spesimen_tool').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#tgl_ambil').removeAttr('disabled');
			}else{
				$('#tgl_ambil').attr('disabled','disabled');
			}
			return false;
		});

		$('#tgl_ambil').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#tgl_kirim_kab').removeAttr('disabled');
			}else{
				$('#tgl_kirim_kab').attr('disabled','disabled').select2('val','');
			}
			return false;
		});

		$('#tgl_kirim_kab').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#tambah_spesimen').removeAttr('disabled');
				$('#tgl_kirim_prov').removeAttr('disabled');
			}else{
				$('#tambah_spesimen').attr('disabled','disabled');
				$('#tgl_kirim_prov').attr('disabled','disabled').val(null);
			}
			return false;
		});

		$('select').on('change', function() { $(this).valid(); });
		generateData();

		$('.batal').on('click',function(){
			window.location.href = '{!! url('case/afp'); !!}';
			return false;
		});

		$('#form').validate({
			rules:{},
			messages:{},
			submitHandler: function(){
				var action = BASE_URL+'case/afp/pe/store';
				var data = $('#form').serializeJSON();
				$.ajax({
					method  : "POST",
					url     : action,
					data    : JSON.stringify([data]),
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
					success: function(data, status){
						if (data.success==true) {
							window.location.href = '{!! url('case/afp#tab_4'); !!}';
						}else{
							messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
							endProcess();
						}
					}
				});
				return false;
			}
		});

		$('#berobat_ke_unit_pelayanan_lain').on('change',function(){
			var val = $(this).val();
			if(val==1){
				$('#nama_unit_pelayanan').removeAttr('disabled');
				$('#tgl_berobat').removeAttr('disabled');
				$('#diagnosis').removeAttr('disabled');
				$('#no_rekmed').removeAttr('disabled');
			}else{
				$('#nama_unit_pelayanan').attr('disabled','disabled').val(null);
				$('#tgl_berobat').attr('disabled','disabled').val(null);
				$('#diagnosis').attr('disabled','disabled').val(null);
				$('#no_rekmed').attr('disabled','disabled').val(null);
			}
			return false;
		});

		$('#bepergian_sebelum_sakit').on('change',function(){
			var val = $(this).val();
			if(val==1){
				$('#lokasi').removeAttr('disabled');
				$('#tgl_pergi').removeAttr('disabled');
			}else{
				$('#lokasi').attr('disabled','disabled').val(null);
				$('#tgl_pergi').attr('disabled','disabled').val(null);
			}
			return false;
		});
	});

	function activeinformasi(id) {
		var val = $('#'+id).val();
		if(val==null){
			$('#informasi_'+id).attr('disabled','disabled').select2('val',null);
		}else{
			$('#informasi_'+id).removeAttr('disabled');
		}
		return false;
	}

	function add_spesimen(data){
		var st = $('#spesimen_tool option:selected').text();
		var s = $('#spesimen_tool').val();
		var ta = $('#tgl_ambil').val();
		var tak = $('#tgl_kirim_kab').val();
		var tap = $('#tgl_kirim_prov').val();
		var tal = $('#tgl_kirim_lab').val();
		if(isset(data)){
			st =data.jenis_spesimen_txt;
			s =data.jenis_spesimen;
			ta =data.tgl_ambil_spesimen;
			tak = isset(data.tgl_kirim_kab) ? data.tgl_kirim_kab : '';
			tap = isset(data.tgl_kirim_prov) ? data.tgl_kirim_prov : '';
			tal = isset(data.tgl_kirim_lab) ? data.tgl_kirim_lab : '';
		}
		$('#spesimen tbody').append('<tr>'+
			'<td>'+st+'</td>'+
			'<td>'+ta+'</td>'+
			'<td>'+tak+'</td>'+
			'<td>'+tap+'</td>'+
			'<td>'+tal+'</td>'+
			'<td><a href="javascript:void(0);" class="btn-sm btn-danger rm"><i class="fa fa-remove"></i></a>'+
			'<input type="hidden" name="ds[spesimen][]" value="'+s+'|'+ta+'|'+tak+'|'+tap+'|'+tal+'"></td>'+
			'</tr>'
			);
		$('#spesimen').val(null);
		$('#tgl_ambil').val(null);
		$('#tgl_kirim_kab').val(null).attr('disabled','disabled');
		$('#tgl_kirim_prov').val(null).attr('disabled','disabled');
		$('#tgl_kirim_lab').val(null).attr('disabled','disabled');
		$('#tambah_spesimen').attr('disabled','disabled');
		$('.rm').on('click',function(){
			$(this).parent().parent().remove();
		});
		return false;
	}

	var igejala = 0;
	function add_gejala(key,data) {
		var id=igejala++;
		var other = $('#other').val();
		var kelumpuhan = $('#kelumpuhanother').val();
		var gangguan_raba = $('#gangguanrabaother').val();
		if(isset(data)){
			other = data.other_gejala;
			kelumpuhan = data.val_kelumpuhan;
			gangguan_raba = data.val_gangguan_raba;
		}
		$('#gejala tbody').append('<tr>'+
			'<td><input name="dg[other][]" type="text" class="form-control other_'+id+'" value="'+other+'" placeholder="Lainnya, sebutkan."></td>'+
			'<td><select name="dg[kelumpuhan_other][]" class="form-control kelumpuhan_'+id+'"><option value="">--Pilih--</option><option value="1">Ya</option><option value="2">Tidak</option></select></td>'+
			'<td><select name="dg[gangguan_raba_other][]" class="form-control gangguan_raba_'+id+'"><option value="">--Pilih--</option><option value="1">Ya</option><option value="2">Tidak</option></select></td>'+
			'<td><a data-toggle="tooltip" title="Hapus gejala" class="btn-sm btn-danger remove" ><i class="fa fa-remove"></i></a></td>'+
			'</tr>'
			);
		$('.kelumpuhan_'+id).val(kelumpuhan).trigger('change');
		$('.gangguan_raba_'+id).val(gangguan_raba).trigger('change');
		$('#other').val('');
		$('#kelumpuhanother').val('');
		$('#gangguanrabaother').val('');
		$('select').select2({ allowClear  : false, closeOnSelect : true, width : '100%' });
		$('.remove').on('click',function(){
			$(this).parent().parent().remove();
			return false;
		});
	}

	function add_petugas(data) {
		var nama_petugas = $('#pelaksana').val();
		if(isset(data)){
			nama_petugas = data.nama_pelaksana;
		}
		$('#list_petugas').append('<tr>'+
			'<td><input name="dpel[pelaksana][]" type="text" class="form-control" placeholder="Nama Petugas" value="'+nama_petugas+'">'+
			'<td><a data-toggle="tooltip" title="Hapus Petugas" class="btn-sm btn-danger remove" ><i class="fa fa-remove"></i></a></td>'+
			'</tr>'
			);
		$('#pelaksana').val('');
		$('.remove').on('click',function(){
			$(this).parent().parent().remove();
			return false;
		});
	}

</script>
@endsection

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">
			Gejala Tanda
		</h3>
	</div>
	<div class="form-horizontal">
		<div class="box-body">
			<table class="table table-striped" id="gejala">
				<thead style="background: #4caf50;color: white;">
					<tr>
						<th></th>
						<th width="30%">Kelumpuhan</th>
						<th width="30%">Gangguan Raba</th>
						<th width='5%'></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Tungkai Kanan</td>
						<td>
							{!! Form::select("dg[kelumpuhan][31]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'kelumpuhan_31']) !!}
						</td>
						<td>
							{!! Form::select("dg[gangguan_raba][39]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'gangguan_raba_39']) !!}
						</td>
						<td></td>
					</tr>
					<tr>
						<td>Tungkai Kiri</td>
						<td>
							{!! Form::select("dg[kelumpuhan][32]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'kelumpuhan_32']) !!}
						</td>
						<td>
							{!! Form::select("dg[gangguan_raba][40]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'gangguan_raba_40']) !!}
						</td>
						<td></td>
					</tr>
					<tr>
						<td>Lengan Kanan</td>
						<td>
							{!! Form::select("dg[kelumpuhan][33]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'kelumpuhan_33']) !!}
						</td>
						<td>
							{!! Form::select("dg[gangguan_raba][41]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'gangguan_raba_41']) !!}
						</td>
						<td></td>
					</tr>
					<tr>
						<td>Lengan Kiri</td>
						<td>
							{!! Form::select("dg[kelumpuhan][34]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'kelumpuhan_34']) !!}
						</td>
						<td>
							{!! Form::select("dg[gangguan_raba][42]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'gangguan_raba_42']) !!}
						</td>
						<td></td>
					</tr>
					<tr>
						<td>
							{!!Form::text("dg[other][]", null, ['class' => 'form-control','id'=>'otherlain', 'placeholder'=>'Lainnya, Sebutkan'])!!}
						</td>
						<td>
							{!! Form::select("dg[kelumpuhan_other][]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control','id'=>'kelumpuhanother']) !!}
						</td>
						<td>
							{!! Form::select("dg[gangguan_raba_other][]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control','id'=>'gangguanother']) !!}
						</td>
						<td>
							<a class="btn-sm btn-success" data-toggle="tooltip" onclick="add_gejala()" title="Tambah Gejala">
								<i class="fa fa-plus"></i>
							</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	var igejala = 0;
	function add_gejala(key,data) {
		var id=igejala++;
		var other = $('#otherlain').val();
		var kelumpuhan = $('#kelumpuhanother').val();
		var gangguan_raba = $('#gangguanother').val();
		if(isset(data)){
			id = key;
			other = data.other_gejala;
			kelumpuhan = data.val_kelumpuhan;
			gangguan_raba = data.val_gangguan_raba;
		}
		$('#gejala tbody').append('<tr>'+
			'<td><input name="dg[other][]" type="text" class="form-control other_'+id+'" value="'+other+'" placeholder="Lainnya, sebutkan."></td>'+
			'<td><select name="dg[kelumpuhan_other][]" class="form-control kelumpuhan_'+id+'"><option value="">--Pilih--</option><option value="1">Ya</option><option value="2">Tidak</option></select></td>'+
			'<td><select name="dg[gangguan_raba_other][]" class="form-control gangguan_raba_'+id+'"><option value="">--Pilih--</option><option value="1">Ya</option><option value="2">Tidak</option></select></td>'+
			'<td><a data-toggle="tooltip" title="Hapus gejala" class="btn-sm btn-danger remove" ><i class="fa fa-remove"></i></a></td>'+
			'</tr>'
			);
		$('.kelumpuhan_'+id).val(kelumpuhan).trigger('change');
		$('.gangguan_raba_'+id).val(gangguan_raba).trigger('change');
		$('#otherlain').val('');
		$('#kelumpuhanother').val('');
		$('#gangguanother').val('');
		$('select').select2({ allowClear  : false, closeOnSelect : true, width : '100%' });
		$('.remove').on('click',function(){
			$(this).parent().parent().remove();
			return false;
		});
	}
</script>
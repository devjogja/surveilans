@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#tab_1">Daftar Kasus</a></li>
			@if(in_array(Session::get('id_role'),['puskesmas','rs']))
			<li><a data-toggle="tab" href="#tab_2">Input data individual kasus</a></li>
			@endif
			<li><a data-toggle="tab" href="#tab_3">Analisis Kasus</a></li>
			<li><a data-toggle="tab" href="#tab_4">Daftar kasus penyelidikan epidemologi</a></li>
			<li><a data-toggle="tab" href="#tab_5">Daftar kasus KU60</a></li>
			<li><a data-toggle="tab" href="#tab_6">Impor</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				@include('case.afp.daftar_kasus')
			</div>
			<div class="tab-pane" id="tab_2">
				@include('case.afp.input_kasus')
			</div>
			<div class="tab-pane" id="tab_3">
				@include('case.afp.analisa')
			</div>
			<div class="tab-pane" id="tab_4">
				@include('case.afp.pe.daftar_kasus')
			</div>
			<div class="tab-pane" id="tab_5">
				@include('case.afp.ku60.daftar_kasus')
			</div>
			<div class="tab-pane" id="tab_6">
				@include('case.include.import')
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		var url = document.location.toString();
		if (url.match('#')) {
		 $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
	 }
});
</script>
@endsection

<table border="1">
	<thead>
		<tr>
			<th rowspan="3">No</th>
			<th rowspan="3">Tgl Input</th>
			<th rowspan="3">Tgl Update</th>
			<th rowspan="3">No.RM</th>
			<th rowspan="3">No.Epid PD3I v04.01</th>
			<th rowspan="3">No.Epid PD3I v04.00</th>
			<th rowspan="3">No.Epid Lama</th>
			<th rowspan="3">Nama Faskes</th>
			<th rowspan="3">Kecamatan</th>
			<th rowspan="3">Kabupaten/Kota</th>
			<th rowspan="3">Provinsi</th>
			<th colspan="14">Identitas Pasien</th>
			<th colspan="12">Data surveilans AFP</th>
			<th colspan="11">Gejala Tanda</th>
			<th colspan="4">Data spesimen dan hasil laboratorium</th>
			<th rowspan="3">Klasifikasi final</th>
			<th colspan="37">Data Penyelidikan Epidemologi</th>
		</tr>
		<tr>
			<th rowspan="2">Nama Pasien</th>
			<th rowspan="2">NIK</th>
			<th rowspan="2">Agama</th>
			<th rowspan="2">Nama Ortu</th>
			<th rowspan="2">Jenis Kelamin</th>
			<th rowspan="2">Tanggal Lahir</th>
			<th colspan="3">Umur</th>
			<th colspan="5">Alamat</th>
			<th rowspan="2">Tanggal mulai lumpuh (bukan karena ruda paksa)</th>
			<th rowspan="2">Demam sebelum lumpuh (bukan karena ruda paksa)</th>
			<th colspan="2">Imunisasi rutin polio sebelum sakit</th>
			<th colspan="2">PIN, Mop-up, ORI, BIAS Polio</th>
			<th rowspan="2">Tanggal imunisasi polio terakhir</th>
			<th rowspan="2">Tanggal laporan diterima</th>
			<th rowspan="2">Tanggal pelacakan</th>
			<th rowspan="2">Riwayat Kontak</th>
			<th rowspan="2">Keadaan akhir</th>
			<th rowspan="2">Status Kasus</th>
			<th colspan="4">Kelumpuhan</th>
			<th colspan="4">Gangguan Raba</th>
			<th rowspan="2">Lainnya</th>
			<th rowspan="2">Kelumpuhan</th>
			<th rowspan="2">Gangguan Raba</th>
			<th rowspan="2">Spesimen</th>
			<th rowspan="2">Tanggal Pengambilan Spesimen</th>
			<th rowspan="2">Jenis Pemeriksaan Laboratorium</th>
			<th rowspan="2">Hasil</th>
			<th colspan="2">Titik Koordinat Pasien</th>
			<th colspan="4">Sumber Informasi</th>
			<th colspan="11">Riwayat Sakit</th>
			<th colspan="4">Riwayat Kontak</th>
			<th colspan="5">Status imunisasi polio</th>
			<th colspan="7">Pengumpulan spesimen</th>
			<th rowspan="2">Petugas Pelacak</th>
			<th colspan="3">Hasil pemeriksaan</th>
		</tr>
		<tr>
			<th>Tahun</th>
			<th>Bulan</th>
			<th>Hari</th>
			<th>Alamat</th>
			<th>Kelurahan</th>
			<th>Kecamatan</th>
			<th>Kabupaten</th>
			<th>Provinsi</th>
			<th>Jumlah Dosis</th>
			<th>Sumber Informasi</th>
			<th>Jumlah Dosis</th>
			<th>Sumber Informasi</th>
			<th>Tungkai Kanan</th>
			<th>Tungkai Kiri</th>
			<th>Lengan Kanan</th>
			<th>Lengan Kiri</th>
			<th>Tungkai Kanan</th>
			<th>Tungkai Kiri</th>
			<th>Lengan Kanan</th>
			<th>Lengan Kiri</th>
			<th>Longitude</th>
			<th>Latitude</th>
			<th>Provinsi</th>
			<th>Kabupaten</th>
			<th>Laporan dari</th>
			<th>Keterangan sumber laporan</th>
			<th>Tanggal mulai sakit</th>
			<th>Tanggal mulai lumpuh</th>
			<th>Tanggal meninggal (bila penderita meninggal)</th>
			<th>Sebelum dilaporkan, apakah penderita berobat ke unit pelayanan lain?</th>
			<th>Nama unit pelayanan</th>
			<th>Tanggal berobat</th>
			<th>Diagnosis</th>
			<th>No.Rekam medis</th>
			<th>Apakah kelumpuhan sifatnya akut (1-14 hari)?</th>
			<th>Apakah kelumpuhan sifatnya layuh (flaccid)?</th>
			<th>Apakah kelumpuhan disebabkan ruda?</th>
			<th>Dalam satu bulan terakhir sebelum sakit, apakah penderita pernah bepergian ?</th>
			<th>Lokasi</th>
			<th>Tanggal pergi</th>
			<th>Dalam satu bulan terakhir sebelum sakit, apakah penderita pernah berkunjung ke rumah anak yang baru mendapat imunisasi polio ?</th>
			<th>Imunisasi rutin</th>
			<th>Sumber Imunisasi rutin</th>
			<th>PIN, Mop-up, ORI, BIAS Polio</th>
			<th>Sumber Informasi PIN, Mop-up, ORI, BIAS Polio</th>
			<th>Tanggal imunisasi polio terakhir</th>
			<th>Apakah spesimen diambil</th>
			<th>Tidak diambil spesimen, berikan alasannya</th>
			<th>Spesimen</th>
			<th>Tanggal ambil</th>
			<th>Tanggal kirim ke Kabupaten/Kota</th>
			<th>Tanggal kirim provinsi</th>
			<th>Tanggal Kirim Laboratorium</th>
			<th>Diagnosis</th>
			<th>Nama DSA/DSS/DRM/Dr/pemeriksa lain</th>
			<th>No.Telp</th>
		</tr>
	</thead>
	<tbody>
		@if($dd)
		<?php $no = 1;?>
			@foreach($dd AS $key=>$val)
				<?php $id = $val['id_trx_case'];?>
				<tr>
					@if($arr[$id]['p'] == 'no')
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $no++; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['tgl_input'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['tgl_update'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['no_rm'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['no_epid'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['_no_epid'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['no_epid_lama'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['name_faskes'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['name_kecamatan_faskes'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['name_kabupaten_faskes'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['name_provinsi_faskes'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['name_pasien'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['nik'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['agama'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['nama_ortu'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['jenis_kelamin_txt'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['tgl_lahir'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['umur_thn'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['umur_bln'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['umur_hari'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['alamat'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['kelurahan_pasien'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['kecamatan_pasien'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['kabupaten_pasien'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['provinsi_pasien'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['tgl_mulai_lumpuh'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['demam_sebelum_lumpuh_txt'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['imunisasi_rutin_polio_sebelum_sakit_txt'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['informasi_imunisasi_rutin_polio_sebelum_sakit_txt'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['pin_mopup_ori_biaspolio_txt'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['informasi_pin_mopup_ori_biaspolio_txt'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['tgl_imunisasi_polio_terakhir'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['tgl_laporan_diterima'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['tgl_pelacakan'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['kontak_txt'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $val['keadaan_akhir_txt'] !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}"></td>
						<?php
						$gk = explode('|',$val['gejala_kelumpuhan']);
						$dk = [];
						foreach ($gk as $vk) {
							$k = explode('_',$vk);
							$dk[$k[0]] = !empty($k[1]) ? $k[1]==1 ? 'Ya':'Tidak':'';
						}

						$gr = explode('|',$val['gejala_gangguan_raba']);
						$dr = [];
						foreach ($gr as $vr) {
							$r = explode('_',$vr);
							$dr[$r[0]] = !empty($r[1]) ? $r[1]==1 ? 'Ya':'Tidak':'';
						}
						?>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! !empty($dk[31]) ? $dk[31] : ''; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! !empty($dk[32]) ? $dk[32] : ''; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! !empty($dk[33]) ? $dk[33] : ''; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! !empty($dk[34]) ? $dk[34] : ''; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! !empty($dr[39]) ? $dr[39] : ''; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! !empty($dr[40]) ? $dr[40] : ''; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! !empty($dr[41]) ? $dr[41] : ''; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! !empty($dr[42]) ? $dr[42] : ''; !!}</td>
						<?php
						$og = explode('|', $val['other_gejala']);
						$do = $dko = $dro = '';
						foreach ($og as $vo) {
							$o = explode('_',$vo);
							$do .= empty($o[0])?'<br/>':$o[0].'<br/>';
							$dko .= empty($o[1])?'-<br/>':($o[1] == 1 ?'Ya':'Tidak').'<br/>';
							$dro .= empty($o[2])?'-<br/>':($o[2] == 1 ?'Ya':'Tidak').'<br/>';
						}
						?>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $do !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $dko !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan']; !!}">{!! $dro !!}</td>
					@endif
					<td>{!! $val['jenis_spesimen_txt']; !!}</td>
					<td>{!! $val['tgl_ambil_spesimen']; !!}</td>
					<td>{!! $val['jenis_pemeriksaan_txt']; !!}</td>
					<td>{!! $val['hasil']; !!}</td>
					@if($arr[$id]['p'] == 'no')
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['klasifikasi_final_txt']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['longitude']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['latitude']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['name_provinsi_sumber_informasi']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['name_kabupaten_sumber_informasi']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['sumber_laporan_txt']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['keterangan']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['tgl_sakit']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['tgl_mulai_lumpuh']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['tgl_meninggal']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['berobat_ke_unit_pelayanan_lain_txt']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['nama_unit_pelayanan']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['tgl_berobat']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['diagnosis']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['no_rm_lama']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['kelumpuhan_sifat_akut_txt']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['kelumpuhan_sifat_layuh_txt']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['kelumpuhan_disebabkan_ruda_txt']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['bepergian_sebelum_sakit_txt']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['lokasi_pergi_sebelum_sakit']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['tgl_pergi_sebelum_sakit']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['berkunjung_ke_rumah_anak_imunisasi_polio_txt']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['imunisasi_rutin_polio_sebelum_sakit_txt']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['informasi_imunisasi_rutin_polio_sebelum_sakit_txt']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['pin_mopup_ori_biaspolio_txt']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['informasi_pin_mopup_ori_biaspolio_txt']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['tgl_imunisasi_polio_terakhir']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['pengambilan_spesimen_txt']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['alasan_spesimen_tidak_diambil']; !!}</td>
					@endif
					<td>{!! $val['jenis_spesimen_txt']; !!}</td>
					<td>{!! $val['tgl_ambil_spesimen']; !!}</td>
					<td>{!! $val['tgl_kirim_kab']; !!}</td>
					<td>{!! $val['tgl_kirim_prov']; !!}</td>
					<td>{!! $val['tgl_kirim_lab']; !!}</td>
					@if($arr[$id]['p'] == 'no')
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['petugas_pelacak']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['diagnosis_hasil_pemeriksaan']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['nama_pemeriksa']; !!}</td>
						<td rowspan="{!! $arr[$id]['rowspan'] !!}">{!! $val['no_telp']; !!}</td>
						<?php $arr[$id]['p'] = 'yes';?>
					@endif
				</tr>
			@endforeach
		@endif
	</tbody>
</table>
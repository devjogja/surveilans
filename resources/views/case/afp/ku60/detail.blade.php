@extends('layouts.base')
@section('content')
@if($dt = $data['response'])
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			<table class="table table-striped">
				<caption>Identitas Penderita</caption>
				<tbody>
					<tr>
						<td class="title">No Epidemologi</td>
						<td>{!! $dt->no_epid or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">No Epidemologi Lama</td>
						<td>{!! $dt->no_epid_lama or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Penderita</td>
						<td>{!! $dt->name_pasien or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">NIK</td>
						<td>{!! $dt->nik or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">No. RM</td>
						<td>{!! $dt->no_rm or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Agama</td>
						<td>{!! $dt->religion_text or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Orang Tua</td>
						<td>{!! $dt->nama_ortu or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Jenis Kelamin</td>
						<td>{!! $dt->jenis_kelamin_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Lahir(Umur)</td>
						<td><?php
						$tgl_lahir = ($dt->tgl_lahir)?$dt->tgl_lahir:'-';
						$umur_thn = ($dt->umur_thn)?$dt->umur_thn.' Th':'';
						$umur_bln = ($dt->umur_bln)?$dt->umur_bln.' Bln':'';
						$umur_hari = ($dt->umur_hari)?$dt->umur_hari.' Hari':'';
						?>
					{!! $tgl_lahir !!} ({!!$umur_thn!!} {!!$umur_bln !!} {!!$umur_hari!!})</td>
				</tr>
				<tr>
					<td class="title">Alamat</td>
					<td>{!! $dt->alamat or '' !!} {!! $dt->full_address or '' !!}</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="box-body">
		<table class="table table-striped">
			<caption>Informasi Kunjungan Ulang</caption>
			<tbody>
				<tr>
					<td class="title">Provinsi</td>
					<td>{!! $dt->name_provinsi_kunjungan_ulang or '-' !!}</td>
				</tr>
				<tr>
					<td class="title">Kabupaten</td>
					<td>{!! $dt->name_kabupaten_kunjungan_ulang or '-' !!}</td>
				</tr>
				<tr>
					<td class="title">Tanggal Kunjungan Ulang Seharusnya</td>
					<td>{!! $dt->tgl_kunjungan_ulang_seharusnya or '-' !!}</td>
				</tr>
				<tr>
					<td class="title">Apakah Kunjungan Dilaksanakan</td>
					<?php
						$kunjungan = $dt->kunjungan_txt;
						if($dt->kunjungan == 1){
							$kunjungan .= ', Tanggal kunjungan ulang : '.$dt->tgl_kunjungan_ulang;
						}elseif($dt->kunjungan == 2){
							$alasan_tidak_kunjungan = $dt->tdk_kunjungan_txt;
							if($dt->tdk_kunjungan == 1){
								$alasan_tidak_kunjungan .= ' Tgl meninggal : '.$dt->tgl_meninggal;
							}elseif($dt->tdk_kunjungan == 3){
								$alasan_tidak_kunjungan .= ' Alasan lain : '.$dt->alasan_lain_tdk_kunjungan;
							}
							$kunjungan .= ', Alasan tidak kunjungan ulang : '.$alasan_tidak_kunjungan;
						}
					?>
					<td>{!! $kunjungan or '-' !!}</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="box-body">
		<table class="table table-striped">
			<caption>Riwayat Sakit</caption>
			<tbody>
				<tr>
					<td class="title">Apakah sudah ada diagnosa dari rumah sakit atau dokter yang merawat</td>
					<td colspan="2">{!! $dt->diagnosa_dr_rs_txt or '-' !!}, {!! $dt->diagnosis or '' !!}</td>
				</tr>
				<tr>
					<td class="title">Apakah masih ada paralisis residual</td>
					<td colspan="2">{!! $dt->paralisis_residual_txt or '-' !!}</td>
				</tr>
				<tr>
					<td class="title">Bila ya, apakah sifatnya layuh (flaccid)</td>
					<td colspan="2">{!! $dt->sifat_layuh_txt or '-' !!}</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="box-body">
		<table class="table table-striped">
			<caption>Gejala</caption>
			<tbody>
				<tr>
					<td class="title">Lokasi kelumpuhan dan gangguan raba</td>
				</tr>
			</tbody>
		</table>
		<table class="table table-striped">
			<thead>
				<tr>
					<th></th>
					<th>Paralisis Residual</th>
					<th>Gangguan Raba</th>
				</tr>
			</thead>
			<tbody>
				@if(empty($dt->dtGejala))
				<tr><td colspan="3">Tidak ada gejala.</td></tr>
				@else
				<tr>
					<td>Tungkai Kanan</td>
					<?php
					$v43_paralisis = !empty($dt->dtGejala[43]->val_paralisis_residual_txt)?$dt->dtGejala[43]->val_paralisis_residual_txt:'';
					$v39_kelumpuhan = !empty($dt->dtGejala[39]->val_gangguan_raba_txt)?$dt->dtGejala[39]->val_gangguan_raba_txt:'';
					?>
					<td>{!! $v43_paralisis !!}</td>
					<td>{!! $v39_kelumpuhan !!}</td>
				</tr>
				<tr>
					<td>Tungkai Kiri</td>
					<?php
					$v44_paralisis = !empty($dt->dtGejala[44]->val_paralisis_residual_txt)?$dt->dtGejala[44]->val_paralisis_residual_txt:'';
					$v40_kelumpuhan = !empty($dt->dtGejala[40]->val_gangguan_raba_txt)?$dt->dtGejala[40]->val_gangguan_raba_txt:'';
					?>
					<td>{!! $v44_paralisis !!}</td>
					<td>{!! $v40_kelumpuhan !!}</td>
				</tr>
				<tr>
					<td>Lengan Kanan</td>
					<?php
					$v45_paralisis = !empty($dt->dtGejala[45]->val_paralisis_residual_txt)?$dt->dtGejala[45]->val_paralisis_residual_txt:'';
					$v41_kelumpuhan = !empty($dt->dtGejala[41]->val_gangguan_raba_txt)?$dt->dtGejala[41]->val_gangguan_raba_txt:'';
					?>
					<td>{!! $v45_paralisis !!}</td>
					<td>{!! $v41_kelumpuhan !!}</td>
				</tr>
				<tr>
					<td>Lengan Kiri</td>
					<?php
					$v46_paralisis = !empty($dt->dtGejala[46]->val_paralisis_residual_txt)?$dt->dtGejala[46]->val_paralisis_residual_txt:'';
					$v42_kelumpuhan = !empty($dt->dtGejala[42]->val_gangguan_raba_txt)?$dt->dtGejala[42]->val_gangguan_raba_txt:'';
					?>
					<td>{!! $v46_paralisis !!}</td>
					<td>{!! $v42_kelumpuhan !!}</td>
				</tr>
				@foreach($dt->dtGejala AS $k=>$v)
				@if(empty($v->id_gejala))
				<tr>
					<td>{!! $v->other_gejala !!}</td>
					<td>{!! $v->val_paralisis_residual_txt !!}</td>
					<td>{!! $v->val_gangguan_raba_txt !!}</td>
				</tr>
				@endif
				@endforeach
				@endif
			</tbody>
		</table>
	</div>
	<div class="box-body">
		<table class="table table-striped">
			<caption>Hasil Pemeriksaan</caption>
			<tbody>
				<tr>
					<td class="title">Diagnosis Akhir</td>
					<td>{!! $dt->diagnosa_akhir or '-' !!}</td>
				</tr>
				<tr>
					<td class="title">Nama</td>
					<td>{!! $dt->nama_pemeriksa or '-' !!}</td>
				</tr>
				<tr>
					<td class="title">No. Telp/HP</td>
					<td>{!! $dt->no_telp or '-' !!}</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<div class="box-body">
		<table class="table table-striped">
			<caption>Identitas Petugas Pelacak</caption>
			<tbody>
				<tr>
					<td class="title">Nama Petugas</td>
					<td>
						<table class="table">
							@foreach($dt->dtPelacak AS $key=>$val)
							<tr>
								<td>{!! $val->nama; !!}</td>
							</tr>
							@endforeach
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
</div>
@endif
<div class="col-sm-12">
	<div class="footer">
		<a href="{!!URL::to('case/afp#tab_5')!!}" class="btn btn-primary">Kembali</a>
	</div>
</div>
@endsection
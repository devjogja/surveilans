@extends('layouts.base')
@section('content')
<div class="col-md-12">
	<div class="alert alert-notif alert-dismissable">
		<i class="icon fa fa-warning"></i>
		Text input yang bertanda bintang (*) wajib di isi
	</div>
</div>
{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form', 'class' => 'form-horizontal']) !!}
{!! Form::hidden('id_trx_case', $data['id_trx_case'], ['id'=>'id_trx_case']) !!}
{!! Form::hidden('id_trx_ku60', $data['id_trx_ku60'], ['id'=>'id_trx_ku60']) !!}
<div class="col-md-6">
	@include('case.afp.form_pasien')
</div>
<div class="col-md-6">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Informasi Kunjungan Ulang</h3>
		</div>
		<div class='form-horizontal'>
			<div class="box-body">
				<div class="form-group">
					{!! Form::label(null, 'Provinsi', ['class' => 'col-md-4 control-label']) !!}
					<div class="col-md-5">
						{!! Form::select('dik[code_provinsi_kunjungan_ulang]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control','id'=>'id_provinsi_pelapor','onchange'=>"getKabupaten('_pelapor')"]) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label(null, 'Kabupaten', ['class' => 'col-md-4 control-label']) !!}
					<div class="col-md-5">
						{!! Form::select('dik[code_kabupaten_kunjungan_ulang]', array(null=>'Pilih Kabupaten'), null, ['class' => 'form-control', 'id'=>'id_kabupaten_pelapor']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label(null, 'Tanggal Kunjungan Ulang Seharusnya', ['class' => 'col-md-4 control-label']) !!}
					<div class="col-md-5">
						{!! Form::text('dik[tgl_kunjungan_ulang_seharusnya]', null, ['class' => 'form-control datemax','id'=>'tgl_kunjungan_ulang_seharusnya','placeholder'=>'Tanggal Laporan']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label(null, 'Apakah Kunjungan Dilaksanakan', ['class' => 'col-md-4 control-label']) !!}
					<div class="col-md-8">
						<div class="form-group">
							<label class="col-md-2">
								<input type="radio" name="dik[kunjungan]" value="1" class="kunjungan">&nbsp;Ya
							</label>
							<div class="col-md-8">
								<div class="form-group">
									{!! Form::label(null, 'Tanggal Kunjungan Ulang', ['class' => 'col-md-4 control-label']) !!}
									<div class="col-md-8">
										{!! Form::text('dik[tgl_kunjungan_ulang]', null, ['class' => 'form-control tgl_kunjungan_ulang dates ya','disabled','placeholder'=>'Tanggal Kunjungan Ulang','id'=>'tgl_kunjungan_ulang',]) !!}
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2">
								<input type="radio" name="dik[kunjungan]" value="2" class="kunjungan">&nbsp;Tidak
							</label>
							<div class="col-md-8">
								{!! Form::label(null, 'Alasan tidak dilakukan kunjungan ulang :', ['class' => 'col-md-10 ']) !!}
								<div class="form-group">
									<label class="col-md-4">
										<input type="radio" name="dik[tdk_kunjungan]" value="1" class="tidak">&nbsp;Meninggal
									</label>
									<div class="col-md-8">
										{!! Form::text('dik[tgl_meninggal]', null, ['class' => 'form-control datemax meninggal','disabled','placeholder'=>'Tanggal Meninggal','id'=>'tgl_meninggal']) !!}
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-8">
										<input type="radio" name="dik[tdk_kunjungan]" value="2" class="tidak">&nbsp;Pindah alamat, Alamat tidak jelas
									</label>
								</div>
								<div class="form-group">
									<label class="col-md-4">
										<input type="radio" name="dik[tdk_kunjungan]" value="3" class="tidak">&nbsp;Lain-lain, sebutkan
									</label>
									<div class="col-md-8">
										{!! Form::text('dik[alasan_lain_tdk_kunjungan]', null, ['class' => 'form-control lain','disabled','placeholder'=>'Alasan tidak kunjungan','id'=>'alasan_lain_tdk_kunjungan',]) !!}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Riwayat sakit</h3>
		</div>
		<div class='form-horizontal'>
			<div class="box-body">
				<div class="form-group">
					{!! Form::label(null, 'Apakah sudah ada diagnosa dari rumah sakit atau dokter yang merawat', ['class' => 'col-md-4 control-label']) !!}
					<div class="col-md-8">
						<div class="form-group">
							<label class="col-md-2">
								<input type="radio" name="drs[diagnosa_dr_rs]" value="1" class="diagnosa">&nbsp;Ya
							</label>
							<div class="col-md-8">
								<div class="form-group">
									{!! Form::label(null, 'Diagnosis', ['class' => 'col-md-2 control-label']) !!}
									<div class="col-md-8">
										{!! Form::text('drs[diagnosis]', null, ['class' => 'form-control ','disabled','placeholder'=>'Diagnosis','id'=>'diagnosis',]) !!}
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2">
								<input type="radio" name="drs[diagnosa_dr_rs]" value="2" class="diagnosa">&nbsp;Tidak
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					{!! Form::label(null, 'Apakah masih ada paralisis residual', ['class' => 'col-md-4 control-label']) !!}
					<div class="col-md-4">
						{!! Form::select('drs[paralisis_residual]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'paralisis_residual']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label(null, 'Bila ya, apakah sifatnya layuh (flaccid)', ['class' => 'col-md-4 control-label']) !!}
					<div class="col-md-4">
						{!! Form::select('drs[sifat_layuh]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'sifat_layuh','disabled']) !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class='col-md-12'>
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Gejala Tanda</h3>
		</div>
		<div class='form-horizontal'>
			<div class="box-body">
				<div class="form-group">
					{!! Form::label(null, 'Lokasi kelumpuhan dan gangguan raba', ['class' => 'col-md-4']) !!}
				</div>
				<table class="table table-striped" id="gejala">
					<thead style="background: #4caf50;color: white;">
						<tr>
							<th></th>
							<th width="30%">
								Paralisis Residual
							</th>
							<th width="30%">
								Gangguan Raba
							</th>
							<th width='5%'></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Tungkai Kanan</td>
							<td>
								{!! Form::select("dg[paralisis_residual][43]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'paralisis_residual_43']) !!}
							</td>
							<td>
								{!! Form::select("dg[gangguan_raba][39]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'gangguan_raba_39']) !!}
							</td>
							<td></td>
						</tr>
						<tr>
							<td>Tungkai Kiri</td>
							<td>
								{!! Form::select("dg[paralisis_residual][44]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'paralisis_residual_44']) !!}
							</td>
							<td>
								{!! Form::select("dg[gangguan_raba][40]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'gangguan_raba_40']) !!}
							</td>
							<td></td>
						</tr>
						<tr>
							<td>Lengan Kanan</td>
							<td>
								{!! Form::select("dg[paralisis_residual][45]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'paralisis_residual_45']) !!}
							</td>
							<td>
								{!! Form::select("dg[gangguan_raba][41]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'gangguan_raba_41']) !!}
							</td>
							<td></td>
						</tr>
						<tr>
							<td>Lengan Kiri</td>
							<td>
								{!! Form::select("dg[paralisis_residual][46]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'paralisis_residual_46']) !!}
							</td>
							<td>
								{!! Form::select("dg[gangguan_raba][42]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'gangguan_raba_42']) !!}
							</td>
							<td></td>
						</tr>
						<tr>
							<td>
								{!!Form::text("dg[other][]", null, ['class' => 'form-control','id'=>'other', 'placeholder'=>'Lainnya, Sebutkan'])!!}
							</td>
							<td>
								{!! Form::select("dg[paralisis_residual_other][]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control','id'=>'paralisis_residual_other']) !!}
							</td>
							<td>
								{!! Form::select("dg[gangguan_raba_other][]", array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control','id'=>'gangguan_raba_other']) !!}
							</td>
							<td>
								<a class="btn-sm btn-success" data-toggle="tooltip" onclick="add_gejala()" title="Tambah Gejala">
									<i class="fa fa-plus"></i>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="col-md-6">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Hasil Pemeriksaan</h3>
		</div>
		<div class='form-horizontal'>
			<div class="box-body">
				<div class="form-group">
					{!! Form::label(null, 'Diagnosis Akhir', ['class' => 'col-md-4 control-label']) !!}
					<div class="col-md-5">
						{!! Form::text('dhp[diagnosa_akhir]', null, ['class' => 'form-control','id'=>'diagnosa_akhir','placeholder'=>'Diagnosis Akhir']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label(null, 'Nama', ['class' => 'col-md-4 control-label']) !!}
					<div class="col-md-5">
						{!! Form::text('dhp[nama_pemeriksa]', null, ['class' => 'form-control','id'=>'nama_pemeriksa','placeholder'=>'Nama']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label(null, 'No. Telp/HP', ['class' => 'col-md-4 control-label']) !!}
					<div class="col-md-5">
						{!! Form::text('dhp[no_telp]', null, ['class' => 'form-control','id'=>'no_telp','placeholder'=>'No. Telp/HP']) !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-6">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Identitas Petugas Pelacak</h3>
		</div>
		<div class='form-horizontal'>
			<div class="box-body">
				<div class="form-group">
					{!! Form::label(null, 'Nama Petugas', ['class' => 'col-md-4 control-label']) !!}
					<div class="col-md-6">
						<table class="table table-striped" id="list_petugas">
							<tr>
								<td><input name="dpel[nama][]" type="text" class="form-control" placeholder="Nama Petugas" id="name_petugas" value=""></td>
								<td><a title="Tambah Petugas" data-toggle="tooltip" class="btn-sm btn-success" onclick="add_petugas()"><i class="fa fa-plus"></i></a></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="footer">
		{!! Form::reset("Batal", ['class' => 'btn btn-warning batal']) !!}
		{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
	</div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
	var i = 0;
	function add_gejala(data) {
		var id = i++;
		var other=$('#other').val();
		var paralisis_residual_other=$('#paralisis_residual_other').val();
		var gangguan_raba_other=$('#gangguan_raba_other').val();
		if(isset(data)){
			other = data.other_gejala;
			paralisis_residual_other = data.val_paralisis_residual;
			gangguan_raba_other = data.val_gangguan_raba;
		}
		$('#gejala tbody').append('<tr>'+
			'<td><input name="dg[other][]" type="text" class="form-control other_'+id+'" value="'+other+'">'+
			'<td><select name="dg[paralisis_residual_other][]" class="form-control paralisis_residual_'+id+'"><option value="">--Pilih--</option><option value="1">Ya</option><option value="2">Tidak</option></select></td>'+
			'<td><select name="dg[gangguan_raba_other][]" class="form-control gangguan_raba_'+id+'"><option value="">--Pilih--</option><option value="1">Ya</option><option value="2">Tidak</option></select></td>'+
			'<td><a data-toggle="tooltip" title="Hapus gejala" class="btn-sm btn-danger remove" ><i class="fa fa-remove"></i></a></td>'+
			'</tr>'
			);
		$('.paralisis_residual_'+id).val(paralisis_residual_other).trigger('change');
		$('.gangguan_raba_'+id).val(gangguan_raba_other).trigger('change');
		$('#other').val('');
		$('#paralisis_residual_other').val('');
		$('#gangguan_raba_other').val('');
		$('select').select2({ allowClear  : false, closeOnSelect : true, width : '100%' });
		$('.remove').on('click',function(){
			$(this).parent().parent().remove();
			return false;
		});
		return false;
	}

	function add_petugas(data) {
		var nama_petugas = $('#name_petugas').val();
		if(isset(data)){
			nama_petugas = data.nama;
		}
		$('#list_petugas').append('<tr>'+
			'<td><input name="dpel[nama][]" type="text" class="form-control" placeholder="Nama Petugas" value="'+nama_petugas+'">'+
			'<td><a data-toggle="tooltip" title="Hapus Petugas" class="btn-sm btn-danger remove" ><i class="fa fa-remove"></i></a></td>'+
			'</tr>'
			);
		$('#name_petugas').val('');
		$('.remove').on('click',function(){
			$(this).parent().parent().remove();
			return false;
		});
		return false;
	}

	$(function(){
		$('.batal').on('click',function(){
			window.location.href = '{!! url('case/afp#tab_5'); !!}';
			return false;
		});
		$("input[class='kunjungan']").on('ifChecked',function(){
			var val = $(this).val();
			if(val=='1'){
				$('.ya').removeAttr('disabled');
				$('input[class=tidak]').attr('checked', false);
				$('.tidak').attr('disabled','disabled');
				$('.lain').attr('disabled','disabled').val(null);
				$('.meninggal').attr('disabled','disabled').val(null);
				$('input[class=tidak]:checked').checked = false;
			}else{
				$('.tidak').removeAttr('disabled');
				$('.ya').attr('disabled','disabled').val(null);
			}
		});
		$("input[class='tidak']").on('ifChecked',function(){
			var val = $(this).val();
			if(val=='1'){
				$('.meninggal').removeAttr('disabled');
				$('.lain').attr('disabled','disabled').val(null);
			}else if(val=='2'){
				$('.lain').attr('disabled','disabled').val(null);
				$('.meninggal').attr('disabled','disabled').val(null);
			}else if(val=='3'){
				$('.lain').removeAttr('disabled');
				$('.meninggal').attr('disabled','disabled').val(null);
			}
		});
		$("input[class='diagnosa']").on('ifChecked',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#diagnosis').removeAttr('disabled');
			}else{
				$('#diagnosis').attr('disabled','disabled').val(null);
			}
		});
		$('#paralisis_residual').on('change',function(){
			var val = $(this).val();
			if(val==1){
				$('#sifat_layuh').removeAttr('disabled');
			}else{
				$('#sifat_layuh').attr('disabled','disabled').val(null).trigger('change');
			}
			return false;
		});
		generateData();
		$('#form').validate({
			rules:{},
			messages:{},
			submitHandler: function(){
				var action = BASE_URL+'case/afp/ku60/store';
				var data = $('#form').serializeJSON();
				$.ajax({
					method  : "POST",
					url     : action,
					data    : JSON.stringify([data]),
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
					success: function(data, status){
						if (data.success==true) {
							window.location.href = '{!! url('case/afp#tab_5'); !!}';
						}else{
							messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
							endProcess();
						}
					}
				});
				return false;
			}
		});
	})

	function generateData() {
		var id = $('#id_trx_case').val();
		if(isset(id)){
			var action = BASE_URL+'case/afp/getDetail/'+id;
			$.getJSON(action, function(result){
				var raw = result.response;
				if(isset(raw)){
					$.each(raw, function(k, dt){
						$('#name').val(dt.name_pasien);
						$('#id_pasien').val(dt.id_pasien);
						$('#id_family').val(dt.id_family);
						$('#nik').val(dt.nik);
						$('#no_rm').val(dt.no_rm);
						$('#agama').val(dt.agama).trigger('change');
						$('#nama_ortu').val(dt.nama_ortu);
						$('#jenis_kelamin').val(dt.jenis_kelamin).trigger('change');
						if(isset(dt.tgl_lahir)){$('#tgl_lahir').val(dt.tgl_lahir).removeAttr('disabled');}
						if(isset(dt.umur_hari)){$('#umur_hari').val(dt.umur_hari).removeAttr('disabled');}
						if(isset(dt.umur_bln)){$('#umur_bln').val(dt.umur_bln).removeAttr('disabled');}
						if(isset(dt.umur_thn)){$('#umur_thn').val(dt.umur_thn).removeAttr('disabled');}
						$('#alamat').val(dt.alamat);
						$('#wilayah').val(dt.full_address);
						$('#id_kelurahan_pasien').val(dt.code_kelurahan_pasien);
						$('#id_kecamatan_pasien').val(dt.code_kecamatan_pasien);
						$('#id_kabupaten_pasien').val(dt.code_kabupaten_pasien);
						$('#id_provinsi_pasien').val(dt.code_provinsi_pasien);
						$('#name_faskes').val(dt.name_faskes);
						$('#no_epid').val(dt.no_epid);
						$('#no_epid_lama').val(dt.no_epid_lama);
					});
				}
			});
		}
		var id_trx_ku60 = $('#id_trx_ku60').val();
		if(isset(id_trx_ku60)){
			var action = BASE_URL+'case/afp/ku60/getDetail/'+id_trx_ku60;
			$.getJSON(action, function(result){
				var raw = result.response;
				if(isset(raw)){
					$.each(raw, function(k, dt){
						$('#id_provinsi_pelapor').val(dt.code_provinsi_kunjungan_ulang).trigger('change');
						$('#id_kabupaten_pelapor').html('<option value="'+dt.code_kabupaten_kunjungan_ulang+'">'+dt.name_kabupaten_kunjungan_ulang+'</option>').trigger('change');
						$('#tgl_kunjungan_ulang_seharusnya').val(dt.tgl_kunjungan_ulang_seharusnya);
						$("input[name='dik[kunjungan]'][value='"+dt.kunjungan+"']").prop("checked",true);
						if(isset(dt.tgl_kunjungan_ulang)){
							$('#tgl_kunjungan_ulang').val(dt.tgl_kunjungan_ulang).removeAttr('disabled');
						}
						if(isset(dt.tgl_meninggal)){
							$('#tgl_meninggal').val(dt.tgl_meninggal).removeAttr('disabled');
						}
						if(isset(dt.alasan_lain_tdk_kunjungan)){
							$('#alasan_lain_tdk_kunjungan').val(dt.alasan_lain_tdk_kunjungan).removeAttr('disabled');
						}
						$("input[name='dik[tdk_kunjungan]'][value='"+dt.tdk_kunjungan+"']").prop("checked",true);
						$("input[name='drs[diagnosa_dr_rs]'][value='"+dt.diagnosa_dr_rs+"']").prop("checked",true);
						if(isset(dt.diagnosis)){
							$('#diagnosis').val(dt.diagnosis).removeAttr('disabled');
						}
						$('#paralisis_residual').val(dt.paralisis_residual).trigger('change');
						if(isset(dt.sifat_layuh)){
							$('#sifat_layuh').val(dt.sifat_layuh).trigger('change');
						}
						var dtGejala = dt.dtGejala;
						$.each(dtGejala, function(key, val){
							$('#paralisis_residual_'+val.id_gejala).val(val.val_paralisis_residual).trigger('change');
							$('#gangguan_raba_'+val.id_gejala).val(val.val_gangguan_raba).trigger('change');
							if(val.id_gejala==null){
								add_gejala(val);
							}
						});
						$('#diagnosa_akhir').val(dt.diagnosa_akhir);
						$('#nama_pemeriksa').val(dt.nama_pemeriksa);
						$('#no_telp').val(dt.no_telp);
						var dtPelacak = dt.dtPelacak;
						$.each(dtPelacak, function(key, val){
							if(isset(val)){
								add_petugas(val);
							}
						});
					});
				}
			});
		}
	}
</script>
@endsection

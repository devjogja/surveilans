@extends('layouts.base')
@section('content')
@if($dt = $data['response'])
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			<table class="table table-striped">
				<caption>Identitas Pasien</caption>
				<tbody>
					<tr>
						<td class="title">No Epidemologi</td>
						<td>{!! $dt->no_epid or '-' !!} { {!! $dt->_no_epid or '-' !!} }</td>
					</tr>
					<tr>
						<td class="title">No Epidemologi Lama</td>
						<td>{!! $dt->no_epid_lama or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">No RM</td>
						<td>{!! $dt->no_rm or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Penderita</td>
						<td>{!! $dt->name_pasien or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">NIK</td>
						<td>{!! $dt->nik or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama orang tua</td>
						<td>{!! $dt->nama_ortu or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Jenis kelamin</td>
						<td>{!! $dt->jenis_kelamin_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Lahir(Umur)</td>
						<td><?php
							$tgl_lahir = ($dt->tgl_lahir)?$dt->tgl_lahir:'-';
							$umur_thn = ($dt->umur_thn)?$dt->umur_thn.' Th':'';
							$umur_bln = ($dt->umur_bln)?$dt->umur_bln.' Bln':'';
							$umur_hari = ($dt->umur_hari)?$dt->umur_hari.' Hari':'';
						?>
						{!! $tgl_lahir !!} ({!!$umur_thn!!} {!!$umur_bln !!} {!!$umur_hari!!})
						</td>
					</tr>
					<tr>
						<td class="title">Agama</td>
						<td>{!! $dt->religion_text or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Alamat</td>
						<td>{!! $dt->alamat or '' !!} {!! $dt->full_address or '' !!}</td>
					</tr>
					<tr>
						<td class="title">Longitude</td>
						<td>{!! $dt->longitude or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Latitude</td>
						<td>{!! $dt->latitude or '-' !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Data Surveilans Afp</caption>
				<tbody>
					<tr>
						<td class="title">Tanggal mulai lumpuh (bukan karena ruda paksa)</td>
						<td>{!! $dt->tgl_mulai_lumpuh or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Demam sebelum lumpuh (bukan karena ruda paksa)</td>
						<td>{!! $dt->demam_sebelum_lumpuh_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Imunisasi rutin polio sebelum sakit</td>
						<td>
							{!! $dt->imunisasi_rutin_polio_sebelum_sakit_txt or '-' !!}
							<span class="title">, Sumber Informasi :
								{!! $dt->informasi_imunisasi_rutin_polio_sebelum_sakit_txt or '-' !!}
							</span>
						</td>
					</tr>
					<tr>
						<td class="title">PIN, Mop-up, ORI, BIAS Polio</td>
						<td>
							{!! $dt->pin_mopup_ori_biaspolio_txt or '-' !!}
							<span class="title">, Sumber Informasi :
								{!! $dt->informasi_pin_mopup_ori_biaspolio_txt or '-' !!}
							</span>
						</td>
					</tr>
					<tr>
						<td class="title">Tanggal imunisasi polio terakhir</td>
						<td>{!! $dt->tgl_imunisasi_polio_terakhir or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal laporan diterima</td>
						<td>{!! $dt->tgl_laporan_diterima or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal pelacakan</td>
						<td>{!! $dt->tgl_pelacakan or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Riwayat Kontak</td>
						<td>{!! $dt->kontak_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Keadaan akhir</td>
						<td>{!! $dt->keadaan_akhir_txt !!}</td>
					</tr>
					<tr>
						<td class="title">Status Kasus</td>
						<td>{!! $dt->hot_case_txt !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Data Gejala</caption>
				<thead>
					<tr>
						<th></th>
						<th>Kelumpuhan</th>
						<th>Gangguan Raba</th>
					</tr>
				</thead>
				<tbody>
					@if(empty($dt->dtGejala))
					<tr><td colspan="3">Tidak ada gejala.</td></tr>
					@else
					<tr>
						<td>Tungkai Kanan</td>
						<td>{!! $dt->dtGejala[31]->val_kelumpuhan_txt or '-' !!}</td>
						<td>{!! $dt->dtGejala[39]->val_gangguan_raba_txt or '-' !!}</td>
					</tr>
					<tr>
						<td>Tungkai Kiri</td>
						<td>{!! $dt->dtGejala[32]->val_kelumpuhan_txt or '-' !!}</td>
						<td>{!! $dt->dtGejala[40]->val_gangguan_raba_txt or '-' !!}</td>
					</tr>
					<tr>
						<td>Lengan Kanan</td>
						<td>{!! $dt->dtGejala[33]->val_kelumpuhan_txt or '-' !!}</td>
						<td>{!! $dt->dtGejala[41]->val_gangguan_raba_txt or '-' !!}</td>
					</tr>
					<tr>
						<td>Lengan Kiri</td>
						<td>{!! $dt->dtGejala[34]->val_kelumpuhan_txt or '-' !!}</td>
						<td>{!! $dt->dtGejala[42]->val_gangguan_raba_txt or '-' !!}</td>
					</tr>
					@foreach($dt->dtGejala AS $k=>$v)
					@if(empty($v->id_gejala))
					<tr>
						<td>{!! $v->other_gejala !!}</td>
						<td>{!! $v->val_kelumpuhan_txt !!}</td>
						<td>{!! $v->val_gangguan_raba_txt !!}</td>
					</tr>
					@endif
					@endforeach
					@endif
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Data spesimen dan hasil laboratorium</caption>
				<thead>
					<tr>
						<th>Jenis Spesimen</th>
						<th>Jenis Pemeriksaan Laboratorium</th>
						<th>Tanggal Pengambilan Spesimen</th>
						<th>Hasil</th>
					</tr>
				</thead>
				<tbody>
					@if(empty($dt->dtSpesimen))
					<tr><td colspan="4">Tidak terdapat data spesimen yang diambil.</td></tr>
					@else
					@foreach($dt->dtSpesimen AS $k=>$v)
					<tr>
						<td>{!! $v->jenis_spesimen_txt or '-' !!}</td>
						<td>{!! $v->jenis_pemeriksaan_txt or '-' !!}</td>
						<td>{!! $v->tgl_ambil_spesimen or '-'!!}</td>
						<td>{!! $v->hasil or '-'; !!}</td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Klasifikasi Final</caption>
				<tbody>
					<tr>
						<td class="title">Hasil klasifikasi final</td>
						<td>{!! $dt->klasifikasi_final_txt or '-'!!}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endif
<div class="col-sm-12">
	<div class="footer">
		<a href="{!!URL::to('case/afp')!!}" class="btn btn-primary">Kembali</a>
	</div>
</div>
@endsection
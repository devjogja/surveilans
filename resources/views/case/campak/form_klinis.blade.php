<style type="text/css">
	tbody tr td{
		padding: 2px;
	}
</style>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Data Klinis Campak</h3>
	</div>
	<div class='form-horizontal'>
		<div class="box-body">
			<div class="form-group">
				{!! Form::label('tgl_imunisasi_campak', 'Tanggal imunisasi campak terakhir', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dk[tgl_imunisasi_campak]', null, ['class' => 'form-control datemax','id'=>'tgl_imunisasi_campak','placeholder'=>'Tanggal imunisasi campak terakhir']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('jml_imunisasi_campak', 'Imunisasi campak sebelum sakit berapa kali', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					<div class="input-group">
						{!! Form::select('dk[jml_imunisasi_campak]', array(NULL=>'--Pilih--','1'=>'1x','2'=>'2x','3'=>'3x','4'=>'4x','5'=>'5x','6'=>'6x','7'=>'Tidak','8'=>'Tidak tahu','9'=>'Belum pernah'), null, ['class' => 'form-control', 'id'=>'jml_imunisasi_campak']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('tgl_mulai_demam', 'Tanggal mulai demam', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dk[tgl_mulai_demam]', null, ['class' => 'form-control datemax','onchange'=>"getEpid();getEpidKlb();getAge();",'id'=>'tgl_mulai_demam','placeholder'=>'Tanggal mulai demam']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('tgl_mulai_rash', 'Tanggal mulai rash', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					<div class="input-group">
						{!! Form::text('dk[tgl_mulai_rash]', null, ['class' => 'form-control datemax','onchange'=>"getEpid();getEpidKlb();getAge();",'id'=>'tgl_mulai_rash','placeholder'=>'Tanggal mulai rash']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Gejala/tanda lainnya', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-7">
					<table class="col-sm-12">
						@foreach(Helper::getGejala('1') AS $key=>$val)
						<tr>
							<td style="width: 45%">
								<label>
									<input type="checkbox" name="dg[gejala][]" id="gejala{!!$val->id!!}" value="{!!$val->id!!}" class="col-sm-1">&nbsp;{!!$val->name!!}
								</label>
							</td>
							<td>
								{!! Form::text('dg[tgl_gejala][]', null, ['class' => 'form-control datemax','id'=>"tglKejadian$val->id",'placeholder'=>'Tanggal Kejadian','disabled']) !!}
							</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Komplikasi', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-8">
					<table class="col-sm-12">
						@foreach(Helper::getKomplikasi('1') AS $key=>$val)
						<tr>
							<td>
								<label>
									<input type="checkbox" name="dkom[komplikasi][]" value="{!!$val->name!!}" id="{!!$val->name!!}" class="col-sm-1">&nbsp;{!!$val->name!!}
								</label>
							</td>
						</tr>
						@endforeach
					</table>
					<table class="col-sm-10">
						<tr>
							<td style="width: 35%;">
								<label>
									<input type="checkbox" name="" id="komplikasi_lain" value="other" class="col-sm-1">&nbsp;Lain-lain
								</label>
							</td>
							<td>
								{!! Form::text('dkom[otherKomplikasi]', null, ['class' => 'form-control col-sm-6','placeholder'=>'Sebutkan komplikasi yang lain.','id'=>'komplikasi_lain_desc','disabled']) !!}
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Tanggal laporan diterima', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dk[tgl_laporan_diterima]', null, ['class' => 'form-control datemax','id'=>'tgl_laporan_diterima','placeholder'=>'Tanggal laporan diterima']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Tanggal pelacakan', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dk[tgl_pelacakan]', null, ['class' => 'form-control datemax','id'=>'tgl_pelacakan','placeholder'=>'Tanggal pelacakan']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Diberi vitamin A', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[vitamin_A]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'vitamin_A']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Keadaan akhir', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[keadaan_akhir]', array(null=>'--Pilih--','1'=>'Hidup','2'=>'Meninggal'),null, ['class' => 'form-control', 'id'=>'keadaan_akhir']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Jenis Kasus', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					<div class="input-group">
						{!! Form::select('dk[jenis_kasus]', array(null=>'--Pilih--','1'=>'KLB','2'=>'Bukan KLB'), null, ['class' => 'form-control', 'id'=>'jenis_kasus']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'KLB Ke-', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					<div class="input-group">
						{!! Form::select('dk[klb_ke]', array(null=>'--Pilih--','KI'=>'KI','KII'=>'KII','KIII'=>'KIII','KIV'=>'KIV','KV'=>'KV','KVI'=>'KVI','KVII'=>'KVII','KVIII'=>'KVIII','KIX'=>'KIX','KX'=>'KX','KXI'=>'KXI','KXII'=>'KXII'), null, ['class' => 'form-control', 'id'=>'klb_ke', 'disabled','onchange'=>'getEpidKlb();']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Status Kasus', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[status_kasus]', array(null=>'--Pilih--','1'=>'Index','2'=>'Bukan Index'), null, ['class' => 'form-control', 'id'=>'status_kasus']) !!}
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		@for ($i = 0; $i <= 3; $i++)
		$("input[id='gejala{!! $i; !!}']").on('ifChecked',function(){
			$('#tglKejadian{!! $i; !!}').removeAttr('disabled');
		});
		$("input[id='gejala{!! $i; !!}']").on('ifUnchecked',function(){
			$('#tglKejadian{!! $i; !!}').attr('disabled','disabled').val(null);
		});
		@endfor
		$("input[id='komplikasi_lain']").on('ifChecked',function(){
			$('#komplikasi_lain_desc').removeAttr('disabled');
		});
		$("input[id='komplikasi_lain']").on('ifUnchecked',function(){
			$('#komplikasi_lain_desc').attr('disabled','disabled').val(null);
		});
		$('#jenis_kasus').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#klb_ke').removeAttr('disabled');
				$('#klb').removeClass('hide');
			}else{
				$('#klb_ke').select2('val',null);
				$('#klb_ke').attr('disabled','disabled');
				$('#klb').addClass('hide');
				$('#no_epid_klb').val(null);
			}
		});
	});
</script>
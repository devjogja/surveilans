<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Klasifikasi final</h3>
	</div>
	<div class="form-horizontal">
		<div class="box-body">
			<div class="form-group">
				{!! Form::label(null, 'Klasifikasi Final', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-3">
					{!! Form::select('dc[klasifikasi_final]', array(null=>'--Pilih--','1'=>'Campak Klinis','2'=>'Campak Lab','3'=>'Campak Epid','4'=>'Rubella','5'=>'Bukan campak atau Rubella (Discarded)','6'=>'Mix (Rubella positif dan Campak Positif)','7'=>'Pending'), null, ['class' => 'form-control','id'=>'klasifikasi_final']) !!}
				</div>
			</div>
		</div>
	</div>
</div>

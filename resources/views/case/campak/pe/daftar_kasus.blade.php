<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Data PE Campak</h3>
	</div>
	<div class="box-body">
		<table id="list_pe" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th>No.Epid</th>
					<th>Nama Pasien</th>
					<th>Nama Orang Tua</th>
					<th>Jenis_kelamin</th>
					<th>Alamat</th>
					<th>Jumlah Total Kasus Tambahan</th>
					<th>Faskes</th>
					<th width="6%">Tgl Input</th>
					<th width="6%">Tgl Update</th>
					<th width="10%">Aksi</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		var tUrl = "{!! url('case/campak/pe/getData'); !!}";
		var tData = [
		{ data: 'DT_Row_Index', searchable:false, orderable:false},
		{ data : "no_epid", name:'no_epid' },
		{ data : "name_pasien", name:'name_pasien' },
		{ data : "nama_ortu", name:'nama_ortu' },
		{ data : "jenis_kelamin_txt", name:'jenis_kelamin_txt' },
		{ data : "full_address", name:'full_address' },
		{ data : 'total_kasus_tambahan', name:'total_kasus_tambahan' },
		{ data: 'name_faskes', name:'name_faskes'},
		{ data: 'tgl_input', name:'tgl_input'},
		{ data: 'tgl_update', name:'tgl_update'},
		{ data : 'action', searchable:false, orderable:false},
		];
		var dtable = $('#list_pe').DataTable({
			paging: true,
			lengthChange: true,
			searching: true,
			ordering: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			ajax: {
				url     : tUrl,
				type    : "POST",
			},
			buttons: [],
			columns: tData,
		});
		$(".dataTables_filter").addClass('pull-right');
		$('#list_pe').parent().css({"overflow":"auto"});

		$('#list_pe').on('draw.dt', function(){
			$(".delete").unbind();
			$(".delete-yes").unbind();
			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();    
					},
				})
				.done(function(response){
					if(response.success){
						setTimeout( function(){
							messageAlert('info', 'Berhasil.', 'Data pe berhasil di hapus.');
							$('.dataTable').DataTable().draw(false);
							endProcess();
						}, 0);
					}
				});
				return false;
			});
		});
	});
</script>
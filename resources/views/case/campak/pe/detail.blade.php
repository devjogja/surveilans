@extends('layouts.base')
@section('content')
@if($dt = $data['response'])
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			<table class="table table-striped">
				<caption>Data Pasien</caption>
				<tbody>
					<tr>
						<td class="title">No Epid</td>
						<td>{!! $dt->no_epid !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Pasien</td>
						<td>{!! $dt->name_pasien or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Jenis Kelamin</td>
						<td>{!! $dt->jenis_kelamin_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Orang tua</td>
						<td>{!! $dt->nama_ortu or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Lahir</td>
						<td>{!! $dt->tgl_lahir or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Umur</td>
						<td>{!! $dt->umur_thn or '-' !!} Th {!! $dt->umur_bln or '-' !!} Bln {!! $dt->umur_hari or '-' !!} Hari</td>
					</tr>
					<tr>
						<?php
						$alamat = $dt->alamat.' '.$dt->full_address;
						?>
						<td class="title">Alamat</td>
						<td>{!! $alamat or '-' !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Riwayat Pengobatan</caption>
				<tbody>
					<tr>
						<td class="title">Mendapatkan pengobatan pertama kali</td>
						<td>{!! $dt->tgl_pengobatan_pertama_kali !!}</td>
					</tr>
					<tr>
						<td class="title">Tempat mendapatkan pengobatan pertama kali</td>
						<td>{!! $dt->tempat_pengobatan_pertama_kali or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Obat yang sudah diberikan</td>
						<td>{!! $dt->obat_yg_diberikan or '-' !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Riwayat kontak</caption>
				<tbody>
					<tr>
						<td class="title">Apakah dirumah ada yang sakit seperti yang di alami sekarang</td>
						<td>{!! Helper::yesno($dt->penyakit_sama_dirumah) !!}</td>
						<td class="title" style="width: 8%;">Tanggal Sakit</td>
						<td>{!! $dt->tgl_penyakit_sama_dirumah  or '-'!!}</td>
					</tr>
					<tr>
						<td class="title">Apakah disekolah anak ada yang sakit seperti yang dialami sekarang</td>
						<td>{!! Helper::yesno($dt->penyakit_sama_disekolah) !!}</td>
						<td class="title" style="width: 8%;">Tanggal Sakit</td>
						<td>{!! $dt->tgl_penyakit_sama_disekolah or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Apakah penderita menunjukkan keadaan kekurangan gizi (BB/U)</td>
						<td colspan="3">{!! Helper::yesno($dt->keadaan_kurang_gizi) !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		@if($dt->dtJmlKasusTambahan)
		<div class="box-body">
			<table class="table table-striped">
				<caption>Jumlah kasus tambahan di Lokasi PE</caption>
				<thead>
					<tr>
						<th>Lokasi</th>
						<th>Keterangan</th>
						<th>Tanggal PE</th>
						<th>Jumlah Kasus Tambahan</th>
					</tr>
				</thead>
				<tbody>
					@if($dt->dtJmlKasusTambahan)
					@foreach($dt->dtJmlKasusTambahan AS $key=>$val)
					@if($val->lokasi=='k1')
					<tr>
						<td>Alamat tinggal sementara saat sakit</td>
						<td>{!! $val->keterangan !!}</td>
						<td>{!! $val->tgl_pe !!}</td>
						<td>{!! $val->jml_kasus !!}</td>
					</tr>
					@elseif($val->lokasi=='k2')
					<tr>
						<td>Alamat tempat tinggal</td>
						<td>{!! $val->keterangan !!}</td>
						<td>{!! $val->tgl_pe !!}</td>
						<td>{!! $val->jml_kasus !!}</td>
					</tr>
					@elseif($val->lokasi=='k3')
					<tr>
						<td>Sekolah</td>
						<td>{!! $val->keterangan !!}</td>
						<td>{!! $val->tgl_pe !!}</td>
						<td>{!! $val->jml_kasus !!}</td>
					</tr>
					@elseif($val->lokasi=='k4')
					<tr>
						<td>Tempat kerja</td>
						<td>{!! $val->keterangan !!}</td>
						<td>{!! $val->tgl_pe !!}</td>
						<td>{!! $val->jml_kasus !!}</td>
					</tr>
					@else
					<tr>
						<td>{!! $val->lokasi !!}</td>
						<td>{!! $val->keterangan !!}</td>
						<td>{!! $val->tgl_pe !!}</td>
						<td>{!! $val->jml_kasus !!}</td>
					</tr>
					@endif
					@endforeach
					@else
					<tr>
						<td>Alamat tinggal sementara saat sakit</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
					</tr>
					<tr>
						<td>Alamat tempat tinggal</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
					</tr>
					<tr>
						<td>Sekolah</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
					</tr>
					<tr>
						<td>Tempat kerja</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
					</tr>
					@endif
				</tbody>
				<tfoot>
					<tr>
						<th colspan="3" style="text-align:right;">Jumlah Total Kasus Tambahan</th>
						<th>{!! $dt->total_kasus_tambahan or '0' !!}</th>
					</tr>
				</tfoot>
			</table>
		</div>
		@endif
		<div class="box-body">
			<table class="table table-striped">
				<caption>Informasi penyelidikan</caption>
				<tbody>
					<tr>
						<td class="title">Tanggal penyelidikan</td>
						<td>{!! $dt->tgl_penyelidikan_pe !!}</td>
					</tr>
					<tr>
						<td class="title">Pelaksana</td>
						<td>
							@if(count($dt->dtPelaksana) > 0)
							<table class="table table-striped">
								@foreach($dt->dtPelaksana AS $key=>$val)
								<tr>
									<td>{!! $val->nama_pelaksana !!}</td>
								</tr>
								@endforeach
							</table>
							@else
							-
							@endif
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endif
<div class="col-sm-12">
	<div class="footer">
		<a href="{!!URL::to('case/campak#tab_4')!!}" class="btn btn-primary">Kembali</a>
	</div>
</div>
@endsection
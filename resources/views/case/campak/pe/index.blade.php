@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="alert alert-notif alert-dismissable">
		<i class="icon fa fa-warning"></i> Text input yang bertanda bintang (*) wajib di isi
	</div>
	<div class="row">
		{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form', 'class' => 'form-horizontal']) !!}
		{!! Form::hidden('id_trx_case', $data['id_trx_case'], ['id'=>'id_trx_case']) !!}
		{!! Form::hidden('id_trx_pe', $data['id_trx_pe'], ['id'=>'id_trx_pe']) !!}
		{!! Form::hidden('dpe[total_kasus_tambahan]', null,["class"=>"total_kasus"]) !!}
		{!! Form::hidden('dpe[id_faskes]', Helper::role()->id_faskes,['id'=>'id_faskes']) !!}
		{!! Form::hidden('dpe[id_role]', Helper::role()->id_role,['id'=>'id_role']) !!}
		{!! Form::hidden('dpe[jenis_input]', '1',['id'=>'jenis_input']) !!}
		<div class="col-sm-12">
			<div class="box box-success">
				<div class="box-body">
					<div class="form-group">
						{!! Form::label('longitude', 'Longitude', ['class' => 'col-sm-2 control-label']) !!}
						<div class="col-sm-4">
							{!! Form::text('dpe[longitude]', null, ['class' => 'form-control','id'=>'longitude','placeholder'=>'Longitude']) !!}
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('latitude', 'Latitude', ['class' => 'col-sm-2 control-label']) !!}
						<div class="col-sm-4">
							{!! Form::text('dpe[latitude]', null, ['class' => 'form-control','id'=>'latitude','placeholder'=>'Latitude']) !!}
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Riwayat pengobatan</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Kapan mendapatkan pengobatan pertama kali?', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drp[tgl_pengobatan_pertama_kali]', null, ['class' => 'form-control datemax','id'=>'tgl_pengobatan_pertama_kali','placeholder'=>'Pengobatan pertama kali']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Dimana mendapatkan pengobatan pertama kali?', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drp[tempat_pengobatan_pertama_kali]', null, ['class' => 'form-control','id'=>'tempat_pengobatan_pertama_kali','placeholder'=>'Tempat pengobatan pertama kali']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Obat yang sudah diberikan', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drp[obat_yg_diberikan]', null, ['class' => 'form-control','id'=>'obat_yg_diberikan','placeholder'=>'Obat yang sudah diberikan']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Riwayat kontak</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Apakah dirumah ada yang sakit seperti yang di alami sekarang', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-2">
								{!! Form::select('drk[penyakit_sama_dirumah]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'penyakit_sama_dirumah','onchange'=>"active('penyakit_sama_dirumah')"]) !!}
							</div>
							{!! Form::label(null, 'Kapan sakit', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-3">
								{!! Form::text('drk[tgl_penyakit_sama_dirumah]', null, ['class' => 'form-control datemax','id'=>'tgl_penyakit_sama_dirumah','placeholder'=>'Kapan sakit','disabled']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Apakah disekolah anak ada yang sakit seperti yang dialami sekarang', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-2">
								{!! Form::select('drk[penyakit_sama_disekolah]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'penyakit_sama_disekolah','onchange'=>"active('penyakit_sama_disekolah')"]) !!}
							</div>
							{!! Form::label(null, 'Kapan sakit', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-3">
								{!! Form::text('drk[tgl_penyakit_sama_disekolah]', null, ['class' => 'form-control datemax','id'=>'tgl_penyakit_sama_disekolah','placeholder'=>'Kapan sakit','disabled']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Apakah penderita menunjukkan keadaan kekurangan gizi (BB/U)', ['class' => 'col-sm-4 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('drk[keadaan_kurang_gizi]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'keadaan_kurang_gizi']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@if($data['status_kasus']=='1')
		<div class="col-sm-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Jumlah kasus tambahan di Lokasi PE</h3>
				</div>
				<div class="box-body">
					<table class="table table-striped" id="kasus_tambahan">
						<thead style="background: #4caf50;color: white;">
							<tr>
								<th></th>
								<th>Keterangan</th>
								<th>Tanggal PE</th>
								<th>Jumlah kasus tambahan</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									{!!Form::label(null, 'Alamat tinggal sementara saat sakit (bila ada)', ['class' => 'control-label'])!!}
									{!!Form::hidden('dk[lokasi][]', 'k1', ['class' => 'form-control','id'=>'k1'])!!}
								</td>
								<td>{!!Form::text('dk[keterangan][]', null, ['class' => 'form-control','id'=>'keterangan_k1','placeholder'=>'Alamat Sementara'])!!}</td>
								<td>{!!Form::text('dk[tgl_pe][]', null, ['class' => 'form-control datemax','id'=>'tgl_pe_k1'])!!}</td>
								<td>{!!Form::text('dk[jml_kasus][]', null, ['class' => 'form-control sum','id'=>'jml_kasus_k1'])!!}</td>
							</tr>
							<tr>
								<td>
									{!!Form::label(null, 'Alamat tempat tinggal', ['class' => 'control-label'])!!}
									{!!Form::hidden('dk[lokasi][]', 'k2', ['class' => 'form-control','id'=>'k2'])!!}
								</td>
								<td>{!!Form::text('dk[keterangan][]', null, ['class' => 'form-control','id'=>'keterangan_k2','placeholder'=>'Alamat Tempat Tinggal'])!!}	</td>
								<td>{!!Form::text('dk[tgl_pe][]', null, ['class' => 'form-control datemax','id'=>'tgl_pe_k2'])!!}</td>
								<td>{!!Form::text('dk[jml_kasus][]', null, ['class' => 'form-control sum','id'=>'jml_kasus_k2'])!!}</td>
							</tr>
							<tr>
								<td>
									{!!Form::label(null, 'Sekolah', ['class' => 'control-label'])!!}
									{!!Form::hidden('dk[lokasi][]', 'k3', ['class' => 'form-control','id'=>'k3'])!!}
								</td>
								<td>{!!Form::text('dk[keterangan][]', null, ['class' => 'form-control','id'=>'keterangan_k3','placeholder'=>'Sekolah'])!!}	</td>
								<td>{!!Form::text('dk[tgl_pe][]', null, ['class' => 'form-control datemax','id'=>'tgl_pe_k3'])!!}</td>
								<td>{!!Form::text('dk[jml_kasus][]', null, ['class' => 'form-control sum','id'=>'jml_kasus_k3'])!!}</td>
							</tr>
							<tr>
								<td>
									{!!Form::label(null, 'Tempat kerja', ['class' => 'control-label'])!!}
									{!!Form::hidden('dk[lokasi][]', 'k4', ['class' => 'form-control','id'=>'k4'])!!}
								</td>
								<td>{!!Form::text('dk[keterangan][]', null, ['class' => 'form-control','id'=>'keterangan_k4','placeholder'=>'Tempat Kerja'])!!}</td>
								<td>{!!Form::text('dk[tgl_pe][]', null, ['class' => 'form-control datemax','id'=>'tgl_pe_k4'])!!}</td>
								<td>{!!Form::text('dk[jml_kasus][]', null, ['class' => 'form-control sum','id'=>'jml_kasus_k4'])!!}</td>
							</tr>
							<tr>
								<td><input name="dk[lokasi][]" id="other_lokasi" type="text" class="form-control" value="" placeholder="Lokasi lain"></td>
								<td><input name="dk[keterangan][]" id="other_keterangan" type="text" class="form-control" value="" placeholder="Keterangan lokasi PE"></td>
								<td><input name="dk[tgl_pe][]" id="other_tgl_pe" type="text" class="form-control datemax" value=""></td>
								<td><input name="dk[jml_kasus][]" id="other_jml_kasus" type="text" class="form-control sum" value=""></td>
								<td width="5%"><a title="Tambah kasus" data-toggle="tooltip" class="btn-sm btn-success" onclick="add_kasus()"><i class="fa fa-plus"></i></a></td>
							</tr>
						</tbody>
						<tfoot style="background: #4caf50;color: white;">
							<tr>
								<th colspan="3" style="text-align:right;">Jumlah Total Kasus tambahan</th>
								<th style="text-align: right;"><span class="total_kasus" >0</span></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		@endif
		<div class="col-sm-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Informasi penyelidikan</h3>
				</div>
				<div class='form-horizontal'>
					<div class="form-group">
						{!! Form::label(null, 'Tanggal Penyelidikan', ['class' => 'col-sm-2 control-label']) !!}
						<div class="col-sm-2">
							{!! Form::text('dpe[tgl_penyelidikan]', null, ['class' => 'form-control datemax','id'=>'tgl_penyelidikan','placeholder'=>'Tanggal Penyelidikan']) !!}
						</div>
					</div>
					<div class="form-group">
						{!! Form::label(null, 'Pelaksana', ['class' => 'col-sm-2 control-label']) !!}
						<div class="col-sm-4" style="padding-left:6px">
							<table class="table table-striped" id="info_pelaksana" >
								<tr>
									<td>
										<input name="dp[pelaksana][]" type="text" id="pelaksana" class="form-control" placeholder="Nama Pelaksana" value="">
									</td>
									<td>
										<a data-toggle="tooltip" title="Tambah Pelaksana" class="btn-sm btn-success" onclick="add_pelaksana()"><i class="fa fa-plus"></i></a>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="footer">
				{!! Form::reset("Batal", ['class' => 'btn btn-warning batal']) !!}
				{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
<script type="text/javascript">
	function active(id) {
		var val = $('#'+id).val();
		if(val=='1'){
			$('#tgl_'+id).removeAttr('disabled');
		}else{
			$('#tgl_'+id).attr('disabled','disabled').val(null);
		}
		return false;
	}
	function add_kasus(data) {
		var lokasi=$('#other_lokasi').val();
		var keterangan=$('#other_keterangan').val();
		var tgl_pe=$('#other_tgl_pe').val();
		var jml_kasus=$('#other_jml_kasus').val();
		if(isset(data)){
			lokasi = data.lokasi;
			keterangan = data.keterangan;
			tgl_pe = data.tgl_pe;
			jml_kasus = data.jml_kasus;
		}
		$('#kasus_tambahan tbody').append('<tr>'+
			'<td><input name="dk[lokasi][]" type="text" class="form-control" value="'+lokasi+'" placeholder="Lokasi lain"></td>'+
			'<td><input name="dk[keterangan][]" type="text" class="form-control" value="'+keterangan+'" placeholder="Keterangan lokasi PE">'+
			'<td><input name="dk[tgl_pe][]" type="text" class="form-control datemax" value="'+tgl_pe+'">'+
			'<td><input name="dk[jml_kasus][]" type="text" class="form-control sum" value="'+jml_kasus+'">'+
			'<td><a data-toggle="tooltip" title="Hapus Kasus" class="btn-sm btn-danger remove" ><i class="fa fa-remove"></i></a></td>'+
			'</tr>'
			);
		$('#other_lokasi').val('');
		$('#other_keterangan').val('');
		$('#other_tgl_pe').val('');
		$('#other_jml_kasus').val('');
		$(".datemax").datepicker({ autoclose: true, todayHighlight: true, format: 'dd/mm/yyyy', endDate: new Date() }).inputmask({'alias':'dd/mm/yyyy'});
		$('.remove').on('click',function(){
			$(this).parent().parent().remove();
			$('.sum').trigger('change');
			return false;
		});
		return false;
	}
	function add_pelaksana(data) {
		var nama_pelaksana = $('#pelaksana').val();
		if(isset(data)){
			nama_pelaksana = data;
		}
		$('#info_pelaksana').append('<tr>'+
			'<td><input name="dp[pelaksana][]" type="text" class="form-control" placeholder="Nama Pelaksana" value="'+nama_pelaksana+'">'+
			'<td><a data-toggle="tooltip" title="Hapus Pelaksana" class="btn-sm btn-danger remove" ><i class="fa fa-remove"></i></a></td>'+
			'</tr>'
			);
		$('#pelaksana').val('');
		$('.remove').on('click',function(){
			$(this).parent().parent().remove();
			return false;
		});
		return false;
	}
	$(document).on('change','.sum',function(){
		var sum = 0;
		$('.sum').each(function(){
			sum += Number($(this).val());
		});
		$('.total_kasus').html(sum).val(sum);
		return false;
	});

	function generateData() {
		var id = $('#id_trx_pe').val();
		if(isset(id)){
			var action = BASE_URL+'case/campak/pe/getDetail/'+id;
			$.getJSON(action, function(result){
				var raw = result.response;
				if(isset(raw)){
					$.each(raw, function(k, dt){
						$('#longitude').val(dt.longitude);
						$('#latitude').val(dt.latitude);
						$('#tgl_pengobatan_pertama_kali').val(dt.tgl_pengobatan_pertama_kali);
						$('#tempat_pengobatan_pertama_kali').val(dt.tempat_pengobatan_pertama_kali);
						$('#obat_yg_diberikan').val(dt.obat_yg_diberikan);
						$('#penyakit_sama_dirumah').select2('val',dt.penyakit_sama_dirumah);
						$('#tgl_penyakit_sama_dirumah').val(dt.tgl_penyakit_sama_dirumah);
						$('#penyakit_sama_disekolah').select2('val',dt.penyakit_sama_disekolah);
						$('#tgl_penyakit_sama_disekolah').val(dt.tgl_penyakit_sama_disekolah);
						$('#keadaan_kurang_gizi').select2('val',dt.keadaan_kurang_gizi);
						$('#tgl_penyelidikan').val(dt.tgl_penyelidikan_pe);
						$.each(dt.dtJmlKasusTambahan,function(key, val){
							$('#keterangan_'+val.lokasi).val(val.keterangan);
							$('#tgl_pe_'+val.lokasi).val(val.tgl_pe);
							$('#jml_kasus_'+val.lokasi).val(val.jml_kasus);
							if($.inArray(val.lokasi,['k1','k2','k3','k4']) == -1){
								var data = {lokasi:val.lokasi,keterangan:val.keterangan,tgl_pe:val.tgl_pe,jml_kasus:val.jml_kasus};
								add_kasus(data);
							}
							$('.sum').trigger('change');
						});
						$.each(dt.dtPelaksana,function(key, val){
							if(isset(val.nama_pelaksana)){
								add_pelaksana(val.nama_pelaksana);
							}
						});
					});
				}
			});
		}
		return false;
	}

	$(function(){
		generateData();
		$('select').on('change', function() { $(this).valid(); });
		$('.batal').on('click',function(){
			window.location.href = "{!! url('case/campak#tab_4'); !!}";
			return false;
		});
		$('.sum').mask('999');
		$('#form').validate({
			rules:{},
			messages:{},
			submitHandler: function(){
				var action = BASE_URL+'case/campak/pe/store';
				var data = $('#form').serializeJSON();
				$.ajax({
					method  : "POST",
					url     : action,
					data    : JSON.stringify([data]),
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
					success: function(data, status){
						if (data.success==true) {
							window.location.href = '{!! url('case/campak#tab_4'); !!}';
						}else{
							messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
							endProcess();
						}
					}
				});
				return false;
			}
		});
	});
</script>
@endsection

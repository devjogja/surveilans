<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Data Zero Report</h3>
		<a class="btn btn-sm btn-success pull-right" data-toggle="modal" data-target=".addZeroReport"><i class="fa fa-plus-square"></i>&nbsp;Tambah</a>
	</div>
	<div class="box-body">
		<table id="table_zero_report" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th>Instansi</th>
					<th>Tanggal Laporan Mulai</th>
					<th>Tanggal Laporan Selesai</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>
<div class="modal fade addZeroReport" tabindex="-1" role="dialog" aria-labelledby="">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			{!! Form::open(['method' => 'POST', 'url' => 'case/campak/zeroreport/store', 'id'=>'form-zeroreport', 'class' => 'form-horizontal']) !!}
			<div class="modal-header">
				<h5>Zero Report</h5>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('faskes', 'Nama Faskes', ['class' => 'col-sm-3 control-label']) !!}
					<div class="col-sm-8">
						{!! Form::text(null, $content['name_faskes'], ['class' => 'form-control','id'=>'name_faskes','disabled'=>'disabled']) !!}
						{!! Form::hidden('dt[id_faskes]', $content['id_faskes']) !!}
						{!! Form::hidden('dt[id_role]', $content['id_role']) !!}
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Rentang Waktu</label>
					<div class="col-sm-3">
						{!! Form::select('', array(null=>'All','1'=>'Hari','2'=>'Bulan'), null, ['class' => 'form-control', 'id'=>'range_time1']) !!}
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Sejak</label>
					<div class="col-sm-3" >
						<select class="form-control" id="from_day1" name="from_day">
							<option value="">--Pilih--</option>
							@for($i=1;$i<=31;$i++)
							<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
							@endfor
						</select>
					</div>
					<div class="col-sm-3" >
						{!! Form::select('from_month',Helper::getMonth(), null, ['class'=>'form-control','id'=>'from_month']) !!}
					</div>
					<div class="col-sm-3" >
						<select class="form-control" id="from_year1" name="from_year">
							<option value="">--Pilih--</option>
							@for($i=2010;$i<=date('Y')+5;$i++)
							<option value="{!! $i !!}">{!! $i !!}</option>
							@endfor
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Sampai</label>
					<div class="col-sm-3" >
						<select class="form-control" id="to_day1" name="to_day">
							<option value="">--Pilih--</option>
							@for($i=1;$i<=31;$i++)
							<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
							@endfor
						</select>
					</div>
					<div class="col-sm-3" >
						{!! Form::select('to_month',Helper::getMonth(), null, ['class'=>'form-control','id'=>'to_month1']) !!}
					</div>
					<div class="col-sm-3" >
						<select class="form-control" id="to_year1" name="to_year">
							<option value="">--Pilih--</option>
							@for($i=2010;$i<=2020;$i++)
							<option value="{!! $i !!}">{!! $i !!}</option>
							@endfor
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				{!! Form::reset("Batal", ['class' => 'btn btn-warning cl', 'data-dismiss'=>'modal']) !!}
				{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('#from_day1,#from_month,#from_year1,#to_day1,#to_month1,#to_year1').attr('disabled', 'disabled');
		$('#range_time1').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#from_day1, #from_month, #from_year1, #to_day1, #to_month1, #to_year1').removeAttr('disabled');
			}else if(val=='2'){
				$('#from_month, #from_year1, #to_month1, #to_year1').removeAttr('disabled');
				$('#from_day1, #to_day1').attr('disabled','disabled').val(null).trigger('change');
			}else{
				$('#from_day1, #from_month, #from_year1, #to_day1, #to_month1, #to_year1').attr('disabled','disabled').val(null).trigger('change');
			}
			return false;
		});

		var tUrlZeroReport = "{!! url('case/campak/zeroreport/getData'); !!}";
		var tData = [
		{ data: 'DT_Row_Index', searchable:false, orderable:false},
		{ data: 'name_faskes', name:'name_faskes'},
		{ data: 'trx_zero_report_start', name:'trx_zero_report_start'},
		{ data: 'trx_zero_report_end', name:'trx_zero_report_end'},
		{ data: 'action', searchable:false, orderable:false},
		];
		var dtable = $('#table_zero_report').DataTable({
			paging: true,
			lengthChange: true,
			searching: true,
			ordering: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			ajax: {
				url     : tUrlZeroReport,
				type    : "POST"
			},
			buttons: [],
			columns: tData,
		});

		$('#table_zero_report').on('draw.dt', function(){
			$(".delete").unbind();
			$(".delete-yes").unbind();
			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					if(response.success){
						setTimeout( function(){
							messageAlert('info', 'Berhasil.', 'Data kasus berhasil di hapus.');
							$('.dataTable').DataTable().draw(false);
							endProcess();
						}, 0);
					}
				});
				return false;
			});

			$('#form-zeroreport').validate({
				rules:{
					'dp[name]':'required',
				},
				messages:{
					'dp[name]':'Nama pasien wajib di isi',
				},
				submitHandler: function(){
					var action = BASE_URL+'case/campak/zeroreport/store';
					var data = $('#form-zeroreport').serializeJSON();
					$.ajax({
						method  : "POST",
						url     : action,
						data    : JSON.stringify(data),
						dataType: "json",
						beforeSend: function(){
							startProcess();
						},
						success: function(data, status){
							endProcess();
							if (data.success) {
								$('#table_zero_report').DataTable().draw(false);
								$('.cl').click();
							}else{
								messageAlert('warning', 'Peringatan', 'Data gagal di simpan, data sudah ada.');
							}
						}
					});
					return false;
				}
			});
		});
	});
</script>

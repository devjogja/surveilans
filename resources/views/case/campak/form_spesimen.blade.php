<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Data spesimen dan hasil laboratorium</h3>
	</div>
	<div class='form-horizontal'>
		<div class="box-body">
			<table class="table table-striped table-bordered" id="spesimen">
				<thead>
					<tr>
						<th width="25%">Nama Pemeriksaan</th>
						<th width="25%">Jenis Sampel</th>
						<th width="20%">Tanggal ambil sampel</th>
						@if(Helper::role()->id_role == '6')
						<th width="25%">Hasil Lab</th>
						@endif
						<th width="5%">Action</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			@if(in_array(Helper::role()->code_role,['puskesmas','rs']))
			<div style="margin: 14px 0px;">
				<button type="button" class="btn btn-success btn-flat" id="addSpesimen"><i class="fa fa-plus-circle"></i> Tambah Sampel</button>
			</div>
			@endif
			<div id="div_sampel" style="margin-top: 2%">
				<table class="table table-bordered" id="data_sampel">
					<tr>
						<th width="40%">Nama pemeriksaan</th>
						<th width="40%">Jenis sampel</th>
						<th width="20%">Tanggal ambil sampel</th>
					</tr>
					<tr>
						<td>{!! Form::select(null, array(null=>'--Pilih--','1'=>'Serologi','2'=>'Virologi'), null, ['class' => 'form-control','id'=>'jenis_pemeriksaan']) !!}</td>
						<td>{!! Form::select(null, array(null=>'--Pilih--','11'=>'Darah','12'=>'Urin'), null, ['class' => 'form-control','id'=>'jenis_spesimen','disabled']) !!}</td>
						<td>{!! Form::text(null, null, ['class' => 'form-control datemax','placeholder'=>'Tanggal ambil sampel','id'=>'tgl_ambil_spesimen','disabled']) !!}</td>
					</tr>
				</table>
				<div class="row">
					<div class="col-md-offset-4 col-md-8">
						<button class="btn btn-default" id='tutup_sampel'>Tutup</button>
						<button class="btn btn-success" disabled="disabled" id="tambah_spesimen">Tambah</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('#div_sampel').hide();
		$('#tutup_sampel').on('click',function(){
			$('#div_sampel').hide(500);
			$('#addSpesimen').show();
			return false;
		});
		$('#addSpesimen').on('click',function(){
			$('#div_sampel').show(500);
			$('#addSpesimen').hide();
			return false;
		});
		$('#jenis_pemeriksaan').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#jenis_spesimen').removeAttr('disabled');
				$('#jenis_spesimen').html("<option value=''>--Pilih--</option><option value='11'>Darah</option><option value='12'>Urin</option>");
			}else if(val=='2'){
				$('#jenis_spesimen').removeAttr('disabled');
				$('#jenis_spesimen').html("<option value=''>--Pilih--</option><option value='21'>Urin</option><option value='22'>Usap Tenggorokan</option><option value='23'>Cairan mulut</option>");
			}else{
				$('#jenis_spesimen').attr('disabled','disabled');
			}
			$('#jenis_spesimen').val('').trigger('change');
			$('#tgl_ambil_spesimen').attr('disabled','disabled');
			$('#tgl_ambil_spesimen').val('');
			$('#tambah_spesimen').attr('disabled','disabled');
			return false;
		});
		$('#jenis_spesimen').on('change',function(){
			var val = $(this).val();
			if(val==''){
				$('#tgl_ambil_spesimen').attr('disabled','disabled');
			}else{
				$('#tgl_ambil_spesimen').removeAttr('disabled');
			}
			$('#tgl_ambil_spesimen').val('');
			$('#tambah_spesimen').attr('disabled','disabled');
			return false;
		});
		$('#tgl_ambil_spesimen').on('change',function(){
			var val = $(this).val();
			if(val==''){
				$('#tambah_spesimen').attr('disabled','disabled');
			}else{
				$('#tambah_spesimen').removeAttr('disabled');
			}
			return false;
		});
		$('#tambah_spesimen').on('click',function(){
			var jpt = $('#jenis_pemeriksaan option:selected').text();
			var jp = $('#jenis_pemeriksaan').val();
			var jst = $('#jenis_spesimen option:selected').text();
			var js = $('#jenis_spesimen').val();
			var ts = $('#tgl_ambil_spesimen').val();
			var hasillab = '';
			if(jp=='1'){
				hasillab = "<select name='ds[hasil_lab][]' class='form-control'>"+
				"<option value=''>-- Pilih --</option>"+
				"<option value='1'>IgM Campak Positif</option>"+
				"<option value='2'>IgM Campak Negatif</option>"+
				"<option value='3'>IgM Rubella Positif</option>"+
				"<option value='4'>IgM Rubella Negatif</option>"+
				"<option value='5'>IgM Campak Positif & IgM Rubella Positif</option>"+
				"<option value='6'>IgM Rubella Equivocal</option>"+
				"<option value='7'>IgM Campak Equivocal</option>"+
				"<option value='8'>IgM Campak Negatif & IgM Rubella Negatif</option>"+
				"</select>";
			}else{
				hasillab = "<input type='text' class='form-control' name='ds[hasil_lab][]'>";
			}
			$('#spesimen tbody').append('<tr>'+
				'<td>'+jpt+'</td>'+
				'<td>'+jst+'</td>'+
				'<td>'+ts+'</td>'+
				@if(Helper::role()->code_role=='kabupaten')
				'<td>'+hasillab+'</td>'+
				@endif
				'<td class="text-center"><a href="javascript:void(0);" class="btn-sm btn-warning rm"><i class="fa fa-remove"></i></a>'+
				'<input type="hidden" name="ds[spesimen][]" value="'+jp+'|'+js+'|'+ts+'"></td>'+
				'</tr>'
				);
			$('#jenis_pemeriksaan').val('').trigger('change');
			$('#jenis_spesimen').val('').trigger('change');
			$('#jenis_spesimen').attr('disabled','disabled');
			$('#tgl_ambil_spesimen').val('');
			$('#tgl_ambil_spesimen').attr('disabled','disabled');
			$('#tambah_spesimen').attr('disabled','disabled');
			$('select').select2({ allowClear  : false, closeOnSelect : true, width : '100%' });
			$('.rm').on('click',function(){
				$(this).parent().parent().remove();
			});
			return false;
		});
	});
</script>
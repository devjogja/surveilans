@include('case.include.filter_analisa')
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Campak Berdasar Jenis Kelamin</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(1);']) !!}
		<div class="pull-right col-md-1" style="margin: 0px -10px 0px -7px;">
			{!! Form::select(null, array(null=>'Rutin dan KLB','2'=>'Rutin','1'=>'KLB'), 'campak', ['class' => 'form-control', 'id'=>'jenisKasusJenisKelamin','onchange'=>'graphJenisKelaminCampak();']) !!}
		</div>
		<div class="pull-right col-md-2" style="padding-right: 0px;">
			{!! Form::select(null, array('campak'=>'Suspek Campak','campak_all'=>'Campak (Lab,Epid, dan Klinis)','rubella'=>'Rubella'), 'campak', ['class' => 'form-control', 'id'=>'jenisDataJenisKelamin','onchange'=>'graphJenisKelaminCampak();']) !!}
		</div>
	</div>
	@include('case.chart.chart_gender')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Campak Berdasar Waktu</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(2);']) !!}
		<div class="pull-right col-md-1" style="margin: 0px -10px 0px -7px;">
			{!! Form::select(null, array(null=>'Rutin dan KLB','2'=>'Rutin','1'=>'KLB'), 'campak', ['class' => 'form-control', 'id'=>'jenisKasusWaktu','onchange'=>'graphWaktuCampak();']) !!}
		</div>
		<div class="pull-right col-md-2" style="padding-right: 0px;">
			{!! Form::select(null, array('campak'=>'Suspek Campak','campak_all'=>'Campak (Lab,Epid, dan Klinis)','rubella'=>'Rubella'), 'campak', ['class' => 'form-control', 'id'=>'jenisDataWaktu','onchange'=>'graphWaktuCampak();']) !!}
		</div>
	</div>
	@include('case.chart.chart_waktu')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Campak Berdasar Umur</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(3);']) !!}
		<div class="pull-right col-md-1" style="margin: 0px -10px 0px -7px;">
			{!! Form::select(null, array(null=>'Rutin dan KLB','2'=>'Rutin','1'=>'KLB'), 'campak', ['class' => 'form-control', 'id'=>'jenisKasusUmur','onchange'=>'graphUmurCampak();']) !!}
		</div>
		<div class="pull-right col-md-2" style="padding-right: 0px;">
			{!! Form::select(null, array('campak'=>'Suspek Campak','campak_all'=>'Campak (Lab,Epid, dan Klinis)','rubella'=>'Rubella'), 'campak', ['class' => 'form-control', 'id'=>'jenisDataUmur','onchange'=>'graphUmurCampak();']) !!}
		</div>
	</div>
	@include('case.chart.chart_umur')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Campak Berdasar Status Imunisasi</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(4);']) !!}
		<div class="pull-right col-md-1" style="margin: 0px -10px 0px -7px;">
			{!! Form::select(null, array(null=>'Rutin dan KLB','2'=>'Rutin','1'=>'KLB'), 'campak', ['class' => 'form-control', 'id'=>'jenisKasusStatImun','onchange'=>'graphStatImunCampak();']) !!}
		</div>
		<div class="pull-right col-md-2" style="padding-right: 0px;">
			{!! Form::select(null, array('campak'=>'Suspek Campak','campak_all'=>'Campak (Lab,Epid, dan Klinis)','rubella'=>'Rubella'), 'campak', ['class' => 'form-control', 'id'=>'jenisDataStatImun','onchange'=>'graphStatImunCampak();']) !!}
		</div>
	</div>
	@include('case.chart.chart_stat_imun')

</div>
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Campak Berdasar Klasifikasi Final</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(5);']) !!}
	</div>
	@include('case.chart.chart_final')
</div>

<script type="text/javascript">
	var dataquery;
	$('#id_provinsi_filter_analisa').on('change',function(){
		var val = $(this).val();
		if(val!=''){
			$('#id_kabupaten_filter_analisa').removeAttr('disabled');
		}else{
			$('#id_kabupaten_filter_analisa').attr('disabled','disabled');
		}
		return false;
	});
	$('#id_kabupaten_filter_analisa').on('change',function(){
		var val = $(this).val();
		if(val!=''){
			$('#id_kecamatan_filter_analisa').removeAttr('disabled');
		}else{
			$('#id_kecamatan_filter_analisa').attr('disabled','disabled');
		}
		return false;
	});
	$('#id_kecamatan_filter_analisa').on('change',function(){
		var val = $(this).val();
		if(val!='' && $('#filter_type').val()==2){
			$('#puskesmas_filter_analisa').removeAttr('disabled');
		}else{
			$('#puskesmas_filter_analisa').attr('disabled','disabled');
		}
		return false;
	});

	function unduh(val) {
		var tmp=[];
		var data=[];
		switch (val) {
			case 1:
			var rawdata = dataquery.jenis_kelamin;
			var title 	= 'Jenis Kelamin Campak';
			for (var i = 0; i < rawdata.length; i++) {
				tmp['Jenis kelamin'] 	= rawdata[i][0];
				tmp['Jumlah'] 				= rawdata[i][1];
				data.push(tmp);
				tmp=[];
			}
			break;
			case 2:
			var rawdata = dataquery.waktu.data[0].data;
			var title 	= 'Waktu '+dataquery.waktu.data[0].name+' Campak';
			for (var i = 0; i < rawdata.length; i++) {
				tmp['Bulan'] 	= (rawdata[i]['name'])?rawdata[i]['name']:rawdata[i][0];
				tmp['Jumlah'] = (rawdata[i]['y'])?rawdata[i]['y']:rawdata[i][1];
				data.push(tmp);
				tmp=[];
			}
			break;
			case 3:
			var rawdata = dataquery.umur[0].data;
			var title 	= dataquery.umur[0].name+' Campak';
			var category = [@foreach (Helper::getCategoryUmur('campak') as $item)
			'{{ $item }}',
			@endforeach ];
			for (var i = 0; i < rawdata.length; i++) {
				tmp['Umur'] 	= category[i];
				tmp['Jumlah'] = rawdata[i];
				data.push(tmp);
				tmp=[];
			}
			break;
			case 4:
			var rawdata = dataquery.stat_imun;
			var title 	= 'Status Imunisasi Campak';
			for (var i = 0; i < rawdata.length; i++) {
				tmp['Status Imunisasi'] = rawdata[i][0];
				tmp['Jumlah'] 					= rawdata[i][1];
				data.push(tmp);
				tmp=[];
			}
			break;
			case 5:
			var rawdata = dataquery.klasifikasi_final;
			var title 	= 'Klasifikasi Final Campak';
			for (var i = 0; i < rawdata.length; i++) {
				tmp['Klasifikasi Final'] = rawdata[i][0];
				tmp['Jumlah'] 					 = rawdata[i][1];
				data.push(tmp);
				tmp=[];
			}
			break;
		}
    // if(data == '')
    //     return;

    JSONToCSVConvertor(data, title, true);
  };


  var category = [@foreach (Helper::getCategoryUmur('campak') as $item)
  '{{ $item }}',
  @endforeach ];
  var title1 = 'Suspek Campak';
  var title2 = ' Rutin dan KLB';
  var range = 'Nasional'+'<br>Tahun 2010-'+new Date().getFullYear();
  var senddata = {'filter':{'code_provinsi_pasien':''}};
  $.ajax({
  	method  : "POST",
  	url     : BASE_URL+'api/analisa/campak',
  	data    : JSON.stringify(senddata),
	// dataType: "json",
	beforeSend: function(){
		// startProcess();
	},
	success: function(data){
		if (data.success==true) {
			endProcess();
			var dt=data.response;
			dataquery = dt;
			graphJenisKelamin(title1,title2,range,dt.jenis_kelamin);
			graphStatImun(title1,title2,range,dt.stat_imun);
			graphFinal(title1,range,dt.klasifikasi_final);
			graphUmur(title1,title2,range,category,dt.umur);
			graphWaktu(title1,title2,range,dt.waktu);
		}else{
			messageAlert('warning', 'Peringatan', 'Data gagal salah');
		}
	}
});
  senddata=[];
  var action = BASE_URL+'api/analisa/campak';
  $('#filter_analisa').validate({
  	rules:{
  		'filter_type':'required',
  	},
  	messages:{
  		'filter_type':'Jenis data wajib diisi',
  	},
  	submitHandler: function(){
  		var data = $('#filter_analisa').serializeJSON();
  		range = $('#id_provinsi_filter_analisa option:selected').text();
  		if(range=='Pilih Provinsi'){range='Nasional';}
  		if($('#from_year_analisa').val() && $('#to_year_analisa').val()){range = range+'<br>'+'Tahun'+$('#from_year_analisa').val()+'-'+$('#to_year_analisa').val();
  	}
  	else{
  		range=range+'<br>Tahun 2010-'+new Date().getFullYear();
  	}
  	$.ajax({
  		method  : "POST",
  		url     : action,
  		data    : JSON.stringify(data),
      // dataType: "json",
      beforeSend: function(){
        // startProcess();
      },
      success: function(data){
      	if (data.success==true) {
      		endProcess();
      		var dt=data.response;
      		dataquery = dt;
      		graphJenisKelamin(title1,title2,range,dt.jenis_kelamin);
      		graphStatImun(title1,title2,range,dt.stat_imun);
      		graphFinal(title1,range,dt.klasifikasi_final);
      		graphUmur(title1,title2,range,category,dt.umur);
      		graphWaktu(title1,title2,range,dt.waktu);
      	}else{
      		messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
      		endProcess();
      	}
      }
    });
  	return false;
  }
});

  function graphJenisKelaminCampak() {
  	if($('#filter_type').val()!=''){
  		jenis_kasus = $('#jenisKasusJenisKelamin').val();
  		jenis_data = $('#jenisDataJenisKelamin').val();
  		title1 = 'Suspek Campak';
  		if(jenis_data=='campak_all'){
  			title1 = 'Campak (Lab,Epid, dan Klinis)';
  		}else if(jenis_data=='rubella'){
  			title1 = 'Rubella';
  		}
  		title2 = ' Rutin'
  		if (jenis_kasus=='1') {
  			title2 = ' KLB';
  		}else if(jenis_kasus==''){
  			title2 = ' Rutin dan KLB';
  		}
  		var data = $('#filter_analisa').serializeJSON();
  		data['filter']['jenis_kasus']=jenis_kasus;
  		range = $('#id_provinsi_filter_analisa option:selected').text();
  		if(range=='Pilih Provinsi'){range='Nasional';}
  		if($('#from_year_analisa').val() && $('#to_year_analisa').val()){range = range+'<br>'+'Tahun'+$('#from_year_analisa').val()+'-'+$('#to_year_analisa').val();
  	}
  	else{
  		range=range+'<br>Tahun 2010-'+new Date().getFullYear();
  	}
  	$.ajax({
  		method  : "POST",
  		url     : action,
  		data    : JSON.stringify(data),
					// dataType: "json",
					beforeSend: function(){
						startProcess();
					},
					success: function(data){
						if (data.success==true) {
							var dt=data.response;
							dataquery = dt;
							graphJenisKelamin(title1,title2,range,dt.jenis_kelamin);
							endProcess();
						}else{
							messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
							endProcess();
						}
					}
				});
  }else{
  	messageAlert('warning', 'Peringatan', 'Pilihan filter harus diisi');
  }
		// graphJenisKelamin(title1,title2,range);
	}

	function graphWaktuCampak() {
		if($('#filter_type').val()!=''){
			jenis_kasus = $('#jenisKasusWaktu').val();
			jenis_data = $('#jenisDataWaktu').val();
			title1 = 'Suspek Campak';
			if(jenis_data=='campak_all'){
				title1 = 'Campak (Lab,Epid, dan Klinis)';
			}else if(jenis_data=='rubella'){
				title1 = 'Rubella';
			}
			title2 = ' Rutin'
			if (jenis_kasus=='1') {
				title2 = ' KLB';
			}else if(jenis_kasus==''){
				title2 = ' Rutin dan KLB';
			}
			var data = $('#filter_analisa').serializeJSON();
			data['filter']['jenis_kasus']=jenis_kasus;
			range = $('#id_provinsi_filter_analisa option:selected').text();
			if(range=='Pilih Provinsi'){range='Nasional';}
			if($('#from_year_analisa').val() && $('#to_year_analisa').val()){range = range+'<br>'+'Tahun'+$('#from_year_analisa').val()+'-'+$('#to_year_analisa').val();
		}
		else{
			range=range+'<br>Tahun 2010-'+new Date().getFullYear();
		}
		$.ajax({
			method  : "POST",
			url     : action,
			data    : JSON.stringify(data),
				// dataType: "json",
				beforeSend: function(){
					startProcess();
				},
				success: function(data){
					if (data.success==true) {
						endProcess();
						var dt=data.response;
						dataquery = dt;
						graphWaktu(title1,title2,range,dt.waktu);
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
						endProcess();
					}
				}
			});
	}else{
		messageAlert('warning', 'Peringatan', 'Pilihan filter harus diisi');
	}
}

function graphUmurCampak() {
	if($('#filter_type').val()!=''){
		jenis_kasus = $('#jenisKasusUmur').val();
		jenis_data = $('#jenisDataUmur').val();
		title1 = 'Suspek Campak';
		if(jenis_data=='campak_all'){
			title1 = 'Campak (Lab,Epid, dan Klinis)';
		}else if(jenis_data=='rubella'){
			title1 = 'Rubella';
		}
		title2 = ' Rutin'
		if (jenis_kasus=='1') {
			title2 = ' KLB';
		}else if(jenis_kasus==''){
			title2 = ' Rutin dan KLB';
		}
		var data = $('#filter_analisa').serializeJSON();
		data['filter']['jenis_kasus']=jenis_kasus;
		range = $('#id_provinsi_filter_analisa option:selected').text();
		if(range=='Pilih Provinsi'){range='Nasional';}
		if($('#from_year_analisa').val() && $('#to_year_analisa').val()){range = range+'<br>'+'Tahun'+$('#from_year_analisa').val()+'-'+$('#to_year_analisa').val();
	}
	else{
		range=range+'<br>Tahun 2010-'+new Date().getFullYear();
	}
	$.ajax({
		method  : "POST",
		url     : action,
		data    : JSON.stringify(data),
				// dataType: "json",
				beforeSend: function(){
					startProcess();
				},
				success: function(data){
					if (data.success==true) {
						endProcess();
						var dt=data.response;
						dataquery = dt;
						graphUmur(title1,title2,range,category,dt.umur);
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
						endProcess();
					}
				}
			});
}else{
	messageAlert('warning', 'Peringatan', 'Pilihan filter harus diisi');
}

}

function graphStatImunCampak() {
	if($('#filter_type').val()!=''){
		jenis_kasus = $('#jenisKasusStatImun').val();
		jenis_data = $('#jenisDataStatImun').val();
		title1 = 'Suspek Campak';
		if(jenis_data=='campak_all'){
			title1 = 'Campak (Lab,Epid, dan Klinis)';
		}else if(jenis_data=='rubella'){
			title1 = 'Rubella';
		}
		title2 = ' Rutin'
		if (jenis_kasus=='1') {
			title2 = ' KLB';
		}else if(jenis_kasus==''){
			title2 = ' Rutin dan KLB';
		}
		var data = $('#filter_analisa').serializeJSON();
		data['filter']['jenis_kasus']=jenis_kasus;
		range = $('#id_provinsi_filter_analisa option:selected').text();
		if(range=='Pilih Provinsi'){range='Nasional';}
		if($('#from_year_analisa').val() && $('#to_year_analisa').val()){range = range+'<br>'+'Tahun'+$('#from_year_analisa').val()+'-'+$('#to_year_analisa').val();
	}
	else{
		range=range+'<br>Tahun 2010-'+new Date().getFullYear();
	}
	$.ajax({
		method  : "POST",
		url     : action,
		data    : JSON.stringify(data),
				// dataType: "json",
				beforeSend: function(){
					startProcess();
				},
				success: function(data){
					if (data.success==true) {
						endProcess();
						var dt=data.response;
						dataquery = dt;
						graphStatImun(title1,title2,range,dt.stat_imun);
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
						endProcess();
					}
				}
			});
}else{
	messageAlert('warning', 'Peringatan', 'Pilihan filter harus diisi');
}
		// graphStatImun(title1,title2,range);
	}

	function hasilLabFinalCampak() {
		hasil_lab = $('#hasilLabFinal').val();
		title = 'Suspek Campak';
		if(hasil_lab=='campak_epid'){
			title = 'Campak Epid';
		}else if(hasil_lab=='campak_klinis'){
			title = 'Campak Klinis';
		}
		graphFinal(title);
	}
</script>


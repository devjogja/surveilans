<table border="1">
	<thead>
		<tr>
			<th rowspan="3">No</th>
			<th rowspan="3">Tgl input</th>
			<th rowspan="3">Tgl update</th>
			<th rowspan="3">No.RM</th>
			<th rowspan="3">No.Epid PD3I v04.01</th>
			<th rowspan="3">No.Epid PD3I v04.00</th>
			<th rowspan="3">No.Epid Lama</th>
			<th rowspan="3">No.Epid KLB</th>
			<th rowspan="3">Nama Faskes</th>
			<th rowspan="3">Kecamatan</th>
			<th rowspan="3">Kabupaten/Kota</th>
			<th rowspan="3">Provinsi</th>
			<th colspan='14'>Identitas Pasien</th>
			<th colspan='23'>Data Klinis Campak</th>
			<th colspan='4'>Data Spesimen dan Laboratorium</th>
			<th rowspan="3">Klasifikasi final</th>
			<th colspan="29">Penyelidikan Epidomologi</th>
		</tr>
		<tr>
			<th rowspan="2">Nama Pasien</th>
			<th rowspan="2">NIK</th>
			<th rowspan="2">Agama</th>
			<th rowspan="2">Nama Ortu</th>
			<th rowspan="2">Jenis Kelamin</th>
			<th rowspan="2">Tanggal Lahir</th>
			<th colspan="3">Umur</th>
			<th colspan="5">Alamat</th>
			<th rowspan="2">Tgl Imunisasi campak terakhir</th>
			<th rowspan="2">Imunisasi campak Sebelum sakit</th>
			<th rowspan="2">Tgl mulai demam</th>
			<th rowspan="2">Tgl mulai rash</th>
			<th colspan="6">Gejala Tanda lain</th>
			<th colspan="6">Komplikasi</th>
			<th rowspan="2">Tgl laporan diterima</th>
			<th rowspan="2">Tgl Pelacakan</th>
			<th rowspan="2">Vitamin A</th>
			<th rowspan="2">Keadaan akhir</th>
			<th rowspan="2">Jenis Kasus</th>
			<th rowspan="2">KLB Ke-</th>
			<th rowspan="2">Status Kasus</th>
			<th rowspan="2">Nama pemeriksaan</th>
			<th rowspan="2">Jenis Sampel</th>
			<th rowspan="2">Tgl ambil sampel</th>
			<th rowspan="2">Hasil lab</th>
			<th colspan="2">Titik Koordinat Pasien</th>
			<th colspan="3">Riwayat pengobatan</th>
			<th colspan="5">Riwayat kontak</th>
			<th colspan="17">Jml Kasus Tambahan di lokasi PE</th>
			<th colspan="2">Informasi penyelidikan</th>
		</tr>
		<tr>
			<th>Tahun</th>
			<th>Bulan</th>
			<th>Hari</th>
			<th>Alamat</th>
			<th>Kelurahan</th>
			<th>Kecamatan</th>
			<th>Kabupaten</th>
			<th>Provinsi</th>
			<th>Mata Merah</th>
			<th>Tgl Kejadian</th>
			<th>Batuk</th>
			<th>Tgl Kejadian</th>
			<th>Pilek</th>
			<th>Tgl Kejadian</th>
			<th>Diare</th>
			<th>Pneumonia</th>
			<th>Bronchopneumonia</th>
			<th>OMA</th>
			<th>Ensefalitis</th>
			<th>Lain-lain</th>
			<th>Longitude</th>
			<th>Latitude</th>
			<th>Tgl Pengobatan pertama kali</th>
			<th>Tempat mendapatkan pengobatan pertama kali</th>
			<th>Obat yg diberikan</th>
			<th>Apa dirumah ada sakit yang sama</th>
			<th>Tgl Sakit</th>
			<th>Apa disekolah ada sakit yang sama</th>
			<th>Tgl Sakit</th>
			<th>Apakah penderita kekurang gizi (BB/U)</th>
			<th>Alamat tinggal sementara</th>
			<th>Tgl PE</th>
			<th>Jml kasus tambahan</th>
			<th>Alamat tempat tinggal</th>
			<th>Tgl PE</th>
			<th>Jml kasus tambahan</th>
			<th>Sekolah</th>
			<th>Tgl PE</th>
			<th>Jml kasus tambahan</th>
			<th>Tempat kerja</th>
			<th>Tgl PE</th>
			<th>Jml kasus tambahan</th>
			<th>Lain-lain</th>
			<th>Ketarangan</th>
			<th>Tgl PE</th>
			<th>Jml kasus tambahan</th>
			<th>Total Kasus Tambahan</th>
			<th>Tgl Penyelidikan</th>
			<th>Pelaksana</th>
		</tr>
	</thead>
	<tbody>
		@if($dd)
		<?php $no = 1;?>
			@foreach($dd AS $key=>$val)
			<?php $val = (array) $val;?>
				<tr>
						<td>{!! $no++; !!}</td>
						<td>{!! $val['tgl_input']; !!}</td>
						<td>{!! $val['tgl_update']; !!}</td>
						<td>{!! $val['no_rm']; !!}</td>
						<td>{!! $val['no_epid']; !!}</td>
						<td>{!! $val['_no_epid']; !!}</td>
						<td>{!! $val['no_epid_lama']; !!}</td>
						<td>{!! $val['no_epid_klb']; !!}</td>
						<td>{!! $val['name_faskes']; !!}</td>
						<td>{!! $val['name_kecamatan_faskes']; !!}</td>
						<td>{!! $val['name_kabupaten_faskes']; !!}</td>
						<td>{!! $val['name_provinsi_faskes']; !!}</td>
						<td>{!! $val['name_pasien']; !!}</td>
						<td>{!! $val['nik']; !!}</td>
						<td>{!! $val['agama']; !!}</td>
						<td>{!! $val['nama_ortu']; !!}</td>
						<td>{!! $val['jenis_kelamin_txt']; !!}</td>
						<td>{!! $val['tgl_lahir']; !!}</td>
						<td>{!! $val['umur_thn']; !!}</td>
						<td>{!! $val['umur_bln']; !!}</td>
						<td>{!! $val['umur_hari']; !!}</td>
						<td>{!! $val['alamat']; !!}</td>
						<td>{!! $val['kelurahan_pasien']; !!}</td>
						<td>{!! $val['kecamatan_pasien']; !!}</td>
						<td>{!! $val['kabupaten_pasien']; !!}</td>
						<td>{!! $val['provinsi_pasien']; !!}</td>
						<td>{!! $val['tgl_imunisasi_campak']; !!}</td>
						<td>{!! $val['jml_imunisasi_campak_txt']; !!}</td>
						<td>{!! $val['tgl_mulai_demam']; !!}</td>
						<td>{!! $val['tgl_mulai_rash']; !!}</td>
						<?php
						$gg = explode('|',$val['gejala']);
						$dg = [];
						foreach ($gg as $vg) {
							$d = explode('_',$vg);
							$dg[$d[0]] = !empty($d[1]) ? $d[1]:'';
						}
						?>
						<td  class="text-center">{!! !empty($dg[1]) ? 'V':'' !!}</td>
						<td >{!! !empty($dg[1]) ? $dg[1]:'' !!}</td>
						<td  class="text-center">{!! !empty($dg[2]) ? 'V':'' !!}</td>
						<td >{!! !empty($dg[2]) ? $dg[2]:'' !!}</td>
						<td  class="text-center">{!! !empty($dg[3]) ? 'V':'' !!}</td>
						<td >{!! !empty($dg[3]) ? $dg[3]:'' !!}</td>
						<?php
						$kk = explode('|',$val['komplikasi']);
						$dk = [];
						foreach ($kk as $vk) {
							$dk[$vk] = 'V';
						}
						?>
						<td  class="text-center">{!! !empty($dk['Diare']) ? $dk['Diare']:'' !!}</td>
						<td  class="text-center">{!! !empty($dk['Pneumonia']) ? $dk['Pneumonia']:'' !!}</td>
						<td  class="text-center">{!! !empty($dk['Bronchopneumonia']) ? $dk['Bronchopneumonia']:'' !!}</td>
						<td  class="text-center">{!! !empty($dk['OMA']) ? $dk['OMA']:'' !!}</td>
						<td  class="text-center">{!! !empty($dk['Ensefalitis']) ? $dk['Ensefalitis']:'' !!}</td>
						<td >{!! $val['other_komplikasi'] !!}</td>
						<td >{!! $val['tgl_laporan_diterima'] !!}</td>
						<td >{!! $val['tgl_pelacakan'] !!}</td>
						<td >{!! $val['vitamin_a_txt'] !!}</td>
						<td >{!! $val['keadaan_akhir_txt'] !!}</td>
						<td >{!! $val['jenis_kasus_txt'] !!}</td>
						<td >{!! $val['klb_ke'] !!}</td>
						<td >{!! $val['status_kasus_txt'] !!}</td>
						<?php
						$vs = explode('|',$val['spesimen']);
						$ds = [];
						$s0 = $s1 = $s2 = $s3 = '';
						foreach ($vs as $vss) {
							$d = explode('_',$vss);
							if (!empty($d[0])) {
								if ($d[0] == 1) {
									$s0 .= 'Serologi<br/>';
								}else if ($d[0] == 2) {
									$s0 .= 'Virologi<br/>';
								}
							}else{
								$s0 .= '<br/>';
							}
							if (!empty($d[1])) {
								if ($d[1] == 11) {
									$s1 .= 'Darah<br/>';
								}else if ($d[1] == 12) {
									$s1 .= 'Urin<br/>';
								}else if ($d[1] == 21) {
									$s1 .= 'Urin<br/>';
								}else if ($d[1] == 22) {
									$s1 .= 'Usap Tenggorokan<br/>';
								}else if ($d[1] == 23) {
									$s1 .= 'Cairan mulut<br/>';
								}
							}else{
								$s1 .= '<br/>';
							}
							$s2 .= !empty($d[2])?$d[2].'<br/>':'<br/>';
							if (!empty($d[3])) {
								if ($d[0] == 1 && $d[3] == 1) {
									$s3 .= 'IgM Campak Positif<br/>';
								}else if ($d[0] == 1 && $d[3] == 2) {
									$s3 .= 'IgM Campak Negatif<br/>';
								}else if ($d[0] == 1 && $d[3] == 3) {
									$s3 .= 'IgM Rubella Positif<br/>';
								}else if ($d[0] == 1 && $d[3] == 4) {
									$s3 .= 'IgM Rubella Positif<br/>';
								}else if ($d[0] == 1 && $d[3] == 5) {
									$s3 .= 'IgM Campak Positif & IgM Rubella Positif<br/>';
								}else if ($d[0] == 1 && $d[3] == 6) {
									$s3 .= 'IgM Rubella Equivocal<br/>';
								}else if ($d[0] == 1 && $d[3] == 7) {
									$s3 .= 'IgM Campak Equivocal<br/>';
								}else if ($d[0] == 1 && $d[3] == 8) {
									$s3 .= 'IgM Campak Negatif & IgM Rubella Negatif<br/>';
								}else if ($d[0] == 2) {
									$s3 .= $d[3];
								}
							}else{
								$s3 .= '<br/>';
							}
						}
						?>
						<td >{!! $s0 !!}</td>
						<td >{!! $s1 !!}</td>
						<td >{!! $s2 !!}</td>
						<td >{!! $s3 !!}</td>
						<td>{!! $val['klasifikasi_final_txt']; !!}</td>
						<td>{!! $val['longitude']; !!}</td>
						<td>{!! $val['latitude']; !!}</td>
						<td>{!! $val['tgl_pengobatan_pertama_kali']; !!}</td>
						<td>{!! $val['tempat_pengobatan_pertama_kali']; !!}</td>
						<td>{!! $val['obat_yg_diberikan']; !!}</td>
						<td>{!! $val['penyakit_sama_dirumah_txt']; !!}</td>
						<td>{!! $val['tgl_penyakit_sama_dirumah']; !!}</td>
						<td>{!! $val['penyakit_sama_disekolah_txt']; !!}</td>
						<td>{!! $val['tgl_penyakit_sama_disekolah']; !!}</td>
						<td>{!! $val['keadaan_kurang_gizi_txt']; !!}</td>
						<?php
						$jkt = explode('|',$val['jml_kasus_tambahan']);
						$djkt = [];
						$ll = $lk = $lt = $lj = '';
						if (!empty($val['jml_kasus_tambahan'])) {
							foreach ($jkt as $vjkt) {
								$djkt2 = explode('_', $vjkt);
								$djkt[$djkt2[0]] = [
									'ket'=>$djkt2[1],
									'tgl_pej'=>$djkt2[2],
									'jml_pej'=>!empty($djkt2[3])?$djkt2[3]:'',
								];
								if (!in_array($djkt2[0], ['k1','k2','k3','k4'])) {
									$ll .= $djkt2[0].'<br />';
									$lk .= $djkt2[1].'<br />';
									$lt .= $djkt2[2].'<br />';
									$lj .= !empty($djkt2[3])?$djkt2[3].'<br />':'';
								}
							}
						}
						?>
						<td>{!! !empty($djkt['k1']) ? $djkt['k1']['ket']:'' !!}</td>
						<td>{!! !empty($djkt['k1']) ? $djkt['k1']['tgl_pej']:'' !!}</td>
						<td>{!! !empty($djkt['k1']) ? $djkt['k1']['jml_pej']:'' !!}</td>
						<td>{!! !empty($djkt['k2']) ? $djkt['k2']['ket']:'' !!}</td>
						<td>{!! !empty($djkt['k2']) ? $djkt['k2']['tgl_pej']:'' !!}</td>
						<td>{!! !empty($djkt['k2']) ? $djkt['k2']['jml_pej']:'' !!}</td>
						<td>{!! !empty($djkt['k3']) ? $djkt['k3']['ket']:'' !!}</td>
						<td>{!! !empty($djkt['k3']) ? $djkt['k3']['tgl_pej']:'' !!}</td>
						<td>{!! !empty($djkt['k3']) ? $djkt['k3']['jml_pej']:'' !!}</td>
						<td>{!! !empty($djkt['k4']) ? $djkt['k4']['ket']:'' !!}</td>
						<td>{!! !empty($djkt['k4']) ? $djkt['k4']['tgl_pej']:'' !!}</td>
						<td>{!! !empty($djkt['k4']) ? $djkt['k4']['jml_pej']:'' !!}</td>
						<td>{!! !empty($ll) ? $ll : '' !!}</td>
						<td>{!! !empty($lk) ? $lk : '' !!}</td>
						<td>{!! !empty($lt) ? $lt : '' !!}</td>
						<td>{!! !empty($lj) ? $lj : '' !!}</td>
						<td>{!! $val['total_kasus_tambahan']; !!}</td>
						<td>{!! $val['tgl_penyelidikan_pe']; !!}</td>
						<td>{!! $val['nama_pelaksana']; !!}</td>
				</tr>
			@endforeach
		@endif
	</tbody>
</table>
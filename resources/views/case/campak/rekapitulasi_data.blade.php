<div class="row">
    <div class="col-sm-4">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Pencarian</h3>
            </div>
            <div class="box-body">
                <div id="mainfilter"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Laporan</h3>
            </div>
            <div class="box-body">
                content
            </div>
        </div>
    </div>
</div>

<script>
    $('#mainfilter').jstree({
		'core' : {
			'data' : {
                "url" : BASE_URL+'getExpandWilayah',
				"data" : function (node) {
					return { "code" : node.id };
				},
			}
		},
        "plugins" : [ "wholerow" ]
	});
</script>
@extends('layouts.base')
@section('content')
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Data Klinis Campak</h3>
	</div>
	<div class="box-body">
		<table id="table" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">No.Epid</th>
					<th rowspan="2">Nama Pasien</th>
					<th colspan="3">Usia</th>
					<th rowspan="2">Alamat</th>
					<th rowspan="2">Keadaan akhir</th>
					<th rowspan="2">Klasifikasi final</th>

				</tr>
				<tr>
					<th>Thn</th>
					<th>Bln</th>
					<th>Hari</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('#table').DataTable({
			// "paging": true,
			"lengthChange": true,
			"autoWidth": true,
			"processing": true,
			"serverSide": true,
			"ajax": {
				url     : '{!! URL::to('case/campak/getData') !!}',
				type    : "POST",
				// data 	: function(d){
				// 	d.dt = JSON.stringify($('#filter').serializeJSON());
				// },
			},
			"columns"   : [
			{ "data" : "no" },
			{ "data" : "no_epid" },
			{ "data" : "name_pasien" },
			{ "data" : "umur_thn" },
			{ "data" : "umur_bln" },
			{ "data" : "umur_hari" },
			{ "data" : "alamat" },
			{ "data" : "keadaan_akhir_txt" },
			{ "data" : "klasifikasi_final" },
			]
		});
		// $(".dataTables_filter").addClass('pull-right');
		//
		// $('#submit_filter').on('click',function(){
		// 	$('#table').DataTable().draw();
		// 	return false;
		// });

		$('#table').on('draw.dt', function(){
			$(".delete").unbind();
			$(".delete-yes").unbind();
			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					if(response.success){
						setTimeout( function(){
							messageAlert('info', 'Berhasil.', 'Data kasus berhasil di hapus.');
							endProcess();
						}, 0);
					}
					$('#table').DataTable().draw();
				});
				return false;
			});
		});
	});
</script>

@endsection

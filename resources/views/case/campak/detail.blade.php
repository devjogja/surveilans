@extends('layouts.base')
@section('content')
@if($dt = $data['response'])
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			<table class="table table-striped">
				<caption>Identitas Pasien</caption>
				<tbody>
					<tr>
						<td class="title">No Epidemologi</td>
						<td>{!! $dt->no_epid or '-' !!} { {!! $dt->_no_epid or '-' !!} }</td>
					</tr>
					@if($dt->no_epid_klb)
					<tr>
						<td class="title">No Epidemologi KLB</td>
						<td>{!! $dt->no_epid_klb or '-' !!}</td>
					</tr>
					@endif
					<tr>
						<td class="title">No Epidemologi Lama</td>
						<td>{!! $dt->no_epid_lama or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">No RM</td>
						<td>{!! $dt->no_rm or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Penderita</td>
						<td>{!! $dt->name_pasien or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">NIK</td>
						<td>{!! $dt->nik or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama orang tua</td>
						<td>{!! $dt->nama_ortu or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Jenis kelamin</td>
						<td>{!! $dt->jenis_kelamin_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Lahir(Umur)</td>
						<td><?php
						$tgl_lahir = ($dt->tgl_lahir)?$dt->tgl_lahir:'';
						$umur_thn = ($dt->umur_thn)?$dt->umur_thn.' Th':'';
						$umur_bln = ($dt->umur_bln)?$dt->umur_bln.' Bln':'';
						$umur_hari = ($dt->umur_hari)?$dt->umur_hari.' Hari':'';
						?>
					{!! $tgl_lahir !!} ({!!$umur_thn!!} {!!$umur_bln !!} {!!$umur_hari!!})</td>
				</tr>
				<tr>
					<td class="title">Agama</td>
					<td>{!! $dt->religion_text or '-' !!}</td>
				</tr>
				<tr>
					<td class="title">Alamat</td>
					<td>{!! $dt->alamat or '' !!} {!! $dt->full_address or '' !!}</td>
				</tr>
				<tr>
					<td class="title">Longitude</td>
					<td>{!! $dt->longitude or '-' !!}</td>
				</tr>
				<tr>
					<td class="title">Latitude</td>
					<td>{!! $dt->latitude or '-' !!}</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="box-body">
		<table class="table table-striped">
			<caption>Data Klinis Campak</caption>
			<tbody>
				<tr>
					<td class="title">Tanggal imunisasi campak terakhir</td>
					<td>{!! $dt->tgl_imunisasi_campak or '-' !!}</td>
				</tr>
				<tr>
					<td class="title">Imunisasi campak sebelum sakit berapa kali</td>
					<td>{!! $dt->jml_imunisasi_campak_txt !!}</td>
				</tr>
				<tr>
					<td class="title">Tanggal mulai demam</td>
					<td>{!! $dt->tgl_mulai_demam or '-' !!}</td>
				</tr>
				<tr>
					<td class="title">Tanggal mulai rash</td>
					<td>{!! $dt->tgl_mulai_rash or '-' !!}</td>
				</tr>
				<tr>
					<td class="title">Gejala / tanda lainnya</td>
					<td>
						@if($dtGejala = $data['response']->dtGejala)
						<table class="table table-striped" style="width: 25%">
							<thead>
								<tr>
									<th>Nama Gejala</th>
									<th>Tanggal Kejadian</th>
								</tr>
							</thead>
							<tbody>
								@foreach($dtGejala AS $key=>$val)
								@if(!empty($val->name))
								<tr>
									<td>{!! $val->name !!}</td>
									<td>{!! $val->tgl_kejadian or '-' !!}</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
						@else
						-
						@endif
					</td>
				</tr>
				<tr>
					<td class="title">Komplikasi</td>
					<td>
						@if($dtKomplikasi = $data['response']->dtKomplikasi)
						<table class="table table-striped" style="width: 25%">
							<thead>
								<tr><th>Nama Komplikasi</th></tr>
							</thead>
							<tbody>
								@foreach($dtKomplikasi AS $key=>$val)
								@if(!empty($val->name) || !empty($val->other_komplikasi))
								<tr><td>{!!$val->name!!} {!! $val->other_komplikasi !!}</td></tr>
								@endif
								@endforeach
							</tbody>
						</table>
						@else
						-
						@endif
					</td>
				</tr>
				<tr>
					<td class="title">Tanggal laporan diterima</td>
					<td>{!! $dt->tgl_laporan_diterima or '-' !!}</td>
				</tr>
				<tr>
					<td class="title">Tanggal pelacakan</td>
					<td>{!! $dt->tgl_pelacakan or '-' !!}</td>
				</tr>
				<tr>
					<td class="title">Diberi vitamin A</td>
					<td>{!! $dt->vitamin_a_txt !!}</td>
				</tr>
				<tr>
					<td class="title">Keadaan akhir</td>
					<td>{!! $dt->keadaan_akhir_txt !!}</td>
				</tr>
				<tr>
					<td class="title">Jenis kasus</td>
					<td>{!! $dt->jenis_kasus_txt !!}</td>
				</tr>
				@if($dt->klb_ke)
				<tr>
					<td class="title">KLB Ke-</td>
					<td>{!! $dt->klb_ke !!}</td>
				</tr>
				@endif
				<tr>
					<td class="title">Status Kasus</td>
					<td>{!! $dt->status_kasus_txt !!}</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="box-body">
		<table class="table table-striped">
			<caption>Data spesimen dan hasil laboratorium</caption>
			<thead>
				<tr>
					<th>Jenis Pemeriksaan</th>
					<th>Jenis Sampel</th>
					<th>Tanggal ambil sampel</th>
					<th>Hasil Lab</th>
				</tr>
			</thead>
			<tbody>
				@if(empty($data['response']->dtSpesimen))
				<tr><td colspan="4">Tidak terdapat data spesimen yang diambil.</td></tr>
				@else
				@foreach($data['response']->dtSpesimen AS $key=>$val)
				<tr>
					<td>{!! $val->jenis_pemeriksaan_txt !!}</td>
					<td>{!! $val->jenis_spesimen_txt !!}</td>
					<td>{!! $val->tgl_ambil_spesimen or '-' !!}</td>
					<td>{!! $val->hasil_txt !!}</td>
				</tr>
				@endforeach
				@endif
			</tbody>
		</table>
	</div>
	<div class="box-body">
		<table class="table table-striped">
			<caption>Klasifikasi Final</caption>
			<tbody>
				<tr>
					<td class="title">Hasil klasifikasi final</td>
					<td>{!! $dt->klasifikasi_final_txt or '-' !!}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
</div>
@endif
<div class="col-sm-12">
	<div class="footer">
		<a href="{!!URL::to('case/campak')!!}" class="btn btn-primary">Kembali</a>
	</div>
</div>
@endsection

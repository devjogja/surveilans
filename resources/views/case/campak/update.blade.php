@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			@include('case.campak.input_kasus')
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		getDetail();
	});

	function getDetail() {
		var id = $('#id_trx_case').val();
		var action = BASE_URL+'case/campak/getDetail/'+id;
		$.getJSON(action, function(result){
			var raw = result.response;
			if (isset(raw)) {
				$.each(raw, function(k, dt){
					$('#id_faskes').val(dt.id_faskes);
					$('#id_role').val(dt.id_role);
					$('#jenis_input').val(dt.jenis_input);
					$('#id_trx_case').val(dt.id);
					$('#name').val(dt.name_pasien);
					$('#id_pasien').val(dt.id_pasien);
					$('#nik').val(dt.nik);
					$('#no_rm').val(dt.no_rm);
					$('#agama').val(dt.agama).trigger('change');
					$('#nama_ortu').val(dt.nama_ortu);
					$('#id_family').val(dt.id_family);
					$('#jenis_kelamin').val(dt.jenis_kelamin).trigger('change');
					if(isset(dt.tgl_lahir)){$('#tgl_lahir').val(dt.tgl_lahir).removeAttr('disabled');}
					if(isset(dt.umur_hari)){$('#umur_hari').val(dt.umur_hari).removeAttr('disabled');}
					if(isset(dt.umur_bln)){$('#umur_bln').val(dt.umur_bln).removeAttr('disabled');}
					if(isset(dt.umur_thn)){$('#umur_thn').val(dt.umur_thn).removeAttr('disabled');}
					$('#alamat').val(dt.alamat);
					$('#wilayah').val(dt.full_address);
					$('#id_kelurahan_pasien').val(dt.code_kelurahan_pasien);
					$('#id_kecamatan_pasien').val(dt.code_kecamatan_pasien);
					$('#id_kabupaten_pasien').val(dt.code_kabupaten_pasien);
					$('#id_provinsi_pasien').val(dt.code_provinsi_pasien);
					$('#longitude').val(dt.longitude);
					$('#latitude').val(dt.latitude);
					$('#no_epid').val(dt.no_epid);
					if(isset(dt.no_epid_klb)){$('#no_epid_klb').val(dt.no_epid_klb);$('#klb').removeClass('hide')}
					$('#name_faskes').val(dt.name_faskes);
					$('#no_epid_lama').val(dt.no_epid_lama);
					$('#tgl_imunisasi_campak').val(dt.tgl_imunisasi_campak);
					$('#jml_imunisasi_campak').val(dt.jml_imunisasi_campak).trigger('change');
					$('#tgl_mulai_demam').val(dt.tgl_mulai_demam);
					$('#tgl_mulai_rash').val(dt.tgl_mulai_rash);
					$('#tgl_laporan_diterima').val(dt.tgl_laporan_diterima);
					$('#tgl_pelacakan').val(dt.tgl_pelacakan);
					$('#vitamin_A').val(dt.vitamin_a).trigger('change');
					$('#keadaan_akhir').val(dt.keadaan_akhir).trigger('change');
					$('#jenis_kasus').val(dt.jenis_kasus).trigger('change');
					if(isset(dt.klb_ke)){$('#klb_ke').val(dt.klb_ke).removeAttr('disabled').trigger('change');}
					$('#status_kasus').val(dt.status_kasus).trigger('change');
					$('#klasifikasi_final').val(dt.klasifikasi_final).trigger('change');

					var dtGejala = dt.dtGejala;
					var dtKomplikasi = dt.dtKomplikasi;
					var dtSpesimen = dt.dtSpesimen;
					$.each(dtGejala, function(key, val){
						$('#gejala'+val.id_gejala).iCheck('check').trigger('click');
						$('#tglKejadian'+val.id_gejala).val(val.tgl_kejadian);
					});
					var other_komplikasi = '';
					$.each(dtKomplikasi, function(key, val){
						$('#'+val.name).iCheck('check');
						if(isset(val.other_komplikasi)){
							other_komplikasi += val.other_komplikasi+', ';
						}
					});
					if(isset(other_komplikasi)){$('#komplikasi_lain_desc').val(other_komplikasi).removeAttr('disabled');}
					$.each(dtSpesimen, function(key, val){
						var hasillab = '';
						if(val.jenis_pemeriksaan=='1'){
							hasillab = "<select name='ds[hasil_lab][]' class='form-control ck spesimen"+key+"'>"+
							"<option value=''>-- Pilih --</option>"+
							"<option value='1'>IgM Campak Positif</option>"+
							"<option value='2'>IgM Campak Negatif</option>"+
							"<option value='3'>IgM Rubella Positif</option>"+
							"<option value='4'>IgM Rubella Negatif</option>"+
							"<option value='5'>IgM Campak Positif & IgM Rubella Positif</option>"+
							"<option value='6'>IgM Rubella Equivocal</option>"+
							"<option value='7'>IgM Campak Equivocal</option>"+
							"<option value='8'>IgM Campak Negatif & IgM Rubella Negatif</option>"+
							"</select>";
						}else{
							hasillab = "<input type='text' class='form-control ck' value='"+val.hasil+"' name='ds[hasil_lab][]'>";
						}
						$('#spesimen tbody').append(
							'<tr>'+
							'<td>'+val.jenis_pemeriksaan_txt+'</td>'+
							'<td>'+val.jenis_spesimen_txt+'</td>'+
							'<td>'+val.tgl_ambil_spesimen+'</td>'+
							@if(Helper::role()->id_role=='6')
							'<td>'+hasillab+'</td>'+
							@endif
							'<td class="text-center"><a href="javascript:void(0);" class="btn-sm btn-warning rm"><i class="fa fa-remove"></i></a>'+
							'<input type="hidden" name="ds[spesimen][]" value="'+val.jenis_pemeriksaan+'|'+val.jenis_spesimen+'|'+val.tgl_ambil_spesimen+'"></td>'+
							'</tr>'
							);
						$('.spesimen'+key).val(val.hasil).trigger('change');
						$('select').select2({ allowClear  : false, closeOnSelect : true, width : '100%' });
						$('.rm').on('click',function(){
							$(this).parent().parent().remove();
						});
						$('.ck').on('change',function(){
							var val = $(this).val();
							var vk = '';
							if(val=='1'){
								vk = 2;
							}else if(val=='3'){
								vk = 4;
							}else if(val=='8'){
								vk = 5;
							}else if(val=='5'){
								vk = 6;
							}else if(val=='7'){
								vk = 2;
							}else if(val=='6'){
								vk = 4;
							}
							$('#klasifikasi_final').val(vk).trigger('change');
						});
					});
				});
			return false;
			};
		});
}
</script>

@endsection

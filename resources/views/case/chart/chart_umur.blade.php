<div class="box-body">
	<div id="graph_umur" class="chart" style="margin: 0 auto"></div>
</div>

<script type="text/javascript">
	function graphUmur(title1,title2,range,category,data){
		if (title2==null) {
			title2=" ";
		}
		$('#graph_umur').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: 'Grafik Penderita '+title1+''+title2+' Berdasar Umur'
			},
			xAxis: {
				categories :category
			},
			yAxis: {
				title: {
					text: 'Jumlah Penderita (orang)'
				}
			},
			subtitle: {
				text: range
			},
			legend: {
				enabled: true,
				align: 'center',
				verticalAlign: 'bottom',
				labelFormatter: function() {
					var total=0, string;
					Highcharts.each(this.userOptions.data, function(p, i) {
						total += p;
					});
					string = this.name+'<br>Jumlah Penderita: ' + total+' orang';
					return string;
				}
			},
			tooltip: {
				pointFormat: 'Jumlah Kasus '+title1+''+title2+' tahun 2008: <b>{point.y:.1f} orang</b>'
			},
			plotOptions: {
				series: {
					borderWidth: 0,
					dataLabels: {
						enabled: true,
						format: '{point.y} orang',
					}
				}
			},
			credits:{
				enabled:false
			},
			series: data
		});
	}

</script>

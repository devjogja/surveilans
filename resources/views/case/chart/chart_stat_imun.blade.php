<div class="box-body">
	<div id="graph_status_imunisasi" class="chart" style="margin: 0 auto"></div>
	<div><center><span style="font-size:larger;font-weight:bold">Total Penderita :</span>  <span style="font-size:larger" id="totalStatImun"></span></center></div>
</div>

<script type="text/javascript">
	function graphStatImun(title1,title2,range,data){
		if (title2==null) {
			title2=" ";
		}
		$('#graph_status_imunisasi').highcharts({
			chart: {
				type: 'pie',
				options3d: {
					enabled: true,
					alpha: 45
				},
				events: {
					load: function(event) {
						var total = 0;
						for(var i=0, len=this.series[0].yData.length; i<len; i++){
							total += this.series[0].yData[i];
						}
						jumlah =  total;
					}
				}
			},
			title: {
				text: '',
			},
			subtitle: {
				text: range
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					cursor: 'pointer',
					depth: 45,
					dataLabels: {
						distance:1,
						enabled: true,
						connectorWidth: 2,
						format: '<span style="font-size:larger">{point.name} </span>({point.percentage:.2f}%)<br />{point.y:.1f} orang',
						style: {
							color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						}
					}
				}
			},
			credits:{
				enabled:false
			},
			series: [{
				name: 'Status Imunisasi',
				colorByPoint: true,
				data: data
			}]
		});
		chart = $('#graph_status_imunisasi').highcharts();
		chart.setTitle({text:'Grafik Penderita '+title1+' Berdasar Status Imunisasi'+title2+' '}, {text:range})
		document.getElementById("totalStatImun").innerHTML=jumlah+" orang";
	}
</script>

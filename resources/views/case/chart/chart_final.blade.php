<div class="box-body">
	<div id="graph_final" class="chart" style="margin: 0 auto"></div>
	<div><center><span style="font-size:larger;font-weight:bold">Total Penderita :</span>  <span style="font-size:larger" id="totalKlasifikasiFinal"></span></center></div>
</div>

<script type="text/javascript">
	function graphFinal(title,range,data){
		$('#graph_final').highcharts({
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false,
				type: 'pie',
				options3d: {
					enabled: true,
					alpha: 45
				},
				events: {
					load: function(event) {
						var total = 0;
						for(var i=0, len=this.series[0].yData.length; i<len; i++){
							total += this.series[0].yData[i];
						}
						jumlah =  total;
					}
				}
			},
			title: {
				text: 'Grafik Penderita '+title+' Berdasar Klasifikasi Final',
			},
			subtitle: {
				text: range
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					depth: 35,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						format: '<span style="font-size:larger">{point.name}</span><br />{point.y:.1f} orang',
						style: {
							color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						}
					}
				}
			},
			credits:{
				enabled:false
			},
			series: [{
				name: 'Klasifikasi Final',
				colorByPoint: true,
				data: data
			}]
		});
		chart = $('#graph_final').highcharts();
		chart.setTitle({text:'Grafik Penderita '+title+' Berdasar Klasifikasi Final'}, {text:range})
		document.getElementById("totalKlasifikasiFinal").innerHTML=jumlah+" orang";
	}
</script>

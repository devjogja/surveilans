<div class="box-body">
	<div id="graph_waktu" class="chart" style="margin: 0 auto"></div>
	<div class="text-center" style="font-weight: bold" >Total Penderita : <span id="totalBulan"></span></div>
</div>

<script type="text/javascript">
	function graphWaktu(title1,title2,faskes,data){
		if (title2==null) {
			title2=" ";
		}
		$('#graph_waktu').highcharts({
			chart: {
				type: 'column',
				events: {
					load: function(event) {
						var total = 0;
						$.each(this.series, function(p, i){
							$.each(i.yData, function(x, y){
								total += y;
							})
						});
						jmlTotalBulan = total;
					}
				}
			},
			title: {
				text: 'Grafik Penderita '+title1+''+title2+' Berdasar Waktu'
			},
			subtitle: {
				text: faskes
			},
			xAxis: {
				type: 'category'
			},
			yAxis: {
				title: {
					text: 'Jumlah Penderita (orang)'
				}
			},
			legend: {
				enabled: true,
				align: 'left',
				verticalAlign: 'bottom',
				labelFormatter: function() {
					var total=0, string;
					Highcharts.each(this.userOptions.data, function(p, i) {
						if (p.y) {
							tmp = parseInt(p.y | 0);
							total += tmp;
						}else if (p.y==0) {
							tmp = 0;
							total += tmp;
						}else {
							total += p[1];
						}
					});
					string = this.name+'<br>Jumlah Penderita: ' + total+' orang';
					return string;
				}
			},
			tooltip: {
				formatter: function() {
					if (this.point.drilldown) {
						return  'Jumlah kasus bulan '+this.key+' '+this.series.name+': <b>'+this.point.y+' orang</b>';
					}else{
						return  'Jumlah kasus tanggal '+this.key+' '+this.series.name+': <b>'+this.point.y+' orang</b>';
					}
				}
			},
			plotOptions: {
				series: {
					borderWidth: 0,
					dataLabels: {
						enabled: true,
						format: '{point.y} orang',
					}
				}
			},
			credits:{
				enabled:false
			},
			series:data.data,
			drilldown: {
				series: data.drilldown
			}
		});
		document.getElementById("totalBulan").innerHTML=jmlTotalBulan+" orang";
	};
</script>

<div id="graph_jenis_kelamin" class="chart" style="margin: 0 auto"></div>
<div>
	<center style="font-size:larger;font-weight:bold">
		<span>Total Penderita :</span>
		<span id="totalGender"></span>
	</center>
</div>

<script type="text/javascript">
	function graphJenisKelamin(title1,title2,range,data){
		if (title2==null) {
			title2=" ";
		}
		var jumlah='';
		$('#graph_jenis_kelamin').highcharts({
			chart: {
				type: 'pie',
				options3d: {
					enabled: true,
					alpha: 45
				},
				events: {
					load: function(event) {
						var total = 0;
						for(var i=0, len=this.series[0].yData.length; i<len; i++){
							total += this.series[0].yData[i];
						}
						jumlah =  total;
					}
				}
			},
			title: {
				text: '',
			},
			subtitle: {
				text: range
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					cursor: 'pointer',
					depth: 45,
					dataLabels: {
						distance:1,
						enabled: true,
						connectorWidth: 2,
						format: '<span style="font-size:larger">{point.name} </span>({point.percentage:.2f}%)<br />{point.y:.1f} orang',
						style: {
							color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						}
					}
				}
			},
			credits:{
				enabled:false
			},
			series: [{
				name: 'Jenis Kelamin',
				colorByPoint: true,
				data: data
			}]
		});
		chart = $('#graph_jenis_kelamin').highcharts();
		chart.setTitle({text:'Grafik Penderita '+title1+''+title2+' Berdasar Jenis Kelamin'}, {text:range})
		document.getElementById("totalGender").innerHTML=jumlah+" orang";
	}
</script>

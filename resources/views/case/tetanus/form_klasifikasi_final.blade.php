<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Klasifikasi final</h3>
	</div>
	<div class="form-horizontal">
		<div class="box-body">
			<div class="form-group">
				{!! Form::label(null, 'Klasifikasi final', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-3">
					{!! Form::select('dc[klasifikasi_final]', array(null=>'--Pilih--','1'=>'Konfirm TN','2'=>'Tersangka TN','3'=>'Bukan TN'), null, ['class' => 'form-control', 'id'=>'klasifikasi_final']) !!}
				</div>
			</div>
		</div>
	</div>
</div>
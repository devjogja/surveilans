@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			@include('case.tetanus.input_kasus')
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		getDetail();
	});

	function getDetail() {
		var id = $('#id_trx_case').val();
		var action = BASE_URL+'case/tetanus/getDetail/'+id;
		$.getJSON(action, function(result){
			var raw = result.response;
			if (isset(raw)) {
				$.each(raw, function(k, dt){
					$('#id_faskes').val(dt.id_faskes);
					$('#id_role').val(dt.id_role);
					$('#jenis_input').val(dt.jenis_input);
					$('#id_trx_case').val(dt.id);
					$('#name').val(dt.name_pasien);
					$('#id_pasien').val(dt.id_pasien);
					$('#nik').val(dt.nik);
					$('#no_rm').val(dt.no_rm);
					$('#nama_ortu').val(dt.nama_ortu);
					$('#id_family').val(dt.id_family);
					$('#jenis_kelamin').val(dt.jenis_kelamin).trigger('change');
					if(isset(dt.tgl_lahir)){$('#tgl_lahir').val(dt.tgl_lahir).removeAttr('disabled');}
					if(isset(dt.umur_hari)){$('#umur_hari').val(dt.umur_hari).removeAttr('disabled');}
					if(isset(dt.umur_bln)){$('#umur_bln').val(dt.umur_bln).removeAttr('disabled');}
					if(isset(dt.umur_thn)){$('#umur_thn').val(dt.umur_thn).removeAttr('disabled');}
					$('#alamat').val(dt.alamat);
					$('#wilayah').val(dt.full_address);
					$('#id_kelurahan_pasien').val(dt.code_kelurahan_pasien);
					$('#id_kecamatan_pasien').val(dt.code_kecamatan_pasien);
					$('#id_kabupaten_pasien').val(dt.code_kabupaten_pasien);
					$('#id_provinsi_pasien').val(dt.code_provinsi_pasien);
					$('#no_epid').val(dt.no_epid);
					$('#no_epid_lama').val(dt.no_epid_lama);
					$('#tgl_mulai_sakit').val(dt.tgl_mulai_sakit);
					$('#tgl_laporan_diterima').val(dt.tgl_laporan_diterima);
					$('#tgl_pelacakan').val(dt.tgl_pelacakan);
					$('#antenatal_care').val(dt.antenatal_care).trigger('change');
					$('#status_imunisasi_ibu').val(dt.status_imunisasi_ibu).trigger('change');
					$('#penolong_persalinan').val(dt.penolong_persalinan).trigger('change');
					$('#alat_pemotong_tali_pusat').val(dt.alat_pemotong_tali_pusat).trigger('change');
					$('#perawatan_tali_pusat').val(dt.perawatan_tali_pusat).trigger('change');
					$('#rawat_rumah_sakit').val(dt.rawat_rumah_sakit).trigger('change');
					$('#keadaan_akhir').val(dt.keadaan_akhir).trigger('change');
					$('#klasifikasi_final').val(dt.klasifikasi_final).trigger('change');
				});
				return false;
			};
		});
	}
</script>

@endsection
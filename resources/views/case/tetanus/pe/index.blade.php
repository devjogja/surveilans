@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="alert alert-notif alert-dismissable">
		<i class="icon fa fa-warning"></i> Text input yang bertanda bintang (*) wajib di isi
	</div>
	<div class="row">
		{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form', 'class' => 'form-horizontal']) !!}
		{!! Form::hidden('id_trx_case', $data['id_trx_case'], ['id'=>'id_trx_case']) !!}
		{!! Form::hidden('id_trx_pe', $data['id_trx_pe'], ['id'=>'id_trx_pe']) !!}
		{!! Form::hidden('dpe[id_faskes]', Helper::role()->id_faskes,['id'=>'id_faskes']) !!}
		{!! Form::hidden('dpe[id_role]', Helper::role()->id_role,['id'=>'id_role']) !!}
		{!! Form::hidden('dpe[jenis_input]', '1',['id'=>'jenis_input']) !!}
		<div class="col-sm-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Informasi Pelapor</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Sumber Laporan', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dpel[sumber_laporan]', array(null=>'--Pilih--','1'=>'Rumah Sakit','2'=>'Puskesmas','3'=>'Praktek Swasta','4'=>'Masyarakat','5'=>'Lain-lain'), null, ['class' => 'form-control', 'id'=>'sumber_laporan']) !!}
							</div>
						</div> 
						<div class="form-group">
							{!! Form::label(null, 'Jika lain-lain sebutkan', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dpel[sumber_laporan_lain]', null, ['class' => 'form-control','id'=>'sumber_laporan_lain','disabled','placeholder'=>'Sebutkan dengan lengkap']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Tanggal Laporan Diterima', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dpel[tgl_laporan]', null, ['class' => 'form-control datemax','id'=>'tgl_laporan','placeholder'=>'Tanggal laporan diterima']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Tanggal Pelacakan', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dpel[tgl_investigasi]', null, ['class' => 'form-control datemax','id'=>'tgl_investigasi','placeholder'=>'Tanggal pelacakan']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Identitas Bayi</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'No.Epidemologi', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('', null, ['class' => 'form-control','placeholder'=>'No.Epidemologi','id'=>'no_epid','readonly']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Nama Bayi ', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dp[name]', null, ['class' => 'form-control','placeholder'=>'Nama bayi (Jika belum diberi nama, maka diisi “An Nama Ibu Kandung”)','id'=>'name']) !!}
								{!! Form::hidden('id_pasien', null, ['class' => 'form-control','id'=>'id_pasien']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Jenis Kelamin', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dp[jenis_kelamin]', array(null=>'--Pilih--','L'=>'Laki-laki','P'=>'Perempuan','T'=>'Tidak Jelas'), null, ['class' => 'form-control', 'id'=>'jenis_kelamin']) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								<input type="radio" name="tgl_lahir" value="1" class="age">&nbsp;Tanggal Lahir
							</label>
							<div class="col-sm-3">
								{!! Form::text('dp[tgl_lahir]', null, ['class' => 'form-control tgl_lahir datemax','placeholder'=>'Tanggal Lahir','id'=>'tgl_lahir','disabled']) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								<input type="radio" name="tgl_lahir" value="2" class="age">&nbsp;Umur
							</label>
							<div class="col-sm-3" style="padding-right: 0px;">
								<div class="input-group">
									{!! Form::text('dp[umur_hari]', null, ['class' => 'form-control umur','disabled','id'=>'umur_hari']) !!}
									<span class="input-group-addon">Hari</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Anak ke', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-3">
								{!! Form::text('dp[anak_keberapa]', null, ['class' => 'form-control','placeholder'=>'Anak ke berapa','id'=>'anak_keberapa']) !!}
							</div>
						</div>
						<!-- ayah -->
						<div class="form-group">
							{!! Form::label(null, 'Nama Ayah', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('df[nama_ayah]', null, ['class' => 'form-control','placeholder'=>'Nama ayah','id'=>'nama_ayah']) !!}
								{!! Form::hidden('id_family', null, ['class' => 'form-control','id'=>'id_family']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Umur Ayah', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-7">
								<div class="row">
									<div class="col-sm-2" style="padding-right: 0px;">
										<div class="input-group">
											{!! Form::text('df[umur_th_ayah]', null, ['class' => 'form-control','id'=>'umur_th_ayah']) !!}
											<span class="input-group-addon">Thn</span>
										</div>
									</div>
									<div class="col-sm-2" style="padding-right: 0px;">
										<div class="input-group">
											{!! Form::text('df[umur_bln_ayah]', null, ['class' => 'form-control','id'=>'umur_bln_ayah']) !!}
											<span class="input-group-addon">Bln</span>
										</div>
									</div>
									<div class="col-sm-2" style="padding-right: 0px;">
										<div class="input-group">
											{!! Form::text('df[umur_hari_ayah]', null, ['class' => 'form-control','id'=>'umur_hari_ayah']) !!}
											<span class="input-group-addon">Hari</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Pendidikan Terakhir Ayah', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('df[pend_terakhir_ayah]', array(null=>'--Pilih--')+Helper::getEducation(), null, ['class' => 'form-control', 'id'=>'pend_terakhir_ayah']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Pekerjaan Ayah', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('df[pekerjaan_ayah]', array(null=>'--Pilih--')+Helper::getOccupation(), null, ['class' => 'form-control', 'id'=>'pekerjaan_ayah']) !!}
							</div>
						</div>
						<!-- ibu -->
						<div class="form-group">
							{!! Form::label(null, 'Nama Ibu', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('df[nama_ibu]', null, ['class' => 'form-control','placeholder'=>'Nama Ibu','id'=>'nama_ibu']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Umur Ibu', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-7">
								<div class="row">
									<div class="col-sm-2" style="padding-right: 0px;">
										<div class="input-group">
											{!! Form::text('df[umur_th_ibu]', null, ['class' => 'form-control','id'=>'umur_th_ibu']) !!}
											<span class="input-group-addon">Thn</span>
										</div>
									</div>
									<div class="col-sm-2" style="padding-right: 0px;">
										<div class="input-group">
											{!! Form::text('df[umur_bln_ibu]', null, ['class' => 'form-control','id'=>'umur_bln_ibu']) !!}
											<span class="input-group-addon">Bln</span>
										</div>
									</div>
									<div class="col-sm-2" style="padding-right: 0px;">
										<div class="input-group">
											{!! Form::text('df[umur_hari_ibu]', null, ['class' => 'form-control','id'=>'umur_hari_ibu']) !!}
											<span class="input-group-addon">Hari</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Pendidikan Terakhir Ibu', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('df[pend_terakhir_ibu]', array(null=>'--Pilih--')+Helper::getEducation(), null, ['class' => 'form-control', 'id'=>'pend_terakhir_ibu']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Pekerjaan Ibu', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('df[pekerjaan_ibu]', array(null=>'--Pilih--')+Helper::getOccupation(), null, ['class' => 'form-control', 'id'=>'pekerjaan_ibu']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Provinsi', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dp[code_provinsi]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control','id'=>'id_provinsi_pasien','onchange'=>"getKabupaten('_pasien')"]) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Kabupaten', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dp[code_kabupaten]', array(null=>'Pilih Kabupaten/Kota'), null, ['class' => 'form-control', 'id'=>'id_kabupaten_pasien','onchange'=>"getKecamatan('_pasien')"]) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Kecamatan', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dp[code_kecamatan]', array(null=>'Pilih Kecamatan'), null, ['class' => 'form-control','id'=>'id_kecamatan_pasien','onchange'=>"getKelurahan('_pasien')"]) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Kelurahan', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dp[code_kelurahan]', array(null=>'Pilih Kelurahan'), null, ['class' => 'form-control','id'=>'id_kelurahan_pasien']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Alamat', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dp[alamat]', null, ['class' => 'form-control','placeholder'=>'Alamat (diisi lengkap Rt/Rw desa kelurahan)','id'=>'alamat']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Informasi Riwayat Kesakitan/Kematian bayi 3-28 Hari</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Nama yang diwawancarai', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dirk[nama_yg_diwawancarai]', null, ['class' => 'form-control','placeholder'=>'Nama yang diwawancarai','id'=>'nama_yg_diwawancarai']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Hubungan dengan keluarga bayi', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dirk[hub_dgn_keluarga_bayi]', null, ['class' => 'form-control','placeholder'=>'Hubungan dengan keluarga bayi','id'=>'hub_dgn_keluarga_bayi']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Bayi lahir hidup', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dirk[bayi_lahir_hidup]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'bayi_lahir_hidup']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Tanggal lahir bayi', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dirk[tgl_lahir_bayi]', null, ['class' => 'form-control datemax','id'=>'tgl_lahir_bayi','placeholder'=>'Tanggal lahir bayi']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Apakah bayi meninggal', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dirk[bayi_meninggal]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'bayi_meninggal']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Tanggal Meninggal', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dirk[tgl_meninggal]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal meninggal','id'=>'tgl_meninggal','disabled']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Umur Saat Meninggal', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dirk[umur_meninggal]', null, ['class' => 'form-control ','placeholder'=>'Umur saat meninggal','id'=>'umur_meninggal','disabled']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Waktu lahir apakah bayi menangis', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dirk[lahir_bayi_menangis]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak Tahu'), null, ['class' => 'form-control', 'id'=>'lahir_bayi_menangis']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Apakah terlihat tanda-tanda kehidupan lain dari bayi (misal : sedikit gerakan)', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dirk[tanda_kehidupan_lain]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak Tahu'), null, ['class' => 'form-control', 'id'=>'tanda_kehidupan_lain']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Setelah lahir, apakah bayi bisa menetek atau menghisap susu botol dengan baik', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dirk[bayi_menetek_menghisap_susu_botol_dgn_baik]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak Tahu'), null, ['class' => 'form-control', 'id'=>'bayi_menetek_menghisap_susu_botol_dgn_baik']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Apakah 3 hari kemudian tiba-tiba mulut bayi mencucu dan tidak bisa menetek ', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dirk[3_hari_tiba_tiba_mulut_bayi_mencucu_tidak_menetek]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak Tahu'), null, ['class' => 'form-control', 'id'=>'3_hari_tiba_tiba_mulut_bayi_mencucu_tidak_menetek']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Tanggal tidak mau menetek/minum', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dirk[tgl_tidak_mau_menetek]', null, ['class' => 'form-control datemax','id'=>'tgl_tidak_mau_menetek','placeholder'=>'Tanggal tidak mau menetek/minum']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Apakah bayi mudah kejang jika disentuh/terkena sinar atau bunyi', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dirk[bayi_mudah_kejang_jika_dsentuh]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak Tahu'), null, ['class' => 'form-control', 'id'=>'bayi_mudah_kejang_jika_dsentuh']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Tanggal mulai kejang (bila ada rangsang sentuh, suara,cahaya)', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dirk[tgl_mulai_kejang]', null, ['class' => 'form-control datemax','id'=>'tgl_mulai_kejang','placeholder'=>'Tanggal mulai kejang']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Bayi dirawat', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dirk[bayi_dirawat]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'bayi_dirawat']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Fasilitas kesehatan', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dirk[fasilitas_kesehatan]', null, ['class' => 'form-control','placeholder'=>'Fasilitas kesehatan','disabled','id'=>'fasilitas_kesehatan']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Tanggal mulai dirawat', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dirk[tgl_dirawat]', null, ['class' => 'form-control datemax','id'=>'tgl_dirawat','disabled','placeholder'=>'Tanggal mulai dirawat']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Keadaan bayi setelah dirawat ', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dirk[keadaan_bayi]', array(null=>'--Pilih--','1'=>'Sembuh','2'=>'Belum Sembuh','3'=>'Meninggal'), null, ['class' => 'form-control', 'id'=>'keadaan_bayi']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Riwayat kehamilan</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'GPA', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drk[gpa]', null, ['class' => 'form-control','id'=>'gpa','placeholder'=>'Masukkan  status GPA']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Berapa kali periksa kehamilan dengan ', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-7">
								<table class="col-sm-12">
									<tr style="height: 40px;">
										<td width= "45%">
											<label>
												<input type="checkbox" name="drk[periksa_dokter]" id="periksa_dokter" value="1" class="col-sm-1 periksa">&nbsp;Dokter
											</label>
										</td>
										<td>
											{!! Form::text('drk[jml_periksa_dokter]', null, ['class' => 'form-control','id'=>"jml_periksa_dokter",'placeholder'=>'Jumlah periksa','disabled']) !!}
										</td>
									</tr>
									<tr style="height: 40px;">
										<td >
											<label>
												<input type="checkbox" name="drk[periksa_bidan]" id="periksa_bidan" value="1" class="col-sm-1 periksa">&nbsp;Bidan/Perawat
											</label>
										</td>
										<td>
											{!! Form::text('drk[jml_periksa_bidan]', null, ['class' => 'form-control','id'=>"jml_periksa_bidan",'placeholder'=>'Jumlah periksa','disabled']) !!}
										</td>
									</tr>
									<tr style="height: 40px;">
										<td >
											<label>
												<input type="checkbox" name="drk[periksa_dukun]" id="periksa_dukun" value="1" class="col-sm-1 periksa">&nbsp;Dukun
											</label>
										</td>
										<td>
											{!! Form::text('drk[jml_periksa_dukun]', null, ['class' => 'form-control','id'=>"jml_periksa_dukun",'placeholder'=>'Jumlah periksa','disabled']) !!}
										</td>
									</tr>
									<tr style="height: 40px;">
										<td >
											<label>
												<input type="checkbox" name="drk[tidak_anc]" id="tidak_anc" value="1" class="col-sm-1 ">&nbsp;Tidak ANC
											</label>
										</td>
									</tr>
									<tr style="height: 40px;">
										<td >
											<label>
												<input type="checkbox" name="drk[tidak_jelas]" id="tidak_jelas" value="1" class="col-sm-1">&nbsp;Tidak jelas
											</label>
										</td>
									</tr>

								</table>
							</div>
						</div>

						<div class="form-group">
							{!! Form::label(null, 'Petugas Pemeriksa', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-9">
								<table class="table table-striped" id="petugas_pemeriksa">
									<thead style="background: #4caf50;color: white;">
										<tr>
											<th><center>Nama</center></th>
											<th><center>Profesi</center></th>
											<th><center>Alamat</center></th>
											<th><center>Frekuensi</center></th>
											<th><center>Action</center></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>{!! Form::text('drkp[nama][]', null, ['class' => 'form-control','placeholder'=>'Nama','id'=>'nama_petugas']) !!}</td>
											<td>{!! Form::select('drkp[profesi][]', array(null=>'--Pilih--','1'=>'Dokter','2'=>'Bidan/Perawat','3'=>'Dukun','4'=>'Lainnya'), null, ['class' => 'form-control', 'id'=>'profesi_petugas']) !!}</td>
											<td>{!! Form::text('drkp[alamat][]', null, ['class' => 'form-control','placeholder'=>'Alamat','id'=>'alamat_petugas']) !!}</td>
											<td>{!! Form::text('drkp[frekuensi][]', null, ['class' => 'form-control','placeholder'=>'Frekuensi','id'=>'frekuensi_petugas']) !!}</td>
											<td width="5%">
												<a title="Tambah Data" data-toggle="tooltip" class="btn-sm btn-success" onclick="add_petugas()"><i class="fa fa-plus"></i></a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="form-group">
							{!! Form::label(null, 'Apakah ibu pernah mendapat imunisasi TT pada waktu hamil bayi ini', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('drk[imunisasi_tt_ibu_waktu_hamil]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'imunisasi_tt_ibu_waktu_hamil']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Sumber informasi imunisasi TT', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('drk[sumber_informasi_imunisasi_tt]', array(null=>'--Pilih--','1'=>'Ingatan','2'=>'Buku Catatan'), null, ['class' => 'form-control', 'id'=>'sumber_informasi_imunisasi_tt']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Berapa kali mendapat imunisasi TT pada saat kehamilan bayi tersebut', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-8">
								<table class="col-sm-12">
									<tr style="height: 40px;">
										<td style="width: 25%">
											<label>
												<input type="checkbox" name="drk[imunisasi_ibu_tt_pertama]" id="imunisasi1" value="1" class="col-sm-1">&nbsp;Pertama Kali
											</label>
										</td>
										<td style="width: 40%;padding-right:10px">
											{!! Form::label('', 'Umur kehamilan saat mendapat imunisasi TT (bulan)', ['class' => 'control-label ','id'=>""]) !!}
										</td>
										<td style="width: 85%">
											{!! Form::text('drk[umur_kehamilan_ibu_tt_pertama]', null, ['class' => 'form-control imunisasi1','id'=>"umur_imunisasi1",'disabled','placeholder'=>'Dalam Bulan']) !!}
										</td>
									</tr>
									<tr style="height: 40px;">
										<td style="width: 25%">
										</td>
										<td style="width: 40%;padding-right:10px">
											{!! Form::label('', 'Tanggal imunisasi TT', ['class' => 'control-label','id'=>""]) !!}
										</td>
										<td style="width: 85%">
											{!! Form::text('drk[tgl_imunisasi_ibu_tt_pertama]', null, ['class' => 'form-control datemax imunisasi1','id'=>"tgl_imunisasi1",'disabled','placeholder'=>'Tanggal imunisasi']) !!}
										</td>
									</tr>
									<tr style="height: 40px;">
										<td style="width: 25%">
											<label>
												<input type="checkbox" name="drk[imunisasi_ibu_tt_kedua]" id="imunisasi2" value="1" class="col-sm-1">&nbsp;Kedua Kali
											</label>
										</td>
										<td style="width: 40%;padding-right:10px">
											{!! Form::label('', 'Umur kehamilan saat mendapat imunisasi (bulan)', ['class' => 'control-label','id'=>""]) !!}
										</td>
										<td style="width: 85%">
											{!! Form::text('drk[umur_kehamilan_ibu_tt_kedua]', null, ['class' => 'form-control imunisasi2','id'=>"umur_imunisasi2",'disabled','placeholder'=>'Dalam Bulan']) !!}
										</td>
									</tr>
									<tr style="height: 40px;">
										<td style="width: 25%">
										</td>
										<td style="width: 40%;padding-right:10px">
											{!! Form::label('', 'Tanggal imunisasi TT', ['class' => 'control-label','id'=>""]) !!}
										</td>
										<td style="width: 85%">
											{!! Form::text('drk[tgl_imunisasi_ibu_tt_kedua]', null, ['class' => 'form-control datemax imunisasi2','id'=>"tgl_imunisasi2",'disabled','placeholder'=>'Tanggal imunisasi']) !!}
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Apakah ibu mendapat imunisasi TT pada kehamilan sebelumnya', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('drk[imunisasi_tt_kehamilan_sebelumnya]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'imunisasi_TT']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Suntikan Pertama (tahun)', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drk[suntikan_tt_pertama_kehamilan_sebelumnya]', null, ['class' => 'form-control','id'=>"suntik1",'disabled','placeholder'=>'Tahun imunisasi']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Suntikan Kedua (tahun)', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drk[suntikan_tt_kedua_kehamilan_sebelumnya]', null, ['class' => 'form-control','id'=>"suntik2",'disabled','placeholder'=>'Tahun imunisasi']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Apakah ibu mendapat suntikan TT calon pengantin', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('drk[suntikan_tt_calon_pengantin]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'TT_pengantin']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Suntikan Pertama (tahun)', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drk[suntikan_tt_pertama_calon_pengantin]', null, ['class' => 'form-control','id'=>"suntik_1",'disabled','placeholder'=>'Tahun imunisasi']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Suntikan Kedua (tahun)', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drk[suntikan_tt_kedua_calon_pengantin]', null, ['class' => 'form-control','id'=>"suntik_2",'disabled','placeholder'=>'Tahun imunisasi']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Status TT ibu pada saat kehamilan bayi tersebut', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('drk[status_tt_ibu_saat_kehamilan]', array(null=>'--Pilih--','1'=>'TT1','2'=>'TT2','3'=>'TT3','4'=>'TT4','5'=>'TT5'), null, ['class' => 'form-control', 'id'=>'status_tt_ibu_saat_kehamilan']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<!-- Riwayat Persalinan -->
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Riwayat Persalinan</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Data Penolong Persalinan', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-9">
								<table class="table table-striped" id="data_persalinan">
									<thead style="background: #4caf50;color: white;">
										<tr>
											<th>Profesi</th>
											<th>Nama</th>
											<th>Alamat</th>
											<th>Tempat Persalinan</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>{!! Form::text('drpr[profesi][]', null, ['class' => 'form-control','placeholder'=>'Profesi','id'=>'profesi_persalinan']) !!}</td>
											<td>{!! Form::text('drpr[nama][]', null, ['class' => 'form-control','placeholder'=>'Nama','id'=>'nama_persalinan']) !!}</td>
											<td>{!! Form::text('drpr[alamat][]', null, ['class' => 'form-control','placeholder'=>'Alamat','id'=>'alamat_persalinan']) !!}</td>
											<td>{!! Form::text('drpr[tempat_persalinan][]', null, ['class' => 'form-control','placeholder'=>'Tempat Persalinan','id'=>'tempat_persalinan']) !!}</td>
											<td width="5%">
												<a title="Tambah Data" data-toggle="tooltip" class="btn-sm btn-success" onclick="add_persalinan()"><i class="fa fa-plus"></i></a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Obat yang dibubuhkan setelah tali pusat dipotong', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('drp[obat_yg_dibubuhkan_setelah_tali_pusat_dipotong]', array(null=>'--Pilih--','1'=>'Alkohol/Iod','2'=>'Ramuan Tradisional','3'=>'Kasa Kering','4'=>'Lainnya'), null, ['class' => 'form-control', 'id'=>'obat_yg_dibubuhkan_setelah_tali_pusat_dipotong']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Jika jawaban ramuan tradisional atau lainnya, sebutkan', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drp[sebutkan_obat_bubuhan]', null, ['class' => 'form-control','id'=>"sebutkan_obat_bubuhan",'placeholder'=>' Jika lebih dari satu jawaban, maka pisahkan dengan tanda baca koma']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Yang melakukan perawatan tali pusat sejak lahir sampai tali pusat puput', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('drp[petugas_perawat_tali_pusat]', array(null=>'--Pilih--','1'=>'Tenaga Kesehatan','2'=>'Bukan Tenaga Kesehatan'), null, ['class' => 'form-control', 'id'=>'petugas_perawat_tali_pusat']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Obat atau ramuan apa saja yang dibubuhkan selama merawat tali pusat', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drp[obat_saat_merawat_tali_pusat]', null, ['class' => 'form-control','id'=>"obat_saat_merawat_tali_pusat",'placeholder'=>' Jika lebih dari satu jawaban, maka pisahkan dengan tanda baca koma']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Kesimpulan diagnosis', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('drp[kesimpulan_diagnosis]', array(null=>'--Pilih--','1'=>'Konfirm TN','2'=>'Tersangka TN','3'=>'Bukan TN'), null, ['class' => 'form-control', 'id'=>'kesimpulan_diagnosis']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Data Cakupan</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Cakupan imunisasi TT di desa kasus TN', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								@for($i=1;$i<=5;$i++)
								<div class="form-group">
									{!! Form::label(null, "TT$i(%)", ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text("dtc[cakupan_imunisasi_tt$i]", null, ['class' => 'form-control','id'=>"cakupan_imunisasi_tt$i",'placeholder'=>'Dalam Persen']) !!}
									</div>
								</div>
								@endfor
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, "Cakupan persalinan tenaga kesehatan (%)", ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dtc[cakupan_persalinan_tenaga_kesehatan]', null, ['class' => 'form-control','id'=>'cakupan_persalinan_tenaga_kesehatan','placeholder'=>'Dalam Persen']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Cakupan kunjungan neonatus', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								@for($i=1;$i<=2;$i++)
								<div class="form-group">
									{!! Form::label(null, "KN$i(%)", ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text("dtc[cakupan_kunjungan_neonatus$i]", null, ['class' => 'form-control','id'=>"cakupan_kunjungan_neonatus$i",'placeholder'=>'Dalam Persen']) !!}
									</div>
								</div>
								@endfor
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Tim Pelacak</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							<div class="col-sm-12">
								<table class="table table-striped" id="tim_pelacak">
									<thead style="background: #4caf50;color: white;">
										<tr>
											<th>Nama</th>
											<th>Jabatan</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>{!! Form::text('dtpel[nama][]', null, ['class' => 'form-control','placeholder'=>'Nama','id'=>'nama_pelacak']) !!}</td>
											<td>{!! Form::text('dtpel[jabatan][]', null, ['class' => 'form-control','placeholder'=>'Jabatan','id'=>'jabatan_pelacak']) !!}</td>
											<td width="5%">
												<a title="Tambah Data" data-toggle="tooltip" class="btn-sm btn-success" onclick="add_tim()"><i class="fa fa-plus"></i></a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="footer">
				{!! Form::reset("Batal", ['class' => 'btn btn-warning batal']) !!}
				{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
			</div>
		</div>
	</div>
	{!! Form::close() !!}
</div>
<script type="text/javascript">
	$(function(){
		$('#sumber_laporan').on('change',function(){
			var val = $(this).val();
			if(val==5){
				$('#sumber_laporan_lain').removeAttr('disabled');
			}else{
				$('#sumber_laporan_lain').attr('disabled','disabled').val(null);
			}
			return false;
		});

		$('#bayi_dirawat').on('change',function(){
			var val = $(this).val();
			if(val==1){
				$('#fasilitas_kesehatan').removeAttr('disabled');
				$('#tgl_dirawat').removeAttr('disabled');
			}else{
				$('#fasilitas_kesehatan').attr('disabled','disabled').val(null);
				$('#tgl_dirawat').attr('disabled','disabled').val(null);
			}
			return false;
		});

		$('#imunisasi_TT').on('change',function(){
			var val = $(this).val();
			if(val==1){
				$('#suntik1').removeAttr('disabled');
				$('#suntik2').removeAttr('disabled');
			}else{
				$('#suntik1').attr('disabled','disabled').val(null);
				$('#suntik2').attr('disabled','disabled').val(null);
			}
			return false;
		});

		$('#TT_pengantin').on('change',function(){
			var val = $(this).val();
			if(val==1){
				$('#suntik_1').removeAttr('disabled');
				$('#suntik_2').removeAttr('disabled');
			}else{
				$('#suntik_1').attr('disabled','disabled').val(null);
				$('#suntik_2').attr('disabled','disabled').val(null);
			}
			return false;
		});

		$('#bayi_meninggal').on('change',function(){
			var val = $(this).val();
			if(val==1){
				$('#tgl_meninggal').removeAttr('disabled');
				$('#umur_meninggal').removeAttr('disabled');
			}else{
				$('#tgl_meninggal').attr('disabled','disabled').val(null);
				$('#umur_meninggal').attr('disabled','disabled').val(null);
			}
			return false;
		});

		$("input[id='periksa_dokter']").on('ifChecked',function(){
			$('#jml_periksa_dokter').removeAttr('disabled');
		});
		$("input[id='periksa_dokter']").on('ifUnchecked',function(){
			$('#jml_periksa_dokter').attr('disabled','disabled').val(null);
		});
		$("input[id='periksa_bidan']").on('ifChecked',function(){
			$('#jml_periksa_bidan').removeAttr('disabled');
		});
		$("input[id='periksa_bidan']").on('ifUnchecked',function(){
			$('#jml_periksa_bidan').attr('disabled','disabled').val(null);
		});
		$("input[id='periksa_dukun']").on('ifChecked',function(){
			$('#jml_periksa_dukun').removeAttr('disabled');
		});
		$("input[id='periksa_dukun']").on('ifUnchecked',function(){
			$('#jml_periksa_dukun').attr('disabled','disabled').val(null);
		});

		$("input[id='imunisasi1']").on('ifChecked',function(){
			$('.imunisasi1').removeAttr('disabled');
			$('.imunisasi1').removeAttr('disabled');
		});
		$("input[id='imunisasi1']").on('ifUnchecked',function(){
			$('.imunisasi1').attr('disabled','disabled').val(null);
			$('.imunisasi1').attr('disabled','disabled').val(null);
		});
		$("input[id='imunisasi2']").on('ifChecked',function(){
			$('.imunisasi2').removeAttr('disabled');
			$('.imunisasi2').removeAttr('disabled');
		});
		$("input[id='imunisasi2']").on('ifUnchecked',function(){
			$('.imunisasi2').attr('disabled','disabled').val(null);
			$('.imunisasi2').attr('disabled','disabled').val(null);
		});

		$("input[class='age']").on('ifChecked',function(){
			var val = $(this).val();
			if(val=='1'){
				$('.tgl_lahir').removeAttr('disabled');
				$('.umur').attr('disabled','disabled').val(null);
			}else{
				$('.umur').removeAttr('disabled');
				$('.tgl_lahir').attr('disabled','disabled').val(null);
			}
		});
	})

	var ip = 0;
	function add_petugas(data) {
		var nama_petugas = $('#nama_petugas').val();
		var profesi_petugas = $('#profesi_petugas').val();
		var alamat_petugas = $('#alamat_petugas').val();
		var frekuensi_petugas = $('#frekuensi_petugas').val();
		if(isset(data)){
			nama_petugas = data.nama;
			profesi_petugas = data.profesi;
			alamat_petugas = data.alamat;
			frekuensi_petugas = data.frekuensi;
		}

		$('#petugas_pemeriksa').append('<tr>'+
			'<td><input name="drkp[nama][]" type="text" class="form-control" placeholder="Nama" value="'+nama_petugas+'">'+
			'<td><select name="drkp[profesi][]" class="form-control" id="drkp'+ip+'"><option value=""></option><option value="1">Dokter</option><option value="2">Bidan/Perawat</option><option value="3">Dukun</option><option value="4">Lainnya</option></select></td>'+
			'<td><input name="drkp[alamat][]" type="text" class="form-control" placeholder="Alamat" value="'+alamat_petugas+'">'+
			'<td><input name="drkp[frekuensi][]" type="text" class="form-control" placeholder="Frekuensi" value="'+frekuensi_petugas+'">'+
			'<td width="5%"><a data-toggle="tooltip" title="Hapus Petugas" class="btn-sm btn-danger remove" ><i class="fa fa-remove"></i></a></td>'+
			'</tr>'
			);
		$('#drkp'+ip).val(profesi_petugas).trigger('change');
		$('#nama_petugas').val('');
		$('#profesi_petugas').val('').trigger('change');
		$('#alamat_petugas').val('');
		$('#frekuensi_petugas').val('');
		ip++;
		$('.remove').on('click',function(){
			$(this).parent().parent().remove();
			return false;
		});
		return false;
	}

	function add_persalinan(data) {
		var profesi_persalinan = $('#profesi_persalinan').val();
		var nama_persalinan = $('#nama_persalinan').val();
		var alamat_persalinan = $('#alamat_persalinan').val();
		var tempat_persalinan = $('#tempat_persalinan').val();
		if(isset(data)){
			profesi_persalinan = data.profesi;
			nama_persalinan = data.nama;
			alamat_persalinan = data.alamat;
			tempat_persalinan = data.tempat_persalinan;
		}
		$('#data_persalinan').append('<tr>'+
			'<td><input name="drpr[profesi][]" type="text" class="form-control" placeholder="Profesi" value="'+profesi_persalinan+'">'+
			'<td><input name="drpr[nama][]" type="text" class="form-control" placeholder="Nama" value="'+nama_persalinan+'">'+
			'<td><input name="drpr[alamat][]" type="text" class="form-control" placeholder="Alamat" value="'+alamat_persalinan+'">'+
			'<td><input name="drpr[tempat_persalinan][]" type="text" class="form-control" placeholder="Tempat Persalinan" value="'+tempat_persalinan+'">'+
			'<td width="5%"><a data-toggle="tooltip" title="Hapus data" class="btn-sm btn-danger remove" ><i class="fa fa-remove"></i></a></td>'+
			'</tr>'
			);
		$('#profesi_persalinan').val('');
		$('#nama_persalinan').val('');
		$('#alamat_persalinan').val('');
		$('#tempat_persalinan').val('');
		$('.remove').on('click',function(){
			$(this).parent().parent().remove();
			return false;
		});
		return false;
	}

	function add_tim(data) {
		var nama_pelacak = $('#nama_pelacak').val();
		var jabatan_pelacak = $('#jabatan_pelacak').val();
		if(isset(data)){
			nama_pelacak = data.nama;
			jabatan_pelacak = data.jabatan;
		}
		$('#tim_pelacak').append('<tr>'+
			'<td><input name="dtpel[nama][]" type="text" class="form-control" placeholder="Nama" value="'+nama_pelacak+'">'+
			'<td><input name="dtpel[jabatan][]" type="text" class="form-control" placeholder="Jabatan" value="'+jabatan_pelacak+'">'+
			'<td width="5%"><a data-toggle="tooltip" title="Hapus data" class="btn-sm btn-danger remove" ><i class="fa fa-remove"></i></a></td>'+
			'</tr>'
			);
		$('#nama_pelacak').val('');
		$('#jabatan_pelacak').val('');
		$('.remove').on('click',function(){
			$(this).parent().parent().remove();
			return false;
		});
		return false;
	}

	function generateData() {
		var id = $('#id_trx_case').val();
		if(isset(id)){
			var action = BASE_URL+'case/tetanus/getDetail/'+id;
			$.getJSON(action, function(result){
				var raw = result.response;
				if(isset(raw)){
					$.each(raw, function(k, dt){
						$('#no_epid').val(dt.no_epid);
						$('#name').val(dt.name_pasien);
						$('#id_pasien').val(dt.id_pasien);
						$('#id_family').val(dt.id_family);
						$('#jenis_kelamin').val(dt.jenis_kelamin).trigger('change');
						if(isset(dt.tgl_lahir)){
							$('#tgl_lahir').val(dt.tgl_lahir).removeAttr('disabled');
						}
						if(isset(dt.umur_hari)){
							$('#umur_hari').val(dt.umur_hari).removeAttr('disabled');
						}
						$('#anak_keberapa').val(dt.anak_keberapa);
						$('#nama_ayah').val(dt.nama_ayah);
						$('#umur_th_ayah').val(dt.umur_th_ayah);
						$('#umur_bln_ayah').val(dt.umur_bln_ayah);
						$('#umur_hari_ayah').val(dt.umur_hari_ayah);
						$('#pend_terakhir_ayah').val(dt.pend_terakhir_ayah).trigger('change');
						$('#pekerjaan_ayah').val(dt.pekerjaan_ayah).trigger('change');
						$('#nama_ibu').val(dt.nama_ibu);
						$('#pend_terakhir_ibu').val(dt.pend_terakhir_ibu).trigger('change');
						$('#pekerjaan_ibu').val(dt.pekerjaan_ibu).trigger('change');
						$('#id_provinsi_pasien').val(dt.code_provinsi_pasien).trigger('change');
						$('#id_kabupaten_pasien').html('<option value="'+dt.code_kabupaten_pasien+'">'+dt.name_kabupaten_pasien+'</option>').trigger('change');
						$('#id_kecamatan_pasien').html('<option value="'+dt.code_kecamatan_pasien+'">'+dt.name_kecamatan_pasien+'</option>').trigger('change');
						$('#id_kelurahan_pasien').html('<option value="'+dt.code_kelurahan_pasien+'">'+dt.name_kelurahan_pasien+'</option>').trigger('change');
						$('#alamat').val(dt.alamat);
					});
				}
			});
		}
		var id_pe = $('#id_trx_pe').val();
		if(isset(id_pe)){
			var action = BASE_URL+'case/tetanus/pe/getDetail/'+id_pe;
			$.getJSON(action, function(result){
				var raw = result.response;
				if(isset(raw)){
					$.each(raw, function(k, dt){
						$('#sumber_laporan').val(dt.sumber_laporan).trigger('change');
						if(dt.sumber_laporan_lain){
							$('#sumber_laporan_lain').val(dt.sumber_laporan_lain).removeAttr('disabled');
						}
						$('#tgl_laporan').val(dt.tgl_laporan);
						$('#tgl_investigasi').val(dt.tgl_investigasi);
						$('#nama_yg_diwawancarai').val(dt.nama_yg_diwawancarai);
						$('#hub_dgn_keluarga_bayi').val(dt.hub_dgn_keluarga_bayi);
						$('#bayi_lahir_hidup').val(dt.bayi_lahir_hidup).trigger('change');
						$('#tgl_lahir_bayi').val(dt.tgl_lahir_bayi);
						$('#bayi_meninggal').val(dt.bayi_meninggal).trigger('change');
						if(dt.tgl_meninggal){
							$('#tgl_meninggal').val(dt.tgl_meninggal).removeAttr('change');
						}
						if(dt.umur_meninggal){
							$('#umur_meninggal').val(dt.umur_meninggal).removeAttr('change');
						}
						$('#lahir_bayi_menangis').val(dt.lahir_bayi_menangis).trigger('change');
						$('#tanda_kehidupan_lain').val(dt.tanda_kehidupan_lain).trigger('change');
						$('#bayi_menetek_menghisap_susu_botol_dgn_baik').val(dt.bayi_menetek_menghisap_susu_botol_dgn_baik).trigger('change');
						$('#3_hari_tiba_tiba_mulut_bayi_mencucu_tidak_menetek').val(dt.tiga_hari_tiba_tiba_mulut_bayi_mencucu_tidak_menetek).trigger('change');
						$('#tgl_tidak_mau_menetek').val(dt.tgl_tidak_mau_menetek);
						$('#bayi_mudah_kejang_jika_dsentuh').val(dt.bayi_mudah_kejang_jika_dsentuh).trigger('change');
						$('#tgl_mulai_kejang').val(dt.tgl_mulai_kejang);
						$('#bayi_dirawat').val(dt.bayi_dirawat).trigger('change');
						if(isset(dt.fasilitas_kesehatan)){
							$('#fasilitas_kesehatan').val(dt.fasilitas_kesehatan).removeAttr('disabled');
						}
						if(isset(dt.tgl_dirawat)){
							$('#tgl_dirawat').val(dt.tgl_dirawat).removeAttr('disabled');
						}
						$('#keadaan_bayi').val(dt.keadaan_bayi).trigger('change');
						$('#gpa').val(dt.gpa);
						if(dt.periksa_dokter=='1'){
							$('#periksa_dokter').iCheck('check');
							$('#jml_periksa_dokter').val(dt.jml_periksa_dokter).removeAttr('disabled');
						}
						if(dt.periksa_bidan=='1'){
							$('#periksa_bidan').iCheck('check');
							$('#jml_periksa_bidan').val(dt.jml_periksa_bidan).removeAttr('disabled');
						}
						if(dt.periksa_dukun=='1'){
							$('#periksa_dukun').iCheck('check');
							$('#jml_periksa_dukun').val(dt.jml_periksa_dukun).removeAttr('disabled');
						}
						if(dt.tidak_anc=='1'){
							$('#tidak_anc').iCheck('check');
						}
						if(dt.tidak_jelas=='1'){
							$('#tidak_jelas').iCheck('check');
						}
						if(isset(dt.dtPemeriksaKehamilan)){
							$.each(dt.dtPemeriksaKehamilan, function(k1, d1){
								add_petugas(d1);
							});
						}
						$('#imunisasi_tt_ibu_waktu_hamil').val(dt.imunisasi_tt_ibu_waktu_hamil).trigger('change');
						$('#sumber_informasi_imunisasi_tt').val(dt.sumber_informasi_imunisasi_tt).trigger('change');
						if(dt.imunisasi_ibu_tt_pertama=='1'){
							$('#imunisasi1').iCheck('check');
							$('#umur_imunisasi1').val(dt.umur_kehamilan_ibu_tt_pertama);
							$('#tgl_imunisasi1').val(dt.tgl_imunisasi_ibu_tt_pertama);
						}
						if(dt.imunisasi_ibu_tt_kedua=='1'){
							$('#imunisasi2').iCheck('check');
							$('#umur_imunisasi2').val(dt.umur_kehamilan_ibu_tt_kedua);
							$('#tgl_imunisasi2').val(dt.tgl_imunisasi_ibu_tt_kedua);
						}
						$('#imunisasi_TT').val(dt.imunisasi_tt_kehamilan_sebelumnya).trigger('change');
						$('#suntik1').val(dt.suntikan_tt_pertama_kehamilan_sebelumnya);
						$('#suntik2').val(dt.suntikan_tt_kedua_kehamilan_sebelumnya);
						$('#TT_pengantin').val(dt.suntikan_tt_calon_pengantin).trigger('change');
						$('#suntik_1').val(dt.suntikan_tt_pertama_calon_pengantin);
						$('#suntik_2').val(dt.suntikan_tt_kedua_calon_pengantin);
						$('#status_tt_ibu_saat_kehamilan').val(dt.status_tt_ibu_saat_kehamilan).trigger('change');
						if(isset(dt.dtPenolongPersalinan)){
							$.each(dt.dtPenolongPersalinan, function(k2, d2){
								add_persalinan(d2);
							});
						}
						$('#obat_yg_dibubuhkan_setelah_tali_pusat_dipotong').val(dt.obat_yg_dibubuhkan_setelah_tali_pusat_dipotong).trigger('change');
						$('#sebutkan_obat_bubuhan').val(dt.sebutkan_obat_bubuhan);
						$('#petugas_perawat_tali_pusat').val(dt.petugas_perawat_tali_pusat).trigger('change');
						$('#obat_saat_merawat_tali_pusat').val(dt.obat_saat_merawat_tali_pusat);
						$('#kesimpulan_diagnosis').val(dt.kesimpulan_diagnosis).trigger('change');
						$('#cakupan_imunisasi_tt1').val(dt.cakupan_imunisasi_tt1);
						$('#cakupan_imunisasi_tt2').val(dt.cakupan_imunisasi_tt2);
						$('#cakupan_imunisasi_tt3').val(dt.cakupan_imunisasi_tt3);
						$('#cakupan_imunisasi_tt4').val(dt.cakupan_imunisasi_tt4);
						$('#cakupan_imunisasi_tt5').val(dt.cakupan_imunisasi_tt5);
						$('#cakupan_persalinan_tenaga_kesehatan').val(dt.cakupan_persalinan_tenaga_kesehatan);
						$('#cakupan_kunjungan_neonatus1').val(dt.cakupan_kunjungan_neonatus1);
						$('#cakupan_kunjungan_neonatus2').val(dt.cakupan_kunjungan_neonatus2);
						if(isset(dt.dtPelacak)){
							$.each(dt.dtPelacak, function(k3, d3){
								add_tim(d3);
							});
						}
					})
}
})
}
return false;
}

$(function(){
	generateData();
	$('select').on('change', function() { $(this).valid(); });
	$('.batal').on('click',function(){
		window.location.href = '{!! url('case/tetanus'); !!}';
		return false;
	});
	$('#form').validate({
		rules:{},
		messages:{},
		submitHandler: function(){
			var action = BASE_URL+'case/tetanus/pe/store';
			var data = $('#form').serializeJSON();
			$.ajax({
				method  : "POST",
				url     : action,
				data    : JSON.stringify([data]),
				dataType: "json",
				beforeSend: function(){
					startProcess();
				},
				success: function(data, status){
					if (data.success==true) {
						window.location.href = '{!! url('case/tetanus#tab_4'); !!}';
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
						endProcess();
					}
				}
			});
			return false;
		}
	});
});
</script>
@endsection

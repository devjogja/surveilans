@extends('layouts.base')
@section('content')
@if($dt = $data['response'])
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			<table class="table table-striped">
				<caption>Informasi Pelapor</caption>
				<tbody>
					<tr>
						<td class="title">Sumber Laporan</td>
						<td>{!! $dt['sumber_laporan_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Sumber laporan lain</td>
						<td>{!! $dt['sumber_laporan_lain'] !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Laporan Diterima</td>
						<td>{!! $dt['tgl_laporan'] !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Pelacakan</td>
						<td>{!! $dt['tgl_investigasi'] !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Identitas Bayi</caption>
				<tbody>
					<tr>
						<td class="title">No.Epidemologi</td>
						<td>{!! $dt['no_epid'] !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Bayi</td>
						<td>{!! $dt['name_pasien'] !!}</td>
					</tr>
					<tr>
						<td class="title">Jenis Kelamin</td>
						<td>{!! $dt['jenis_kelamin_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Lahir</td>
						<td>{!! $dt['tgl_lahir'].', ('.$dt['umur_hari'].' hari)' !!}</td>
					</tr>
					<tr>
						<td class="title">Anak ke</td>
						<td>{!! $dt['anak_keberapa'] !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Ayah</td>
						<td>{!! $dt['nama_ayah'] !!}</td>
					</tr>
					<tr>
						<td class="title">Umur Ayah</td>
						<td>{!! $dt['umur_th_ayah'] or 0 !!} Th, {!! $dt['umur_bln_ayah'] or 0 !!} Bln, {!! $dt['umur_hari_ayah'] or 0 !!} Hari</td>
					</tr>
					<tr>
						<td class="title">Pendidikan Terakhir Ayah</td>
						<td>{!! $dt['pend_terakhir_ayah_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Pekerjaan Ayah</td>
						<td>{!! $dt['pekerjaan_ayah_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Ibu</td>
						<td>{!! $dt['nama_ibu'] !!}</td>
					</tr>
					<tr>
						<td class="title">Pendidikan Terakhir Ibu</td>
						<td>{!! $dt['pend_terakhir_ibu_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Pekerjaan Ibu</td>
						<td>{!! $dt['pekerjaan_ibu_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Alamat</td>
						<td>{!! $dt['alamat'].' '.$dt['full_address'] !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Informasi Riwayat Kesakitan/Kematian bayi 3-28 Hari</caption>
				<tbody>
					<tr>
						<td class="title">Nama yang diwawancarai</td>
						<td>{!! $dt['nama_yg_diwawancarai'] !!}</td>
					</tr>
					<tr>
						<td class="title">Hubungan dengan keluarga bayi</td>
						<td>{!! $dt['hub_dgn_keluarga_bayi'] !!}</td>
					</tr>
					<tr>
						<td class="title">Bayi lahir hidup</td>
						<td>{!! $dt['bayi_lahir_hidup_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal lahir bayi</td>
						<td>{!! $dt['tgl_lahir_bayi'] !!}</td>
					</tr>
					<tr>
						<td class="title">Apakah bayi meninggal</td>
						<td>{!! $dt['bayi_meninggal_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Meninggal</td>
						<td>{!! $dt['tgl_meninggal'] !!}</td>
					</tr>
					<tr>
						<td class="title">Umur Saat Meninggal</td>
						<td>{!! $dt['umur_meninggal'] !!}</td>
					</tr>
					<tr>
						<td class="title">Waktu lahir apakah bayi menangis</td>
						<td>{!! $dt['lahir_bayi_menangis_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Apakah terlihat tanda-tanda kehidupan lain dari bayi (misal : sedikit gerakan)</td>
						<td>{!! $dt['tanda_kehidupan_lain_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Setelah lahir, apakah bayi bisa menetek atau menghisap susu botol dengan baik</td>
						<td>{!! $dt['bayi_menetek_menghisap_susu_botol_dgn_baik_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Apakah 3 hari kemudian tiba-tiba mulut bayi mencucu dan tidak bisa menetek</td>
						<td>{!! $dt['3_hari_tiba_tiba_mulut_bayi_mencucu_tidak_menetek_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal tidak mau menetek/minum</td>
						<td>{!! $dt['tgl_tidak_mau_menetek'] !!}</td>
					</tr>
					<tr>
						<td class="title">Apakah bayi mudah kejang jika disentuh/terkena sinar atau bunyi</td>
						<td>{!! $dt['bayi_mudah_kejang_jika_dsentuh_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal mulai kejang (bila ada rangsang sentuh, suara,cahaya)</td>
						<td>{!! $dt['tgl_mulai_kejang'] !!}</td>
					</tr>
					<tr>
						<td class="title">Bayi dirawat</td>
						<td>{!! $dt['bayi_dirawat_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Fasilitas kesehatan</td>
						<td>{!! $dt['fasilitas_kesehatan'] !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal mulai dirawat</td>
						<td>{!! $dt['tgl_dirawat'] !!}</td>
					</tr>
					<tr>
						<td class="title">Keadaan bayi setelah dirawat</td>
						<td>{!! $dt['keadaan_bayi_txt'] !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Riwayat kehamilan</caption>
				<tbody>
					<tr>
						<td class="title">GPA</td>
						<td>{!! $dt['gpa'] !!}</td>
					</tr>
					<tr>
						<td class="title">Berapa kali periksa kehamilan dengan</td>
						<td>
							<table class="table table-striped">
								@if($dt['periksa_dokter']=='1')
								<tr>
									<td>Dokter, Jumlah Periksa : {!!$dt['jml_periksa_dokter'] !!}</td>
								</tr>
								@endif
								@if($dt['periksa_bidan']=='1')
								<tr>
									<td>Bidan/Perawat, Jumlah Periksa : {!!$dt['jml_periksa_bidan'] !!}</td>
								</tr>
								@endif
								@if($dt['periksa_dukun']=='1')
								<tr>
									<td>Dukun, Jumlah Periksa : {!!$dt['jml_periksa_dukun'] !!}</td>
								</tr>
								@endif
								@if($dt['tidak_anc']=='1')
								<tr>
									<td>Tidak ANC</td>
								</tr>
								@endif
								@if($dt['tidak_jelas']=='1')
								<tr>
									<td>Tidak Jelas</td>
								</tr>
								@endif
							</table>
						</td>
					</tr>
					<tr>
						<td class="title">Petugas Pemeriksa</td>
						<td>
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th><center>Nama</center></th>
										<th><center>Profesi</center></th>
										<th><center>Alamat</center></th>
										<th><center>Frekuensi</center></th>
									</tr>
								</thead>
								<tbody>
									@if(!empty($dt['dtPemeriksaKehamilan']))
									@foreach($dt['dtPemeriksaKehamilan'] AS $key=>$val)
									<tr>
										<td>{!! $val->nama; !!}</td>
										<td>{!! $val->profesi_txt; !!}</td>
										<td>{!! $val->alamat; !!}</td>
										<td>{!! $val->frekuensi; !!}</td>
									</tr>
									@endforeach
									@else
									<tr>
										<td colspan="4">Tidak ada data petugas pemeriksa</td>
									</tr>
									@endif
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td class="title">Apakah ibu pernah mendapat imunisasi TT pada waktu hamil bayi ini</td>
						<td>{!! $dt['imunisasi_tt_ibu_waktu_hamil_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Sumber informasi imunisasi TT</td>
						<td>{!! $dt['sumber_informasi_imunisasi_tt_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Berapa kali mendapat imunisasi TT pada saat kehamilan bayi tersebut</td>
						<td>
							<table class="table table-striped">
								@if($dt['imunisasi_ibu_tt_pertama']=='1')
								<tr>
									<td>Pertama Kali</td>
									<td>
										<table class="table table-striped">
											<tr>
												<td class="title">Umur kehamilan saat mendapat imunisasi TT (bulan)</td>
												<td>{!! $dt['umur_kehamilan_ibu_tt_pertama'] !!}</td>
											</tr>
											<tr>
												<td class="title">Tanggal imunisasi TT</td>
												<td>{!! $dt['tgl_imunisasi_ibu_tt_pertama'] !!}</td>
											</tr>
										</table>
									</td>
								</tr>
								@endif
								@if($dt['imunisasi_ibu_tt_kedua']=='1')
								<tr>
									<td>Kedua Kali</td>
									<td>
										<table class="table table-striped">
											<tr>
												<td class="title">Umur kehamilan saat mendapat imunisasi (bulan)</td>
												<td>{!! $dt['umur_kehamilan_ibu_tt_kedua'] !!}</td>
											</tr>
											<tr>
												<td class="title">Tanggal imunisasi TT</td>
												<td>{!! $dt['tgl_imunisasi_ibu_tt_kedua'] !!}</td>
											</tr>
										</table>
									</td>
								</tr>
								@endif
							</table>
						</td>
					</tr>
					<tr>
						<td class="title">Apakah ibu mendapat imunisasi TT pada kehamilan sebelumnya</td>
						<td>{!! $dt['imunisasi_tt_kehamilan_sebelumnya_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Suntikan Pertama (tahun)</td>
						<td>{!! $dt['suntikan_tt_pertama_kehamilan_sebelumnya'] !!}</td>
					</tr>
					<tr>
						<td class="title">Suntikan Kedua (tahun)</td>
						<td>{!! $dt['suntikan_tt_kedua_kehamilan_sebelumnya'] !!}</td>
					</tr>
					<tr>
						<td class="title">Apakah ibu mendapat suntikan TT calon pengantin</td>
						<td>{!! $dt['suntikan_tt_calon_pengantin_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Suntikan Pertama (tahun)</td>
						<td>{!! $dt['suntikan_tt_pertama_calon_pengantin'] !!}</td>
					</tr>
					<tr>
						<td class="title">Suntikan Kedua (tahun)</td>
						<td>{!! $dt['suntikan_tt_kedua_calon_pengantin'] !!}</td>
					</tr>
					<tr>
						<td class="title">Status TT ibu pada saat kehamilan bayi tersebut</td>
						<td>{!! $dt['status_tt_ibu_saat_kehamilan_txt'] !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Riwayat Persalinan</caption>
				<tbody>
					<tr>
						<td class="title">Data Penolong Persalinan</td>
						<td>
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th><center>Profesi</center></th>
										<th><center>Nama</center></th>
										<th><center>Alamat</center></th>
										<th><center>Tempat Persalinan</center></th>
									</tr>
								</thead>
								<tbody>
									@if(!empty($dt['dtPenolongPersalinan']))
									@foreach($dt['dtPenolongPersalinan'] AS $key=>$val)
									<tr>
										<td>{!! $val->profesi !!}</td>
										<td>{!! $val->nama !!}</td>
										<td>{!! $val->alamat !!}</td>
										<td>{!! $val->tempat_persalinan !!}</td>
									</tr>
									@endforeach
									@else
									<tr>
										<td colspan="4">Tidak ada data penolong persalinan</td>
									</tr>
									@endif
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td class="title">Obat yang dibubuhkan setelah tali pusat dipotong</td>
						<td>{!! $dt['obat_yg_dibubuhkan_setelah_tali_pusat_dipotong_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Jika jawaban ramuan tradisional atau lainnya, sebutkan</td>
						<td>{!! $dt['sebutkan_obat_bubuhan'] !!}</td>
					</tr>
					<tr>
						<td class="title">Yang melakukan perawatan tali pusat sejak lahir sampai tali pusat puput</td>
						<td>{!! $dt['petugas_perawat_tali_pusat_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Obat atau ramuan apa saja yang dibubuhkan selama merawat tali pusat</td>
						<td>{!! $dt['obat_saat_merawat_tali_pusat'] !!}</td>
					</tr>
					<tr>
						<td class="title">Kesimpulan diagnosis</td>
						<td>{!! $dt['kesimpulan_diagnosis_txt'] !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Data Cakupan</caption>
				<tbody>
					<tr>
						<td class="title">Cakupan imunisasi TT di desa kasus TN</td>
						<td>
							<table class="table table-striped">
								@for($i=1;$i<=5;$i++)
								<tr>
									<td class="title">TT{!! $i; !!}(%)</td>
									<td>{!! $dt['cakupan_imunisasi_tt'.$i]; !!} %</td>
								</tr>
								@endfor
							</table>
						</td>
					</tr>
					<tr>
						<td class="title">Cakupan persalinan tenaga kesehatan (%)</td>
						<td>{!! $dt['cakupan_persalinan_tenaga_kesehatan'] !!}</td>
					</tr>
					<tr>
						<td class="title">Cakupan kunjungan neonatus</td>
						<td>
							<table class="table table-striped">
								@for($i=1;$i<=2;$i++)
								<tr>
									<td class="title">KN{!! $i; !!}(%)</td>
									<td>{!! $dt['cakupan_kunjungan_neonatus'.$i]; !!} %</td>
								</tr>
								@endfor
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-bordered table-striped">
				<caption>Tim Pelacak</caption>
				<thead>
					<tr>
						<th><center>Nama</center></th>
						<th><center>Jabatan</center></th>
					</tr>
				</thead>
				<tbody>
					@if(!empty($dt['dtPelacak']))
					@foreach($dt['dtPelacak'] AS $key=>$val)
					<tr>
						<td>{!! $val->nama !!}</td>
						<td>{!! $val->jabatan !!}</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="2">Tidak ada data tim pelacak</td>
					</tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>
@endif
<div class="col-sm-12">
	<div class="footer">
		<a href="{!!URL::to('case/tetanus#tab_4')!!}" class="btn btn-primary">Kembali</a>
	</div>
</div>
@endsection
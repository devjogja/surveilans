<table border="1">
	<thead>
		<tr>
			<th rowspan="3">No</th>
			<th rowspan="3">Tgl Input</th>
			<th rowspan="3">Tgl Update</th>
			<th rowspan="3">No.RM</th>
			<th rowspan="3">No.Epid PD3I v04.01</th>
			<th rowspan="3">No.Epid PD3I v04.00</th>
			<th rowspan="3">No.Epid Lama</th>
			<th rowspan="3">Nama Faskes</th>
			<th rowspan="3">Kecamatan</th>
			<th rowspan="3">Kabupaten/Kota</th>
			<th rowspan="3">Provinsi</th>
			<th colspan="14">Identitas Pasien</th>
			<th colspan="10">Data surveilans tetanus neonatorum</th>
			<th rowspan="3">Klasifikasi final</th>
			<th colspan="2">Titik Koordinat Pasien</th>
			<th colspan="4">Informasi Pelapor</th>
			<th colspan="13">Identitas Bayi</th>
			<th colspan="18">Informasi Riwayat Kesakitan/Kematian bayi 3-28 Hari</th>
			<th colspan="28">Riwayat kehamilan</th>
			<th colspan="9">Riwayat Persalinan</th>
			<th colspan="8">Data Cakupan</th>
			<th colspan="2">Tim Pelacak</th>
		</tr>
		<tr>
			<th rowspan="2">Nama Pasien</th>
			<th rowspan="2">NIK</th>
			<th rowspan="2">Agama</th>
			<th rowspan="2">Nama Ortu</th>
			<th rowspan="2">Jenis Kelamin</th>
			<th rowspan="2">Tanggal Lahir</th>
			<th colspan="3">Umur</th>
			<th colspan="5">Alamat</th>
			<th rowspan="2">Tanggal mulai sakit</th>
			<th rowspan="2">Tanggal laporan diterima</th>
			<th rowspan="2">Tanggal pelacakan</th>
			<th rowspan="2">Antenatal care (ANC)</th>
			<th rowspan="2">Status Imunisasi Ibu</th>
			<th rowspan="2">Penolong persalinan</th>
			<th rowspan="2">Alat Pemotong tali pusat</th>
			<th rowspan="2">Perawatan tali pusat</th>
			<th rowspan="2">Rawat rumah sakit</th>
			<th rowspan="2">Keadaan akhir</th>
			<th rowspan="2">Longitude</th>
			<th rowspan="2">Latitude</th>
			<th rowspan="2">Sumber Laporan</th>
			<th rowspan="2">Jika lain-lain sebutkan</th>
			<th rowspan="2">Tanggal Laporan Diterima</th>
			<th rowspan="2">Tanggal Pelacakan</th>
			<th rowspan="2">Anak ke</th>
			<th rowspan="2">Nama Ayah</th>
			<th colspan="3">Umur Ayah</th>
			<th rowspan="2">Pendidikan Terakhir Ayah</th>
			<th rowspan="2">Pekerjaan Ayah</th>
			<th rowspan="2">Nama Ibu</th>
			<th colspan="3">Umur Ibu</th>
			<th rowspan="2">Pendidikan Terakhir Ibu</th>
			<th rowspan="2">Pekerjaan Ibu</th>
			<th rowspan="2">Nama yang diwawancarai</th>
			<th rowspan="2">Hubungan dengan keluarga bayi</th>
			<th rowspan="2">Bayi lahir hidup</th>
			<th rowspan="2">Tanggal lahir bayi</th>
			<th rowspan="2">Apakah bayi meninggal</th>
			<th rowspan="2">Tanggal Meninggal</th>
			<th rowspan="2">Umur Saat Meninggal</th>
			<th rowspan="2">Waktu lahir apakah bayi menangis</th>
			<th rowspan="2">Apakah terlihat tanda-tanda kehidupan lain dari bayi (misal : sedikit gerakan)</th>
			<th rowspan="2">Setelah lahir, apakah bayi bisa menetek atau menghisap susu botol dengan baik</th>
			<th rowspan="2">Apakah 3 hari kemudian tiba-tiba mulut bayi mencucu dan tidak bisa menetek</th>
			<th rowspan="2">Tanggal tidak mau menetek/minum</th>
			<th rowspan="2">Apakah bayi mudah kejang jika disentuh/terkena sinar atau bunyi</th>
			<th rowspan="2">Tanggal mulai kejang (bila ada rangsang sentuh, suara,cahaya)</th>
			<th rowspan="2">Bayi dirawat</th>
			<th rowspan="2">Fasilitas kesehatan</th>
			<th rowspan="2">Tanggal mulai dirawat</th>
			<th rowspan="2">Keadaan bayi setelah dirawat</th>
			<th rowspan="2">GPA</th>
			<th colspan="8">Berapa kali periksa kehamilan dengan</th>
			<th colspan="4">Petugas Pemeriksa</th>
			<th rowspan="2">Apakah ibu pernah mendapat imunisasi TT pada waktu hamil bayi ini</th>
			<th rowspan="2">Sumber informasi imunisasi TT</th>
			<th colspan="6">Berapa kali mendapat imunisasi TT pada saat kehamilan bayi tersebut</th>
			<th rowspan="2">Apakah ibu mendapat imunisasi TT pada kehamilan sebelumnya</th>
			<th rowspan="2">Suntikan Pertama (tahun)</th>
			<th rowspan="2">Suntikan Kedua (tahun)</th>
			<th rowspan="2">Apakah ibu mendapat suntikan TT calon pengantin</th>
			<th rowspan="2">Suntikan Pertama (tahun)</th>
			<th rowspan="2">Suntikan Kedua (tahun)</th>
			<th rowspan="2">Status TT ibu pada saat kehamilan bayi tersebut</th>
			<th colspan="4">Data Penolong Persalinan</th>
			<th rowspan="2">Obat yang dibubuhkan setelah tali pusat dipotong</th>
			<th rowspan="2">Jika jawaban ramuan tradisional atau lainnya, sebutkan</th>
			<th rowspan="2">Yang melakukan perawatan tali pusat sejak lahir sampai tali pusat puput</th>
			<th rowspan="2">Obat atau ramuan apa saja yang dibubuhkan selama merawat tali pusat</th>
			<th rowspan="2">Kesimpulan diagnosis</th>
			<th colspan="5">Cakupan imunisasi TT di desa kasus TN</th>
			<th rowspan="2">Cakupan persalinan tenaga kesehatan (%)</th>
			<th colspan="2">Cakupan kunjungan neonatus</th>
			<th rowspan="2">Nama</th>
			<th rowspan="2">Jabatan</th>
		</tr>
		<tr>
			<th>Tahun</th>
			<th>Bulan</th>
			<th>Hari</th>
			<th>Alamat</th>
			<th>Kelurahan</th>
			<th>Kecamatan</th>
			<th>Kabupaten</th>
			<th>Provinsi</th>
			<th>Tahun</th>
			<th>Bulan</th>
			<th>Hari</th>
			<th>Tahun</th>
			<th>Bulan</th>
			<th>Hari</th>
			<th>Dokter</th>
			<th>Jumlah Periksa</th>
			<th>Bidan/Perawat</th>
			<th>Jumlah Periksa</th>
			<th>Dukun</th>
			<th>Jumlah Periksa</th>
			<th>Tidak ANC</th>
			<th>Tidak jelas</th>
			<th>Nama</th>
			<th>Profesi</th>
			<th>Alamat</th>
			<th>Frekuensi</th>
			<th>Pertama Kali</th>
			<th>Umur kehamilan saat mendapat imunisasi TT (bulan)</th>
			<th>Tanggal imunisasi TT</th>
			<th>Kedua Kali</th>
			<th>Umur kehamilan saat mendapat imunisasi (bulan)</th>
			<th>Tanggal imunisasi TT</th>
			<th>Profesi</th>
			<th>Nama</th>
			<th>Alamat</th>
			<th>Tempat Persalinan</th>
			<th>TT1(%)</th>
			<th>TT2(%)</th>
			<th>TT3(%)</th>
			<th>TT4(%)</th>
			<th>TT5(%)</th>
			<th>KN1(%)</th>
			<th>KN2(%)</th>
		</tr>
	</thead>
	<tbody>
		@if($dd)
		<?php
		$no = 1;
		$yt = [
			null=>'',
			1=>'Ya',
			2=>'Tidak',
			3=>'Tidak Tahu',
		];
		?>
			@foreach($dd AS $key=>$val)
				<?php $val = (array) $val;?>
				<tr>
					<td>{!! $no++; !!}</td>
					<td>{!! $val['tgl_input'] !!}</td>
					<td>{!! $val['tgl_update'] !!}</td>
					<td>{!! $val['no_rm'] !!}</td>
					<td>{!! $val['no_epid'] !!}</td>
					<td>{!! $val['_no_epid'] !!}</td>
					<td>{!! $val['no_epid_lama'] !!}</td>
					<td>{!! $val['name_faskes'] !!}</td>
					<td>{!! $val['name_kecamatan_faskes'] !!}</td>
					<td>{!! $val['name_kabupaten_faskes'] !!}</td>
					<td>{!! $val['name_provinsi_faskes'] !!}</td>
					<td>{!! $val['name_pasien'] !!}</td>
					<td>{!! $val['nik'] !!}</td>
					<td>{!! $val['agama'] !!}</td>
					<td>{!! $val['nama_ortu'] !!}</td>
					<td>{!! $val['jenis_kelamin_txt'] !!}</td>
					<td>{!! $val['tgl_lahir'] !!}</td>
					<td>{!! $val['umur_thn'] !!}</td>
					<td>{!! $val['umur_bln'] !!}</td>
					<td>{!! $val['umur_hari'] !!}</td>
					<td>{!! $val['alamat'] !!}</td>
					<td>{!! $val['kelurahan_pasien'] !!}</td>
					<td>{!! $val['kecamatan_pasien'] !!}</td>
					<td>{!! $val['kabupaten_pasien'] !!}</td>
					<td>{!! $val['provinsi_pasien'] !!}</td>
					<td>{!! $val['tgl_mulai_sakit'] !!}</td>
					<td>{!! $val['tgl_laporan_diterima'] !!}</td>
					<td>{!! $val['tgl_pelacakan'] !!}</td>
					<td>{!! $val['antenatal_care_txt'] !!}</td>
					<td>{!! $val['status_imunisasi_ibu_txt'] !!}</td>
					<td>{!! $val['penolong_persalinan_txt'] !!}</td>
					<td>{!! $val['alat_pemotong_tali_pusat_txt'] !!}</td>
					<td>{!! $val['perawatan_tali_pusat_txt'] !!}</td>
					<td>{!! $val['rawat_rumah_sakit_txt'] !!}</td>
					<td>{!! $val['keadaan_akhir_txt'] !!}</td>
					<td>{!! $val['klasifikasi_final_txt'] !!}</td>
					<td>{!! $val['longitude'] !!}</td>
					<td>{!! $val['latitude'] !!}</td>
					<td>{!! $val['sumber_laporan_txt'] !!}</td>
					<td>{!! $val['sumber_laporan_lain'] !!}</td>
					<td>{!! $val['tgl_laporan'] !!}</td>
					<td>{!! $val['tgl_investigasi'] !!}</td>
					<td>{!! $val['anak_keberapa'] !!}</td>
					<td>{!! $val['nama_ayah'] !!}</td>
					<td>{!! $val['umur_th_ayah'] !!}</td>
					<td>{!! $val['umur_bln_ayah'] !!}</td>
					<td>{!! $val['umur_hari_ayah'] !!}</td>
					<td>{!! $val['pend_terakhir_ayah_txt'] !!}</td>
					<td>{!! $val['pekerjaan_ayah_txt'] !!}</td>
					<td>{!! $val['nama_ibu'] !!}</td>
					<td>{!! $val['umur_th_ibu'] !!}</td>
					<td>{!! $val['umur_bln_ibu'] !!}</td>
					<td>{!! $val['umur_hari_ibu'] !!}</td>
					<td>{!! $val['pend_terakhir_ibu_txt'] !!}</td>
					<td>{!! $val['pekerjaan_ibu_txt'] !!}</td>
					<td>{!! $val['nama_yg_diwawancarai'] !!}</td>
					<td>{!! $val['hub_dgn_keluarga_bayi'] !!}</td>
					<td>{!! $yt[$val['bayi_lahir_hidup']] !!}</td>
					<td>{!! $val['tgl_lahir_bayi'] !!}</td>
					<td>{!! $yt[$val['bayi_meninggal']] !!}</td>
					<td>{!! $val['tgl_meninggal'] !!}</td>
					<td>{!! $val['umur_meninggal'] !!}</td>
					<td>{!! $yt[$val['lahir_bayi_menangis']] !!}</td>
					<td>{!! $yt[$val['tanda_kehidupan_lain']] !!}</td>
					<td>{!! $yt[$val['bayi_menetek_menghisap_susu_botol_dgn_baik']] !!}</td>
					<td>{!! $yt[$val['3_hari_tiba_tiba_mulut_bayi_mencucu_tidak_menetek']] !!}</td>
					<td>{!! $val['tgl_tidak_mau_menetek'] !!}</td>
					<td>{!! $yt[$val['bayi_mudah_kejang_jika_dsentuh']] !!}</td>
					<td>{!! $val['tgl_mulai_kejang'] !!}</td>
					<td>{!! $yt[$val['bayi_dirawat']] !!}</td>
					<td>{!! $val['fasilitas_kesehatan'] !!}</td>
					<td>{!! $val['tgl_dirawat'] !!}</td>
					<td>{!! $val['keadaan_bayi_txt'] !!}</td>
					<td>{!! $val['gpa'] !!}</td>
					<td>{!! $yt[$val['periksa_dokter']] !!}</td>
					<td>{!! $val['jml_periksa_dokter'] !!}</td>
					<td>{!! $yt[$val['periksa_bidan']] !!}</td>
					<td>{!! $val['jml_periksa_bidan'] !!}</td>
					<td>{!! $yt[$val['periksa_dukun']] !!}</td>
					<td>{!! $val['jml_periksa_dukun'] !!}</td>
					<td>{!! $yt[$val['tidak_anc']] !!}</td>
					<td>{!! $yt[$val['tidak_jelas']] !!}</td>
					<?php
					$profesi = [
						null=>'',
						1=>'Dokter',
						2=>'Bidan/Perawat',
						3=>'Dukun',
						4=>'Lainnya',
					];
					$a1 = explode('|',$val['petugas_pemeriksa']);
					$nama1 = $profesi2 = $alamat3 = $frekuensi4 = '';
					foreach ($a1 as $a3=>$a4) {
						$a5 = explode('_', $a4);
						$nama1 .= empty($a5[0])?'<br/>':$a5[0].'<br/>';
						$profesi2 .= empty($a5[1])?'<br/>':$profesi[$a5[1]].'<br/>';
						$alamat3 .= empty($a5[2])?'<br/>':$a5[2].'<br/>';
						$frekuensi4 .= empty($a5[3])?'<br/>':$a5[3].'<br/>';
					}
					?>
					<td>{!! $nama1; !!}</td>
					<td>{!! $profesi2; !!}</td>
					<td>{!! $alamat3; !!}</td>
					<td>{!! $frekuensi4; !!}</td>
					<td>{!! $yt[$val['imunisasi_tt_ibu_waktu_hamil']] !!}</td>
					<td>{!! $val['sumber_informasi_imunisasi_tt_txt'] !!}</td>
					<td>{!! $yt[$val['imunisasi_ibu_tt_pertama']] !!}</td>
					<td>{!! $val['umur_kehamilan_ibu_tt_pertama'] !!}</td>
					<td>{!! $val['tgl_imunisasi_ibu_tt_pertama'] !!}</td>
					<td>{!! $yt[$val['imunisasi_ibu_tt_kedua']] !!}</td>
					<td>{!! $val['umur_kehamilan_ibu_tt_kedua'] !!}</td>
					<td>{!! $val['tgl_imunisasi_ibu_tt_kedua'] !!}</td>
					<td>{!! $yt[$val['imunisasi_tt_kehamilan_sebelumnya']] !!}</td>
					<td>{!! $val['suntikan_tt_pertama_kehamilan_sebelumnya'] !!}</td>
					<td>{!! $val['suntikan_tt_kedua_kehamilan_sebelumnya'] !!}</td>
					<td>{!! $yt[$val['suntikan_tt_calon_pengantin']] !!}</td>
					<td>{!! $val['suntikan_tt_pertama_calon_pengantin'] !!}</td>
					<td>{!! $val['suntikan_tt_kedua_calon_pengantin'] !!}</td>
					<td>{!! $val['status_tt_ibu_saat_kehamilan_txt'] !!}</td>
					<?php
					$c1 = explode('|',$val['penolong_persalinan']);
					$nama2 = $profesi2 = $alamat2 = $tempat_persalinan2 = '';
					foreach ($c1 as $c4) {
						$c5 = explode('_', $c4);
						$nama2 .= empty($c5[0])?'<br/>':$c5[0].'<br/>';
						$profesi2 .= empty($c5[1])?'<br/>':$c5[1].'<br/>';
						$alamat2 .= empty($c5[2])?'<br/>':$c5[2].'<br/>';
						$tempat_persalinan2 .= empty($c5[3])?'<br/>':$c5[3].'<br/>';
					}
					?>
					<td>{!! $nama2; !!}</td>
					<td>{!! $profesi2; !!}</td>
					<td>{!! $alamat2; !!}</td>
					<td>{!! $tempat_persalinan2; !!}</td>
					<td>{!! $val['obat_yg_dibubuhkan_setelah_tali_pusat_dipotong_txt'] !!}</td>
					<td>{!! $val['sebutkan_obat_bubuhan'] !!}</td>
					<td>{!! $val['petugas_perawat_tali_pusat_txt'] !!}</td>
					<td>{!! $val['obat_saat_merawat_tali_pusat'] !!}</td>
					<td>{!! $val['kesimpulan_diagnosis_txt'] !!}</td>
					<td>{!! $val['cakupan_imunisasi_tt1'] !!}</td>
					<td>{!! $val['cakupan_imunisasi_tt2'] !!}</td>
					<td>{!! $val['cakupan_imunisasi_tt3'] !!}</td>
					<td>{!! $val['cakupan_imunisasi_tt4'] !!}</td>
					<td>{!! $val['cakupan_imunisasi_tt5'] !!}</td>
					<td>{!! $val['cakupan_persalinan_tenaga_kesehatan'] !!}</td>
					<td>{!! $val['cakupan_kunjungan_neonatus1'] !!}</td>
					<td>{!! $val['cakupan_kunjungan_neonatus2'] !!}</td>
					<?php
					$b1 = explode('|',$val['pelacak']);
					$nama3 = $jabatan3 = '';
					foreach ($b1 as $b4) {
						$b5 = explode('_', $b4);
						$nama3 .= empty($b5[0])?'<br/>':$b5[0].'<br/>';
						$jabatan3 .= empty($b5[1])?'<br/>':$b5[1].'<br/>';
					}
					?>
					<td>{!! $nama3; !!}</td>
					<td>{!! $jabatan3; !!}</td>
				</tr>
			@endforeach
		@endif
	</tbody>
</table>
@extends('layouts.base')
@section('content')
@include('case.include.filter_analisa_webview')
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Tetanus Berdasar Jenis Kelamin</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(1);']) !!}
		<div class="pull-right col-md-2">
				{!! Form::select(null, array(null=>'Suspek TN','1'=>'Tetanus Neonatorum (Konfirm TN)'), 'suspek_tn', ['class' => 'form-control', 'id'=>'hasilLabJenisKelamin','onchange'=>'hasilLabJenisKelaminTetanus();']) !!}
		</div>
	</div>
	@include('case.chart.chart_gender')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Tetanus Berdasar Waktu</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(2);']) !!}
		<div class="pull-right col-md-2">
			{!! Form::select(null, array(null=>'Suspek TN','1'=>'Tetanus Neonatorum (Konfirm TN)'), 'suspek_tn', ['class' => 'form-control', 'id'=>'hasilLabWaktu','onchange'=>'hasilLabWaktuTetanus();']) !!}
		</div>
	</div>
	@include('case.chart.chart_waktu')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Tetanus Berdasar Umur</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(3);']) !!}
		<div class="pull-right col-md-2">
			{!! Form::select(null, array(null=>'Suspek TN','1'=>'Tetanus Neonatorum (Konfirm TN)'), 'suspek_tn', ['class' => 'form-control', 'id'=>'hasilLabUmur','onchange'=>'hasilLabUmurTetanus();']) !!}
		</div>
	</div>
	@include('case.chart.chart_umur')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Tetanus Berdasar Status Imunisasi</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(4);']) !!}
		<div class="pull-right col-md-2">
			{!! Form::select(null, array(null=>'Suspek TN','1'=>'Tetanus Neonatorum (Konfirm TN)'), 'suspek_tn', ['class' => 'form-control', 'id'=>'hasilLabStatImun','onchange'=>'hasilLabStatImunTetanus();']) !!}
		</div>
	</div>
	@include('case.chart.chart_stat_imun')

</div>
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Tetanus Berdasar Klasifikasi Final</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(5);']) !!}

	</div>
	@include('case.chart.chart_final')
</div>

<script type="text/javascript">
var dataquery;
function unduh(val) {
	var tmp=[];
	var data=[];
		switch (val) {
			case 1:
			var rawdata = dataquery.jenis_kelamin;
			var title 	= 'Jenis Kelamin Tetanus';
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Jenis kelamin'] 	= rawdata[i][0];
					tmp['Jumlah'] 				= rawdata[i][1];
					data.push(tmp);
					tmp=[];
				}
				break;
			case 2:
			var rawdata = dataquery.waktu.data[0].data;
			var title 	= 'Waktu '+dataquery.waktu.data[0].name+' Tetanus';
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Bulan'] 	= (rawdata[i]['name'])?rawdata[i]['name']:rawdata[i][0];
					tmp['Jumlah'] = (rawdata[i]['y'])?rawdata[i]['y']:rawdata[i][1];
					data.push(tmp);
					tmp=[];
				}
				break;
			case 3:
			var rawdata = dataquery.umur[0].data;
			var title 	= dataquery.umur[0].name+' Tetanus';
			var category = [@foreach (Helper::getCategoryUmur('Tetanus') as $item)
			    '{{ $item }}',
			@endforeach ];
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Umur'] 	= category[i];
					tmp['Jumlah'] = rawdata[i];
					data.push(tmp);
					tmp=[];
				}
				break;
			case 4:
			var rawdata = dataquery.stat_imun;
			var title 	= 'Status Imunisasi Tetanus';
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Status Imunisasi'] = rawdata[i][0];
					tmp['Jumlah'] 					= rawdata[i][1];
					data.push(tmp);
					tmp=[];
				}
				break;
			case 5:
			var rawdata = dataquery.klasifikasi_final;
			var title 	= 'Klasifikasi Final Tetanus';
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Klasifikasi Final'] = rawdata[i][0];
					tmp['Jumlah'] 					 = rawdata[i][1];
					data.push(tmp);
					tmp=[];
				}
				break;
		}
    // if(data == '')
    //     return;

    JSONToCSVConvertor(data, title, true);
};

$('#id_provinsi_filter_analisa').on('change',function(){
	var val = $(this).val();
	if(val!=''){
		$('#id_kabupaten_filter_analisa').removeAttr('disabled');
	}else{
		$('#id_kabupaten_filter_analisa').attr('disabled','disabled');
	}
	return false;
});
$('#id_kabupaten_filter_analisa').on('change',function(){
	var val = $(this).val();
	if(val!=''){
		$('#id_kecamatan_filter_analisa').removeAttr('disabled');
	}else{
		$('#id_kecamatan_filter_analisa').attr('disabled','disabled');
	}
	return false;
});
$('#id_kecamatan_filter_analisa').on('change',function(){
	var val = $(this).val();
	if(val!='' && $('#filter_type').val()==2){
		$('#puskesmas_filter_analisa').removeAttr('disabled');
	}else{
		$('#puskesmas_filter_analisa').attr('disabled','disabled');
	}
	return false;
});

var category = [@foreach (Helper::getCategoryUmur('tn') as $item)
    '{{ $item }}',
@endforeach ];
var title = 'Suspek Tetanus';
var range = 'Nasional'+'<br>Tahun 2010-'+new Date().getFullYear();
var senddata = {'filter':{'code_provinsi_pasien':''}};
$.ajax({
	method  : "POST",
	url     : BASE_URL+'api/analisa/tetanus',
	data    : JSON.stringify(senddata),
	// dataType: "json",
	beforeSend: function(){
		// startProcess();
	},
	success: function(data){
		if (data.success==true) {
			endProcess();
			var dt=data.response;
			dataquery = dt;
			graphJenisKelamin(title,null,range,dt.jenis_kelamin);
			graphStatImun(title,null,range,dt.stat_imun);
			graphFinal(title,range,dt.klasifikasi_final);
			graphUmur(title,null,range,category,dt.umur);
			graphWaktu(title,null,range,dt.waktu);
		}else{
			messageAlert('warning', 'Peringatan', 'Data gagal salah');
		}
	}
});

		var action = BASE_URL+'api/analisa/tetanus';
		$('#filter_analisa').validate({
			rules:{
				'filter_type':'required',
			},
			messages:{
				'filter_type':'Jenis data wajib diisi',
			},
		  submitHandler: function(){
				var data = $('#filter_analisa').serializeJSON();
				range = $('#id_provinsi_filter_analisa option:selected').text();
		    console.log(JSON.stringify(data));
		    $.ajax({
		      method  : "POST",
		      url     : action,
		      data    : JSON.stringify(data),
		      // dataType: "json",
		      beforeSend: function(){
		        // startProcess();
		      },
		      success: function(data){
		        if (data.success==true) {
		          endProcess();
							console.log(data.response);
							// console.log(JSON.stringify(data.response));
							var dt=data.response;
							dataquery = dt;
							range = range+'<br>'+dt.waktu[0].name;
							graphJenisKelamin(title,null,range,dt.jenis_kelamin);
							graphStatImun(title,null,range,dt.stat_imun);
							graphFinal(title,range,dt.klasifikasi_final);
							graphUmur(title,null,range,category,dt.umur);
							graphWaktu(title,null,range,dt.waktu);
		        }else{
		          messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
		          endProcess();
		        }
		      }
		    });
		    return false;
		  }
		});

function hasilLabJenisKelaminTetanus() {
	if($('#filter_type').val()!=''){
			jenis_kasus = $('#hasilLabJenisKelamin').val();
			title = 'Suspek Tetanus';
			if(jenis_kasus!=''){
				title = $('#hasilLabJenisKelamin option:selected').text();;
			}
			var data = $('#filter_analisa').serializeJSON();
			data['filter']['klasifikasi_final']=jenis_kasus;
			range = $('#id_provinsi_filter_analisa option:selected').text();
				if(range=='Pilih Provinsi'){range='Nasional';}
			if($('#from_year_analisa').val() && $('#to_year_analisa').val()){range = range+'<br>'+'Tahun'+$('#from_year_analisa').val()+'-'+$('#to_year_analisa').val();
				}
			else{
				range=range+'<br>Tahun 2010-'+new Date().getFullYear();
			}
			console.log(JSON.stringify(data));
			$.ajax({
				method  : "POST",
				url     : action,
				data    : JSON.stringify(data),
				// dataType: "json",
				beforeSend: function(){
					startProcess();
				},
				success: function(data){
					if (data.success==true) {
						var dt=data.response;
						dataquery = dt;
						console.log(data.response);
						graphJenisKelamin(title,null,range,dt.jenis_kelamin);
						endProcess();
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
						endProcess();
					}
				}
			});
	}else{
		messageAlert('warning', 'Peringatan', 'Pilihan filter harus diisi');
	}
}

function hasilLabWaktuTetanus() {
	if($('#filter_type').val()!=''){
			jenis_kasus = $('#hasilLabWaktu').val();
			title = 'Suspek Tetanus';
			if(jenis_kasus!=''){
				title = $('#hasilLabWaktu option:selected').text();;
			}
			var data = $('#filter_analisa').serializeJSON();
			data['filter']['klasifikasi_final']=jenis_kasus;
			range = $('#id_provinsi_filter_analisa option:selected').text();
				if(range=='Pilih Provinsi'){range='Nasional';}
			if($('#from_year_analisa').val() && $('#to_year_analisa').val()){range = range+'<br>'+'Tahun'+$('#from_year_analisa').val()+'-'+$('#to_year_analisa').val();
				}
			else{
				range=range+'<br>Tahun 2010-'+new Date().getFullYear();
			}
			console.log(JSON.stringify(data));
			$.ajax({
				method  : "POST",
				url     : action,
				data    : JSON.stringify(data),
				// dataType: "json",
				beforeSend: function(){
					startProcess();
				},
				success: function(data){
					if (data.success==true) {
						var dt=data.response;
						dataquery = dt;
						console.log(data.response);
						graphWaktu(title,null,range,dt.waktu);
						endProcess();
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
						endProcess();
					}
				}
			});
	}else{
		messageAlert('warning', 'Peringatan', 'Pilihan filter harus diisi');
	}
}

function hasilLabUmurTetanus() {
		var hasil_lab = $('#hasilLabUmur').val();
		var title = 'Suspek Tetanus';
		if(hasil_lab=='tetanus'){
			title = 'Tetanus Neonatorum';
		}
		graphUmur(title,null,faskes,category,dataUmur);
}

function hasilLabStatImunTetanus() {
		var hasil_lab = $('#hasilLabStatImun').val();
		var title = 'Suspek Tetanus';
		if(hasil_lab=='tetanus'){
			title = 'Tetanus Neonatorum';
		}
		graphStatImun(title,null,faskes);
}

function hasilLabFinalTetanus() {
		var hasil_lab = $('#hasilLabFinal').val();
		var title = 'Suspek Tetanus';
		if(hasil_lab=='tetanus'){
			title = 'Tetanus Neonatorum';
		}
		graphFinal(title,null,faskes);
}
</script>
@endsection

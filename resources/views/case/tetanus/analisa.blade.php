@include('case.include.filter_analisa')
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Tetanus Neonatorum Berdasar Jenis Kelamin </h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','onclick'=>'graph1("1");']) !!}
		<div class="pull-right col-md-2" style="margin: 0px -10px 0px -7px;">
			{!! Form::select(null, array(null=>'--Pilih--', '1'=>'Suspek TN','2'=>'Tetanus Neonatorum (Konfirm TN)'), 'tetanus', ['class' => 'form-control', 'id'=>'jenisDataJenisKelamin','onchange'=>'graph1();']) !!}
		</div>
	</div>
	@include('case.chart.chart_gender')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Tetanus Neonatorum Berdasar Waktu</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','onclick'=>'graph2("1");']) !!}
		<div class="pull-right col-md-2" style="margin: 0px -10px 0px -7px;">
			{!! Form::select(null, array(null=>'--Pilih--', '1'=>'Suspek TN','2'=>'Tetanus Neonatorum (Konfirm TN)'), 'tetanus', ['class' => 'form-control', 'id'=>'jenisDataWaktu','onchange'=>'graph2();']) !!}
		</div>
	</div>
	@include('case.chart.chart_waktu')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Tetanus Neonatorum Berdasar Umur</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','onclick'=>'graph3("1");']) !!}
		<div class="pull-right col-md-2" style="margin: 0px -10px 0px -7px;">
			{!! Form::select(null, array(null=>'--Pilih--', '1'=>'Suspek TN','2'=>'Tetanus Neonatorum (Konfirm TN)'), 'tetanus', ['class' => 'form-control', 'id'=>'jenisDataUmur','onchange'=>'graph3();']) !!}
		</div>
	</div>
	@include('case.chart.chart_umur')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Tetanus Neonatorum Berdasar Status Imunisasi</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','onclick'=>'graph4("1");']) !!}
		<div class="pull-right col-md-2" style="margin: 0px -10px 0px -7px;">
			{!! Form::select(null, array(null=>'--Pilih--', '1'=>'Suspek TN','2'=>'Tetanus Neonatorum (Konfirm TN)'), 'tetanus', ['class' => 'form-control', 'id'=>'jenisDataStatImun','onchange'=>'graph4();']) !!}
		</div>
	</div>
	@include('case.chart.chart_stat_imun')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Tetanus Neonatorum Berdasar Klasifikasi Final</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','onclick'=>'graph5("1");']) !!}
	</div>
	@include('case.chart.chart_final')
</div>

{!! HTML::script('asset/tableExport.js') !!}

<script type="text/javascript">
	var category = [@foreach (Helper::getCategoryUmur('tetanus') as $item)
	'{{ $item }}',
	@endforeach ];
	$(function(){
		graph1();
		graph2();
		graph3();
		graph4();
		graph5();
		$('#filter_analisa').validate({
			rules:{
				'filter_type':'required',
			},
			messages:{
				'filter_type':'Jenis data wajib diisi',
			},
			submitHandler: function(){
				graph1();
				graph2();
				graph3();
				graph4();
				graph5();
			}
		});
	});

	function titleFilter(){
		var wil = 'Nasional';
		var fw = $('#filter_type_daftar_kasus_analisa').val();
		var prov = $('#id_provinsi_filter_analisa option:selected');
		var kab = $('#id_kabupaten_filter_analisa option:selected');
		var kec = $('#id_kecamatan_filter_analisa option:selected');
		var rs = $('#rs_filter_analisa option:selected');
		var pkm = $('#puskesmas_filter_analisa option:selected');
		if(isset(fw) && isset(prov.val())){
			wil = '';
			if(isset(prov.val())){
				wil += prov.text();
				if(fw=='2'){
					wil = 'DINAS KESEHATAN PROVINSI '+prov.text();
				}
			}
			if(isset(kab.val())){
				wil += kab.text()+', ';
				if(fw=='2'){
					wil = 'DINAS KESEHATAN KABUPATEN '+kab.text()+', PROVINSI '+prov.text();
				}
			}
			if(isset(kec.val())){ wil += kec.text()+', '; }
			if(isset(rs.val())){ wil = rs.text(); }
			if(isset(pkm.val())){ wil = 'PUSKESMAS '+pkm.text(); }
		}

		var time = 'Tahun 2010-'+new Date().getFullYear();
		var ft = $('#range_time_analisa').val();
		var fd = $('#from_day_analisa option:selected');
		var fm = $('#from_month_analisa option:selected');
		var fy = $('#from_year_analisa option:selected');
		var td = $('#to_day_analisa option:selected');
		var tm = $('#to_month_analisa option:selected');
		var ty = $('#to_year_analisa option:selected');
		if(isset(ft)){
			time = '';
			if(isset(fd.val())){ time += fd.text()+' '};
			if(isset(fm.val())){ time += fm.text()+' '};
			if(isset(fy.val())){ time += fy.text()};
			time += ' sd ';
			if(isset(td.val())){ time += td.text()+' '};
			if(isset(tm.val())){ time += tm.text()+' '};
			if(isset(ty.val())){ time += ty.text()};
		}
		return {"wil":wil,"time":time};
	}

	function graph1($unduh){
		var jenisDataJenisKelamin = $('#jenisDataJenisKelamin option:selected');
		var title1 = jenisDataJenisKelamin.text();
		var title2 = '';
		var gTitle = titleFilter();
		var range = gTitle.wil+'<br>'+gTitle.time;
		var fd = {};
		fd['fdata'] = JSON.stringify({"jenis_data":jenisDataJenisKelamin.val()});
		fd['dt'] = JSON.stringify($('#filter_analisa').serializeJSON());
		$.ajax({
			method  : "POST",
			url     : BASE_URL+'api/analisa/graphJenisKelamin/tetanus',
			data    : fd,
			success: function(data){
				if (data.success==true) {
					var dt=data.response;
					if(isset($unduh)){
						var title = 'Kasus '+title1+' Berdasar Jenis Kelamin <br>'+gTitle.wil+'<br>'+gTitle.time;
						var head = ['Jenis Kelamin','Jumlah'];
						var item = dt.export;
						exportXls(title, head, item);
					}else{
						graphJenisKelamin(title1,title2,range,dt.jenis_kelamin);
					}
				}else{
					messageAlert('warning', 'Peringatan', 'Data gagal salah');
				}
			}
		});
	}

	function graph2($unduh){
		var jenisDataWaktu = $('#jenisDataWaktu option:selected');
		var title1 = jenisDataWaktu.text();
		var title2 = '';
		var gTitle = titleFilter();
		var range = gTitle.wil+'<br>'+gTitle.time;
		var fd = {};
		fd['fdata'] = JSON.stringify({"jenis_data":jenisDataWaktu.val()});
		fd['dt'] = JSON.stringify($('#filter_analisa').serializeJSON());
		$.ajax({
			method  : "POST",
			url     : BASE_URL+'api/analisa/graphWaktu/tetanus',
			data    : fd,
			success: function(data){
				if (data.success==true) {
					var dt=data.response;
					if(isset($unduh)){
						var title = 'Kasus '+title1+' Berdasar Waktu <br>'+gTitle.wil+'<br>'+gTitle.time;
						var head = ['Bulan','Jumlah'];
						var item = dt.export;
						exportXls(title, head, item);
					}else{
						graphWaktu(title1,title2,range,dt.waktu);
					}
				}else{
					messageAlert('warning', 'Peringatan', 'Data gagal salah');
				}
			}
		});
	}

	function graph3($unduh){
		var jenisDataUmur = $('#jenisDataUmur option:selected');
		var title1 = jenisDataUmur.text();
		var title2 = '';
		var gTitle = titleFilter();
		var range = gTitle.wil+'<br>'+gTitle.time;
		var fd = {};
		fd['fdata'] = JSON.stringify({"jenis_data":jenisDataUmur.val()});
		fd['dt'] = JSON.stringify($('#filter_analisa').serializeJSON());
		$.ajax({
			method  : "POST",
			url     : BASE_URL+'api/analisa/graphUmur/tetanus',
			data    : fd,
			success: function(data){
				if (data.success==true) {
					var dt=data.response;
					if(isset($unduh)){
						var title = 'Kasus '+title1+' Berdasar Umur <br>'+gTitle.wil+'<br>'+gTitle.time;
						var head = ['Umur','Jumlah'];
						var item = dt.export;
						exportXls(title, head, item);
					}else{
						graphUmur(title1,title2,range,category,dt.umur);
					}
				}else{
					messageAlert('warning', 'Peringatan', 'Data gagal salah');
				}
			}
		});
	}

	function graph4($unduh){
		var jenisDataStatImun = $('#jenisDataStatImun option:selected');
		var title1 = jenisDataStatImun.text();
		var title2 = '';
		var gTitle = titleFilter();
		var range = gTitle.wil+'<br>'+gTitle.time;
		var fd = {};
		fd['fdata'] = JSON.stringify({"jenis_data":jenisDataStatImun.val()});
		fd['dt'] = JSON.stringify($('#filter_analisa').serializeJSON());
		$.ajax({
			method  : "POST",
			url     : BASE_URL+'api/analisa/graphStatusImunisasi/tetanus',
			data    : fd,
			success: function(data){
				if (data.success==true) {
					var dt=data.response;
					if(isset($unduh)){
						var title = 'Kasus '+title1+' Berdasar Status Imunisasi <br>'+gTitle.wil+'<br>'+gTitle.time;
						var head = ['Status Imunisasi','Jumlah'];
						var item = dt.export;
						exportXls(title, head, item);
					}else{
						graphStatImun(title1,title2,range,dt.stat_imun);
					}
				}else{
					messageAlert('warning', 'Peringatan', 'Data gagal salah');
				}
			}
		});
	}

	function graph5($unduh){
		var title1 = '';
		var title2 = '';
		var gTitle = titleFilter();
		var range = gTitle.wil+'<br>'+gTitle.time;
		var fd = {};
		fd['fdata'] = JSON.stringify({"jenis_data":'',"jenis_kasus":''});
		fd['dt'] = JSON.stringify($('#filter_analisa').serializeJSON());
		$.ajax({
			method  : "POST",
			url     : BASE_URL+'api/analisa/graphKlasifikasiFinal/tetanus',
			data    : fd,
			success: function(data){
				if (data.success==true) {
					var dt=data.response;
					if(isset($unduh)){
						var title = 'Kasus '+title1+' Berdasar Status Klasifikasi Final <br>'+gTitle.wil+'<br>'+gTitle.time;
						var head = ['Klasifikasi Final','Jumlah'];
						var item = dt.export;
						exportXls(title, head, item);
					}else{
						graphFinal(title1,range,dt.klasifikasi_final);
					}
				}else{
					messageAlert('warning', 'Peringatan', 'Data gagal salah');
				}
			}
		});
	}
</script>
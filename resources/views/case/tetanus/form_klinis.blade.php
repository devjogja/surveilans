<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Data surveilans tetanus neonatorum</h3>
	</div>
	<div class="form-horizontal">
		<div class="box-body">
			<div class="form-group">
				{!! Form::label(null, 'Tanggal mulai sakit', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					<div class="input-group">
					{!! Form::text('dk[tgl_mulai_sakit]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal mulai sakit','id'=>'tgl_mulai_sakit','onchange'=>'getEpid();getAge();']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Tanggal laporan diterima', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dk[tgl_laporan_diterima]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal laporan diterima','id'=>'tgl_laporan_diterima']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Tanggal pelacakan', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dk[tgl_pelacakan]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal pelacakan','id'=>'tgl_pelacakan']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Antenatal care (ANC)', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[antenatal_care]', array(null=>'--Pilih--','1'=>'Dokter','2'=>'Bidan/Perawat','3'=>'Dukun','4'=>'Tidak ANC','5'=>'Tidak Jelas'), null, ['class' => 'form-control', 'id'=>'antenatal_care']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Status Imunisasi Ibu', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[status_imunisasi_ibu]', array(null=>'--Pilih--','1'=>'TT2+','2'=>'TT1','3'=>'Tidak Imunisasi','4'=>'Tidak Jelas'), null, ['class' => 'form-control', 'id'=>'status_imunisasi_ibu']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Penolong persalinan', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[penolong_persalinan]', array(null=>'--Pilih--','1'=>'Dokter','2'=>'Bidan','3'=>'Dukun','4'=>'Tidak jelas'), null, ['class' => 'form-control', 'id'=>'penolong_persalinan']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Alat Pemotong tali pusat', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[alat_pemotong_tali_pusat]', array(null=>'--Pilih--','1'=>'Gunting','2'=>'Bambu','3'=>'Silet','4'=>'Pisau','5'=>'Lain-lain','6'=>'Tidak tahu'), null, ['class' => 'form-control', 'id'=>'alat_pemotong_tali_pusat']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Perawatan tali pusat', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[perawatan_tali_pusat]', array(null=>'--Pilih--','1'=>'Alc/Iod','2'=>'Ramuan','3'=>'Lain-lain','4'=>'Tidak Jelas'), null, ['class' => 'form-control', 'id'=>'perawatan_tali_pusat']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Rawat rumah sakit', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[rawat_rumah_sakit]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak Jelas'), null, ['class' => 'form-control', 'id'=>'rawat_rumah_sakit']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Keadaan akhir', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[keadaan_akhir]', array(null=>'--Pilih--','1'=>'Hidup','2'=>'Meninggal'), null, ['class' => 'form-control', 'id'=>'keadaan_akhir']) !!}
				</div>
			</div>
		</div>
	</div>
</div>

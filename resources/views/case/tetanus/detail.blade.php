@extends('layouts.base')
@section('content')
@if($dt = $data['response'])
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			<table class="table table-striped">
				<caption>Identitas Pasien</caption>
				<tbody>
					<tr>
						<td class="title">No Epidemologi</td>
						<td>{!! $dt->no_epid or '-' !!} { {!! $dt->_no_epid or '-' !!} }</td>
					</tr>
					<tr>
						<td class="title">No Epidemologi Lama</td>
						<td>{!! $dt->no_epid_lama or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">No RM</td>
						<td>{!! $dt->no_rm or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Penderita</td>
						<td>{!! $dt->name_pasien or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">NIK</td>
						<td>{!! $dt->nik or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama orang tua</td>
						<td>{!! $dt->nama_ortu or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Jenis kelamin</td>
						<td>{!! $dt->jenis_kelamin_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Lahir(Umur)</td>
						<td><?php
							$tgl_lahir = ($dt->tgl_lahir)?$dt->tgl_lahir:'-';
							$umur_thn = ($dt->umur_thn)?$dt->umur_thn.' Th':'';
							$umur_bln = ($dt->umur_bln)?$dt->umur_bln.' Bln':'';
							$umur_hari = ($dt->umur_hari)?$dt->umur_hari.' Hari':'';
						?>
						{!! $tgl_lahir !!} ({!!$umur_thn!!} {!!$umur_bln !!} {!!$umur_hari!!})</td>
					</tr>
					<tr>
						<td class="title">Alamat</td>
						<td>{!! $dt->alamat or '' !!} {!! $dt->full_address or '' !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Data surveilans tetanus neonatorum</caption>
				<tbody>
					<tr>
						<td class="title">Tanggal mulai sakit</td>
						<td>{!! $dt->tgl_mulai_sakit or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal laporan diterima</td>
						<td>{!! $dt->tgl_laporan_diterima or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal pelacakan</td>
						<td>{!! $dt->tgl_pelacakan or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Antenatal care (ANC)</td>
						<td>{!! $dt->antenatal_care_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Status Imunisasi Ibu</td>
						<td>{!! $dt->status_imunisasi_ibu_txt !!}</td>
					</tr>
					<tr>
						<td class="title">Penolong persalinan</td>
						<td>{!! $dt->penolong_persalinan_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Alat Pemotong tali pusat</td>
						<td>{!! $dt->alat_pemotong_tali_pusat_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Perawatan tali pusat</td>
						<td>{!! $dt->perawatan_tali_pusat_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Rawat rumah sakit</td>
						<td>{!! $dt->rawat_rumah_sakit_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Keadaan akhir</td>
						<td>{!! $dt->keadaan_akhir_txt or '-' !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Klasifikasi Final</caption>
				<tbody>
					<tr>
						<td class="title">Hasil klasifikasi final</td>
						<td>{!! $dt->klasifikasi_final_txt or '-' !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endif
<div class="col-sm-12">
	<div class="footer">
		<a href="{!!URL::to('case/tetanus')!!}" class="btn btn-primary">Kembali</a>
	</div>
</div>
@endsection
@extends('layouts.base')
@section('content')
@include('case.include.filter_analisa_webview')
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Difteri Berdasar Jenis Kelamin</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(1);']) !!}
		<div class="pull-right col-md-2">
			{!! Form::select(null, array(null=>'Suspek Difteri','[1,2]'=>'Difteri (Konfirm dan Probable)','2'=>'Difteri Konfirm','1'=>'Difteri Probable'), 'difteri', ['class' => 'form-control', 'id'=>'hasilLabJenisKelaminDifteri','onchange'=>'hasilLabJenisKelaminDifteri();']) !!}
		</div>
	</div>
	@include('case.chart.chart_gender')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Difteri Berdasar Waktu</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(2);']) !!}
		<div class="pull-right col-md-2">
			{!! Form::select(null, array(null=>'Suspek Difteri','[1,2]'=>'Difteri (Konfirm dan Probable)','2'=>'Difteri Konfirm','1'=>'Difteri Probable'), 'difteri', ['class' => 'form-control', 'id'=>'hasilLabWaktuDifteri','onchange'=>'hasilLabWaktuDifteri();']) !!}
		</div>
	</div>
	@include('case.chart.chart_waktu')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Difteri Berdasar Umur</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(3);']) !!}
		<div class="pull-right col-md-2">
			{!! Form::select(null, array(null=>'Suspek Difteri','[1,2]'=>'Difteri (Konfirm dan Probable)','2'=>'Difteri Konfirm','1'=>'Difteri Probable'), 'difteri', ['class' => 'form-control', 'id'=>'hasilLabUmurDifteri','onchange'=>'hasilLabUmurDifteri();']) !!}
		</div>
	</div>
	@include('case.chart.chart_umur')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Difteri Berdasar Status Imunisasi</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(5);']) !!}
		<div class="pull-right col-md-2">
			{!! Form::select(null, array(null=>'Suspek Difteri','[1,2]'=>'Difteri (Konfirm dan Probable)','2'=>'Difteri Konfirm','1'=>'Difteri Probable'), 'difteri', ['class' => 'form-control', 'id'=>'hasilLabStatImunDifteri','onchange'=>'hasilLabStatImunDifteri();']) !!}
		</div>
	</div>
	@include('case.chart.chart_stat_imun')

</div>
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Difteri Berdasar Klasifikasi Final</h3>
		{!! Form::button('Unduh Data &nbsp;<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','onclick'=>'unduh(1);']) !!}

	</div>
	@include('case.chart.chart_final')
</div>

<script type="text/javascript">
var dataquery;
function unduh(val) {
	var tmp=[];
	var data=[];
		switch (val) {
			case 1:
			var rawdata = dataquery.jenis_kelamin;
			var title 	= 'Jenis Kelamin Difteri';
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Jenis kelamin'] 	= rawdata[i][0];
					tmp['Jumlah'] 				= rawdata[i][1];
					data.push(tmp);
					tmp=[];
				}
				break;
			case 2:
			var rawdata = dataquery.waktu.data[0].data;
			var title 	= 'Waktu '+dataquery.waktu.data[0].name+' Difteri';
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Bulan'] 	= (rawdata[i]['name'])?rawdata[i]['name']:rawdata[i][0];
					tmp['Jumlah'] = (rawdata[i]['y'])?rawdata[i]['y']:rawdata[i][1];
					data.push(tmp);
					console.log(data);
					tmp=[];
				}
				break;
			case 3:
			var rawdata = dataquery.umur[0].data;
			var title 	= dataquery.umur[0].name+' Difteri';
			var category = [@foreach (Helper::getCategoryUmur('Difteri') as $item)
			    '{{ $item }}',
			@endforeach ];
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Umur'] 	= category[i];
					tmp['Jumlah'] = rawdata[i];
					data.push(tmp);
					tmp=[];
				}
				break;
			case 4:
			var rawdata = dataquery.stat_imun;
			var title 	= 'Status Imunisasi Difteri';
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Status Imunisasi'] = rawdata[i][0];
					tmp['Jumlah'] 					= rawdata[i][1];
					data.push(tmp);
					tmp=[];
				}
				break;
			case 5:
			var rawdata = dataquery.klasifikasi_final;
			var title 	= 'Klasifikasi Final Difteri';
				for (var i = 0; i < rawdata.length; i++) {
					tmp['Klasifikasi Final'] = rawdata[i][0];
					tmp['Jumlah'] 					 = rawdata[i][1];
					data.push(tmp);
					tmp=[];
				}
				break;
		}
    // if(data == '')
    //     return;

    JSONToCSVConvertor(data, title, true);
};

$('#id_provinsi_filter_analisa').on('change',function(){
	var val = $(this).val();
	if(val!=''){
		$('#id_kabupaten_filter_analisa').removeAttr('disabled');
	}else{
		$('#id_kabupaten_filter_analisa').attr('disabled','disabled');
	}
	return false;
});
$('#id_kabupaten_filter_analisa').on('change',function(){
	var val = $(this).val();
	if(val!=''){
		$('#id_kecamatan_filter_analisa').removeAttr('disabled');
	}else{
		$('#id_kecamatan_filter_analisa').attr('disabled','disabled');
	}
	return false;
});
$('#id_kecamatan_filter_analisa').on('change',function(){
	var val = $(this).val();
	if(val!='' && $('#filter_type').val()==2){
		$('#puskesmas_filter_analisa').removeAttr('disabled');
	}else{
		$('#puskesmas_filter_analisa').attr('disabled','disabled');
	}
	return false;
});

var category = [@foreach (Helper::getCategoryUmur('difteri') as $item)
    '{{ $item }}',
@endforeach ];
var title = 'Suspek Difteri';
var range = 'Nasional'+'<br>Tahun 2010-'+new Date().getFullYear();
var senddata = {'filter':{'code_provinsi_pasien':''}};
$.ajax({
	method  : "POST",
	url     : BASE_URL+'api/analisa/difteri',
	data    : JSON.stringify(senddata),
	// dataType: "json",
	beforeSend: function(){
		// startProcess();
	},
	success: function(data){
		if (data.success==true) {
			endProcess();
			var dt=data.response;
			dataquery = dt;
			console.log(dt);
			graphJenisKelamin(title,null,range,dt.jenis_kelamin);
			graphStatImun(title,null,range,dt.stat_imun);
			graphFinal(title,range,dt.klasifikasi_final);
			graphUmur(title,null,range,category,dt.umur);
			graphWaktu(title,null,range,dt.waktu);
		}else{
			messageAlert('warning', 'Peringatan', 'Data gagal salah');
		}
	}
});

var action = BASE_URL+'api/analisa/difteri';
$('#filter_analisa').validate({
	rules:{
		'filter_type':'required',
	},
	messages:{
		'filter_type':'Jenis data wajib diisi',
	},
  submitHandler: function(){
		var data = $('#filter_analisa').serializeJSON();
		range = $('#id_provinsi_filter_analisa option:selected').text();
    console.log(JSON.stringify(data));
    $.ajax({
      method  : "POST",
      url     : action,
      data    : JSON.stringify(data),
      // dataType: "json",
      beforeSend: function(){
        // startProcess();
      },
      success: function(data){
        if (data.success==true) {
          endProcess();
					console.log(data.response);
					// console.log(JSON.stringify(data.response));
					var dt=data.response;
dataquery = dt;
					range = range+'<br>'+dt.waktu[0].name;
					graphJenisKelamin(title,null,range,dt.jenis_kelamin);
					graphStatImun(title,null,range,dt.stat_imun);
					graphFinal(title,null,dt.klasifikasi_final);
					graphUmur(title,null,range,category,dt.umur);
					graphWaktu(title,null,range,dt.waktu);
        }else{
          messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
          endProcess();
        }
      }
    });
    return false;
  }
});

function hasilLabJenisKelaminDifteri() {
	if($('#filter_type').val()!=''){
			jenis_kasus = $('#hasilLabJenisKelaminDifteri').val();
			title = 'Suspek Difteri';
			if(jenis_kasus!=''){
				title = $('#hasilLabJenisKelaminDifteri option:selected').text();;
			}
			var data = $('#filter_analisa').serializeJSON();
			data['filter']['klasifikasi_final']=jenis_kasus;
			range = $('#id_provinsi_filter_analisa option:selected').text();
				if(range=='Pilih Provinsi'){range='Nasional';}
			if($('#from_year_analisa').val() && $('#to_year_analisa').val()){range = range+'<br>'+'Tahun'+$('#from_year_analisa').val()+'-'+$('#to_year_analisa').val();
				}
			else{
				range=range+'<br>Tahun 2010-'+new Date().getFullYear();
			}
			console.log(JSON.stringify(data));
			$.ajax({
				method  : "POST",
				url     : action,
				data    : JSON.stringify(data),
				// dataType: "json",
				beforeSend: function(){
					startProcess();
				},
				success: function(data){
					if (data.success==true) {
						var dt=data.response;
dataquery = dt;
						console.log(data.response);
						graphJenisKelamin(title,null,range,dt.jenis_kelamin);
						endProcess();
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
						endProcess();
					}
				}
			});
	}else{
		messageAlert('warning', 'Peringatan', 'Pilihan filter harus diisi');
	}
}

function hasilLabWaktuDifteri() {
	if($('#filter_type').val()!=''){
			jenis_kasus = $('#hasilLabWaktuDifteri').val();
			title = 'Suspek Difteri';
			if(jenis_kasus!=''){
				title = $('#hasilLabWaktuDifteri option:selected').text();;
			}
			var data = $('#filter_analisa').serializeJSON();
			data['filter']['klasifikasi_final']=jenis_kasus;
			range = $('#id_provinsi_filter_analisa option:selected').text();
				if(range=='Pilih Provinsi'){range='Nasional';}
			if($('#from_year_analisa').val() && $('#to_year_analisa').val()){range = range+'<br>'+'Tahun'+$('#from_year_analisa').val()+'-'+$('#to_year_analisa').val();
				}
			else{
				range=range+'<br>Tahun 2010-'+new Date().getFullYear();
			}
			console.log(JSON.stringify(data));
			$.ajax({
				method  : "POST",
				url     : action,
				data    : JSON.stringify(data),
				// dataType: "json",
				beforeSend: function(){
					startProcess();
				},
				success: function(data){
					if (data.success==true) {
						var dt=data.response;
dataquery = dt;
						console.log(data.response);
						graphWaktu(title,null,range,dt.waktu);
						endProcess();
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
						endProcess();
					}
				}
			});
	}else{
		messageAlert('warning', 'Peringatan', 'Pilihan filter harus diisi');
	}
}

function hasilLabUmurDifteri() {
	if($('#filter_type').val()!=''){
			jenis_kasus = $('#hasilLabUmurDifteri').val();
			title = 'Suspek Difteri';
			if(jenis_kasus!=''){
				title = $('#hasilLabUmurDifteri option:selected').text();;
			}
			var data = $('#filter_analisa').serializeJSON();
			data['filter']['klasifikasi_final']=jenis_kasus;
			range = $('#id_provinsi_filter_analisa option:selected').text();
				if(range=='Pilih Provinsi'){range='Nasional';}
			if($('#from_year_analisa').val() && $('#to_year_analisa').val()){range = range+'<br>'+'Tahun'+$('#from_year_analisa').val()+'-'+$('#to_year_analisa').val();
				}
			else{
				range=range+'<br>Tahun 2010-'+new Date().getFullYear();
			}
			console.log(JSON.stringify(data));
			$.ajax({
				method  : "POST",
				url     : action,
				data    : JSON.stringify(data),
				// dataType: "json",
				beforeSend: function(){
					startProcess();
				},
				success: function(data){
					if (data.success==true) {
						var dt=data.response;
dataquery = dt;
						console.log(data.response);
						graphUmur(title,null,range,category,dt.umur);
						endProcess();
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
						endProcess();
					}
				}
			});
	}else{
		messageAlert('warning', 'Peringatan', 'Pilihan filter harus diisi');
	}
}

function hasilLabStatImunDifteri() {
	if($('#filter_type').val()!=''){
			jenis_kasus = $('#hasilLabStatImunDifteri').val();
			title = 'Suspek Difteri';
			if(jenis_kasus!=''){
				title = $('#hasilLabStatImunDifteri option:selected').text();;
			}
			var data = $('#filter_analisa').serializeJSON();
			data['filter']['klasifikasi_final']=jenis_kasus;
			range = $('#id_provinsi_filter_analisa option:selected').text();
				if(range=='Pilih Provinsi'){range='Nasional';}
			if($('#from_year_analisa').val() && $('#to_year_analisa').val()){range = range+'<br>'+'Tahun'+$('#from_year_analisa').val()+'-'+$('#to_year_analisa').val();
				}
			else{
				range=range+'<br>Tahun 2010-'+new Date().getFullYear();
			}
			console.log(JSON.stringify(data));
			$.ajax({
				method  : "POST",
				url     : action,
				data    : JSON.stringify(data),
				// dataType: "json",
				beforeSend: function(){
					startProcess();
				},
				success: function(data){
					if (data.success==true) {
						var dt=data.response;
dataquery = dt;
						console.log(data.response);
						graphStatImun(title,null,range,dt.stat_imun);
						endProcess();
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
						endProcess();
					}
				}
			});
	}else{
		messageAlert('warning', 'Peringatan', 'Pilihan filter harus diisi');
	}
}

function hasilLabFinalDifteri() {
		var hasil_lab = $('#hasilLabFinalDifteri').val();
		var title = 'Suspek Difteri';
		if(hasil_lab=='difteri_all'){
			title = 'Difteri (Konfirm dan Probable)';
		}else if(hasil_lab=='konfirm'){
			title = 'Difteri Konfirm';
		}else if(hasil_lab=='probable'){
			title = 'Difteri Probable';
		}
		graphFinal(title,null,range);
}
</script>

@endsection

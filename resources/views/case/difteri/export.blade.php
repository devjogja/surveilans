<table border="1">
	<thead>
		<tr>
			<th rowspan="3">No</th>
			<th rowspan="3">Tgl Input</th>
			<th rowspan="3">Tgl Update</th>
			<th rowspan="3">No.RM</th>
			<th rowspan="3">No.Epid PD3I v04.01</th>
			<th rowspan="3">No.Epid PD3I v04.00</th>
			<th rowspan="3">No.Epid Lama</th>
			<th rowspan="3">Nama Faskes</th>
			<th rowspan="3">Kecamatan</th>
			<th rowspan="3">Kabupaten/Kota</th>
			<th rowspan="3">Provinsi</th>
			<th colspan="14">Identitas Pasien</th>
			<th colspan="9">Data surveilans Difteri</th>
			<th colspan="5">Data spesimen dan hasil laboratorium</th>
			<th rowspan="3">Klasifikasi final</th>
			<th colspan="2">Titik Koordinat Pasien</th>
			<th colspan="6">Identitas pelapor</th>
			<th colspan="13">Identitas penderita</th>
			<th colspan="16">Riwayat sakit</th>
			<th colspan="12">Riwayat pengobatan</th>
			<th colspan="6">Riwayat kontak</th>
			<th colspan="9">Kontak Kasus</th>
		</tr>
		<tr>
			<th rowspan="2">Nama Pasien</th>
			<th rowspan="2">NIK</th>
			<th rowspan="2">Agama</th>
			<th rowspan="2">Nama Ortu</th>
			<th rowspan="2">Jenis Kelamin</th>
			<th rowspan="2">Tanggal Lahir</th>
			<th colspan="3">Umur</th>
			<th colspan="5">Alamat</th>
			<th rowspan="2">Tanggal mulai sakit (demam)</th>
			<th rowspan="2">Imunisasi DPT sebelum sakit berapa kali</th>
			<th rowspan="2">Tanggal imunisasi difteri terakhir</th>
			<th rowspan="2">Tanggal pelacakan</th>
			<th rowspan="2">Gejala lain</th>
			<th colspan="3">Kontak</th>
			<th rowspan="2">Keadaan akhir</th>
			<th rowspan="2">Jenis Spesimen</th>
			<th rowspan="2">Tanggal ambil spesimen</th>
			<th rowspan="2">Jenis Pemeriksaan</th>
			<th rowspan="2">Kode Spesimen</th>
			<th rowspan="2">Hasil</th>
			<th rowspan="2">Longitude</th>
			<th rowspan="2">Latitude</th>
			<th rowspan="2">Nama</th>
			<th rowspan="2">Nama Kantor</th>
			<th rowspan="2">Jabatan</th>
			<th rowspan="2">Provinsi</th>
			<th rowspan="2">Kabupaten</th>
			<th rowspan="2">Tanggal Laporan</th>
			<th rowspan="2">No.Telp</th>
			<th rowspan="2">Tempat tinggal saat ini</th>
			<th rowspan="2">Tempat tinggal sesuai kartu identitas/alamat asal</th>
			<th rowspan="2">Pekerjaan</th>
			<th rowspan="2">Alamat tempat kerja</th>
			<th rowspan="2">Faskes tempat periksa/puskesmas</th>
			<th colspan="7">Orang tua/saudara dekat yang dapat dihubungi</th>
			<th rowspan="2">Tanggal mulai sakit (demam)</th>
			<th rowspan="2">Keluhan utama yang mendorong untuk berobat?</th>
			<th colspan="11">Gejala dan tanda sakit</th>
			<th rowspan="2">Status imunisasi difteri</th>
			<th colspan="2">Jika sudah pernah imunisasi DPT, sebutkan frekuensi dan tahun imunisasi</th>
			<th colspan="8">Penderita berobat ke</th>
			<th rowspan="2">Antibiotik</th>
			<th rowspan="2">Obat lain</th>
			<th rowspan="2">ADS (Anti Difteri Serum)</th>
			<th rowspan="2">Kondisi kasus saat ini</th>
			<th rowspan="2">Dalam 2 minggu terakhir sebelum sakit apakah penderita bepergian</th>
			<th rowspan="2">Jika pernah, kemana?</th>
			<th rowspan="2">Dalam 2 minggu terakhir sebelum sakit apakah penderita pernah berkunjung ke rumah teman/saudara yang sakit/meninggal dengan gejala yang sama</th>
			<th rowspan="2">Jika pernah, kemana?</th>
			<th rowspan="2">Dalam 2 minggu terakhir, apakah pernah menerima tamu dengan sakit dengan gejala yang sama</th>
			<th rowspan="2">Jika pernah, dari mana?</th>
			<th rowspan="2">Nama</th>
			<th rowspan="2">Umur</th>
			<th rowspan="2">Hubungan dengan kasus</th>
			<th rowspan="2">Status Imunisasi</th>
			<th colspan="2">Tenggorokan</th>
			<th colspan="2">Hidung</th>
			<th rowspan="2">Profilaksis</th>
		</tr>
		<tr>
			<th>Tahun</th>
			<th>Bulan</th>
			<th>Hari</th>
			<th>Alamat</th>
			<th>Kelurahan</th>
			<th>Kecamatan</th>
			<th>Kabupaten</th>
			<th>Provinsi</th>
			<th>Jumlah</th>
			<th>Diambil spec (swab hidung)</th>
			<th>Positif</th>
			<th>Nama</th>
			<th>Alamat</th>
			<th>Kelurahan</th>
			<th>Kecamatan</th>
			<th>Kabupaten</th>
			<th>Provinsi</th>
			<th>Telepon</th>
			<th>Demam</th>
			<th>Tanggal Sakit</th>
			<th>Sakit Kerongkongan</th>
			<th>Tanggal Sakit</th>
			<th>Leher bengkak</th>
			<th>Tanggal Sakit</th>
			<th>Sesak nafas</th>
			<th>Tanggal Sakit</th>
			<th>Pseudemomembran</th>
			<th>Tanggal Sakit</th>
			<th>Gejala lain</th>
			<th>Berapa kali imunisasi</th>
			<th>Tahun Imunisasi</th>
			<th>Rumah sakit</th>
			<th>Dirawat</th>
			<th>Trakeostomi</th>
			<th>Puskesmas</th>
			<th>Dirawat</th>
			<th>Dokter praktek swasta</th>
			<th>Perawat/mantri/bidan</th>
			<th>Tidak Berobat</th>
			<th>Tanggal ambil spesimen</th>
			<th>Hasil lab</th>
			<th>Tanggal ambil spesimen</th>
			<th>Hasil lab</th>
		</tr>
	</thead>
	<tbody>
		@if($dd)
		<?php $no = 1;?>
		@foreach($dd AS $key=>$val)
			<?php $val = (array) $val;?>
			<tr>
					<td>{!! $no++; !!}</td>
					<td>{!! $val['tgl_input'] !!}</td>
					<td>{!! $val['tgl_update'] !!}</td>
					<td>{!! $val['no_rm'] !!}</td>
					<td>{!! $val['no_epid'] !!}</td>
					<td>{!! $val['_no_epid'] !!}</td>
					<td>{!! $val['no_epid_lama'] !!}</td>
					<td>{!! $val['name_faskes'] !!}</td>
					<td>{!! $val['name_kecamatan_faskes'] !!}</td>
					<td>{!! $val['name_kabupaten_faskes'] !!}</td>
					<td>{!! $val['name_provinsi_faskes'] !!}</td>
					<td>{!! $val['name_pasien'] !!}</td>
					<td>{!! $val['nik'] !!}</td>
					<td>{!! $val['agama'] !!}</td>
					<td>{!! $val['nama_ortu'] !!}</td>
					<td>{!! $val['jenis_kelamin_txt'] !!}</td>
					<td>{!! $val['tgl_lahir'] !!}</td>
					<td>{!! $val['umur_thn'] !!}</td>
					<td>{!! $val['umur_bln'] !!}</td>
					<td>{!! $val['umur_hari'] !!}</td>
					<td>{!! $val['alamat'] !!}</td>
					<td>{!! $val['kelurahan_pasien'] !!}</td>
					<td>{!! $val['kecamatan_pasien'] !!}</td>
					<td>{!! $val['kabupaten_pasien'] !!}</td>
					<td>{!! $val['provinsi_pasien'] !!}</td>
					<td>{!! $val['tgl_mulai_demam'] !!}</td>
					<td>{!! $val['jml_imunisasi_dpt_txt'] !!}</td>
					<td>{!! $val['tgl_imunisasi_difteri'] !!}</td>
					<td>{!! $val['tgl_pelacakan'] !!}</td>
					<td>{!! $val['gejala_lain'] !!}</td>
					<td>{!! $val['jml_kontak'] !!}</td>
					<td>{!! $val['diambil_spec_kontak'] !!}</td>
					<td>{!! $val['positif_kontak'] !!}</td>
					<td>{!! $val['keadaan_akhir_txt'] !!}</td>
					<?php
					$vs = explode('|',$val['spesimen']);
					$ds = [];
					$s0 = $s1 = $s2 = $s3 = $s4 = '';
					foreach ($vs as $vss) {
						$d = explode('_',$vss);
						if (!empty($d[0])) {
							if ($d[0] == 1) {
								$s0 .= 'Tenggorokan<br/>';
							}else if ($d[0] == 2) {
								$s0 .= 'Hidung<br/>';
							}
						}else{
							$s0 .= '<br/>';
						}
						$s1 .= !empty($d[1])?$d[1].'<br/>':'<br/>';
						if (!empty($d[2])) {
							if ($d[2] == 1) {
								$s2 .= 'Kultur<br/>';
							}else if ($d[2] == 2) {
								$s2 .= 'Mikroskop<br/>';
							}
						}else{
							$s2 .= '<br/>';
						}
						$s3 .= !empty($d[3])?$d[3].'<br/>':'<br/>';
						if (!empty($d[4])) {
							if ($d[4] == 1) {
								$s4 .= 'Positif<br/>';
							}else if ($d[4] == 2) {
								$s4 .= 'Negatif<br/>';
							}
						}else{
							$s4 .= '<br/>';
						}
					}
					?>
					<td >{!! $s0 !!}</td>
					<td >{!! $s1 !!}</td>
					<td >{!! $s2 !!}</td>
					<td >{!! $s3 !!}</td>
					<td >{!! $s4 !!}</td>
					<td>{!! $val['klasifikasi_final_txt']; !!}</td>
					<td>{!! $val['longitude']; !!}</td>
					<td>{!! $val['latitude']; !!}</td>
					<td>{!! $val['nama_pelapor']; !!}</td>
					<td>{!! $val['nama_kantor']; !!}</td>
					<td>{!! $val['jabatan']; !!}</td>
					<td>{!! $val['name_provinsi_pelapor']; !!}</td>
					<td>{!! $val['name_kabupaten_pelapor']; !!}</td>
					<td>{!! $val['tgl_laporan']; !!}</td>
					<td>{!! $val['no_telp']; !!}</td>
					<td>{!! $val['tempat_tinggal_saat_ini']; !!}</td>
					<td>{!! $val['tempat_tinggal_sesuai_kartu']; !!}</td>
					<td>{!! $val['pekerjaan']; !!}</td>
					<td>{!! $val['alamat_kerja']; !!}</td>
					<td>{!! $val['faskes_tempat_periksa']; !!}</td>
					<td>{!! $val['nama_saudara_yg_dihubungi']; !!}</td>
					<td>{!! $val['alamat_saudara']; !!}</td>
					<td>{!! $val['kelurahan_saudara']; !!}</td>
					<td>{!! $val['kecamatan_saudara']; !!}</td>
					<td>{!! $val['kabupaten_saudara']; !!}</td>
					<td>{!! $val['provinsi_saudara']; !!}</td>
					<td>{!! $val['no_telp_saudara']; !!}</td>
					<td>{!! $val['tgl_mulai_sakit']; !!}</td>
					<td>{!! $val['keluhan_untuk_berobat']; !!}</td>
					<?php
						$g = explode('|',$val['gejala_tanda_sakit']);
						$dg = [];
						foreach ($g as $vg) {
							$g1 = explode('_', $vg);
							$dg[$g1[0]]['is'] = 'V';
							$dg[$g1[0]]['tgl_kejadian'] = !empty($g1[1])?$g1[1]:'';
						}
					?>
					<td>{!! empty($dg['26'])?'':$dg['26']['is']; !!}</td>
					<td>{!! empty($dg['26'])?'':$dg['26']['tgl_kejadian']; !!}</td>
					<td>{!! empty($dg['27'])?'':$dg['27']['is']; !!}</td>
					<td>{!! empty($dg['27'])?'':$dg['27']['tgl_kejadian']; !!}</td>
					<td>{!! empty($dg['28'])?'':$dg['28']['is']; !!}</td>
					<td>{!! empty($dg['28'])?'':$dg['28']['tgl_kejadian']; !!}</td>
					<td>{!! empty($dg['29'])?'':$dg['29']['is']; !!}</td>
					<td>{!! empty($dg['29'])?'':$dg['29']['tgl_kejadian']; !!}</td>
					<td>{!! empty($dg['30'])?'':$dg['30']['is']; !!}</td>
					<td>{!! empty($dg['30'])?'':$dg['30']['tgl_kejadian']; !!}</td>
					<td>{!! $val['gejala_lain_pe']; !!}</td>
					<td>{!! $val['status_imunisasi_difteri_txt']; !!}</td>
					<td>{!! $val['jml_imunisasi_dpt']; !!}</td>
					<td>{!! $val['tahun_imunisasi_dpt']; !!}</td>
					<td>{!! $val['berobat_ke_rs']!=1?'':'V'; !!}</td>
					<td>{!! $val['riwayat_pengobatan_rs_txt']; !!}</td>
					<td>{!! $val['trakeostomi_txt']; !!}</td>
					<td>{!! $val['berobat_ke_puskesmas']!=1?'':'V'; !!}</td>
					<td>{!! $val['riwayat_pengobatan_puskesmas_txt']; !!}</td>
					<td>{!! $val['dokter_praktek_swasta']!=1?'':'V'; !!}</td>
					<td>{!! $val['perawat_mantri_bidan']!=1?'':'V'; !!}</td>
					<td>{!! $val['tidak_berobat']!=1?'':'V'; !!}</td>
					<td>{!! $val['antibiotik']; !!}</td>
					<td>{!! $val['obat_lain']; !!}</td>
					<td>{!! $val['ads']; !!}</td>
					<td>{!! $val['kondisi_kasus_saat_ini_txt']; !!}</td>
					<td>{!! $val['bepergian_dalam_2_minggu_txt']; !!}</td>
					<td>{!! $val['tujuan_bepergian_dalam_2_minggu']; !!}</td>
					<td>{!! $val['2_minggu_terakhir_berkunjung_ke_org_meninggal_gejala_sama_txt']; !!}</td>
					<td>{!! $val['tujuan_berkunjung_ke_org_meninggal_gejala_sama']; !!}</td>
					<td>{!! $val['2_minggu_terakhir_ada_tamu_gejala_sama_txt']; !!}</td>
					<td>{!! $val['asal_tamu_gejala_sama']; !!}</td>
					<?php
					$h = explode('|', $val['kontak_kasus']);
					$dh1=$dh2=$dh3=$dh4=$dh5=$dh6=$dh7=$dh8=$dh9 = '';
					foreach ($h as $vh) {
						$vh1 = explode('_', $vh);
						$dh1 .= empty($vh1[0])?'<br/>':$vh1[0].'<br/>';
						$dh2 .= empty($vh1[1])?'<br/>':$vh1[1].'<br/>';
						$dh3 .= empty($vh1[2])?'<br/>':$vh1[2].'<br/>';
						$dh4 .= empty($vh1[3])?'<br/>':$vh1[3].'<br/>';
						$dh5 .= empty($vh1[4])?'<br/>':$vh1[4].'<br/>';
						$dh6 .= empty($vh1[5])?'<br/>':$vh1[5].'<br/>';
						$dh7 .= empty($vh1[6])?'<br/>':$vh1[6].'<br/>';
						$dh8 .= empty($vh1[7])?'<br/>':$vh1[7].'<br/>';
						$dh9 .= empty($vh1[8])?'<br/>':$vh1[8].'<br/>';
					}
					?>
					<td>{!! $dh1; !!}</td>
					<td>{!! $dh2; !!}</td>
					<td>{!! $dh3; !!}</td>
					<td>{!! $dh4; !!}</td>
					<td>{!! $dh5; !!}</td>
					<td>{!! $dh6; !!}</td>
					<td>{!! $dh7; !!}</td>
					<td>{!! $dh8; !!}</td>
					<td>{!! $dh9; !!}</td>
			</tr>
		@endforeach
	@endif
</table>

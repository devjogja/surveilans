<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Data spesimen dan hasil laboratorium</h3>
	</div>
	<div class='form-horizontal'>
		<div class="box-body">
			<table class="table table-striped table-bordered" id="spesimen">
				<thead>
					<tr>
						<th>Jenis Spesimen</th>
						<th>Tanggal ambil spesimen</th>
						<th>Jenis Pemeriksaan</th>
						<th>Kode Spesimen</th>
						<th>Hasil</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			<div style="margin: 14px 0px;">
				<button type="button" class="btn btn-success btn-flat" id="addSpesimen"><i class="fa fa-plus-circle"></i> Tambah Spesimen</button>
			</div>
			<div id="div_sampel" style="margin-top: 2%">
				<table class="table table-bordered">
					<tr>
						<td>Jenis Spesimen</td>
						<td>Tanggal ambil spesimen</td>
						<td>Jenis Pemeriksaan</td>
						<td>Kode Spesimen</td>
						<td>Hasil</td>
					</tr>
					<tr>
						<td>{!! Form::select(null, array(null=>'--Pilih--','1'=>'Tenggorokan','2'=>'Hidung'), null, ['class' => 'form-control','id'=>'jenis_spesimen']) !!}</td>
						<td>{!! Form::text(null, null, ['class' => 'form-control datemax','placeholder'=>'Tanggal ambil spesimen','id'=>'tgl_ambil_spesimen','disabled']) !!}</td>
						<td>{!! Form::select(null, array(null=>'--Pilih--','1'=>'Kultur','2'=>'Mikroskop'), null, ['class' => 'form-control','id'=>'jenis_pemeriksaan','disabled']) !!}</td>
						<td>{!! Form::text('', null, ['class' => 'form-control','placeholder'=>'Kode Spesimen','id'=>'kode_spesimen','disabled']) !!}</td>
						<td>{!! Form::select(null, array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif'), null, ['class' => 'form-control','id'=>'hasil','disabled']) !!}</td>
					</tr>
				</table>
				<div class="row">
					<div class="col-md-offset-4 col-md-8">
						<button type="button" class="btn btn-default" id='tutup_sampel'>Tutup</button>
						<button type="button" class="btn btn-success" disabled="disabled" id="tambah_spesimen">Tambah</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('#div_sampel').hide();
		$('#tutup_sampel').on('click',function(){
			$('#div_sampel').hide(500);
			$('#addSpesimen').show();
			return false;
		});
		$('#addSpesimen').on('click',function(){
			$('#div_sampel').show(500);
			$('#addSpesimen').hide();
			return false;
		});
		$('#jenis_spesimen').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#tgl_ambil_spesimen').removeAttr('disabled');
			}else{
				$('#tgl_ambil_spesimen').attr('disabled','disabled').val(null);
			}
			$('#jenis_pemeriksaan').attr('disabled','disabled').select2('val','');
			$('#hasil').attr('disabled','disabled').select2('val','');
			return false;
		});
		$('#tgl_ambil_spesimen').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#jenis_pemeriksaan').removeAttr('disabled');
			}else{
				$('#jenis_pemeriksaan').attr('disabled','disabled').select2('val','');
			}
			if(isset(val)){
				$('#tambah_spesimen').removeAttr('disabled');
			}else{
				$('#tambah_spesimen').attr('disabled','disabled');
			}
			$('#hasil').attr('disabled','disabled').val(null);
			return false;
		});
		$('#jenis_pemeriksaan').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#kode_spesimen').removeAttr('disabled');
			}else{
				$('#kode_spesimen').attr('disabled','disabled').val(null);
			}
			return false;
		});$('#kode_spesimen').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#hasil').removeAttr('disabled');
			}else{
				$('#hasil').attr('disabled','disabled').val(null);
			}
			return false;
		});
		$('#tambah_spesimen').on('click',function(){
			var jst = $('#jenis_spesimen option:selected').text();
			var js = $('#jenis_spesimen').val();
			var ts = $('#tgl_ambil_spesimen').val();
			var jpt = $('#jenis_pemeriksaan option:selected').text();
			var jp = $('#jenis_pemeriksaan').val();
			var ks = $('#kode_spesimen').val();
			var h = $('#hasil').val();
			var ht = $('#hasil option:selected').text();
			$('#spesimen tbody').append('<tr>'+
				'<td>'+jst+'</td>'+
				'<td>'+ts+'</td>'+
				'<td>'+jpt+'</td>'+
				'<td>'+ks+'</td>'+
				'<td>'+ht+'</td>'+
				'<td><a href="javascript:void(0);" class="rm btn-sm btn-danger"><i class="fa fa-remove"></i></a>'+
				'<input type="hidden" name="ds[spesimen][]" value="'+js+'|'+ts+'|'+jp+'|'+ks+'|'+h+'"></td>'+
				'</tr>'
				);
			$('#jenis_spesimen, #jenis_pemeriksaan, #hasil').select2('val','');
			$('#jenis_pemeriksaan').attr('disabled','disabled');
			$('#tgl_ambil_spesimen').val(null).attr('disabled','disabled');
			$('#kode_spesimen').val(null).attr('disabled','disabled');
			$('#tambah_spesimen').attr('disabled','disabled');
			$('.rm').on('click',function(){
				$(this).parent().parent().remove();
			});
			return false;
		});
	});
</script>

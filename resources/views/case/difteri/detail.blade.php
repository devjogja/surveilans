@extends('layouts.base')
@section('content')
@if($dt = $data['response'])
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			<table class="table table-striped">
				<caption>Identitas Pasien</caption>
				<tbody>
					<tr>
						<td class="title">No Epidemologi</td>
						<td>{!! $dt->no_epid or '-' !!} { {!! $dt->_no_epid or '-' !!} }</td>
					</tr>
					<tr>
						<td class="title">No Epidemologi Lama</td>
						<td>{!! $dt->no_epid_lama or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">No RM</td>
						<td>{!! $dt->no_rm or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Penderita</td>
						<td>{!! $dt->name_pasien or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">NIK</td>
						<td>{!! $dt->nik or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Nama orang tua</td>
						<td>{!! $dt->nama_ortu or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Jenis kelamin</td>
						<td>{!! $dt->jenis_kelamin_txt or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Lahir(Umur)</td>
						<td><?php
							$tgl_lahir = ($dt->tgl_lahir)?$dt->tgl_lahir:'-';
							$umur_thn = ($dt->umur_thn)?$dt->umur_thn.' Th':'';
							$umur_bln = ($dt->umur_bln)?$dt->umur_bln.' Bln':'';
							$umur_hari = ($dt->umur_hari)?$dt->umur_hari.' Hari':'';
							?>
							{!! $tgl_lahir !!}  ({!!$umur_thn!!} {!!$umur_bln !!} {!!$umur_hari!!})
						</td>
					</tr>
					<tr>
						<td class="title">Agama</td>
						<td>{!! $dt->religion_text or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Alamat</td>
						<td>{!! $dt->alamat or '' !!} {!! $dt->full_address or '' !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Data surveilans Difteri</caption>
				<tbody>
					<tr>
						<td class="title">Tanggal mulai sakit (demam)</td>
						<td>{!! $dt->tgl_mulai_demam  !!}</td>
					</tr>
					<tr>
						<td class="title">Imunisasi DPT sebelum sakit berapa kali</td>
						<td>{!! $dt->jml_imunisasi_dpt_txt !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal imunisasi difteri terakhir</td>
						<td>{!! $dt->tgl_imunisasi_difteri  !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal pelacakan</td>
						<td>{!! $dt->tgl_pelacakan  !!}</td>
					</tr>
					<tr>
						<td class="title">Gejala lain</td>
						<td>{!! $dt->gejala_lain or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Kontak</td>
						<td>
							<table class="table table-striped" style="width:20%">
								<tr>
									<td class="title" style="width:80%;">Jumlah</td>
									<td>{!! $dt->jml_kontak or '-' !!}</td>
								</tr>
								<tr>
									<td class="title">Diambil spec (swab hidung)</td>
									<td>{!! $dt->diambil_spec_kontak or '-' !!}</td>
								</tr>
								<tr>
									<td class="title">Positif</td>
									<td>{!! $dt->positif_kontak or '-' !!}</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="title">Keadaan akhir</td>
						<td>{!! $dt->keadaan_akhir_txt or '-' !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Data spesimen dan hasil laboratorium</caption>
				<thead>
					<tr>
						<th>Jenis Spesimen</th>
						<th>Tanggal ambil sampel</th>
						<th>Jenis Pemeriksaan</th>
						<th>Kode Spesimen</th>
						<th>Hasil</th>
					</tr>
				</thead>
				<tbody>
					@if(empty($dt->dtSpesimen))
					<tr><td colspan="5">Tidak terdapat data spesimen yang diambil.</td></tr>
					@else
					@foreach($dt->dtSpesimen AS $key=>$val)
					<tr>
						<td>{!! $val->jenis_spesimen_txt!!}</td>
						<td>{!! $val->tgl_ambil_spesimen !!}</td>
						<td>{!! $val->jenis_pemeriksaan_txt!!}</td>
						<td>{!! $val->kode_spesimen!!}</td>
						<td>{!! $val->hasil_txt!!}</td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				<caption>Klasifikasi Final</caption>
				<tbody>
					<tr>
						<td class="title">Hasil klasifikasi final</td>
						<td>{!! $dt->klasifikasi_final_txt or '-' !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endif
<div class="col-sm-12">
	<div class="footer">
		<a href="{!!URL::to('case/difteri')!!}" class="btn btn-primary">Kembali</a>
	</div>
</div>
@endsection
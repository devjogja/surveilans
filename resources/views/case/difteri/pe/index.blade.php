@extends('layouts.base')
@section('content')
<style type="text/css">
.gejala tr td{
	width: 50%;
	padding-bottom: 4px;
}
</style>

<div class="col-sm-12">
	<div class="alert alert-notif alert-dismissable">
		<i class="icon fa fa-warning"></i> Text input yang bertanda bintang (*) wajib di isi
	</div>
	<div class="row">
		{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form', 'class' => 'form-horizontal']) !!}
		{!! Form::hidden('id_trx_case', $data['id_trx_case'], ['id'=>'id_trx_case']) !!}
		{!! Form::hidden('id_trx_pe', $data['id_trx_pe'], ['id'=>'id_trx_pe']) !!}
		{!! Form::hidden('dpe[id_faskes]', Helper::role()->id_faskes,['id'=>'id_faskes']) !!}
		{!! Form::hidden('dpe[id_role]', Helper::role()->id_role,['id'=>'id_role']) !!}
		{!! Form::hidden('dpe[jenis_input]', '1',['id'=>'jenis_input']) !!}

		<div class="col-sm-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Identitas pelapor</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Nama', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dpel[nama_pelapor]', null, ['class' => 'form-control','id'=>'nama_pelapor','placeholder'=>'Nama Pelapor']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Nama Kantor', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dpel[nama_kantor]', null, ['class' => 'form-control','id'=>'nama_kantor','placeholder'=>'Nama Kantor']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Jabatan', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dpel[jabatan]', null, ['class' => 'form-control','id'=>'jabatan','placeholder'=>'Jabatan']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Provinsi', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dpel[code_provinsi_pelapor]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control','id'=>'id_provinsi_pelapor','onchange'=>"getKabupaten('_pelapor')"]) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Kabupaten', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('dpel[code_kabupaten_pelapor]', array(null=>'Pilih Kabupaten'), null, ['class' => 'form-control', 'id'=>'id_kabupaten_pelapor']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Tanggal Laporan', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dpel[tgl_laporan]', null, ['class' => 'form-control datemax','id'=>'tgl_laporan','placeholder'=>'Tanggal Laporan']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Identitas penderita</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'No.epidemologi', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dc[no_epid]', null, ['class' => 'form-control','id'=>'no_epid','placeholder'=>'No.epidemologi','readonly']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Nama Penderita', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dp[name]', null, ['class' => 'form-control','id'=>'name_pasien','placeholder'=>'Nama penderita']) !!}
								{!! Form::hidden('id_pasien', null, ['class' => 'form-control','id'=>'id_pasien']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'NIK', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dp[nik]', null, ['class' => 'form-control','id'=>'nik','placeholder'=>'NIK']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('name', 'Nama Orang Tua', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('df[name]', null, ['class' => 'form-control','placeholder'=>'Nama Orang Tua','id'=>'nama_ortu']) !!}
								{!! Form::hidden('id_family', null, ['class' => 'form-control','id'=>'id_family']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('jenis_kelamin', 'Jenis Kelamin', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-4">
								<div class="input-group">
									{!! Form::select('dp[jenis_kelamin]', array(null=>'--Pilih--','L'=>'Laki-laki','P'=>'Perempuan','T'=>'Tidak Jelas'), null, ['class' => 'form-control', 'id'=>'jenis_kelamin']) !!}
								</div>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('tgl_lahir', 'Tanggal Lahir', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-4">
								{!! Form::text('dp[tgl_lahir]', null, ['class' => 'form-control tgl_lahir datemax','placeholder'=>'Tanggal Lahir','id'=>'tgl_lahir']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('umur', 'Umur', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-3" style="padding-right: 0px;">
										<div class="input-group">
											{!! Form::text('dp[umur_thn]', null, ['class' => 'form-control umur','id'=>'umur_thn']) !!}
											<span class="input-group-addon">Thn</span>
										</div>
									</div>
									<div class="col-sm-3" style="padding-right: 0px;">
										<div class="input-group">
											{!! Form::text('dp[umur_bln]', null, ['class' => 'form-control umur','id'=>'umur_bln']) !!}
											<span class="input-group-addon">Bln</span>
										</div>
									</div>
									<div class="col-sm-3" style="padding-right: 0px;">
										<div class="input-group">
											{!! Form::text('dp[umur_hari]', null, ['class' => 'form-control umur','id'=>'umur_hari']) !!}
											<span class="input-group-addon">Hari</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'No.Telp', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dp[no_telp]', null, ['class' => 'form-control','id'=>'no_telp','placeholder'=>'No.Telp']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Tempat tinggal saat ini', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::textarea('dp[tempat_tinggal_saat_ini]', null, ['class' => 'form-control','rows'=>'3','id'=>'tempat_tinggal_saat_ini','placeholder'=>'Tempat tinggal saat ini']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Tempat tinggal sesuai kartu identitas/alamat asal', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::textarea('dp[tempat_tinggal_sesuai_kartu]', null, ['class' => 'form-control','rows'=>'3','id'=>'tempat_tinggal_sesuai_kartu','placeholder'=>'Tempat tinggal sesuai kartu identitas/alamat asal']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Pekerjaan', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dp[pekerjaan]', null, ['class' => 'form-control','id'=>'pekerjaan','placeholder'=>'Pekerjaan']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Alamat tempat kerja', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::textarea('dp[alamat_tempat_kerja]', null, ['class' => 'form-control','rows'=>'3','id'=>'alamat_tempat_kerja','placeholder'=>'Alamat tempat kerja']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Faskes tempat periksa/puskesmas', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('dpe[faskes_tempat_periksa]', null, ['class' => 'form-control','id'=>'faskes_tempat_periksa','placeholder'=>'Faskes tempat periksa/puskesmas']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Orang tua/saudara dekat yang dapat dihubungi', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-8">
								<div class="form-group">
									{!! Form::label(null, 'Nama', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-7">
										{!! Form::text('df[nama_saudara_yg_dihubungi]', null, ['class' => 'form-control','id'=>'nama_saudara_yg_dihubungi','placeholder'=>'Nama yang di hubungi.']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label(null, 'Tempat tinggal saat ini', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-8">
										<div class="form-group">
											{!! Form::label(null, 'Provinsi', ['class' => 'col-sm-3 control-label']) !!}
											<div class="col-sm-8">
												{!! Form::select('df[code_provinsi]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control','id'=>'id_provinsi_family','onchange'=>"getKabupaten('_family')"]) !!}
											</div>
										</div>
										<div class="form-group">
											{!! Form::label(null, 'Kabupaten', ['class' => 'col-sm-3 control-label']) !!}
											<div class="col-sm-8">
												{!! Form::select('df[code_kabupaten]', array(null=>'Pilih Kabupaten/Kota'), null, ['class' => 'form-control', 'id'=>'id_kabupaten_family','onchange'=>"getKecamatan('_family')"]) !!}
											</div>
										</div>
										<div class="form-group">
											{!! Form::label(null, 'Kecamatan', ['class' => 'col-sm-3 control-label']) !!}
											<div class="col-sm-8">
												{!! Form::select('df[code_kecamatan]', array(null=>'Pilih Kecamatan'), null, ['class' => 'form-control','id'=>'id_kecamatan_family','onchange'=>"getKelurahan('_family')"]) !!}
											</div>
										</div>
										<div class="form-group">
											{!! Form::label(null, 'Kelurahan', ['class' => 'col-sm-3 control-label']) !!}
											<div class="col-sm-8">
												<div class="input-group">
													{!! Form::select('df[code_kelurahan]', array(null=>'Pilih Kelurahan'), null, ['class' => 'form-control','id'=>'id_kelurahan_family']) !!}
													<span class="input-group-addon al">(*)</span>
												</div>
											</div>
										</div>
										<div class="form-group">
											{!! Form::label('alamat', 'Alamat', ['class' => 'col-sm-3 control-label']) !!}
											<div class="col-sm-8">
												{!! Form::textarea('df[alamat]',null, ['class' => 'form-control','id'=>'alamat_family','rows'=>'3','placeholder'=>'Hanya diisi nama jalan, no. rumah dan no. RT - RW']) !!}
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									{!! Form::label(null, 'Telepon', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-7">
										{!! Form::text('df[no_telp]', null, ['class' => 'form-control','id'=>'no_telp_family','placeholder'=>'Telepon']) !!}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Riwayat sakit</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Tanggal mulai sakit (demam)', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								<div class="input-group">
									{!! Form::text('drs[tgl_mulai_sakit]', null, ['class' => 'form-control datemax',"onchange"=>"getAge();",'id'=>'tgl_mulai_sakit','placeholder'=>'Tanggal mulai sakit']) !!}
									<span class="input-group-addon al">(*)</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Keluhan utama yang mendorong untuk berobat?', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::text('drs[keluhan_untuk_berobat]', null, ['class' => 'form-control','id'=>'keluhan_untuk_berobat','placeholder'=>'Keluhan utama yang mendorong untuk berobat']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Gejala dan tanda sakit', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-7">
								<table class="gejala">
									@foreach(Helper::getGejala('3','1') AS $key=>$val)
									<tr>
										<td><input type="checkbox" name="dg[gejala][]" id="gejala{!!$val->id!!}" value="{!!$val->id!!}" class="col-sm-1">&nbsp;{!!$val->name!!}</td>
										@if($val->id == 27)
										<td>{!! Form::text('dg[tgl_gejala][]', null, ['class' => 'form-control datemax','id'=>"tglSakit$val->id",'placeholder'=>'Tanggal sakit','disabled','required'=>'']) !!}</td>
										@else
										<td>{!! Form::text('dg[tgl_gejala][]', null, ['class' => 'form-control datemax','id'=>"tglSakit$val->id",'placeholder'=>'Tanggal sakit','disabled']) !!}</td>
										@endif
									</tr>
									@endforeach
									<tr>
										<td><input type="checkbox" name="dg[other][]" id="other_gejala" value="other" class="col-sm-1">&nbsp;Gejala lain,sebutkan</td>
										<td>{!! Form::text('dg[other_val][]', null, ['class' => 'form-control','id'=>"other_gejala_txt",'placeholder'=>'Contoh : batuk,pilek,dll','disabled']) !!}</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Status imunisasi difteri', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-5">
								{!! Form::select('drs[status_imunisasi_difteri]', array(null=>'--Pilih--','1'=>'Belum Pernah','2'=>'Sudah Pernah','3'=>'Tidak tahu'), null, ['class' => 'form-control', 'id'=>'status_imunisasi_difteri']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Jika sudah pernah imunisasi DPT, sebutkan frekuensi dan tahun imunisasi', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-8">
								<div class="form-group">
									{!! Form::label(null, 'Berapa kali imunisasi', ['class' => 'col-sm-3 control-label']) !!}
									<div class="col-sm-7">
										{!! Form::input('number','drs[jml_imunisasi_dpt]', null, ['class' => 'form-control','id'=>'jml_imunisasi_dpt','placeholder'=>'Berapa kali imunisasi']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label(null, 'Tahun Imunisasi', ['class' => 'col-sm-3 control-label']) !!}
									<div class="col-sm-7">
										{!! Form::text('drs[tahun_imunisasi_dpt]', null, ['class' => 'form-control','id'=>'tahun_imunisasi_dpt','placeholder'=>'Contoh : 2005, 2009']) !!}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Data Spesimen</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, '', ['class' => 'col-sm-1 control-label']) !!}
							<div class="col-sm-12">
								<table class="table table-striped" id="spesimen">
									<thead style="background: #4caf50;color: white;border:1px solid white">
										<tr>
											<th>Jenis Spesimen</th>
											<th>Tanggal ambil spesimen</th>
											<th>Jenis Pemeriksaan</th>
											<th>Kode Spesimen</th>
											<th colspan='2'>Hasil</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
								<div style="margin: 14px 0px;">
									<button type="button" class="btn btn-success btn-flat" id="addSpesimen"><i class="fa fa-plus-circle"></i> Tambah Spesimen</button>
								</div>
								<div id="div_sampel" style="margin-top: 2%">
									<table class="table table-bordered">
										<tr>
											<td>Jenis Spesimen</td>
											<td>Tanggal ambil spesimen</td>
											<td>Jenis Pemeriksaan</td>
											<td>Kode Spesimen</td>
											<td>Hasil</td>
										</tr>
										<tr>
											<td>{!! Form::select(null, array(null=>'--Pilih--','1'=>'Tenggorokan','2'=>'Hidung'), null, ['class' => 'form-control','id'=>'jenis_spesimen']) !!}</td>
											<td>{!! Form::text(null, null, ['class' => 'form-control datemax','placeholder'=>'Tanggal ambil spesimen','id'=>'tgl_ambil_spesimen','disabled']) !!}</td>
											<td>{!! Form::select(null, array(null=>'--Pilih--','1'=>'Kultur','2'=>'Mikroskop'), null, ['class' => 'form-control','id'=>'jenis_pemeriksaan','disabled']) !!}</td>
											<td>{!! Form::text('', null, ['class' => 'form-control','placeholder'=>'Kode Spesimen','id'=>'kode_spesimen','disabled']) !!}</td>
											<td>{!! Form::select(null, array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif'), null, ['class' => 'form-control','id'=>'hasil','disabled']) !!}</td>
										</tr>
									</table>
									<div class="row">
										<div class="col-md-offset-4 col-md-8">
											<button class="btn btn-default" id='tutup_sampel'>Tutup</button>
											<button class="btn btn-success" disabled="disabled" id="tambah_spesimen">Tambah</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Riwayat pengobatan</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Penderita berobat ke', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-8">
								<table class="table">
									<tr>
										<td style="width:30%"><input type="checkbox" name='drp[berobat_ke_rs]' id='berobat_ke_rs' value="1">&nbsp;Rumah sakit</td>
										<td>
											<table class="table">
												<tr>
													<td style="width:30%">{!! Form::label(null, 'Dirawat', ['class' => 'control-label']) !!}</td>
													<td>
														{!! Form::select('drp[riwayat_pengobatan_rs]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'riwayat_pengobatan_rs']) !!}
													</td>
												</tr>
												<tr>
													<td>{!! Form::label(null, 'Trakeostomi', ['class' => 'control-label']) !!}</td>
													<td>
														{!! Form::select('drp[trakeostomi]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'trakeostomi']) !!}
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td><input type="checkbox" id="berobat_ke_puskesmas" name='drp[berobat_ke_puskesmas]' value="1" >&nbsp;Puskesmas</td>
										<td>
											<table class="table">
												<tr>
													<td width="30%">{!! Form::label(null, 'Dirawat', ['class' => 'control-label']) !!}</td>
													<td>
														{!! Form::select('drp[riwayat_pengobatan_puskesmas]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'riwayat_pengobatan_puskesmas']) !!}
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr style="height:30px">
										<td><input type="checkbox" id="dokter_praktek_swasta" name='drp[dokter_praktek_swasta]' value="1" class="col-sm-1">&nbsp;Dokter praktek swasta</td>
									</tr>
									<tr style="height:30px">
										<td><input type="checkbox" id="perawat_mantri_bidan" name='drp[perawat_mantri_bidan]' value="1" class="col-sm-1">&nbsp;Perawat/mantri/bidan</td>
									</tr>
									<tr style="height:30px">
										<td><input type="checkbox" id="tidak_berobat" name='drp[tidak_berobat]' value="1" class="col-sm-1">&nbsp;Tidak Berobat</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Antibiotik', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-4">
								{!! Form::text('drp[antibiotik]', null, ['class' => 'form-control','id'=>'antibiotik','placeholder'=>'Antibiotik']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Obat lain', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-4">
								{!! Form::text('drp[obat_lain]', null, ['class' => 'form-control','id'=>'obat_lain','placeholder'=>'Obat lain']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'ADS (Anti Difteri Serum)', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-4">
								{!! Form::text('drp[ads]', null, ['class' => 'form-control','id'=>'ads','placeholder'=>'ADS (Anti Difteri Serum)']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Kondisi kasus saat ini', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-4">
								{!! Form::select('drp[kondisi_kasus_saat_ini]', array(null=>'--Pilih--','1'=>'Masih sakit','2'=>'Sembuh','3'=>'Meninggal'), null, ['class' => 'form-control', 'id'=>'kondisi_kasus_saat_ini']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Riwayat kontak</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, 'Dalam 2 minggu terakhir sebelum sakit apakah penderita bepergian', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-2">
								{!! Form::select('drk[bepergian_dalam_2_minggu]', array(null=>'--Pilih--','1'=>'Pernah','2'=>'Tidak Pernah','3'=>'Tidak jelas'), null, ['class' => 'form-control', 'id'=>'bepergian_dalam_2_minggu']) !!}
							</div>
							{!! Form::label(null, 'Jika pernah, kemana?', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-3">
								{!! Form::text('drk[tujuan_bepergian_dalam_2_minggu]', null, ['class' => 'form-control','id'=>'tujuan_bepergian_dalam_2_minggu']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Dalam 2 minggu terakhir sebelum sakit apakah penderita pernah berkunjung ke rumah teman/saudara yang sakit/meninggal dengan gejala yang sama', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-2">
								{!! Form::select('drk[2_minggu_terakhir_berkunjung_ke_org_meninggal_gejala_sama]', array(null=>'--Pilih--','1'=>'Pernah','2'=>'Tidak Pernah','3'=>'Tidak jelas'), null, ['class' => 'form-control', 'id'=>'2_minggu_terakhir_berkunjung_ke_org_meninggal_gejala_sama']) !!}
							</div>
							{!! Form::label(null, 'Jika pernah, kemana?', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-3">
								{!! Form::text('drk[tujuan_berkunjung_ke_org_meninggal_gejala_sama]', null, ['class' => 'form-control','id'=>'tujuan_berkunjung_ke_org_meninggal_gejala_sama']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label(null, 'Dalam 2 minggu terakhir, apakah pernah menerima tamu dengan sakit dengan gejala yang sama', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-2">
								{!! Form::select('drk[2_minggu_terakhir_ada_tamu_gejala_sama]', array(null=>'--Pilih--','1'=>'Pernah','2'=>'Tidak Pernah','3'=>'Tidak jelas'), null, ['class' => 'form-control', 'id'=>'2_minggu_terakhir_ada_tamu_gejala_sama']) !!}
							</div>
							{!! Form::label(null, 'Jika pernah, dari mana?', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-3">
								{!! Form::text('drk[asal_tamu_gejala_sama]', null, ['class' => 'form-control','id'=>'asal_tamu_gejala_sama']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Kontak Kasus</h3>
				</div>
				<div class='form-horizontal'>
					<div class="box-body">
						<div class="form-group">
							{!! Form::label(null, '', ['class' => 'col-sm-1 control-label']) !!}
							<div class="col-sm-12">
								<table class="table table-striped" id="data_kontak_kasus">
									<thead style="background: #4caf50;color: white;border:1px solid white">
										<tr>
											<th rowspan="3" valign="top"><center>Nama</center></th>
											<th rowspan="3" valign="top"><center>Umur</center></th>
											<th rowspan="3" valign="top"><center>Hubungan dengan kasus</center></th>
											<th rowspan="3" valign="top"><center>Status Imunisasi</center></th>
											<th colspan="4"><center>Data Spesimen</center></center></th>
											<th rowspan="3" valign="top"><center>Profilaksis</center></th>
											<th rowspan="3" valign="top"><center>Aksi</center></th>
										</tr>
										<tr>
											<th colspan="2"><center>Tenggorokan</center></th>
											<th colspan="2"><center>Hidung</center></th>
										</tr>
										<tr>
											<th><center>Tanggal ambil spesimen</center></th>
											<th><center>Hasil lab</center></th>
											<th><center>Tanggal ambil spesimen</center></th>
											<th><center>Hasil lab</center></th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
								<div style="margin: 14px 0px;">
									<button type="button" class="btn btn-success btn-flat" id="adddata_kontak_kasus"><i class="fa fa-plus-circle"></i> Tambah Kontak Kasus</button>
								</div>
								<div id="div_sampel_kontak_kasus" style="margin-top: 2%">
									<table class="table table-bordered">
										<tr>
											<th rowspan="3" valign="top"><center>Nama</center></th>
											<th rowspan="3" valign="top"><center>Umur</center></th>
											<th rowspan="3" valign="top"><center>Hubungan dengan kasus</center></th>
											<th rowspan="3" valign="top"><center>Status Imunisasi</center></th>
											<th colspan="4"><center>Data Spesimen</center></center></th>
											<th rowspan="3" valign="top"><center>Profilaksis</center></th>
										</tr>
										<tr>
											<th colspan="2"><center>Tenggorokan</center></th>
											<th colspan="2"><center>Hidung</center></th>
										</tr>
										<tr>
											<th><center>Tanggal ambil spesimen</center></th>
											<th><center>Hasil lab</center></th>
											<th><center>Tanggal ambil spesimen</center></th>
											<th><center>Hasil lab</center></th>
										</tr>
										<tr>
											<td>{!! Form::text('', null, ['class' => 'form-control','placeholder'=>'Nama','id'=>'nama']) !!}</td>
											<td>{!! Form::text('', null, ['class' => 'form-control','placeholder'=>'Umur','id'=>'umur']) !!}</td>
											<td>{!! Form::text('', null, ['class' => 'form-control','placeholder'=>'Hubungan dengan kasus','id'=>'hub_dg_kasus']) !!}</td>
											<td>{!! Form::text('', null, ['class' => 'form-control','placeholder'=>'Status imunisasi','id'=>'stat_imun']) !!}</td>
											<td>{!! Form::text('', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal ambil spesimen','id'=>'tgl_ambil_tgrok']) !!}</td>
											<td>{!! Form::select('', array(null=>'--Pilih--','Positif'=>'Positif','Negatif'=>'Negatif'), null, ['class' => 'form-control', 'id'=>'hasil_lab_tgrok']) !!}</td>
											<td>{!! Form::text('', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal ambil spesimen','id'=>'tgl_ambil_hidung']) !!}</td>
											<td>{!! Form::select('', array(null=>'--Pilih--','Positif'=>'Positif','Negatif'=>'Negatif'), null, ['class' => 'form-control', 'id'=>'hasil_lab_hidung']) !!}</td>
											<td>{!! Form::text('', null, ['class' => 'form-control','placeholder'=>'Profilaksis','id'=>'profilaksis']) !!}</td>
										</tr>
									</table>
									<div class="row">
										<div class="col-md-offset-5 col-md-7">
											<button class="btn btn-default" id='tutup_sampel_kontak_kasus'>Tutup</button>
											<button class="btn btn-success" disabled="disabled" id="tambah_kontak_kasus">Tambah</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-sm-12">
	<div class="footer">
		{!! Form::reset("Batal", ['class' => 'btn btn-warning batal']) !!}
		{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
	</div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
	$(function(){
		$('#div_sampel').hide();
		$('#tutup_sampel').on('click',function(){
			$('#div_sampel').hide(500);
			$('#addSpesimen').show();
			return false;
		});

		$('#addSpesimen').on('click',function(){
			$('#div_sampel').show(500);
			$('#addSpesimen').hide();
			return false;
		});

		$('#jenis_spesimen').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#tgl_ambil_spesimen').removeAttr('disabled');
			}else{
				$('#tgl_ambil_spesimen').attr('disabled','disabled').val(null);
			}
			$('#jenis_pemeriksaan').attr('disabled','disabled').select2('val','');
			$('#hasil').attr('disabled','disabled').select2('val','');
			return false;
		});

		$('#tgl_ambil_spesimen').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#jenis_pemeriksaan').removeAttr('disabled');
			}else{
				$('#jenis_pemeriksaan').attr('disabled','disabled').select2('val','');
			}
			$('#hasil').attr('disabled','disabled').val(null);
			return false;
		});

		$('#jenis_pemeriksaan').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#kode_spesimen').removeAttr('disabled');
			}else{
				$('#kode_spesimen').attr('disabled','disabled').val(null);
			}
			return false;
		});

		$('#kode_spesimen').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#hasil').removeAttr('disabled');
			}else{
				$('#hasil').attr('disabled','disabled').val(null);
			}
			return false;
		});

		$('#hasil').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#tambah_spesimen').removeAttr('disabled');
			}else{
				$('#tambah_spesimen').attr('disabled','disabled');
			}
			return false;
		});

		$('#tambah_spesimen').on('click',function(){
			var jst = $('#jenis_spesimen option:selected').text();
			var js = $('#jenis_spesimen').val();
			var ts = $('#tgl_ambil_spesimen').val();
			var jpt = $('#jenis_pemeriksaan option:selected').text();
			var jp = $('#jenis_pemeriksaan').val();
			var ks = $('#kode_spesimen').val();
			var h = $('#hasil').val();
			var ht = $('#hasil option:selected').text();
			$('#spesimen tbody').append('<tr>'+
				'<td>'+jst+'</td>'+
				'<td>'+ts+'</td>'+
				'<td>'+jpt+'</td>'+
				'<td>'+ks+'</td>'+
				'<td>'+ht+'</td>'+
				'<td><a href="javascript:void(0);" class="rm btn-sm btn-danger"><i class="fa fa-remove"></i></a>'+
				'<input type="hidden" name="ds[spesimen][]" value="'+js+'|'+ts+'|'+jp+'|'+ks+'|'+h+'"></td>'+
				'</tr>'
				);
			$('#jenis_spesimen, #jenis_pemeriksaan, #hasil').select2('val','');
			$('#jenis_pemeriksaan').attr('disabled','disabled');
			$('#tgl_ambil_spesimen').val(null).attr('disabled','disabled');
			$('#kode_spesimen').val(null).attr('disabled','disabled');
			$('#tambah_spesimen').attr('disabled','disabled');
			$('.rm').on('click',function(){
				$(this).parent().parent().remove();
			});
			return false;
		});

		$('#div_sampel_kontak_kasus').hide();
		$('#tutup_sampel_kontak_kasus').on('click',function(){
			$('#div_sampel_kontak_kasus').hide(500);
			$('#adddata_kontak_kasus').show();
			return false;
		});

		$('#adddata_kontak_kasus').on('click',function(){
			$('#div_sampel_kontak_kasus').show(500);
			$('#adddata_kontak_kasus').hide();
			return false;
		});

		$('#stat_imun').on('change',function(){
			var val = $(this).val();
			if(isset(val)){
				$('#tambah_kontak_kasus').removeAttr('disabled');
			}else{
				$('#tambah_kontak_kasus').attr('disabled','disabled');
			}
			return false;
		});

		$('#tambah_kontak_kasus').on('click',function(){
			var n = $('#nama').val();
			var u = $('#umur').val();
			var hub = $('#hub_dg_kasus').val();
			var st = $('#stat_imun').val();
			var tt = $('#tgl_ambil_tgrok').val();
			var ht = $('#hasil_lab_tgrok option:selected').text();
			var th = $('#tgl_ambil_hidung').val();
			var hh = $('#hasil_lab_hidung option:selected').text();
			var p = $('#profilaksis').val();
			$('#data_kontak_kasus tbody').append('<tr>'+
				'<td>'+n+'</td>'+
				'<td>'+u+'</td>'+
				'<td>'+hub+'</td>'+
				'<td>'+st+'</td>'+
				'<td>'+tt+'</td>'+
				'<td>'+ht+'</td>'+
				'<td>'+th+'</td>'+
				'<td>'+hh+'</td>'+
				'<td>'+p+'</td>'+
				'<td><a href="javascript:void(0);" class="rm btn-sm btn-danger"><i class="fa fa-remove"></i></a>'+
				'<input type="hidden" name="dkk[kontak_kasus][]" value="'+n+'|'+u+'|'+hub+'|'+st+'|'+tt+'|'+ht+'|'+th+'|'+hh+'|'+p+'"></td>'+
				'</tr>'
				);
			$('#nama ,#umur ,#hub_dg_kasus ,#stat_imun ,#tgl_ambil_tgrok, #tgl_ambil_hidung, #profilaksis').val(null);
			$("#hasil_lab_tgrok").select2('val','');
			$("#hasil_lab_hidung").select2('val','');
			$('#tambah_spesimen').attr('disabled','disabled');
			$('.rm').on('click',function(){
				$(this).parent().parent().remove();
			});
			return false;
		});

		@foreach(Helper::getGejala('3','1') AS $key=>$val)
		$("input[id='gejala{!!$val->id!!}']").on('ifChecked',function(){
			$('#tglSakit{!!$val->id!!}').removeAttr('disabled');
		});
		$("input[id='gejala{!!$val->id!!}']").on('ifUnchecked',function(){
			$('#tglSakit{!!$val->id!!}').attr('disabled','disabled').val(null);
		});
		@endforeach

		$("input[id='other_gejala']").on('ifChecked',function(){
			$('#other_gejala_txt').removeAttr('disabled');
		});
		$("input[id='other_gejala']").on('ifUnchecked',function(){
			$('#other_gejala_txt').attr('disabled','disabled').val(null);
		});
	});

function generateData() {
	var id = $('#id_trx_case').val();
	if(isset(id)){
		var action = BASE_URL+'case/difteri/getDetail/'+id;
		$.getJSON(action, function(result){
			var raw = result.response;
			if (isset(raw)) {
				$.each(raw, function(k, dt){
					$('#no_epid').val(dt.no_epid);
					$('#id_pasien').val(dt.id_pasien);
					$('#name_pasien').val(dt.name_pasien);
					$('#nik').val(dt.nik);
					$('#nama_ortu').val(dt.nama_ortu);
					$('#jenis_kelamin').val(dt.jenis_kelamin).trigger('change');
					$('#tgl_lahir').val(dt.tgl_lahir);
					$('#umur_thn').val(dt.umur_thn);
					$('#umur_bln').val(dt.umur_bln);
					$('#umur_hari').val(dt.umur_hari);
					$('#no_telp').val(dt.no_telp);
					$('#tempat_tinggal_saat_ini').val(dt.tempat_tinggal_saat_ini);
					$('#tempat_tinggal_sesuai_kartu').val(dt.tempat_tinggal_sesuai_kartu);
					$('#pekerjaan').val(dt.pekerjaan);
					$('#alamat_tempat_kerja').val(dt.alamat_tempat_kerja);
					$('#faskes_tempat_periksa').val(dt.faskes_tempat_periksa);
					$('#nama_saudara_yg_dihubungi').val(dt.nama_saudara_yg_dihubungi);
					$('#id_provinsi_family').val(dt.id_provinsi_family).trigger('change');
					$('#alamat_family').val(dt.alamat_family);
					$('#no_telp_family').val(dt.no_telp_family);
				});
			}
		});
	}
	var id_trx_pe = $('#id_trx_pe').val();
	if(isset(id_trx_pe)){
		var action = BASE_URL+'case/difteri/pe/getDetail/'+id_trx_pe;
		$.getJSON(action, function(result){
			var raw = result.response;
			if (isset(raw)) {
				$.each(raw, function(k, dt){
					$('#nama_pelapor').val(dt.nama_pelapor);
					$('#nama_kantor').val(dt.nama_kantor);
					$('#jabatan').val(dt.jabatan);
					$('#id_provinsi_pelapor').val(dt.code_provinsi_pelapor).trigger('change');
					$('#id_kabupaten_pelapor').html('<option value='+dt.code_kabupaten_pelapor+'>'+dt.name_kabupaten_pelapor+'</option>').trigger('change');
					$('#tgl_laporan').val(dt.tgl_laporan);
					$('#id_provinsi_family').val(dt.code_provinsi_ortu).trigger('change');
					$('#id_kabupaten_family').html('<option value='+dt.code_kabupaten_ortu+'>'+dt.name_kabupaten_ortu+'</option>').trigger('change');
					$('#id_kecamatan_family').html('<option value='+dt.code_kecamatan_ortu+'>'+dt.name_kecamatan_ortu+'</option>').trigger('change');
					$('#id_kelurahan_family').html('<option value='+dt.code_kelurahan_ortu+'>'+dt.name_kelurahan_ortu+'</option>').trigger('change');
					$('#alamat_family').val(dt.alamat_ortu);
					$('#no_telp_family').val(dt.no_telp_ortu);
					$('#tgl_mulai_sakit').val(dt.tgl_mulai_sakit);
					$.each(dt.dtGejala,function(kg,dg){
						$('#gejala'+dg.id_gejala).iCheck('check');
						$('#tglSakit'+dg.id_gejala).val(dg.tgl_kejadian).removeAttr('disabled');
						if(isset(dg.other_gejala)){
							$('#other_gejala').iCheck('check');
							$('#other_gejala_txt').val(dg.val_gejala).removeAttr('disabled');
						}
					});
					$('#keluhan_untuk_berobat').val(dt.keluhan_untuk_berobat);
					$('#status_imunisasi_difteri').val(dt.status_imunisasi_difteri).trigger('change');
					$('#jml_imunisasi_dpt').val(dt.jml_imunisasi_dpt);
					$('#tahun_imunisasi_dpt').val(dt.tahun_imunisasi_dpt);
					$.each(dt.dtSpesimen,function(ks,ds){
						$('#spesimen tbody').append('<tr>'+
							'<td>'+ds.jenis_spesimen_txt+'</td>'+
							'<td>'+ds.tgl_ambil_spesimen+'</td>'+
							'<td>'+ds.jenis_pemeriksaan_txt+'</td>'+
							'<td>'+ds.kode_spesimen+'</td>'+
							'<td>'+ds.hasil_txt+'</td>'+
							'<td><a href="javascript:void(0);" class="rm btn-sm btn-danger"><i class="fa fa-remove"></i></a>'+
							'<input type="hidden" name="ds[spesimen][]" value="'+ds.jenis_spesimen+'|'+ds.tgl_ambil_spesimen+'|'+ds.jenis_pemeriksaan+'|'+ds.kode_spesimen+'|'+ds.hasil+'"></td>'+
							'</tr>'
							);
					});
					if(dt.berobat_ke_rs=='1'){
						$('#berobat_ke_rs').iCheck('check');
						$('#riwayat_pengobatan_rs').val(dt.riwayat_pengobatan_rs).trigger('change');
						$('#trakeostomi').val(dt.trakeostomi).trigger('change');
					}
					if(dt.berobat_ke_puskesmas=='1'){
						$('#berobat_ke_puskesmas').iCheck('check');
						$('#riwayat_pengobatan_puskesmas').val(dt.riwayat_pengobatan_puskesmas).trigger('change');
					}
					if(dt.dokter_praktek_swasta=='1'){
						$('#dokter_praktek_swasta').iCheck('check');
					}
					if(dt.perawat_mantri_bidan=='1'){
						$('#perawat_mantri_bidan').iCheck('check');
					}
					if(dt.tidak_berobat=='1'){
						$('#tidak_berobat').iCheck('check');
					}
					$('#antibiotik').val(dt.antibiotik);
					$('#obat_lain').val(dt.obat_lain);
					$('#ads').val(dt.ads);
					$('#kondisi_kasus_saat_ini').val(dt.kondisi_kasus_saat_ini).trigger('change');
					$('#antibiotik').val(dt.antibiotik);
					$('#bepergian_dalam_2_minggu').val(dt.bepergian_dalam_2_minggu).trigger('change');
					$('#tujuan_bepergian_dalam_2_minggu').val(dt.tujuan_bepergian_dalam_2_minggu);
					$('#2_minggu_terakhir_berkunjung_ke_org_meninggal_gejala_sama').val(dt.dua_minggu_terakhir_berkunjung_ke_org_meninggal_gejala_sama).trigger('change');
					$('#tujuan_berkunjung_ke_org_meninggal_gejala_sama').val(dt.tujuan_berkunjung_ke_org_meninggal_gejala_sama);
					$('#2_minggu_terakhir_ada_tamu_gejala_sama').val(dt.dua_minggu_terakhir_ada_tamu_gejala_sama).trigger('change');
					$('#asal_tamu_gejala_sama').val(dt.asal_tamu_gejala_sama);
					$.each(dt.dtKontakKasus,function(kk,dk){
						$('#data_kontak_kasus tbody').append('<tr>'+
							'<td><center>'+dk.nama+'</center></td>'+
							'<td><center>'+dk.umur+'</center></td>'+
							'<td><center>'+dk.hub_dg_kasus+'</center></td>'+
							'<td><center>'+dk.status_imunisasi+'</center></td>'+
							'<td><center>'+dk.tgl_ambil_tenggorokan+'</center></td>'+
							'<td><center>'+dk.hasil_tenggorokan+'</center></td>'+
							'<td><center>'+dk.tgl_ambil_hidung+'</center></td>'+
							'<td><center>'+dk.hasil_hidung+'</center></td>'+
							'<td><center>'+dk.profilaksis+'</center></td>'+
							'<td><a href="javascript:void(0);" class="rm btn-sm btn-danger"><i class="fa fa-remove"></i></a>'+
							'<input type="hidden" name="dkk[kontak_kasus][]" value="'+dk.nama+'|'+dk.umur+'|'+dk.hub_dg_kasus+'|'+dk.status_imunisasi+'|'+dk.tgl_ambil_tenggorokan+'|'+dk.hasil_tenggorokan+'|'+dk.tgl_ambil_hidung+'|'+dk.hasil_hidung+'|'+dk.profilaksis+'"></td>'+
							'</tr>'
							);
					});
					$('.rm').on('click',function(){
						$(this).parent().parent().remove();
					});
				});
};
});
}
return false;
}

$(function(){
	$('select').on('change', function() { $(this).valid(); });
	generateData();

	$('.batal').on('click',function(){
		window.location.href = "{!! url('case/difteri#tab_4'); !!}";
		return false;
	});

	$('#form').validate({
		rules:{
			'drs[tgl_mulai_sakit]':'required',
		},
		messages:{
			'drs[tgl_mulai_sakit]':'Tanggal mulai sakit wajib di isi',
		},
		submitHandler: function(){
			var action = BASE_URL+'case/difteri/pe/store';
			var data = $('#form').serializeJSON();
			$.ajax({
				method  : "POST",
				url     : action,
				data    : JSON.stringify([data]),
				dataType: "json",
				beforeSend: function(){
					startProcess();
				},
				success: function(data, status){
					if (data.success==true) {
						window.location.href = '{!! url('case/difteri#tab_4'); !!}';
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
						endProcess();
					}
				}
			});
			return false;
		}
	});
});
</script>
@endsection
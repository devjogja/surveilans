@extends('layouts.base')
@section('content')
@if($dt = $data['response'])
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			<table class="table table-striped">
				<caption>Identitas pelapor</caption>
				<tbody>
					<tr>
						<td class="title">Nama</td>
						<td>{!! $dt['nama_pelapor'] !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Kantor</td>
						<td>{!! $dt['nama_kantor'] or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Jabatan</td>
						<td>{!! $dt['jabatan'] or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Provinsi</td>
						<td>{!! $dt['name_provinsi_pelapor'] or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Kabupaten</td>
						<td>{!! $dt['name_kabupaten_pelapor'] or '-' !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Laporan</td>
						<td>{!! $dt['tgl_laporan'] or '-' !!}</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="box-body">
			<table class="table table-striped">
				<caption>Identitas penderita</caption>
				<tbody>
					<tr>
						<td class="title">No.epidemologi</td>
						<td>{!! $dt['no_epid'] !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Penderita</td>
						<td>{!! $dt['name_pasien'] !!}</td>
					</tr>
					<tr>
						<td class="title">NIK</td>
						<td>{!! $dt['nik'] !!}</td>
					</tr>
					<tr>
						<td class="title">Nama Orang Tua</td>
						<td>{!! $dt['nama_ortu'] !!}</td>
					</tr>
					<tr>
						<td class="title">Jenis Kelamin</td>
						<td>{!! $dt['jenis_kelamin_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Tanggal Lahir</td>
						<td>{!! $dt['tgl_lahir'] !!}</td>
					</tr>
					<tr>
						<td class="title">Umur</td>
						<td>{!! $dt['umur_thn'] or 0 !!} Th {!! $dt['umur_bln'] or 0 !!} Th {!! $dt['umur_hari'] or 0 !!} Th</td>
					</tr>
					<tr>
						<td class="title">No.Telp</td>
						<td>{!! $dt['no_telp'] !!}</td>
					</tr>
					<tr>
						<td class="title">Tempat tinggal saat ini</td>
						<td>{!! $dt['tempat_tinggal_saat_ini'] !!}</td>
					</tr>
					<tr>
						<td class="title">Tempat tinggal sesuai kartu identitas/alamat asal</td>
						<td>{!! $dt['tempat_tinggal_sesuai_kartu'] !!}</td>
					</tr>
					<tr>
						<td class="title">Pekerjaan</td>
						<td>{!! $dt['pekerjaan'] !!}</td>
					</tr>
					<tr>
						<td class="title">Alamat tempat kerja</td>
						<td>{!! $dt['alamat_kerja'] !!}</td>
					</tr>
					<tr>
						<td class="title">Faskes tempat periksa/puskesmas</td>
						<td>{!! $dt['name_faskes'] !!}</td>
					</tr>
					<tr>
						<td class="title">Orang tua/saudara dekat yang dapat dihubungi</td>
						<td>
							<table class="table table-striped">
								<tbody>
									<tr>
										<td class="title">Nama</td>
										<td>{!! $dt['nama_saudara_yg_dihubungi'] !!}</td>
									</tr>
									<tr>
										<td class="title">Alamat tempat tinggal saat ini</td>
										<td>{!! $dt['alamat_ortu'].','.$dt['name_kelurahan_ortu'].','.$dt['name_kecamatan_ortu'].','.$dt['name_kabupaten_ortu'].','.$dt['name_provinsi_ortu'] !!}</td>
									</tr>
									<tr>
										<td class="title">Telepon</td>
										<td>{!! $dt['no_telp_ortu'] !!}</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="box-body">
			<table class="table table-striped">
				<caption>Riwayat sakit</caption>
				<tbody>
					<tr>
						<td class="title">Tanggal mulai sakit (demam)</td>
						<td>{!! $dt['tgl_mulai_sakit'] !!}</td>
					</tr>
					<tr>
						<td class="title">Keluhan utama yang mendorong untuk berobat?</td>
						<td>{!! $dt['keluhan_untuk_berobat'] !!}</td>
					</tr>
					<tr>
						<td class="title">Gejala dan tanda sakit</td>
						<td>
							<table class="table table-striped">
								@if(!empty($dt['dtGejala']))
								@foreach($dt['dtGejala'] AS $kg=>$vg)
								@if($vg->other_gejala)
								<tr>
									<td>{!! $vg->other_gejala !!}</td>
									<td>{!! $vg->val_gejala !!}</td>
								</tr>
								@else
								<tr>
									<td width="20%">{!! $vg->name !!}</td>
									<td>Tanggal Sakit, {!! $vg->tgl_kejadian !!}</td>
								</tr>
								@endif
								@endforeach
								@endif
							</table>
						</td>
					</tr>
					<tr>
						<td class="title">Status imunisasi difteri</td>
						<td>{!! $dt['status_imunisasi_difteri_txt'] !!}</td>
					</tr>
					<tr>
						<td class="title">Jika sudah pernah imunisasi DPT, sebutkan frekuensi dan tahun imunisasi</td>
						<td>
							<table class="table table-striped">
								<tr>
									<td>Berapa kali imunisasi</td>
									<td>{!! $dt['jml_imunisasi_dpt'] !!}</td>
								</tr>
								<tr>
									<td>Tahun Imunisasi</td>
									<td>{!! $dt['tahun_imunisasi_dpt'] !!}</td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="box-body">
			<table class="table table-striped">
				<caption>Data Spesimen</caption>
				<thead>
					<tr>
						<th>Jenis Spesimen</th>
						<th>Tanggal ambil spesimen</th>
						<th>Jenis Pemeriksaan</th>
						<th>Kode Spesimen</th>
						<th>Hasil</th>
					</tr>
				</thead>
				<tbody>
					@if(!empty($dt['dtSpesimen']))
					@foreach($dt['dtSpesimen'] AS $ks=>$vs)
					<tr>
						<td>{!! $vs->jenis_spesimen_txt !!}</td>
						<td>{!! $vs->tgl_ambil_spesimen !!}</td>
						<td>{!! $vs->jenis_pemeriksaan_txt !!}</td>
						<td>{!! $vs->kode_spesimen !!}</td>
						<td>{!! $vs->hasil_txt !!}</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="5">Tidak ada data spesimen</td>
					</tr>
					@endif
				</tbody>
			</table>
		</div>

		<div class="box-body">
			<table class="table table-striped">
				<caption>Riwayat pengobatan</caption>
				<tbody>
					<tr>
						<td class="title">Penderita berobat ke</td>
						<td>
							<table class="table table-striped">
								@if($dt['berobat_ke_rs']=='1')
								<tr>
									<td width="20%">Rumah Sakit</td>
									<td>
										<table class="table table-striped">
											<tr>
												<td width="20%">Dirawat</td>
												<td>{!! $dt['riwayat_pengobatan_rs_txt'] !!}</td>
											</tr>
											<tr>
												<td>Trakeostomi</td>
												<td>{!! $dt['trakeostomi_txt'] !!}</td>
											</tr>
										</table>
									</td>
								</tr>
								@endif
								@if($dt['berobat_ke_puskesmas']=='1')
								<tr>
									<td width="20%">Puskesmas</td>
									<td>
										<table class="table table-striped">
											<tr>
												<td width="20%">Dirawat</td>
												<td>{!! $dt['riwayat_pengobatan_puskesmas_txt'] !!}</td>
											</tr>
										</table>
									</td>
								</tr>
								@endif
								@if($dt['dokter_praktek_swasta']=='1')
								<tr>
									<td colspan="2">Praktek Swasta</td>
								</tr>
								@endif
								@if($dt['perawat_mantri_bidan']=='1')
								<tr>
									<td colspan="2">Perawat/mantri/bidan</td>
								</tr>
								@endif
								@if($dt['tidak_berobat']=='1')
								<tr>
									<td colspan="2">Tidak Berobat</td>
								</tr>
								@endif
							</table>
						</td>
					</tr>
					<tr>
						<td class="title">Antibiotik</td>
						<td>{!! $dt['antibiotik'] !!}</td>
					</tr>
					<tr>
						<td class="title">Obat lain</td>
						<td>{!! $dt['obat_lain'] !!}</td>
					</tr>
					<tr>
						<td class="title">ADS (Anti Difteri Serum)</td>
						<td>{!! $dt['ads'] !!}</td>
					</tr>
					<tr>
						<td class="title">Kondisi kasus saat ini</td>
						<td>{!! $dt['kondisi_kasus_saat_ini_txt'] !!}</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="box-body">
			<table class="table table-striped">
				<caption>Riwayat kontak</caption>
				<tbody>
					<tr>
						<td class="title">Dalam 2 minggu terakhir sebelum sakit apakah penderita bepergian</td>
						<td>{!! $dt['bepergian_dalam_2_minggu_txt'] !!}, Tujuan {!! $dt['tujuan_bepergian_dalam_2_minggu'] !!}</td>
					</tr>
					<tr>
						<td class="title">Dalam 2 minggu terakhir sebelum sakit apakah penderita pernah berkunjung ke rumah teman/saudara yang sakit/meninggal dengan gejala yang sama</td>
						<td>{!! $dt['2_minggu_terakhir_berkunjung_ke_org_meninggal_gejala_sama_txt'] !!}, Tujuan {!! $dt['tujuan_berkunjung_ke_org_meninggal_gejala_sama'] !!}</td>
					</tr>
					<tr>
						<td class="title">Dalam 2 minggu terakhir, apakah pernah menerima tamu dengan sakit dengan gejala yang sama</td>
						<td>{!! $dt['2_minggu_terakhir_ada_tamu_gejala_sama_txt'] !!}, Dari {!! $dt['asal_tamu_gejala_sama'] !!}</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="box-body">
			<table class="table table-striped">
				<caption>Kontak Kasus</caption>
				<thead>
					<tr>
						<th rowspan="3" valign="top"><center>Nama</center></th>
						<th rowspan="3" valign="top"><center>Umur</center></th>
						<th rowspan="3" valign="top"><center>Hubungan dengan kasus</center></th>
						<th rowspan="3" valign="top"><center>Status Imunisasi</center></th>
						<th colspan="4"><center>Data Spesimen</center></center></th>
						<th rowspan="3" valign="top"><center>Profilaksis</center></th>
					</tr>
					<tr>
						<th colspan="2"><center>Tenggorokan</center></th>
						<th colspan="2"><center>Hidung</center></th>
					</tr>
					<tr>
						<th><center>Tanggal ambil spesimen</center></th>
						<th><center>Hasil lab</center></th>
						<th><center>Tanggal ambil spesimen</center></th>
						<th><center>Hasil lab</center></th>
					</tr>
				</thead>
				<tbody>
					@if(!empty($dt['dtKontakKasus']))
					@foreach($dt['dtKontakKasus'] AS $kk=>$vk)
					<tr>
						<td><center>{!! $vk->nama !!}</center></td>
						<td><center>{!! $vk->umur !!}</center></td>
						<td><center>{!! $vk->hub_dg_kasus !!}</center></td>
						<td><center>{!! $vk->status_imunisasi !!}</center></td>
						<td><center>{!! $vk->tgl_ambil_tenggorokan !!}</center></td>
						<td><center>{!! $vk->hasil_tenggorokan !!}</center></td>
						<td><center>{!! $vk->tgl_ambil_hidung !!}</center></td>
						<td><center>{!! $vk->hasil_hidung !!}</center></td>
						<td><center>{!! $vk->profilaksis !!}</center></td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="9">Tidak ada data kontak kasus</td>
					</tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>
@endif
<div class="col-sm-12">
	<div class="footer">
		<a href="{!!URL::to('case/difteri#tab_4')!!}" class="btn btn-primary">Kembali</a>
	</div>
</div>
@endsection
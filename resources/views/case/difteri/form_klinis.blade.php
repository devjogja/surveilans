<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Data surveilans Difteri</h3>
	</div>
	<div class="form-horizontal">
		<div class="box-body">
			<div class="form-group">
				{!! Form::label('', 'Tanggal mulai sakit (demam)', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-4">
					<div class="input-group">
						{!! Form::text('dk[tgl_mulai_demam]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal mulai sakit (demam)','id'=>'tgl_mulai_demam','onchange'=>'getEpid();getAge();']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Imunisasi DPT sebelum sakit berapa kali', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					<div class="input-group">
						{!! Form::select('dk[jml_imunisasi_dpt]', array(null=>'--Pilih--','1'=>'1x','2'=>'2x','3'=>'3x','4'=>'4x','5'=>'5x','6'=>'6x','7'=>'Tidak','8'=>'Tidak tahu'), null, ['class' => 'form-control', 'id'=>'jml_imunisasi_dpt']) !!}
						<span class="input-group-addon al">(*)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Tanggal imunisasi difteri terakhir', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dk[tgl_imunisasi_difteri]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal imunisasi difteri terakhir','id'=>'tgl_imunisasi_difteri']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Tanggal pelacakan', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dk[tgl_pelacakan]', null, ['class' => 'form-control dates','placeholder'=>'Tanggal pelacakan','id'=>'tgl_pelacakan']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Gejala lain', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dk[gejala_lain]', null, ['class' => 'form-control','placeholder'=>'Gejala lain','id'=>'gejala_lain']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kontak', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-8">
					<div class="form-group">
						{!! Form::label(null, 'Jumlah', ['class' => 'col-sm-4 control-label']) !!}
						<div class="col-sm-5">
							{!! Form::text('dk[jml_kontak]', null, ['class' => 'form-control','placeholder'=>'Jumlah','id'=>'jml_kontak']) !!}
						</div>
					</div>
					<div class="form-group">
						{!! Form::label(null, 'Diambil spec (swab hidung)', ['class' => 'col-sm-4 control-label']) !!}
						<div class="col-sm-5">
							{!! Form::text('dk[diambil_spec_kontak]', null, ['class' => 'form-control','placeholder'=>'Diambil spec (swab hidung)','id'=>'diambil_spec_kontak']) !!}
						</div>
					</div>
					<div class="form-group">
						{!! Form::label(null, 'Positif', ['class' => 'col-sm-4 control-label']) !!}
						<div class="col-sm-5">
							{!! Form::text('dk[positif_kontak]', null, ['class' => 'form-control','placeholder'=>'Positif','id'=>'positif_kontak']) !!}
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Keadaan akhir', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[keadaan_akhir]', array(null=>'--Pilih--','1'=>'Hidup','2'=>'Meninggal'), null, ['class' => 'form-control', 'id'=>'keadaan_akhir']) !!}
				</div>
			</div>
			@if(Helper::role()->id_role == 6)
			<div class="form-group">
				{!! Form::label(null, 'Hot Case', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dk[hot_case]', array(null=>'--Pilih--','1'=>'Hot Case','2'=>'Bukan Hot Case'), null, ['class' => 'form-control', 'id'=>'hot_case']) !!}
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			@include('case.difteri.input_kasus')
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		getDetail();
	});

	function getDetail() {
		var id = $('#id_trx_case').val();
		var action = BASE_URL+'case/difteri/getDetail/'+id;
		$.getJSON(action, function(result){
			var raw = result.response;
			if (isset(raw)) {
				$.each(raw, function(k, dt){
					$('#id_faskes').val(dt.id_faskes);
					$('#id_role').val(dt.id_role);
					$('#jenis_input').val(dt.jenis_input);
					$('#id_trx_case').val(dt.id);
					$('#name').val(dt.name_pasien);
					$('#id_pasien').val(dt.id_pasien);
					$('#nik').val(dt.nik);
					$('#no_rm').val(dt.no_rm);
					$('#agama').val(dt.agama).trigger('change');
					$('#nama_ortu').val(dt.nama_ortu);
					$('#id_family').val(dt.id_family);
					$('#jenis_kelamin').val(dt.jenis_kelamin).trigger('change');
					if(isset(dt.tgl_lahir)){$('#tgl_lahir').val(dt.tgl_lahir).removeAttr('disabled');}
					if(isset(dt.umur_hari)){$('#umur_hari').val(dt.umur_hari).removeAttr('disabled');}
					if(isset(dt.umur_bln)){$('#umur_bln').val(dt.umur_bln).removeAttr('disabled');}
					if(isset(dt.umur_thn)){$('#umur_thn').val(dt.umur_thn).removeAttr('disabled');}
					$('#alamat').val(dt.alamat);
					$('#wilayah').val(dt.full_address);
					$('#id_kelurahan_pasien').val(dt.code_kelurahan_pasien);
					$('#id_kecamatan_pasien').val(dt.code_kecamatan_pasien);
					$('#id_kabupaten_pasien').val(dt.code_kabupaten_pasien);
					$('#id_provinsi_pasien').val(dt.code_provinsi_pasien);
					$('#no_epid').val(dt.no_epid);
					$('#no_epid_lama').val(dt.no_epid_lama);
					$('#tgl_mulai_demam').val(dt.tgl_mulai_demam);
					$('#jml_imunisasi_dpt').val(dt.jml_imunisasi_dpt).trigger('change');
					$('#tgl_imunisasi_difteri').val(dt.tgl_imunisasi_difteri);
					$('#tgl_pelacakan').val(dt.tgl_pelacakan);
					$('#gejala_lain').val(dt.gejala_lain);
					$('#jml_kontak').val(dt.jml_kontak);
					$('#diambil_spec_kontak').val(dt.diambil_spec_kontak);
					$('#positif_kontak').val(dt.positif_kontak);
					$('#hot_case').val(dt.hot_case).trigger('change');
					$('#keadaan_akhir').val(dt.keadaan_akhir).trigger('change');
					var dtSpesimen = dt.dtSpesimen;
					$.each(dtSpesimen, function(key, val){
						$('#spesimen tbody').append('<tr>'+
							'<td>'+val.jenis_spesimen_txt+'</td>'+
							'<td>'+val.tgl_ambil_spesimen+'</td>'+
							'<td>'+val.jenis_pemeriksaan_txt+'</td>'+
							'<td>'+val.kode_spesimen+'</td>'+
							'<td>'+val.hasil_txt+'</td>'+
							'<td><a href="javascript:void(0);" class="rm btn-sm btn-danger"><i class="fa fa-remove"></i></a>'+
							'<input type="hidden" name="ds[spesimen][]" value="'+val.jenis_spesimen+'|'+val.tgl_ambil_spesimen+'|'+val.jenis_pemeriksaan+'|'+val.kode_spesimen+'|'+val.hasil+'"></td>'+
							'</tr>'
							);
					});
					$('.rm').on('click',function(){
						$(this).parent().parent().remove();
						return false;
					});
					$('#klasifikasi_final').val(dt.klasifikasi_final).trigger('change');
				});
				return false;
			};
		});
	}
</script>

@endsection
<div class="alert alert-notif alert-dismissable">
	<i class="icon fa fa-warning"></i> Text input yang bertanda bintang (*) wajib di isi
</div>

<div class="row">
	{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form', 'class' => 'form-horizontal']) !!}
	{!! Form::hidden('dc[id_faskes]', Helper::role()->id_faskes,['id'=>'id_faskes']) !!}
	{!! Form::hidden('dc[id_role]', Helper::role()->id_role,['id'=>'id_role']) !!}
	{!! Form::hidden('dc[jenis_input]', '1',['id'=>'jenis_input']) !!}
	{!! Form::hidden('code_wilayah_faskes', Helper::role()->code_wilayah_faskes,['id'=>'code_wilayah_faskes']) !!}
	{!! Form::hidden('id_trx_case', $data['id_trx_case'], ['id'=>'id_trx_case']) !!}

	<div class="col-sm-6">
		@include('case.difteri.form_pasien')
	</div>
	<div class="col-sm-6">
		@include('case.difteri.form_klinis')
	</div>
	<div class="col-sm-12">
		@include('case.difteri.form_spesimen')
	</div>
	<div class="col-sm-12">
		@include('case.difteri.form_klasifikasi_final')
	</div>
	<div class="col-sm-12">
		<div class="footer">
			{!! Form::reset("Batal", ['class' => 'btn btn-warning batal']) !!}
			{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
		</div>
	</div>
	{!! Form::close() !!}
</div>

<script type="text/javascript">
	function getEpid() {
		var id_kelurahan = $('#id_kelurahan_pasien').val();
		var tgl_sakit = $('#tgl_mulai_demam').val();
		if(isset(id_kelurahan)){
			var url = BASE_URL+'case/difteri/getEpid';
			$.post(url, JSON.stringify({id_kelurahan:id_kelurahan, tgl_sakit:tgl_sakit}), function(data, status){
				if(isset(data.response)){
					$('#no_epid').val(data.response);
				}
			});
		}
		return false;
	}
	$(function(){
		$('.batal').on('click',function(){
			window.location.href = '{!! url('case/difteri'); !!}';
			return false;
		});
		$('#form').validate({
			rules:{
				'dp[name]':'required',
				'dp[jenis_kelamin]':'required',
				'df[name]':'required',
				'wilayah':'required',
				'dk[tgl_mulai_demam]':'required',
				'dk[jml_imunisasi_dpt]':'required',
			},
			messages:{
				'dp[name]':'Nama pasien wajib di isi',
				'dp[jenis_kelamin]':'Jenis kelamin wajib di isi',
				'df[name]':'Nama orang tua wajib di isi',
				'wilayah':'Wilayah wajib di isi',
				'dk[tgl_mulai_demam]':'Tanggal mulai demam wajib di isi',
				'dk[jml_imunisasi_dpt]':'Jumlah Imunisasi DPT Wajib di isi'
			},
			submitHandler: function(){
				var code_kelurahan = $('#id_kelurahan_pasien').val();
				if(isset(code_kelurahan)){
					var action = BASE_URL+'case/difteri/store';
					var data = $('#form').serializeJSON();
					$.ajax({
						method  : "POST",
						url     : action,
						data    : JSON.stringify([data]),
						dataType: "json",
						beforeSend: function(){
							startProcess();
						},
						success: function(data, status){
							if (data.success==true) {
								window.location.href = '{!! url('case/difteri'); !!}';
							}else{
								messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
								endProcess();
							}
						}
					});
				}else{
					alert('Alamat Pasien Wajib di isi dengan benar, tuliskan nama desa/kecamatan sampai muncul pilihan wilayah kemudian pilih salah satu.');
				}
				return false;
			}
		});
	});
</script>

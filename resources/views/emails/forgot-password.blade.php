<h3>{!! $user->first_name.' '.$user->last_name !!} yang terhormat,</h3>
<p>	Untuk dapat mengubah kata sandi silahkan klik tautan di bawah ini:</p>
<a href="{!! env('APP_URL') !!}/reset_password/{!! $user->email !!}/{!! $code !!}" title="">Ubah kata sandi</a>
</p>
<p>Demi keamanan akun Anda, segera lakukan penggantian kata sandi.</p>
<p>Tautan di atas hanya berlaku selama 1 x 24 jam.</p>
<br/>
<p>Salam,</p>
<p>Pengelola Sistem Informasi Surveilans PD3I</p>
--------------------------------------------------------------
<p>Email ini dikirim secara otomatis oleh sistem. Anda tidak perlu membalas atau mengirim email ke alamat ini.</p>
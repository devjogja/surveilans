@extends('layouts.base')
@section('content')
<div class="col-md-12">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Puskesmas</h3>
		</div>
		<div class="box-body">
			<table id="table_pkm" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Petugas</th>
						<th>Instansi</th>
						<th>Kabupaten</th>
						<th>Provinsi</th>
						<th>Jabatan</th>
						<th>No.Telp</th>
						<th>Email</th>
						@if (Helper::role()->id_role=='3')
						<th>Action</th>
						@endif
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Rumah sakit</h3>
		</div>
		<div class="box-body">
			<table id="table_rs" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Petugas</th>
						<th>Instansi</th>
						<th>Kabupaten</th>
						<th>Provinsi</th>
						<th>Jabatan</th>
						<th>No.Telp</th>
						<th>Email</th>
						@if (Helper::role()->id_role=='3')
						<th>Action</th>
						@endif
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Laboratorium</h3>
		</div>
		<div class="box-body">
			<table id="table_lab" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Petugas</th>
						<th>Instansi</th>
						<th>Jabatan</th>
						<th>No.Telp</th>
						<th>Email</th>
						@if (Helper::role()->id_role=='3')
						<th>Action</th>
						@endif
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Dinas Kesehatan Kabupaten</h3>
		</div>
		<div class="box-body">
			<table id="table_kab" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Petugas</th>
						<th>Instansi</th>
						<th>Provinsi</th>
						<th>Jabatan</th>
						<th>No.Telp</th>
						<th>Email</th>
						@if (Helper::role()->id_role=='3')
						<th>Action</th>
						@endif
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Dinas Kesehatan Provinsi</h3>
		</div>
		<div class="box-body">
			<table id="table_prov" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Petugas</th>
						<th>Instansi</th>
						<th>Jabatan</th>
						<th>No.Telp</th>
						<th>Email</th>
						@if (Helper::role()->id_role=='3')
						<th>Action</th>
						@endif
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Kementerian Kesehatan & WHO</h3>
		</div>
		<div class="box-body">
			<table id="table_pusat" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Petugas</th>
						<th>Instansi</th>
						<th>Jabatan</th>
						<th>No.Telp</th>
						<th>Email</th>
						@if (Helper::role()->id_role=='3')
						<th>Action</th>
						@endif
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		var tUrlPkm = '{!! url('user/getData/1'); !!}';
		var tDataPkm = [
		{ data: 'DT_Row_Index', searchable:false, orderable:false},
		{ data: 'full_name', name: 'full_name'},
		{ data: 'faskes_name', name: 'faskes_name'},
		{ data: 'name_kabupaten', name: 'name_kabupaten'},
		{ data: 'name_provinsi', name: 'name_provinsi'},
		{ data: 'jabatan', name: 'jabatan'},
		{ data: 'no_telp', name: 'no_telp'},
		{ data: 'email', name: 'email'},
		@if (Helper::role()->id_role=='3')
		{ data: 'action', searchable:false, orderable:false},
		@endif
		];
		$('#table_pkm').DataTable({
			paging: true,
			lengthChange: true,
			searching: true,
			ordering: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			ajax: {
				url     : tUrlPkm,
				type    : "POST",
				data 	: function(d){
					d.dt = JSON.stringify($('#filter').serializeJSON());
				},
			},
			columns: tDataPkm,
		});

		var tUrlRs = '{!! url('user/getData/2'); !!}';
		var tDataRs = [
		{ data: 'DT_Row_Index', searchable:false, orderable:false},
		{ data: 'full_name', name: 'full_name'},
		{ data: 'faskes_name', name: 'faskes_name'},
		{ data: 'name_kabupaten', name: 'name_kabupaten'},
		{ data: 'name_provinsi', name: 'name_provinsi'},
		{ data: 'jabatan', name: 'jabatan'},
		{ data: 'no_telp', name: 'no_telp'},
		{ data: 'email', name: 'email'},
		@if (Helper::role()->id_role=='3')
		{ data: 'action', searchable:false, orderable:false},
		@endif
		];
		$('#table_rs').DataTable({
			paging: true,
			lengthChange: true,
			searching: true,
			ordering: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			ajax: {
				url     : tUrlRs,
				type    : "POST",
				data 	: function(d){
					d.dt = JSON.stringify($('#filter').serializeJSON());
				},
			},
			columns: tDataRs,
		});
		var tUrlKabupaten = '{!! url('user/getData/6'); !!}';
		var tDataKabupaten = [
		{ data: 'DT_Row_Index', searchable:false, orderable:false},
		{ data: 'full_name', name: 'full_name'},
		{ data: 'faskes_name', name: 'faskes_name'},
		{ data: 'name_provinsi', name: 'name_provinsi'},
		{ data: 'jabatan', name: 'jabatan'},
		{ data: 'no_telp', name: 'no_telp'},
		{ data: 'email', name: 'email'},
		@if (Helper::role()->id_role=='3')
		{ data: 'action', searchable:false, orderable:false},
		@endif
		];
		$('#table_kab').DataTable({
			paging: true,
			lengthChange: true,
			searching: true,
			ordering: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			ajax: {
				url     : tUrlKabupaten,
				type    : "POST",
				data 	: function(d){
					d.dt = JSON.stringify($('#filter').serializeJSON());
				},
			},
			columns: tDataKabupaten,
		});
		var tUrlProvinsi = '{!! url('user/getData/5'); !!}';
		var tDataProvinsi = [
		{ data: 'DT_Row_Index', searchable:false, orderable:false},
		{ data: 'full_name', name: 'full_name'},
		{ data: 'faskes_name', name: 'faskes_name'},
		{ data: 'jabatan', name: 'jabatan'},
		{ data: 'no_telp', name: 'no_telp'},
		{ data: 'email', name: 'email'},
		@if (Helper::role()->id_role=='3')
		{ data: 'action', searchable:false, orderable:false},
		@endif
		];
		$('#table_prov').DataTable({
			paging: true,
			lengthChange: true,
			searching: true,
			ordering: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			ajax: {
				url     : tUrlProvinsi,
				type    : "POST",
				data 	: function(d){
					d.dt = JSON.stringify($('#filter').serializeJSON());
				},
			},
			columns: tDataProvinsi,
		});
		var tUrlPusat = '{!! url('user/getData/3'); !!}';
		var tDataPusat = [
		{ data: 'DT_Row_Index', searchable:false, orderable:false},
		{ data: 'full_name', name: 'full_name'},
		{ data: 'faskes_name', name: 'faskes_name'},
		{ data: 'jabatan', name: 'jabatan'},
		{ data: 'no_telp', name: 'no_telp'},
		{ data: 'email', name: 'email'},
		@if (Helper::role()->id_role=='3')
		{ data: 'action', searchable:false, orderable:false},
		@endif
		];
		$('#table_pusat').DataTable({
			paging: true,
			lengthChange: true,
			searching: true,
			ordering: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			ajax: {
				url     : tUrlPusat,
				type    : "POST",
				data 	: function(d){
					d.dt = JSON.stringify($('#filter').serializeJSON());
				},
			},
			columns: tDataPusat,
		});
		var tUrlPusat = '{!! url('user/getData/4'); !!}';
		var tDataPusat = [
		{ data: 'DT_Row_Index', searchable:false, orderable:false},
		{ data: 'full_name', name: 'full_name'},
		{ data: 'faskes_name', name: 'faskes_name'},
		{ data: 'jabatan', name: 'jabatan'},
		{ data: 'no_telp', name: 'no_telp'},
		{ data: 'email', name: 'email'},
		@if (Helper::role()->id_role=='3')
		{ data: 'action', searchable:false, orderable:false},
		@endif
		];
		$('#table_lab').DataTable({
			paging: true,
			lengthChange: true,
			searching: true,
			ordering: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			ajax: {
				url     : tUrlPusat,
				type    : "POST",
				data 	: function(d){
					d.dt = JSON.stringify($('#filter').serializeJSON());
				},
			},
			columns: tDataPusat,
		});
		$(".dataTables_filter").addClass('pull-right');
		$('#table_kab').on('draw.dt',function(){
			$(".resetpassword").unbind();
			$(".modal-yes").unbind();
			$('.resetpassword').on('click',function(){
				var action = $(this).attr('action');
				$('.modal-yes').attr('action',action);
				$('.content-modal').html('Anda yakin akan mereset password.?');
			});
			$('.modal-yes').on('click', function(){
				$('.modalalert').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					if(response.success){
						messageAlert('info', 'Berhasil.', 'Password berhasil di reset.');
						endProcess();
					}
					$('#table_kab').DataTable().draw();
				});
				return false;
			});
			$(".delete").unbind();
			$(".delete-yes").unbind();
			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					endProcess();
					if(response.success){
						messageAlert('info', 'Berhasil.', 'Data berhasil di hapus.');
					}
					$('#table_kab').DataTable().draw();
				});
				return false;
			});
		});
		$('#table_pkm').on('draw.dt',function(){
			$(".resetpassword").unbind();
			$(".modal-yes").unbind();
			$('.resetpassword').on('click',function(){
				var action = $(this).attr('action');
				$('.modal-yes').attr('action',action);
				$('.content-modal').html('Anda yakin akan mereset password.?');
			});
			$('.modal-yes').on('click', function(){
				$('.modalalert').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					if(response.success){
						messageAlert('info', 'Berhasil.', 'Password berhasil di reset.');
						endProcess();
					}
					$('#table_pkm').DataTable().draw();
				});
				return false;
			});
			$(".delete").unbind();
			$(".delete-yes").unbind();
			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					endProcess();
					if(response.success){
						messageAlert('info', 'Berhasil.', 'Data berhasil di hapus.');
					}
					$('#table_pkm').DataTable().draw();
				});
				return false;
			});
		});
		$('#table_rs').on('draw.dt',function(){
			$(".resetpassword").unbind();
			$(".modal-yes").unbind();
			$('.resetpassword').on('click',function(){
				var action = $(this).attr('action');
				$('.modal-yes').attr('action',action);
				$('.content-modal').html('Anda yakin akan mereset password.?');
			});
			$('.modal-yes').on('click', function(){
				$('.modalalert').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					if(response.success){
						messageAlert('info', 'Berhasil.', 'Password berhasil di reset.');
						endProcess();
					}
					$('#table_rs').DataTable().draw();
				});
				return false;
			});
			$(".delete").unbind();
			$(".delete-yes").unbind();
			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					endProcess();
					if(response.success){
						messageAlert('info', 'Berhasil.', 'Data berhasil di hapus.');
					}
					$('#table_rs').DataTable().draw();
				});
				return false;
			});
		});
		$('#table_prov').on('draw.dt',function(){
			$(".resetpassword").unbind();
			$(".modal-yes").unbind();
			$('.resetpassword').on('click',function(){
				var action = $(this).attr('action');
				$('.modal-yes').attr('action',action);
				$('.content-modal').html('Anda yakin akan mereset password.?');
			});
			$('.modal-yes').on('click', function(){
				$('.modalalert').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					if(response.success){
						messageAlert('info', 'Berhasil.', 'Password berhasil di reset.');
						endProcess();
					}
					$('#table_prov').DataTable().draw();
				});
				return false;
			});
			$(".delete").unbind();
			$(".delete-yes").unbind();
			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					endProcess();
					if(response.success){
						messageAlert('info', 'Berhasil.', 'Data berhasil di hapus.');
					}
					$('#table_prov').DataTable().draw();
				});
				return false;
			});
		});
		$('#table_pusat').on('draw.dt',function(){
			$(".resetpassword").unbind();
			$(".modal-yes").unbind();
			$('.resetpassword').on('click',function(){
				var action = $(this).attr('action');
				$('.modal-yes').attr('action',action);
				$('.content-modal').html('Anda yakin akan mereset password.?');
			});
			$('.modal-yes').on('click', function(){
				$('.modalalert').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					if(response.success){
						messageAlert('info', 'Berhasil.', 'Password berhasil di reset.');
						endProcess();
					}
					$('#table_pusat').DataTable().draw();
				});
				return false;
			});
			$(".delete").unbind();
			$(".delete-yes").unbind();
			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					endProcess();
					if(response.success){
						messageAlert('info', 'Berhasil.', 'Data berhasil di hapus.');
					}
					$('#table_pusat').DataTable().draw();
				});
				return false;
			});
		});
		$('#table_lab').on('draw.dt',function(){
			$(".resetpassword").unbind();
			$(".modal-yes").unbind();
			$('.resetpassword').on('click',function(){
				var action = $(this).attr('action');
				$('.modal-yes').attr('action',action);
				$('.content-modal').html('Anda yakin akan mereset password.?');
			});
			$('.modal-yes').on('click', function(){
				$('.modalalert').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					if(response.success){
						messageAlert('info', 'Berhasil.', 'Password berhasil di reset.');
						endProcess();
					}
					$('#table_lab').DataTable().draw();
				});
				return false;
			});
			$(".delete").unbind();
			$(".delete-yes").unbind();
			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					endProcess();
					if(response.success){
						messageAlert('info', 'Berhasil.', 'Data berhasil di hapus.');
					}
					$('#table_lab').DataTable().draw();
				});
				return false;
			});
		});
	});
</script>
@endsection
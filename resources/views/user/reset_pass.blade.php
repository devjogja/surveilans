@extends('layouts.base')
@section('content')
<div class="col-md-12">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
        </div>
        {!! Form::open(['method' => 'POST', 'id'=>'form' , 'class' => 'form-horizontal']) !!}
        <div class="box-body">
            <div class="form-group">
                {!! Form::label('password', 'Password', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-3">
                    <input class="form-control" id="password" name="password" type="password" />
                </div>
            </div>
			<div class="form-group">
				{!! Form::label('ulangi_password', 'Ulangi Password', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-3">
					<input class="form-control" id="ulangi_password" name="password_repeat" type="password" />
				</div>
			</div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-offset-4 col-md-8">
                    {!! Form::reset("Kembali", ['class' => 'btn btn-default back']) !!}
                    {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script type="text/javascript">
$(function() {
    $(".back").on('click', function() {
        window.location.href = BASE_URL;
    });
    $("#form").validate({
        rules: {
            'password': {
                required: true,
                minlength: 5
            },
            'password_repeat': {
                minlength: 5,
                equalTo: '#password'
            }
        },
        messages: {
            'password': {
                required: 'Password Wajib di isi',
                minlength: 'Password harus lebih dari 5 karakter'
            },
            'password_repeat': {
                minlength: 'Password harus lebih dari 5 karakter',
                equalTo: 'Password tidak sama'
            }
        },
        submitHandler: function() {
            var url = BASE_URL+"reset_password/{!! $data['email']; !!}/{!! $data['resetCode'] !!}";
            var data = $('#form').serializeJSON();
            $.ajax({
                    method: "POST",
                    url: url,
                    data: JSON.stringify(data),
                    dataType: "json",
                    beforeSend: function() {
                        startProcess();
                    },
                })
                .done(function(response) {
                    if (response.success == true) {
                        setTimeout(function() {
                            window.location.href = BASE_URL;
                        }, 100);
                    } else {
                        endProcess();
                        setTimeout(function() {
                            messageAlert('warning', 'Failed', response.response);
                        }, 100);
                    }
                });
            return false;
        }
    });
})
</script>
@endsection

@extends('layouts.base')
@section('content')
<script type="text/javascript">

      tinymce.init({
        selector: '#addKontenArea',
        height: 400,
        theme: 'modern',
        plugins: [
          'advlist autolink lists link image charmap print preview hr anchor pagebreak',
          'searchreplace wordcount visualblocks visualchars code fullscreen',
          'insertdatetime media nonbreaking save table contextmenu directionality',
          'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons |',
        image_advtab: true,
        content_css: [
          '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
          '//www.tinymce.com/css/codepen.min.css'
        ],
        automatic_uploads: true,
        paste_data_images: true,
        file_browser_callback_types: 'file image media',
        file_browser_callback: function(field_name, url, type, win) {
          console.log(field_name);
          var inp = $('<input id="tinymce-uploader'+field_name+'" type="file" name="pic" accept="image/*" style="display:none">');
          $(document.getElementById('form')).append(inp);
            $('#tinymce-uploader'+field_name).click();
            var input = $('#tinymce-uploader').get(0);
            inp.on("change",function(){
                var input = inp.get(0);
                var file = input.files[0];
                var fr = new FileReader();
                fr.onload = function() {
                    var img = new Image();
                    img.src = fr.result;
                    console.log(img.src);
                    document.getElementById(field_name).value = img.src;
                    inp.val('');
                }
                fr.readAsDataURL(file);
            });
        },
        // images_upload_base_path: '{{asset('images/uploaded/')}}',
      });
</script><div class="col-sm-12">
  <div class="box">
    <div class="box-header">
      <h4>Masukkan konten FAQ</h4>
    </div>
    <div class="box-body">

      {!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form', 'class' => 'form-horizontal']) !!}
        <div class="form-group">
          {!! Form::label(null, 'Judul FAQ', ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-8">
            {!! Form::textarea('', null, ['class' => 'form-control','id'=>'pertanyaan','placeholder'=>'Masukkan Judul FAQ (max 100)','maxlength' => 100,'size' => '30x2', ]) !!}
            {!! Form::hidden('id', $data->id, ['id'=>'id']) !!}
          </div>
        </div>
        <div class="form-group">
          {!! Form::label(null, 'Konten Pembelajaran Mandiri', ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-8">
            <textarea class="form-control" id="addKontenArea" type="text">{!! $data->jawaban !!}</textarea>
          </div>
        </div>
        <br>
        {!! Form::submit("Update", ['class' => 'btn btn-warning pull-right']) !!}
      <!-- {!! Form::button('Reset', ['class' => 'btn btn-danger pull-right','onclick'=>'pret()','id'=>'kembali']) !!} -->
      <a title="Kembali" href="{{ URL::to("help/faq") }}">{!! Form::button('Kembali', ['class' => 'btn btn-default pull-right','id'=>'kembali',]) !!}</a>

      {!! Form::close() !!}
    </div>
  </div>
</div>
<script type="text/javascript">
$(function(){
  var action = BASE_URL+'help/faq/getDetail/';

});
$('#pertanyaan').val("{{$data->pertanyaan}}");
$('#form').validate({
  submitHandler: function(){
    var action = BASE_URL+'help/faq/store';
    var data = [];
    data.push($('#id').val());
    data.push($('#pertanyaan').val());
    data.push(tinyMCE.get('addKontenArea').getContent());
    console.log(JSON.stringify(data));
    $.ajax({
      method  : "POST",
      url     : action,
      data    : JSON.stringify(data),
      // dataType: "json",
      beforeSend: function(){
        startProcess();
      },
      success: function(data, status){
        if (data.success==true) {
          endProcess();
          messageAlert('info', 'Berhasil.', 'Data berhasil disimpan.');
          setTimeout( function(){
            window.location.href = '{!! url('help/faq'); !!}';
          }, 100);
        }else{
          messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
          endProcess();
        }
      }
    });
    return false;
  }
});
</script>
@endsection

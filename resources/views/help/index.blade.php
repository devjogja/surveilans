@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab_1" data-toggle="tab">Frequently Asked Questions</a></li>
			@if($user=Sentinel::check())
			<li><a href="#tab_2" data-toggle="tab">Belajar Mandiri</a></li>
			<li><a href="#tab_3" data-toggle="tab">Hubungi Kami</a></li>
			@endif
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				@include('help.faq')
			</div>
			@if($user=Sentinel::check())
			<div class="tab-pane" id="tab_2">
				@include('help.course')
			</div>
			<div class="tab-pane" id="tab_3">
				@include('help.contact_us')
			</div>
			@endif
		</div>
	</div>
</div>
@endsection

<div class="box">
	@if(isset(Helper::role()->id_role))
	@if(Helper::role()->id_role=='3')
	<div class="box-header">
		<a href="{{ URL::to("help/faq/addFaq") }}">{!! Form::button('Tambah &nbsp <i class="fa fa-plus"></i>', ['class' => 'btn btn-success pull-right','data-toggle'=>'modal','id'=>'','data-target'=>'#modalAddCourse',]) !!}</a>
	</div>
	@endif
	@endif
	<div class="box-body">
		@foreach ($dataFaq as $dt)
		<div class="box box-success">
			<div class="box-header with-border" style="line-height:26px;background-color:#eeeeee">
				<h2 class="box-title"  style="font-weight:600">{!! $dt->pertanyaan !!}
				</h2>
				@if(isset(Helper::role()->id_role))
				@if(Helper::role()->id_role=='3')
				<a title="Hapus FAQ" data-toggle="tooltip" class="btn-sm btn-danger pull-right" onclick="deleteFaq({{$dt->id}})"><i class="fa fa-remove"></i></a>
				<a title="Perbaharui FAQ" href="{{ URL::to("help/faq/edit/$dt->id") }}" data-toggle="tooltip" class="btn-sm btn-warning pull-right"><i class="fa fa-pencil"></i></a>
				@endif
				@endif
			</div>
			<div class="form-horizontal" style="padding:3rem">
				{!! $dt->jawaban !!}
			</div>
		</div>
		@endforeach
	</div>
</div>

<script type="text/javascript">
	$('table').addClass('table table-bordered table-striped')
	$('#form').validate({
		rules:{
			'a[pertanyaan]':'required',
			'a[jawaban]':'required'
		},
		messages:{
			'a[pertanyaan]':'Pertanyaan wajib di isi',
			'a[jawaban]':'Jawaban wajib di isi'
		},
		submitHandler: function(){
			var action = BASE_URL+'help/faq/store';
			var data = $('#form').serializeJSON();
			$.ajax({
				method  : "POST",
				url     : action,
				data    : JSON.stringify(data),
				dataType: "json",
				beforeSend: function(){
					startProcess();
				},
				success: function(data, status){
					if (data.success==true) {
						$('#modalAddFaq').modal('hide');
						endProcess();
						messageAlert('info', 'Berhasil.', 'Data Faq berhasil disimpan.');
						setTimeout( function(){
							window.location.reload();
						}, 100);
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
						endProcess();
					}
				}
			});
			return false;
		}
	});
	function deleteFaq(id){
		var action = BASE_URL+'help/faq/delete/'+id;
		if(confirm("Apa anda yakin akan menghapus FAQ ini?")){
			$.ajax({
				method  : "POST",
				url     : action,
				data    : null,
				dataType: "json",
				beforeSend: function(){
					startProcess();
				},
			})
			.done(function(response){
				if(response.success){
					endProcess();
					messageAlert('info', 'Berhasil.', 'Data berhasil di hapus.');
					setTimeout( function(){
						window.location.reload();
					}, 100);
				}
			});
		}else{
			return false;
		}
	};
</script>

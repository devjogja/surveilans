<div class="box">
	@if(isset(Helper::role()->id_role))
	@if(Helper::role()->id_role=='3')
	<div class="box-header">
		<a href="{{ URL::to("help/course/addCourse") }}">{!! Form::button('Tambah &nbsp <i class="fa fa-plus"></i>', ['class' => 'btn btn-success pull-right','data-toggle'=>'modal','id'=>'','data-target'=>'#modalAddCourse',]) !!}</a>
	</div>
	@endif
	@endif
	<div class="box-body">
		@foreach ($dataCourse as $dt)
		<div class="box box-success">
			<div class="box-header with-border" style="line-height:26px;background-color:#eeeeee">
				<h2 class="box-title"  style="font-weight:600">{!! $dt->course_judul !!}
				</h2>
				@if(isset(Helper::role()->id_role))
				@if(Helper::role()->id_role=='3')
				<a title="Hapus Course" data-toggle="tooltip" class="btn-sm btn-danger pull-right" onclick="deleteCourse({{$dt->id}})"><i class="fa fa-remove"></i></a>
				<a title="Perbaharui Course" href="{{ URL::to("help/course/edit/$dt->id") }}" data-toggle="tooltip" class="btn-sm btn-warning pull-right"><i class="fa fa-pencil"></i></a>
				@endif
				@endif
			</div>
			<div class="form-horizontal" style="padding:3rem">
				{!! $dt->course_konten !!}
			</div>
		</div>
		@endforeach
	</div>
</div>
<script type="text/javascript">
	function deleteCourse(id){
		var action = BASE_URL+'help/course/delete/'+id;
		if(confirm("Apa anda yakin akan menghapus ini?")){
			$.ajax({
				method  : "POST",
				url     : action,
				data    : null,
				dataType: "json",
				beforeSend: function(){
					startProcess();
				},
			})
			.done(function(response){
				if(response.success){
					endProcess();
					messageAlert('info', 'Berhasil.', 'Data berhasil di hapus.');
					setTimeout( function(){
						window.location.reload();
					}, 100);
				}
			});
		}else{
			return false;
		}
	};
	function editCourse(id){
		var action = BASE_URL+'help/course/delete/'+id;
		$.ajax({
			method  : "POST",
			url     : action,
			data    : null,
			dataType: "json",
			beforeSend: function(){
				startProcess();
			},
		})
		.done(function(response){
			if(response.success){
				endProcess();
				messageAlert('info', 'Berhasil.', 'Data Faq berhasil di hapus.');
				setTimeout( function(){
					window.location.reload();
				}, 100);
			}
		});
	};
</script>

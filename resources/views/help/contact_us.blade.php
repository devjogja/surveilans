<div class="box">
  <div class="box-body">
    <h4>&nbsp Jika anda memiliki pertanyaan dapat menghubungi kami melalui form dibawah ini.</h4>
    </br>
    {!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form_contact', 'class' => 'form-horizontal']) !!}
    <div class="form-group">
      {!! Form::label(null, 'Nama', ['class' => 'col-sm-1 control-label']) !!}
      <div class="col-sm-6">
        {!! Form::text('cu[name]', null, ['class' => 'form-control','id'=>'pertanyaan','placeholder'=>'Masukkan Nama Anda']) !!}
      </div>
    </div>
    <div class="form-group">
      {!! Form::label(null, 'Email', ['class' => 'col-sm-1 control-label']) !!}
      <div class="col-sm-6">
        {!! Form::text('cu[email]', null, ['class' => 'form-control','id'=>'pertanyaan','placeholder'=>'Masukkan Email Anda']) !!}
      </div>
    </div>
    <div class="form-group">
      {!! Form::label(null, 'Pesan', ['class' => 'col-sm-1 control-label']) !!}
      <div class="col-sm-6">
        {!! Form::textarea('cu[pesan]', null, ['class' => 'form-control','id'=>'jawaban','placeholder'=>'Masukkan Pesan Anda (max 500)','maxlength' => 500]) !!}
      </div>
    </div>
    <div class="col-sm-7">
      {!! Form::submit("Kirim", ['class' => 'btn btn-success pull-right']) !!}
    </div>

    {!! Form::close() !!}
  </div>
<script type="text/javascript">
$('#form_contact').validate({
  rules:{
    'cu[name]':'required',
    'cu[email]':'required',
    'cu[pesan]':'required'
  },
  messages:{
    'cu[name]':'Nama wajib di isi',
    'cu[email]':'Email wajib di isi',
    'cu[pesan]':'Pesan wajib di isi'
  },
  submitHandler: function(){
    var action = BASE_URL+'help/contact/store';
    var data = $('#form_contact').serializeJSON();
    $.ajax({
      method  : "POST",
      url     : action,
      data    : JSON.stringify(data),
      dataType: "json",
      beforeSend: function(){
        startProcess();
      },
      success: function(data, status){
        if (data.success==true) {
          endProcess();
          messageAlert('info', 'Berhasil.', 'Pesan berhasil dikirimkan.');
          // setTimeout( function(){
          //   window.location.reload();
          // }, 100);
        }else{
          messageAlert('warning', 'Peringatan', 'Pesan gagal dikirimkan');
          endProcess();
        }
      }
    });
    return false;
  }
});</script>


</div>

@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">
			</h3>
		</div>
		{!! Form::open(['method' => 'POST', 'url' => 'register/post', 'id'=>'form' , 'class' => 'form-horizontal']) !!}
		<div class="box-body">
			<div class="form-group">
				{!! Form::label('first_name', 'Nama Depan', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('user[first_name]', null, ['class' => 'form-control','id'=>'first_name','placeholder'=>'Nama Depan']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('last_name', 'Nama Belakang', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('user[last_name]', null, ['class' => 'form-control','id'=>'last_name','placeholder'=>'Nama Belakang']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('instansi', 'Instansi', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('user_meta[instansi]', null, ['class' => 'form-control','id'=>'instansi','placeholder'=>'Instansi']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('jabatan', 'Jabatan', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('user_meta[jabatan]', null, ['class' => 'form-control','id'=>'jabatan','placeholder'=>'Jabatan']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::email('user[email]', null, ['class' => 'form-control','id'=>'email','placeholder'=>'Email']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('password', 'Password', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-3">
					<input class="form-control" id="password" name="user[password]" type="password" />
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('ulangi_password', 'Ulangi Password', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-3">
					<input class="form-control" id="ulangi_password" name="user[password_repeat]" type="password" />
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('no_telp', 'No.Telp/Hp', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-3">
					{!! Form::text('user_meta[no_telp]', null, ['class' => 'form-control','id'=>'no_telp','placeholder'=>'No.Telp/Hp']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('jenis_kelamin', 'Jenis Kelamin', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-3">
					{!! Form::select('user_meta[jenis_kelamin]', array(null=>'--Pilih--','L'=>'Laki-laki','P'=>'Perempuan'), null, ['class' => 'form-control', 'id'=>'jenis_kelamin']) !!}
				</div>
				{!! Form::hidden('id', null, ['class' => 'form-control','id'=>'idx']) !!}
			</div>
			<div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-offset-4 col-md-8">
							{!! Form::reset("Kembali", ['class' => 'btn btn-default back']) !!}
							{!! Form::reset("Batal", ['class' => 'btn btn-warning reset']) !!}
							{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
						</div>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function generateData() {
	var action = BASE_URL+'user/getData';
	$.getJSON(action, function(result){
		var dt = result.response[0];
		if (isset(dt)) {
			$('#idx').val(dt.id);
			$('#first_name').val(dt.first_name);
			$('#last_name').val(dt.last_name);
			$('#instansi').val(dt.instansi);
			$('#jabatan').val(dt.jabatan);
			$('#email').val(dt.email);
			$('#no_telp').val(dt.no_telp);
			$('#jenis_kelamin').val(dt.jenis_kelamin).select2();
		}
	});
}

	$(function(){
		generateData();
		$('#no_telp').mask('+6200000000000');
		$(".back").on('click',function () {
			window.location.href = BASE_URL;
		});
		$('.reset').on('click',function(){
			$('form').trigger("reset");
			$('select').select2('val',null);
		});
		$("#form").validate({
			rules:{
				'user[first_name]' : "required",
				'user[email]' : {
					required    : true,
					email       : true,
				},
				'user[password]' : {
					required    : true,
					minlength   : 5
				},
				'user[password_repeat]' : {
					minlength : 5,
					equalTo : '#password'
				},
				'user_meta[no_telp]' : 'required'
			},
			messages:{
				'user[first_name]' : "Nama Depan Wajib di isi",
				'user[email]' : {
					required    : 'Email Wajib di isi',
					email       : 'Isikan alamat email yang sesuai.',
				},
				'user[password]' : {
					required    : 'Password Wajib di isi',
					minlength   : 'Password harus lebih dari 5 karakter'
				},
				'user[password_repeat]' : {
					minlength   : 'Password harus lebih dari 5 karakter',
					equalTo     : 'Password tidak sama'
				},
				'user_meta[no_telp]' : 'No Telp Wajib di isi'
			},
			submitHandler: function(){
				var action  = $('#form').attr('action');
				var data = $('#form').serializeJSON();
				$.ajax({
					method  : "POST",
					url     : action,
					data    : JSON.stringify(data),
					dataType: "json",
					beforeSend: function(){
						startProcess();    
					},
				})
				.done(function(response){
					if(response.success==true){
						setTimeout( function(){
							window.location.href = BASE_URL;
						}, 100);
					}else{
						endProcess();
						setTimeout( function(){
							messageAlert('warning','Failed',response.response);
						}, 100);
					}
				});
				return false;
			}
		});
	})
</script>
@endsection
<nav class="navbar navbar-static-top">
	<div class="container">
		<div class="navbar-header">
			<a href="{{ URL::to('/') }}" class="navbar-brand"><b>SURVEILANS PD3I</b></a>
		</div>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				@if($user=Sentinel::check())
				<li class="dropdown notifications-menu">
					<a href="{{ URL::to('help/faq') }}"><b>FAQ DAN BELAJAR MANDIRI</b></a>
				</li>
				@if(!empty(Helper::role()->id_role))
				@if(in_array(Helper::role()->id_role, ['3']))
				<li class="dropdown notifications-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span class="hidden-xs"><b>MASTER DATA</b></span>
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li class="header"><b>WILAYAH</b></li>
						<li>
							<ul class="menu">
								<li><a href="{!! URL::to("district/provinsi"); !!}">PROVINSI</a></li>
								<li><a href="{!! URL::to("district/kabupaten"); !!}">KABUPATEN</a></li>
								<li><a href="{!! URL::to("district/kecamatan"); !!}">KECAMATAN</a></li>
								<li><a href="{!! URL::to("district/desa"); !!}">DESA</a></li>
							</ul>
						</li>
						<li class="header"><b>FASILITAS KESEHATAN</b></li>
						<li>
							<ul class="menu">
								<li><a href="{!! URL::to("faskes/laboratorium"); !!}">LABORATORIUM</a></li>
								<li><a href="{!! URL::to("faskes/rumahsakit"); !!}">RUMAH SAKIT</a></li>
								<li><a href="{!! URL::to("faskes/puskesmas"); !!}">PUSKESMAS</a></li>
								<li><a href="{!! URL::to("faskes/wilayah_kerja_puskesmas"); !!}">WILAYAH KERJA PUSKESMAS</a></li>
							</ul>
						</li>
					</ul>
				</li>
				@else
				<li class="dropdown notifications-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span class="hidden-xs"><b>MASTER DATA</b></span>
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li class="header"><b>FASILITAS KESEHATAN</b></li>
						<li style="height: 40px">
							<ul class="menu">
								<li><a href="{!! URL::to("faskes/wilayah_kerja_puskesmas"); !!}">WILAYAH KERJA PUSKESMAS</a></li>
							</ul>
						</li>
					</ul>
				</li>
				@endif
				<li class="dropdown notifications-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span class="hidden-xs"><b>PILIH KASUS LAIN</b></span>
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li class="header"><b>PILIH KASUS LAIN</b></li>
						<li>
							<ul class="menu">
								@if(Helper::role()->id_role=='4')
								@foreach(Helper::getCase() AS $key=>$val)
								@if($val->alias=='campak'||$val->alias=='crs'||$val->alias=='afp')
								<li><a href="{!! URL::to("lab/case/$val->alias") !!}">{!! strtoupper($val->name) !!}</a></li>
								@endif
								@endforeach
								@else
								@foreach(Helper::getCase() AS $key=>$val)
								<li><a href="{!! URL::to("case/$val->alias") !!}">{!! strtoupper($val->name) !!}</a></li>
								@endforeach
								@endif
							</ul>
						</li>
					</ul>
				</li>
				@else
				<li class="dropdown notifications-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span class="hidden-xs"><b>MASTER DATA</b></span>
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li class="header"><b>FASILITAS KESEHATAN</b></li>
						<li style="height: 40px">
							<ul class="menu">
								<li><a href="{!! URL::to("faskes/wilayah_kerja_puskesmas"); !!}">WILAYAH KERJA PUSKESMAS</a></li>
							</ul>
						</li>
					</ul>
				</li>
				@endif
				<li class="nav-user dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding: 10px;">
						<?php
						$iuser = (Helper::getUser()->jenis_kelamin=='L')?'user2.png':'user1.png';
						?>
						{!! Html::image('asset/images/'.$iuser,$user->first_name.' '.$user->last_name,array('class'=>'nav-avatar')) !!}
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li style="padding: 6px;"><a href="{!! url('user/edit'); !!}">{{ strtoupper($user->first_name).' '.strtoupper($user->last_name) }}</a></li>
						@if(!empty(Helper::role()->code_role))
						<?php
						$role = Helper::role();
						$roles = ($role->code_role=='puskesmas')?$role->name_role:'';
						?>
						<li style="padding: 6px;"><a href="{!! url('user/faskes/edit'); !!}">{{ strtoupper($roles).' '.strtoupper($role->name_faskes) }}</a></li>
						@endif
						<li class="divider"></li>
						<li style="padding: 6px;"><a href="{!! URL::to('logout') !!}">LOGOUT</a></li>
					</ul>
				</li>
				@else
				<li class="dropdown notifications-menu">
					<a href="{{ URL::to('faq') }}" style="margin:0 1rem 0 0" class="navbar-brand"><b>FAQ</b></a>
				</li>
				<li class="dropdown notifications-menu">
					<a href="{!! URL::to('register') !!}" class="navbar-brand"><b>SIGN UP</b></a>
				</li>
				@endif
			</ul>
		</div>
	</div>
</nav>

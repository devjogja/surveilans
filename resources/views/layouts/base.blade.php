<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Surveilans PD3I | {{ $title or 'Surveilans'}}</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	@include('layouts.css')
	@include('layouts.script')
</head>
<body class="hold-transition skin-green-light layout-top-nav">
	<div class="wrapper">
		<header class="main-header">
			@include('layouts.header')
		</header>
		<div class="content-wrapper">
			@include('layouts.custom')
			<section class="content-header">
				@include('layouts.content_header')
			</section>
			<section class="content">
				<div class="row">
					@yield('content')
				</div>
			</section>
		</div>
		<footer class="main-footer">
			@include('layouts.footer')
		</footer>
	</div>
	<!-- modal -->
	<div class="modal fade modalalert" tabindex="-1" role="dialog" aria-labelledby="">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<h5>Surveilans Peringatan</h5>
				</div>
				<div class="modal-body">
					<p class="content-modal"></p>
				</div>
				<div class="modal-footer">
					<a class="btn btn-default" data-dismiss="modal">Batal</a>
					<a action="" class="btn btn-primary modal-yes">Ya</a>
				</div>
			</div>
		</div>
	</div>
	<!-- end modal -->
	<!-- modal -->
	<div class="modal fade modaldelete" tabindex="-1" role="dialog" aria-labelledby="">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<h5>Surveilans Peringatan</h5>
				</div>
				<div class="modal-body">
					<p>
						Hapus data?
					</p>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" data-dismiss="modal">Batal</button>
					<a action="" class="btn btn-primary delete-yes">Hapus</a>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade modalmove" tabindex="-1" role="dialog" aria-labelledby="">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<h5>Surveilans Peringatan</h5>
				</div>
				<div class="modal-body">
					<p>
						Anda yakin akan mengirimkan data?
					</p>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" data-dismiss="modal">Batal</button>
					<a action="" class="btn btn-primary move-yes">Move</a>
				</div>
			</div>
		</div>
	</div>
	<!-- end modal -->
	<!-- flash message -->
	@if(Session::has('flash_message'))
	<?php $fmsg = Session::get('flash_message');?>
	<script type="text/javascript">
		$(function(){
			messageAlert("<?=$fmsg['status'];?>", "<?=$fmsg['title'];?>", "<?=$fmsg['message'];?>");
		})
	</script>
	@endif
</body>
</html>

<!-- Notifikasi Javascript -->
<div class="col-lg-4 notification">
    <div class="callout">
        <h4 class="title"></h4>
        <p class="message"></p>
    </div>
</div>
<!-- End Notifikasi Javascript -->
<!-- Progress Bars -->
<div id="progress-bar-box" class="progress-bar-box">
    <div class="progress active">
        <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
          <span class="sr-only"></span>
        </div>
    </div>
</div>
<!-- End Progress Bar -->

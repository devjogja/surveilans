<!-- Bootstrap 3.3.5 -->
{!! HTML::style('bootstrap/css/bootstrap.min.css') !!}
<!-- Font Awesome -->
{!! HTML::style('bootstrap/css/font-awesome.min.css') !!}
{!! HTML::style('asset/font-awesome.css') !!}
<!-- Ionicons -->
{!! HTML::style('bootstrap/css/ionicons.min.css') !!}
<!-- bootstrap inout -->
{!! HTML::style('plugins/bootstrap-input/fileinput.min.css') !!}
<!-- Select2 -->
{!! HTML::style('plugins/select2/select2.min.css') !!}
<!-- Theme style -->
{!! HTML::style('dist/css/AdminLTE.min.css') !!}
<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
{!! HTML::style('dist/css/skins/_all-skins.min.css') !!}
<!-- iCheck -->
{!! HTML::style('plugins/iCheck/flat/blue.css') !!}
<!-- jvectormap -->
{!! HTML::style('plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
<!-- Date Picker -->
{!! HTML::style('plugins/datepicker/css/bootstrap-datepicker3.standalone.css') !!}
{!! HTML::style('plugins/colorpicker/bootstrap-colorpicker.min.css') !!}
{!! HTML::style('plugins/timepicker/bootstrap-timepicker.min.css') !!}
<!-- Datatables -->
{!! HTML::style('plugins/datatables/jquery.dataTables.min.css') !!}
<!-- Daterange picker -->
{!! HTML::style('plugins/daterangepicker/daterangepicker-bs3.css') !!}
<!-- bootstrap wysihtml5 - text editor -->
{!! HTML::style('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
<!-- JStree -->
{!! HTML::style('jstree/themes/default/style.min.css') !!}
<!-- icheck -->
{!! HTML::style('plugins/iCheck/all.css') !!}
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements  -->
{!! HTML::style('asset/custom.css') !!}
{!! HTML::style('asset/select2_custom.css') !!}

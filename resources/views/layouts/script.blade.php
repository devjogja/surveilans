<!-- jQuery 2.1.4 -->
{!! HTML::script('plugins/jQuery/jQuery-2.1.4.min.js') !!}
<!-- jQuery UI 1.11.4 -->
{!! HTML::script('plugins/jQueryUI/jquery-ui.min.js') !!}
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script type="text/javascript">
	$.widget.bridge('uibutton', $.ui.button);
	var BASE_URL = {!! json_encode(url('/')) !!}+'/';
</script>
<!-- Bootstrap 3.3.5 -->
{!! HTML::script('bootstrap/js/bootstrap.min.js') !!}
<!-- Sparkline -->
{!! HTML::script('plugins/sparkline/jquery.sparkline.min.js') !!}
<!-- jvectormap -->
{!! HTML::script('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
{!! HTML::script('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
<!-- datepicker -->
{!! HTML::script('plugins/datepicker/js/bootstrap-datepicker.js') !!}
<!-- Bootstrap WYSIHTML5 -->
{!! HTML::script('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
<!-- FastClick -->
{!! HTML::script('plugins/fastclick/fastclick.min.js') !!}
<!-- AdminLTE App -->
{!! HTML::script('dist/js/app.min.js') !!}
<!-- proj4js -->
{!! HTML::script('dist/js/proj4.js') !!}
<!-- Maps script -->
{!! HTML::script('dist/js/maps_jml_case.js') !!}
{!! HTML::script('dist/js/maps_plot.js') !!}
<!-- DataTables -->
{!! HTML::script('plugins/datatables/jquery.dataTables.min.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.min.js') !!}
<!-- SlimScroll -->
{!! HTML::script('plugins/slimScroll/jquery.slimscroll.min.js') !!}
<!-- Jquery Block UI -->
{!! HTML::script('blockui/jquery.blockUI.js') !!}
<!-- Select2 -->
{!! HTML::script('plugins/select2/select2.full.min.js') !!}
<!-- InputMask -->
{!! HTML::script('plugins/input-mask/jquery.inputmask.js') !!}
{!! HTML::script('plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
{!! HTML::script('plugins/input-mask/jquery.inputmask.extensions.js') !!}
<!-- date-range-picker -->
{!! HTML::script('plugins/moment/moment.min.js') !!}
{!! HTML::script('plugins/daterangepicker/daterangepicker.js') !!}
<!-- bootstrap color picker -->
{!! HTML::script('plugins/colorpicker/bootstrap-colorpicker.min.js') !!}
<!-- bootstrap input -->
{!! HTML::script('plugins/bootstrap-input/purify.min.js') !!}
{!! HTML::script('plugins/bootstrap-input/sortable.min.js') !!}
{!! HTML::script('plugins/bootstrap-input/canvas-to-blob.min.js') !!}
{!! HTML::script('plugins/bootstrap-input/fileinput.min.js') !!}
<!-- bootstrap time picker -->
{!! HTML::script('plugins/timepicker/bootstrap-timepicker.min.js') !!}
<!-- iCheck 1.0.1 -->
{!! HTML::script('plugins/iCheck/icheck.min.js') !!}
<!-- jquery number masking -->
{!! HTML::script('plugins/masking/jquery.mask.min.js') !!}
<!-- validation -->
{!! HTML::script('plugins/validation/jquery.validate.js') !!}
<!-- jsTree -->
{!! HTML::script('jstree/jstree.min.js') !!}
<!-- icheck -->
{!! HTML::script('plugins/iCheck/icheck.min.js') !!}
{!! HTML::script('bootstrap/js/jquery.serializejson.js') !!}
{!! HTML::script('asset/custom.js') !!}
<!-- highcharts -->
{!! HTML::script('highcharts/js/highcharts.js') !!}
{!! HTML::script('highcharts/js/highcharts-3d.js') !!}
{!! HTML::script('highcharts/js/modules/exporting.js') !!}
{!! HTML::script('highcharts/js/modules/drilldown.js') !!}
<!-- tinymce -->
{!! HTML::script('plugins/tinymce/js/tinymce/tinymce.min.js') !!}
<!-- {!! HTML::script('plugins/tinymce/js/tinymce/jquery.tinymce.min.js') !!} -->
<!-- map indo -->

<div class="box box-success">
	{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'filter' , 'class' => 'form-horizontal']) !!}
	<div class="box-body">
		<div class="row">
			<div class="col-sm-6">
				<div class="box-body">
					<div class="form-group">
						<label for="input1" class="col-sm-3 control-label">Rentang Waktu</label>
						<div class="col-sm-5">
							{!! Form::select('filter[range]', array(null=>'All','1'=>'Hari','2'=>'Bulan','3'=>'Tahun'), null, ['class' => 'form-control', 'id'=>'range_time']) !!}
						</div>
					</div>
					<div class="form-group">
						<label for="input2" class="col-sm-3 control-label">Sejak</label>
						<div class="col-sm-2" style="padding-right: 0px;">
							<select name="from[day]" id="from_day" class="form-control" disabled="disabled">
								<option value="">--Pilih--</option>
								@for($i=1;$i<=31;$i++)
									<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
								@endfor
							</select>
						</div>
						<div class="col-sm-3" style="padding-right: 0px;">
							{!! Form::select('from[month]',
								array(null => '--Pilih--',
												'01'=>'Januari',
												'02'=>'Febuari',
												'03'=>'Maret',
												'04'=>'April',
												'05'=>'Mei',
												'06'=>'Juni',
												'07'=>'Juli',
												'08'=>'Agustus',
												'09'=>'September',
												'10'=>'Oktober',
												'11'=>'November',
												'12'=>'Desember',
												), null, ['class'=>'form-control','id'=>'from_month','disabled']) !!}
						</div>
						<div class="col-sm-3" style="padding-right: 0px;">
							<select name="from[year]" id="from_year" class="form-control" disabled="disabled">
								<option value="">--Pilih--</option>
								@for($i=2010;$i<=2020;$i++)
									<option value="{!! $i !!}">{!! $i !!}</option>
								@endfor
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">Sampai</label>
						<div class="col-sm-2" style="padding-right: 0px;">
							<select name="to[day]" id="to_day" class="form-control" disabled="disabled">
								<option value="">--Pilih--</option>
								@for($i=1;$i<=31;$i++)
									<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
								@endfor
							</select>
						</div>
						<div class="col-sm-3" style="padding-right: 0px;">
							{!! Form::select('to[month]',
								array(null => '--Pilih--',
												'01'=>'Januari',
												'02'=>'Febuari',
												'03'=>'Maret',
												'04'=>'April',
												'05'=>'Mei',
												'06'=>'Juni',
												'07'=>'Juli',
												'08'=>'Agustus',
												'09'=>'September',
												'10'=>'Oktober',
												'11'=>'November',
												'12'=>'Desember',
												), null, ['class'=>'form-control','id'=>'to_month','disabled']) !!}
						</div>
						<div class="col-sm-3" style="padding-right: 0px;">
							<select name="to[year]" id="to_year" class="form-control" disabled="disabled">
								<option value="">--Pilih--</option>
								@for($i=2010;$i<=2020;$i++)
									<option value="{!! $i !!}">{!! $i !!}</option>
								@endfor
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="box-body">
					<div class="form-group">
						<label for="input1" class="col-sm-3 control-label">Provinsi</label>
						<div class="col-sm-5">
							{!! Form::select('district[id_provinsi]', array(null=>'Pilih Laboratorium')+Helper::getLaboratorium(), null, ['class' => 'form-control','onchange'=>"getKabupaten('_filter')",'id'=>'id_provinsi_filter']) !!}
						</div>
					</div>
					<div class="form-group">
						<label for="input1" class="col-sm-3 control-label">Provinsi</label>
						<div class="col-sm-5">
							{!! Form::select('district[id_provinsi]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control','onchange'=>"getKabupaten('_filter')",'id'=>'id_provinsi_filter']) !!}
						</div>
					</div>
					<div class="form-group">
						<label for="input1" class="col-sm-3 control-label">Kabupaten</label>
						<div class="col-sm-5">
							{!! Form::select('district[id_kabupaten]', array(null=>'Pilih Kabupaten'), null, ['class' => 'form-control','onchange'=>"getKecamatan('_filter')",'id'=>'id_kabupaten_filter']) !!}
						</div>
					</div>
					<div class="form-group">
						<label for="input1" class="col-sm-3 control-label">Kecamatan</label>
						<div class="col-sm-5">
							{!! Form::select('district[id_kecamatan]', array(null=>'Pilih Kecamatan'), null, ['class' => 'form-control','onchange'=>"getPuskesmas('_filter')",'id'=>'id_kecamatan_filter']) !!}
						</div>
					</div>
					<div class="form-group">
						<label for="input1" class="col-sm-3 control-label">Puskesmas</label>
						<div class="col-sm-5">
							{!! Form::select('district[id_puskesmas]', array(null=>'Pilih Puskesmas'), null, ['class' => 'form-control','id'=>'puskesmas_filter']) !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer" style="text-align: center">
		{!! Form::submit("Tampilkan", ['class' => 'btn btn-success','id'=>'submit_filter']) !!}
		{!! Form::submit("Export Excel", ['class' => 'btn btn-info','id'=>'export_excel','disabled']) !!}
	</div>
	{!! Form::close() !!}
</div>

<script type="text/javascript">
	$(function(){
		$('#puskesmas_filter').on('change',function(){
			var val = $(this).val();
			var id_faskes = "{!! Helper::role()->id_faskes !!}";
			if(val==id_faskes){
				$('#export_excel').removeAttr('disabled');
			}else{
				$('#export_excel').attr('disabled','disabled');
			}
			return false;
		});
		$('#range_time').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#from_day').removeAttr('disabled');
				$('#from_month').removeAttr('disabled');
				$('#from_year').removeAttr('disabled');
				$('#to_day').removeAttr('disabled');
				$('#to_month').removeAttr('disabled');
				$('#to_year').removeAttr('disabled');
			}else if(val=='2'){
				$('#from_day').attr('disabled','disabled').select2('val',null);
				$('#from_month').removeAttr('disabled');
				$('#from_year').removeAttr('disabled');
				$('#to_day').attr('disabled','disabled').select2('val',null);
				$('#to_month').removeAttr('disabled');
				$('#to_year').removeAttr('disabled');
			}else if(val=='3'){
				$('#from_day').attr('disabled','disabled').select2('val',null);
				$('#from_month').attr('disabled','disabled').select2('val',null);
				$('#from_year').removeAttr('disabled');
				$('#to_day').attr('disabled','disabled').select2('val',null);
				$('#to_month').attr('disabled','disabled').select2('val',null);
				$('#to_year').removeAttr('disabled');
			}else{
				$('#from_day').attr('disabled','disabled').select2('val',null);
				$('#from_month').attr('disabled','disabled').select2('val',null);
				$('#from_year').attr('disabled','disabled').select2('val',null);
				$('#to_day').attr('disabled','disabled').select2('val',null);
				$('#to_month').attr('disabled','disabled').select2('val',null);
				$('#to_year').attr('disabled','disabled').select2('val',null);
			}
			return false;
		});
	})
</script>

@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab_1" data-toggle="tab">Cari Pasien</a></li>
			<li><a href="#tab_2" data-toggle="tab">Daftar Sampel</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div id='caripasien'>
					@include('lab.case.crs.cari_pasien')
				</div>
				<div id="tambahpasien" style="display:none;">
					@include('lab.case.crs.input_kasus')
				</div>
			</div>
			<div class="tab-pane" id="tab_2">
				{{-- @include('lab.case.crs.daftar_kasus') --}}
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		var url = document.location.toString();
		if (url.match('#')) {
			$('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
		}
	});
</script>
@endsection

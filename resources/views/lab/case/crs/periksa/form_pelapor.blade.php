<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Data pelapor</h3>
	</div>
	<div class="form-horizontal">
		@if($role = Helper::role())
		<div class="box-body">
			<div class="form-group">

				{!! Form::label(null, 'Provinsi', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::select('dpel[code_provinsi_pelapor]', array(null=>'Pilih Provinsi')+Helper::getProvince(), $role->code_provinsi_faskes, ['class' => 'form-control','id'=>'id_provinsi_pelapor','onchange'=>"getKabupaten('_pelapor')"]) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Nama Faskes', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-5">
					{!! Form::text('dpel[nama_pelapor]', strtoupper($role->name_role).' '.$role->name_faskes, ['class' => 'form-control','placeholder'=>'Nama pelapor','id'=>'nama_pelapor']) !!}
				</div>
			</div>
			
		</div>
		@endif
	</div>
</div>

@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab_1" data-toggle="tab">Pemeriksaan Lab CRS</a></li>
		</ul>
		<div class="tab-content">
			<div class="alert alert-notif alert-dismissable">
				<i class="icon fa fa-warning"></i> Text input yang bertanda bintang (*) wajib di isi
			</div>

			<div class="row">
				{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form', 'class' => 'form-horizontal']) !!}
				{!! Form::hidden('id_trx_case', $data['id_trx_case'], ['id'=>'id_trx_case']) !!}

				<div class="col-sm-12">
						@include('lab.case.crs.periksa.form_pelapor')
						@include('case.crs.form_pasien')
				</div>
				<div class="col-sm-12">
					@include('lab.case.crs.periksa.form_lab_rs')
				</div>
				<div class="col-sm-12">
					@include('lab.case.crs.periksa.form_lab')
				</div>
				<div class="col-sm-12">
					@include('case.crs.form_klasifikasi_final')
				</div>
				<div class="col-sm-12">
					<div class="footer">
						{!! Form::reset("Batal", ['class' => 'btn btn-warning batal']) !!}
						{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		var url = document.location.toString();
		if (url.match('#')) {
			$('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
		}
	});
</script>
@endsection

<div class="alert alert-notif alert-dismissable">
  <i class="icon fa fa-warning"></i> Text input yang bertanda bintang (*) wajib di isi
</div>

<div class="row">
  {!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form', 'class' => 'form-horizontal']) !!}
  {!! Form::hidden('id_trx_case', $data['id_trx_case'], ['id'=>'id_trx_case']) !!}

  <div class="col-sm-12">
    @include('lab.case.afp.periksa.form_pengumpulan_spesimen')
  </div>

  <div class="col-sm-12">
    <div class="footer">
      {!! Form::reset("Batal", ['class' => 'btn btn-warning batal']) !!}
      {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
    </div>
  </div>
  {!! Form::close() !!}
</div>

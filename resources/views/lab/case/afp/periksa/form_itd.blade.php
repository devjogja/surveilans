  <div class='form-horizontal' style="display:none" id="form_itd">
    <div class="box-body">
      <h3>ITD</h3>
      <h4>Data Isolat</h4>
      <div class="form-group">
      {!! Form::open(['method' => 'POST', 'url' => '', 'id'=>"form_pemeriksaan_itd" , 'class' => 'form-horizontal']) !!}
        {!! Form::hidden('id', null, ['class' => 'form-control','id'=>'pi_id']) !!}
        {!! Form::label('nik', 'Kode Laboratorium', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('ds[code_lab_isolat]', null, ['class' => 'form-control','id'=>'code_lab_isolat','placeholder'=>'Kode Laboratorium']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Tanggal Isolat pertama diterima di laboratorium', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('ds[tgl_isolat_pertama_diterima]', null, ['class' => 'form-control datemax','id'=>'tgl_isolat_pertama_diterima','placeholder'=>'Tanggal Isolat pertama diterima di laboratorium']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Dirujuk', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::select('ds[dirujuk_isolat]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'),null, ['class' => 'form-control select','id'=>'dirujuk_isolat',]) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Alasan dirujuk', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::select('ds[alasan_rujuk_isolat]', array(null=>'--Pilih--','1'=>'Banking','2'=>'Sequencing','3'=>'Discordant','4'=>'ITD','5'=>'SEQ (SP) + Banking','6'=>'SEQ (SP) + ITD (SP)','7'=>'Banking (SP)+ITD'),null, ['class' => 'form-control select','id'=>'alasan_rujuk_isolat','disabled']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Laboratorium rujukan', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::select('ds[lab_rujuk_isolat]', array(null=>'--Pilih--','1'=>'INO,Bandung','2'=>'INO,Jakarta','3'=>'INO,Surabaya'),null, ['class' => 'form-control select','placeholder'=>'Laboratorium rujukan','id'=>'lab_rujuk_isolat','disabled']) !!}
        </div>
      </div>
      <br>
      @include('lab.case.afp.periksa.form_arm1')
      @include('lab.case.afp.periksa.form_arm2')
      <hr>
      <div class="form-group">
        {!! Form::label(null, 'Hasil final pemeriksaan spesimen ITD', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
          <div class="form-group">
            {!! Form::label(null, 'P1', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::select('ds[hasil_isolat_p1]', array(null=>'--Pilih--','1'=>'Wild','2'=>'Sabin','3'=>'Mixture of wild and sabin','4'=>'VDPV','5'=>'Discordant ref for sequencing','6'=>'Pending ITD','7'=>'Negative','8'=>'Invalid Ref To GSL','9'=>'Other'),null, ['class' => 'form-control select','id'=>'hasil_isolat_p1',]) !!}
            </div>
          </div>
          <div class="form-group">
            {!! Form::label(null, 'P2', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::select('ds[hasil_isolat_p2]', array(null=>'--Pilih--','1'=>'Wild','2'=>'Sabin','3'=>'Mixture of wild and sabin','4'=>'VDPV','5'=>'Discordant ref for sequencing','6'=>'Pending ITD','7'=>'Negative','8'=>'Invalid Ref To GSL','9'=>'Other'),null, ['class' => 'form-control select','id'=>'hasil_isolat_p2',]) !!}
            </div>
          </div>
          <div class="form-group">
            {!! Form::label(null, 'P3', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::select('ds[hasil_isolat_p3]', array(null=>'--Pilih--','1'=>'Wild','2'=>'Sabin','3'=>'Mixture of wild and sabin','4'=>'VDPV','5'=>'Discordant ref for sequencing','6'=>'Pending ITD','7'=>'Negative','8'=>'Invalid Ref To GSL','9'=>'Other'),null, ['class' => 'form-control select','id'=>'hasil_isolat_p3',]) !!}
            </div>
          </div>
          <div class="form-group">
            {!! Form::label(null, 'NPEV by PCR', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::select('ds[hasil_isolat_npev]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak dilakukan'),null, ['class' => 'form-control select','id'=>'hasil_isolat_npev',]) !!}
            </div>
          </div>
          <div class="form-group">
            {!! Form::label(null, 'NEV', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::select('ds[hasil_isolat_nev]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak','3'=>'Tidak dilakukan'),null, ['class' => 'form-control select','id'=>'hasil_isolat_nev',]) !!}
            </div>
          </div>
        </div>
      </div>
      <br>
      <div class="form-group">
        {!! Form::label(null, 'Tanggal hasil pemeriksaan ITD tersedia', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('dc[tgl_hasil_isolat_tersedia]', null, ['class' => 'form-control datemax','id'=>'tgl_hasil_isolat_tersedia','placeholder'=>'Tanggal hasil pemeriksaan ITD tersedia']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Tanggal hasil ITD dikirim dari laboratorium ke EPI', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('dc[tgl_hasil_itd_dikirim_ke_epi]', null, ['class' => 'form-control datemax','id'=>'tgl_hasil_itd_dikirim_ke_epi','placeholder'=>'Tanggal hasil ITD dikirim dari laboratorium ke EPI']) !!}
        </div>
      </div>
      <div class="row">
        <div class="col-md-offset-4 col-md-8">
          <a class="btn btn-warning" id='tutup_pemeriksaan_itd'>Tutup</a>
          <!-- <a class="btn btn-success" id="tambah_pemeriksaan_itd">Simpan</a> -->
          {!! Form::submit("Simpan", ['class' => 'btn btn-success','id'=>"submit_form_pemeriksaan_itd"]) !!}
          {!! Form::close() !!}
        </div>
      </div>
			</div>
    </div>
<script type="text/javascript">
  $(function(){
    $('#form_pemeriksaan_itd').validate({
      rules:{
        'ds[dilakukan_sequencing]':'required',
        'ds[id_sequencing]'       :'required',
        'ds[hasil_final_kultur]'  :'required',
      },
      messages:{
        'ds[dilakukan_sequencing]':'Data wajib diisi',
        'ds[id_sequencing]'       :'Data wajib diisi',
        'ds[hasil_final_kultur]'  :'Data wajib diisi',
      },
      submitHandler: function(){
        var action = BASE_URL+'api/analisa/'+kasus;
        // $.ajax({
        //   method  : "POST",
        //   url     : action,
        //   data    : JSON.stringify(senddata),
        //   beforeSend: function(){
        //     startProcess();
        //   },
        //   success: function(data){
        //     if (data.success==true) {
        //       endProcess();
        //       var dt=data.response;
              var id=$('#pi_id').val();
              $('#input_pemeriksaan').hide();
              $('#form_itd').hide();
              $('.form-control').val(null);
              $('.select').select2('val',null);
              $('#itd_'+id).removeClass("btn-info");
              $('#itd_'+id).addClass("btn-danger");
              $('#itd_'+id).attr('disabled','disabled');
        //     }else{
        //       messageAlert('warning', 'Peringatan', 'Data gagal salah');
        //       endProcess();
        //     }
        //   }
        // });
        // return false;
      }
    });
    $('#tutup_pemeriksaan_itd').on('click',function(){
      var id=$('#pi_id').val();
      $('#input_pemeriksaan').hide();
      $('#form_itd').hide();
      $('.form-control').val(null);
      $('.select').select2('val',null);
      $('#itd_'+id).removeClass("btn-info");
      $('#itd_'+id).addClass("btn-danger");
      $('#itd_'+id).attr('disabled','disabled');
      return false;
    });
    $('#dirujuk_isolat').on('change',function(){
      var val = $(this).val();
      if(val=='1'){
        $('#alasan_rujuk_isolat').removeAttr('disabled');
        $('#lab_rujuk_isolat').removeAttr('disabled');
      }else{
        $('#alasan_rujuk_isolat').attr('disabled','disabled').select2('val',null);
        $('#lab_rujuk_isolat').attr('disabled','disabled').select2('val',null);
      }
      return false;
    });
  });
</script>

<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Pengumpulan spesimen</h3>
  </div>
  <div class='form-horizontal'>
    <div class="box-body">
      <div class="form-group">
        {!! Form::label(null, 'Apakah spesimen diambil', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-2">
          {!! Form::select('ds[pengambilan_spesimen]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'pengambilan_spesimen']) !!}
        </div>
      </div>
      <table class="table table-striped table-bordered" id="pengumpulan_spesimen">
				<thead style="background: #4caf50;color: white;">
					<tr>
						<th>Spesimen</th>
						<th>Tanggal</th>
						<th>Kondisi Spesimen diterima di Provinsi</th>
            <th>Apa Dilakukan Pengambilan Ulang Spesimen</th>
            <th>Aksi</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			<div style="margin: 14px 0px;">
				<button type="button" class="btn btn-success btn-flat" id="add_pengumpulan_spesimen"><i class="fa fa-plus-circle"></i> Tambah Spesimen</button>
			</div>
			<div id="div_sampel_pengumpulan" style="margin-top: 2%">
				<table class="table table-bordered">
					<tr>
						<th>Spesimen</th>
						<th style="width:27%">Tanggal</th>
            <th>Kondisi Spesimen diterima di Provinsi</th>
						<th>Apa Dilakukan Pengambilan Ulang Spesimen</th>
					</tr>
					<tr>
						<td>{!! Form::text(null, null, ['class' => 'form-control','id'=>'spesimen_ke','placeholder'=>'Spesimen Ke-']) !!}</td>
						<td>
							<table>
								<tr><td>Tanggal Kirim ke kabupaten/kota</td>
									<td>{!! Form::text(null, null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Kirim ke kabupaten/kota','id'=>'tgl_kirim_kab']) !!}</td>
								</tr>
								<tr>
									<td>Tanggal Kirim ke provinsi</td>
									<td>{!! Form::text(null, null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Kirim ke provinsi','id'=>'tgl_kirim_prov']) !!}</td>
								</tr>
								<tr>
									<td>Tanggal Kirim ke laboratorium</td>
									<td>{!! Form::text(null, null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Kirim ke laboratorium','id'=>'tgl_kirim_lab']) !!}</td>
								</tr>
							</table>
						</td>
            <td>{!! Form::select(null, array(null=>'--Pilih--','1'=>'Baik','2'=>'Volume Kurang ','3'=>'Tidak Dingin','4'=>'Kering','5'=>'Pot Bocor','6'=>'Tidak Baik'),null, ['class' => 'form-control','placeholder'=>'Kondisi spesimen diterima di provinsi','id'=>'kondisi',]) !!}</td>
						<td>
              <table>
              <tr>
                <td colspan="2" style="width:100%">{!! Form::select(null, array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'),null, ['class' => 'form-control','placeholder'=>'Pengambilan Ulang','id'=>'pengambilan_ulang',]) !!}</td>
              </tr>
              <tr><td style="width:20%">Alasan</td>
                <td>{!! Form::textarea(null, null, ['class' => 'form-control','id'=>'alasan_spesimen_tidak_diambil','placeholder'=>'Alasan spesimen tidak diambil','rows'=>'3','disabled']) !!}</td>
              </tr>
            </table></td>
					</tr>
				</table>
				<div class="row">
					<div class="col-md-offset-4 col-md-8">
						<button class="btn btn-default" id='tutup_sampel_pengumpulan'>Tutup</button>
						<button class="btn btn-success" id="tambah_spesimen_pengumpulan">Tambah</button>
					</div>
				</div>
			</div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(function(){
  $('#div_sampel_pengumpulan').hide();
  $('#tutup_sampel_pengumpulan').on('click',function(){
    $('#div_sampel_pengumpulan').hide(500);
    $('#add_pengumpulan_spesimen').show();
    return false;
  });
  $('#add_pengumpulan_spesimen').on('click',function(){
    $('#div_sampel_pengumpulan').show(500);
    $('#add_pengumpulan_spesimen').hide();
    return false;
  });
  $('#pengambilan_ulang').on('change',function(){
    var val = $(this).val();
    if (val==1) {
      $('#alasan_spesimen_tidak_diambil').removeAttr('disabled');
    }else{
      $('#alasan_spesimen_tidak_diambil').attr('disabled','disabled');
    }
    return false;
  });


  $('#tambah_spesimen_pengumpulan').on('click',function(){
    var s = $('#spesimen_ke').val();
    var tak = $('#tgl_kirim_kab').val();
    var tap = $('#tgl_kirim_prov').val();
    var tal = $('#tgl_kirim_lab').val();
    var kt = $('#kondisi option:selected').text()
    var k = $('#kondisi').val();
    var p = $('#pengambilan_ulang').val();
    var pt = $('#pengambilan_ulang option:selected').text();
    var a = $('#alasan_spesimen_tidak_diambil').val();
    $('#pengumpulan_spesimen').append('<tr>'+
      '<td class="text-center">'+s+'</td>'+
      '<td><table class="table table-bordered"><tr><td style="width:60%">Tanggal Kirim ke Kabupaten/Kota</td><td>'+tak+'</td></tr>'+
      '<tr><td>Tanggal Kirim ke Provinsi</td><td>'+tap+'</td></tr>'+
      '<tr><td>Tanggal Kirim ke Laboratorium</td><td>'+tal+'</td></tr></table></td>'+
      '<td>'+kt+'</td>'+
      '<td><table class="table table-bordered"><tr><td colspan="2" style="width:100%">'+pt+'</td></tr>'+
      '<tr><td style="width:20%">Alasan</td><td>'+a+'</td></tr></table></td>'+
      '<td class="text-center"><a href="javascript:void(0);" class="rm btn-sm btn-danger"><i class="fa fa-remove"></i></a>'+
      '<input type="hidden" name="ds[spesimen][]" value="'+s+'|'+tak+'|'+tap+'|'+tal+'|'+k+'|'+p+'|'+a+'"></td>'+
      '</tr>'
      );
    $('.rm').on('click',function(){
      $(this).parent().parent().remove();
    });
    $('#div_sampel_pengumpulan').hide(500);
    $('#add_pengumpulan_spesimen').show();
    // reset form
    $('#spesimen_ke').val('');
    $('#tgl_kirim_kab').val('');
    $('#tgl_kirim_prov').val('');
    $('#tgl_kirim_lab').val('');
    $('#kondisi').select2('val','');
    $('#pengambilan_ulang').select2('val','');
    $('#pengambilan_ulang').val('');
    $('#alasan_spesimen_tidak_diambil').val('');
    return false;
  });
});
</script>

<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Kesimpulan Hasil Laboratorium</h3>
  </div>
  <div class='form-horizontal'>
    <div class="box-body">
      <div class="form-group">
				{!! Form::label(null, 'Virus Polio', ['class' => 'col-sm-2 control-label']) !!}
        {!! Form::label(null, 'P1', ['class' => 'col-sm-1 control-label']) !!}
				<div class="col-sm-3">
					{!! Form::select('dhl[hasil_lab_P1]', array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif'), null, ['class' => 'form-control','id'=>'hasil_lab_P1',]) !!}
				</div>
			</div>
      <div class="form-group">
				{!! Form::label(null, '', ['class' => 'col-sm-2 control-label']) !!}
        {!! Form::label(null, 'P2', ['class' => 'col-sm-1 control-label']) !!}
				<div class="col-sm-3">
					{!! Form::select('dhl[hasil_lab_P2]', array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif'), null, ['class' => 'form-control','id'=>'hasil_lab_P2',]) !!}
				</div>
			</div>
      <div class="form-group">
				{!! Form::label(null, '', ['class' => 'col-sm-2 control-label']) !!}
        {!! Form::label(null, 'P3', ['class' => 'col-sm-1 control-label']) !!}
				<div class="col-sm-3">
					{!! Form::select('dhl[hasil_lab_P3]', array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif'), null, ['class' => 'form-control','id'=>'hasil_lab_P3',]) !!}
				</div>
			</div>
      <div class="form-group">
        {!! Form::label(null, 'NPEV', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::select('dhl[hasil_npev]', array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif'),null, ['class' => 'form-control','id'=>'hasil_npev',]) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Jika positif, sebutkan', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('dhl[ket_positif_npev]', null, ['class' => 'form-control','id'=>'ket_positif_npev','placeholder'=>'Sebutkan','disabled']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Entero Virus', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::select('dhl[hasil_entero_virus]', array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif'),null, ['class' => 'form-control','id'=>'hasil_entero_virus',]) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Jika positif, sebutkan', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('dhl[ket_positif_entero_virus]', null, ['class' => 'form-control','id'=>'ket_positif_entero_virus','placeholder'=>'Sebutkan','disabled']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Keterangan', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::textarea('dhl[ket_hasil_kesimpulan]', null, ['class' => 'form-control','id'=>'ket_hasil_kesimpulan','placeholder'=>'Uraikan keterangan tambahan terkait spesimen dan pemeriksaan laboratorium jika diperlukan', 'rows'=>3]) !!}
        </div>
      </div>
    </div>
  </div>
</div>
  
<script type="text/javascript">
$(function(){
  $('#hasil_npev').on('change',function(){
    var val = $(this).val();
    if (val == 1) {
      $('#ket_positif_npev').removeAttr('disabled');
    }else{
      $('#ket_positif_npev').attr('disabled','disabled');
      $('#ket_positif_npev').val(null);
    }
    return false;
  });
  $('#hasil_entero_virus').on('change',function(){
    var val = $(this).val();
    if (val == 1) {
      $('#ket_positif_entero_virus').removeAttr('disabled');
    }else{
      $('#ket_positif_entero_virus').attr('disabled','disabled');
      $('#ket_positif_entero_virus').val(null);
    }
    return false;
  });
});
</script>


  <div class='form-horizontal' style="display:none" id="form_sequencing">
    <div class="box-body">
      <h4>Sequencing</h4>
      <div class="form-group">
      {!! Form::open(['method' => 'POST', 'url' => '', 'id'=>"form_pemeriksaan_sequencing" , 'class' => 'form-horizontal']) !!}
        {!! Form::hidden('id', null, ['class' => 'form-control','id'=>'ps_id']) !!}
        {!! Form::label('nik', 'Dilakukan pemeriksaan', ['class' => 'col-sm-3 control-label',]) !!}
        <div class="col-sm-4">
          {!! Form::select('ds[dilakukan_sequencing]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'),null, ['class' => 'form-control select','id'=>'dilakukan_sequencing',]) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Dirujuk', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::select('ds[dirujuk_sequencing]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'),null, ['class' => 'form-control select','id'=>'dirujuk_sequencing','disabled']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Laboratorium rujukan', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::select('ds[lab_rujukan_sequencing]', array(null=>'--Pilih--','1'=>'INO, Bandung','2'=>'INO, Jakarta','3'=>'INO, Surabaya'),null, ['class' => 'form-control dirujuk_sequencing select','id'=>'lab_rujukan_sequencing','disabled']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Tanggal isolat dirujuk ke lab rujukan untuk permeriksaan sequencing', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('ds[tgl_isolat_dirujuk_lab_sequencing]', null, ['class' => 'form-control datemax dirujuk_sequencing','placeholder'=>'Tanggal isolat dirujuk ke lab rujukan','id'=>'tgl_isolat_dirujuk_lab_sequencing','disabled']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Tanggal isolat diterima di lab rujukan untuk permeriksaan sequencing', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('ds[tgl_isolat_diterima_lab_sequencing]', null, ['class' => 'form-control datemax dirujuk_sequencing','placeholder'=>'Tanggal isolat diterima di lab rujukan','id'=>'tgl_isolat_diterima_lab_sequencing','disabled']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'ID Sequencing', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('ds[id_sequencing]', null, ['class' => 'form-control dilakukan_sequencing','placeholder'=>'ID Sequencing','id'=>'id_sequencing','disabled']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Tanggal Pemeriksaan Sequencing', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('ds[tgl_periksa_sequencing]', null, ['class' => 'form-control datemax dilakukan_sequencing','placeholder'=>'Tanggal Pemeriksaan Sequencing','id'=>'tgl_periksa_sequencing','disabled']) !!}
        </div>
      </div>

      <div class="form-group">
        {!! Form::label(null, 'Hasil pemeriksaan sequencing', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
          <div class="form-group">
            {!! Form::label(null, 'P1', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::select('ds[hasil_sequencing_p1]', array(null=>'--Pilih--')+Helper::getHasilSequencing(),null, ['class' => 'form-control dilakukan_sequencing select','id'=>'hasil_sequencing_p1','disabled']) !!}
            </div>
          </div>
          <div class="form-group">
            {!! Form::label(null, 'P2', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::select('ds[hasil_sequencing_p2]', array(null=>'--Pilih--')+Helper::getHasilSequencing(),null, ['class' => 'form-control dilakukan_sequencing select','id'=>'hasil_sequencing_p2','disabled']) !!}
            </div>
          </div>
          <div class="form-group">
            {!! Form::label(null, 'P3', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::select('ds[hasil_sequencing_p3]', array(null=>'--Pilih--')+Helper::getHasilSequencing(),null, ['class' => 'form-control dilakukan_sequencing select','id'=>'hasil_sequencing_p3','disabled']) !!}
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Interpretasi hasil pemeriksaan sequencing', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::textarea('ds[interpretasi_sequencing]', null, ['class' => 'form-control dilakukan_sequencing','id'=>'interpretasi_sequencing','placeholder'=>'Interpretasi hasil pemeriksaan sequencing','maxlength' => 200,'size' => '30x8','disabled']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Tanggal tersedia hasil sequencing', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('ds[tgl_tersedia_hasil_sequencing]', null, ['class' => 'form-control datemax dilakukan_sequencing','placeholder'=>'Tanggal tersedia hasil sequencing sel','id'=>'tgl_tersedia_hasil_sequencing','disabled']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Tanggal hasil sequencing dikirim dari lab ke EPI', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('ds[tgl_hasil_sequencing_dikirm_epi]', null, ['class' => 'form-control datemax dilakukan_sequencing','placeholder'=>'Tanggal hasil final kutur sel dikirim dari lab ke EPI','id'=>'tgl_hasil_sequencing_dikirm_epi','disabled']) !!}
        </div>
      </div>
      <div class="row">
        <div class="col-md-offset-4 col-md-8">
          <a class="btn btn-warning" id='tutup_pemeriksaan_sequencing'>Tutup</a>
          <!-- <a class="btn btn-success" id="simpan_pemeriksaan_sequencing">Simpan</a> -->
          {!! Form::submit("Simpan", ['class' => 'btn btn-success','id'=>"submit_form_pemeriksaan_sequencing"]) !!}
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
  $(function(){
    $('#form_pemeriksaan_sequencing').validate({
      rules:{
        'ds[dilakukan_sequencing]':'required',
        'ds[id_sequencing]'       :'required',
        'ds[hasil_final_kultur]'  :'required',
      },
      messages:{
        'ds[dilakukan_sequencing]':'Data wajib diisi',
        'ds[id_sequencing]'       :'Data wajib diisi',
        'ds[hasil_final_kultur]'  :'Data wajib diisi',
      },
      submitHandler: function(){
        var action = BASE_URL+'api/analisa/'+kasus;
        // $.ajax({
        //   method  : "POST",
        //   url     : action,
        //   data    : JSON.stringify(senddata),
        //   beforeSend: function(){
        //     startProcess();
        //   },
        //   success: function(data){
        //     if (data.success==true) {
        //       endProcess();
        //       var dt=data.response;
              var id=$('#ps_id').val();
              $('#input_pemeriksaan').hide();
              $('#form_sequencing').hide();
              $('.form-control').val(null);
              $('.select').select2('val',null);
              $('#seq_'+id).removeClass("btn-info");
              $('#seq_'+id).addClass("btn-danger");
              $('#seq_'+id).attr('disabled','disabled');
        //     }else{
        //       messageAlert('warning', 'Peringatan', 'Data gagal salah');
        //       endProcess();
        //     }
        //   }
        // });
        // return false;
      }
    });

    $('#tutup_pemeriksaan_sequencing').on('click',function(){
      var id=$('#ps_id').val();
      $('#input_pemeriksaan').hide();
      $('#form_sequencing').hide();
      $('.form-control').val(null);
      $('.select').select2('val',null);
      $('#seq_'+id).removeClass("btn-info");
      $('#seq_'+id).addClass("btn-danger");
      $('#seq_'+id).attr('disabled','disabled');
      return false;
    });
    $('#add_pemeriksaan_sequencing').on('click',function(){
      $('#div_pemeriksaan_sequencing').show(500);
      $('#add_pemeriksaan_sequencing').hide();
      return false;
    });
    $('#dilakukan_sequencing').on('change',function(){
      var val = $(this).val();
      if (val==1) {
        $('#dirujuk_sequencing').removeAttr('disabled');
        $('.dilakukan_sequencing').removeAttr('disabled');
      }else{
        $('#dirujuk_sequencing').attr('disabled','disabled');
        $('#dirujuk_sequencing').select2('val',null);
        $('.dilakukan_sequencing').attr('disabled','disabled');
        $('.dilakukan_sequencing').select2('val',null);
      }
      return false;
    });
    $('#dirujuk_sequencing').on('change',function(){
      var val = $(this).val();
      if (val==1) {
        $('.dirujuk_sequencing').removeAttr('disabled');
      }else{
        $('.dirujuk_sequencing').attr('disabled','disabled');
        $('.dirujuk_sequencing').select2('val',null);
      }
      return false;
    });
  });
  </script>

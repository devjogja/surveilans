<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Pemeriksaan Laboratorium</h3>
  </div>
  @include('lab.case.afp.periksa.form_kultur_sel')
  @include('lab.case.afp.periksa.form_itd')
  @include('lab.case.afp.periksa.form_sequencing')
</div>

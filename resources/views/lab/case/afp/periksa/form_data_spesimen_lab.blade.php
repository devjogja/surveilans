<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Data Spesimen dan Laboratorium</h3>
  </div>
  <div class='form-horizontal'>
    <div class="box-body">
      <table class="table table-striped table-bordered" id="data_spesimen">
				<thead style="background: #4caf50;color: white;">
					<tr>
            <th>ID Spesimen (Stool)</th>
            <th>Spesimen Ke-</th>
						<th>Total Spesimen</th>
						<th>Tanggal</th>
						<th style="width:10%">Kondisi Spesimen diterima di Laboratorium</th>
            <th style="width:17%">Stool Dirujuk</th>
            <th>Tanggal</th>
            <th colspan="2">Aksi</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			<div style="margin: 14px 0px;">
				<button type="button" class="btn btn-success btn-flat" id="add_data_spesimen"><i class="fa fa-plus-circle"></i> Tambah Spesimen</button>
			</div>
			<div id="div_sampel_data" style="margin-top: 2%">
				<table class="table table-bordered">
					<tr>
            <th>ID Spesimen (Stool)</th>
            <th>Spesimen Ke-</th>
            <th>Total Spesimen</th>
            <th>Tanggal</th>
            <th style="width:10%">Kondisi Spesimen diterima di Laboratorium</th>
            <th style="width:17%">Stool Dirujuk</th>
            <th>Tanggal</th>
					</tr>
          <tr>
            <td>{!! Form::text(null, null, ['class' => 'form-control','id'=>'id_stool','placeholder'=>'ID Spesimen (Stool)']) !!}</td>
            <td>{!! Form::text(null, null, ['class' => 'form-control','id'=>'spesimen_ke_','placeholder'=>'Spesimen Ke-']) !!}</td>
            <td>{!! Form::text(null, null, ['class' => 'form-control','id'=>'total','placeholder'=>'Total Spesimen']) !!}</td>
            <td>
              <table class="table table-bordered">
                <tr><td>Tanggal Pengambilan Stool</td>
                  <td>{!! Form::text(null, null, ['class' => 'form-control datemax','placeholder'=>'Tanggal ambil stool','id'=>'tgl_ambil_stool']) !!}</td>
                </tr>
                <tr>
                  <td>Tanggal Kirim ke Laboratorium</td>
                  <td>{!! Form::text(null, null, ['class' => 'form-control datemax','placeholder'=>'Tanggal kirim ke laboratorium','id'=>'tgl_kirim_lab']) !!}</td>
                </tr>
                <tr>
                  <td>Tanggal Terima di Laboratorium</td>
                  <td>{!! Form::text(null, null, ['class' => 'form-control datemax','placeholder'=>'Tanggal terima di laboratorium','id'=>'tgl_terima_lab']) !!}</td>
                </tr>
              </table>
            </td>
            <td>{!! Form::select(null, array(null=>'--Pilih--','1'=>'Adekuat','2'=>'Tidak Adekuat ','3'=>'Tidak diketahui'),null, ['class' => 'form-control','placeholder'=>'Kondisi spesimen diterima di provinsi','id'=>'kondisi_stool',]) !!}</td>
            <td>
              <table class="table table-bordered">
                <tr>
                  <td colspan="2" style="width:100%">{!! Form::select(null, array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'),null, ['class' => 'form-control','placeholder'=>'Dirujuk','id'=>'rujuk',]) !!}</td>
                </tr>
                <tr><td style="width:35%">Alasan dirujuk</td>
                  <td>{!! Form::select(null, array(null=>'--Pilih--','1'=>'Banking','2'=>'Sequencing','3'=>'Discordant','4'=>'ITD','5'=>'SEQ (SP) + Banking','6'=>'SEQ (SP) + ITD (SP)','7'=>'Banking (SP)+ITD','8'=>'Culture Cell'),null, ['class' => 'form-control','id'=>'alasan_rujuk','disabled']) !!}</td>
                </tr>
                <tr><td>Laboratorium rujukan</td>
                  <td>{!! Form::select(null, array(null=>'--Pilih--','1'=>'INO,Bandung','2'=>'INO,Jakarta','3'=>'INO,Surabaya'),null, ['class' => 'form-control','placeholder'=>'Laboratorium rujukan','id'=>'lab_rujuk','disabled']) !!}</td>
                </tr>
              </table>
            </td>
            <td>
              <table class="table table-bordered">
                <tr><td>Tanggal spesimen dirujuk ke lab rujukan</td>
                  <td>{!! Form::text(null, null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Kirim ke kabupaten/kota','id'=>'tgl_kirim_lab_rujukan']) !!}</td>
                </tr>
                <tr>
                  <td>Tanggal spesimen diterima di lab rujukan</td>
                  <td>{!! Form::text(null, null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Kirim ke laboratorium','id'=>'tgl_terima_lab_rujukan']) !!}</td>
                </tr>
              </table>
            </td>
          </tr>
				</table>
				<div class="row">
					<div class="col-md-offset-4 col-md-8">
						<button class="btn btn-default" id='tutup_sampel_data'>Tutup</button>
						<button class="btn btn-success" id="tambah_spesimen_data">Tambah</button>
					</div>
				</div>
			</div>
    </div>
  </div>
</div>
<script type="text/javascript">
function inputDataPemeriksaanKultur(dt) {
  $(window).scrollTop(500);
  $('#input_pemeriksaan').show(500);
  $('#form_sequencing').hide();
  $('#form_itd').hide();
  $('#form_kultur').show();
  $('#pk_id').val(dt);
}
function inputDataPemeriksaanSeq(dt) {
  $(window).scrollTop(500);
  $('#input_pemeriksaan').show(500);
  $('#form_kultur').hide();
  $('#form_itd').hide();
  $('#form_sequencing').show();
  $('#ps_id').val(dt);
}
function inputDataPemeriksaanItd(dt) {
  $(window).scrollTop(500);
  $('#input_pemeriksaan').show(500);
  $('#form_sequencing').hide();
  $('#form_kultur').hide();
  $('#form_itd').show();
  $('#pi_id').val(dt);
}

$(function(){
  $('#div_sampel_data').hide();
  $('#tutup_sampel_data').on('click',function(){
    $('#div_sampel_data').hide(500);
    $('#add_data_spesimen').show();
    return false;
  });
  $('#add_data_spesimen').on('click',function(){
    $('#div_sampel_data').show(500);
    $('#add_data_spesimen').hide();
    return false;
  });
  $('#rujuk').on('change',function(){
    var val = $(this).val();
    if (val==1) {
      $('#alasan_rujuk').removeAttr('disabled');
      $('#lab_rujuk').removeAttr('disabled');
    }else{
      $('#alasan_rujuk').attr('disabled','disabled');
      $('#lab_rujuk').attr('disabled','disabled');
      $('#lab_rujuk').select2('val',null);
    }
    return false;
  });

  var i=0;
  $('#tambah_spesimen_data').on('click',function(){
    var id = $('#id_stool').val();
    var sk = $('#spesimen_ke_').val();
    var tot = $('#total').val();
    var tas = $('#tgl_ambil_stool').val();
    var tkl = $('#tgl_kirim_lab').val();
    var ttl = $('#tgl_terima_lab').val();
    var kst = $('#kondisi_stool option:selected').text()
    var ks = $('#kondisi_stool').val();
    var rt = $('#rujuk option:selected').text();
    var r = $('#rujuk').val();
    var art = $('#alasan_rujuk option:selected').text();
    var ar = $('#alasan_rujuk').val();
    var lrt = $('#lab_rujuk option:selected').text();
    var lr = $('#lab_rujuk').val();
    var tkr = $('#tgl_kirim_lab_rujukan').val();
    var ttr = $('#tgl_terima_lab_rujukan').val();
    $('#data_spesimen').append('<tr>'+
      '<td>'+id+'</td>'+
      '<td>'+sk+'</td>'+
      '<td>'+tot+'</td>'+
      '<td><table class="table table-bordered"><tr><td style="width:60%">Tanggal Pengambilan Stool</td><td>'+tas+'</td></tr>'+
      '<tr><td>Tanggal kirim ke laboratorium</td><td>'+tkl+'</td></tr>'+
      '<tr><td>Tanggal terima di laboratorium</td><td>'+ttl+'</td></tr></table></td>'+
      '<td>'+kst+'</td>'+
      '<td><table class="table table-bordered"><tr><td colspan="2" style="width:100%">'+rt+'</td></tr>'+
      '<tr><td style="width:35%">Alasan dirujuk</td><td>'+art+'</td></tr><tr><td>Laboratorium rujukan</td><td>'+lrt+'</td></tr></table></td>'+
      '<td><table class="table table-bordered"><tr><td style="width:60%">Tanggal spesimen dirujuk ke lab rujukan</td><td>'+tkr+'</td></tr>'+
      '<tr><td>Tanggal spesimen diterima di lab rujukan</td><td>'+ttr+'</td></tr></table></td>'+
      '<td id="'+i+'"><a id="kultur_'+i+'"title="Kultur Sel" data-toggle="tooltip" style="margin-bottom:2px" onclick="inputDataPemeriksaanKultur('+i+')" class="btn btn-info btn-flat">Kultur Sel</a><br>'+
      '<a id="itd_'+i+'" title="ITD" data-toggle="tooltip" style="margin-bottom:2px" onclick="inputDataPemeriksaanItd('+i+')" class="btn btn-info btn-flat">ITD</a><br>'+
      '<a id="seq_'+i+'"title="Sequencing" data-toggle="tooltip" style="margin-bottom:2px" onclick="inputDataPemeriksaanSeq('+i+')" class="btn btn-info btn-flat">Sequencing</a></td>'+
      '<td class="text-center"><a href="javascript:void(0);" class="rm btn-sm btn-danger"><i class="fa fa-remove"></i></a>'+
      '<input type="hidden" name="ds[spesimen][]" value="'+id+'|'+sk+'|'+tot+'|'+tas+'|'+tkl+'|'+ttl+'|'+ks+'|'+r+'|'+ar+'|'+lr+'|'+tkr+'|'+ttr+'|"></td>'+
      '</tr>'
      );
      i++;
    $('.rm').on('click',function(){
      $(this).parent().parent().remove();
    });
    $('#div_sampel_data').hide(500);
    $('#add_data_spesimen').show();
    return false;
  });
});
</script>

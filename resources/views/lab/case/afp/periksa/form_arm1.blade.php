<hr>
<h4>Arm-1 (LB20)</h4>
      <div class="form-group">
        {!! Form::label('nik', 'ID Isolat', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('ds[id_isolat_arm1]', null, ['class' => 'form-control','placeholder'=>'ID Sequencing','id'=>'id_isolat_arm1']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label('no_rm', 'Kondisi isolat', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::select('ds[kondisi_isolat_arm1]', array(null=>'--Pilih--','1'=>'Good','2'=>'not cold','3'=>'dried','4'=>'leaking','5'=>'not sufficient','6'=>'unknown'),null, ['class' => 'form-control select','id'=>'kondisi_isolat_arm1',]) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label('no_rm', 'Tanggal isolat diterima di lab rujukan untuk pemeriksaan', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('ds[tgl_isolat_diterima_lab_arm1]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal isolat diterima di lab rujukan','id'=>'tgl_isolat_diterima_lab_arm1']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label('no_rm', 'Dilakukan pemeriksaan', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::select('ds[dilakukan_pemeriksaan_arm1]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'),null, ['class' => 'form-control select','id'=>'dilakukan_pemeriksaan_arm1',]) !!}
        </div>
      </div>
			<div id="div_pemeriksaan_arm1" style="margin-top: 2%">
				<table class="table table-bordered">
					<tr style="background: #4caf50;color: white;">
            <!-- <th style="width:30%">ARM-1(LB20)</th> -->
            <th style="width:30%">PCR/rRT-PCR</th>
						<th style="width:30%">rRTPCR-VDPV</th>
					</tr>
          <tr>
            <td><table class="table table-bordered">
              <tr><td>Hasil</td>
                <td>{!! Form::select('ds[hasil_pcr_arm1]', array(null=>'--Pilih--','1'=>'Wild P1','2'=>'Wild P2','3'=>'Wild P3','4'=>'Wild Mix (SP)','5'=>'SL P1','6'=>'SL P2','7'=>'SL P3','8'=>'SL Mix (SP)','9'=>'Wild (SP) + SL (SP) Mix','10'=>'NPEV','11'=>'NEV','12'=>'Others'),null, ['class' => 'form-control pemeriksaan_arm1 select','id'=>'hasil_pcr_arm1',]) !!}</td>
              </tr>
              <tr>
                <td>Jika lainnya, sebutkan</td>
                <td>{!! Form::text('ds[hasil_other_pcr_arm1]', null, ['class' => 'form-control','placeholder'=>'Jika lainnya, sebutkan','id'=>'hasil_other_pcr_arm1','disabled']) !!}</td>
              </tr>
              <tr>
                <td>Tanggal proses</td>
                <td>{!! Form::text('ds[tgl_proses_pcr_arm1]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal proses','id'=>'tgl_proses_pcr_arm1']) !!}</td>
              </tr>
              <tr>
                <td>Aksi</td>
                <td>{!! Form::select('ds[aksi_pcr_arm1]', array(null=>'--Pilih--','1'=>'NSL (SP) Ref To Seq','2'=>'SL (SP) Ref To ELISA','3'=>'Mix (SP) Ref to PNT','4'=>'NSL (SP) Ref For Seq + SL (SP)','5'=>'NPEV Report','6'=>'NEV Report','7'=>'Others/Invalid Ref For ITD','8'=>'rRT-PCR - SL (SP) Ref To rRT PCR - VDPV Assay','9'=>'rRT-PCR - NSL (SP) Ref For Seq + SL (SP) Ref For rRt - VDPV Assay'),null, ['class' => 'form-control pemeriksaan_arm1 select','id'=>'aksi_pcr_arm1',]) !!}</td>
              </tr>
            </table></td>
            <td><table class="table table-bordered">
              <tr><td>Hasil</td>
                <td>{!! Form::select('ds[hasil_rrtpcr_arm1]', array(null=>'--Pilih--','1'=>'Wild P1','2'=>'Wild P2','3'=>'Wild P3','4'=>'Wild Mix (SP)','5'=>'SL P1','6'=>'SL P2','7'=>'SL P3','8'=>'SL Mix (SP)','9'=>'Wild (SP) + SL (SP) Mix','10'=>'NPEV','11'=>'NEV','12'=>'Others'),null, ['class' => 'form-control pemeriksaan_arm1 select','id'=>'hasil_rrtpcr_arm1',]) !!}</td>
              </tr>
              <tr>
                <td>Jika lainnya, sebutkan</td>
                <td>{!! Form::text('ds[hasil_other_rrtpcr_arm1]', null, ['class' => 'form-control pemeriksaan_arm1','placeholder'=>'Jika lainnya, sebutkan','id'=>'hasil_other_rrtpcr_arm1','disabled']) !!}</td>
              </tr>
              <tr>
                <td>Tanggal proses</td>
                <td>{!! Form::text('ds[tgl_proses_rrtpcr_arm1]', null, ['class' => 'form-control datemax pemeriksaan_arm1','placeholder'=>'Tanggal proses','id'=>'tgl_proses_rrtpcr_arm1']) !!}</td>
              </tr>
              <tr>
                <td>Aksi</td>
                <td>{!! Form::select('ds[aksi_rrtpcr_arm1]', array(null=>'--Pilih--','1'=>'NSL (SP) Ref To Seq','2'=>'SL (SP) Ref To ELISA','3'=>'Mix (SP) Ref to PNT','4'=>'NSL (SP) Ref For Seq + SL (SP)','5'=>'NPEV Report','6'=>'NEV Report','7'=>'Others/Invalid Ref For ITD','8'=>'rRT-PCR - SL (SP) Ref To rRT PCR - VDPV Assay','9'=>'rRT-PCR - NSL (SP) Ref For Seq + SL (SP) Ref For rRt - VDPV Assay'),null, ['class' => 'form-control pemeriksaan_arm1 select','id'=>'aksi_rrtpcr_arm1',]) !!}</td>
              </tr>
            </table></td>
          </tr>
				</table>
        <table class="table table-bordered">
					<tr style="background: #4caf50;color: white;">
						<th style="width:30%">ELISA</th>
            <th style="width:30%">PNT</th>
					</tr>
          <tr>
            <td><table class="table table-bordered">
              <tr><td>Hasil</td>
                <td>{!! Form::select('ds[hasil_elisa_arm1]', array(null=>'--Pilih--','1'=>'Wild P1','2'=>'Wild P2','3'=>'Wild P3','4'=>'Wild Mix (SP)','5'=>'SL P1','6'=>'SL P2','7'=>'SL P3','8'=>'SL Mix (SP)','9'=>'Wild (SP) + SL (SP) Mix','10'=>'NPEV','11'=>'NEV','12'=>'Others'),null, ['class' => 'form-control pemeriksaan_arm1  select','id'=>'hasil_elisa_arm1',]) !!}</td>
              </tr>
              <tr>
                <td>Jika lainnya, sebutkan</td>
                <td>{!! Form::text('ds[hasil_other_elisa_arm1]', null, ['class' => 'form-control pemeriksaan_arm1','placeholder'=>'Jika lainnya, sebutkan','id'=>'hasil_other_elisa_arm1','disabled']) !!}</td>
              </tr>
              <tr>
                <td>Tanggal proses</td>
                <td>{!! Form::text('ds[tgl_proses_elisa_arm1]', null, ['class' => 'form-control datemax pemeriksaan_arm1','placeholder'=>'Tanggal proses','id'=>'tgl_proses_elisa_arm1']) !!}</td>
              </tr>
              <tr>
                <td>Aksi</td>
                <td>{!! Form::select('ds[aksi_elisa_arm1]', array(null=>'--Pilih--','1'=>'NSL (SP) Ref To Seq','2'=>'SL (SP) Ref To ELISA','3'=>'Mix (SP) Ref to PNT','4'=>'NSL (SP) Ref For Seq + SL (SP)','5'=>'NPEV Report','6'=>'NEV Report','7'=>'Others/Invalid Ref For ITD','8'=>'rRT-PCR - SL (SP) Ref To rRT PCR - VDPV Assay','9'=>'rRT-PCR - NSL (SP) Ref For Seq + SL (SP) Ref For rRt - VDPV Assay'),null, ['class' => 'form-control pemeriksaan_arm1 select','id'=>'aksi_elisa_arm1',]) !!}</td>
              </tr>
            </table></td>
            <td><table class="table table-bordered">
              <tr><td>Hasil</td>
                <td>{!! Form::select('ds[hasil_pnt_arm1]', array(null=>'--Pilih--','1'=>'Wild P1','2'=>'Wild P2','3'=>'Wild P3','4'=>'Wild Mix (SP)','5'=>'SL P1','6'=>'SL P2','7'=>'SL P3','8'=>'SL Mix (SP)','9'=>'Wild (SP) + SL (SP) Mix','10'=>'NPEV','11'=>'NEV','12'=>'Others'),null, ['class' => 'form-control pemeriksaan_arm1 select','id'=>'hasil_pnt_arm1',]) !!}</td>
              </tr>
              <tr>
                <td>Jika lainnya, sebutkan</td>
                <td>{!! Form::text('ds[hasil_other_pnt_arm1]', null, ['class' => 'form-control pemeriksaan_arm1' ,'placeholder'=>'Jika lainnya, sebutkan','id'=>'hasil_other_pnt_arm1','disabled']) !!}</td>
              </tr>
              <tr>
                <td>Tanggal proses</td>
                <td>{!! Form::text('ds[tgl_proses_pnt_arm1]', null, ['class' => 'form-control datemax pemeriksaan_arm1','placeholder'=>'Tanggal proses','id'=>'tgl_proses_pnt_arm1']) !!}</td>
              </tr>
              <tr>
                <td>Aksi</td>
                <td>{!! Form::select('ds[aksi_pnt_arm1]', array(null=>'--Pilih--','1'=>'NSL (SP) Ref To Seq','2'=>'SL (SP) Ref To ELISA','3'=>'Mix (SP) Ref to PNT','4'=>'NSL (SP) Ref For Seq + SL (SP)','5'=>'NPEV Report','6'=>'NEV Report','7'=>'Others/Invalid Ref For ITD','8'=>'rRT-PCR - SL (SP) Ref To rRT PCR - VDPV Assay','9'=>'rRT-PCR - NSL (SP) Ref For Seq + SL (SP) Ref For rRt - VDPV Assay'),null, ['class' => 'form-control pemeriksaan_arm1 select','id'=>'aksi_pnt_arm1',]) !!}</td>
              </tr>
            </table></td>
          </tr>
				</table>
        <br>
        <div class="form-group">
          {!! Form::label('no_rm', 'Tanggal tersedia hasil akhir isolat', ['class' => 'col-sm-3 control-label']) !!}
          <div class="col-sm-4">
            {!! Form::text('ds[tgl_tersedia_hasil_arm1]', null, ['class' => 'form-control datemax pemeriksaan_arm1','placeholder'=>'Tanggal tersedia hasil akhir isolat','id'=>'tgl_tersedia_hasil_arm1' ]) !!}
          </div>
        </div>
        <div class="form-group">
          {!! Form::label('no_rm', 'Hasil final isolat', ['class' => 'col-sm-3 control-label']) !!}
          <div class="col-sm-4">
            {!! Form::select('ds[hasil_arm1]', array(null=>'--Pilih--','1'=>'Wild (SP)','2'=>'Sabin (SP)','3'=>'Discordant (SP)','4'=>'Negative','5'=>'NPEV','6'=>'NEV','7'=>'Wild (SP) + Sabin (SP)','8'=>'Sabin (SP) + NPEV','9'=>'Wild (SP) + NEV','10'=>'INC. (SP) Refer To GSL','11'=>'SL (SP) + Invalid (SP)','12'=>'NSL (SP) + Invalid (SP)','13'=>'Test Not Required'),null, ['class' => 'form-control pemeriksaan_arm1 select','id'=>'hasil_arm1',]) !!}
          </div>
        </div>
			</div>

  <script type="text/javascript">
  $(function(){
    $('.pemeriksaan_arm1').attr('disabled','disabled').val(null);
    $('#hasil_other_pcr_arm1').attr('disabled','disabled').val(null);
    $('#hasil_other_rrtpcr_arm1').attr('disabled','disabled').val(null);
    $('#hasil_other_elisa_arm1').attr('disabled','disabled').val(null);
    $('#hasil_other_pnt_arm1').attr('disabled','disabled').val(null);


    $('#hasil_pcr_arm1').on('change',function(){
      var val = $(this).val();
      if(val=='12'){
        $('#hasil_other_pcr_arm1').removeAttr('disabled');
      }else{
        $('#hasil_other_pcr_arm1').attr('disabled','disabled').val(null);
      }
      return false;
    });
    $('#hasil_rrtpcr_arm1').on('change',function(){
      var val = $(this).val();
      if(val=='12'){
        $('#hasil_other_rrtpcr_arm1').removeAttr('disabled');
      }else{
        $('#hasil_other_rrtpcr_arm1').attr('disabled','disabled').val(null);
      }
      return false;
    });
    $('#hasil_elisa_arm1').on('change',function(){
      var val = $(this).val();
      if(val=='12'){
        $('#hasil_other_elisa_arm1').removeAttr('disabled');
      }else{
        $('#hasil_other_elisa_arm1').attr('disabled','disabled').val(null);
      }
      return false;
    });
    $('#hasil_pnt_arm1').on('change',function(){
      var val = $(this).val();
      if(val=='12'){
        $('#hasil_other_pnt_arm1').removeAttr('disabled');
      }else{
        $('#hasil_other_pnt_arm1').attr('disabled','disabled').val(null);
      }
      return false;
    });

    $('#div_pemeriksaan_arm1').hide();

    $('#dilakukan_pemeriksaan_arm1').on('change',function(){
      var val = $(this).val();
      if(val=='1'){
        $('.pemeriksaan_arm1').removeAttr('disabled');
        $('#div_pemeriksaan_arm1').show(500);
      }else{
        $('#div_pemeriksaan_arm1').hide(500);
        $('.pemeriksaan_arm1').attr('disabled','disabled').val(null);
      }
      return false;
    });

    // $('#tambah_pemeriksaan_arm1').on('click',function(){
    //   var iia1 = $('#id_isolat_arm1').val();
    //   var kia1 = $('#kondisi_isolat_arm1').val();
    //   var kia1t = $('#kondisi_isolat_arm1 option:selected').text();
    //   var tidla1 = $('#tgl_isolat_diterima_lab_arm1').val();
    //   var dpa1 = $('#dilakukan_pemeriksaan_arm1').val();
    //   var dpa1t = $('#dilakukan_pemeriksaan_arm1 option:selected').text();
    //
    //   var hpcra1 = $('#hasil_pcr_arm1').val();
    //   var hpcra1t = $('#hasil_pcr_arm1 option:selected').text();
    //   var hopcra1 = $('#hasil_other_pcr_arm1').val();
    //   var tppcra1 = $('#tgl_proses_pcr_arm1').val();
    //   var apcra1 = $('#aksi_pcr_arm1').val();
    //   var apcra1t = $('#aksi_pcr_arm1 option:selected').text();
    //
    //   var hrrta1 = $('#hasil_rrtpcr_arm1').val();
    //   var hrrta1t = $('#hasil_rrtpcr_arm1 option:selected').text();
    //   var horrta1 = $('#hasil_other_rrtpcr_arm1').val();
    //   var tprrta1 = $('#tgl_proses_rrtpcr_arm1').val();
    //   var arrta1 = $('#aksi_rrtpcr_arm1').val();
    //   var arrta1t = $('#aksi_rrtpcr_arm1 option:selected').text();
    //
    //   var helsa1 = $('#hasil_elisa_arm1').val();
    //   var helsa1t = $('#hasil_elisa_arm1 option:selected').text();
    //   var hoelsa1 = $('#hasil_other_elisa_arm1').val();
    //   var tpelsa1 = $('#tgl_proses_elisa_arm1').val();
    //   var aelsa1 = $('#aksi_elisa_arm1').val();
    //   var aelsa1t = $('#aksi_elisa_arm1 option:selected').text();
    //
    //   var hpnta1 = $('#hasil_pnt_arm1').val();
    //   var hpnta1t = $('#hasil_pnt_arm1 option:selected').text();
    //   var hopnta1 = $('#hasil_other_pnt_arm1').val();
    //   var tppnta1 = $('#tgl_proses_pnt_arm1').val();
    //   var apnta1 = $('#aksi_pnt_arm1').val();
    //   var apnta1t = $('#aksi_pnt_arm1 option:selected').text();
    //
    //   var ttha1 = $('#tgl_tersedia_hasil_arm1').val();
    //   var ha1 = $('#hasil_arm1').val();
    //   var ha1t = $('#hasil_arm1 option:selected').text();
    //
    //   $('#data_arm1').append('<tr>'+
    //     // '<td><table class="table table-bordered"><tr><td>ID Isolat</td><td style="width:55%">'+iia1+'</td></tr>'+
    //     // '<tr><td>Kondisi isolat</td><td>'+kia1t+'</td></tr>'+
    //     // '<tr><td>Tanggal isolat diterima di lab rujukan untuk pemeriksaan</td><td>'+tidla1+'</td></tr>'+
    //     // '<tr><td>Dilakukan pemeriksaan</td><td>'+dpa1t+'</td></tr></table></td>'+
    //     '<td><table class="table table-bordered"><tr><td style="width:40%">Hasil</td><td>'+hpcra1t+'</td></tr>'+
    //     '<tr><td>Jika lainnya, sebutkan</td><td>'+hopcra1+'</td></tr><tr><td>Tanggal proses</td><td>'+tppcra1+'</td></tr>'+
    //     '<tr><td>Aksi</td><td>'+apcra1t+'</td></tr></table></td>'+
    //     '<td><table class="table table-bordered"><tr><td style="width:40%">Hasil</td><td>'+hrrta1t+'</td></tr>'+
    //     '<tr><td>Jika lainnya, sebutkan</td><td>'+horrta1+'</td></tr><tr><td>Tanggal proses</td><td>'+tprrta1+'</td></tr>'+
    //     '<tr><td>Aksi</td><td>'+arrta1t+'</td></tr></table></td>'+
    //     '<td><table class="table table-bordered"><tr><td style="width:40%">Hasil</td><td>'+helsa1t+'</td></tr>'+
    //     '<tr><td>Jika lainnya, sebutkan</td><td>'+hoelsa1+'</td></tr><tr><td>Tanggal proses</td><td>'+tpelsa1+'</td></tr>'+
    //     '<tr><td>Aksi</td><td>'+aelsa1t+'</td></tr></table></td>'+
    //     '<td><table class="table table-bordered"><tr><td style="width:40%">Hasil</td><td>'+hpnta1t+'</td></tr>'+
    //     '<tr><td>Jika lainnya, sebutkan</td><td>'+hopnta1+'</td></tr><tr><td>Tanggal proses</td><td>'+tppnta1+'</td></tr>'+
    //     '<tr><td>Aksi</td><td>'+apnta1t+'</td></tr></table></td>'+
    //     '<td><table class="table table-bordered"><tr><td style="width:40%">Tanggal tersedia hasil isolat</td><td>'+ttha1+'</td></tr>'+
    //     '<tr><td>Hasil</td><td>'+ha1t+'</td></tr>'+
    //     '</table></td>'+
    //     '<td><a href="javascript:void(0);" class="rm"><i class="fa fa-remove"></i></a>'+
    //     '<input type="hidden" name="ds[spesimen][]" value="'+iia1+'|'+kia1+'|'+tidla1+'|'+dpa1+'|'+hpcra1+'|'+hopcra1+'|'+apcra1+'|'+hrrta1+'|'+horrta1+'|'+tprrta1+'|'+arrta1+
    //     '|'+helsa1+'|'+hoelsa1+'|'+tpelsa1+'|'+aelsa1+'|'+hpnta1+'|'+hopnta1+'|'+tppnta1+'|'+apnta1+'|'+ttha1+'|'+ha1t+'|"></td>'+
    //     '</tr>'
    //     );
    //   $('.rm').on('click',function(){
    //     $(this).parent().parent().remove();
    //   });
    //   $('#div_pemeriksaan_arm1').hide(500);
    //   $('#add_pemeriksaan_arm1').show();
    //   return false;
    // });



  });
  </script>

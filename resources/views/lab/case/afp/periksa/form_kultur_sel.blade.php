<div class='form-horizontal' style="display:none" id="form_kultur">
    <div class="box-body">
      <h4>Kultur Sel</h4>
      <div class="form-group">
      {!! Form::open(['method' => 'POST', 'url' => '', 'id'=>"form_pemeriksaan_kultur" , 'class' => 'form-horizontal']) !!}
        {!! Form::hidden('id', null, ['class' => 'form-control','id'=>'pk_id']) !!}
        {!! Form::label('nik', 'Dilakukan pemeriksaan', ['class' => 'col-sm-3 control-label',]) !!}
        <div class="col-sm-4">
          {!! Form::select('ds[dilakukan_kultur]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'),null, ['class' => 'form-control select','id'=>'dilakukan_kultur',]) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label('no_rm', 'Tanggal Mulai Pemeriksaan Kultur Sel', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('ds[tgl_mulai_periksa_kultur]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Mulai Pemeriksaan Kultur Sel','id'=>'tgl_mulai_periksa_kultur','disabled']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label('no_rm', 'Algoritma', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::select('ds[algoritma]', array(null=>'--Pilih--','1'=>'Lama','2'=>'Baru'),null, ['class' => 'form-control select','id'=>'algoritma','disabled']) !!}
        </div>
      </div>
      <hr>
      <table class="table table-striped table-bordered" id="data_kultur_sel">
				<thead style="background: #4caf50;color: white;">
					<tr>
            <th>Arm</th>
						<th>Tipe</th>
						<th>Tanggal mulai pemeriksaan</th>
            <th>Hasil pemeriksaan</th>
            <th>Tanggal tersedia hasil pemeriksaan</th>
            <th>Aksi</th>
            <th>Tanggal dikirim untuk pemeriksaan ITD</th>
            <th>Aksi</th>
					</tr>
				</thead>
				<tbody id="body_table_kultur"></tbody>
			</table>
			<div style="margin: 14px 0px;" >
				<button type="button" class="btn btn-success btn-flat" id="add_pemeriksaan_kultur" disabled><i class="fa fa-plus-circle"></i> Tambah Data Pemeriksaan</button>
			</div>
			<div id="div_pemeriksaan_kultur" style="margin-top: 2%">
				<table class="table table-bordered">
					<tr>
            <th>Arm</th>
						<th>Tipe</th>
						<th>Tanggal mulai pemeriksaan</th>
            <th>Hasil pemeriksaan</th>
            <th>Tanggal tersedia hasil pemeriksaan</th>
            <th>Aksi</th>
            <th>Tanggal dikirim untuk pemeriksaan ITD</th>
					</tr>
          <tr>
                <td>{!! Form::select('ds[arm]', array(null=>'--Pilih--','1'=>'LB20','2'=>'RD'),null, ['class' => 'form-control select','id'=>'arm',]) !!}</td>
                <td>{!! Form::select('ds[tipe_arm]',array(null=>'--Pilih--')+Helper::getARM(),null, ['class' => 'form-control select','id'=>'tipe_arm',]) !!}</td>
                <td>{!! Form::text('ds[tgl_mulai_pemeriksaan]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal mulai pemeriksaan','id'=>'tgl_mulai_pemeriksaan']) !!}</td>
                <td>{!! Form::select('ds[hasil_pemeriksaan_kultur]', array(null=>'--Pilih--','1'=>'CPE +ve','2'=>'CPE -ve','3'=>'Degeneration','4'=>'Contamination'),null, ['class' => 'form-control select','id'=>'hasil_pemeriksaan_kultur',]) !!}</td>
                <td>{!! Form::text('ds[tgl_tersedia_pemeriksaan_kultur]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal tersedia hasil pemeriksaan kultur','id'=>'tgl_tersedia_pemeriksaan_kultur']) !!}</td>
                <td>{!! Form::select('ds[aksi_kultur]', array(null=>'--Pilih--','1'=>'Repeat','2'=>'Pass in RD','3'=>'Pass in L20B','4'=>'Report L20B+ve isolate ref for ITD','5'=>'Report Negative','6'=>'Report NPEV','7'=>'Report inc. isolate ref for ITD','8'=>'Re-process','9'=>'Other'),null, ['class' => 'form-control select','id'=>'aksi_kultur',]) !!}</td>
                <td>{!! Form::text('ds[tgl_dikirim_pemeriksaan_itd]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal dikirim untuk pemeriksaan ITD','id'=>'tgl_dikirim_pemeriksaan_itd']) !!}</td>
              </tr>
            </table></td>
          </tr>
				</table>
				<div class="row">
					<div class="col-md-offset-4 col-md-8">
						<button class="btn btn-default" id='tutup_pemeriksaan_kultur'>Tutup</button>
						<button class="btn btn-success" id="tambah_pemeriksaan_kultur">Tambah</button>
					</div>
				</div>
			</div>
      <hr>
      <br>
      <div class="form-group">
        {!! Form::label(null, 'Hasil Inokulasi', ['class' => 'col-sm-3 control-label']) !!}
        {!! Form::label(null, 'di LB20 cell line', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-3">
          {!! Form::select('ds[hasil_inokulasi_lb20]', array(null=>'--Pilih--','1'=>'L+R+','2'=>'L+R-','3'=>'Negatif'),null, ['class' => 'form-control select','id'=>'hasil_inokulasi_lb20',]) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, '', ['class' => 'col-sm-3 control-label']) !!}
        {!! Form::label(null, 'di RD cell line', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-3">
          {!! Form::select('ds[hasil_inokulasi_rd]', array(null=>'--Pilih--','1'=>'L+R+','2'=>'L+R-','3'=>'Negatif'),null, ['class' => 'form-control select','id'=>'hasil_inokulasi_rd',]) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Hasil final kultur sel', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::select('ds[hasil_final_kultur]', array(null=>'--Pilih--','1'=>'L20B Positive Ref to ITD','2'=>'Negative','3'=>'Inconclusive Ref to ITD','4'=>'NPEV','5'=>'L20B Positive& NPEV Ref to ITD','6'=>'Other'),null, ['class' => 'form-control select','id'=>'hasil_final_kultur',]) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Tanggal tersedia hasil final kultur sel', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('ds[tgl_tersedia_hasil_kultur]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal tersedia hasil final kultur sel','id'=>'tgl_tersedia_hasil_kultur']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label(null, 'Tanggal hasil final kutur sel dikirim dari lab ke EPI', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-4">
          {!! Form::text('ds[tgl_hasil_final_dikirm_epi]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal hasil final kutur sel dikirim dari lab ke EPI','id'=>'tgl_hasil_final_dikirm_epi']) !!}
        </div>
      </div>
      <div class="row">
        <div class="col-md-offset-4 col-md-8">
          <a class="btn btn-warning" id='tutup_input_kultur'>Tutup</a>
          {!! Form::submit("Simpan", ['class' => 'btn btn-success','id'=>"submit_form_pemeriksaan_kultur"]) !!}
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
<script type="text/javascript">
$(function(){
  $('#form_pemeriksaan_kultur').validate({
    rules:{
      'ds[hasil_inokulasi_rd]'  :'required',
      'ds[hasil_inokulasi_lb20]':'required',
      'ds[hasil_final_kultur]'  :'required',
    },
    messages:{
      'ds[hasil_inokulasi_lb20]':'Data wajib diisi',
      'ds[hasil_inokulasi_rd]'  :'Data wajib diisi',
      'ds[hasil_final_kultur]'  :'Data wajib diisi',
    },
    submitHandler: function(){
      var action = BASE_URL+'api/analisa/'+kasus;
      // $.ajax({
      //   method  : "POST",
      //   url     : action,
      //   data    : JSON.stringify(senddata),
      //   beforeSend: function(){
      //     startProcess();
      //   },
      //   success: function(data){
      //     if (data.success==true) {
      //       endProcess();
      //       var dt=data.response;
            var id=$('#pk_id').val();
            $('#input_pemeriksaan').hide();
            $('#form_kultur').hide();
            $('.form-control').val(null);
            $('.select').select2('val',null);
            $('#kultur_'+id).removeClass("btn-info");
            $('#kultur_'+id).addClass("btn-danger");
            $('#kultur_'+id).attr('disabled','disabled');
            $('#body_table_kultur').children().remove();
      //     }else{
      //       messageAlert('warning', 'Peringatan', 'Data gagal salah');
      //       endProcess();
      //     }
      //   }
      // });
      // return false;
    }
  });

  $('#tutup_input_kultur').on('click',function(){
    var id=$('#pk_id').val();
    $('#input_pemeriksaan').hide();
    $('#form_kultur').hide();
    $('.form-control').val(null);
    $('.select').select2('val',null);
    $('#kultur_'+id).removeClass("btn-info");
    $('#kultur_'+id).addClass("btn-danger");
    $('#kultur_'+id).attr('disabled','disabled');
    $('#body_table_kultur').children().remove();
    return false;
  });

  $('#dilakukan_kultur').on('change',function(){
    var val = $(this).val();
    if (val==1) {
      $('#tgl_mulai_periksa_kultur').removeAttr('disabled');
      $('#algoritma').removeAttr('disabled');
      $('#add_pemeriksaan_kultur').removeAttr('disabled');
    }else{
      $('#tgl_mulai_periksa_kultur').attr('disabled','disabled');
      $('#tgl_mulai_periksa_kultur').val(null);
      $('#algoritma').attr('disabled','disabled');
      $('#algoritma').select2('val',null);
      $('#add_pemeriksaan_kultur').attr('disabled','disabled');
    }
    return false;
  });

  $('#div_pemeriksaan_kultur').hide();
  $('#tutup_pemeriksaan_kultur').on('click',function(){
    $('#div_pemeriksaan_kultur').hide(500);
    $('#add_pemeriksaan_kultur').show();
    return false;
  });
  $('#add_pemeriksaan_kultur').on('click',function(){
    $('#div_pemeriksaan_kultur').show(500);
    $('#add_pemeriksaan_kultur').hide();
    return false;
  });

  $('#tambah_pemeriksaan_kultur').on('click',function(){
    var arm = $('#arm').val();
    var armt = $('#arm option:selected').text();
    var ta = $('#tipe_arm').val();
    var tat = $('#tipe_arm option:selected').text();
    var tmp = $('#tgl_mulai_pemeriksaan').val();
    var hpk = $('#hasil_pemeriksaan_kultur').val();
    var hpkt = $('#hasil_pemeriksaan_kultur option:selected').text();
    var ttpk = $('#tgl_tersedia_pemeriksaan_kultur').val();
    var ak = $('#aksi_kultur').val();
    var akt = $('#aksi_kultur option:selected').text();
    var tgpi = $('#tgl_dikirim_pemeriksaan_itd').val();

    $('#data_kultur_sel').append('<tr>'+
      '<td>'+armt+'</td>'+
      '<td>'+tat+'</td><td>'+tmp+'</td>'+
      '<td>'+hpkt+'</td><td>'+ttpk+'</td>'+
      '<td>'+akt+'</td><td>'+tgpi+'</td>'+
      '<td><a href="javascript:void(0);" class="rm btn-sm btn-danger"><i class="fa fa-remove"></i></a>'+
      '<input type="hidden" name="ds[pemeriksaan_kultur][]" value="'+arm+'|'+ta+'|'+tmp+'|'+hpk+'|'+ttpk+'|'+ak+'|'+tgpi+'"></td>'+
      '</tr>'
      );
    $('.rm').on('click',function(){
      $(this).parent().parent().remove();
    });
    $('#div_pemeriksaan_kultur').hide(500);
    $('#add_pemeriksaan_kultur').show();
    return false;
  });
});
</script>

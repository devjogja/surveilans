<div class="alert alert-notif alert-dismissable">
	<i class="icon fa fa-warning"></i> Text input yang bertanda bintang (*) wajib di isi
</div>

<div class="row">
	{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form', 'class' => 'form-horizontal']) !!}
	{!! Form::hidden('id_trx_case', $data['id_trx_case'], ['id'=>'id_trx_case']) !!}

	<div class="col-sm-6">
		@include('case.afp.form_pasien')
	</div>
	<div class="col-sm-6">
		@include('case.afp.form_klinis')
	</div>
	<div class="col-sm-12">
		@include('case.afp.form_spesimen')
	</div>
	<div class="col-sm-12">
		@include('case.afp.form_klasifikasi_final')
	</div>
	<div class="col-sm-12">
		<div class="footer">
			{!! Form::reset("Batal", ['class' => 'btn btn-warning batal']) !!}
			{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
		</div>
	</div>
	{!! Form::close() !!}
</div>

<script type="text/javascript">
	function getEpid() {
		var id_kelurahan = $('#id_kelurahan_pasien').val();
		var tgl_mulai_lumpuh = $('#tgl_mulai_lumpuh').val();
		var tgl_sakit = '';
		if(isset(tgl_mulai_lumpuh)){
			tgl_sakit = tgl_mulai_lumpuh;
		}
		if(isset(id_kelurahan)){
			var url = BASE_URL+'case/afp/getEpid';
			$.post(url, JSON.stringify({id_kelurahan:id_kelurahan, tgl_sakit:tgl_sakit}), function(data, status){
				if(isset(data.response)){
					$('#no_epid').val(data.response);
				}
			});
		}
		return false;
	}
	$(function(){
		// $('select').on('change', function() { $(this).valid(); });
		$('.batal').on('click',function(){
			window.location.href = '{!! url('lab/case/afp'); !!}';
			return false;
		});
		$('#form').validate({
			rules:{
				'dp[name]':'required',
				'dp[jenis_kelamin]':'required',
				'df[name]':'required',
				'dp[code_kelurahan]':'required',
				'dk[tgl_mulai_lumpuh]':'required',
			},
			messages:{
				'dp[name]':'Nama pasien wajib di isi',
				'dp[jenis_kelamin]':'Jenis kelamin wajib di isi',
				'df[name]':'Nama orang tua wajib di isi',
				'dp[code_kelurahan]':'Kelurahan wajib di isi',
				'dk[tgl_mulai_lumpuh]':'Tanggal mulai lumpuh wajib di isi',
			},
			submitHandler: function(){
				var action = BASE_URL+'case/afp/store';
				var data = $('#form').serializeJSON();
				$.ajax({
					method  : "POST",
					url     : action,
					data    : JSON.stringify(data),
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
					success: function(data, status){
						if (data.success==true) {
							window.location.href = "{!! url('lab/case/afp'); !!}";
						}else{
							messageAlert('warning', 'Peringatan', 'Data gagal di simpan');
							endProcess();
						}
					}
				});
				return false;
			}
		});
	});
</script>

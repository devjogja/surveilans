@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			@include('lab.case.afp.periksa.index')
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		getDetail();
	});

	function getDetail() {
		var id = $('#id_trx_case').val();
		var action = BASE_URL+'case/afp/getDetail/'+id;
		$.getJSON(action, function(result){
			var dt = result.response;
			if (isset(dt)) {
				var dtCase = dt.dtCase;
				var dtSpesimen = dt.dtSpesimen;
				$('#name').val(dtCase.name_pasien);
				$('#id_pasien').val(dtCase.id_pasien);
				$('#nik').val(dtCase.nik);
				$('#nama_ortu').val(dtCase.nama_ortu);
				$('#id_family').val(dtCase.id_family);
				$('#jenis_kelamin').val(dtCase.jenis_kelamin).select2();
				if (isset(dtCase.tgl_lahir)) {
					$('#tgl_lahir').val(humanDate(dtCase.tgl_lahir)).removeAttr('disabled');
				}
				if(isset(dtCase.umur_thn) || isset(dtCase.umur_bln) || isset(dtCase.umur_hari)){
					$('#umur').val(dtCase.umur_thn).removeAttr('disabled');
					$('#umur_bln').val(dtCase.umur_bln).removeAttr('disabled');
					$('#umur_hari').val(dtCase.umur_hari).removeAttr('disabled');
				}
				$('#id_provinsi_pasien').val(dtCase.code_provinsi_pasien).select2();
				$('#id_kabupaten_pasien').empty().append('<option value="'+dtCase.code_kabupaten_pasien+'">'+dtCase.name_kabupaten_pasien+'</option>').select2();
				$('#id_kecamatan_pasien').empty().append('<option value="'+dtCase.code_kecamatan_pasien+'">'+dtCase.name_kecamatan_pasien+'</option>').select2();
				$('#id_kelurahan_pasien').empty().append('<option value="'+dtCase.code_kelurahan_pasien+'">'+dtCase.name_kelurahan_pasien+'</option>').select2();
				$('#alamat').val(dtCase.alamat);
				$('#no_epid').val(dtCase.no_epid);
				$('#no_epid_lama').val(dtCase.no_epid_lama);
				$('#tgl_mulai_lumpuh').val(humanDate(dtCase.tgl_mulai_lumpuh));
				$('#demam_sebelum_lumpuh').select2('val',dtCase.demam_sebelum_lumpuh);
				$('#kelumpuhan_anggota_gerak_kanan').select2('val',dtCase.kelumpuhan_anggota_gerak_kanan);
				$('#kelumpuhan_anggota_gerak_kiri').select2('val',dtCase.kelumpuhan_anggota_gerak_kiri);
				$('#gangguan_anggota_gerak_kanan').select2('val',dtCase.gangguan_anggota_gerak_kanan);
				$('#gangguan_anggota_gerak_kiri').select2('val',dtCase.gangguan_anggota_gerak_kiri);
				$('#imunisasi_rutin_polio_sebelum_sakit').select2('val',dtCase.imunisasi_rutin_polio_sebelum_sakit);
				$('#informasi_imunisasi_rutin_polio_sebelum_sakit').select2('val',dtCase.informasi_imunisasi_rutin_polio_sebelum_sakit);
				$('#pin_mopup_ori_biaspolio').select2('val',dtCase.pin_mopup_ori_biaspolio);
				$('#informasi_pin_mopup_ori_biaspolio').select2('val',dtCase.informasi_pin_mopup_ori_biaspolio);
				$('#tgl_imunisasi_polio_terakhir').val(humanDate(dtCase.tgl_imunisasi_polio_terakhir));
				$('#tgl_laporan_diterima').val(humanDate(dtCase.tgl_laporan_diterima));
				$('#tgl_pelacakan').val(humanDate(dtCase.tgl_pelacakan));
				$('#kontak').select2('val',dtCase.kontak);
				$('#keadaan_akhir').select2('val',dtCase.keadaan_akhir);
				$('#status_kasus').val(dtCase.status_kasus).select2();
				$.each(dtSpesimen, function(key, val){
					var jts = humanDate(val.tgl_ambil_spesimen);
					var jp = val.jenis_pemeriksaan;
					switch(val.jenis_pemeriksaan) {
						case '1':jjp='Isolasi Virus';break;
						case '2':jjp='ITD';break;
						case '3':jjp='Sequencing';break;
					}
					var jh = val.hasil;
					$('#spesimen tbody').append('<tr>'+
						'<td>'+jts+'</td>'+
						'<td>'+jjp+'</td>'+
						'<td>'+jh+'</td>'+
						'<td><a href="javascript:void(0);" class="rm"><i class="fa fa-remove"></i></a>'+
						'<input type="hidden" name="ds[spesimen][]" value="'+jts+'|'+jp+'|'+jh+'"></td>'+
						'</tr>'
						);
				});
				$('.rm').on('click',function(){
					$(this).parent().parent().remove();
					return false;
				});
				$('#klasifikasi_final').select2('val',dtCase.klasifikasi_final);
				return false;
			};
		});
	}
</script>

@endsection

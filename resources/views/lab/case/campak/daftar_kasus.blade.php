@include('lab.filter')
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Data Sampel Campak</h3>
	</div>
	<div class="box-body">
		<table id="table" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">No.Epid</th>
					<th rowspan="2">Nama Pasien</th>
					<th colspan="3">Usia</th>
					<th rowspan="2">Jenis Kelamin</th>
					<th rowspan="2">Kabupaten</th>
					<th colspan="4">Hasil Lab</th>
					<th rowspan="2">Keadaan akhir</th>
					<th rowspan="2">Aksi</th>
				</tr>
				<tr>
					<th>Thn</th>
					<th>Bln</th>
					<th>Hari</th>
					<th>Tgl Pemeriksaan</th>
					<th>Jenis Pemeriksaan</th>
					<th>Jenis Sampel</th>
					<th>Hasil Lab</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('#table').DataTable({
			"paging": true,
			"lengthChange": true,
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": true,
			"processing": true,
			"serverSide": true,
			"ajax": {
				url     : "{!! URL::to('lab/case/campak/getDataSampel') !!}",
				type    : "POST",
				data 	: function(d){
					d.dt = JSON.stringify($('#filter').serializeJSON());
				},
			},
			"columns"   : [
			{ "data" : "no" },
			{ "data" : "no_epid" },
			{ "data" : "name_pasien" },
			{ "data" : "umur_thn" },
			{ "data" : "umur_bln" },
			{ "data" : "umur_hari" },
			{ "data" : "jenis_kelamin" },
			{ "data" : "kabupaten" },
			{ "data" : "tgl_periksa" },
			{ "data" : "jenis_pemeriksaan" },
			{ "data" : "jenis_sampel" },
			{ "data" : "hasil" },
			{ "data" : "keadaan_akhir_txt" },
			{ "data" : "action" },
			]
		});
		$(".dataTables_filter").addClass('pull-right');

		$('#submit_filter').on('click',function(){
			$('#table').DataTable().draw();
			return false;
		});

		$('#table').on('draw.dt', function(){
			$(".delete").unbind();
			$(".delete-yes").unbind();
			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();
					},
				})
				.done(function(response){
					if(response.success){
						setTimeout( function(){
							messageAlert('info', 'Berhasil.', 'Data kasus berhasil di hapus.');
							endProcess();
						}, 0);
					}
					$('#table').DataTable().draw();
				});
				return false;
			});
		});
	});
</script>

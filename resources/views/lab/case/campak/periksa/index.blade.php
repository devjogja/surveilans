@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab_1" data-toggle="tab">Input Data Individual</a></li>
			<li><a href="#tab_2" data-toggle="tab">Input Hasil Laboratorium</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				@include('lab.case.campak.periksa.input_kasus')
			</div>
			<div class="tab-pane" id="tab_2">
				@include('lab.case.campak.periksa.input_lab')
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
	startProcess();
	getDetail();
});

function getDetail() {
	var id = $('#id_trx_case').val();
	var action = BASE_URL+'case/campak/getDetail/'+id;
	$.getJSON(action, function(result){
		var raw = result.response;
		if (isset(raw)) {
			$.each(raw, function(k, dt){
				$('.id_trx_case').val(dt.id);
				$('#name').val(dt.name_pasien);
				$('#id_pasien').val(dt.id_pasien);
				$('#nik').val(dt.nik);
				$('#no_rm').val(dt.no_rm);
				$('#agama').val(dt.agama).trigger('change');
				$('#nama_ortu').val(dt.nama_ortu);
				$('#id_family').val(dt.id_family);
				$('#jenis_kelamin').val(dt.jenis_kelamin).trigger('change');
				if(isset(dt.tgl_lahir)){$('#tgl_lahir').val(dt.tgl_lahir).removeAttr('disabled');}
				if(isset(dt.umur_hari)){$('#umur_hari').val(dt.umur_hari).removeAttr('disabled');}
				if(isset(dt.umur_bln)){$('#umur_bln').val(dt.umur_bln).removeAttr('disabled');}
				if(isset(dt.umur_thn)){$('#umur_thn').val(dt.umur_thn).removeAttr('disabled');}
				$('#alamat').val(dt.alamat);
				$('#wilayah').val(dt.full_address);
				$('#id_kelurahan_pasien').val(dt.code_kelurahan_pasien);
				$('#id_kecamatan_pasien').val(dt.code_kecamatan_pasien);
				$('#id_kabupaten_pasien').val(dt.code_kabupaten_pasien);
				$('#id_provinsi_pasien').val(dt.code_provinsi_pasien);
				$('#longitude').val(dt.longitude);
				$('#latitude').val(dt.latitude);
				$('#no_epid').val(dt.no_epid);
				if(isset(dt.no_epid_klb)){$('#no_epid_klb').val(dt.no_epid_klb);$('#klb').removeClass('hide')}
				$('#name_faskes').val(dt.name_faskes);
				$('#no_epid_lama').val(dt.no_epid_lama);
				$('#tgl_imunisasi_campak').val(dt.tgl_imunisasi_campak);
				$('#jml_imunisasi_campak').val(dt.jml_imunisasi_campak).trigger('change');
				$('#tgl_mulai_demam').val(dt.tgl_mulai_demam);
				$('#tgl_mulai_rash').val(dt.tgl_mulai_rash);
				$('#tgl_laporan_diterima').val(dt.tgl_laporan_diterima);
				$('#tgl_pelacakan').val(dt.tgl_pelacakan);
				$('#vitamin_A').val(dt.vitamin_a).trigger('change');
				$('#keadaan_akhir').val(dt.keadaan_akhir).trigger('change');
				$('#jenis_kasus').val(dt.jenis_kasus).trigger('change');
				if(isset(dt.klb_ke)){$('#klb_ke').val(dt.klb_ke).removeAttr('disabled').trigger('change');}
				$('#status_kasus').val(dt.status_kasus).trigger('change');
				$('#klasifikasi_final').val(dt.klasifikasi_final).trigger('change');
				var dtGejala = dt.dtGejala;
				var dtKomplikasi = dt.dtKomplikasi;
				var dtSpesimen = dt.dtSpesimen;
				$.each(dtGejala, function(key, val){
					$('#gejala'+val.id_gejala).iCheck('check').trigger('click');
					$('#tglKejadian'+val.id_gejala).val(val.tgl_kejadian);
				});
				var other_komplikasi = '';
				$.each(dtKomplikasi, function(key, val){
					$('#'+val.name).iCheck('check');
					if(isset(val.other_komplikasi)){
						other_komplikasi += val.other_komplikasi+', ';
					}
				});
				$.each(dtSpesimen, function(key, val){
					var hasil = '';
					if(isset(val.hasil_kultur)){ hasil += 'Kultur : '+val.hasil_kultur_txt; }
					if(isset(val.hasil_pcr)){ hasil += '<br/>PCR : '+val.hasil_pcr_txt; }
					if(isset(val.hasil_sqc)){ hasil += '<br/>Sequencing : '+val.hasil_sqc_txt; }
					if(isset(val.hasil_igm_campak)){ hasil += 'Igm Campak : '+val.hasil_igm_campak_txt; }
					if(isset(val.hasil_igm_rubella)){ hasil += '<br/>Igm Rubella : '+val.hasil_igm_rubella_txt; }
					$('#spesimen').append('<tr>'+
						'<td>'+isset(val.jenis_pemeriksaan_txt)+'</td>'+
						'<td>'+isset(val.jenis_spesimen_txt)+'</td>'+
						'<td><table class="table table-bordered"><tr><td>Tanggal pengambilan sampel</td><td style="width:20%; white-space:nowrap;">'+isset(val.tgl_ambil_spesimen)+'</td>'+
						'<tr><td>Tanggal pengiriman sampel</td><td>'+isset(val.tgl_kirim_lab)+'</td></tr>'+
						'<tr><td>Tanggal terima sampel</td><td>'+isset(val.tgl_terima_lab)+'</td></tr></table></td>'+
						'<td style="text-align:center;">'+isset(val.sampel_ke)+'</td>'+
						'<td id="'+val.id_trx_spesimen+'">'+hasil+'</td>'+
						'<td style="text-align:center;"><a class="btn btn-success" onclick="inputDataPemeriksaan('+val.id_trx_spesimen+','+val.jenis_pemeriksaan+')">Input Data Pemeriksaan</a>'+
						'<a href="javascript:void(0);" aid="'+val.id_trx_spesimen+'" class="btn btn-danger rm"><i class="fa fa-remove"></i></a>'+
						'</td>'+
						'</tr>'
					);
				});
			});
			$('.rm').on('click',function(e){
				if( !confirm('Yakin data akan di hapus ?') ){
					e.preventDefault();
				} else{
					var url = BASE_URL+'case/campak/deleteSpesimen';
					var dt = {
						"id": $(this).attr('aid')
					};
					$.post(url, JSON.stringify(dt), function(data){});
					$(this).parent().parent().remove();
				}
			});
			endProcess();
		};
	});
	var action1 = BASE_URL+'lab/case/campak/getDetail/'+id;
	$.getJSON(action1, function(result){
		var raw = result.response;
		if (isset(raw)) {
			$.each(raw, function(k, dt){
				$('#sumber_spesimen').select2('val',dt.sumber_spesimen);
				$('#id_outbreak').val(dt.id_outbreak);
				$('#laporan_oleh').select2('val',dt.laporan_oleh);
				$('#id_provinsi_outbreak').select2('val',dt.id_provinsi_outbreak);
				$('.id_kabupaten_outbreak').val(dt.id_kabupaten_outbreak);
				$('.id_kecamatan_outbreak').val(dt.id_kecamatan_outbreak);
				$('.id_kelurahan_outbreak').val(dt.id_kelurahan_outbreak);
				$('#alamat_outbreak').val(dt.alamat);
				$('#code_lab').val(dt.code_lab);
				$('#tgl_terima_spesimen').val(dt.tgl_terima_spesimen);
				$('#kondisi').select2('val',dt.kondisi);
			});
		}
	});
	return false;
}
</script>
@endsection

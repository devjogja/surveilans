<h4>Pemeriksaan Virologi</h4><hr>
{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form_virologi', 'class' => 'form-horizontal']) !!}
{!! Form::hidden('id', null, ['class' => 'form-control','id'=>'pv_id']) !!}
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Data Dasar Spesimen</h3>
	</div>
	<div class="box-body">
		<table class="table">
			<tr>
				<td style="width:20%" class="control-label">
					{!! Form::label('', 'Nama  :', ['class' => '']) !!}
				</td>
				<td style="width:30%">
					{!! Form::label('', '-', ['id' => 'pv_nama','class' => '']) !!}
				</td>
				<td style="width:20%" class="control-label">
					{!! Form::label('', 'Tanggal pemeriksaan kultur :', ['class' => '']) !!}
				</td>
				<td style="width:30%">
					{!! Form::label('', '-', ['id' => 'pv_tgl_periksa_kultur','class' => '']) !!}
				</td>
			</tr>
			<tr>
				<td class="control-label">
					{!! Form::label('', 'No. Epid  :', ['class' => '']) !!}
				</td>
				<td>
					{!! Form::label('', '-', ['id' => 'pv_no_epid', 'class' => '']) !!}
				</td>
				<td class="control-label">
					{!! Form::label('', 'Tanggal pemeriksaan PCR :', ['class' => '']) !!}
				</td>
				<td>
					{!! Form::label('', '-', ['id' => 'pv_tgl_periksa_pcr','class' => '']) !!}
				</td>
			</tr>
			<tr>
				<td class="control-label">
					{!! Form::label('', 'No. Outbreak  :', ['class' => '']) !!}
				</td>
				<td>
					{!! Form::label('', '-', ['id' => 'pv_id_outbreak','class' => '']) !!}
				</td>
				<td class="control-label">
					{!! Form::label('', 'Tanggal pemeriksaan sequencing :', ['class' => '']) !!}
				</td>
				<td>
					{!! Form::label('', '-', ['id' => 'pv_tgl_periksa_sequencing','class' => '']) !!}
				</td>
			</tr>
			<tr>
				<td class="control-label">
					{!! Form::label(null, 'No. Laboratorium  :', ['class' => '']) !!}
				</td>
				<td>
					{!! Form::label('', '-', ['id' => 'pv_code_lab','class' => '']) !!}
				<td></td><td></td>
			</tr>
		</table>
	</div>
</div>
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Kultur</h3>
	</div>
	<div class="box-body">
		<div class="form-group">
			{!! Form::label(null, 'Tanggal pemeriksaan kultur', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::text('dk[tgl_periksa_kultur]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal pemeriksaan kultur','id'=>'tgl_periksa_kultur']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Tanggal hasil kultur tersedia', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::text('dk[tgl_hasil_kultur_tersedia]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal hasil kultur tersedia','id'=>'tgl_hasil_kultur_tersedia']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Hasil pemeriksaan kultur', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::select('dk[hasil_kultur]', array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif','3'=>'Pending','4'=>'Equivocal'), null, ['class' => 'form-control', 'id'=>'hasil_kultur']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Tanggal hasil kultur dilaporkan', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::text('dk[tgl_hasil_dilaporkan_kultur]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal hasil tersedia','id'=>'tgl_hasil_dilaporkan_kultur']) !!}
			</div>
		</div>
	</div>
</div>
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title"> PCR</h3>
	</div>
	<div class="box-body">
		<div class="form-group">
			{!! Form::label(null, 'Onset kualifikasi spesimen PCR ', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::text('dp[onset_kualifikasi_pcr', null, ['class' => 'form-control','placeholder'=>'Uraikan onset kualifikasi spesiemen PCR dengan angka dalam satuan hari','id'=>'onset_kualifikasi_pcr']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Jenis Pemeriksaan', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::select('dp[jenis_pemeriksaan_pcr]', array(null=>'--Pilih--','1'=>'Genotyping (Campak)','2'=>'Genotyping (Rubella)','3'=>'Deteksi (Rubella)'), null, ['class' => 'form-control','id'=>'jenis_pemeriksaan_pcr']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Tanggal pemeriksaan', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::text('dp[tgl_periksa_pcr]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal pemeriksaan','id'=>'tgl_periksa_pcr']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Tanggal keluar hasil', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::text('dp[tgl_keluar_hasil_pcr]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal keluar hasil','id'=>'tgl_keluar_hasil_pcr']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Metode PCR', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::select('dp[metode_pcr]', array(null=>'--Pilih--','1'=>'Konvensional','2'=>"Realtime"), null, ['class' => 'form-control','id'=>'metode_pcr']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Hasil pemeriksaan PCR', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::select('dp[hasil_pcr]', array(null=>'--Pilih--','1'=>'Positif virus campak','2'=>'Negatif virus campak','3'=>'Positif virus rubella','4'=>'Negatif virus rubella','5'=>'Pending'), null, ['class' => 'form-control', 'id'=>'hasil_pcr']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Tanggal hasil dilaporkan', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::text('dp[tgl_hasil_dilaporkan_pcr]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal hasil dilaporkan','id'=>'tgl_hasil_dilaporkan_pcr']) !!}
			</div>
		</div>
	</div>
</div>
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Sequencing</h3>
	</div>
	<div class="box-body">
		<div class="form-group">
			{!! Form::label(null, 'Sampel dirujuk', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::select('ds[sampel_dirujuk_sqc]', array(null=>'--Pilih--','1'=>'Ya','2'=>'Tidak'), null, ['class' => 'form-control','id'=>'sampel_dirujuk_sqc']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Laboratorium tujuan rujukan', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::select('ds[lab_rujukan_sqc]', array(null=>'--Pilih--','1'=>'Biofarma','2'=>'LitBangKes Jakarta'), null, ['class' => 'form-control','id'=>'lab_rujukan_sqc']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Tanggal spesimen dirujuk', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::text('ds[tgl_dirujuk_sqc]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal spesimen dirujuk','id'=>'tgl_dirujuk_sqc']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Tanggal pemeriksaan sequencing', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::text('ds[tgl_pemeriksaan_sqc]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal pemeriksaan sequencing','id'=>'tgl_pemeriksaan_sqc']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Tanggal hasil sequencing tersedia', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::text('ds[tgl_hasil_sqc]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal hasil sequencing tersedia','id'=>'tgl_hasil_sqc']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Hasil sequencing (genotype)', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::select('ds[hasil_sqc]', array(null=>'--Pilih--','1'=>'Positif virus campak','2'=>'Negatif virus campak','3'=>'Positif virus rubella','4'=>'Negatif virus rubella','5'=>'Pending'), null, ['class' => 'form-control', 'id'=>'hasil_sqc']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Tanggal hasil sequencing dikirim dari lab rujukan', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::text('ds[tgl_hasil_kirim_darilab_sqc]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal hasil dilaporkan','id'=>'tgl_hasil_kirim_darilab_sqc']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Tanggal hasil sequencing dilaporkan', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::text('ds[tgl_hasil_dilaporkan_sqc]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal hasil sequencing tersedia','id'=>'tgl_hasil_dilaporkan_sqc']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'MeaNS/Ruben bank ref number', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::textarea('ds[means_ruben]',null, ['class' => 'form-control','id'=>'means_ruben','rows'=>'3','placeholder'=>'Uraiakan sesuai ketentuan yang berlaku']) !!}
			</div>
		</div>
	</div>
</div>
<div class="footer">
	<a class="btn btn-warning" onclick="backpv()">Tutup</a>
	{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
</div>
{!! Form::close() !!}

<script type="text/javascript">
function backpv(){
	$('#input_lab').show();
	$('#input_pemeriksaan').hide();
	var id=$('#pv_id').val();
	var hasil = '';
	if($('#hasil_kultur').val()!=''){
		hasil += 'Kultur : '+$('#hasil_kultur option:selected').text();
	}
	if($('#hasil_pcr').val()!=''){
		hasil += '<br/>PCR : '+$('#hasil_pcr option:selected').text();
	}
	if($('#hasil_sqc').val()!=''){
		hasil += '<br/>Sequencing : '+$('#hasil_sqc option:selected').text();
	}
	document.getElementById(id).innerHTML = hasil;
}
$(function(){
	$('#form_virologi').validate({
		rules:{
		'dfv[hasil_kultur]'  :'required',
		'dfv[hasil_pcr]':'required',
		'dfv[hasil_sqc]'  :'required',
		},
		messages:{
		'dfv[hasil_kultur]':'Data wajib diisi',
		'dfv[hasil_pcr]'  :'Data wajib diisi',
		'dfv[hasil_sqc]'  :'Data wajib diisi',
		},
		submitHandler: function(){
			var action = BASE_URL+'lab/case/campak/postHasilLab';
			var data = $('#form_virologi').serializeJSON();
			$.ajax({
				method : "POST",
				url : action,
				data : JSON.stringify(data),
				beforeSend: function(){
					startProcess();
				},
				success: function(data){
					endProcess();
					if (data.success==true) {
						backpv();
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal');
					}
				}
			});
			return false;
		}
  	});

	$('#tgl_periksa_kultur').on('change',function(){
		document.getElementById("pv_tgl_periksa_kultur").innerHTML=$(this).val();
		return false;
	});

	$('#tgl_periksa_pcr').on('change',function(){
		document.getElementById("pv_tgl_periksa_pcr").innerHTML=$(this).val();
		return false;
	});

	$('#tgl_pemeriksaan_sqc').on('change',function(){
		document.getElementById("pv_tgl_periksa_sequencing").innerHTML=$(this).val();
		return false;
	});
});
</script>

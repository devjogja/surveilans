<h4>Pemeriksaan Serologi</h4><hr>
{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form_serologi', 'class' => 'form-horizontal']) !!}
{!! Form::hidden('id', null, ['class' => 'form-control','id'=>'ps_id']) !!}
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Data Dasar Spesimen</h3>
	</div>
	<div class="box-body">
		<table class="table">
			<tr>
				<td style="width:20%" class="control-label">
					{!! Form::label('nama', 'Nama  :', ['class' => '']) !!}
				</td>
				<td style="width:30%">
					{!! Form::label('', '-', ['id' => 'ps_nama','class' => '']) !!}
				</td>
				<td style="width:20%" class="control-label">
					{!! Form::label('', 'Tanggal pemeriksaan IgM Campak :', ['class' => '']) !!}
				</td>
				<td>
					{!! Form::label('', '-', ['id' => 'ps_tgl_periksa_igm_campak','class' => '']) !!}
				</td>
			</tr>
			<tr>
				<td class="control-label">
					{!! Form::label('no_epid', 'No. Epid  :', ['class' => '']) !!}
				</td>
				<td>
					{!! Form::label('', '-', ['id' => 'ps_no_epid', 'class' => '']) !!}
				</td>
				<td class="control-label">
					{!! Form::label('', 'Tanggal pemeriksaan IgM Rubella :', ['class' => '']) !!}
				</td>
				<td>
					{!! Form::label('', '-', ['id' => 'ps_tgl_periksa_igm_rubella','class' => '']) !!}
				</td>
			</tr>
			<tr>
				<td class="control-label">
					{!! Form::label('', 'No. Outbreak  :', ['class' => '']) !!}
				</td>
				<td>
					{!! Form::label('', '-', ['id' => 'ps_id_outbreak','class' => '']) !!}
				</td>
				<td></td><td></td>
			</tr>
			<tr>
				<td class="control-label">
					{!! Form::label(null, 'No. Laboratorium  :', ['class' => '']) !!}
				</td>
				<td>
					{!! Form::label('', '-', ['id' => 'ps_code_lab','class' => '']) !!}
				<td></td><td></td>
			</tr>
		</table>
	</div>
</div>
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">IgM Campak</h3>
	</div>
	<div class="box-body">
		<div class="form-group">
			{!! Form::label(null, 'Tanggal pemeriksaan sampel', ['class' => 'col-sm-4 control-label']) !!}
			<div class="col-sm-5">
				{!! Form::text('dsr[tgl_periksa_igm_campak]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal pemeriksaan sampel','id'=>'tgl_periksa_igm_campak']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'KIT Elisa yang digunakan', ['class' => 'col-sm-4 control-label']) !!}
			<div class="col-sm-5">
				{!! Form::select('dsr[kit_igm_campak]', array(null=>'--Pilih--','1'=>'Siemens','2'=>'Lainnya'), ['class' => 'form-control','id'=>'kit_igm_campak']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Hasil IgM Campak', ['class' => 'col-sm-4 control-label']) !!}
			<div class="col-sm-5">
				{!! Form::select('dsr[hasil_igm_campak]', array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif','3'=>'Pending','4'=>'Equivocal'), null, ['class' => 'form-control', 'id'=>'hasil_igm_campak']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Tanggal hasil tersedia', ['class' => 'col-sm-4 control-label']) !!}
			<div class="col-sm-5">
				{!! Form::text('dsr[tgl_hasil_tersedia_igm_campak]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal hasil tersedia','id'=>'tgl_hasil_tersedia_igm_campak']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Tanggal hasil dilaporkan', ['class' => 'col-sm-4 control-label']) !!}
			<div class="col-sm-5">
				{!! Form::text('dsr[tgl_hasil_dilaporkan_igm_campak]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal hasil dilaporkan','id'=>'tgl_hasil_dilaporkan_igm_campak']) !!}
			</div>
		</div>
	</div>
</div>
<div class="box box-success" id="igm_rubella">
	<div class="box-header with-border">
		<h3 class="box-title">IgM Rubella</h3>
	</div>
	<div class="box-body">
		<div class="form-group">
			{!! Form::label(null, 'Tanggal pemeriksaan sampel', ['class' => 'col-sm-4 control-label']) !!}
			<div class="col-sm-5">
				{!! Form::text('dsr[tgl_periksa_igm_rubella', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal pemeriksaan sampel','id'=>'tgl_periksa_igm_rubella']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'KIT Elisa yang digunakan', ['class' => 'col-sm-4 control-label']) !!}
			<div class="col-sm-5">
				{!! Form::select('dsr[kit_igm_rubella]', array(null=>'--Pilih--','1'=>'Siemens','2'=>'Lainnya'), ['class' => 'form-control','id'=>'kit_igm_rubella',]) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Hasil IgM Rubella', ['class' => 'col-sm-4 control-label']) !!}
			<div class="col-sm-5">
				{!! Form::select('dsr[hasil_igm_rubella]', array(null=>'--Pilih--','1'=>'Positif','2'=>'Negatif','3'=>'Pending','4'=>'Equivocal'), null, ['class' => 'form-control', 'id'=>'hasil_igm_rubella']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Tanggal hasil tersedia', ['class' => 'col-sm-4 control-label']) !!}
			<div class="col-sm-5">
				{!! Form::text('dsr[tgl_hasil_tersedia_igm_rubella]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal hasil tersedia','id'=>'tgl_hasil_tersedia_igm_rubella']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label(null, 'Tanggal hasil dilaporkan', ['class' => 'col-sm-4 control-label']) !!}
			<div class="col-sm-5">
				{!! Form::text('dsr[tgl_hasil_dilaporkan_igm_rubella]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal hasil dilaporkan','id'=>'tgl_hasil_dilaporkan_igm_rubella']) !!}
			</div>
		</div>
	</div>
</div>
<div class="form-group">
	{!! Form::label(null, 'Penanggung Jawab', ['class' => 'col-sm-4 control-label']) !!}
	<div class="col-sm-5">
		{!! Form::text('dsr[penanggung_jawab]', null, ['class' => 'form-control','placeholder'=>'Penanggung Jawab','id'=>'penanggung_jawab']) !!}
	</div>
</div>
<div class="footer">
	<a class="btn btn-warning" onclick="backps()">Tutup</a>
	{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
</div>
{!! Form::close() !!}

<script type="text/javascript">
function backps(){
	$('#input_lab').show();
	$('#input_pemeriksaan').hide();
	var id=$('#ps_id').val();
	var hasil = 'Igm Campak : '+$('#hasil_igm_campak option:selected').text();
	if ($('#hasil_igm_rubella').val()!='') {
		hasil += '<br/>Igm Rubella : '+$('#hasil_igm_rubella option:selected').text();
	}
	document.getElementById(id).innerHTML = hasil;
}

$(function(){
	$('#form_serologi').validate({
		rules:{
		'dsr[hasil_igm_campak]'  :'required',
		},
		messages:{
		'dsr[hasil_igm_campak]':'Data wajib diisi',
		},
		submitHandler: function(){
			var action = BASE_URL+'lab/case/campak/postHasilLab';
			var data = $('#form_serologi').serializeJSON();
			$.ajax({
				method : "POST",
				url : action,
				data : JSON.stringify(data),
				beforeSend: function(){
					startProcess();
				},
				success: function(data){
					endProcess();
					if (data.success==true) {
						backps();
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal');
					}
				}
			});
			return false;
		}
  	});

	$('#igm_rubella').hide();
	$('#tgl_periksa_igm_campak').on('change',function(){
		document.getElementById("ps_tgl_periksa_igm_campak").innerHTML=$(this).val();
		return false;
	});

	$('#tgl_periksa_igm_rubella').on('change',function(){
		document.getElementById("ps_tgl_periksa_igm_rubella").innerHTML=$(this).val();
		return false;
	});

	$('#hasil_igm_campak').on('change',function(){
	    var val = $(this).val();
	    if (val==2) {
			$('#igm_rubella').show();
	    }else{
			$('#igm_rubella').hide();
	    }
	    return false;
	});
});
</script>

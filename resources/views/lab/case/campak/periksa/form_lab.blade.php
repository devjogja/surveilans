<div class="row">
	{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form_hasillab', 'class' => 'form-horizontal']) !!}
	{!! Form::hidden('dh[id_trx_case]', $data['id_trx_case'], ['class'=>'id_trx_case']) !!}
	<div class="col-sm-6">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Informasi Outbreak / Geografik</h3>
			</div>
			<div class="box-body">
				{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form_outbreak', 'class' => 'form-horizontal']) !!}
				<div class="form-group">
					{!! Form::label('sumber', 'Sumber Spesimen', ['class' => 'col-sm-4 control-label']) !!}
					<div class="col-sm-6">
						{!! Form::select('dh[sumber_spesimen]', array(null=>'--Pilih--','1'=>'KLB','2'=>'CBMS'),null, ['class' => 'form-control','id'=>'sumber_spesimen']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('id_outbreak', 'ID Outbreak', ['class' => 'col-sm-4 control-label']) !!}
					<div class="col-sm-6">
							{!! Form::text('dh[id_outbreak]', null, ['class' => 'form-control','placeholder'=>'ID Outbreak','id'=>'id_outbreak']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('laporan_oleh', 'Dilaporkan Oleh', ['class' => 'col-sm-4 control-label']) !!}
					<div class="col-sm-6">
							{!! Form::select('dh[laporan_oleh]', array(null=>'--Pilih--','1'=>'Puskesmas SMO/SO','2'=>'Klinik/Balai Pengobatan','3'=>'Rumah Sakit','4'=>'Praktek Swasta','5'=>'Lainnya','9'=>'Tidak Diketahui'), null, ['class' => 'form-control', 'id'=>'laporan_oleh']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label(null, 'Provinsi', ['class' => 'col-sm-4 control-label']) !!}
					<div class="col-sm-6">
						{!! Form::select('dh[id_provinsi_outbreak]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control','id'=>'id_provinsi_outbreak','onchange'=>"getKabupaten('_outbreak')"]) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label(null, 'Kabupaten/Kota', ['class' => 'col-sm-4 control-label']) !!}
					<div class="col-sm-6">
						{!! Form::hidden('', null, ['class' => 'id_kabupaten_outbreak']) !!}
						{!! Form::select('dh[id_kabupaten_outbreak]', array(null=>'Pilih Kabupaten/Kota'), null, ['class' => 'form-control', 'id'=>'id_kabupaten_outbreak','onchange'=>"getKecamatan('_outbreak')"]) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label(null, 'Kecamatan', ['class' => 'col-sm-4 control-label']) !!}
					<div class="col-sm-6">
						{!! Form::hidden('', null, ['class' => 'id_kecamatan_outbreak']) !!}
						{!! Form::select('dh[id_kecamatan_outbreak]', array(null=>'Pilih Kecamatan'), null, ['class' => 'form-control','id'=>'id_kecamatan_outbreak','onchange'=>"getKelurahan('_outbreak')"]) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label(null, 'Kelurahan/Desa', ['class' => 'col-sm-4 control-label']) !!}
					<div class="col-sm-6">
						<div class="input-group">
							{!! Form::hidden('', null, ['class' => 'id_kelurahan_outbreak']) !!}
							{!! Form::select('dh[id_kelurahan_outbreak]', array(null=>'Pilih Kelurahan/Desa'), null, ['class' => 'form-control','id'=>'id_kelurahan_outbreak']) !!}
							<span class="input-group-addon al">(*)</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('alamat', 'Alamat', ['class' => 'col-sm-4 control-label']) !!}
					<div class="col-sm-6">
						{!! Form::textarea('dh[alamat]',null, ['class' => 'form-control','id'=>'alamat_outbreak','rows'=>'3','placeholder'=>'Hanya diisi nama jalan, no. rumah dan no. RT - RW']) !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Informasi Spesimen Serologi</h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					{!! Form::label('no', 'No Laboratorium', ['class' => 'col-sm-4 control-label']) !!}
					<div class="col-sm-6">
						{!! Form::text('dh[code_lab]', null, ['class' => 'form-control','placeholder'=>'No Laboratorium','id'=>'code_lab']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('tgl_terima', 'Tanggal Terima Spesimen ', ['class' => 'col-sm-4 control-label']) !!}
					<div class="col-sm-6">
							{!! Form::text('dh[tgl_terima_spesimen]', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal Terima Spesimen','id'=>'tgl_terima_spesimen']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('kondisi', 'Kondisi spesimen waktu di terima di laboratorium', ['class' => 'col-sm-4 control-label']) !!}
					<div class="col-sm-6">
							{!! Form::select('dh[kondisi]', array(null=>'--Pilih--','1'=>'Baik','2'=>'Volume Kurang ','3'=>'Tidak Dingin','4'=>'Lisis','5'=>'Bocor/Tumpah','6'=>'Tidak Baik'), null, ['class' => 'form-control', 'id'=>'kondisi']) !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Data spesimen dan hasil laboratorium</h3>
			</div>
			<div class="box-body">
				<table class="table table-striped table-bordered" id="spesimen">
					<thead>
						<tr>
							<th>Jenis Pemeriksaan</th>
							<th>Tipe Spesimen</th>
							<th>Tanggal</th>
							<th>Sampel Ke-</th>
							<th>Hasil</th>
							<th style="width: 15%;">Aksi</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
				<div style="margin: 14px 0px;">
					<button type="button" class="btn btn-success btn-flat" id="addSpesimen"><i class="fa fa-plus-circle"></i> Tambah Spesimen</button>
				</div>
				<div id="div_sampel">
					<table class="table table-bordered">
						<tr>
							<th>Jenis Pemeriksaan</th>
							<th>Tipe Spesimen</th>
							<th style="width:30%">Tanggal</th>
							<th>Sampel Ke-</th>
						</tr>
						<tr>
							<td>{!! Form::select('', array(null=>'--Pilih--','1'=>'Serologi','2'=>'Virologi'), null, ['class' => 'form-control','id'=>'jenis_spesimen']) !!}</td>
							<td>{!! Form::select('', array(null=>'--Pilih--','11'=>'Darah','12'=>'Urin'), null, ['class' => 'form-control','id'=>'tipe_spesimen']) !!}</td>
							<td>
								<table>
									<tr><td>Tanggal pengambilan sampel</td>
										<td>{!! Form::text('', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal pengambilan sample','id'=>'tgl_ambil_sampel_campak']) !!}</td>
									</tr>
									<tr>
										<td>Tanggal pengiriman sampel</td>
										<td>{!! Form::text('', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal pengiriman sample','id'=>'tgl_kirim_sampel_campak']) !!}</td>
									</tr>
									<tr>
										<td>Tanggal terima sampel</td>
										<td>{!! Form::text('', null, ['class' => 'form-control datemax','placeholder'=>'Tanggal terima sample','id'=>'tgl_terima_sampel_campak']) !!}</td>
									</tr>
								</table>
							</td>
							<td>{!! Form::select('', array(null=>'--Pilih--','1'=>'1','2'=>'2 ','3'=>'3','4'=>'4','5'=>'5'), null, ['class' => 'form-control', 'id'=>'sample_ke']) !!}</td>
						</tr>
					</table>
					<div class="row">
						<div class="col-md-offset-4 col-md-8">
							<button class="btn btn-default" id='tutup_sampel'>Tutup</button>
							<button class="btn btn-success" id="tambah_spesimen">Tambah</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="footer">
			{!! Form::reset("Batal", ['class' => 'btn btn-warning batal']) !!}
			{!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
		</div>
	</div>
	{!! Form::close() !!}
</div>

<script type="text/javascript">
function inputDataPemeriksaan(dt,js) {
	$(window).scrollTop(100);
	getDetailHasillab(dt);
	if(js==1){
		$('#input_lab').hide();
		$('#input_pemeriksaan').show();
		$('#serologi').show();
		$('#virologi').hide();
		$('#ps_id').val(dt);
		document.getElementById("ps_nama").innerHTML=$('#name').val();
		document.getElementById("ps_id_outbreak").innerHTML=$('#id_outbreak').val();
		document.getElementById("ps_code_lab").innerHTML=$('#code_lab').val();
		document.getElementById("ps_no_epid").innerHTML=$('#no_epid').val();
	}else{
		$('#input_lab').hide();
		$('#input_pemeriksaan').show();
		$('#virologi').show();
		$('#serologi').hide();
		$('#pv_id').val(dt);
		document.getElementById("pv_nama").innerHTML=$('#name').val();
		document.getElementById("pv_id_outbreak").innerHTML=$('#id_outbreak').val();
		document.getElementById("pv_code_lab").innerHTML=$('#code_lab').val();
		document.getElementById("pv_no_epid").innerHTML=$('#no_epid').val();
	}
}

function getDetailHasillab(id) {
	var action1 = BASE_URL+'lab/case/campak/periksa/getDetail/'+id;
	$.getJSON(action1, function(result){
		var raw = result.response;
		if (isset(raw)) {
			$.each(raw, function(k, dt){
				$.each(dt, function(k1, v1){
					$('#'+k1).val(v1).trigger('change');
				});
			});
		}
	});
}

$(function(){
	$('#form_hasillab').validate({
		submitHandler: function(){
			var action = BASE_URL+'lab/case/campak/postInputLab';
			var data = $('#form_hasillab').serializeJSON();
			$.ajax({
				method : "POST",
				url : action,
				data : JSON.stringify(data),
				beforeSend: function(){
					startProcess();
				},
				success: function(data){
					endProcess();
					if (data.success==true) {
					}else{
						messageAlert('warning', 'Peringatan', 'Data gagal');
					}
				}
			});
			return false;
		}
  	});

	$('#div_sampel').hide();
	$('#tutup_sampel').on('click',function(){
		$('#div_sampel').hide(500);
		$('#addSpesimen').show();
		return false;
	});

	$('#addSpesimen').on('click',function(){
		$('#div_sampel').show(500);
		$('#addSpesimen').hide();
		$('#tambah_spesimen').attr('disabled','disabled');
		return false;
	});

	$('#jenis_spesimen').on('change',function(){
		var val = $(this).val();
		if(val=='1'){
			$('#tipe_spesimen').html("<option value=''>--Pilih--</option><option value='11'>Darah</option><option value='12'>Urin</option>");
		}else if(val=='2'){
			$('#tipe_spesimen').html("<option value=''>--Pilih--</option><option value='21'>Urin</option><option value='22'>Usap Tenggorokan</option><option value='23'>Cairan mulut</option>");
		}
		$('#tipe_spesimen').select2('val', '');
		$('#tambah_spesimen').removeAttr('disabled');
		return false;
	});

	$('#tambah_spesimen').on('click',function(){
		var jst = $('#jenis_spesimen option:selected').text();
		var js = $('#jenis_spesimen').val();
		var typet = $('#tipe_spesimen option:selected').text();
		var type = $('#tipe_spesimen').val();
		var tas = $('#tgl_ambil_sampel_campak').val();
		var tks = $('#tgl_kirim_sampel_campak').val();
		var tts = $('#tgl_terima_sampel_campak').val();
		var skt = $('#sample_ke').val();
		var sk = $('#sample_ke').val();
		$('#div_sampel').hide(500);
		$('#addSpesimen').show();
		$('#jenis_spesimen').select2('val','');
		$('#tipe_spesimen').select2('val', '');
		$('#tgl_ambil_sampel_campak').val(null);
		$('#tgl_kirim_sampel_campak').val(null);
		$('#tgl_terima_sampel_campak').val(null);
		$('#sample_ke').select2('val','');
		var url = BASE_URL+'case/campak/postSpesimen';
		var dt = {
			"id_trx_case": $('#id_trx_case').val(),
			"jenis_pemeriksaan": js,
			"jenis_spesimen": type,
			"tgl_ambil_spesimen": tas,
			"tgl_kirim_lab": tks,
			"tgl_terima_lab": tts,
			"sampel_ke":sk
		};
		$.post(url, JSON.stringify(dt), function(data){
			var id_trx_spesimen = data.response;
			$('#spesimen').append('<tr>'+
				'<td>'+jst+'</td>'+
				'<td>'+typet+'</td>'+
				'<td><table class="table table-bordered"><tr><td>Tanggal pengambilan sampel</td><td style="width:20%">'+tas+'</td>'+
				'<tr><td>Tanggal pengiriman sampel</td><td>'+tks+'</td></tr>'+
				'<tr><td>Tanggal terima sampel</td><td>'+tts+'</td></tr></table></td>'+
				'<td style="text-align:center;">'+skt+'</td>'+
				'<td id="'+id_trx_spesimen+'"></td>'+
				'<td style="text-align:center;"><a class="btn btn-success" onclick="inputDataPemeriksaan('+id_trx_spesimen+','+js+')">Input Data Pemeriksaan</a>'+
				'<a href="javascript:void(0);" aid="'+id_trx_spesimen+'" class="btn btn-danger rm"><i class="fa fa-remove"></i></a>'+
				'</td>'+
				'</tr>'
			);
			$('.rm').on('click',function(){
				if( !confirm('Yakin data akan di hapus ?') ){
					e.preventDefault();
				} else{
					var url = BASE_URL+'case/campak/deleteSpesimen';
					var dt = {
						"id": $(this).attr('aid')
					};
					$.post(url, JSON.stringify(dt), function(data){});
					$(this).parent().parent().remove();
				}
			});
		});
		return false;
	});
});
</script>

<div id="input_lab">
	@include('lab.case.campak.periksa.form_lab')
</div>
<div id="input_pemeriksaan">
	@include('lab.case.campak.periksa.input_data_pemeriksaan')
</div>

<script type="text/javascript">
$(function(){
	$('#input_lab').show();
	$('#input_pemeriksaan').hide();
});
</script>

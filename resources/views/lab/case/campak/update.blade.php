@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			@include('lab.case.campak.periksa.index')
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		getDetail();
	});

	function getDetail() {
		var id = $('#id_trx_case').val();
		var action = BASE_URL+'case/campak/getDetail/'+id;
		$.getJSON(action, function(result){
			var dt = result.response;
			if (isset(dt)) {
				var dtCase = dt.dtCase;
				var dtGejala = dt.dtGejala;
				var dtKomplikasi = dt.dtKomplikasi;
				var dtSpesimen = dt.dtSpesimen;
				$('#name').val(dtCase.name_pasien);
				$('#id_pasien').val(dtCase.id_pasien);
				$('#nik').val(dtCase.nik);
				$('#nama_ortu').val(dtCase.nama_ortu);
				$('#id_family').val(dtCase.id_family);
				$('#jenis_kelamin').val(dtCase.jenis_kelamin).select2();
				$('#tgl_lahir').val(humanDate(dtCase.tgl_lahir)).removeAttr('disabled');
				if(isset(dtCase.umur_thn) || isset(dtCase.umur_bln) || isset(dtCase.umur_hari)){
					$('#umur').val(dtCase.umur_thn).removeAttr('disabled');
					$('#umur_bln').val(dtCase.umur_bln).removeAttr('disabled');
					$('#umur_hari').val(dtCase.umur_hari).removeAttr('disabled');
				}
				$('#id_provinsi_pasien').val(dtCase.code_provinsi_pasien).select2();
				$('#id_kabupaten_pasien').empty().append('<option value="'+dtCase.code_kabupaten_pasien+'">'+dtCase.name_kabupaten_pasien+'</option>').select2();
				$('#id_kecamatan_pasien').empty().append('<option value="'+dtCase.code_kecamatan_pasien+'">'+dtCase.name_kecamatan_pasien+'</option>').select2();
				$('#id_kelurahan_pasien').empty().append('<option value="'+dtCase.code_kelurahan_pasien+'">'+dtCase.name_kelurahan_pasien+'</option>').select2();
				$('#alamat').val(dtCase.alamat);
				$('#no_epid').val(dtCase.no_epid);
				if(isset(dtCase.no_epid_klb)){
					$('#klb').removeClass('hide');
				}
				$('#no_epid_klb').val(dtCase.no_epid_klb);
				$('#no_epid_lama').val(dtCase.no_epid_lama);
				$('#tgl_imunisasi_campak').val(humanDate(dtCase.tgl_imunisasi_campak));
				$('#jml_imunisasi_campak').val(dtCase.jml_imunisasi_campak).select2();
				$('#tgl_mulai_demam').val(humanDate(dtCase.tgl_mulai_demam));
				$('#tgl_mulai_rash').val(humanDate(dtCase.tgl_mulai_rash));
				$('#tgl_laporan_diterima').val(humanDate(dtCase.tgl_laporan_diterima));
				$('#tgl_pelacakan').val(humanDate(dtCase.tgl_pelacakan));
				$('#vitamin_A').val(dtCase.vitamin_a).select2();
				$('#keadaan_akhir').val(dtCase.keadaan_akhir).select2();
				$('#jenis_kasus').val(dtCase.jenis_kasus).select2();
				$('#klb_ke').val(dtCase.klb_ke).select2();
				if(isset(dtCase.klb_ke)){
					$('#klb_ke').removeAttr('disabled');
				}
				$('#status_kasus').val(dtCase.status_kasus).select2();
				$.each(dtGejala, function(key, val){
					$('#gejala'+val.id).iCheck('check').trigger('click');
					$('#tglKejadian'+val.id).val(humanDate(val.tgl_kejadian));
				});
				var other_komplikasi = '';
				$.each(dtKomplikasi, function(key, val){
					$('#'+val.name).iCheck('check');
					if(isset(val.other_komplikasi)){
						other_komplikasi += val.other_komplikasi+', ';
					}
				});
				$('#komplikasi_lain_desc').val(other_komplikasi);
				if(isset(other_komplikasi)){
					$('#komplikasi_lain_desc').removeAttr('disabled');
				}
				$.each(dtSpesimen, function(key, val){
					var jpt = (val.jenis_pemeriksaan=='1')?'Serologi':'Virologi';
					var jp = val.jenis_pemeriksaan;
					var jst = '';
					if(val.jenis_spesimen=='11'){
						jst = 'Darah';
					}else if(val.jenis_spesimen=='12'){
						jst = 'Urin';
					}else if(val.jenis_spesimen=='21'){
						jst = 'Urin';
					}else if(val.jenis_spesimen=='22'){
						jst = 'Usap tenggorokan';
					}else if(val.jenis_spesimen=='23'){
						jst = 'Cairan Mulut';
					}
					var js = val.jenis_spesimen;
					var ts = humanDate(val.tgl_ambil_spesimen);
					$('.spesimen tbody').append('<tr>'+
						'<td>'+jpt+'</td>'+
						'<td>'+jst+'</td>'+
						'<td>'+ts+'</td>'+
						'<td><a href="javascript:void(0);" class="rm"><i class="fa fa-remove"></i></a>'+
						'<input type="hidden" name="ds[spesimen][]" value="'+jp+'|'+js+'|'+ts+'"></td>'+
						'</tr>'
						);
				});
				$('.rm').on('click',function(){
					$(this).parent().parent().remove();
				});
				return false;
			};
		});
}
</script>

@endsection

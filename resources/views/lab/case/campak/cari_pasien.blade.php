<div class="box-body">
	{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form', 'class' => 'form-horizontal']) !!}
	{!! Form::hidden('id_trx_case', $data['id_trx_case'], ['id'=>'id_trx_case']) !!}
	<div class="form-group">
		{!! Form::label('name', 'No Epid / Nama', ['class' => 'col-sm-2 control-label']) !!}
		<div class="col-sm-3">
				{!! Form::text('term', null, ['class' => 'form-control','id'=>'name','placeholder'=>'No.Epid / Nama Penderita','minlength'=>'3']) !!}
				{!! Form::hidden('id_pasien', null, ['class' => 'form-control','id'=>'id_pasien']) !!}
		</div>
		{!! Form::submit("Cari Pasien", ['class' => 'btn btn-success']) !!}
		<hr>
	</div>
	{!! Form::close() !!}
</div>
<div class="box-header" id="add_pasien" style="display:none">
	{!! Form::Button("Tambah Pasien", ['class' => 'btn btn-warning pull-right', 'id'=>'tambah_pasien_btn']) !!}
</div>
<div class="box-body">
	<table id="table_search" class="table table-bordered table-striped" style="display:none">
		<thead>
			<tr>
				<th>No</th>
				<th>No.Epid</th>
				<th>Nama Pasien</th>
				<th>Nama Ortu</th>
				<th>Alamat</th>
				<th>Rujukan</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody id="tbody_table_search"></tbody>
	</table>
	<?php
		$arealab  		=	Helper::getLabArea('campak',Helper::role()->id_faskes);
		$prov     		=	Helper::getProvince();
		$areaprovlab  =	array_intersect_key($prov,array_flip($arealab));
	?>
	@include('lab.filter_area_lab')
	<div class="box-body">
		<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#filter_modal">Sortir Data &nbsp;<i class="fa fa-cogs"></i></button>
	</div>
	<table id="table_daftar_pasien" class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>No</th>
				<th>No.Epid</th>
				<th>Nama Pasien</th>
				<th>Nama Ortu</th>
				<th>Alamat</th>
				<th>Faskes Pengirim</th>
				<th>Status</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody id="tbody_table_search"></tbody>
	</table>
</div>

<script type="text/javascript">
$(function(){
	var area = [@foreach (Helper::getLabArea('campak',Helper::role()->id_faskes) as $item)
	    '{{ $item }}',
	@endforeach ];

	$('#table_daftar_pasien').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"processing": true,
		"serverSide": true,
		"ajax": {
			url     : "{!! URL::to('lab/case/campak/getDataBasedAreaLab') !!}",
			type    : "POST",
			data 		: function(d){
				d.area = JSON.stringify(area);
				d.dt = JSON.stringify($('#filter_area_lab').serializeJSON());
				$('#filter_modal').modal('hide');
			},
		},
		"columns"   : [
			{ data: 'DT_Row_Index', searchable:false, orderable:false},
			{ data: 'no_epid', name:'no_epid'},
			{ data: 'name_pasien', name:'name_pasien'},
			{ data: 'nama_ortu', name:'nama_ortu'},
			{ data: 'full_address', name:'full_address'},
			{ data: 'name_faskes', name:'name_faskes'},
			{ data: 'status_kasus', name:'status_kasus'},
			{ data: 'action', searchable:false, orderable:false},
		]
	});

	$('#submit_area_lab').on('click',function(){
		if ($('#id_provinsi_area_lab').val()!=null) {
			area = [];
		}else{
			area = [@foreach (Helper::getLabArea('campak',Helper::role()->id_faskes) as $item)
			    '{{ $item }}',
			@endforeach ];
		}
		$('#table_daftar_pasien').DataTable().draw();
		return false;
	});
});

$('#tambah_pasien_btn').on('click',function(){
	$('#tambahpasien').show(500);
	$('#caripasien').hide();
	return false;
});

function getData(dt){
	$("#tbody_table_search").empty();
	for (var i = 0, len = dt.length; i < len; i++) {
		if (dt[i]['status_kasus']==1) {
			$label='<br><span class="label label-danger">Index</span>';
		}else{
			$label='';
		}
		$('#table_search').append('<tr>'+
		'<td>'+(i+1)+
		'<td>'+dt[i]['no_epid']+'</td>'+
		'<td>'+dt[i]['name_pasien']+$label+'</td>'+
		'<td>'+dt[i]['nama_ortu']+'</td>'+
		'<td>'+dt[i]['full_address']+'</td>'+
		'<td>--</td>'+
		'<td><a href="campak/periksa/'+dt[i]['id_trx_case']+'" title="Input Baru" data-toggle="tooltip" class="btn-sm btn-success" style="cursor: pointer;" >Input Baru</a></td>'+
		'</tr>'
		);
	}
}

$('#form').validate({
	rules:{
		'term':'required'
	},
	messages:{
		// 'term':'Wajib diisi'
	},
  submitHandler: function(){
    var action = BASE_URL+'pasien/getDataLab';
	var data = 	$('#name').val();
	$.ajax({
		method  : "GET",
		url     : action,
		data    : { term:data, case: 'campak' },
		beforeSend: function(){
			startProcess();
		},
		success: function(data){
			endProcess();
			if (data.length > 0) {
				$('#table_search').show();
				$('#add_pasien').show();
				getData(data);
			}else{
				$('#table_search').hide();
				$('#add_pasien').show();
				messageAlert('warning', 'Peringatan', 'Data tidak ditemukan.');
			}
		}
	});
    return false;
  	}
});

</script>

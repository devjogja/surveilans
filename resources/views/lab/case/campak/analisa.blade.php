@include('case.include.filter_analisa')
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Campak Berdasar Jenis Kelamin</h3>
		{!! Form::button('Unduh Data &nbsp<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','disabled']) !!}
		<div class="pull-right col-md-1" style="margin: 0px -10px 0px -7px;">
			{!! Form::select(null, array('rutin'=>'Rutin','klb'=>'KLB','all'=>'Rutin dan KLB'), 'campak', ['class' => 'form-control', 'id'=>'jenisKasusJenisKelamin','onchange'=>'graphJenisKelaminCampak();']) !!}
		</div>
		<div class="pull-right col-md-2" style="padding-right: 0px;">
			{!! Form::select(null, array('campak'=>'Suspek Campak','campak_all'=>'Campak (Lab,Epid, dan Klinis)','rubella'=>'Rubella'), 'campak', ['class' => 'form-control', 'id'=>'jenisDataJenisKelamin','onchange'=>'graphJenisKelaminCampak();']) !!}
		</div>
	</div>
	@include('case.chart.chart_gender')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Campak Berdasar Waktu</h3>
		{!! Form::button('Unduh Data &nbsp<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','disabled']) !!}
		<div class="pull-right col-md-1" style="margin: 0px -10px 0px -7px;">
			{!! Form::select(null, array('rutin'=>'Rutin','klb'=>'KLB','all'=>'Rutin dan KLB'), 'campak', ['class' => 'form-control', 'id'=>'jenisKasusWaktu','onchange'=>'graphWaktuCampak();']) !!}
		</div>
		<div class="pull-right col-md-2" style="padding-right: 0px;">
			{!! Form::select(null, array('campak'=>'Suspek Campak','campak_all'=>'Campak (Lab,Epid, dan Klinis)','rubella'=>'Rubella'), 'campak', ['class' => 'form-control', 'id'=>'jenisDataWaktu','onchange'=>'graphWaktuCampak();']) !!}
		</div>
	</div>
	@include('case.chart.chart_waktu')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Campak Berdasar Umur</h3>
		{!! Form::button('Unduh Data &nbsp<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','disabled']) !!}
		<div class="pull-right col-md-1" style="margin: 0px -10px 0px -7px;">
			{!! Form::select(null, array('rutin'=>'Rutin','klb'=>'KLB','all'=>'Rutin dan KLB'), 'campak', ['class' => 'form-control', 'id'=>'jenisKasusUmur','onchange'=>'graphUmurCampak();']) !!}
		</div>
		<div class="pull-right col-md-2" style="padding-right: 0px;">
			{!! Form::select(null, array('campak'=>'Suspek Campak','campak_all'=>'Campak (Lab,Epid, dan Klinis)','rubella'=>'Rubella'), 'campak', ['class' => 'form-control', 'id'=>'jenisDataUmur','onchange'=>'graphUmurCampak();']) !!}
		</div>
	</div>
	@include('case.chart.chart_umur')
</div>

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Campak Berdasar Status Imunisasi</h3>
		{!! Form::button('Unduh Data &nbsp<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','disabled']) !!}
		<div class="pull-right col-md-1" style="margin: 0px -10px 0px -7px;">
			{!! Form::select(null, array('rutin'=>'Rutin','klb'=>'KLB','all'=>'Rutin dan KLB'), 'campak', ['class' => 'form-control', 'id'=>'jenisKasusStatImun','onchange'=>'graphStatImunCampak();']) !!}
		</div>
		<div class="pull-right col-md-2" style="padding-right: 0px;">
			{!! Form::select(null, array('campak'=>'Suspek Campak','campak_all'=>'Campak (Lab,Epid, dan Klinis)','rubella'=>'Rubella'), 'campak', ['class' => 'form-control', 'id'=>'jenisDataStatImun','onchange'=>'graphStatImunCampak();']) !!}
		</div>
	</div>
	@include('case.chart.chart_stat_imun')

</div>
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Grafik Penderita Campak Berdasar Klasifikasi Final</h3>
		{!! Form::button('Unduh Data &nbsp<i class="fa fa-download"></i>', ['class' => 'btn btn-info pull-right','id'=>'export_excel','disabled']) !!}

	</div>
	@include('case.chart.chart_final')
</div>

<script type="text/javascript">
var range = '{{ strtoupper(Helper::role()->name_role).' '.Helper::role()->name_faskes }}'+'<br>'+'Tahun 2015-2016';
var title1 = 'Suspek Campak';
var title2 = ' Rutin';
var category = [@foreach (Helper::getCategoryUmur('campak') as $item)
    '{{ $item }}',
@endforeach ];
var dataUmur= [{name: 'Umur',colorByPoint: false,data: [6,8,3,5,1,]}];

		graphJenisKelamin(title1,title2,range);
		graphWaktu(title1,title2,range);
		graphUmur(title1,title2,range,category,dataUmur);
		graphStatImun(title1,title2,range);
		graphFinal(title1,range);

function graphJenisKelaminCampak() {
		jenis_kasus = $('#jenisKasusJenisKelamin').val();
		jenis_data = $('#jenisDataJenisKelamin').val();
		title1 = 'Suspek Campak';
		if(jenis_data=='campak_all'){
			title1 = 'Campak (Lab,Epid, dan Klinis)';
		}else if(jenis_data=='rubella'){
			title1 = 'Rubella';
		}
		title2 = ' Rutin'
		if (jenis_kasus=='klb') {
			title2 = ' KLB';
		}else if(jenis_kasus=='all'){
			title2 = ' Rutin dan KLB';
		}
		graphJenisKelamin(title1,title2,range);
}

function graphWaktuCampak() {
	jenis_kasus = $('#jenisKasusWaktu').val();
	jenis_data = $('#jenisDataWaktu').val();
	title1 = 'Suspek Campak';
	if(jenis_data=='campak_all'){
		title1 = 'Campak (Lab,Epid, dan Klinis)';
	}else if(jenis_data=='rubella'){
		title1 = 'Rubella';
	}
	title2 = ' Rutin'
	if (jenis_kasus=='klb') {
		title2 = ' KLB';
	}else if(jenis_kasus=='all'){
		title2 = ' Rutin dan KLB';
	}
		graphWaktu(title1,title2,range);
}

function graphUmurCampak() {
	jenis_kasus = $('#jenisKasusUmur').val();
	jenis_data = $('#jenisDataUmur').val();
	title1 = 'Suspek Campak';
	if(jenis_data=='campak_all'){
		title1 = 'Campak (Lab,Epid, dan Klinis)';
	}else if(jenis_data=='rubella'){
		title1 = 'Rubella';
	}
	title2 = ' Rutin'
	if (jenis_kasus=='klb') {
		title2 = ' KLB';
	}else if(jenis_kasus=='all'){
		title2 = ' Rutin dan KLB';
	}
	graphUmur(title1,title2,range,category,dataUmur);

}

function graphStatImunCampak() {
	jenis_kasus = $('#jenisKasusStatImun').val();
	jenis_data = $('#jenisDataStatImun').val();
	title1 = 'Suspek Campak';
	if(jenis_data=='campak_all'){
		title1 = 'Campak (Lab,Epid, dan Klinis)';
	}else if(jenis_data=='rubella'){
		title1 = 'Rubella';
	}
	title2 = ' Rutin'
	if (jenis_kasus=='klb') {
		title2 = ' KLB';
	}else if(jenis_kasus=='all'){
		title2 = ' Rutin dan KLB';
	}
		graphStatImun(title1,title2,range);
}

function hasilLabFinalCampak() {
		hasil_lab = $('#hasilLabFinal').val();
		title = 'Suspek Campak';
		if(hasil_lab=='campak_epid'){
			title = 'Campak Epid';
		}else if(hasil_lab=='campak_klinis'){
			title = 'Campak Klinis';
		}
		graphFinal(title);
}
</script>
<!-- @include('case.chart.chart_waktu')
@include('case.chart.chart_umur')
@include('case.chart.chart_stat_imun')
@include('case.chart.chart_final') -->

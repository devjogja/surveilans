@extends('layouts.base')
@section('content')
<div class="col-sm-12">
	<div class="box box-success">
		<div class="box-body">
			<div class="row">
				<div class="col-sm-8">
					SELAMAT DATANG DI {{ strtoupper(Helper::role()->name_role).' '.Helper::role()->name_faskes }}
					<a href='{{ URL::to('selectProfile')}}' class="btn btn-success btn-flat">Pilih Profil</a>
				</div>
				<div class="col-sm-4">
					<a href='' class="btn btn-primary btn-flat pull-right">Daftar Petugas Surveilans</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-sm-12">
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="item active">
				<div class="row-fluid col-sm-10 col-sm-offset-1">
					<center>
						@foreach(Helper::getCase() AS $key=>$val)
						@if($val->alias=='campak'||$val->alias=='crs'||$val->alias=='afp')
						<div class="col-sm-2">
							<div class="thumbnail" style="height:18rem;padding-top:3rem"><a
									href="{{ URL::to("lab/case/$val->alias")}}"><img
										src="{{URL::to("asset/images/$val->image")}}" alt="Image" class="img-circle"
										style="max-width:50%;" />
									<div class="caption">{{$val->name}}</div>
								</a></div>
						</div>
						@endif
						@endforeach
					</center>
				</div>
			</div>
			<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
				<span class="fa fa-angle-left blue"></span>
			</a>
			<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
				<span class="fa fa-angle-right blue"></span>
			</a>
		</div>
	</div>
</div>
<div class="col-sm-9">
	<div class="box box-success" style="height:500px">
		@include('dashboard.graph_dashboard')
	</div>
</div>
<div class="col-sm-3">
	<div class="box box-success" style="height:500px">
		<div class="box-body">
			<div class="col-sm-12">
				<span class="btn btn-block btn-default btn-flat" style="border-color: white;">Jumlah<br>Sample</span>
			</div>
			<div class="col-sm-12">
				<span class="btn btn-block btn-success btn-flat" style="border-color: white;">Campak<br>
					<span id="jml_sample_campak">{{$data['campak']}}</span> Sample</span>
			</div>
			<div class="col-sm-12">
				<span class="btn btn-block btn-success btn-flat" style="border-color: white;">AFP<br>
					<span id="jml_sample_afp">{{$data['afp']}}</span> Sample</span>
			</div>
			<div class="col-sm-12">
				<span class="btn btn-block btn-success btn-flat" style="border-color: white;">CRS<br>
					<span id="jml_sample_crs">{{$data['crs']}}</span> Sample</span>
			</div>
		</div>
	</div>
</div>
@endsection
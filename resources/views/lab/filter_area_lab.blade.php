<div class="modal fade" id="filter_modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Pengaturan Tampilan Data</h4>
			</div>
			<div class="modal-body">
        <div class="box box-success">
        	{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'filter_area_lab' , 'class' => 'form-horizontal']) !!}
        	<div class="box-body">
        		<div class="row">
        			<div class="col-sm-12">
        				<div class="box-body">
        					<div class="form-group">
        						<label for="input1" class="col-sm-3 control-label">Rentang Waktu</label>
        						<div class="col-sm-5">
        							{!! Form::select('range', array(null=>'All','1'=>'Hari','2'=>'Bulan','3'=>'Tahun'), null, ['class' => 'form-control', 'id'=>'range_time_analisa']) !!}
        						</div>
        					</div>
        					<div class="form-group">
        						<label for="input2" class="col-sm-3 control-label">Sejak</label>
        						<div class="col-sm-2" style="padding-right: 0px;">
        							<select name="from[day]" id="from_day_analisa" class="form-control" disabled="disabled">
        								<option value="">--Pilih--</option>
        								@for($i=1;$i<=31;$i++)
        									<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
        								@endfor
        							</select>
        						</div>
        						<div class="col-sm-3" style="padding-right: 0px;">
        							{!! Form::select('from[month]',
        								array(null => '--Pilih--',
        												'01'=>'Januari',
        												'02'=>'Febuari',
        												'03'=>'Maret',
        												'04'=>'April',
        												'05'=>'Mei',
        												'06'=>'Juni',
        												'07'=>'Juli',
        												'08'=>'Agustus',
        												'09'=>'September',
        												'10'=>'Oktober',
        												'11'=>'November',
        												'12'=>'Desember',
        												), null, ['class'=>'form-control','id'=>'from_month_analisa','disabled']) !!}
        						</div>
        						<div class="col-sm-3" style="padding-right: 0px;">
        							<select name="from[year]" id="from_year_analisa" class="form-control" disabled="disabled">
        								<option value="">--Pilih--</option>
        								@for($i=2010;$i<=2020;$i++)
        									<option value="{!! $i !!}">{!! $i !!}</option>
        								@endfor
        							</select>
        						</div>
        					</div>
        					<div class="form-group">
        						<label for="inputEmail3" class="col-sm-3 control-label">Sampai</label>
        						<div class="col-sm-2" style="padding-right: 0px;">
        							<select name="to[day]" id="to_day_analisa" class="form-control" disabled="disabled">
        								<option value="">--Pilih--</option>
        								@for($i=1;$i<=31;$i++)
        									<option value="{!! str_pad($i, 2, 0, STR_PAD_LEFT) !!}">{!! $i !!}</option>
        								@endfor
        							</select>
        						</div>
        						<div class="col-sm-3" style="padding-right: 0px;">
        							{!! Form::select('to[month]',
        								array(null => '--Pilih--',
        												'01'=>'Januari',
        												'02'=>'Febuari',
        												'03'=>'Maret',
        												'04'=>'April',
        												'05'=>'Mei',
        												'06'=>'Juni',
        												'07'=>'Juli',
        												'08'=>'Agustus',
        												'09'=>'September',
        												'10'=>'Oktober',
        												'11'=>'November',
        												'12'=>'Desember',
        												), null, ['class'=>'form-control','id'=>'to_month_analisa','disabled']) !!}
        						</div>
        						<div class="col-sm-3" style="padding-right: 0px;">
        							<select name="to[year]" id="to_year_analisa" class="form-control" disabled="disabled">
        								<option value="">--Pilih--</option>
        								@for($i=2010;$i<=2020;$i++)
        									<option value="{!! $i !!}">{!! $i !!}</option>
        								@endfor
        							</select>
        						</div>
        					</div>
        				</div>
        			</div>
        			<div class="col-sm-12">
        				<div class="box-body">
        					<div class="form-group">
        						<label for="input1" class="col-sm-3 control-label">Filter</label>
        						<div class="col-sm-5">
        							{!! Form::select('filter_type', array(null=>'--Pilih Filter Alamat--','1'=> 'Berdasarkan Alamat Pasien','2'=>'Data Instansi'), null, ['class' => 'form-control','id'=>'filter_type']) !!}
        						</div>
        					</div>
        					<div class="form-group">
        						<label for="input1" class="col-sm-3 control-label">Provinsi</label>
        						<div class="col-sm-5">
        							{!! Form::select('filter[id_provinsi]', array(null=>'Pilih Provinsi')+$areaprovlab, null, ['class' => 'form-control alamat prov','onchange'=>"getKabupaten('_area_lab')",'id'=>'id_provinsi_area_lab','disabled']) !!}
        						</div>
        					</div>
        					<div class="form-group">
        						<label for="input1" class="col-sm-3 control-label">Kabupaten</label>
        						<div class="col-sm-5">
        							{!! Form::select('filter[id_kabupaten]', array(null=>'Pilih Kabupaten'), null, ['class' => 'form-control alamat','onchange'=>"getKecamatan('_area_lab');getRs('_area_lab')",'id'=>'id_kabupaten_area_lab','disabled']) !!}
        						</div>
        					</div>
                  <div class="form-group">
        						<label for="input1" class="col-sm-3 control-label">Rumah Sakit</label>
        						<div class="col-sm-5">
        							{!! Form::select('filter[id_faskes]', array(null=>'Pilih Rumah Sakit'), null, ['class' => 'form-control alamat','id'=>'rs_area_lab','disabled']) !!}
        						</div>
        					</div><div class="form-group">
        						<label for="input1" class="col-sm-3 control-label">Kecamatan</label>
        						<div class="col-sm-5">
        							{!! Form::select('filter[id_kecamatan]', array(null=>'Pilih Kecamatan'), null, ['class' => 'form-control alamat','onchange'=>"getPuskesmas('_area_lab')",'id'=>'id_kecamatan_area_lab','disabled']) !!}
        						</div>
        					</div>
        					<div class="form-group">
        						<label for="input1" class="col-sm-3 control-label">Puskesmas</label>
        						<div class="col-sm-5">
        							{!! Form::select('filter[id_puskesmas]', array(null=>'Pilih Puskesmas'), null, ['class' => 'form-control alamat','id'=>'puskesmas_area_lab','disabled']) !!}
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        	<div class="box-footer" style="text-align: center">
        		{!! Form::submit("Tampilkan", ['class' => 'btn btn-success','id'=>'submit_area_lab']) !!}
        		{!! Form::submit("Export Excel", ['class' => 'btn btn-info','id'=>'export_excel_analisa','disabled']) !!}
        	</div>
        	{!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(function(){
    $('#id_provinsi_area_lab').on('change',function(){
    	var val = $(this).val();
    	if(val!=''){
    		$('#id_kabupaten_area_lab').removeAttr('disabled');
    	}else{
    		$('#id_kabupaten_area_lab').attr('disabled','disabled');
    	}
    	return false;
    });
    $('#id_kabupaten_area_lab').on('change',function(){
    	var val = $(this).val();
    	if(val!=''){
        $('#id_kecamatan_area_lab').removeAttr('disabled');
    		$('#rs_area_lab').removeAttr('disabled');
    	}else{
        $('#id_kecamatan_area_lab').attr('disabled','disabled');
    		$('#rs_area_lab').attr('disabled','disabled');
    	}
    	return false;
    });
    $('#id_kecamatan_area_lab').on('change',function(){
    	var val = $(this).val();
    	if(val!='' && $('#filter_type').val()==2){
    		$('#puskesmas_area_lab').removeAttr('disabled');
    	}else{
    		$('#puskesmas_area_lab').attr('disabled','disabled');
    	}
    	return false;
    });

		$('#puskesmas_area_lab').on('change',function(){
			var val = $(this).val();
			var id_faskes = "{!! Helper::role()->id_faskes !!}";
			if(val==id_faskes){
				$('#export_excel_analisa').removeAttr('disabled');
			}else{
				$('#export_excel_analisa').attr('disabled','disabled');
			}
			return false;
		});

		$('#filter_type').on('change',function(){
			var val = $(this).val();
			if(val==''){
				$('.alamat').attr('disabled','disabled').select2('val',null);
			}else if(val=='1'){
				$('#id_provinsi_area_lab').removeAttr('disabled');
				$('#id_provinsi_area_lab').attr('name',"filter[code_provinsi_pasien]");
				$('#id_kabupaten_area_lab').attr('name',"filter[code_kabupaten_pasien]");
				$('#id_kecamatan_area_lab').attr('name',"filter[code_kecamatan_pasien]");
				$('#puskesmas_area_lab').attr('name',"filter[code_puskesmas_pasien]");
			}else if(val=='2'){
				$('#id_provinsi_area_lab').removeAttr('disabled');
				$('#id_provinsi_area_lab').attr('name',"filter[code_provinsi_faskes]");
				$('#id_kabupaten_area_lab').attr('name',"filter[code_kabupaten_faskes]");
				$('#id_kecamatan_area_lab').attr('name',"filter[code_kecamatan_faskes]");
				$('#puskesmas_area_lab').attr('name',"filter[code_puskesmas_faskes]");
			}
			return false;
		});

		$('#range_time_analisa').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('#from_day_analisa').removeAttr('disabled');
				$('#from_month_analisa').removeAttr('disabled');
				$('#from_year_analisa').removeAttr('disabled');
				$('#to_day_analisa').removeAttr('disabled');
				$('#to_month_analisa').removeAttr('disabled');
				$('#to_year_analisa').removeAttr('disabled');
			}else if(val=='2'){
				$('#from_day_analisa').attr('disabled','disabled').select2('val',null);
				$('#from_month_analisa').removeAttr('disabled');
				$('#from_year_analisa').removeAttr('disabled');
				$('#to_day_analisa').attr('disabled','disabled').select2('val',null);
				$('#to_month_analisa').removeAttr('disabled');
				$('#to_year_analisa').removeAttr('disabled');
			}else if(val=='3'){
				$('#from_day_analisa').attr('disabled','disabled').select2('val',null);
				$('#from_month_analisa').attr('disabled','disabled').select2('val',null);
				$('#from_year_analisa').removeAttr('disabled');
				$('#to_day_analisa').attr('disabled','disabled').select2('val',null);
				$('#to_month_analisa').attr('disabled','disabled').select2('val',null);
				$('#to_year_analisa').removeAttr('disabled');
			}else{
				$('#from_day_analisa').attr('disabled','disabled').select2('val',null);
				$('#from_month_analisa').attr('disabled','disabled').select2('val',null);
				$('#from_year_analisa').attr('disabled','disabled').select2('val',null);
				$('#to_day_analisa').attr('disabled','disabled').select2('val',null);
				$('#to_month_analisa').attr('disabled','disabled').select2('val',null);
				$('#to_year_analisa').attr('disabled','disabled').select2('val',null);
			}
			return false;
		});
	})
</script>

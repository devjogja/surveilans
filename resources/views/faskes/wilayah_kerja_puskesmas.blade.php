@extends('layouts.base')
@section('content')
<div class="col-md-4">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">File Upload</h3>
            <a type="button" class="btn btn-success pull-right"  href="{!! url('faskes/wilayah_kerja_puskesmas/contoh_format') !!}" >Contoh Format</a>
        </div>
        <div class="box-body" style="text-align: center">
            <div class="col-sm-12">
                {!! Form::file("import_wilayah", ['class' => 'file-loading','id'=>'import_wilayah','multiple', 'files' => true, 'data-show-preview' => true,'data-allowed-file-extensions'=>'["csv","xls","xlsx"]']) !!}
                <div id="errorBlock" class="help-block"></div>
            </div>
        </div>
    </div>
    <script>
    $(document).on('ready', function() {
        $("#import_wilayah").fileinput({
            uploadUrl: BASE_URL + 'faskes/wilayah_kerja_puskesmas/import',
            maxFilePreviewSize: 10240
        });
    });
    </script>
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Form Wilayah Kerja Puskesmas</h3>
        </div>
        {!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form' , 'class' => 'form']) !!}
        {!! Form::hidden('id', null, ['id'=>'id']) !!}
        <div class="box-body">
            <div class="form-group">
                {!! Form::label(null, 'Provinsi', ['class' => 'control-label']) !!}
                {!! Form::select('', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control','id'=>'id_provinsi_master','onchange'=>"getKabupaten('_master')"]) !!}
            </div>
            <div class="form-group">
                {!! Form::label(null, 'Kabupaten', ['class' => 'control-label']) !!}
                {!! Form::select('', array(null=>'Pilih Kabupaten/Kota'), null, ['class' => 'form-control', 'id'=>'id_kabupaten_master','onchange'=>"getKecamatan('_master')"]) !!}
            </div>
            <div class="form-group">
                {!! Form::label(null, 'Kecamatan', ['class' => 'control-label']) !!}
                {!! Form::select('', array(null=>'Pilih Kecamatan'), null, ['class' => 'form-control', 'id'=>'id_kecamatan_master','onchange'=>"getKelurahan('_master'), getPuskesmas('_master')"]) !!}
            </div>
            <div class="form-group">
                {!! Form::label(null, 'Kelurahan', ['class' => 'control-label']) !!}
                {!! Form::select('dt[code_kelurahan]', array(null=>'Pilih Kelurahan'), null, ['class' => 'form-control', 'id'=>'id_kelurahan_master']) !!}
            </div>
            <div class="form-group">
                {!! Form::label(null, 'Puskesmas', ['class' => 'control-label']) !!}
                {!! Form::select('dt[code_faskes]', array(null=>'Pilih Puskesmas'), null, ['class' => 'form-control', 'id'=>'puskesmas_master']) !!}
            </div>
        </div>
        <div class="box-footer">
            {!! Form::reset("Batal", ['class' => 'btn btn-default','onclick'=>'resetform()']) !!}
            {!! Form::submit("Simpan", ['class' => 'btn btn-success pull-right']) !!}
        </div>
        {!! Form::close() !!}
    </div>
</div>
<div class="col-md-8">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">List Wilayah Kerja Puskesmas</h3>
        </div>
        <div class="box-body">
            <table id="table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Code</th>
                        <th>Nama Puskesmas</th>
                        <th>Nama Kelurahan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function() {
    var tUrl = "{!! url('faskes/wilayah_kerja_puskesmas/getData'); !!}";
    var tData = [
        { data: 'no', searchable: false, orderable: false, className: 'text-center nowrap' },
        { data: "code_faskes" },
        { data: "name" },
        { data: "full_address" },
        { data: 'action', searchable: false, orderable: false, className: 'text-center nowrap' },
    ];
    var dtable = $('#table').DataTable({
        paging: true,
        lengthChange: true,
        searching: true,
        ordering: true,
        autoWidth: false,
        processing: true,
        serverSide: true,
        ajax: {
            url: tUrl,
            type: "POST",
            data: function(d) {
                d.dt = JSON.stringify($('#filter').serializeJSON());
            },
        },
        buttons: [],
        columns: tData,
    });
    $(".dataTables_filter").addClass('pull-right');
    $('#table').on('draw.dt', function() {
        $(".edit").unbind();
        $(".delete").unbind();
        $(".delete-yes").unbind();

        $('.delete').on('click', function() {
            var action = $(this).attr('action');
            $('.delete-yes').attr('action', action);
        });
        $('.delete-yes').on('click', function() {
            $('.modaldelete').modal('toggle');
            var action = $(this).attr('action');
            $.ajax({
                method: "POST",
                url: action,
                data: null,
                dataType: "json",
                beforeSend: function() {
                    startProcess();
                },
                success: function(response) {
                    setTimeout(function() {
                        messageAlert(response.messageType, response.title, response.message);
                        endProcess();
                    }, 0);
                    $('#table').DataTable().draw();
                }
            });
            return false;
        });

        $('.edit').on('click', function() {
            var id = $(this).attr('dtId');
            var url = BASE_URL + 'faskes/wilayah_kerja_puskesmas/getDetail/' + id;
            $.getJSON(url, function(d, status) {
                if (isset(d.response)) {
                    d = d.response;
                    $('#id').val(d.id);
                    $('#code_faskes').val(d.code_faskes);
                    $('#name_kelurahan').val(d.alamat);
                    $('#id_provinsi_master').val(d.code_provinsi).select2();
                    $('#id_kabupaten_master').empty().append('<option value="' + d.code_kabupaten + '">' + d.name_kabupaten + '</option>').select2();
                    $('#id_kecamatan_master').empty().append('<option value="' + d.code_kecamatan + '">' + d.name_kecamatan + '</option>').select2();
                    $('#id_kelurahan_master').empty().append('<option value="' + d.code_kelurahan + '">' + d.name_kelurahan + '</option>').select2();
                    $('#puskesmas_master').empty().append('<option value="' + d.id_faskes + '">' + d.name + '</option>').select2();
                }
            });
            return false;
        });

        $('#form').validate({
            rules: {
                'dt[code_faskes]': 'required',
                'dt[code_kelurahan]': 'required'
            },
            messages: {
                'dt[code_faskes]': 'Code puskesmas wajib di isi',
                'dt[code_kelurahan]': 'Name Kelurahan wajib di isi',
            },
            submitHandler: function() {
                var action = BASE_URL + 'faskes/wilayah_kerja_puskesmas/post';
                var data = $('#form').serialize();
                $.ajax({
                    method: "POST",
                    url: action,
                    data: data,
                    dataType: "json",
                    beforeSend: function() {
                        startProcess();
                    },
                    success: function(response) {
                        if (response.success == '1') {
                            setTimeout(function() {
                                messageAlert(response.messageType, response.title, response.message);
                                endProcess();
                            }, 100);
                        }
                        window.location.reload();
                    }
                });
                return false;
            }
        });
    });
});

function resetform() {
    $('#id').val('');
    $('select').val('').trigger('change');
    $('form').trigger('reset');
    return false;
}
</script>
@endsection
@extends('layouts.base')
@section('content')
<div class="col-md-4">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Form Laboratorium</h3>
		</div>
		{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form' , 'class' => 'form']) !!}
		{!! Form::hidden('id', null, ['id'=>'id']) !!}
		<div class="box-body">
			<div class="form-group">
				{!! Form::label(null, 'Code Laboratorium', ['class' => 'control-label']) !!}
				{!! Form::text('dt[code]', null, ['class' => 'form-control','id'=>'code','placeholder'=>'Code Laboratorium']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Nama Laboratorium', ['class' => 'control-label']) !!}
				{!! Form::text('dt[name]', null, ['class' => 'form-control','id'=>'name','placeholder'=>'Nama Laboratorium']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Provinsi', ['class' => 'control-label']) !!}
				{!! Form::select('dt[code_provinsi]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control','id'=>'id_provinsi_master','onchange'=>"getKabupaten('_master')"]) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kabupaten', ['class' => 'control-label']) !!}
				{!! Form::select('dt[code_kabupaten]', array(null=>'Pilih Kabupaten/Kota'), null, ['class' => 'form-control', 'id'=>'id_kabupaten_master','onchange'=>"getKecamatan('_master')"]) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kecamatan', ['class' => 'control-label']) !!}
				{!! Form::select('dt[code_kecamatan]', array(null=>'Pilih Kecamatan'), null, ['class' => 'form-control', 'id'=>'id_kecamatan_master','onchange'=>"getKelurahan('_master')"]) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kelurahan/Desa', ['class' => 'control-label']) !!}
				{!! Form::select('dt[code_kelurahan]', array(null=>'Pilih Kelurahan'), null, ['class' => 'form-control', 'id'=>'id_kelurahan_master']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Alamat', ['class' => 'control-label']) !!}
				{!! Form::textarea('dt[alamat]', null, ['class' => 'form-control','id'=>'alamat','placeholder'=>'Alamat','rows'=>'3']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'No.Telp', ['class' => 'control-label']) !!}
				{!! Form::text('dt[telepon]', null, ['class' => 'form-control','id'=>'telepon','placeholder'=>'No.Telp']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'No.Fax', ['class' => 'control-label']) !!}
				{!! Form::text('dt[fax]', null, ['class' => 'form-control','id'=>'fax','placeholder'=>'Fax']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Email', ['class' => 'control-label']) !!}
				{!! Form::email('dt[email]', null, ['class' => 'form-control','id'=>'email','placeholder'=>'Email']) !!}
			</div>
		</div>
		<div class="box-footer">
			{!! Form::reset("Batal", ['class' => 'btn btn-default reset']) !!}
			{!! Form::submit("Simpan", ['class' => 'btn btn-success pull-right']) !!}
		</div>
		{!! Form::close() !!}
	</div>
</div>
<div class="col-md-8">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">List Laboratorium</h3>
		</div>
		<div class="box-body">
			<table id="table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Code</th>
						<th>Nama</th>
						<th>Alamat</th>
						<th>No.telp</th>
						@if(Helper::role()->id_role=='pusat')
						<th>Konfirm Code</th>
						@endif
						<th width="15%">Action</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('#table').DataTable({
			"paging": true,
			"lengthChange": true,
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				url     : '{!! URL::to('faskes/laboratorium/getData') !!}',
				type    : "POST",
			},
			"columns"   : [
			{ "data" : "code" },
			{ "data" : "name" },
			{ "data" : "full_alamat" },
			{ "data" : "telepon" },
			@if(Helper::role()->id_role=='pusat')
			{ "data" : "konfirm_code" },
			@endif
			{ "data" : "action" },
			]
		});
		$(".dataTables_filter").addClass('pull-right');
		$('.reset').on('click',function(){
			resetform();
			return false;
		});

		$('#table').on( 'draw.dt', function () {
			$(".edit").unbind();
			$(".cancel").unbind();
			$(".delete").unbind();
			$(".delete-yes").unbind();

			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();    
					},
					success: function(response){
						setTimeout( function(){
							messageAlert(response.messageType, response.title, response.message);
							endProcess();
						}, 0);
						$('#table').DataTable().draw();
					}
				});
				return false;
			});

			$('.edit').on('click',function(){
				var id = $(this).attr('dtId');
				var url= BASE_URL+'faskes/laboratorium/getDetail/'+id;
				$.getJSON(url,function(d, status){
					if(isset(d.response)){
						d=d.response;
						$('#id').val(d.id);
						$('#code').val(d.code);
						$('#name').val(d.name);
						$('#alamat').val(d.alamat);
						$('#telepon').val(d.telepon);
						$('#fax').val(d.fax);
						$('#email').val(d.email);
						$('#id_provinsi_master').val(d.code_provinsi).select2();
						$('#id_kabupaten_master').empty().append('<option value="'+d.code_kabupaten+'">'+d.name_kabupaten+'</option>').select2();
						$('#id_kecamatan_master').empty().append('<option value="'+d.code_kecamatan+'">'+d.name_kecamatan+'</option>').select2();
						$('#id_kelurahan_master').empty().append('<option value="'+d.code_kelurahan+'">'+d.name_kelurahan+'</option>').select2();
						$('#konfirm_code').val(d.konfirm_code);
					}
				});
				return false;
			});

			$('#form').validate({
				rules:{
					'dt[code]': 'required',
					'dt[name]' : 'required'
				},
				messages:{
					'dt[code]': 'Code wajib di isi',
					'dt[name]': 'Name wajib di isi',
				},
				submitHandler: function(){
					var action  = BASE_URL+'faskes/laboratorium/post';
					var data = $('#form').serialize();
					$.ajax({
						method  : "POST",
						url     : action,
						data    : data,
						dataType: "json",
						beforeSend: function(){
							startProcess();    
						},
						success: function(response){
							if(response.success=='1'){
								setTimeout( function(){
									messageAlert(response.messageType, response.title, response.message);
									endProcess();
								}, 100);
							}
							resetform();
							$('#table').DataTable().draw();
						}
					});
					return false;
				}
			});
		});
	});

	function resetform() {
		$('#id').val(null);
		$('select').val(null).select2();
		$('form').trigger('reset');
		return false;
	}
</script>
@endsection
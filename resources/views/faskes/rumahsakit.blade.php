@extends('layouts.base')
@section('content')
<div class="col-md-4">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Form Rumah Sakit</h3>
		</div>
		{!! Form::open(['method' => 'POST', 'url' => '', 'id'=>'form' , 'class' => 'form']) !!}
		{!! Form::hidden('id', null, ['id'=>'id']) !!}
		<div class="box-body">
			<div class="form-group">
				{!! Form::label(null, 'Code Faskes', ['class' => 'control-label']) !!}
				{!! Form::text('dt[code_faskes]', null, ['class' => 'form-control','id'=>'code_faskes','placeholder'=>'Code Faskses']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Code Faskes BPJS', ['class' => 'control-label']) !!}
				{!! Form::text('dt[code_faskes_bpjs]', null, ['class' => 'form-control','id'=>'code_faskes_bpjs','placeholder'=>'Code Faskes BPJS']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Nama Rumah sakit', ['class' => 'control-label']) !!}
				{!! Form::text('dt[name]', null, ['class' => 'form-control','id'=>'name','placeholder'=>'Nama Rumah sakit']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Type', ['class' => 'control-label']) !!}
				{!! Form::text('dt[type]', null, ['class' => 'form-control','id'=>'type','placeholder'=>'Type']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kepemilikan', ['class' => 'control-label']) !!}
				{!! Form::text('dt[kepemilikan]', null, ['class' => 'form-control','id'=>'kepemilikan','placeholder'=>'Kepemilikan']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Provinsi', ['class' => 'control-label']) !!}
				{!! Form::select('dt[code_provinsi]', array(null=>'Pilih Provinsi')+Helper::getProvince(), null, ['class' => 'form-control','id'=>'id_provinsi_master','onchange'=>"getKabupaten('_master')"]) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kabupaten', ['class' => 'control-label']) !!}
				{!! Form::select('dt[code_kabupaten]', array(null=>'Pilih Kabupaten/Kota'), null, ['class' => 'form-control', 'id'=>'id_kabupaten_master']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Alamat', ['class' => 'control-label']) !!}
				{!! Form::textarea('dt[alamat]', null, ['class' => 'form-control','id'=>'alamat','placeholder'=>'Alamat','rows'=>'3']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Kontrak BPJS', ['class' => 'control-label']) !!}
				{!! Form::select('dt[kontrak_bpjs]', array(null=>'Pilih','Ya'=>'Ya','Tidak'=>'Tidak'), null, ['class' => 'form-control', 'id'=>'kontrak_bpjs']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Regional BPJS', ['class' => 'control-label']) !!}
				{!! Form::text('dt[regional_bpjs]', null, ['class' => 'form-control','id'=>'regional_bpjs','placeholder'=>'Regional BPJS']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Longitude', ['class' => 'control-label']) !!}
				{!! Form::text('dt[longitude]', null, ['class' => 'form-control','id'=>'longitude','placeholder'=>'Longitude']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'Latitude', ['class' => 'control-label']) !!}
				{!! Form::text('dt[latitude]', null, ['class' => 'form-control','id'=>'latitude','placeholder'=>'Latitude']) !!}
			</div>
			<div class="form-group">
				{!! Form::label(null, 'No.Telp', ['class' => 'control-label']) !!}
				{!! Form::text('dt[telp]', null, ['class' => 'form-control','id'=>'telp','placeholder'=>'No.Telp']) !!}
			</div>
		</div>
		<div class="box-footer">
			{!! Form::reset("Batal", ['class' => 'btn btn-default reset']) !!}
			{!! Form::submit("Simpan", ['class' => 'btn btn-success pull-right']) !!}
		</div>
		{!! Form::close() !!}
	</div>
</div>
<div class="col-md-8">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">List Rumah sakit</h3>
		</div>
		<div class="box-body">
			<table id="table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Code</th>
						<th>Nama</th>
						<th>Alamat</th>
						<th>No.telp</th>
						@if(Helper::role()->id_role=='pusat')
						<th>Konfirm Code</th>
						@endif
						<th width="15%">Action</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('#table').DataTable({
			"paging": true,
			"lengthChange": true,
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				url     : '{!! URL::to('faskes/rumahsakit/getData') !!}',
				type    : "POST",
			},
			"columns"   : [
			{ "data" : "code_faskes" },
			{ "data" : "name" },
			{ "data" : "alamat" },
			{ "data" : "telp" },
			@if(Helper::role()->id_role=='pusat')
			{ "data" : "konfirm_code" },
			@endif
			{ "data" : "action" },
			]
		});
		$(".dataTables_filter").addClass('pull-right');
		$('.reset').on('click',function(){
			resetform();
			return false;
		});

		$('#table').on( 'draw.dt', function () {
			$(".edit").unbind();
			$(".cancel").unbind();
			$(".delete").unbind();
			$(".delete-yes").unbind();

			$('.delete').on('click',function(){
				var action = $(this).attr('action');
				$('.delete-yes').attr('action',action);
			});
			$('.delete-yes').on('click', function(){
				$('.modaldelete').modal('toggle');
				var action  = $(this).attr('action');
				$.ajax({
					method  : "POST",
					url     : action,
					data    : null,
					dataType: "json",
					beforeSend: function(){
						startProcess();    
					},
					success: function(response){
						setTimeout( function(){
							messageAlert(response.messageType, response.title, response.message);
							endProcess();
						}, 0);
						$('#table').DataTable().draw();
					}
				});
				return false;
			});

			$('.edit').on('click',function(){
				var id = $(this).attr('dtId');
				var url= BASE_URL+'faskes/rumahsakit/getDetail/'+id;
				$.getJSON(url,function(d, status){
					if(isset(d.response)){
						d=d.response;
						$('#id').val(d.id);
						$('#code_faskes').val(d.code_faskes);
						$('#code_faskes_bpjs').val(d.code_faskes_bpjs);
						$('#name').val(d.name);
						$('#type').val(d.type);
						$('#kepemilikan').val(d.kepemilikan);
						$('#alamat').val(d.alamat);
						$('#kontrak_bpjs').val(d.kontrak_bpjs).select2();
						$('#regional_bpjs').val(d.regional_bpjs);
						$('#longitude').val(d.longitude);
						$('#latitude').val(d.latitude);
						$('#telp').val(d.telp);
						$('#id_provinsi_master').val(d.code_provinsi).select2();
						$('#id_kabupaten_master').empty().append('<option value="'+d.code_kabupaten+'">'+d.name_kabupaten+'</option>').select2();
						$('#konfirm_code').val(d.konfirm_code);
					}
				});
				return false;
			});

			$('#form').validate({
				rules:{
					'dt[code_faskes]': 'required',
					'dt[name]' : 'required'
				},
				messages:{
					'dt[code_faskes]': 'Code wajib di isi',
					'dt[name]': 'Name wajib di isi',
				},
				submitHandler: function(){
					var action  = BASE_URL+'faskes/rumahsakit/post';
					var data = $('#form').serialize();
					$.ajax({
						method  : "POST",
						url     : action,
						data    : data,
						dataType: "json",
						beforeSend: function(){
							startProcess();    
						},
						success: function(response){
							if(response.success=='1'){
								setTimeout( function(){
									messageAlert(response.messageType, response.title, response.message);
									endProcess();
								}, 100);
							}
							resetform();
							$('#table').DataTable().draw();
						}
					});
					return false;
				}
			});
		});
	});

	function resetform() {
		$('#id').val(null);
		$('select').val(null).select2();
		$('form').trigger('reset');
		return false;
	}
</script>
@endsection